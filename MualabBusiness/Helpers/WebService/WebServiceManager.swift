//
//  WebServiceClass.swift
//  Link
//
//  Created by MINDIII on 10/3/17.
//  Copyright © 2017 MINDIII. All rights reserved.

import UIKit
import Alamofire

var strAuthToken : String = ""
let objServiceManager = WebServiceManager.sharedObject()
let objActivity = activityIndicator()

/*
 strip live
 pk_live_PfDNm4HgHKXuIK4yVCrr4ye500EJ4Cbm22
 sk_live_yIqrIhWPMHynsvs4W23NJbaB00WgrhEygt
 strip test
 pk_test_2mCH7F6JmjI04AyuBpS29VpK00Srv2nBF7
 sk_test_0RIcHEnw2dna6r9qEk1yLWDU006s0c6hT4
 */

class WebServiceManager: NSObject {
    
    //MARK: - Shared object
    fileprivate var window = UIApplication.shared.keyWindow
    
    private static var sharedNetworkManager: WebServiceManager = {
        let networkManager = WebServiceManager()
        return networkManager
    }()
    
    func getCurrentTimeZone() -> String{
        return TimeZone.current.identifier
    }
    
    // MARK: - Accessors
    class func sharedObject() -> WebServiceManager {
        return sharedNetworkManager
    }
    
    private let manager: Alamofire.SessionManager = {
        let configuration = URLSessionConfiguration.default
        return Alamofire.SessionManager(configuration: configuration)
    }()
    
    
    public func requestPost(strURL:String, params : [String:Any], success:@escaping(Dictionary<String,Any>) ->Void, failure:@escaping (Error) ->Void) {
        
        strAuthToken = ""
        if let token = UserDefaults.standard.string(forKey:UserDefaults.keys.authToken){
            strAuthToken = token
        }
        
        let url = WebURL.BaseUrl+strURL
        
        let headers = ["Authorization" : strAuthToken, "Content-Type":"application/x-www-form-urlencoded","X-APP-TYPE":"1","X-TIMEZONE":"","X-DEVICE-TYPE":"1","X-LOGIN-TYPE":"1"]
        
        // let headers = ["authtoken" : strAuthToken]
        //"Content-Type":"multipart/form-data"]
        print("BaseUrl = ",WebURL.BaseUrl)
        print("header = \(headers)")

        print("\nstrURL = \(strURL)")
        print("\nparams = \(params)")
        print("\nstrAuthToken = \(strAuthToken)")
        
        //manager.retrier = OAuth2Handler()
        manager.request(url, method: .post, parameters: params, headers: headers).responseJSON { responseObject in

            if responseObject.result.isSuccess {
                do {
                    let dictionary = try JSONSerialization.jsonObject(with: responseObject.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                    // Session Expire
                    let dict = dictionary as! Dictionary<String, Any>
                    if dict["status"] as? String != "success"{
                        if let msg = dict["message"] as? String{
                            if msg == message.invalidToken{
                                objAppShareData.showAlert(withMessage: "", type: alertType.sessionExpire,on: (self.window?.rootViewController)!)
                                return
                            }
                        }
                    }
                    print("\nResponce = \(dictionary)")
                    success(dictionary as! Dictionary<String, Any>)
                    self.StopIndicator()
                }catch{
                }
            }
            if responseObject.result.isFailure {
                let error : Error = responseObject.result.error!
                print("error = \(error)")
                failure(error)
                self.StopIndicator()
            }
        }
    }
    
    //==============================XXXX=================================//
    
    // MARK: - Request get method ----

    public func requestGetNew(strURL:String, params : [String : AnyObject]?, strCustomValidation:String , success:@escaping(Dictionary<String,Any>) ->Void, failure:@escaping (Error) ->Void ) {
        if !NetworkReachabilityManager()!.isReachable{
            let app = UIApplication.shared.delegate as? AppDelegate
            let window = app?.window
           // objAlert.showAlertVc(title: NoNetwork, controller: window!)
            DispatchQueue.main.async {
                self.StopIndicator()
            }
            return
        }
        
        strAuthToken = ""
        strAuthToken = ""
                if let token = UserDefaults.standard.string(forKey:UserDefaults.keys.authToken){
                    strAuthToken = token
                }
        
       // strAuthToken = "Bearer" + " " + objAppShareData.UserDetail.straAuthToken
        
        let currentTimeZone = getCurrentTimeZone()
        
//        var strUdidi = ""
//        if let MyUniqueId = UserDefaults.standard.string(forKey:UserDefaults.KeysDefault.strVenderId) {
//            print("defaults VenderID: \(MyUniqueId)")
//            strUdidi = MyUniqueId
//        }
        
         let headers = ["Authorization" : strAuthToken, "Content-Type":"application/x-www-form-urlencoded","X-APP-TYPE":"1","X-TIMEZONE":"","X-DEVICE-TYPE":"1","X-LOGIN-TYPE":"1"]
                
        let url = WebURL.BaseUrl+strURL
        print("url....\(url)")
        print("header....\(headers)")
        Alamofire.request(url, method: .get, parameters: params, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
            
            self.StopIndicator()
            
            if responseObject.result.isSuccess {
                do {
                    let dictionary = try JSONSerialization.jsonObject(with: responseObject.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                    success(dictionary as! Dictionary<String, Any>)
                    print(dictionary)
                    let status = dictionary["status"] as? String
                    if status == "error"
                    {
                        let error = dictionary["error_type"] as? String
                        
                        if error == "SESSION_EXPIRED" || error == "USER_NOT_FOUND"
                        {
                            //self.setRootSessionExpired()
                            self.StopIndicator()
                        }
                    }
                    
                }catch{
                    
                    let error : Error = responseObject.result.error!
                    failure(error)
                    let str = String(decoding: responseObject.data!, as: UTF8.self)
                    print("PHP ERROR : \(str)")
                }
            }
            if responseObject.result.isFailure {
                self.StopIndicator()
                let error : Error = responseObject.result.error!
                failure(error)
                
                let str = String(decoding: responseObject.data!, as: UTF8.self)
                print("PHP ERROR : \(str)")
            }
        }
    }
    
    
    //===========================XXXX=================================//
    
   
     public func requestGetString(strURL:String, params : [String : AnyObject]?, success:@escaping(Dictionary<String,Any>) ->Void, failure:@escaping (Error) ->Void ) {
         
         
         strAuthToken = ""
         if let token = UserDefaults.standard.string(forKey:UserDefaults.keys.authToken){
             strAuthToken = token
         }
         
         let url = WebURL.BaseUrl+strURL
         let headers = ["authtoken" : strAuthToken, "Content-Type":"application/x-www-form-urlencoded","X-APP-TYPE":"1","X-TIMEZONE":"","X-DEVICE-TYPE":"1","X-LOGIN-TYPE":"1"]
         //let headers = ["authtoken" : strAuthToken, "Content-Type":"Application/json","X-APP-TYPE":"1","X-TIMEZONE":"","X-DEVICE-TYPE":"1","X-LOGIN-TYPE":"1"]
       //  let headers = ["authtoken" : strAuthToken]
         //                      "Content-Type":"Application/json"]
         
         manager.retrier = OAuth2Handler()
         print("BaseUrl = ",WebURL.BaseUrl)

         print("\nstrURL = \(strURL)")
         //print("\nparams = \(params)")
         print("\nstrAuthToken = \(strAuthToken)")
         
         manager.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseString { responseObject in
             
             self.StopIndicator()
             
             if responseObject.result.isSuccess {
                 print(responseObject.data)
                 print("Response String: \(responseObject.result.value)")
                 //                let resJson = JSON(responseObject.result.value!)
                 //                success(resJson)
                
                
            //   let data = responseObject.data(using: String.Encoding.utf8, allowLossyConversion: false)
              //  let data = responseObject.data(using: .utf8)!
                //let data = responseObject.

                if let jsonData = responseObject.data
                {
                    // Will return an object or nil if JSON decoding fails
                    do
                    {
                        let message = try JSONSerialization.jsonObject(with: jsonData, options:.mutableContainers)
                        if let jsonResult = message as? NSMutableArray
                        {
                            print(jsonResult)

                           // return jsonResult //Will return the json array output
                        }
                        else
                        {
                            print("Error")
                          //  return nil
                        }
                    }
                    catch let error as NSError
                    {
                        print("An error occurred: \(error)")
                      //  return nil
                    }
                }
                else
                {
                    print("Error2")
                    // Lossless conversion of the string was not possible
                   // return nil
                }
//                 do {
//                     let dictionary = try JSONSerialization.jsonObject(with: responseObject.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
//                     // Session Expire
//                     let dict = dictionary as! Dictionary<String, Any>
//                     if dict["status"] as! String != "success"{
//
//                         if let msg = dict["message"] as? String{
//                             if msg == message.invalidToken{
//                                 self.StopIndicator()
//                                 objAppShareData.showAlert(withMessage: "", type: alertType.sessionExpire,on: (self.window?.rootViewController)!)
//                                 return
//                             }
//                         }
//                     }
//                     print("\nResponce = \(dictionary)")
//                     success(dictionary as! Dictionary<String, Any>)
//                 }catch{
//
//                 }
             }
             if responseObject.result.isFailure {
                 let error : Error = responseObject.result.error!
                 print("error = \(error)")
                 failure(error)
             }
         }
     }
    
    
    
    public func requestGet(strURL:String, params : [String : AnyObject]?, success:@escaping(Dictionary<String,Any>) ->Void, failure:@escaping (Error) ->Void ) {
        
        
        strAuthToken = ""
        if let token = UserDefaults.standard.string(forKey:UserDefaults.keys.authToken){
            strAuthToken = token
        }
        
        let url = WebURL.BaseUrl+strURL
        let headers = ["authtoken" : strAuthToken, "Content-Type":"application/x-www-form-urlencoded","X-APP-TYPE":"1","X-TIMEZONE":"","X-DEVICE-TYPE":"1","X-LOGIN-TYPE":"1"]
      //  let headers = ["authtoken" : strAuthToken]
        //                      "Content-Type":"Application/json"]
        
        manager.retrier = OAuth2Handler()
        print("BaseUrl = ",WebURL.BaseUrl)

        print("\nstrURL = \(strURL)")
        //print("\nparams = \(params)")
        print("\nstrAuthToken = \(strAuthToken)")
        
        manager.request(url, method: .get, parameters: params, encoding: URLEncoding.default, headers: headers).responseJSON { responseObject in
            
            self.StopIndicator()
            
            if responseObject.result.isSuccess {
               
                //                let resJson = JSON(responseObject.result.value!)
                //                success(resJson)
                do {
                    let dictionary = try JSONSerialization.jsonObject(with: responseObject.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                    // Session Expire
                    let dict = dictionary as! Dictionary<String, Any>
                    if dict["status"] as! String != "success"{
                        
                        if let msg = dict["message"] as? String{
                            if msg == message.invalidToken{
                                self.StopIndicator()
                                objAppShareData.showAlert(withMessage: "", type: alertType.sessionExpire,on: (self.window?.rootViewController)!)
                                return
                            }
                        }
                    }
                    print("\nResponce = \(dictionary)")
                    success(dictionary as! Dictionary<String, Any>)
                }catch{
                    
                }
            }
            if responseObject.result.isFailure {
                let error : Error = responseObject.result.error!
                print("error = \(error)")
                failure(error)
            }
        }
    }
    
    public func uploadMultipartData(strURL:String, params : [String : AnyObject]?, imageData:Data?, fileName:String, key:String, mimeType:String, success:@escaping(Dictionary<String,Any>) ->Void, failure:@escaping (Error) ->Void){
        if !NetworkReachabilityManager()!.isReachable{
            self.StopIndicator()
            let app = UIApplication.shared.delegate as? AppDelegate
            let window = app?.window
            self.showAlert(message: "", title: message.noNetwork , controller: window!)
            
            return
        }
        
        strAuthToken = ""
        if let token = UserDefaults.standard.string(forKey:UserDefaults.keys.authToken){
            strAuthToken = token
        }
        
        let url = WebURL.BaseUrl+strURL
        let headers = ["authtoken" : strAuthToken, "Content-Type":"application/x-www-form-urlencoded","X-APP-TYPE":"1","X-TIMEZONE":"","X-DEVICE-TYPE":"1","X-LOGIN-TYPE":"1"]
       // let headers = ["authtoken" : strAuthToken]
        //  "Content-Type":"Application/json"]
        manager.retrier = OAuth2Handler()
        print("BaseUrl = ",WebURL.BaseUrl)

        print("\nstrURL = \(strURL)")
       // print("\nparams = \(params)")
        print("\nstrAuthToken = \(strAuthToken)")
        
        manager.upload(multipartFormData:{ multipartFormData in
            
            if key == "NO"{}else{
            if let data = imageData{
                multipartFormData.append(data,
                                         withName:key,
                                         fileName:fileName,
                                         mimeType:mimeType)
            }
            }
            for (key, value) in params! {
                multipartFormData.append(value.data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }},
                       usingThreshold:UInt64.init(),
                       to:url,
                       method:.post,
                       headers:headers,
                       encodingCompletion: { encodingResult in
                        switch encodingResult {
                        case .success(let upload, _, _):
                            upload.responseJSON { responseObject in
                                self.StopIndicator()
                                if responseObject.result.isSuccess {
                                    //                                        let resJson = JSON(responseObject.result.value!)
                                    //                                        success(resJson)
                                    do {
                                        let dictionary = try JSONSerialization.jsonObject(with: responseObject.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                                        // Session Expire
                                        let dict = dictionary as! Dictionary<String, Any>
                                        if dict["status"] as! String != "success"{
                                            
                                            if let msg = dict["message"] as? String{
                                                if msg == message.invalidToken{
                                                    objAppShareData.showAlert(withMessage: "", type: alertType.sessionExpire,on: (self.window?.rootViewController)!)
                                                    return
                                                }
                                            }
                                            
                                        }
                                        print("\nResponce = \(dictionary)")
                                        success(dictionary as! Dictionary<String, Any>)
                                    }catch{
                                    }
                                }
                                if responseObject.result.isFailure {
                                    let error : Error = responseObject.result.error!
                                    failure(error)
                                }
                            }
                        case .failure(let encodingError):
                            print(encodingError)
                            self.StopIndicator()
                            failure(encodingError)
                        }
        })
    }
    
    public func requestPostMultipartData(strURL:String, params : [String:Any], success:@escaping(Dictionary<String,Any>) ->Void, failure:@escaping (Error) ->Void ) {
        
        if !NetworkReachabilityManager()!.isReachable{
            self.StopIndicator()
            let app = UIApplication.shared.delegate as? AppDelegate
            let window = app?.window
            self.showAlert(message: "", title: message.noNetwork ,controller: window!)
            return
        }
        
        strAuthToken = ""
        if let token = UserDefaults.standard.string(forKey:UserDefaults.keys.authToken){
            strAuthToken = token
        }
        
        let url = WebURL.BaseUrl+strURL
        let headers = ["authtoken" : strAuthToken, "Content-Type":"application/x-www-form-urlencoded","X-APP-TYPE":"1","X-TIMEZONE":"","X-DEVICE-TYPE":"1","X-LOGIN-TYPE":"1"]
       // let headers = ["authtoken" : strAuthToken,
        //               "Content-Type":"multipart/form-data"]
        
        manager.retrier = OAuth2Handler()
        print("BaseUrl = ",WebURL.BaseUrl)

        print("\nstrURL = \(strURL)")
        print("\nparams = \(params)")
        print("\nstrAuthToken = \(strAuthToken)")
        
        manager.upload(multipartFormData:{ multipartFormData in
            
            for (key, value) in params {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }},
                       // usingThreshold:UInt64.init(),
            to:url,
            method:.post,
            headers:headers,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { responseObject in
                        self.StopIndicator()
                        if responseObject.result.isSuccess {
                            //                                        let resJson = JSON(responseObject.result.value!)
                            //                                        success(resJson)
                            do {
                                let dictionary = try JSONSerialization.jsonObject(with: responseObject.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                                // Session Expire
                                let dict = dictionary as! Dictionary<String, Any>
                                if dict["status"] as! String != "success"{
                                    
                                    if let msg = dict["message"] as? String{
                                        if msg == message.invalidToken{
                                            objAppShareData.showAlert(withMessage: "", type: alertType.sessionExpire,on: (self.window?.rootViewController)!)
                                            return
                                        }
                                    }
                                }
                                print("\nResponce = \(dictionary)")
                                success(dictionary as! Dictionary<String, Any>)
                            }catch{
                            }
                        }
                        if responseObject.result.isFailure {
                            let error : Error = responseObject.result.error!
                            failure(error)
                        }
                    }
                case .failure(let encodingError):
                    print(encodingError)
                    self.StopIndicator()
                    failure(encodingError)
                }
        })
    }
    //MARK : -  Return Json Methods
    public func requestPostForJson(strURL:String, params : [String:Any], success:@escaping(Dictionary<String,Any>) ->Void, failure:@escaping (Error) ->Void ) {
        
        strAuthToken = ""
        if let token = UserDefaults.standard.string(forKey:UserDefaults.keys.authToken){
            strAuthToken = token
        }
        
        let url = WebURL.BaseUrl+strURL
        let headers = ["authtoken" : strAuthToken]
        
        //"Content-Type":"multipart/form-data"]
        manager.retrier = OAuth2Handler()
        print("BaseUrl = ",WebURL.BaseUrl)

        print("\nstrURL = \(strURL)")
        print("\nparams = \(params)")
        print("\nstrAuthToken = \(strAuthToken)")
        
        manager.request(url, method: .post, parameters: params, headers: headers).responseJSON { responseObject in
            self.StopIndicator()
            if responseObject.result.isSuccess {
                let convertedString = String(data: responseObject.data!, encoding: String.Encoding.utf8) // the data will be converted to the string
                let dict = self.convertToDictionary(text: convertedString!)
                
                if dict!["status"] as! String != "success"{
                    if let msg = dict!["message"] as? String{
                        if msg == message.invalidToken{
                            objAppShareData.showAlert(withMessage: "", type: alertType.sessionExpire,on: (self.window?.rootViewController)!)
                            return
                        }
                    }
                }
               // print("\nResponce = \(dict)")
                success(dict!)
            }
            if responseObject.result.isFailure {
                let error : Error = responseObject.result.error!
                print("error = \(error)")
                failure(error)
            }
        }
    }
    
    // Return Json Methods
    public func requestGetForJson(strURL:String, params : [String:Any], success:@escaping(Dictionary<String,Any>) ->Void, failure:@escaping (Error) ->Void ) {
        
        strAuthToken = ""
        if let token = UserDefaults.standard.string(forKey:UserDefaults.keys.authToken){
            strAuthToken = token
        }
        
        let url = WebURL.BaseUrl+strURL
        let headers = ["authtoken" : strAuthToken]
        //"Content-Type":"multipart/form-data"]
        
        manager.retrier = OAuth2Handler()
        print("BaseUrl = ",WebURL.BaseUrl)

        print("\nstrURL = \(strURL)")
        print("\nparams = \(params)")
        print("\nstrAuthToken = \(strAuthToken)")
        
        manager.request(url, method: .get, parameters: params, headers: headers).responseJSON { responseObject in
            self.StopIndicator()
            if responseObject.result.isSuccess {
                let convertedString = String(data: responseObject.data!, encoding: String.Encoding.utf8) // the data will be converted to the string
                let dict = self.convertToDictionary(text: convertedString!)
                if dict!["status"] as! String != "success"{
                    if let msg = dict!["message"] as? String{
                        if msg == message.invalidToken{
                            objAppShareData.showAlert(withMessage: "", type: alertType.sessionExpire,on: (self.window?.rootViewController)!)
                            return
                        }
                    }
                }
                success(dict!)
            }
            if responseObject.result.isFailure {
                let error : Error = responseObject.result.error!
                print("error = \(error)")
                failure(error)
            }
        }
    }
    
    //MARK: - mathod without indicators
    public func requestPostWithOutIndicator(strURL:String, params : [String:Any], success:@escaping(Dictionary<String,Any>) ->Void, failure:@escaping (Error) ->Void ) {
        
        strAuthToken = ""
        if let token = UserDefaults.standard.string(forKey:UserDefaults.keys.authToken){
            strAuthToken = token
        }
        
        let url = WebURL.BaseUrl+strURL
        print("BaseUrl = ",WebURL.BaseUrl)
        let headers = ["authtoken" : strAuthToken]
        //"Content-Type":"multipart/form-data"]
        
        manager.retrier = OAuth2Handler()
        
        manager.request(url, method: .post, parameters: params, headers: headers).responseJSON { responseObject in
            if responseObject.result.isSuccess {
                let convertedString = String(data: responseObject.data!, encoding: String.Encoding.utf8) // the data will be converted to the string
                let dict = self.convertToDictionary(text: convertedString!)
                if dict!["status"] as! String != "success"{
                    if let msg = dict!["message"] as? String{
                        if msg == message.invalidToken{
                            objAppShareData.showAlert(withMessage: "", type: alertType.sessionExpire,on: (self.window?.rootViewController)!)
                            return
                        }
                    }
                }
                success(dict!)
            }
            if responseObject.result.isFailure {
                let error : Error = responseObject.result.error!
                print("error = \(error)")
                failure(error)
            }
        }
    }
    
    //MARK: - Convert String to Dict
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    func isNetworkAvailable() -> Bool{
        if !NetworkReachabilityManager()!.isReachable{
            return false
        }else{
            return true
        }
    }
    
    func StartIndicator(){
        objActivity.startActivityIndicator()
    }
    
    func StopIndicator(){
        objActivity.stopActivity()
    }
    
    func showAlert(message: String = "", title: String , controller: UIWindow) {
        DispatchQueue.main.async(execute: {
            
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let subView = alertController.view.subviews.first!
            let alertContentView = subView.subviews.first!
            alertContentView.backgroundColor = UIColor.gray
            alertContentView.layer.cornerRadius = 20
            let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(OKAction)
            self.window?.rootViewController?.present(alertController, animated: true, completion: nil)
        })
    }
}

//MARK :- retrier

class OAuth2Handler : RequestRetrier {
    
    public func should(_ manager: SessionManager, retry request: Request, with error: Error, completion: @escaping RequestRetryCompletion) {
        
        /*if let response = request.task?.response as? HTTPURLResponse{
         if response.statusCode == 401 {
         completion(true, 1.0) // retry after 1 second
         } else if response.statusCode == -1005 {
         completion(true, 1.0) // retry after 1 second
         } else {
         completion(false, 0.0) // don't retry
         }
         }*/
        
        if error.localizedDescription.count > 0 && request.retryCount < 2{
            
            if error.localizedDescription == "The request timed out."{
                completion(false, 0.0) // don't retry
            }else{
                completion(true, 1.0)  // retry after 1 second
            }
            
        }else{
            completion(false, 0.0) // don't retry
        }
    }
}


//MARK:- extension for stripe payment method
extension WebServiceManager{
    public func requestAddCardOnStripe(strURL:String, params :[String : Any]?, success:@escaping(Dictionary<String,Any>) ->Void, failure:@escaping (Error) ->Void ) {
        
        
        let url = strURL
        print("url = \(url)")
        
        let headers = ["Authorization" :  stripeKey,"Content-Type":"application/x-www-form-urlencoded"]
        
        
        Alamofire.request(url, method: .post, parameters: params, headers: headers).responseJSON { responseObject in
            
            print(responseObject)
            if responseObject.result.isSuccess {
                do {
                    objActivity.startActivityIndicator()
                  //  SVProgressHUD.show(withStatus: "Please wait..")
                    let dictionary = try JSONSerialization.jsonObject(with: responseObject.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                    success(dictionary as! Dictionary<String, Any>)
                    // print(dictionary)
                    objActivity.stopActivity()
                }catch{
                   objActivity.stopActivity()
                    let error : Error = responseObject.result.error!
                    failure(error)
                }
            }
            if responseObject.result.isFailure {
                objActivity.stopActivity()
                let error : Error = responseObject.result.error!
                failure(error)
            }
        }
    }
    
    // Delete card
    public func requestDeleteCardFromStripe(strURL:String, params :[String : Any]?, success:@escaping(Dictionary<String,Any>) ->Void, failure:@escaping (Error) ->Void ) {
        
        
        let url = strURL
        print("url = \(url)")
        
        let headers = ["Authorization" :  stripeKey,"Content-Type":"application/x-www-form-urlencoded"]
        
        Alamofire.request(url, method: .delete, parameters: params, headers: headers).responseJSON { responseObject in
            
            print(responseObject)
            if responseObject.result.isSuccess {
                do {
                    //SVProgressHUD.show(withStatus: "Please wait..")
                    objActivity.startActivityIndicator()
                    let dictionary = try JSONSerialization.jsonObject(with: responseObject.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                    success(dictionary as! Dictionary<String, Any>)
                    // print(dictionary)
                    objActivity.stopActivity()
                }catch{
                   objActivity.stopActivity()
                    let error : Error = responseObject.result.error!
                    failure(error)
                }
            }
            if responseObject.result.isFailure {
                objActivity.stopActivity()
                let error : Error = responseObject.result.error!
                failure(error)
            }
        }
    }
    
    // Get all card List
    public func requestGetCardsFromStripe(strURL:String, params :[String : Any]?, success:@escaping(Dictionary<String,Any>) ->Void, failure:@escaping (Error) ->Void ) {
        
        
        let url = strURL
        print("url = \(url)")
        
        let headers = ["Authorization" : stripeKey,"Content-Type":"application/x-www-form-urlencoded"]
        
        Alamofire.request(url, method: .get, parameters: params, headers: headers).responseJSON { responseObject in
            
            // print(responseObject)
            if responseObject.result.isSuccess {
                do {
                    objActivity.startActivityIndicator()
                    let dictionary = try JSONSerialization.jsonObject(with: responseObject.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                    success(dictionary as! Dictionary<String, Any>)
                    // print(dictionary)
                    objActivity.stopActivity()
                }catch{
                    objActivity.stopActivity()
                    let error : Error = responseObject.result.error!
                    failure(error)
                }
            }
            if responseObject.result.isFailure {
                objActivity.stopActivity()
                let error : Error = responseObject.result.error!
                failure(error)
            }
        }
    }
}

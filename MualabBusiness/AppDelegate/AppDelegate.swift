//
//  AppDelegate.swift
//  MualabBusiness
//
//  Created by Mac on 21/12/2017 .
//  Copyright © 2017 Mindiii. All rights reserved.
//

import UIKit
import GooglePlacePicker
import GoogleMaps
import UserNotifications
import GooglePlaces
import Firebase
import FirebaseMessaging
import CoreData
import CoreLocation
import Stripe
import Fabric
import Crashlytics

var strNotificationType = ""
var forReview = false

let appDelegate = AppDelegate.sharedObject()
let notificationDelegate = SampleNotificationDelegate()

var lat = objAppShareData.objModelBusinessSetup.trackingLatitude
var long = objAppShareData.objModelBusinessSetup.trackingLongitude
var backgroundTask: UIBackgroundTaskIdentifier = .invalid

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate,MessagingDelegate,UNUserNotificationCenterDelegate ,CLLocationManagerDelegate{
    var locationManager = CLLocationManager()
    var window: UIWindow?
    var navController: UINavigationController?
    var updateFirstTime = false
    var ref: DatabaseReference!

    //MARK: - Shared object
    private static var sharedManager: AppDelegate = {
        let manager = UIApplication.shared.delegate as! AppDelegate
        return manager
    }()
    // MARK: - Accessors
    class func sharedObject() -> AppDelegate {
        return sharedManager
    }
    
    internal func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        updateFirstTime = true
        GMSPlacesClient.provideAPIKey("AIzaSyCJc8Wpt0C0qsQwrjGPudN4Ojjy3s8eUvI")
        GMSServices.provideAPIKey("AIzaSyCJc8Wpt0C0qsQwrjGPudN4Ojjy3s8eUvI")

      //  STPPaymentConfiguration.shared().publishableKey = "pk_live_LfS0DaPNp2CN5svYSdZKuKMF"
        Stripe.setDefaultPublishableKey(KUrlStripeId)

        //NotificationCenter.default.addObserver(self, selector: #selector(self.redirectionFromServiceTag), name: NSNotification.Name(rawValue: "redirectionFromServiceTag"), object: nil)
        
        objAppShareData.objAppdelegate.locationManager.delegate = self
        //objAppShareData.objAppdelegate.locationManager.requestWhenInUseAuthorization()
        objAppShareData.objAppdelegate.locationManager.requestAlwaysAuthorization()
        objAppShareData.objAppdelegate.locationManager.allowsBackgroundLocationUpdates = false
        if #available(iOS 11.0, *) {
            objAppShareData.objAppdelegate.locationManager.showsBackgroundLocationIndicator = false
        } else {
            // Fallback on earlier versions
        }
        objAppShareData.objAppdelegate.locationManager.startUpdatingLocation()
        
        //Remote notification
        Messaging.messaging().delegate = self
        
        registerForRemoteNotification()
        notificationDelegate.delegate = self
        NotificationCenter.default.addObserver(self, selector:
            #selector(tokenRefreshNotification), name: NSNotification.Name.InstanceIDTokenRefresh, object: nil)
        
        let isLoggedIn : Bool =  UserDefaults.standard.bool(forKey: UserDefaults.keys.isLoggedIn)
        let dict  =  UserDefaults.standard.value(forKey: UserDefaults.keys.userInfoDic) as? [String:Any] ?? ["":""]
        
        let isDocument  = dict[UserDefaults.keys.isDocument] as? String ?? ""
        if isLoggedIn {
            if isDocument == "3" {
                self.gotoTabBar(withAnitmation: false)
            }else{
                objAppShareData.editSelfServices = false
                self.showSignUpProcess()
            }
            //NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshChatBadgeCount"), object: nil)
        }
        // Override point for customization after application launch.
        
        ////
      //  if WebURL.BaseUrl.contains("dev"){
            objChatShareData.kServerKey = "AAAAohAm9co:APA91bHNZDmtwzyLIdcjl4P7Vw7uoq7YwtKiuRsF5Ld5DHkXJBUZmmT4_Wcj6wXeKonhIorQEbivo3nILXfescBWH01HCmoQiOKUhwQgyP1mKByn43xpFTfXKU74E43TWkePKjaRjE7hHtsBh-FgSHTWk43xMPzFew"
            let firebaseConfig = Bundle.main.path(forResource: "GoogleService-Info-Dev", ofType: "plist")
            guard let options = FirebaseOptions(contentsOfFile: firebaseConfig!) else {
                fatalError("Invalid Firebase configuration file.")
            }
            FirebaseApp.configure(options: options)
    //    }else{
//            objChatShareData.kServerKey = "AAAAK1vRFPE:APA91bFDJlGE-pK5f7JarrELoglCDCZl2Bnnm495IBiYjWXte8BInV8ZSdNT9fcW-xx96LQFIQAAGiwvMXYpK8ap6uJX6qfiPXfMCEwbGbfd7KMXtSSm9MLdfpD6AhdpbHbzSQbew5wF"
//            let firebaseConfig = Bundle.main.path(forResource: "GoogleService-Info", ofType: "plist")
//            guard let options = FirebaseOptions(contentsOfFile: firebaseConfig!) else {
//                fatalError("Invalid Firebase configuration file.")
//            }
//            FirebaseApp.configure(options: options)
  //      }
        ////
        //FirebaseApp.configure()
        ref = Database.database().reference()
        
        objAppShareData.objAppdelegate.locationManager.allowsBackgroundLocationUpdates = true
        badgeCount()
        
        //Fabric
        Fabric.sharedSDK().debug = true
        Fabric.with([Crashlytics.self])
        //
        return true
    }
    
    @objc func redirectionFromServiceTag(_ objNotify: Notification){
        print(objNotify)
        print(objNotify.userInfo as! [String:Any])
        print("redirectionFromServiceTag")
        let dictFinal = objNotify.userInfo as! [String:Any]
        if  dictFinal["tabType"] as? String == "service"{
            let sb: UIStoryboard = UIStoryboard(name: "ArtistProfile", bundle: Bundle.main)
            if let objVC = sb.instantiateViewController(withIdentifier:"ServiceDetailVC") as? ServiceDetailVC{
                objVC.hidesBottomBarWhenPushed = true
                objVC.param = dictFinal
                self.navController?.pushViewController(objVC, animated: true)
            }
        }
    }
    
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return true
    }
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        
        print("Continue User Activity called: ")
        
        if userActivity.activityType == NSUserActivityTypeBrowsingWeb {
            if let url = userActivity.webpageURL {
                
                print(url.absoluteString)
                if let dict = url.queryParameters{
                    
                    print("qParams = \(dict)")
                    
                    let decodedData = Data(base64Encoded:dict["share"]!)
                    
                    let decodeUrl = String(data: decodedData!, encoding: .utf8)!
                    let paramsArray = decodeUrl.components(separatedBy: "/")
                    objAppShareData.isFromNotification =  false
                    objAppShareData.notificationType = "feed"
                    if paramsArray.count > 0{
                        objAppShareData.notificationInfoDict = ["notifyId":paramsArray.last ?? ""]
                    }
                    self.gotoTabBar(withAnitmation: false)
                    
                }
            }
        }
        return true
    }
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        if #available(iOS 11.0, *) {
            objAppShareData.objAppdelegate.locationManager.showsBackgroundLocationIndicator = false
        } else {
            // Fallback on earlier versions
        }
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
         UIApplication.shared.applicationIconBadgeNumber = 0
        let isLoggedIn : Bool =  UserDefaults.standard.bool(forKey: UserDefaults.keys.isLoggedIn)
        if isLoggedIn {
           self.manageOnlineStatus(isOnlineValue: 0)
            /////
                var strMyId = ""
                if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.businessInfoDic){
                    if let userId = dict["_id"] as? Int {
                        strMyId = "\(userId)"
                        self.ref.child("socialBookingBadgeCount").child(strMyId).updateChildValues(["totalCount":"0"])
                    }else if let userId = dict["_id"] as? String {
                        strMyId = userId
                        self.ref.child("socialBookingBadgeCount").child(strMyId).updateChildValues(["totalCount":"0"])
                    }
                }else{
                    let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as! Data
                    let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
                    if let id = userInfo["_id"] as? Int {
                        strMyId = String(id)
                        self.ref.child("socialBookingBadgeCount").child(strMyId).updateChildValues(["totalCount":"0"])
                    }else if let userId = userInfo["_id"] as? String {
                        strMyId = userId
                        self.ref.child("socialBookingBadgeCount").child(strMyId).updateChildValues(["totalCount":"0"])
                    }
                }
            ////
                UIApplication.shared.applicationIconBadgeNumber = 0
        }
        ////
        //let userInfo = ["xPosition":"0"] as [String:Any]
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "fromBackgroundHideSlideMenu"), object: nil, userInfo: nil)
        ////
        
        
        var strMyId = ""
        if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.businessInfoDic){
            if let userId = dict["_id"] as? String {
                strMyId = userId
                if strMyId != ""{
                    self.ref.child("businessBookingBadgeCount").child(strMyId).updateChildValues(["totalCount":"0"])
                }
            }
        }
        
        
        
        if #available(iOS 11.0, *) {
            objAppShareData.objAppdelegate.locationManager.showsBackgroundLocationIndicator = false
        } else {
            // Fallback on earlier versions
        }
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        if objAppShareData.isFileIsUploding{
            self.registerBackgroundTask()
        }
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        NotificationCenter.default.post(name: NSNotification.Name("hideTabbarFromChatHistory"), object: nil)
        var strMyId = ""
        UIApplication.shared.applicationIconBadgeNumber = 0
        let isLoggedIn : Bool = UserDefaults.standard.bool(forKey: UserDefaults.keys.isLoggedIn)
        if isLoggedIn {
           self.manageOnlineStatus(isOnlineValue: 1)
           
            /////
                var strMyId = ""
                if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.businessInfoDic){
                    if let userId = dict["_id"] as? Int {
                        strMyId = "\(userId)"
                        self.ref.child("socialBookingBadgeCount").child(strMyId).updateChildValues(["totalCount":"0"])
                    }else if let userId = dict["_id"] as? String {
                        strMyId = userId
                        self.ref.child("socialBookingBadgeCount").child(strMyId).updateChildValues(["totalCount":"0"])
                    }
                }else{
                    let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as! Data
                    let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
                    if let id = userInfo["_id"] as? Int {
                        strMyId = String(id)
                        self.ref.child("socialBookingBadgeCount").child(strMyId).updateChildValues(["totalCount":"0"])
                    }else if let userId = userInfo["_id"] as? String {
                        strMyId = userId
                        self.ref.child("socialBookingBadgeCount").child(strMyId).updateChildValues(["totalCount":"0"])
                    }
                }
            ////
                UIApplication.shared.applicationIconBadgeNumber = 0
        }
        if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.businessInfoDic){
            if let userId = dict["_id"] as? String {
                strMyId = userId
                if strMyId != ""{
                    self.ref.child("businessBookingBadgeCount").child(strMyId).updateChildValues(["totalCount":"0"])
                }
            }
        }
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    
    
    
    

  
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        self.saveContext()
    }
    
    
    ////MARK: -  Remote notification Delegates method all method cover
    //extension AppDelegate : UNUserNotificationCenterDelegate{
    @objc func tokenRefreshNotification(_ notification: Notification) {
        if let refreshedToken = InstanceID.instanceID().token() {
            print("InstanceID token: \(refreshedToken)")
            objAppShareData.firebaseToken = refreshedToken
        }
    }
    
    func application(received remoteMessage: MessagingRemoteMessage) {
        print("%@", remoteMessage.appData)
    }
    
    // - FCM Token
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        print("FCM registration token: \(fcmToken)")
        objAppShareData.firebaseToken = fcmToken
        
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
       
        let d = NSData(data: deviceToken)
        let characterSet = CharacterSet(charactersIn: "<>")
        let deviceTokenString = d.description.trimmingCharacters(in: characterSet).replacingOccurrences(of: " ", with: "");
        print(deviceTokenString)
        objAppShareData.strDeviceToken = deviceTokenString
        
        if objAppShareData.strDeviceToken == ""{
            objAppShareData.strDeviceToken = ""
        }
        
        if let refreshedToken = InstanceID.instanceID().token() {
            print("InstanceID token: \(refreshedToken)")
            objAppShareData.firebaseToken = refreshedToken
        }
        
        Messaging.messaging().apnsToken = deviceToken
        print(objAppShareData.strDeviceToken)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        print( "userInfo = \(userInfo.values)")
        handleNotificationWith(userInfo: userInfo as! [String : Any])
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // handleNotificationWith(userInfo: userInfo)
    }
    
    //called When app in the foreground
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo as! [String : Any]
        print(userInfo["aps"] ?? "")
        
        if UIApplication.shared.applicationState == .active {
            let NotificationType = userInfo["type"] as? String ?? ""
            if NotificationType == "chat"{
                let strOpponentId = userInfo["uid"] as? String ?? ""
                //if objChatVC.strOpponentId == strOpponentId {
                if "0" == strOpponentId {
                }else{
                    completionHandler(.alert)
                }
            }else{
                //handleNotificationWith(userInfo: userInfo)
                completionHandler(.alert)
            }
        }else{
            handleNotificationWith(userInfo: userInfo)
            completionHandler(.alert)
        }
    }
    
    //called When you tap on the notification
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo as! [String : Any]
        print(userInfo["aps"] ?? "")
        handleNotificationWith(userInfo: userInfo)
    }
    
    // Register for Remote notification
    func registerForRemoteNotification() {
        
        /////////////
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
        }
        
        UIApplication.shared.registerForRemoteNotifications()
        Messaging.messaging().delegate = self
        
    }
    
    // - Parse Notification
    func handleNotificationWith(userInfo:[AnyHashable : Any]){
        print(userInfo)
        self.notificationClickAction(userInfo: userInfo)
    }
    
    lazy var persistentContainer: NSPersistentContainer = {
        
        let container = NSPersistentContainer(name: "MualabBusiness")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}

//MARK: -  delegateSampleNotification
extension AppDelegate : delegateSampleNotification {
    
    func handleNotificationWithNotificationData(userInfo: [String : Any]) {
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to:nil, from:nil, for:nil)
        
    }
    func notificationClickAction(userInfo:[AnyHashable : Any]){
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to:nil, from:nil, for:nil)
        let notifyType = userInfo["type"] as? String ?? ""
        
        objAppShareData.selectedTab = 0
        objAppShareData.notificationType = ""
        objAppShareData.isFromNotification =  false
        objAppShareData.notificationInfoDict = nil
        
        let isLoggedIn : Bool =  UserDefaults.standard.bool(forKey: UserDefaults.keys.isLoggedIn)
        guard isLoggedIn else { return }
        print("\n\n>> AppDelegate userInfo = \(userInfo)\n\n")
        
        var notifincationType : Int?
        if let notiType = userInfo["notifincationType"] as? Int{
            notifincationType = notiType
        }else{
            if let notiType = userInfo["notifincationType"] as? String{
                notifincationType = Int(notiType)
            }
        }
        
        guard let notiType = notifincationType else {
            return
        }
        
        switch notiType {
        case 1:
            //var body = 'sent a booking request.';
            //var title = 'Booking request';
            objAppShareData.notificationType = "booking" //Artist side
            break;
            
        case 2:
            //var body = 'accepted your booking request.'
            //var title = 'Booking accept';
            objAppShareData.notificationType = "booking" //->Checked
            break;
            
        case 3:
            //var body = 'rejected your booking request.';
            //var title = 'Booking reject';
            objAppShareData.notificationType = "booking" //->Checked
            break;
            
        case 4:
            //var body = 'cancelled your booking request.';
            //var title = 'Booking cancel';
            objAppShareData.notificationType = "booking" //->Checked
            break;
            
        case 5:
            //var body = 'completed your booking request.';
            //var title = 'Booking complete';
            objAppShareData.notificationType = "booking"
            break;
            
        case 6:
            //var body = 'given review for booking.';
            //var title = 'Booking Review';
            forReview = true
            objAppShareData.notificationType = "booking"
            break;
            
        case 7:
            //var body = 'added a new post.';
            //var title = 'new post';
            objAppShareData.notificationType = "feed" //->Checked
            break;
            
        case 8:
            //var body = 'Payment has completed by';
            //var title = 'Payment';
            objAppShareData.notificationType = "booking" //Artist side //on back open payment history
            break;
            
        case 9:
            //var body = 'commented on your post.';
            //var title = 'Comment';
            objAppShareData.notificationType = "feed" //->Checked
            break;
            
        case 10:
            //var body = 'likes your post.';
            //var title = 'Post like';
            objAppShareData.notificationType = "feed" //->Checked
            break;
            
        case 11:
            //var body = 'likes your comment.';
            //var title = 'Comment like';
            objAppShareData.notificationType = "feed" //->Checked
            break;
            
        case 12:
            //var body = 'started following you.';
            //var title = 'Following';
            objAppShareData.notificationType = "profile" //->Checked
            break;
            
        case 13:
            //var body = 'added to their story.';
            //var title = 'Story';
            objAppShareData.notificationType = "story" // open story //->Checked
            break;
            
        case 14:
            //var body = 'added you as a favorite.';
            //var title = 'Story';
            objAppShareData.notificationType = "profile" //Artist side
            break;
            
        case 15:
            //var body = 'chat.';
            //var title = 'chat';
            objAppShareData.notificationType = "chat"
            break;
            
        case 16:
            //var body = 'tagged you in a post.';
            //var title = 'Tagged';
            objAppShareData.notificationType = "feed" //->Checked
            break;
            
        case 17:
            //var body = 'tagged you in a post.';
            //var title = 'Tagged';
            objAppShareData.notificationType = "invitation" //->Checked
            break;
            
        case 18:
            //var body = 'tagged you in a post.';
            //var title = 'Tagged';
            objAppShareData.notificationType = "invitationAccept" //->Checked
            break;
            
        case 23:
            //var body = 'tagged you in a post.';
            //var title = 'Tagged';
            objAppShareData.notificationType = "certificate" //->Checked
            break;
            
        case 24:
            //var body = 'tagged you in a post.';
            //var title = 'Tagged';
            objAppShareData.notificationType = "holiday" //->Checked
            break;
            
        case 25:
            //var body = 'tagged you in a post.';
            //var title = 'Tagged';
            objAppShareData.notificationType = "booking" //->Checked
            break;
            
        case 26:
            //var body = 'tagged you in a post.';
            //var title = 'Tagged';
            objAppShareData.notificationType = "booking" //->Checked
            break;
        case 30:
            //var body = 'tagged you in a post.';
            //var title = 'Tagged';
            objAppShareData.notificationType = "adminCommission" //->Checked
            break;
        default:
            print("default : No condition matched")
        }
        
        //notificationType : "booking","feed","story","profile","chat"
        
        if objAppShareData.notificationType != "" {
            objAppShareData.isFromNotification =  true
            objAppShareData.notificationInfoDict = userInfo as? [String : Any] ?? ["":""]
            self.gotoTabBar(withAnitmation: false)
        }
        
        /*
         AppDelegate userInfo = [ "aps": {
         alert ={
         body = "Neha commented on your post.";
         title = Comment;
         };
         "content-available" = 1;
         "mutable-content" = 1;
         sound = default;
         },
         "notifyId": 17,
         "gcm.notification.notifincationType": 9,
         "google.c.a.e": 1,
         "title": Comment,
         "gcm.message_id": 0:1533113029672857%c6fca462c6fca462,
         "body": Neha commented on your post.,
         "notifincationType": 9,
         "urlImageString": http://koobi.co.uk:3000/uploads/profile/1532757198120.jpg,
         "gcm.notification.notifyId": 17]
         
         
         >> AppDelegate userInfo = ["userType": user, "notifyId": 1, "body": Pankaj added to their story., "google.c.a.e": 1, "gcm.notification.userType": user, "urlImageString": http://koobi.co.uk:3000/uploads/profile/1532755154540.jpg, "notifincationType": 13, "title": Story, "gcm.message_id": 0:1533292269949856%c6fca462c6fca462, "aps": {
         alert =     {
         body = "Pankaj added to their story.";
         title = Story;
         };
         "content-available" = 1;
         sound = default;
         }, "gcm.notification.notifyId": 1, "gcm.notification.notifincationType": 13]
         */
    } 
}
//MARK: - Custom Methods
extension AppDelegate{
    func logout(){
        UserDefaults.standard.set(false, forKey: UserDefaults.keys.isLoggedIn)
        UserDefaults.standard.set("", forKey: UserDefaults.keys.isDocument)
        UserDefaults.standard.set(["":""], forKey: UserDefaults.keys.userInfoDic)
        objAppShareData.onTracking  = false
        objAppShareData.onTrackView = false
        
        self.clearOnLogout()
        UserDefaults.standard.set(["":""], forKey: UserDefaults.keys.businessInfoDic)
        UserDefaults.standard.synchronize()
        objAppShareData.objBookingCalendarVC.fromStaffSelectId = ""
        objAppShareData.objBookingCalendarVC.fromStaffSelectName = ""
        objAppShareData.selectedTab = 0
        self.gotoLoginPage()
    }
    
    func gotoLoginPage(){
        
        // *** Create Main Navigation *** //
        let sb = UIStoryboard(name: "Main", bundle: Bundle.main)
        navController = sb.instantiateViewController(withIdentifier: "LoginNavigationController") as? UINavigationController
//        let transition = CATransition()
//        transition.duration = 1.0
//        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
//        transition.type = CATransitionType(rawValue: "cube")
//        transition.subtype = CATransitionSubtype.fromLeft
//        transition.delegate = self as? CAAnimationDelegate
        appDelegate.window?.rootViewController = navController
        //appDelegate.window?.layer.add(transition, forKey: nil)
        appDelegate.window?.makeKeyAndVisible()
    }
    
    func showSignUpProcess() {
        // *** Create Main Navigation *** //
        let sb: UIStoryboard = UIStoryboard(name: "SignUpProcess", bundle: Bundle.main)
        navController = sb.instantiateViewController(withIdentifier: "SignUpProcessNav") as? UINavigationController
//        let transition = CATransition()
//        transition.duration = 0.5
//        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
//        transition.type = CATransitionType(rawValue: "cube")
//        transition.subtype = CATransitionSubtype.fromRight
//        transition.delegate = self as? CAAnimationDelegate
        //let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = navController
        //appDelegate.window?.layer.add(transition, forKey: nil)
        window?.makeKeyAndVisible()
    }
    
    func showBooking() {
        // *** Create Main Navigation *** //
        let sb: UIStoryboard = UIStoryboard(name: "Booking", bundle: Bundle.main)
        navController = sb.instantiateViewController(withIdentifier: "BookingNav") as? UINavigationController
        //let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = navController
        window?.makeKeyAndVisible()
    }
    
    func gotoTabBar(withAnitmation: Bool) {
        // *** Create Main Navigation *** //
        let sb: UIStoryboard = UIStoryboard(name: "TabBar", bundle: Bundle.main)
        navController = sb.instantiateViewController(withIdentifier: "TabBarNav") as? UINavigationController
        
        if withAnitmation {
//            let transition = CATransition()
//            transition.duration = 1.0
//            transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
//            transition.type = CATransitionType(rawValue: "cube")
//            transition.subtype = CATransitionSubtype.fromRight
//            transition.delegate = self as? CAAnimationDelegate
//            appDelegate.window?.layer.add(transition, forKey: nil)
        }
        
        appDelegate.window?.rootViewController = navController
        window?.makeKeyAndVisible()
    }
    
    func clearData(){
        objAppShareData.isSearchingUsingFilter = false
        objAppShareData.dictFilter = [String : Any]()
        objAppShareData.arrSelectedService = [BookingServices]()
        objAppShareData.isFromEditBooking = false
        objAppShareData.isFromConfirmBooking = false
    }
    
    //Rohit New Work 7/Feb/2020
    //MARK:- Part 1 Save User Basic Info
    func saveUserInfoDetail(dictOfInfo:[String:Any]){
        var userID = String()
        if let ids = dictOfInfo["user_id"] as? String{
            userID = ids
        }else if let ids = dictOfInfo["user_id"] as? Int{
            userID = String(ids)
        }
        
        var userName = String()
        if let usrName = dictOfInfo["user_name"] as? String{
            userName = usrName
        }
        var firstName = String()
        if let first_name = dictOfInfo["first_name"] as? String{
            firstName = first_name
        }
        var lastName = String()
        if let last_name = dictOfInfo["last_name"] as? String{
            lastName = last_name
        }
        
        var email = String()
        if let email_id = dictOfInfo["email"] as? String{
            email = email_id
        }
        
        var countryCode = String()
        if let country_code = dictOfInfo["country_code"] as? String{
            countryCode = country_code
        }
        
        var contactNo = String()
        if let contact_no = dictOfInfo["contact_no"] as? String{
            contactNo = contact_no
        }
        
        var gender = String()
        if let gender_business = dictOfInfo["gender"] as? String{
            gender = gender_business
        }
        
        var dob = String()
        if let date_of_birth = dictOfInfo["dob"] as? String{
            dob = date_of_birth
        }
        
        var firebaseToken = String()
        if let firebase_Token = dictOfInfo["firebase_token"] as? String{
            firebaseToken = firebase_Token
        }
        
        var profileImage = String()
        if let profileImageUrl = dictOfInfo["profile_image"] as? String{
            profileImage = profileImageUrl
        }
        
        var businessID = String()
        if let business_id = dictOfInfo["business_id"] as? String{
            businessID = business_id
        }else if let business_id = dictOfInfo["business_id"] as? Int{
            businessID = String(business_id)
        }
        
        var staffID = String()
        if let staff_id = dictOfInfo["staff_id"] as? String{
            staffID = staff_id
        }else if let staff_id = dictOfInfo["staff_id"] as? Int{
            staffID = String(staff_id)
        }
        
        var businessName = String()
        if let business_name = dictOfInfo["business_name"] as? String{
            businessName = business_name
        }
        
        var city = String()
        if let biz_city = dictOfInfo["city"] as? String{
            city = biz_city
        }
        
        var state = String()
        if let biz_state = dictOfInfo["state"] as? String{
            state = biz_state
        }
        
        var country = String()
        if let biz_country = dictOfInfo["country"] as? String{
            country = biz_country
        }
        
        var address = String()
        if let user_address = dictOfInfo["address"] as? String{
            address = user_address
        }
        
        var latitude = String()
        if let biz_latitude = dictOfInfo["latitude"] as? String{
            latitude = biz_latitude
        }else if let biz_latitude = dictOfInfo["latitude"] as? Int{
            latitude = "\(biz_latitude)"
        }
        
        var longitude = String()
        if let biz_longitude = dictOfInfo["longitude"] as? String{
            longitude = biz_longitude
        }else if let biz_longitude = dictOfInfo["longitude"] as? Int{
            longitude = "\(biz_longitude)"
        }
        
        var trackingLatitude = String()
        if let biz_trackingLatitude = dictOfInfo["tracking_latitude"] as? String{
            trackingLatitude = biz_trackingLatitude
        }
        
        var trackingLongitude = String()
        if let biz_trackingLongitude = dictOfInfo["tracking_longitude"] as? String{
            trackingLongitude = biz_trackingLongitude
        }
        
        var isDocument = String()
        if let is_doc = dictOfInfo["is_document"] as? String{
            isDocument = is_doc
        }
        
        var walkingBlock = String()
        if let walking_block = dictOfInfo["walking_block"] as? String{
            walkingBlock = walking_block
        }
        
        /*
                        "user_type": 1,
                        "login_type": 1,
                        "device_type": 1,
                        "followers_count": "0",
                        "following_count": "0",
                        "service_count": "0",
                        "certificate_count": "0",
                        "post_count": "0",
                        "review_count": "0",
                        "rating_count": "0",
                        "status": 0
         */
        
        
        
        let dicUserInfo = ["user_id":userID,
        "user_name":userName,
        "first_name":firstName,
        "last_name":lastName,
        "firebase_token":firebaseToken,
        "dob":dob,
        "gender":gender,
        "contact_no":contactNo,
        "country_code":countryCode,
        "email":email,
        "profile_image":profileImage,
        "business_id":businessID,
        "staff_id":staffID,
        "business_name":businessName,
        "city":city,
        "state":state,
        "country":country,
        "address":address,
        "latitude":latitude,
        "longitude":longitude,
        "tracking_latitude":trackingLatitude,
        "tracking_longitude":trackingLongitude,
        "is_document":isDocument,
        "walking_block":walkingBlock,
        ]
        
        UserDefaults.standard.setValue(dicUserInfo, forKey: UserDefaults.keys.userInfoDic)

    }
    
    
     //MARK:- Part 2 Save User Business Info
    func SaveBusinessDetails(dictOfBusinessInfo:[String:Any]){
        
        var userDict = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfoDic) as? [String:Any] ?? ["":""]
        
        if let businessName = dictOfBusinessInfo["business_name"]as? String{
            userDict["\(UserDefaults.keys.businessName)"] = businessName
        }
        
        if let businessType = dictOfBusinessInfo["business_type"]as? String{
            userDict["\(UserDefaults.keys.businessType)"] = businessType
        }
        
        if let business_email = dictOfBusinessInfo["business_email"]as? String{
            userDict["\(UserDefaults.keys.businessEmail)"] = business_email
        }
        
        if let businessContactNo = dictOfBusinessInfo["business_contact_no"]as? String{
            userDict["\(UserDefaults.keys.businessContactNo)"] = businessContactNo
        }
        
        if let businessInCallPreTime = dictOfBusinessInfo["in_call_prepration_time"]as? String{
            userDict["\(UserDefaults.keys.inCallpreprationTime)"] = businessInCallPreTime
        }
        
        if let businessOutCallPreTime = dictOfBusinessInfo["out_call_prepration_time"]as? String{
            userDict["\(UserDefaults.keys.outCallPreparationTime)"] = businessOutCallPreTime
        }
        
        if let bookingSetting = dictOfBusinessInfo["booking_setting"]as? String{
            userDict["\(UserDefaults.keys.bookingSetting)"] = bookingSetting
        }
        
        if let payOption = dictOfBusinessInfo["pay_option"]as? String{
            userDict["\(UserDefaults.keys.payOption)"] = payOption
        }
        
        if let serviceType = dictOfBusinessInfo["service_type"]as? String{
            userDict["\(UserDefaults.keys.serviceType)"] = serviceType
        }
        
        if let radius = dictOfBusinessInfo["radius"]as? String{
            userDict["\(UserDefaults.keys.radius)"] = radius
        }else if let radius = dictOfBusinessInfo["radius"]as? Int{
            userDict["\(UserDefaults.keys.radius)"] = "\(radius)"
        }
        
        
        UserDefaults.standard.setValue(userDict, forKey: UserDefaults.keys.userInfoDic)
        
        
        /*
         user_id:24
         business_id:7
         staff_id:7
       */
        
    }
    
    //MARK:- Part 3 Save User Payment Info
    
    
    
    
    func parseBusinessSignUpDetail(dictOfInfo:[String:Any]){
        
        var  v = ""
        if let id = dictOfInfo["__v"] as? String{
            v = id
        }else if let id = dictOfInfo["__v"] as? Int{
            v = String(id)
        }
//        var id1 = ""
//               if let ids = dictOfInfo["_id"] as? String{
//                   id1 = ids
//               }else if let ids = dictOfInfo["_id"] as? Int{
//                   id1 = String(ids)
//               }else if let ids = dictOfInfo["_Id"] as? String{
//                   id1 = String(ids)
//               }else if let ids = dictOfInfo["_Id"] as? Int{
//                   id1 = String(ids)
//               }
        
        var id1 = ""
        if let ids = dictOfInfo["user_id"] as? String{
            id1 = ids
        }else if let ids = dictOfInfo["user_id"] as? Int{
            id1 = String(ids)
        }else if let ids = dictOfInfo["user_id"] as? String{
            id1 = String(ids)
        }else if let ids = dictOfInfo["user_id"] as? Int{
            id1 = String(ids)
        }
        
        var business_id = ""
        if let ids = dictOfInfo["business_id"] as? String{
            business_id = ids
        }else if let ids = dictOfInfo["business_id"] as? Int{
            business_id = String(ids)
        }
        
        var staff_id = ""
        if let ids = dictOfInfo["staff_id"] as? String{
            staff_id = ids
        }else if let ids = dictOfInfo["staff_id"] as? Int{
            staff_id = String(ids)
        }
        
     
        
        var bankStatus = ""
        if let id = dictOfInfo["bank_status"] as? String{
            bankStatus = id
        }else if let id = dictOfInfo["bank_status"] as? Int{
            bankStatus = String(id)
        }
        
        var certificateCount = ""
        if let id = dictOfInfo["certificateCount"] as? String{
            certificateCount = id
        }else if let id = dictOfInfo["certificateCount"] as? Int{
            certificateCount = String(id)
        }
        
        var chatId = ""
        if let id = dictOfInfo["chatId"] as? String{
            chatId = id
        }else if let id = dictOfInfo["chatId"] as? Int{
            chatId = String(id)
        }
        
        var deviceType = ""
        if let id = dictOfInfo["device_type"] as? String{
            deviceType = id
        }else if let id = dictOfInfo["device_type"] as? Int{
            deviceType = String(id)
        }
        
        var followersCount = ""
        if let id = dictOfInfo["followersCount"] as? String{
            followersCount = id
        }else if let id = dictOfInfo["followersCount"] as? Int{
            followersCount = String(id)
        }
        
        var followingCount = ""
        if let id = dictOfInfo["followingCount"] as? String{
            followingCount = id
        }else if let id = dictOfInfo["followingCount"] as? Int{
            followingCount = String(id)
        }
        
        var isDocument = ""
        if let id = dictOfInfo["is_document"] as? String{
            isDocument = id
        }else if let id = dictOfInfo["is_document"] as? Int{
            isDocument = String(id)
        }
        
        var  mailVerified = ""
        if let id = dictOfInfo["mailVerified"] as? String{
            mailVerified = id
        }else if let id = dictOfInfo["mailVerified"] as? Int{
            mailVerified = String(id)
        }
        
        var otpVerified = ""
        if let id = dictOfInfo["otpVerified"] as? String{
            otpVerified = id
        }else if let id = dictOfInfo["otpVerified"] as? Int{
            otpVerified = String(id)
        }
        
        var postCount = ""
        if let id = dictOfInfo["postCount"] as? String{
            postCount = id
        }else if let id = dictOfInfo["postCount"] as? Int{
            postCount = String(id)
        }
        
        var serviceCount = ""
        if let id = dictOfInfo["service_count"] as? String{
            serviceCount = id
        }else if let id = dictOfInfo["service_count"] as? Int{
            serviceCount = String(id)
        }
        
        var bookingSetting = ""
        if let id = dictOfInfo["booking_setting"] as? String{
            bookingSetting = id
        }else if let id = dictOfInfo["booking_setting"] as? Int{
            bookingSetting = String(id)
        }
        
        var reviewCount = ""
        if let id = dictOfInfo["review_count"] as? String{
            reviewCount = id
        }else if let id = dictOfInfo["review_count"] as? Int{
            reviewCount = String(id)
        }
        
        var ratingCount = ""
        if let id = dictOfInfo["rating_count"] as? String{
            ratingCount = id
        }else if let id = dictOfInfo["rating_count"] as? Int{
            ratingCount = String(id)
        }
        
        var radius = "0"
        if let id = dictOfInfo["radius"] as? String{
            radius = id
        }else if let id = dictOfInfo["radius"] as? Int{
            radius = String(id)
        }
        
        var serviceType = ""
        if let id = dictOfInfo["service_type"] as? String{
            serviceType = id
        }else if let id = dictOfInfo["serviceType"] as? Int{
            serviceType = String(id)
        }
        var businessHours = ""
        if let id = dictOfInfo["business_hours"] as? String{
            businessHours = id
        }else if let id = dictOfInfo["business_hours"] as? Int{
            businessHours = String(id)
        }
        var status = ""
        if let id = dictOfInfo["status"] as? String{
            status = id
        }else if let id = dictOfInfo["status"] as? Int{
            status = String(id)
        }
        
        var payOption = ""
        if let id = dictOfInfo["payOption"] as? String{
            payOption = id
        }else if let id = dictOfInfo["payOption"] as? Int{
            payOption = String(id)
        }

       
        
        
        var socialId = ""
        if let id = dictOfInfo["social_id"] as? String{
            socialId = id
        }else if let id = dictOfInfo["social_id"] as? Int{
            socialId = String(id)
        }
        
        var OTP = ""
        OTP = dictOfInfo["OTP"] as? String ?? ""
        
        var address = ""
        address = dictOfInfo["address"] as? String ?? ""
        
        var address2 = ""
        address2 = dictOfInfo["address2"] as? String ?? ""
        if address2.count == 0{
           address2 = address
        }
        
        var appType = ""
        appType = dictOfInfo["app_type"] as? String ?? ""
        
        var authToken = ""
        authToken = dictOfInfo["auth_token"] as? String ?? ""
        
        var bio = ""
        bio = dictOfInfo["bio"] as? String ?? ""
        
        var buildingNumber = ""
        buildingNumber = dictOfInfo["building_number"] as? String ?? ""
        
        var businessName = "";
        businessName = dictOfInfo["business_name"] as? String ?? ""
        
        var businessType = ""
        businessType = dictOfInfo["business_type"] as? String ?? ""
        
        var businesspostalCode = ""
        businesspostalCode = dictOfInfo["businesspostalCode"] as? String ?? ""
        
        var city = ""
        city = dictOfInfo["city"] as? String ?? ""
        
        var contactNo = ""
        contactNo = dictOfInfo["contact_no"] as? String ?? ""
        
        var country = ""
        country = dictOfInfo["country"] as? String ?? ""
        
        var countryCode = ""
        countryCode = dictOfInfo["country_code"] as? String ?? ""
        
        var crd = ""
        crd = dictOfInfo["crd"] as? String ?? ""
        
        var deviceToken = ""
        deviceToken = dictOfInfo["device_token"] as? String ?? ""
        
        var dob = ""
        dob = dictOfInfo["dob"] as? String ?? ""
        
        var email = ""
        email = dictOfInfo["email"] as? String ?? ""
        
        var firebaseToken = ""
        firebaseToken = dictOfInfo["firebase_token"] as? String ?? ""
        
        var firstName = ""
        firstName = dictOfInfo["first_name"] as? String ?? ""
        
        var gender = ""
        gender = dictOfInfo["gender"] as? String ?? ""
        
        var inCallpreprationTime = "00:00"
        inCallpreprationTime = dictOfInfo["inCallpreprationTime"] as? String ?? "00:00"
        
        
        var socialType = ""
        socialType = dictOfInfo["social_type"] as? String ?? ""
        
        var state = ""
        state = dictOfInfo["state"] as? String ?? ""
        
        var upd = ""
        upd = dictOfInfo["upd"] as? String ?? ""
        
        var userName = ""
        userName = dictOfInfo["user_name"] as? String ?? ""
        
        var userType = ""
        userType = dictOfInfo["user_type"] as? String ?? ""
        
        var profileImage = ""
        profileImage = dictOfInfo["profile_image"] as? String ?? ""
        
        var password = ""
        password = dictOfInfo["password"] as? String ?? ""
        
        var outCallpreprationTime = "00:00"
        outCallpreprationTime = dictOfInfo["outCallpreprationTime"] as? String ?? "00:00"
        
        var longitude = ""
        longitude = dictOfInfo["longitude"] as? String ?? ""
        
        var latitude = ""
        latitude = dictOfInfo["latitude"] as? String ?? ""
        
        var trackingLatitude = ""
        trackingLatitude = dictOfInfo["trackingLatitude"] as? String ?? ""
        
        var trackingLongitude = ""
        trackingLongitude = dictOfInfo["trackingLongitude"] as? String ?? ""
        
        
        var lastName = ""
        lastName = dictOfInfo["last_name"] as? String ?? ""
        
        var businessEmail = ""
        businessEmail = dictOfInfo["businessEmail"] as? String ?? ""
        
        var businessContactNo = ""
        businessContactNo = dictOfInfo["businessContactNo"] as? String ?? ""
        
        
        
        var location = ""
        location = dictOfInfo["location"] as? String ?? ""
        
        var incallTimeHours = "00"
        var incallTimeMinute = "00"
        
        if inCallpreprationTime == ""{
            inCallpreprationTime = "00:00"
        }
        let a = inCallpreprationTime.components(separatedBy: ":")
        incallTimeHours = "\(a[0])"
        incallTimeMinute =  "\(a[1])"
        
        var outcallTimeHours = "00"
        var outcallTimeMinute = "00"
        
        if outCallpreprationTime == ""{
            outCallpreprationTime = "00:00"
        }
        let b = outCallpreprationTime.components(separatedBy: ":")
        outcallTimeHours =  "\(b[0])"
        outcallTimeMinute = "\(b[1])"
        
        UserDefaults.standard.setValue(businessHours, forKey: UserDefaults.keys.businessHourCount)
        
        if let arrWorkingHours = dictOfInfo["business_hour"] as? [[String:Any]]{
            print("arrWorkingHours count = ",arrWorkingHours.count)
            UserDefaults.standard.setValue(String(arrWorkingHours.count), forKey: UserDefaults.keys.businessHourCount)
        }
        
        UserDefaults.standard.setValue(serviceType, forKey: UserDefaults.keys.mainServiceType)
        
        //
        //        let arrDOB = dob.components(separatedBy: "-")
        //        if arrDOB.count > 2{
        //           dob = arrDOB
        //        }
        //      let a UserDefaults.standard.setValue(param, forKey: UserDefaults.keys.businessInfoDic)
        UserDefaults.standard.set(dictOfInfo["customerId"], forKey: UserDefaults.keys.stripeCustomerId)

        objAppShareData.objModelBusinessSetup.serviceType = serviceType
        objAppShareData.objModelBusinessSetup.businessContactNo = businessContactNo
        objAppShareData.objModelBusinessSetup.inCallpreprationTime = inCallpreprationTime
        objAppShareData.objModelBusinessSetup.outCallpreprationTime = outCallpreprationTime
        objAppShareData.objModelBusinessSetup.businessEmail = businessEmail
        objAppShareData.objModelBusinessSetup.businessName = businessName
        objAppShareData.objModelBusinessSetup.location = location
        objAppShareData.objModelBusinessSetup.address2 = address2
        objAppShareData.objModelBusinessSetup.country = country
        objAppShareData.objModelBusinessSetup.state = state
        objAppShareData.objModelBusinessSetup.city = city
        objAppShareData.objModelBusinessSetup.radius = radius
        objAppShareData.objModelBusinessSetup.address = address
        objAppShareData.objModelBusinessSetup.latitude = latitude
        objAppShareData.objModelBusinessSetup.longitude = longitude
        objAppShareData.objModelBusinessSetup.trackingLatitude = trackingLatitude
        objAppShareData.objModelBusinessSetup.trackingLongitude = trackingLongitude
        
        objAppShareData.objModelBusinessSetup.incallTimeHours = incallTimeHours
        objAppShareData.objModelBusinessSetup.incallTimeMinute = incallTimeMinute
        objAppShareData.objModelBusinessSetup.outcallTimeHours = outcallTimeHours
        objAppShareData.objModelBusinessSetup.outcallTimeMinute = outcallTimeMinute
         objAppShareData.objModelBusinessSetup.businessHours = businessHours
        objAppShareData.objModelBusinessSetup.bookingSetting = bookingSetting
        
        objAppShareData.objModelBusinessSetup.paymentType = payOption
        //
        //
        
        let dictabc = ["OTP":OTP,
                       "__v":v,
                       "user_id":id1,
                       "business_id":business_id,
                       "staff_id":staff_id,
                       "address":address,
                       "address2":address2,
                       "app_type":appType,
                       "auth_token":authToken,
                       "bank_status":bankStatus,
                       "bio":bio,
                       "business_hours":businessHours,
                       "business_email":businessEmail,
                       "building_number":buildingNumber,
                       "business_name":businessName,
                       "business_type":businessType,
                       "business_postal_code":businesspostalCode,
                       "certificate_count":certificateCount,
                       "chatId":chatId,
                       "city":city,
                       "pay_option":payOption,
                       "contact_no":contactNo,
                       "country":country,
                       "country_code":countryCode,
                       "crd":crd,
                       "device_token":deviceToken,
                       "deviceType":deviceType,
                       "dob":dob,
                       "email":email,
                       "firebase_token":firebaseToken,
                       "first_name":firstName,
                       "followers_count":followersCount,
                       "following_count":followingCount,
                       "gender":gender,
                       "in_call_prepration_time":inCallpreprationTime,
                       "service_type":serviceType,
                       "social_id":socialId,
                       "social_type":socialType,
                       "state":state,
                       "status":status,
                       "upd":upd,
                       "user_name":userName,
                       "user_type":userType,
                       "service_count":serviceCount,
                       "review_count":reviewCount,
                       "rating_count":ratingCount,
                       "radius":radius,
                       "profile_image":profileImage,
                       "post_count":postCount,
                       "password":password,
                       "out_call_prepration_time":outCallpreprationTime,
                       "otp_verified":otpVerified,
                       "mail_verified":mailVerified,
                       "longitude":longitude,
                       "latitude":latitude,
                       "last_name":lastName,
                       "is_document":isDocument,
                       "business_contact_no":businessContactNo,
                       "incall_timeHours":incallTimeHours,
                       "incall_timeMinute":incallTimeMinute,
                       "outcall_timeHours":outcallTimeHours,
                       "trackingLatitude":trackingLatitude,
                       "trackingLongitude":trackingLongitude,
                       "outcall_timeMinute":outcallTimeMinute,
                       "booking_setting":bookingSetting]
        
        
        
     //   UserDefaults.standard.set(dictabc, forKey: UserDefaults.keys.userInfoDic)
        UserDefaults.standard.set(dictabc, forKey: UserDefaults.keys.businessInfoDic)
        UserDefaults.standard.set(id1, forKey: UserDefaults.keys.userId)
    }
    
    func clearOnLogout(){
        UserDefaults.standard.setValue("0", forKey: UserDefaults.keys.mainServiceType)
        UserDefaults.standard.set("", forKey: UserDefaults.keys.newAdminId)
        UserDefaults.standard.set(["":""], forKey: UserDefaults.keys.ParamCustomerTracking)
        
        
        //       let a UserDefaults.standard.setValue(param, forKey: UserDefaults.keys.businessInfoDic)
        //
        objAppShareData.objModelBusinessSetup.serviceType = "0"
        objAppShareData.objModelBusinessSetup.businessContactNo = ""
        objAppShareData.objModelBusinessSetup.inCallpreprationTime = "00:00"
        objAppShareData.objModelBusinessSetup.outCallpreprationTime = "00:00"
        objAppShareData.objModelBusinessSetup.businessEmail = ""
        objAppShareData.objModelBusinessSetup.businessName = ""
        objAppShareData.objModelBusinessSetup.location = ""
        objAppShareData.objModelBusinessSetup.address2 = ""
        objAppShareData.objModelBusinessSetup.country = ""
        objAppShareData.objModelBusinessSetup.state = ""
        objAppShareData.objModelBusinessSetup.city = ""
        objAppShareData.objModelBusinessSetup.radius = ""
        objAppShareData.objModelBusinessSetup.address = ""
        objAppShareData.objModelBusinessSetup.latitude = ""
        objAppShareData.objModelBusinessSetup.longitude = ""
        objAppShareData.objModelBusinessSetup.incallTimeHours = "00"
        objAppShareData.objModelBusinessSetup.incallTimeMinute = "00"
        objAppShareData.objModelBusinessSetup.outcallTimeHours = "00"
        objAppShareData.objModelBusinessSetup.outcallTimeMinute = "00"
        objAppShareData.objModelBusinessSetup.bookingSetting = ""
        objAppShareData.objModelBusinessSetup.paymentType  = ""

        saveAddress()
    }
    
    func registerBackgroundTask() {
      backgroundTask = UIApplication.shared.beginBackgroundTask { [weak self] in
        self?.endBackgroundTask()
      }
      assert(backgroundTask != .invalid)
    }
    
    func endBackgroundTask() {
      print("Background task ended.")
      UIApplication.shared.endBackgroundTask(backgroundTask)
      backgroundTask = .invalid
    }
}

//MARK: - core data methods
extension AppDelegate{
    
    func saveAddress(){
        if  let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as? Data{
            if let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded ) as? User{
                
                // let addressComponent = userInfo.address
                userInfo.address.city = objAppShareData.objModelBusinessSetup.city
                userInfo.address.locality = objAppShareData.objModelBusinessSetup.address
                //Rohit
               // userInfo.address.postalCode = objAppShareData.objModelBusinessSetup.postalCode
                userInfo.address.state = objAppShareData.objModelBusinessSetup.state
                userInfo.address.country = objAppShareData.objModelBusinessSetup.country
                userInfo.address.address2 = objAppShareData.objModelBusinessSetup.address2
                
                var longitude = ""
                var latitude = ""
                
                longitude = objAppShareData.objModelBusinessSetup.longitude
                latitude = objAppShareData.objModelBusinessSetup.latitude
                
                
                if latitude == ""{
                    latitude = "22.7051382"
                }
                if longitude == ""{
                    longitude = "75.9090618"
                }
                userInfo.address.longitude =  longitude
                userInfo.address.latitude = latitude
                userInfo.address.buildingNumber = objAppShareData.objModelBusinessSetup.buildinngNo
                if objAppShareData.objModelBusinessSetup.address.count > 0{
                    userInfo.address.fullAddress =  objAppShareData.objModelBusinessSetup.address
                }else{
                    userInfo.address.fullAddress = objAppShareData.objModelBusinessSetup.address2
                }
                userInfo.address.placeName = objAppShareData.objModelBusinessSetup.address
                let archivedObject = NSKeyedArchiver.archivedData(withRootObject: userInfo)
                UserDefaults.standard.setValue(archivedObject,forKey: UserDefaults.keys.userInfo)
                UserDefaults.standard.synchronize()
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
        let latitude = location?.coordinate.latitude ?? 0.0
        let longitude = location?.coordinate.longitude ?? 0.0
        objAppShareData.objModelBusinessSetup.trackingLatitude = String(latitude)
        objAppShareData.objModelBusinessSetup.trackingLongitude = String(longitude)
        
        
        let isLoggedIn : Bool =  UserDefaults.standard.bool(forKey: UserDefaults.keys.isLoggedIn)
        let dict  =  UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] ?? ["":""]
        let isDocument  = dict[UserDefaults.keys.isDocument] as? String ?? ""
        if isLoggedIn  == false{
            return
        }
        
        if #available(iOS 11.0, *) {
            objAppShareData.objAppdelegate.locationManager.showsBackgroundLocationIndicator = false
        } else {
            // Fallback on earlier versions
        }
        
        let startPoint = CLLocation(latitude: Double(lat)  ?? 0.0, longitude: Double(long) ?? 0.0)
        let currentPoint = CLLocation(latitude: Double(objAppShareData.objModelBusinessSetup.trackingLatitude) ?? 0.0, longitude:  Double(objAppShareData.objModelBusinessSetup.trackingLongitude) ?? 0.0)
        let distanceFromCurrentPointMeters = startPoint.distance(from: currentPoint)
        
        
        //Rohit Work on 11/Feb/2020
 
//        let geocoder = CLGeocoder()
//        geocoder.reverseGeocodeLocation((location ?? nil)!) { (placemarks, error) in
//            if (error != nil){
//                print("error in reverseGeocode")
//            }
//            if placemarks != nil{
//                let placemark = placemarks! as [CLPlacemark]
//                if placemark.count>0{
//                    let placemark = placemarks![0]
//
//                    let address = String(placemark.subLocality ?? "")+" "+String(placemark.name ?? "")
//                    let address2 = String(placemark.locality ?? "")+" "+String(placemark.administrativeArea ?? "")+" "+String(placemark.country ?? "")
//                    objAppShareData.objModelBusinessSetup.trackingAddress  = address+address2
//                }}
//        }
        
        if distanceFromCurrentPointMeters > 1000{
            lat = objAppShareData.objModelBusinessSetup.trackingLatitude
            long = objAppShareData.objModelBusinessSetup.trackingLongitude
            let param = ["outCallLatitude":lat, "outCallLongitude":long,"trackingLatitude":String(latitude),"trackingLongitude":String(longitude),"trackingAddress":objAppShareData.objModelBusinessSetup.trackingAddress]
            objAppShareData.callWebserviceForUpdateBusinessOutcallAddressp(dicParam: param)
        }else if updateFirstTime == true{
            updateFirstTime = false
            lat = objAppShareData.objModelBusinessSetup.trackingLatitude
            long = objAppShareData.objModelBusinessSetup.trackingLongitude
            let param = ["outCallLatitude":lat, "outCallLongitude":long,"trackingLatitude":String(latitude),"trackingLongitude":String(longitude),"trackingAddress":objAppShareData.objModelBusinessSetup.trackingAddress]
            objAppShareData.callWebserviceForUpdateBusinessOutcallAddressp(dicParam: param)
        }
        
    }
    
    
    
    func badgeCount(){
        
        var strMyId = ""
        if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.businessInfoDic){
            if let userId = dict["_id"] as? String {
                strMyId = userId
            }
            if strMyId != ""{
                
                ref.child("businessBookingBadgeCount").child( UserDefaults.standard.string(forKey: UserDefaults.keys.userId) ?? strMyId).observe(.value, with: { (snapshot) in
                    let dict = snapshot.value as? [String:Any]
                    print(dict)
                    if let count = dict?["totalCount"] as? Int {
                        UIApplication.shared.applicationIconBadgeNumber  = count
                       
                    }else if let count = dict?["totalCount"] as? String {
                        UIApplication.shared.applicationIconBadgeNumber = Int(count) ?? 0
                    }
                })
            }
            
        }}
}
extension URL {
    public var queryParameters: [String: String]? {
        guard let components = URLComponents(url: self, resolvingAgainstBaseURL: true), let queryItems = components.queryItems else {
            return nil
        }
        var parameters = [String: String]()
        for item in queryItems {
            parameters[item.name] = item.value
        }
        return parameters
    }
}

extension AppDelegate{
    
    func manageOnlineStatus(isOnlineValue:Int){
        if let myId = UserDefaults.standard.string(forKey: UserDefaults.keys.myId){
            let calendarDate = ServerValue.timestamp()
            let dict = ["isOnline":isOnlineValue,
                        "lastActivity":calendarDate] as [String : Any]
            Database.database().reference().child("users").child(myId).updateChildValues(dict)
        }}
}


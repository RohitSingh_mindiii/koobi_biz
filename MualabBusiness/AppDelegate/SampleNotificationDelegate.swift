//
//  SampleNotificationDelegate.swift
//  MualabCustomer
//
//  Created by Mac on 30/07/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

//https://medium.com/@lucasgoesvalle/custom-push-notification-with-image-and-interactions-on-ios-swift-4-ffdbde1f457

import Foundation

import UserNotifications
import UserNotificationsUI

protocol delegateSampleNotification {
    func handleNotificationWithNotificationData(userInfo : [String:Any])
}

class SampleNotificationDelegate: NSObject , UNUserNotificationCenterDelegate {
    
    var delegate : delegateSampleNotification!
    
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        if let userInfo = notification.request.content.userInfo as? [String : Any]{
            
            var notifincationType : Int?
            if let notiType = userInfo["notifincationType"] as? Int{
                notifincationType = notiType
            }else{
                if let notiType = userInfo["notifincationType"] as? String{
                    notifincationType = Int(notiType)
                }
            }
            
           
                
        if notifincationType == 15 {            
                if let opponentChatId = userInfo["opponentChatId"] as? String {
                    
                    if (objChatShareData.isOnChatScreen  && (objChatShareData.currentOpponantIDForNotification == opponentChatId)) {
                        completionHandler([])
                    }else{
                        completionHandler([.alert,.sound])
                    }
                }else{
                    completionHandler([.alert,.sound])
                }
            
            }else{
                completionHandler([.alert,.sound])
            }
        }
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        
        
      
        
        switch response.actionIdentifier {
            
        case UNNotificationDismissActionIdentifier:
            print("Dismiss Action")
        case UNNotificationDefaultActionIdentifier:
            print("Open Action")
            if let userInfo = response.notification.request.content.userInfo as? [String : Any]{
                delegate.handleNotificationWithNotificationData(userInfo: userInfo)
            }
        case "Snooze":
            print("Snooze")
        case "Delete":
            print("Delete")
        default:
            print("default")
        }
        completionHandler()
    }
}

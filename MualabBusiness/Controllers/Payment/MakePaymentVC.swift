//
//  MakePaymentVC.swift
//  HourLabors
//
//  Created by Mindiii on 12/24/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import Stripe
//import SVProgressHUD

class MakePaymentVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tblViewCardList: UITableView!
    @IBOutlet weak var lblNocard: UIView!
    @IBOutlet weak var viewMakePayment: UIView!
    @IBOutlet weak var btnPay: UIButton!

    @IBOutlet weak var lblAmount: UILabel!
    var stripeCustomerId = ""
   
    var arrAllCardList = [StripeDetailsModel]()
    var indexPath:Int? = nil
    var selectedIndex: Int = -1
    var strSource = ""
    var strjobid = ""
    var strbidid = ""
    var strserviceProviderid = ""
    var strbidAmount = ""
    var strBookingId = ""
    var isFromSlider: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblViewCardList.delegate = self
        self.tblViewCardList.dataSource = self
        self.lblNocard.isHidden = true
        if self.strBookingId.count == 0{
           self.isFromSlider = true
           self.btnPay.isHidden = true
        }else{
            self.isFromSlider = false
            self.btnPay.isHidden = false
        }
        //self.viewMakePayment.setViewShadowWithoutCornerRadius()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
//         if self.isFromSlider{
//        self.tabBarController?.view.frame =  CGRect(x:0,y:0,width:Int(self.view.frame.width),height:viewHeightGloble)
//        }
    }
    override func viewWillAppear(_ animated: Bool) {
        if objAppShareData.isFromPaymentDoneToAppointmentDetail{
        objAppShareData.isFromPaymentDoneToAppointmentDetail = false
        self.navigationController?.popViewController(animated: true)
        }
        if let strStripeCustomerId = UserDefaults.standard.string(forKey:UserDefaults.keys.stripeCustomerId){
            self.stripeCustomerId = strStripeCustomerId
        }
       
        self.callWebServiceFor_CreateCardDetails()
        self.lblAmount.text = "£" + strbidAmount
    }
    
    //MARK: Table view data source and delegate methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrAllCardList.count > 0  && self.strbidAmount.count != 0{
            self.btnPay.isHidden = false
        }else{
            self.btnPay.isHidden = true
        }
        return arrAllCardList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblViewCardList.dequeueReusableCell(withIdentifier:"CardListTVCell", for: indexPath as IndexPath) as? CardListTVCell
        let objGetData = self.arrAllCardList[indexPath.row]
        if self.selectedIndex == indexPath.row{
            cell?.viewCardDetail.isHidden = false
            cell?.viewCardDetail.isHidden = true
            //cell?.lblNo.textColor = #colorLiteral(red: 0.1882352941, green: 0.6784313725, blue: 0.137254902, alpha: 1)
            cell?.imgSelectCard.image =  UIImage(named:"check-symbol")!
        }else{
            cell?.viewCardDetail.isHidden = true
            //cell?.lblNo.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
            cell?.imgSelectCard.image =  nil
            //cell?.imgSelectCard.image =  UIImage(named:"inactive_check")!
        }
        cell?.lblNo.text = "XXXX XXXX XXXX " + objGetData.strlast4!
        cell?.lblCardNumber.text = "XXXX XXXX XXXX " + objGetData.strlast4!
        cell?.lblExpiryDate.text = String(objGetData.strexpMonth) + " /" + String(objGetData.strexpYear)
        cell?.lblCardHolderName.text = objGetData.strname
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.isFromSlider{
            let addNewCardVC = self.storyboard?.instantiateViewController(withIdentifier: "AddNewCardVC") as! AddNewCardVC
            let objCard = self.arrAllCardList[indexPath.row]
            addNewCardVC.objCardDetail = objCard
            addNewCardVC.isFromSlider = true
            addNewCardVC.isForDetail = true
            addNewCardVC.isfrompay = true
        self.navigationController?.pushViewController(addNewCardVC, animated: true)
            //objAppShareData.showAlert(withMessage: "Go to card detail screen", type: alertType.bannerDark,on: self)
        }else{
            self.selectedIndex = indexPath.row
            self.tblViewCardList.reloadData()
            let indexP = IndexPath.init(row: self.selectedIndex, section: 0)
            self.tblViewCardList.scrollToRow(at: indexP, at: .top, animated: false)
        }
    }

    @IBAction func btnBack(_ sender: Any) {
    self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnGoToAddNewCardVC(_ sender: Any) {
        let addNewCardVC = self.storyboard?.instantiateViewController(withIdentifier: "AddNewCardVC") as! AddNewCardVC
        addNewCardVC.isfrompay = true
        addNewCardVC.strjobid = strjobid
        addNewCardVC.strbidid = strbidid
        addNewCardVC.strserviceProviderid = strserviceProviderid
        addNewCardVC.strbidAmount = strbidAmount
        addNewCardVC.strBookingId = self.strBookingId
        addNewCardVC.isFromSlider = self.isFromSlider
        self.navigationController?.pushViewController(addNewCardVC, animated: true)
    }
    
    @IBAction func btnPay(_ sender: UIButton) {
       self.call_Webservice_Payment()
    }
}

//MARK:- extension for custom method
extension MakePaymentVC{
    func callWebServiceFor_CreateCardDetails(){
        objActivity.startActivityIndicator()
        self.view.endEditing(true)
        let param = ["":""]
        var url = "https://api.stripe.com/v1/customers"
        if let strStripeCustomerId = UserDefaults.standard.string(forKey:UserDefaults.keys.stripeCustomerId){
            url = url + "/" + strStripeCustomerId
            print(strStripeCustomerId)
        }
        objWebserviceManager.requestGetCardsFromStripe(strURL: url, params: param as [String : Any], success: { (response) in
            
            print(response)
            if  let Dict = response["sources"] as? [String: Any]{
            let arrMyPostNewData = Dict["data"] as![[String: Any]]
            self.arrAllCardList.removeAll()
            for dictGetData in arrMyPostNewData{
                let objNewData = StripeDetailsModel.init(dict: dictGetData)
                self.arrAllCardList.append(objNewData)
            }
            }else{
                let message = response["message"] as? String  ?? ""

                //objAppShareData.showAlert(withMessage: message, type: alertType.bannerDark,on: self)

            }
            if self.arrAllCardList.count == 0{
                self.lblNocard.isHidden = false
            }else{
                self.lblNocard.isHidden = true
            }
            objActivity.stopActivity()
            self.tblViewCardList.reloadData()
        }) { (error) in
            print(error)
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)

//            let alertView = UIAlertController(title:"Message" , message: "Failure", preferredStyle: .alert)
//            alertView.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
//            self.present(alertView, animated:true, completion:nil)
        }
    }
}

extension MakePaymentVC{
    
    func call_Webservice_Payment() {
        
//        /api/cardPayment
//
//        id : ('booking Id')
//        token
//        sourceType
//        customerId
        
        self.view.endEditing(true)
        if selectedIndex == -1{
            objAppShareData.showAlert(withMessage: "Please select card", type: alertType.bannerDark,on: self)
            return
        }
        
        let cardId = arrAllCardList[selectedIndex].strid ?? ""
        if let strStripeCustomerId = UserDefaults.standard.string(forKey:UserDefaults.keys.stripeCustomerId){
            stripeCustomerId = strStripeCustomerId
        }
        if !objServiceManager.isNetworkAvailable(){
          objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        objActivity.startActivityIndicator()
//
//        let dicParam = [
//            "id":strBookingId,
//            "token":cardId,
//            "sourceType":"card",
//            "customerId":stripeCustomerId,
//            ] as [String : Any]
        let startDate = getStartEndDate(startDate: true)
        let endDate = getStartEndDate(startDate: false)
        let obj = self.arrAllCardList[self.selectedIndex]
       let dicParam = [ "number":obj.strlast4 ?? "",
        "exp_month":obj.strexpMonth ?? "",
        "exp_year":obj.strexpYear ?? "",
        "cvv":"",
        "customerId":stripeCustomerId,
        "token":cardId ?? "",
        "startDate":startDate ?? "",
        "endDate":endDate ?? "",
        "payment":strbidAmount ?? "0.0"] as [String : Any]

        
        print(dicParam)
        objServiceManager.requestPostForJson(strURL: WebURL.adminPayment, params: dicParam, success: { response in
            print(response)
            objActivity.stopActivity()
            let keyExists = response["responseCode"] != nil
            if keyExists {
                sessionExpireAlertVC(controller: self)
            }else {
                let strSucessStatus = response["status"] as? String ?? ""
                if strSucessStatus == "success"{
               DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                            
                _ = SweetAlert().showAlert("Payment Successful !", subTitle: "Thank You! Your payment has been received.", style: AlertStyle.success, buttonTitle: "OK", buttonColor: UIColor.theameColors.skyBlueNewTheam, action: { (isClicked) in
                    
                    self.navigationController?.popViewController(animated: true)
                })
                    }
                    //objAppShareData.showAlert(withMessage: "Payment done successfully", type: alertType.bannerDark,on: self)
                }else{
                    let message = response["message"] as? String  ?? ""
                     objAppShareData.showAlert(withMessage: message, type: alertType.bannerDark,on: self)
                }
            }
        }) { error in
            objActivity.stopActivity()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
    }
        
        
        
        /*
        let param = ["jobId":strjobid ?? "",
                     "bidId":strbidid ?? "",
        "service_provider_id":strserviceProviderid ?? "",
                     "source":cardId ,"stripe_fees":"",
           "source_type":"card"] as? [String : AnyObject] ?? [:]
        
        objWebserviceManager.requestPost(strURL: WebURL.cardPayment, params: param  , success: { response in
            print(response)
            let message = response["message"] as? String ?? ""
            let status = (response["status"] as? String)!
            if status == "success"{
               self.Alertshow()
            }else{
                SVProgressHUD.showError(withStatus: message)
                print(status)
            }
        }) { (error) in
            print(error)
            let alertView = UIAlertController(title:"Message" , message: "Failure", preferredStyle: .alert)
            alertView.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alertView, animated:true, completion:nil)
        }*/
    }
    
    
    func getStartEndDate(startDate:Bool) -> String{
        
        let thisMonthCurrentDate = Date() ?? Date()
        let thisMonthStartDate = startOfMonth(startDate: Date())  ?? Date()
        let priviousEndDate  = Calendar.current.date(byAdding: .day, value: -1, to: thisMonthStartDate )  ?? Date()
        let priviousStartDate  = startOfMonth(startDate: priviousEndDate ?? Date())  ?? Date()
        
        let preStartDate = objAppShareData.dateFormatToShow1(forAPI: priviousStartDate )
        let preEndDate = objAppShareData.dateFormatToShow1(forAPI: priviousEndDate ?? Date())
        let CurrentDate = objAppShareData.dateFormatToShow1(forAPI: thisMonthCurrentDate )
        let StartDate = objAppShareData.dateFormatToShow1(forAPI: thisMonthStartDate )
     
        if startDate == true{
            return preStartDate
        }else{
            return CurrentDate
        }
    }
    
    func startOfMonth(startDate:Date) -> Date {
        return Calendar.current.date(from: Calendar.current.dateComponents([.year, .month], from: startDate))!
    }
    func endOfMonth(startDate:Date) -> Date {
        return Calendar.current.date(byAdding: DateComponents(month: 1, day: -1), to: startDate)!
    }
    func Alertshow()
    {
        let alertController = UIAlertController(title: "Alert", message: "Payment succesful done", preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
            UIAlertAction in
//            let storyboard = UIStoryboard(name: "Customer", bundle: nil)
//            let reviewVC = storyboard.instantiateViewController(withIdentifier: "reviewVC") as! reviewVC
//            reviewVC.strjobid = self.strjobid
//            reviewVC.strserviceProviderid = self.strserviceProviderid
//            reviewVC.reviewHandler = {
//                self.dismiss(animated: false) {
//                    self.navigationController?.popToRootViewController(animated: false)
//                }
//            }
//            reviewVC.modalPresentationStyle = .overCurrentContext
//            self.present(reviewVC, animated: false, completion: nil)
        }
        
        // Add the actions
        alertController.addAction(okAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
       }
//    func setViewShadowWithoutCornerRadius() {
//        layer.shadowColor = UIColor.darkGray.cgColor
//        //   layer.shadowOffset = CGSize(width: -0.5, height: 0.5)
//        layer.shadowOpacity = 0.2
//        layer.shadowRadius = 1.0
//        layer.shadowOffset = CGSize(width: 0, height: 2)
//    }
}



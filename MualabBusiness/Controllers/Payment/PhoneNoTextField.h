//
//  PhoneNoTextField.h
//  WeedRewards
//
//  Created by Mac on 17/07/17.
//  Copyright © 2017 Mindiii. All rights reserved.
//

#import <UIKit/UIKit.h>



//create delegate protocol
@protocol MyTextFieldDelegate <NSObject>
@optional
- (void)textFieldDidDelete;
@end

@interface PhoneNoTextField : UITextField<UIKeyInput>

//create "myDelegate"
@property (nonatomic, assign) id<MyTextFieldDelegate> myDelegate;
@end

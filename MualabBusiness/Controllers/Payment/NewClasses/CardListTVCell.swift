//
//  CardListTVCell.swift
//  HourLabors
//
//  Created by Mindiii on 12/18/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

class CardListTVCell: UITableViewCell {

    @IBOutlet weak var viewCell: UIView!
    
    @IBOutlet weak var lblCardHolderName: UILabel!
    @IBOutlet weak var lblCardNumber: UILabel!
    
    @IBOutlet weak var lblNo: UILabel!
    
    @IBOutlet weak var viewCardDetail: UIView!
    @IBOutlet weak var imgSelectCard: UIImageView!
    @IBOutlet weak var btnCardNumber: UIButton!
    @IBOutlet weak var lblCardNumberDetail: UILabel!
    
    @IBOutlet weak var btnRemoveCart: UIButton!
    @IBOutlet weak var lblExpiryDate: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        //viewCell.dropShadow(color: .lightGray, opacity: 0.4, offSet:CGSize(width: -1, height: 1), radius: 3, scale: true)
        viewCell.layer.cornerRadius = 5
        //viewCardDetail.dropShadow(color: .lightGray, opacity: 0.4, offSet:CGSize(width: -1, height: 1), radius: 3, scale: true)
        viewCardDetail.layer.cornerRadius = 5
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}

//
//  PaymentHistoryData.swift
//  MualabCustomer
//
//  Created by Mac on 23/06/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import Foundation

class PaymentHistoryData {
    
    var _id : Int = 0
    var artistId : Int = 0
 
    var bookStatus : Int = 0
    var bookingDate : String = ""
    var bookingTime : String = ""
    var location : String = ""
    var paymentStatus : Int = 0
    var paymentType : Int = 0
    var profileImage : String = ""
    var reviewStatus : Int = 0
    var timeCount : Int = 0
    var totalPrice : String = ""
    var userName : String = ""
    var rating : Double = 0
    var reviewByUser : String = ""
    var reviewByArtist : String = ""
    
    var indexPath : IndexPath?
    var arr_artistService : [String] = []
    
    var isReviewAvailable = false
    
    init(dict : [String : Any]){
        
        _id = dict["_id"] as? Int ?? 0
        artistId = dict["artistId"] as? Int ?? 0
        //bookStatus = dict["bookStatus"] as? Int ?? 0
        bookingDate = dict["bookingDate"] as? String ?? ""
        bookingTime = dict["bookingTime"] as? String ?? ""
        location = dict["location"] as? String ?? ""
        paymentStatus = dict["paymentStatus"] as? Int ?? 0
        paymentType = dict["paymentType"] as? Int ?? 0
        profileImage = dict["artistProfileImage"] as? String ?? ""
        timeCount = dict["timeCount"] as? Int ?? 0
        userName = dict["artistName"] as? String ?? ""
        reviewByUser = dict["reviewByUser"] as? String ?? ""
        reviewByArtist = dict["reviewByArtist"] as? String ?? ""
        
        if let rating = dict["userRating"] as? String {
            self.rating = Double(rating) ?? 0
        }else if let rating = dict["userRating"] as? Int {
            self.rating = Double(rating)
        }
        
        if let total = dict["totalPrice"] as? String {
            let doubleValue = Double(total) ?? 0
            let doubleStr = String(format: "%.2f", ceil(doubleValue * 100)/100)
            self.totalPrice = doubleStr
        }
        
        if let arr = dict["artistService"] as? [String]{
            for str in arr{
                self.arr_artistService.append(str)
            }
        }
        
        if let str = dict["bookStatus"] as? String {
            bookStatus = Int(str) ?? 0
        }
        
        reviewStatus = dict["reviewStatus"] as? Int ?? 0
        
        if reviewByUser == "" {
            isReviewAvailable = false
        }else{
            isReviewAvailable = true
        }
    }
}

/*
 {
 "_id" = 29;
 artistId = 2;
 artistName = pankaj;
 artistProfileImage = "http://koobi.co.uk:3000/uploads/profile/1527933785077.jpg";
 artistRating = 0;
 artistService =             (
 "Black hair color"
 );
 bookStatus = 3;
 bookingDate = "2018-06-22";
 bookingTime = "04:20 PM";
 location = "MINDIII Systems Pvt. Ltd., Main Road, Brajeshwari Extension, Pipliyahana, Indore, Madhya Pradesh, India";
 paymentStatus = 0;
 paymentType = 2;
 reviewByArtist = "";
 reviewByUser = "";
 reviewStatus = 0;
 timeCount = 980;
 totalPrice = "50.75";
 transjectionId = "";
 userId = 29;
 userName = rajesh;
 userProfileImage = "http://koobi.co.uk:3000/uploads/profile/1528203800948.jpg";
 userRating = 0;
 }
 */

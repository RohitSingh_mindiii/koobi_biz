//
//  ServicesModel.swift
//  MualabBusiness
//
//  Created by Mac on 23/01/2018 .
//  Copyright © 2018 Mindiii. All rights reserved.
//

import Foundation

class artistServices:NSObject {
    var _id : Int?
    var title : String?
    var subService = [SubService]()
    
    init?(id:Int,title:String,subService:[SubService]) {
        self._id = id
        self.title = title
        self.subService = subService
    }
}


class SubService:NSObject {
    var _id : Int?
     var image : String?
    var title : String?
    var subCategories = [SubCategories]()
    
    var serviceId : Int = 0
    var serviceName : String = ""
    
    var subServiceId : Int = 0
    var subServiceName : String = ""
    var serviceCount = 0
    //var arrSubSubService : [SubSubService] = [SubSubService]()
    var arrInCall : [SubSubService] = [SubSubService]()
    var arrOutCall : [SubSubService] = [SubSubService]()

    init(dict : [String : Any]){
        
        serviceId = dict["serviceId"] as? Int ?? 0
        subServiceId = dict["subServiceId"] as? Int ?? 0
        subServiceName = dict["subServiceName"] as? String ?? ""
        
        if let arr = dict["artistservices"] as? [[String : Any]]{
            serviceCount = arr.count
            for dict in arr{
                let objSubSubService = SubSubService.init(dict: dict)
                let strType = dict["bookingType"] as? String ?? ""
                if strType == "Incall"{
                    objSubSubService.subServiceId = subServiceId
                    objSubSubService.subServiceName = subServiceName
                    self.arrInCall.append(objSubSubService)
                }else if strType == "Outcall"{
                    objSubSubService.subServiceId = subServiceId
                    objSubSubService.subServiceName = subServiceName
                    self.arrOutCall.append(objSubSubService)
                }else if strType == "Both"{
                    objSubSubService.subServiceId = subServiceId
                    objSubSubService.subServiceName = subServiceName
                    self.arrInCall.append(objSubSubService)
                    self.arrOutCall.append(objSubSubService)
                }
            }
            serviceCount = arrOutCall.count+arrInCall.count
        }
        
    }
    
}



class SubServiceAddStaff:NSObject {
    var serviceId : Int = 0
    var serviceName : String = ""
    var subServiceId : Int = 0
    var subServiceName : String = ""
   // var arrSubSubService : [SubSubService] = [SubSubService]()
    init(dict : [String : Any]){
        serviceId = dict["serviceId"] as? Int ?? 0
        subServiceId = dict["subServiceId"] as? Int ?? 0
        subServiceName = dict["subServiceName"] as? String ?? ""
        if let arr = dict["artistservices"] as? [[String : Any]]{
//            for dict in arr{
//                 let objSubSubService = SubSubService.init(dict: dict)
//                objSubSubService.subServiceId = subServiceId
//                objSubSubService.subServiceName = subServiceName
//                self.arrSubSubService.append(objSubSubService)
//            }
        }
        
    }
}
class SubCategories:NSObject {
    
    var _id : Int?
    var title : String?
    var serviceDescription : String?
    var incallPrice : Float?
    var oucallPrice : Float?
    var complitionTime: String?
    
    init?(title:String,desc:String,incallPrice:Float,oucallPrice:Float,complitionTime:String) {
        self.title = title
        self.serviceDescription = desc
        self.incallPrice = incallPrice
        self.oucallPrice = oucallPrice
        self.complitionTime = complitionTime
    }
}

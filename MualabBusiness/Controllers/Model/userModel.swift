//
//  userModel.swift
//  MualabBusiness
//
//  Created by Mac on 10/01/2018 .
//  Copyright © 2018 Mindiii. All rights reserved.
//

import Foundation

class User : NSObject,NSCoding {
    
   // var address = Address(locality:"",address2:"", city:"", state: "", postalCode: "", country:"", placeName:"", fullAddress:"", latitude: "", longitude:"")
    var address = Address(locality:"",address2:"", city:"", state: "", country:"", placeName:"", fullAddress:"", latitude: "", longitude:"")
    
    var type:String = "0"
    var email:String = "0"
    var countryCode:String = "0"
    var contactNumber:String = "0"
    var firstName:String = "0"
    var lastName:String = "0"
    var businessName:String = "0"
    var userName:String = "0"
    var profileImage:String = "0"
    var userId:NSNumber = 0
    var bankStatus:Bool = false
    var bio : String = "0"
    var gender:String = "0"
    var DOB:String = "0"

    required init(address:Address) {
        self.address = address
        super.init()
    }
    required init(userType:String) {
        self.type = userType
        super.init()
    }
    // NSCodeing
    required init?(coder aDecoder: NSCoder) {
        
        if let add = aDecoder.decodeObject(forKey: Keys.user.address) as? Address {
            self.address = add
        }
        //self.address = (aDecoder.decodeObject(forKey: Keys.user.address) as? Address)!
        self.type = aDecoder.decodeObject(forKey: Keys.user.type) as? String ?? "0"
        self.email = aDecoder.decodeObject(forKey: Keys.user.email) as? String ?? "0"
        self.countryCode = aDecoder.decodeObject(forKey: Keys.user.countryCode) as? String ?? "0"
        self.contactNumber = aDecoder.decodeObject(forKey: Keys.user.contactNumber) as? String ?? "0"
        self.firstName = aDecoder.decodeObject(forKey: Keys.user.firstName) as? String ?? "0"
        self.lastName = aDecoder.decodeObject(forKey: Keys.user.lastName) as? String ?? "0"
        self.businessName = aDecoder.decodeObject(forKey: Keys.user.businessName) as? String ?? ""
        self.userName = aDecoder.decodeObject(forKey: Keys.user.userName) as? String ?? ""
        self.profileImage = aDecoder.decodeObject(forKey: Keys.user.profileImage) as? String ?? ""
        self.userId = aDecoder.decodeObject(forKey: Keys.user.userId) as! NSNumber
        self.bankStatus =  Bool(aDecoder.decodeBool(forKey: Keys.user.bankStatus))
        self.bio = aDecoder.decodeObject(forKey: Keys.user.bio) as? String ?? "0"
        self.gender = aDecoder.decodeObject(forKey: Keys.user.gender) as? String ?? "0"
        self.DOB = aDecoder.decodeObject(forKey: Keys.user.DOB) as? String ?? "0"

    }
    
    func encode(with aCoder: NSCoder) {
        
        aCoder.encode(self.address, forKey: Keys.user.address)
        aCoder.encode(self.type, forKey: Keys.user.type)
        aCoder.encode(self.email, forKey: Keys.user.email)
        aCoder.encode(self.countryCode, forKey:Keys.user.countryCode)
        aCoder.encode(self.contactNumber, forKey:Keys.user.contactNumber)
        aCoder.encode(self.firstName, forKey: Keys.user.firstName)
        aCoder.encode(self.lastName, forKey: Keys.user.lastName)
        aCoder.encode(self.businessName, forKey:Keys.user.businessName)
        aCoder.encode(self.userName, forKey:Keys.user.userName)
        aCoder.encode(self.userId  , forKey:Keys.user.userId)
        aCoder.encode(self.profileImage, forKey:Keys.user.profileImage)
        aCoder.encode(self.bankStatus  , forKey:Keys.user.bankStatus)
        aCoder.encode(self.bio  , forKey:Keys.user.bio)
        aCoder.encode(self.gender, forKey:Keys.user.gender)
        aCoder.encode(self.DOB, forKey:Keys.user.DOB)

    }
    
}

class Address : NSObject,NSCoding  {
    
    var locality : String = ""
    var address2 : String = ""
    var city: String = ""
    var state: String = ""
   // var postalCode: String = ""
    var country : String = ""
    var placeName : String = ""
    var fullAddress : String = ""
    var latitude : String = ""
    var longitude : String = ""
    var buildingNumber:String = ""
    
    // required init(locality : String,address2:String,city:String,state:String,postalCode:String,country:String,placeName:String,fullAddress:String,latitude:String,longitude:String) {
    
    required init(locality : String,address2:String,city:String,state:String,country:String,placeName:String,fullAddress:String,latitude:String,longitude:String) {
        
        self.locality = locality
        self.address2 = address2
        self.city = city
        self.state = state
       // self.postalCode = postalCode
        self.country = country
        self.placeName = placeName
        self.fullAddress = fullAddress
        self.latitude = latitude
        self.longitude = longitude
        
        super.init()
    }
    
    // NSCodeing
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.locality, forKey: Keys.user.addressComponent.locality)
        aCoder.encode(self.address2, forKey: Keys.user.addressComponent.address2)
        aCoder.encode(self.city, forKey: Keys.user.addressComponent.city)
        aCoder.encode(self.state, forKey: Keys.user.addressComponent.state)
       // aCoder.encode(self.postalCode, forKey: Keys.user.addressComponent.postalCode)
        aCoder.encode(self.country, forKey: Keys.user.addressComponent.country)
        aCoder.encode(self.placeName, forKey: Keys.user.addressComponent.placeName)
        aCoder.encode(self.fullAddress, forKey: Keys.user.addressComponent.fullAddress)
        aCoder.encode(self.latitude, forKey: Keys.user.addressComponent.latitude)
        aCoder.encode(self.longitude, forKey: Keys.user.addressComponent.longitude)
        aCoder.encode(self.buildingNumber  , forKey:Keys.user.buildingNumber)
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.locality = aDecoder.decodeObject(forKey: Keys.user.addressComponent.locality) as? String ?? ""
        self.address2 = aDecoder.decodeObject(forKey: Keys.user.addressComponent.address2) as? String ?? ""
        self.city = aDecoder.decodeObject(forKey: Keys.user.addressComponent.city) as? String ?? ""
        self.state = aDecoder.decodeObject(forKey: Keys.user.addressComponent.state) as? String ?? ""
       // self.postalCode = aDecoder.decodeObject(forKey: Keys.user.addressComponent.postalCode) as? String ?? ""
        self.country = aDecoder.decodeObject(forKey: Keys.user.addressComponent.country) as? String ?? ""
        self.placeName = aDecoder.decodeObject(forKey: Keys.user.addressComponent.placeName) as? String ?? ""
        self.fullAddress = aDecoder.decodeObject(forKey: Keys.user.addressComponent.fullAddress) as? String ?? ""
        self.latitude = aDecoder.decodeObject(forKey: Keys.user.addressComponent.latitude) as? String ?? ""//Double(aDecoder.decodeDouble(forKey: Keys.user.addressComponent.latitude))
        self.longitude = aDecoder.decodeObject(forKey: Keys.user.addressComponent.longitude) as? String ?? "" //Double(aDecoder.decodeDouble(forKey: Keys.user.addressComponent.longitude))
        self.buildingNumber = aDecoder.decodeObject(forKey: Keys.user.buildingNumber) as? String ?? ""
        
    }
}

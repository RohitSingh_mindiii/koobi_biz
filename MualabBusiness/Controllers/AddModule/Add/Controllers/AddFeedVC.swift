//
//  AddFeedVC.swift
//  Mualab
//
//  Created by Mac on 29/11/2017 .
//  Copyright © 2017 MINDIII. All rights reserved.
//

import UIKit
import Photos
import AVFoundation
import AVKit

fileprivate class assetModel : NSObject {
    var imgForPost : UIImage
    var isSelectedPreview : Bool
    var asset : PHAsset
    
    init(selectState : Bool, phAsset:PHAsset) {
        self.isSelectedPreview = selectState
        self.asset = phAsset
        self.imgForPost = UIImage()
    }
}

class AddFeedVC : UIViewController {
    fileprivate var isNewImageAdd = false
    fileprivate var oldIndexPath = IndexPath()
    fileprivate var page = 0
    fileprivate var arrAllAsset = [assetModel]()
    fileprivate var arrSelectedAsset = [assetModel]()
    fileprivate var arrImages = [UIImage]()
    fileprivate var objLastAsset = assetModel(selectState: false, phAsset: PHAsset())
    fileprivate var isMultipleAsset = false
    fileprivate var isVideo = false
    
    fileprivate var currentIndexPath = IndexPath()
    fileprivate var lastContentOffset: CGFloat = 0
    
    fileprivate var mp4VideoURL : URL?
    fileprivate var imgThumbICLoud : UIImage?
    fileprivate var avPlayer = AVPlayer()
    fileprivate let avPlayerController = AVPlayerViewController()
    
    fileprivate let unSelectedColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.5)
    fileprivate let selectedColor = UIColor(red: 193.0 / 255.0, green: 120.0 / 255.0, blue: 0.0 / 255.0, alpha: 1.0)
    
    //UICollectionViews
    @IBOutlet weak var mediaCollection: UICollectionView!
    
    //UIViews
    @IBOutlet weak var viewVideo: UIView!
    @IBOutlet weak var itemsView: UIView!
    @IBOutlet weak var noResultView: UIView!
    
    
    //UIScrollViews
    //@IBOutlet weak var viewScrollVideo: UIScrollView!
    @IBOutlet weak var dataScrollView: UIScrollView!
    
    //FAScrollView
    @IBOutlet weak var viewScrollImage: FAScrollView!
    
    //UIButtons
    
    @IBOutlet weak var btnCrop: UIButton!
    @IBOutlet weak var btnMultiSelection: UIButton!
    @IBOutlet weak var btnImages: UIButton!
    @IBOutlet weak var btnVideo: UIButton!
    @IBOutlet weak var btnCamera: UIButton!
    
    
    @IBAction func btnLogout (_ sender:Any){
        appDelegate.logout()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
}
//MARK: - Views functions
extension AddFeedVC{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addGesturesToView()
        ////
        configureView()
        //objWebserviceManager.StartIndicator()
        GetAllMediaFromDevice(withPage:0)
        self.btnCrop.isHidden = false
        ////
     
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.hidesBottomBarWhenPushed = true
        imageRatioDynamic = 0
        ////
        if objAppShareData.isCameraForFeed{
            objAppShareData.isCameraForFeed = false
            let sb = UIStoryboard(name: "Add", bundle: Bundle.main)
            let objNewPost = sb.instantiateViewController(withIdentifier:"NewPostVC") as! NewPostVC
            if objAppShareData.isVideoPostFeed{
                objAppShareData.isVideoPostFeed = false
                objNewPost.isVideo = true
                objNewPost.videoURL = objAppShareData.videoUrlForPostFeed
            }else{
                objNewPost.isVideo = false
                objNewPost.videoURL = nil
            }
            objNewPost.imgFeed = objAppShareData.imgForPostFeed//img
            objNewPost.imgThumb = objAppShareData.imgForPostFeed//img
            let arr = [objAppShareData.imgForPostFeed!]
            objNewPost.arrFeedImages = arr
            self.navigationController?.pushViewController(objNewPost, animated: true)
            return
        }
        ////
        if objAppShareData.isSwitchToFeedTab {
            objAppShareData.isCloseAddFeed = false
            avPlayer.pause()
            self.viewVideo.layer.sublayers?.forEach {
                $0.removeFromSuperlayer()
            }
            self.dismiss(animated: false) {
                objAppShareData.objAppdelegate.gotoTabBar(withAnitmation: false)
            }
        }
//        configureView()
//        objWebserviceManager.StartIndicator()
//        GetAllMediaFromDevice(withPage:0)
//        self.btnCrop.isHidden = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    //    override var prefersStatusBarHidden: Bool {
    //        return true
    //    }
}

//MARK: - Local Method
private extension AddFeedVC{
    
    func configureView(){
        unSelectMethod()
        self.btnImages.isSelected = true
        self.btnVideo.isSelected = false
        self.btnCamera.isSelected = false
        
        isVideo = false
        arrSelectedAsset.removeAll()
        self.arrImages.removeAll()
        self.mp4VideoURL = nil
        self.dataScrollView.contentOffset.y = 0
    }
    
    func unSelectMethod(){
        btnCrop.setImage(#imageLiteral(resourceName: "crop_img_ico"), for: .normal)
        btnMultiSelection.setImage(#imageLiteral(resourceName: "fixed_img_ico"), for: .normal)
        
        // btnCrop.layer.borderColor = UIColor.white.cgColor
        //        btnMultiSelection.layer.borderColor = UIColor.white.cgColor
        //        btnMultiSelection.backgroundColor = unSelectedColor
        //        btnCrop.backgroundColor = unSelectedColor
        
        isMultipleAsset = false
    }
    func GetAllMediaFromDevice(withPage pageNo:Int) ->Void{
        arrAllAsset.removeAll()
        PHPhotoLibrary.requestAuthorization { (status) in
            switch status {
            case .authorized:
                self.fetchGallery()
            case .denied, .restricted:
                //"Not allowed"
                self.showGalleryPermissionAlert()
                break
            case .notDetermined:
                //"Not determined yet"
                break
            @unknown default:
                break
            }
        }
    }
    
    func showGalleryPermissionAlert(){
        let alert = UIAlertController(title: "''Koobi Biz'' requires photo gallery  access.", message:"Go to Settings to allow ''Koobi Biz'' to access your photo gallery.", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler:nil))
        alert.addAction(UIAlertAction(title: "Settings", style: .default, handler: { action in
            switch action.style{
            case.default:
                let url = URL(string: UIApplication.openSettingsURLString)
                if UIApplication.shared.canOpenURL(url!) {
                    UIApplication.shared.open(url!, options: [:], completionHandler: nil)
                    UIApplication.shared.open(url!, options: [:], completionHandler: { (success) in
                        print("Open url : \(success)")
                    })
                }
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
                
            }}))
        self.present(alert, animated: true, completion: nil)
    }
    
    func fetchGallery(){
//        DispatchQueue.main.async {
//           objWebserviceManager.StartIndicator()
//        }
        let fetchOptions = PHFetchOptions()
        fetchOptions.sortDescriptors = [NSSortDescriptor(key:"creationDate", ascending: false)]
        
        if self.isVideo {
            fetchOptions.predicate = NSPredicate(format: "mediaType == %d", PHAssetMediaType.video.rawValue)
        }else{
            fetchOptions.predicate = NSPredicate(format: "mediaType == %d", PHAssetMediaType.image.rawValue)
        }
        
        let imagesAndVideos = PHAsset.fetchAssets(with: fetchOptions)
        
        for j in 0..<imagesAndVideos.count {
            let asset = imagesAndVideos.object(at:j)
            if asset.mediaType == .image || asset.mediaType == .video{
                let objAssets = assetModel(selectState: false, phAsset: asset)
                
                self.arrAllAsset.append(objAssets)
            }
        }
        
        DispatchQueue.main.async {
            if self.arrAllAsset.count > 0{
                self.noResultView.isHidden = true
                let objAsset = self.arrAllAsset[0]
                objAsset.isSelectedPreview = true
                self.addAssetObjectToView(withSelectedAsset: objAsset.asset)
                self.mediaCollection.reloadData()
                self.currentIndexPath = IndexPath(row: 0, section: 0)
            }else{
                self.noResultView.isHidden = false
            }
            objActivity.stopActivity()
        }
    }
    func setDataInArray(){
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(5000)) {
            
            //DispatchQueue.main.async {
            if self.arrAllAsset.count > 0{
                self.noResultView.isHidden = true
                let objAsset = self.arrAllAsset[0]
                objAsset.isSelectedPreview = true
                self.addAssetObjectToView(withSelectedAsset: objAsset.asset)
                self.mediaCollection.reloadData()
                self.currentIndexPath = IndexPath(row: 0, section: 0)
            }else{
                self.noResultView.isHidden = false
            }
            objActivity.stopActivity()
        }
    }
    func getAssetThumbnail(asset: PHAsset) -> UIImage {
        
        let manager = PHImageManager.default()
        let option = PHImageRequestOptions()
        var thumbnail = UIImage()
        option.isSynchronous = true
        let dimension = mediaCollection.frame.size.width/4+20
        manager.requestImage(for: asset, targetSize: CGSize(width:dimension, height:dimension), contentMode: .default, options: option, resultHandler: {(result, info)->Void in
            if result != nil{
                thumbnail = result!
            }
        })
        return thumbnail
    }
    
    func getURL(ofMediaWith mPhasset: PHAsset, completionHandler : @escaping ((_ responseURL : URL?) -> Void)) {
        
        if mPhasset.mediaType == .image {
            let options: PHContentEditingInputRequestOptions = PHContentEditingInputRequestOptions()
            options.canHandleAdjustmentData = {(adjustmeta: PHAdjustmentData) -> Bool in
                return true
            }
            mPhasset.requestContentEditingInput(with: options, completionHandler: { (contentEditingInput, info) in
                if contentEditingInput != nil{
                    completionHandler(contentEditingInput!.fullSizeImageURL)
                }else {
                    completionHandler(nil)
                }
            })
            
        } else if mPhasset.mediaType == .video {
            //// Himanshu
            let options: PHVideoRequestOptions = PHVideoRequestOptions()
            options.version = .original
            ////
            PHImageManager.default().requestAVAsset(forVideo: mPhasset, options: options, resultHandler: { (asset, audioMix, info) in

                if let urlAsset = asset as? AVURLAsset {
                    let localVideoUrl = urlAsset.url
                    completionHandler(localVideoUrl)
                } else {

                    completionHandler(nil)
                }
            })
        }
    }
    
    func addAssetObjectToView(withSelectedAsset asset:PHAsset){
        switch asset.mediaType {
        case .image:
            avPlayer.pause()
            self.viewScrollImage.isHidden = false
            self.viewVideo.isHidden = true
            getURL(ofMediaWith: asset, completionHandler: { url in
                if url != nil{
                    do {
                        let imageData = try Data.init(contentsOf: url!)
                        self.viewScrollImage.imageToDisplay = UIImage(data: imageData)
                        self.viewScrollImage.zoom()
                        /////
                        /////
                        if self.isNewImageAdd {
                            self.isNewImageAdd = false
                            if self.arrSelectedAsset.count>0{
                                let objAsset = self.arrSelectedAsset[self.arrSelectedAsset.count-1]
                                let imgNew = self.captureVisibleRect()
                                objAsset.imgForPost = imgNew
                                self.objLastAsset.imgForPost = objAsset.imgForPost
                            }
                        }
                        /////
                        /////
                        //self.btnCrop.backgroundColor = self.unSelectedColor
                        self.btnCrop.isSelected = false
                    } catch {
                        print("dfdffd")
                        //let imageData = UIImagePNGRepresentation(self.imgThumbICLoud)
                        //self.viewScrollImage.imageToDisplay = UIImage(data: imageData)
                        self.viewScrollImage.imageToDisplay = self.imgThumbICLoud
                        self.viewScrollImage.zoom()
                        //self.btnCrop.backgroundColor = self.unSelectedColor
                        self.btnCrop.isSelected = false
                    }
                }else{
                    let manager = PHImageManager.default()
                    let option = PHImageRequestOptions()
                    option.isSynchronous = true
                    let dimension = 200
                    manager.requestImage(for: asset, targetSize: CGSize(width:dimension, height:dimension), contentMode: .default, options: option, resultHandler: {(result, info)->Void in
                        if result != nil{
                            self.imgThumbICLoud = result!
                            self.viewScrollImage.imageToDisplay = self.imgThumbICLoud
                            self.viewScrollImage.zoom()
                            self.btnCrop.isSelected = false
                        }
                    })
                    
                    /*
                     print("NNNNEEWWWdfdffd")
                     //let imageData = UIImagePNGRepresentation(self.imgThumbICLoud)
                     //self.viewScrollImage.imageToDisplay = UIImage(data: imageData)
                     self.viewScrollImage.imageToDisplay = self.imgThumbICLoud
                     self.viewScrollImage.zoom()
                     //self.btnCrop.backgroundColor = self.unSelectedColor
                     self.btnCrop.isSelected = false
                     */
                }
            })
            
        case .video:
            avPlayer.pause()
            self.viewScrollImage.isHidden = true
            self.viewVideo.isHidden = false
            self.viewVideo.layer.sublayers?.forEach { $0.removeFromSuperlayer() }
            getURL(ofMediaWith: asset, completionHandler: { url in
                DispatchQueue.main.async {
                    if url != nil{
                        self.playVideo(WithURL:url!)
                    }else{
                        showAlertVC(title: "Cannot find video", message: "iCloud video", controller: self)
                    }
                }
            })
        default:
            break
        }
    }
    
    func playVideo(WithURL url:URL){
        
        DispatchQueue.main.async {
            self.mp4VideoURL = url
            self.avPlayer = AVPlayer(url:url)
            self.avPlayer.isMuted = false
            self.avPlayerController.player = self.avPlayer
            let avPlayerLayer = AVPlayerLayer(player:self.avPlayer)
            avPlayerLayer.videoGravity=AVLayerVideoGravity.resizeAspect;
            avPlayerLayer.frame = self.viewVideo.frame
            self.viewVideo.layer.addSublayer(avPlayerLayer)
            self.avPlayer.play()
        }
    }
    
    func selectMultipleAssets(withAssetModel objAsset:assetModel, andSelectedCell cell:postCollectionCell){
        
        if arrSelectedAsset.contains(objAsset){
            if self.arrSelectedAsset.count == 1{
                return
            }
            cell.imgSelected.isHidden = true
            if let index = arrSelectedAsset.index(of: objAsset){
                arrSelectedAsset.remove(at: index)
                if index == 0{
                    if arrSelectedAsset.count > 0{
                        let obj = arrSelectedAsset[arrSelectedAsset.count-1]
                        let previousObjAsset = arrAllAsset[self.oldIndexPath.row]
                        if self.objLastAsset.imgForPost == objAsset.imgForPost{
                            self.isNewImageAdd = false
                            addAssetObjectToView(withSelectedAsset:obj.asset)
                            self.objLastAsset.imgForPost = obj.imgForPost
                        }
                    }
                }else{
                    if arrSelectedAsset.count > 0{
                        let obj = arrSelectedAsset[index-1]
                        let previousObjAsset = arrAllAsset[self.oldIndexPath.row]
                        if self.objLastAsset.imgForPost == objAsset.imgForPost{
                            self.isNewImageAdd = false
                            addAssetObjectToView(withSelectedAsset:obj.asset)
                            self.objLastAsset.imgForPost = obj.imgForPost
                        }
                    }
                }
            }
        }else{
            if arrSelectedAsset.count >= 10{
                showAlertVC(title: kAlertTitle, message:"You cannot select more than 10 items at once", controller: self)
                return
            }
            cell.imgSelected.isHidden = false
            if !arrSelectedAsset.contains(objAsset){
                arrSelectedAsset.append(objAsset)
            }
            if arrSelectedAsset.count >= 10{
            }else{
                imgThumbICLoud = cell.imgICloud
                self.isNewImageAdd = true
                addAssetObjectToView(withSelectedAsset:objAsset.asset)
            }
        }
    }
    
    func captureVisibleRect() -> UIImage{
        
        var croprect = CGRect.zero
        let xOffset = ((viewScrollImage.imageToDisplay?.size.width) ?? self.view.frame.width) / viewScrollImage.contentSize.width;
        let yOffset = ((viewScrollImage.imageToDisplay?.size.height) ?? self.view.frame.width)  / viewScrollImage.contentSize.height;
        
        croprect.origin.x = viewScrollImage.contentOffset.x * xOffset;
        croprect.origin.y = viewScrollImage.contentOffset.y * yOffset;
        
        let normalizedWidth = (viewScrollImage?.frame.width)! / (viewScrollImage?.contentSize.width)!
        let normalizedHeight = (viewScrollImage?.frame.height)! / (viewScrollImage?.contentSize.height)!
        
        croprect.size.width = viewScrollImage.imageToDisplay!.size.width * normalizedWidth
        croprect.size.height = viewScrollImage.imageToDisplay!.size.height * normalizedHeight
        
        let toCropImage = viewScrollImage.imageView.image?.fixImageOrientation()
        let cr: CGImage? = toCropImage?.cgImage?.cropping(to: croprect)
        let croppedImage = UIImage(cgImage: cr!)
        
        return croppedImage
    }
    //
    func generateImage(_ URL: URL) -> UIImage {
        
        var image = UIImage()
        let asset = AVAsset.init(url: URL)
        let assetImageGenerator = AVAssetImageGenerator(asset: asset)
        
        assetImageGenerator.appliesPreferredTrackTransform=true;
        let widthNew = self.view.frame.width
        let height = widthNew*0.75
    //    let maxSize = CGSize(width: 320, height: 180)
        let maxSize = CGSize(width: widthNew, height: height)
        assetImageGenerator.maximumSize = maxSize
        
        var time = asset.duration
        time.value = min(time.value, 2)
        
        do {
            let imageRef = try assetImageGenerator.copyCGImage(at: time, actualTime: nil)
            image =  UIImage.init(cgImage: imageRef)
            return image
        } catch {
            
        }
        
        return image
    }
    //
    func addGesturesToView() -> Void {
        let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(self.upSwipe))
        swipeUp.direction = .up
        self.mediaCollection.addGestureRecognizer(swipeUp)
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.downSwipe))
        swipeDown.direction = .down
        self.mediaCollection.addGestureRecognizer(swipeDown)
    }
    
    @objc func upSwipe() ->Void{
        // move up
        if itemsView.isHidden == false{
            UIView.animate(withDuration: 0.5, animations: {
                self.itemsView.isHidden = true
            })
        }
    }
    @objc func downSwipe() ->Void{
        // move down
        if itemsView.isHidden == true{
            itemsView.isHidden = false
            UIView.animate(withDuration: 0.5, animations: {
                self.view.layoutIfNeeded()
            })
        }
    }
    
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
}

//MARK: - IBACtions
private extension AddFeedVC{
    
    @IBAction func closeVC (_ sender: Any) {
        //objAppShareData.isCloseAddFeed = true
        avPlayer.pause()
        self.viewVideo.layer.sublayers?.forEach { $0.removeFromSuperlayer() }
        self.dismiss(animated: false) {
        }
        objAppShareData.objAppdelegate.gotoTabBar(withAnitmation: false)
    }
    
    @IBAction func selectMultipleAssetAction (_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected{
            isMultipleAsset = true
            btnCrop.isHidden = true
            viewScrollImage.isUserInteractionEnabled = false
            /////
            self.mediaCollection.reloadData()
            self.arrSelectedAsset.removeAll()
            
            ////
            if objAppShareData.isImageCrop {
                objAppShareData.isImageCrop = false
                self.btnCrop.setImage(#imageLiteral(resourceName: "crop_img_ico"), for: .selected)
                viewScrollImage.zoom()
            }
            
            let objAsset = arrAllAsset[self.currentIndexPath.row]
            objAsset.imgForPost = self.viewScrollImage.imageView.image!
            let imgNew = self.captureVisibleRect()
            objAsset.imgForPost = imgNew
            self.arrSelectedAsset.append(objAsset)
            
            btnMultiSelection.setImage(#imageLiteral(resourceName: "active_fixed_img_ico"), for: .normal)
            
            // sender.backgroundColor = UIColor.theameColors.pinkColor
        }else{
            isMultipleAsset = false
            btnCrop.isHidden = false
            viewScrollImage.isUserInteractionEnabled = true
            arrSelectedAsset.removeAll()
            
            self.mediaCollection.reloadData()
            let objAsset = arrAllAsset[self.currentIndexPath.row]
            self.arrSelectedAsset.append(objAsset)
            btnMultiSelection.setImage(#imageLiteral(resourceName: "fixed_img_ico"), for: .normal)
            //sender.backgroundColor = unSelectedColor
        }
    }
    
    @IBAction func cropImageAction (_ sender:UIButton){
        sender.isSelected = !sender.isSelected
        if sender.isSelected{
            //sender.backgroundColor = UIColor.theameColors.pinkColor
            objAppShareData.isImageCrop = true
            self.btnCrop.setImage(#imageLiteral(resourceName: "active_crop_ico"), for: .selected)
        }else{
            objAppShareData.isImageCrop = false
            // sender.backgroundColor = unSelectedColor
            self.btnCrop.setImage(#imageLiteral(resourceName: "crop_img_ico"), for: .selected)
            
        }
        viewScrollImage.zoom()
    }
    
    @IBAction func selectImages(_ sender:UIButton){
        objWebserviceManager.StartIndicator()
        self.arrSelectedAsset.removeAll()
        unSelectMethod()
        
        self.isVideo = false
        self.btnMultiSelection.isHidden = false
        self.btnCrop.isHidden = false
        self.GetAllMediaFromDevice(withPage: 0)
        self.btnImages.isSelected = true
        self.btnVideo.isSelected = false
        self.btnCamera.isSelected = false
    }
    
    @IBAction func selectVideo(_ sender:UIButton){
        objWebserviceManager.StartIndicator()
        self.isVideo = true
        self.btnMultiSelection.isHidden = true
        self.btnCrop.isHidden = true
        isMultipleAsset = false
        self.GetAllMediaFromDevice(withPage: 0)
        self.btnImages.isSelected = false
        self.btnVideo.isSelected = true
        self.btnCamera.isSelected = false
    }
    
    @IBAction func selectCamera(_ sender:UIButton){
        avPlayer.pause()
        self.btnImages.isSelected = false
        self.btnVideo.isSelected = false
        self.btnCamera.isSelected = true
        
        /*
         // picker with camera
         let picker = UIImagePickerController()
         picker.delegate = self
         if UIImagePickerController.isSourceTypeAvailable(.camera) {
         picker.allowsEditing = true
         picker.sourceType = .camera
         picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .camera)!
         picker.videoQuality = .typeMedium
         picker.videoMaximumDuration = 60
         picker.showsCameraControls = true
         picker.isNavigationBarHidden = false
         picker.isToolbarHidden = true
         self.present(picker, animated: true) //{ _ in }
         }
         */
        
        let objCamera = UIStoryboard.init(name: "ArtistProfile", bundle: Bundle.main).instantiateViewController(withIdentifier: "CameraVC") as? CameraVC
        objAppShareData.isCameraForFeed = true
        //objCamera?.delegate = self
        objCamera!.modalPresentationStyle = .fullScreen
        self.present(objCamera!, animated: false, completion: nil)
        
        ////
        self.btnImages.isSelected = true
        self.btnVideo.isSelected = false
        self.btnCamera.isSelected = false
        let btn = UIButton()
        self.selectImages(btn)
        ////
    }
    
    @IBAction func nextAction(_ sender: UIButton){
        UserDefaults.standard.setValue("1", forKey: "postType")
        UserDefaults.standard.synchronize()
        if objAppShareData.isFileIsUploding{
            showAlertVC(title: kAlertTitle, message:"Uploading is already in progress", controller: self)
            return
        }
        let sb = UIStoryboard(name: "Add", bundle: Bundle.main)
        let objNewPost = sb.instantiateViewController(withIdentifier:"NewPostVC") as! NewPostVC
        
        objNewPost.isVideo = isVideo
        objNewPost.videoURL = nil
        
        if isVideo{
            avPlayer.pause()
            ////
            if mp4VideoURL == nil{
                return
            }
            ////
            let sourceAsset = AVURLAsset(url:mp4VideoURL!, options: nil)
            let duration: CMTime = sourceAsset.duration
            let sec = CMTimeGetSeconds(duration)
            
            if sec <= 60.5{
                
                objNewPost.videoURL = mp4VideoURL
                objNewPost.imgFeed = self.generateImage(mp4VideoURL!)
                objNewPost.imgThumb = self.generateImage(mp4VideoURL!)
                navigationController?.pushViewController(objNewPost, animated: true)
            }else{
                
                if UIVideoEditorController.canEditVideo(atPath: (self.mp4VideoURL?.path)!){
                    
                    let editController = UIVideoEditorController()
                    editController.delegate = self
                    editController.videoPath = (self.mp4VideoURL?.path)!
                    editController.videoMaximumDuration = 60
                    editController.videoQuality = .typeMedium
                    editController.modalPresentationStyle = .fullScreen
                    present(editController, animated:true)
                }else{
                    showAlertVC(title: kAlertTitle, message:"This video can not be uploaded", controller: self)
                }
            }
            
        }else{
            if arrSelectedAsset.count <= 1{
                isMultipleAsset = false
            }
            
            if isMultipleAsset{
                self.arrImages.removeAll()
                for objAsset in arrSelectedAsset {
                    getURL(ofMediaWith: objAsset.asset, completionHandler: { url in
                        do {
                            if url == nil{
                                return
                            }
                            let imageData = try Data.init(contentsOf: url!)
                            let img = UIImage(data: imageData)
                            
                            if self.arrImages.count == 0{
                                if !self.arrImages.contains(objAsset.imgForPost){
                                    self.arrImages.append(objAsset.imgForPost)
                                }
                            }else{
                                if img!.size.width > 1000{
                                    let i = self.resizeImage(image: img!, newWidth: 1000)
                                    if !self.arrImages.contains(i){
                                        self.arrImages.append(i)
                                    }
                                }else{
                                    if !self.arrImages.contains(img!){
                                        self.arrImages.append(img!)
                                    }
                                }
                            }
                            
                            objNewPost.imgFeed = self.arrImages.first//img!
                            objNewPost.imgThumb = self.arrImages.first//img!
                            if self.arrImages.count == self.arrSelectedAsset.count{
                                objNewPost.arrFeedImages = self.arrImages
                                self.navigationController?.pushViewController(objNewPost, animated: true)
                            }
                        } catch {
                            
                        }
                    })
                }
            }else{
                self.arrImages.removeAll()
                let img = captureVisibleRect()
                self.arrImages.append(img)
                
                ////
                if img.size.height > (img.size.width+20) || img.size.width > (img.size.height+20){
                    objAppShareData.isImageCrop = true
                }
                ////
                
                objNewPost.imgFeed = self.arrImages[0]//self.arrImages.first//img
                
                objNewPost.imgThumb = self.arrImages[0]//self.arrImages.first//img
                objNewPost.arrFeedImages = self.arrImages
                
                ////
                UserDefaults.standard.setValue("0", forKey: "postType")
                UserDefaults.standard.synchronize()
                ////
                if !objAppShareData.isImageCrop && self.arrImages.count == 1{
                    imageRatioDynamic = 1
                }else{
                    
                }
                navigationController?.pushViewController(objNewPost, animated: true)
            }
            
            //            if self.arrImages.count>0{
            //                self.arrImages.remove(at: 0)
            //                self.arrImages.insert(self.viewScrollImage.imageView.image!, at: 0)
            //            }
        }
    }
}

// MARK: - collection view delegate and data source
extension AddFeedVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrAllAsset.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        
        let cell = mediaCollection.dequeueReusableCell(withReuseIdentifier: "postCollectionCell", for:
            indexPath) as! postCollectionCell
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath){
        let displayCell = cell as! postCollectionCell
        if arrAllAsset.count>0{
            let objAsset = arrAllAsset[indexPath.row]
            if arrSelectedAsset.contains(objAsset){
                if isMultipleAsset{
                    displayCell.showLibraryData(withAssets: objAsset.asset, withPreviewState: objAsset.isSelectedPreview, andSelectState: true)
                    
                }else{
                    displayCell.showLibraryData(withAssets: objAsset.asset, withPreviewState: objAsset.isSelectedPreview, andSelectState: false)
                }
            }else{
                displayCell.showLibraryData(withAssets: objAsset.asset, withPreviewState: objAsset.isSelectedPreview, andSelectState: false)
            }
            
            /*if arrAllAsset.count >= 30{
             if indexPath.row >= arrAllAsset.count - 8{
             page = page+1
             GetAllMediaFromDevice(withPage:page)
             }
             }*/
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        
        //        if isMultipleAsset{
        //            if self.arrSelectedAsset.count == 1{
        //                return
        //            }
        //        }
        
        let objAsset = arrAllAsset[indexPath.row]
        let previousObjAsset = arrAllAsset[self.currentIndexPath.row]
        oldIndexPath = self.currentIndexPath
        previousObjAsset.isSelectedPreview = false
        objAsset.isSelectedPreview = true
        
        //        if arrSelectedAsset.count >= 10{
        //        }else{
        //            let cell = collectionView.cellForItem(at: indexPath) as! postCollectionCell
        //            imgThumbICLoud = cell.imgICloud
        //            addAssetObjectToView(withSelectedAsset:objAsset.asset)
        //        }
        
        let visibleIndexes = collectionView.indexPathsForVisibleItems
        for Indexes in visibleIndexes{
            if Indexes == self.currentIndexPath{
                let previousSelectedcell = collectionView.cellForItem(at: self.currentIndexPath) as! postCollectionCell
                previousSelectedcell.selectedView.isHidden = true
            }
        }
        let selectedCell = collectionView.cellForItem(at: indexPath) as! postCollectionCell
        currentIndexPath = indexPath
        selectedCell.selectedView.isHidden = false
        
        if isMultipleAsset{
            selectMultipleAssets(withAssetModel: objAsset, andSelectedCell:selectedCell)
        }else{
            let cell = collectionView.cellForItem(at: indexPath) as! postCollectionCell
            imgThumbICLoud = cell.imgICloud
            addAssetObjectToView(withSelectedAsset:objAsset.asset)
            arrSelectedAsset.removeAll()
            arrSelectedAsset.append(objAsset)
        }
        ////
        if isMultipleAsset{
        }else{
            self.dataScrollView.contentOffset.y = 0
            itemsView.isHidden = false
            UIView.animate(withDuration: 0.5, animations: {
                self.view.layoutIfNeeded()
            })
        }
        ////
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let cellDimension = CGFloat((mediaCollection.frame.size.width/4)-1)
        return CGSize(width: cellDimension, height:cellDimension)
        // your code here
    }
    
}

extension AddFeedVC:UIScrollViewDelegate{
    
    func scrollViewDidScroll(_ scrollView: UIScrollView){
        self.view.endEditing(true)
        if (self.lastContentOffset > scrollView.contentOffset.y) {
            // move up
            if itemsView.isHidden == true{
                itemsView.isHidden = false
                UIView.animate(withDuration: 0.5, animations: {
                    self.view.layoutIfNeeded()
                })
            }
            
        }else if (self.lastContentOffset < scrollView.contentOffset.y){
            // move down
            if itemsView.isHidden == false{
                UIView.animate(withDuration: 0.5, animations: {
                    self.itemsView.isHidden = true
                })
            }
        }
        self.lastContentOffset = scrollView.contentOffset.y
    }
    
}

extension AddFeedVC: UIVideoEditorControllerDelegate {
    
    func videoEditorController(_ editor: UIVideoEditorController,
                               didSaveEditedVideoToPath editedVideoPath: String) {
        if !editor.isBeingDismissed {
            if let url = URL(string: "file://"+editedVideoPath){
                let sb = UIStoryboard(name: "Add", bundle: Bundle.main)
                let objNewPost = sb.instantiateViewController(withIdentifier:"NewPostVC") as! NewPostVC
                objNewPost.isVideo = true
                objNewPost.videoURL = url
                objNewPost.imgFeed = self.generateImage(url)
                objNewPost.imgThumb = self.generateImage(url)
                
                dismiss(animated: false) {
                    self.navigationController?.pushViewController(objNewPost, animated: true)
                }
            }
        }
    }
    
    func videoEditorControllerDidCancel(_ editor: UIVideoEditorController) {
        editor.dismiss(animated:true)
    }
    
    func videoEditorController(_ editor: UIVideoEditorController,
                               didFailWithError error: Error) {
        //print("an error occurred: \(error.localizedDescription)")
    }
}

extension AddFeedVC:UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    // MARK: - UIImagePicker controller delegate
    
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let url = info[UIImagePickerController.InfoKey.mediaURL] as? URL{
            
            mp4VideoURL = url
            let sb = UIStoryboard(name: "Add", bundle: Bundle.main)
            let objNewPost = sb.instantiateViewController(withIdentifier:"NewPostVC") as! NewPostVC
            objNewPost.isVideo = true
            objNewPost.videoURL = self.mp4VideoURL
            objNewPost.imgFeed = self.generateImage(self.mp4VideoURL!)
            objNewPost.imgThumb = self.generateImage(self.mp4VideoURL!)
            dismiss(animated: false) {
               objActivity.stopActivity()
                self.navigationController?.pushViewController(objNewPost, animated: true)
            }
            
        }else{
            self.arrSelectedAsset.removeAll()
            if let img = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
                
                if img.size.width > 1000{
                    let i = self.resizeImage(image: img, newWidth: 1000)
                    self.arrImages.append(i)
                }else{
                    self.arrImages.append(img)
                }
                let sb = UIStoryboard(name: "Add", bundle: Bundle.main)
                let objNewPost = sb.instantiateViewController(withIdentifier:"NewPostVC") as! NewPostVC
                objNewPost.isVideo = false
                objNewPost.videoURL = nil
                objNewPost.imgFeed = self.arrImages.first//img
                objNewPost.imgThumb = self.arrImages.first//img
                objNewPost.arrFeedImages = self.arrImages
                dismiss(animated: false) {
                    objActivity.stopActivity()
                    self.navigationController?.pushViewController(objNewPost, animated: true)
                }
            }
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true) //{ _ in }
    }
}
//MARK: - writing file
fileprivate extension AddFeedVC{
    
    func deleteFile(filePath:NSURL) {
        guard FileManager.default.fileExists(atPath: filePath.path!) else {
            return
        }
        do {
            try FileManager.default.removeItem(atPath: filePath.path!)
        }catch{
            fatalError("Unable to delete file: \(error) : \(#function).")
        }
    }
    
}


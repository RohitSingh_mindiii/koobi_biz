//
//  showImagesVC.swift
//  Mualab
//
//  Created by Mac on 10/11/2017 .
//  Copyright © 2017 MINDIII. All rights reserved.
//

import UIKit


class showFullimagesVC: UIViewController, UIScrollViewDelegate {
    
    //amit
    private(set) var tagsHidden = true
    var objFeeds:feeds?
    //amit
    
    fileprivate var scrollSize: CGFloat = 300
    var strHeaderName = ""
    var arrFeedImages = [Any]()
    @IBOutlet weak var scrollView : UIScrollView!
    @IBOutlet weak var imgToScroll : UIImageView!
    @IBOutlet weak var lblHeader : UILabel!
    
    @IBOutlet weak var pageView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UserDefaults.standard.setValue("no", forKey: "isTakeTagTouch")
        UserDefaults.standard.synchronize()
        if strHeaderName != "" {
            self.lblHeader.text = strHeaderName
        }
        
        self.scrollView.backgroundColor = UIColor.white
        
        let deadlineTime = DispatchTime.now() + .seconds(1/4)
        DispatchQueue.main.asyncAfter(deadline: deadlineTime){
            self.scrollSize = self.view.frame.size.width
            self.scrollSize = self.view.frame.size.height
        }
        if arrFeedImages.count>0{
        //let obj = arrFeedImages[0]
            let obj = arrFeedImages[0] as? feedData
            let url = URL.init(string: obj?.feedPost ?? "")
            //self.imgToScroll.af_setImage(withURL:url!)
            self.imgToScroll.sd_setImage(with: url!, placeholderImage: #imageLiteral(resourceName: "cellBackground"))
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
 
    override func viewDidDisappear(_ animated: Bool) {
        UserDefaults.standard.setValue("yes", forKey: "isTakeTagTouch")
        UserDefaults.standard.synchronize()
    }
    
    //MARK: - ScrollView Delegate
    internal func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.view.endEditing(true)
        if scrollView == self.scrollView{
        }
    }
    
    // MARK: - IBActions
    @IBAction func btnBackAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}


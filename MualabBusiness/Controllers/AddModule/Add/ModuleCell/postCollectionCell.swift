//
//  postCollectionCell.swift
//  Mualab
//
//  Created by MINDIII on 10/27/17.
//  Copyright © 2017 MINDIII. All rights reserved.
//

import UIKit
import Photos
import HCSStarRatingView

//class CellAddRemoveGroupMember: UICollectionViewCell {
//    @IBOutlet weak var viewCellBG: UIView!
//    @IBOutlet weak var imgMember: UIImageView!
//    @IBOutlet weak var btnDeleteMember: UIButton!
//    @IBOutlet weak var lblMemberName: UILabel!
//}

class CellTop: UITableViewCell {
    
    @IBOutlet weak var imgVwOuter: UIImageView!
    @IBOutlet weak var imgVwProfile: UIImageView!
    @IBOutlet weak var imgCheckUnCheck: UIImageView!
    @IBOutlet weak var btnCheck: UIButton!
    @IBOutlet weak var lblPrice: UILabel!

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblPostDetails : UILabel!
    
    var indexPath:IndexPath!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}

class postCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var imgThumb: UIImageView!
    @IBOutlet weak var imgThumbVideo: UIImageView!
    @IBOutlet weak var imgSelected: UIImageView!
    
    @IBOutlet weak var selectedView: UIView!
    @IBOutlet weak var btnPlay: UIButton!
    @IBOutlet weak var lblTime: UILabel!
    var imgICloud : UIImage?
}

extension postCollectionCell{
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    // Bind Photo Library
    func showLibraryData(withAssets asset:PHAsset,withPreviewState state:Bool, andSelectState selectState:Bool) -> Void {
        self.imgThumb.image = getAssetThumbnail(asset: asset)
        if state{
            selectedView.isHidden = false
        }else{
            selectedView.isHidden = true
        }
        if selectState{
            imgSelected.isHidden = false
        }else{
            imgSelected.isHidden = true
        }
        switch asset.mediaType {
        case .image:
            self.lblTime.text = ""
            
        case .video:
            let duration = asset.duration
            var time = duration
            if time >= 60 {
                time = duration / 60
            }else{
                time = duration / 100
            }
            self.lblTime.text = String(format: "%.2f",time)
        default:
            break
        }
    }
}

private extension postCollectionCell {
    
    func getAssetThumbnail(asset: PHAsset) -> UIImage {
        let manager = PHImageManager.default()
        let option = PHImageRequestOptions()
        var thumbnail = UIImage()
        option.isSynchronous = true
        //option.resizeMode = PHImageRequestOptionsResizeMode.exact
        //option.deliveryMode = PHImageRequestOptionsDeliveryMode.opportunistic
        let dimension = self.frame.size.width+20
        //let dimension = 400
        manager.requestImage(for: asset, targetSize: CGSize(width:dimension, height:dimension), contentMode: .default, options: option, resultHandler: {(result, info)->Void in
            if result != nil{
                thumbnail = result!
                self.imgICloud = result!
            }
        })
        
        return thumbnail
    }
}

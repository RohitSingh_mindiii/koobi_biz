//
//  ChatHistoryVC.swift
//  MualabCustomer
//
//  Created by Mindiii on 7/20/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import Firebase
import Alamofire
import AlamofireImage

class JoinGroupVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {
    
    @IBOutlet weak var viewPluseMenu: UIView!
    @IBOutlet weak var viewFiltereMenu: UIView!
    @IBOutlet weak var imgFaviourite: UIImageView!
    
    @IBOutlet weak var tblChatHistory: UITableView!
    @IBOutlet weak var txtSearchUser: UITextField!
    @IBOutlet weak var lblNoDataFound: UIView!
    var strText = ""

    var ref: DatabaseReference!
    var messages: [DataSnapshot]! = []
    fileprivate var _refHandle: DatabaseHandle?

    fileprivate var strMyChatId:String = ""
    fileprivate var arrChatHistory = [GroupModal]()
    fileprivate var arrChatHistoryTextFilter = [GroupModal]()
    fileprivate var strGroupIdForCompare = ""
    fileprivate var strGroupIdForPendingStatus = ""
    fileprivate var isFaviourite:Bool = false
    fileprivate var isFirstTime:Bool = false
    
    let dateFormatter : DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd hh:mm a"
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        return dateFormatter
    }()
}

//MARK: - System Method extension
extension JoinGroupVC{
    
    override func viewDidLoad() {
        super.viewDidLoad()
         strMyChatId = UserDefaults.standard.string(forKey: UserDefaults.keys.myId) ?? ""
        self.viewConfigure()
        self.lblNoDataFound.isHidden = true
        //objWebserviceManager.StartIndicator()
        self.getMyGroupAndRequestData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        objWebserviceManager.StartIndicator()
        strMyChatId = UserDefaults.standard.string(forKey: UserDefaults.keys.myId) ?? ""
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

//MARK: - Custome method extension
fileprivate extension JoinGroupVC{
    func viewConfigure(){
        self.txtSearchUser.delegate = self
        self.tblChatHistory.delegate = self
        self.tblChatHistory.dataSource = self
        ref = Database.database().reference()
    }
    func getMyGroupAndRequestData(){
        _refHandle = self.ref.child("myGroup").child(self.strMyChatId).observe(.value, with: { [weak self] (snapshot) -> Void in
            guard let strongSelf = self else {
                return
            }
            print(snapshot)
            print("\n\n\(String(describing: snapshot.key))")
            print("\n\n\(snapshot.value)")
            if let dict = snapshot.value as? NSDictionary{
            for (key, element) in dict {
                self!.strGroupIdForCompare = self!.strGroupIdForCompare + "," + String(describing: key)
            }
            }
            
            self?._refHandle = self?.ref.child("my_group_request").child(self?.strMyChatId ?? "").observe(.value, with: { [weak self] (snapshot) -> Void in
                guard let strongSelf = self else {
                    return
                }
                print(snapshot)
                print("\n\n\(String(describing: snapshot.key))")
                print("\n\n\(snapshot.value)")
                if let dict = snapshot.value as? NSDictionary{
                    for (key, element) in dict {
                        //self!.strGroupIdForCompare = self!.strGroupIdForCompare + "," + String(describing: key)
                        self!.strGroupIdForPendingStatus = self!.strGroupIdForPendingStatus + "," + String(describing: key)
                    }
                }
                print(self!.strGroupIdForCompare)
              self?.GetChatHistoryFromFirebase(strFilterType: "")

            })
        })
    }
}

//MARK: - Button extension
fileprivate extension JoinGroupVC{
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.view.endEditing(true)
    self.navigationController?.popViewController(animated: true)
    }
   
    @IBAction func btnJoinAction(_ sender: UIButton) {
        self.view.endEditing(true)
        print(sender.tag)
        var objGroup = self.arrChatHistory[0]
        if self.strText.count != 0{
           objGroup = self.arrChatHistoryTextFilter[sender.tag]
        }else{
          objGroup = self.arrChatHistory[sender.tag]
        }
        
        let calendarDate = ServerValue.timestamp()//timeInMiliSeconds()
        let dict = [
           "groupId":checkForNULL(obj:objGroup.strGroupId),
            "senderId":checkForNULL(obj:self.strMyChatId),
            "timestamp":calendarDate]
            let ref = Database.database().reference()
        ref.child("group_request").child(objGroup.strAdminId).childByAutoId().setValue(dict)
            { (error, snap) in
              if error == nil{
                let dictNew = [
                    objGroup.strGroupId:objGroup.strGroupId
                ]
                ref.child("my_group_request").child(self.strMyChatId).updateChildValues(dictNew){ (error, snap) in
                    if error == nil{
                        self.GetChatHistoryFromFirebase(strFilterType: "")
                    }
                }
              }
            }
    }
}

//MARK: - tableview method extension
extension JoinGroupVC{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.strText.count != 0{
            return arrChatHistoryTextFilter.count
        }else{
            return arrChatHistory.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "CellChatHistory", for: indexPath) as? CellChatHistory{
            var objChatList = self.arrChatHistory[indexPath.row]
            if self.strText.count == 0{
                objChatList = arrChatHistory[indexPath.row]
            }else{
                objChatList = arrChatHistoryTextFilter[indexPath.row]
            }
            let url = URL(string: objChatList.strGroupImage)
            //cell.imgUserProfile.af_setImage(withURL: url!)
            cell.imgUserProfile.sd_setImage(with: url!, placeholderImage: UIImage(named: "cellBackground"))
            cell.lblMsg.text = objChatList.strGroupDescription
            cell.lblUserName.text = objChatList.strGroupName
            cell.lblUserType.text = objChatList.strMemberCount + " Members"
            cell.btnJoin.tag = indexPath.row
            if objChatList.strStatusForJoin == "pending"{
                cell.btnJoin.backgroundColor = UIColor.darkGray
                cell.btnJoin.setTitle("Pending", for: .normal)
                cell.btnJoin.isUserInteractionEnabled = false
            }else{
                cell.btnJoin.backgroundColor = UIColor.theameColors.skyBlueNewTheam
                cell.btnJoin.setTitle("Join", for: .normal)
                cell.btnJoin.isUserInteractionEnabled = true
            }
            return cell
        }else{
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}

//MARK: - FireBase Method calls
extension JoinGroupVC{
    
    func GetChatHistoryFromFirebase(strFilterType:String) {
        //objWebserviceManager.StartIndicator()
        _refHandle = self.ref.child("group").observe(.value, with: { [weak self] (snapshot) -> Void in
            
            guard let strongSelf = self else {
                objWebserviceManager.StopIndicator()
                self?.lblNoDataFound.isHidden = false
                return
            }
            print(snapshot)
         
            print("\n\n\(String(describing: snapshot.key))")
            print("\n\n\(snapshot.value)")
            self?.arrChatHistory.removeAll()
            
            if let dict = snapshot.value as? NSDictionary {
                strongSelf.parseDataChatHistory(filterType: strFilterType, dictHistory: dict)
                objWebserviceManager.StopIndicator()
            } else {
                objWebserviceManager.StopIndicator()
                self?.lblNoDataFound.isHidden = false
                strongSelf.arrChatHistory.removeAll()
                strongSelf.tblChatHistory.reloadData()
            }
        })
    }
    
    func tableViewScrollToBottom(animated: Bool) {
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(100)) {
            //DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(0)) {
            let numberOfSections = self.tblChatHistory.numberOfSections
            let numberOfRows = self.tblChatHistory.numberOfRows(inSection: numberOfSections-1)
            if numberOfRows > 0 {
                //let indexPath = IndexPath(row: numberOfRows-1, section: (numberOfSections-1))
                let indexPath = IndexPath(row: 0, section: (0))
                self.tblChatHistory.scrollToRow(at: indexPath, at: .bottom, animated: animated)
                self.tblChatHistory.isHidden = false
            }
        }
    }
    func parseDataChatHistory(filterType:String,dictHistory:NSDictionary){
        //// myGroup , my_group_request
        
        self.arrChatHistory.removeAll()
        for (key, element) in dictHistory {
            print("key = \(key)")
            print("element = \(element)")
            if let dict = element as? [String:Any]{
                let objChatList = GroupModal()
                objChatList.strGroupId = String(describing: key)
                if let strId = dict["adminId"] as? String {
                   objChatList.strAdminId = strId
                }
                if let id = dict["adminId"] as? Int {
                    objChatList.strAdminId = String(id)
                }
                objChatList.strGroupName = dict["groupName"] as? String ?? ""
                objChatList.strGroupImage = dict["groupImg"] as? String ?? ""
                objChatList.strGroupDescription = dict["groupDescription"] as? String ?? ""
                let dictMember = dict["member"] as? [String:Any] ?? [:]
                objChatList.strMemberCount = String(dictMember.count)
                if !strGroupIdForCompare.contains(objChatList.strGroupId){
                    if strGroupIdForPendingStatus.contains(objChatList.strGroupId){
                        objChatList.strStatusForJoin = "pending"
                    }else{
                        objChatList.strStatusForJoin = ""
                    }
                    arrChatHistory.append(objChatList)
                }
            }
        }
        objWebserviceManager.StopIndicator()
        
        if arrChatHistory.count > 0 {
            self.lblNoDataFound.isHidden = true
        }else{
            self.lblNoDataFound.isHidden = false
        }
        print("self.arrChatHistory.count = \(self.arrChatHistory.count)")
        //self.filterDataFromMainArrayToLocalArray()
        self.tblChatHistory.reloadData()
        self.tableViewScrollToBottom(animated: true)
        objWebserviceManager.StopIndicator()
        
        if (self.arrChatHistory.count == 0) {
            self.lblNoDataFound.isHidden = false;
            self.tblChatHistory.isHidden = true;
        } else {
            self.lblNoDataFound.isHidden = true;
            self.tblChatHistory.isHidden = false;
        }
        if self.strText.count > 0{
            var substring = self.strText
            let filteredArray = self.arrChatHistory.filter(){ $0.strGroupName.localizedCaseInsensitiveContains(substring) }
            NSLog("HERE %@", filteredArray)
            strText = substring
            arrChatHistoryTextFilter = filteredArray
            if arrChatHistoryTextFilter.count == 0 && strText.count != 0{
                self.lblNoDataFound.isHidden = false
                self.tblChatHistory.isHidden = true
            }else{
                self.lblNoDataFound.isHidden = true
                self.tblChatHistory.isHidden = false
            }
            self.tblChatHistory.reloadData()
        }
    }
    
 }

extension JoinGroupVC{
    
    internal func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.txtSearchUser{
            txtSearchUser.resignFirstResponder()
        }
        return true
    }
    
internal func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    let arrAny:[Any] = self.arrChatHistory
    arrChatHistoryTextFilter.removeAll()
    let text = textField.text! as NSString
        var substring: String = textField.text!
        substring = (substring as NSString).replacingCharacters(in: range, with: string)
        let filteredArray = self.arrChatHistory.filter(){ $0.strGroupName.localizedCaseInsensitiveContains(substring) }
         NSLog("HERE %@", filteredArray)
        strText = substring
        arrChatHistoryTextFilter = filteredArray
        if arrChatHistoryTextFilter.count == 0 && strText.count != 0{
           self.lblNoDataFound.isHidden = false
           self.tblChatHistory.isHidden = true
        }else{
            self.lblNoDataFound.isHidden = true
            self.tblChatHistory.isHidden = false
        }
        self.tblChatHistory.reloadData()
        return  true
}
    
    
    
   /*
   func filterDataFromMainArrayToLocalArray(){
    if strText.count != 0{
        let filteredArray = self.arrChatHistory.filter(){ $0.strOpponentName.localizedCaseInsensitiveContains(self.txtSearchUser.text!) }
        NSLog("HERE %@", filteredArray)
        strText = self.txtSearchUser.text!
        arrChatHistoryTextFilter = filteredArray
        if arrChatHistoryTextFilter.count == 0 && strText.count != 0{
            self.lblNoDataFound.isHidden = false
            self.tblChatHistory.isHidden = true
        }else{
            self.lblNoDataFound.isHidden = true
            self.tblChatHistory.isHidden = false
        }
    }
    }*/
}

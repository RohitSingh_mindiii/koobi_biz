//
//  MemberRequestList.swift
//  MualabCustomer
//
//  Created by Mindiii on 7/21/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

class MemberRequestList: UIViewController,UITableViewDataSource,UITableViewDelegate {
    @IBOutlet weak var tblAddMember:UITableView!
    @IBOutlet weak var btnChatRequest:UIButton!
    @IBOutlet weak var btnGroupRequest:UIButton!
    @IBOutlet weak var lblBotumeChatRequest:UILabel!
    @IBOutlet weak var lblBotumeGroupRequest:UILabel!
 }

//MARK: - System Method extension
extension MemberRequestList{
    
        override func viewDidLoad() {
            super.viewDidLoad()
            self.viewConfigure()
            // Do any additional setup after loading the view.
        }
        
        override func viewWillAppear(_ animated: Bool) {
        }
        
        override func viewWillDisappear(_ animated: Bool) {
             //self.clearUnreadChatHistory()
        }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
    }
    
    
//MARK: - Custome method extension
fileprivate extension MemberRequestList{
    func viewConfigure(){
            self.tblAddMember.delegate = self
            self.tblAddMember.dataSource = self
            self.tblAddMember.reloadData()
    }
    
    
    func selectChatRequest(){
        self.lblBotumeChatRequest.backgroundColor = #colorLiteral(red: 0.1794688959, green: 0.7797561331, blue: 0.7434092337, alpha: 1)
        self.lblBotumeGroupRequest.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        self.btnChatRequest.setTitleColor(#colorLiteral(red: 0.1794688959, green: 0.7797561331, blue: 0.7434092337, alpha: 1), for: .normal)
        self.btnGroupRequest.setTitleColor(#colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1), for: .normal)

        self.tblAddMember.reloadData()
    }
    
    func selectGroupRequest(){
        self.lblBotumeChatRequest.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        self.lblBotumeGroupRequest.backgroundColor = #colorLiteral(red: 0.1794688959, green: 0.7797561331, blue: 0.7434092337, alpha: 1)
        self.btnChatRequest.setTitleColor(#colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1), for: .normal)
        self.btnGroupRequest.setTitleColor(#colorLiteral(red: 0.1794688959, green: 0.7797561331, blue: 0.7434092337, alpha: 1), for: .normal)

        self.tblAddMember.reloadData()
    }
}
    
//MARK: - Button extension
fileprivate extension MemberRequestList{
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.view.endEditing(true)
       self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnGroupRequestAction(_ sender: UIButton){
        self.view.endEditing(true)
        self.selectGroupRequest()
    }
    
    @IBAction func btnChatRequestAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.selectChatRequest()
    }
}
    
    
//MARK: - tableview method extension
extension MemberRequestList{
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 20
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "CellChatHistory", for: indexPath) as? CellChatHistory{
                
                  return cell
             }
            return UITableViewCell()
        }
        
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            
            let sb: UIStoryboard = UIStoryboard(name: "Chat", bundle: Bundle.main)
            if let objVC = sb.instantiateViewController(withIdentifier:"NewMessageListVC") as? NewMessageListVC{
                navigationController?.pushViewController(objVC, animated: true)
            }
         }
}

//
//  BroadCastDetailsVC.swift
//  MualabCustomer
//
//  Created by Mindiii on 9/5/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//


import UIKit
import Firebase
import Alamofire
import AlamofireImage

class BroadCastDetailsVC:UIViewController,UITableViewDataSource,UITableViewDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate,UITextFieldDelegate,UITextViewDelegate{
        
        var objChatHistoryModel = ChatHistoryData()
        
        @IBOutlet weak var tblCreateGroupList:UITableView!
        @IBOutlet weak var lblNoDataFound:UIView!
        @IBOutlet weak var txtEnterGroupName: UILabel!
        @IBOutlet weak var txtEnterGroupDescription: UILabel!
        @IBOutlet weak var imgGroupProfile:UIImageView!
        
    
        @IBOutlet weak var lblClearChatHistory: UILabel!
        @IBOutlet weak var lblMemberCount: UILabel!
        
        @IBOutlet weak var viewDeletePopup: UIView!
    @IBOutlet weak var viewBroadcastDeletePopup: UIView!

        //Edit Group Outlet
        
        @IBOutlet weak var height: NSLayoutConstraint!
    
        ///////
        var imagePicker = UIImagePickerController()
        
        var ref: DatabaseReference!
        var messages: [DataSnapshot]! = []
        fileprivate var _refHandle: DatabaseHandle?
        fileprivate var storageRef: StorageReference!
        
        fileprivate var strMyChatId:String = ""
        fileprivate var arrChatHistory = [ChatHistoryData]()
        fileprivate var arrChatHistoryMyData = [ChatHistoryData]()
        fileprivate var newGroupName = ""
        fileprivate var imgUrl = ""
        fileprivate var fromImage = false
        fileprivate var strAdminId = ""
    }
    

    
    
    //MARK: - System Method extension
    extension BroadCastDetailsVC{
        override func viewDidLoad() {
            super.viewDidLoad()
            self.viewDeletePopup.isHidden = true
            self.viewBroadcastDeletePopup.isHidden = true
           // self.imgGroupProfile.image = #imageLiteral(resourceName: "logo_2")

            ref =  Database.database().reference()
            storageRef = Storage.storage().reference().child("gs://koobdevelopment.appspot.com")
            
            self.strMyChatId = UserDefaults.standard.string(forKey: UserDefaults.keys.myId) ?? ""
             self.viewConfigure()
             self.GetUserListFromFirebase()
        }
        
        override func viewWillAppear(_ animated: Bool) {
            self.viewDeletePopup.isHidden = true
            self.viewBroadcastDeletePopup.isHidden = true
            if fromImage == false{
                self.GetUserListFromFirebase()
            }else{
                objWebserviceManager.StartIndicator()
            }
            fromImage = false
            
        }
        
        override func viewWillDisappear(_ animated: Bool) {
        }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
        }
    }
    
    //MARK: - Custome method extension
    fileprivate extension BroadCastDetailsVC{
        func viewConfigure(){
            self.tblCreateGroupList.delegate = self
            self.tblCreateGroupList.dataSource = self
             self.imagePicker.delegate = self
            ref = Database.database().reference()
        }
    }
    
    //MARK: - Button extension
    fileprivate extension BroadCastDetailsVC{
        @IBAction func btnBackAction(_ sender: UIButton) {
            self.view.endEditing(true)
            self.dismiss(animated: true, completion: nil)
        }
        
       
        
        @IBAction func btnLeaveGroupAction(_ sender: UIButton){
            self.view.endEditing(true)
            self.viewDeletePopup.isHidden = true
            self.viewBroadcastDeletePopup.isHidden = false
        }
        
        @IBAction func btnBroadcastDeleteYes(_ sender: UIButton) {
            self.leaveGroup()
            self.viewDeletePopup.isHidden = true
            self.viewBroadcastDeletePopup.isHidden = true
            
        }
        
        @IBAction func btnBroadcastDeleteNo(_ sender: UIButton) {
            self.viewDeletePopup.isHidden = true
            self.viewBroadcastDeletePopup.isHidden = true
        }
        
        
        func leaveGroup(){
           
            self.ref.child("broadcast").child(self.objChatHistoryModel.strOpponentId).setValue(nil)
            self.ref.child("broadcastChat").child(self.objChatHistoryModel.strOpponentId).setValue(nil)
            self.ref.child("chat_history").child(self.strMyChatId).child(self.objChatHistoryModel.strOpponentId).setValue(nil)
            self.presentingViewController?.presentingViewController?.dismiss(animated: false, completion: nil)
        }
    }


//MARK: - Leave Group extension
extension BroadCastDetailsVC{
    @IBAction func btnClearChathistoryAction(_ sender: UIButton){
        self.view.endEditing(true)
        self.viewDeletePopup.isHidden = false
        self.viewBroadcastDeletePopup.isHidden = true
    }
    
    @IBAction func btnClearChatYes(_ sender: UIButton) {
        let dictNew = ["message":""]
        self.ref.child("broadcastChat").child(self.objChatHistoryModel.strOpponentId).setValue(nil)
        self.ref.child("chat_history").child(self.strMyChatId).child(self.objChatHistoryModel.strOpponentId).updateChildValues(dictNew)
        self.viewDeletePopup.isHidden = true
        
    }
    @IBAction func btnClearChatNo(_ sender: UIButton) {
        self.viewDeletePopup.isHidden = true
        self.viewBroadcastDeletePopup.isHidden = true
    }
    
}
    //MARK: - tableview method extension
    extension BroadCastDetailsVC{
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            self.lblMemberCount.text = String(self.arrChatHistory.count)+" Recipients"
            self.txtEnterGroupName.text = String(self.arrChatHistory.count)+" Recipients"
            tableHeight()
            return arrChatHistory.count
        }
        
        
        func tableHeight(){
            if arrChatHistory.count <= 3{
                self.height.constant = 200
            }else{
                self.height.constant = (CGFloat(self.arrChatHistory.count*55))
            }
        }
        
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
            if let cell = tableView.dequeueReusableCell(withIdentifier: "CellCreatGroup", for: indexPath) as? CellCreatGroup{
                let objChatList = arrChatHistory[indexPath.row]
                if objChatList.strOpponentProfileImage == ""{
                    cell.imgUserProfile.image = #imageLiteral(resourceName: "cellBackground")
                }else{
                    let url = URL(string: objChatList.strOpponentProfileImage)
                    //cell.imgUserProfile.af_setImage(withURL: url!)
                    cell.imgUserProfile.sd_setImage(with: url!, placeholderImage: UIImage(named: "cellBackground"))
                }
                cell.btnProfile.tag = indexPath.row
                cell.lblUserName.text = objChatList.strOpponentName
                if self.strAdminId == objChatList.strOpponentId{
                    cell.lblUserType.isHidden = false
                }else{
                    cell.lblUserType.isHidden = true
                }
                if objChatList.strOpponentId == self.strMyChatId{
                    cell.lblUserName.text = "You"
                }
                return cell
            }else{
                return UITableViewCell()
            }
        }
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        }
    }
    
    
    
    
    //MARK: - FireBase Method calls
    extension BroadCastDetailsVC{
        func GetUserListFromFirebase() {
            objWebserviceManager.StartIndicator()
            self.ref.child("broadcast").child(self.objChatHistoryModel.strOpponentId).observeSingleEvent(of:.value, with: { [weak self] (snapshot) -> Void in
                guard let strongSelf = self else { return }
                print(snapshot)
                self?.arrChatHistory.removeAll()
                self?.arrChatHistoryMyData.removeAll()
                
                if let dictData = snapshot.value as? NSDictionary{
                    if let aId = dictData["adminId"] as? Int{
                        self?.strAdminId = String(aId)
                    }else if let aId = dictData["adminId"] as? String{
                        self?.strAdminId = aId
                    }
                    
                    self?.txtEnterGroupName.text = dictData["groupName"] as? String ?? ""
                    
                    
                        //self?.imgGroupProfile.image = #imageLiteral(resourceName: "logo_2")
                   
                    
                    if let dictNNNN = dictData["member"] as? NSDictionary {
                        let arr = dictNNNN.allValues as NSArray
                        self?.txtEnterGroupName.text = String(arr.count) + " Recipients"
                        for  dicttt in arr {
                            if let dict = dicttt as? NSDictionary {
                                print("\n\ndicttt = \(dict)")
                                
                                let objChatList = ChatHistoryData()
                                objChatList.strOpponentName = dict["userName"] as? String ?? ""
                                objChatList.strOpponentFireBaseToken = dict["firebaseToken"] as? String ?? ""
                                objChatList.strMemberType  = dict["type"] as? String ?? ""
                                objChatList.strOpponentProfileImage  = dict["profilePic"] as? String ?? ""
                                objChatList.strMuteStatus = String(dict["mute"] as? Int ?? 0)
                                objChatList.strCreatUserStatus = false
                                if let id = dict["memberId"] as? Int {
                                    objChatList.strOpponentId = String(id)
                                }else if let id = dict["memberId"] as? String {
                                    objChatList.strOpponentId = id
                                }
                                
                                if objChatList.strOpponentId == self?.strMyChatId{
                                    self?.arrChatHistoryMyData.append(objChatList)
                                }else{
                                    self?.arrChatHistory.append(objChatList)
                                }
                            }
                            objWebserviceManager.StopIndicator()
                            if (self?.arrChatHistory.count)! > 0 {
                                self?.lblNoDataFound.isHidden = true
                            }else{
                                self?.lblNoDataFound.isHidden = false
                            }
                        }
                        self?.addMyObj()
                        
                    }else if let dict = snapshot.value as? NSDictionary {
                        strongSelf.parseDataChatHistory(dictHistory: dict)
                        objWebserviceManager.StopIndicator()
                    }
                    self?.tblCreateGroupList.reloadData()
                    objWebserviceManager.StopIndicator()
                    
                    if (self?.arrChatHistory.count == 0) {
                        self?.lblNoDataFound.isHidden = false;
                        self?.tblCreateGroupList.isHidden = true;
                    } else {
                        self?.lblNoDataFound.isHidden = true;
                        self?.tblCreateGroupList.isHidden = false;
                    }
                    objWebserviceManager.StopIndicator()
                }else if let dict = snapshot.value as? NSDictionary {
                    strongSelf.parseDataChatHistory(dictHistory: dict)
                    objWebserviceManager.StopIndicator()
                } else {
                    objWebserviceManager.StopIndicator()
                    strongSelf.arrChatHistory.removeAll()
                    strongSelf.arrChatHistoryMyData.removeAll()
                    strongSelf.tblCreateGroupList.reloadData()
                }
            })
        }
        
        func addMyObj(){
            if self.arrChatHistoryMyData.count >= 1{
                self.arrChatHistory.append(self.arrChatHistoryMyData[0])
            }
            for obj in self.arrChatHistory{
                if (obj.strOpponentId == strAdminId) && obj.strOpponentId != self.strMyChatId{
                    let index = self.arrChatHistory.index(of: obj)
                    self.arrChatHistory.remove(at: index!)
                    self.arrChatHistory.insert(obj, at: 0)
                }
            }
        }
        
        func parseDataChatHistory(dictHistory:NSDictionary){
            self.arrChatHistory.removeAll()
            self.arrChatHistoryMyData.removeAll()
            if let aId = dictHistory["adminId"] as? Int{
                self.strAdminId = String(aId)
            }else if let aId = dictHistory["adminId"] as? String{
                self.strAdminId = aId
            }

            
            
            
            
            
            
            
            
            if let newDict = dictHistory as? [String:Any]{
                if let dictmember = newDict["member"] as? NSArray{
                    self.txtEnterGroupName.text = String(dictmember.count) + " Recipients"
                    for obj in dictmember{
                        
                        if let dict = obj as? [String:Any]{
                            
                            
                            let objChatList = ChatHistoryData()
                            objChatList.strOpponentName = dict["userName"] as? String ?? ""
                            objChatList.strOpponentFireBaseToken = dict["firebaseToken"] as? String ?? ""
                            if let id = dict["memberId"] as? Int {
                                objChatList.strOpponentId = String(id)
                            }else if let ids = dict["memberId"] as? String{
                                objChatList.strOpponentId = ids
                            }
                            if strAdminId == objChatList.strOpponentId{
                                objChatList.strUserType = "Admin"
                            }else{
                                objChatList.strUserType = "member"
                            }
                            objChatList.strOpponentProfileImage  = dict["profilePic"] as? String ?? ""
                            objChatList.strMuteStatus = "0"
                            if objChatList.strOpponentId == self.strMyChatId{
                                self.arrChatHistoryMyData.append(objChatList)
                            }else{
                                self.arrChatHistory.append(objChatList)
                            }
                            
                        }
                        objWebserviceManager.StopIndicator()
                        
                        if arrChatHistory.count > 0 {
                            self.lblNoDataFound.isHidden = true
                        }else{
                            self.lblNoDataFound.isHidden = false
                        }
                    }
                }
            }
            
            self.addMyObj()
            
            print("self.arrChatHistory.count = \(self.arrChatHistory.count)")
            self.tblCreateGroupList.reloadData()
            objWebserviceManager.StopIndicator()
            
            if (self.arrChatHistory.count == 0) {
                self.lblNoDataFound.isHidden = false;
                self.tblCreateGroupList.isHidden = true;
            } else {
                self.lblNoDataFound.isHidden = true;
                self.tblCreateGroupList.isHidden = false;
            }
        }
    }



extension BroadCastDetailsVC{
    @IBAction func btnProfileAction( sender: UIButton){
        self.view.endEditing(true)
        let objUser = self.arrChatHistory[sender.tag]
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        self.view.isUserInteractionEnabled = false
        let dicParam = ["userName":objUser.strOpponentName]
        objServiceManager.requestPost(strURL: WebURL.profileByUserName, params: dicParam, success: { response in
            self.view.isUserInteractionEnabled = true
            if response["status"] as? String ?? "" == "success"{
                var strId = ""
                var strType = ""
                if let dictUser = response["userDetail"] as? [String : Any]{
                    let myId = dictUser["_id"] as? Int ?? 0
                    strId = String(myId)
                    strType = dictUser["userType"] as? String ?? ""
                }
                let dic = [
                    "tabType" : "people",
                    "tagId": strId,
                    "userType":strType,
                    "title": objUser.strOpponentName
                    ] as [String : Any]
                self.openProfileForSelectedTagPopoverWithInfo(dict: dic)
            }
        }) { error in
            self.view.isUserInteractionEnabled = true
        }
    }
    
    func openProfileForSelectedTagPopoverWithInfo(dict : [AnyHashable: Any]){
        
        var dictTemp : [AnyHashable : Any]?
        
        dictTemp = dict
        
        if let dict1 = dict as? [String:[String :Any]] {
            if let dict2 = dict1.first?.value {
                dictTemp = dict2
            }
        }
        
        guard let dictFinal = dictTemp as? [String : Any] else { return }
        
        var strUserType: String?
        var tagId : Int?
        
        if let userType = dictFinal["userType"] as? String{
            strUserType = userType
            
            if let idTag = dictFinal["tagId"] as? Int{
                tagId = idTag
            }else{
                if let idTag = dictFinal["tagId"] as? String{
                    tagId = Int(idTag)
                }
            }
            let strTagId = String(tagId!)
            if self.strMyChatId == strTagId {
                objAppShareData.isOtherSelectedForProfile = false
                self.gotoProfileVC()
                return
            }
            
            if let strUserType = strUserType, let tagId = tagId {
                
                objAppShareData.selectedOtherIdForProfile = String(tagId)
                objAppShareData.isOtherSelectedForProfile = true
                objAppShareData.selectedOtherTypeForProfile = strUserType  //"artist" or "user"
                //isNavigate = true
                self.gotoProfileVC()
            }
        }
    }
    func gotoProfileVC (){
        objAppShareData.isFromChatToProfile = true
        let sb: UIStoryboard = UIStoryboard(name: "ArtistProfile", bundle: Bundle.main)
        /*if let objVC = sb.instantiateViewController(withIdentifier:"SWRevealViewController") as? SWRevealViewController{
            
            objVC.hidesBottomBarWhenPushed = true
            let nav = UINavigationController.init(rootViewController: objVC)
            nav.navigationBar.isHidden = true
            self.present(nav, animated: false, completion: nil)
        }*/
        if let objVC = sb.instantiateViewController(withIdentifier:"ArtistProfileVC") as? ArtistProfileVC{
            objVC.hidesBottomBarWhenPushed = true
            let nav = UINavigationController.init(rootViewController: objVC)
            nav.navigationBar.isHidden = true
            nav.modalPresentationStyle = .fullScreen
            self.present(nav, animated: false, completion: nil)
        }
    }
}
    




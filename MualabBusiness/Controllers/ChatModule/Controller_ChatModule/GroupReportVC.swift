//
//  ReportVC.swift
//  MualabCustomer
//
//  Created by Mindiii on 8/22/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import Firebase

class GroupReportVC: UIViewController,UITextFieldDelegate,UITextViewDelegate {
    @IBOutlet weak var txtSelectReason: UITextField!
    @IBOutlet weak var txtViewDescription: UITextView!
    var objModel = ChatHistoryData()
    fileprivate var strMyChatId = ""

    fileprivate var ref: DatabaseReference!
    fileprivate var messages: [DataSnapshot]! = []
    fileprivate var _refHandle: DatabaseHandle?
    var arrReportReasonList = [ModelReportReason]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.callWebserviceForReasonList()
        self.strMyChatId =  UserDefaults.standard.string(forKey: UserDefaults.keys.myId) ?? ""
        self.txtSelectReason.delegate = self
        self.txtViewDescription.delegate = self
        ref = Database.database().reference()
        self.txtViewDescription.tintColor = UIColor.theameColors.skyBlueNewTheam
        self.txtViewDescription.tintColorDidChange()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.strMyChatId =  UserDefaults.standard.string(forKey: UserDefaults.keys.myId) ?? ""
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
 
    internal func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        var edit = true
        var newLength = 0
        var searchString: String? = nil
        if textView == self.txtViewDescription {
            if (text.count) != 0
            { searchString = self.txtViewDescription.text! + (text).uppercased()
                newLength = (self.txtViewDescription.text?.count)! + text.count - range.length
            }
            else {
                //searchString = (self.txtViewDescription.text as NSString?)?.substring(to: (self.txtViewDescription.text?.count)! - 1).uppercased()
            }
            if newLength <= 200{  edit = true
            }else{   edit = false  }
            return edit
        }
        return edit
    }
    
    
    internal func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var edit = true
        var newLength = 0
        var searchString: String? = nil
        if textField == self.txtSelectReason {
            if (string.count ) != 0
            { searchString = self.txtSelectReason.text! + (string).uppercased()
                newLength = (self.txtSelectReason.text?.count)! + string.count - range.length
            }
            else { searchString = (self.txtSelectReason.text as NSString?)?.substring(to: (self.txtSelectReason.text?.count)! - 1).uppercased()
            }
            if newLength <= 50{  edit = true
            }else{   edit = false  }
            return edit
        }
        return edit
    }
    
    internal func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.txtSelectReason{
            txtViewDescription.becomeFirstResponder()
        }
        return true
    }
    
    
    @IBAction func btnSubmitReportAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.txtSelectReason.text = self.txtSelectReason.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        self.txtViewDescription.text = self.txtViewDescription.text.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if self.txtSelectReason.text?.count == 0{
             objAppShareData.showAlert(withMessage: "Please enter report reason", type: alertType.bannerDark,on: self)
        }else if self.txtViewDescription.text?.count == 0{
             objAppShareData.showAlert(withMessage: "Please enter report description", type: alertType.bannerDark,on: self)
        }else{
            self.reportAction()
        }
    }
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func reportAction(){
    self.ref.child("group").child(self.objModel.strOpponentId).observeSingleEvent(of:.value, with: { [weak self] (snapshot) -> Void in
            guard let strongSelf = self else { return }
            print(snapshot)
            var adminId = ""
            if let dictData = snapshot.value as? NSDictionary{
                if let aId = dictData["adminId"] as? Int{
                    adminId = String(aId)
                }else if let aId = dictData["adminId"] as? String{
                    adminId = aId
                }
            }
 
        let time = ServerValue.timestamp()
        let dict = ["adminId":Int(adminId) ?? 0,
                    "groupId":self?.objModel.strOpponentId ?? "",
                    "description":self?.txtViewDescription.text ?? "",
                    "senderId":self?.strMyChatId ?? "",
                    "title":self?.txtSelectReason.text ?? "",
                    "type":"group",
                    "timeStamp":time] as [String : Any]
        
            self?.ref.child("group_report").child((self?.objModel.strOpponentId)!).childByAutoId().setValue(dict)
        objChatShareData.fromReportVC = true
        self?.dismiss(animated: true, completion: nil)
        })
        }
    
    func callWebserviceForReasonList(){
        if !objServiceManager.isNetworkAvailable(){
            return
        }
        objWebserviceManager.StartIndicator()
        self.arrReportReasonList.removeAll()
        let params = ["type":"group"]
        objServiceManager.requestGet(strURL:WebURL.reportReason, params: params as [String : AnyObject], success: { response in
            let  status = response["status"] as? String ?? ""
            print(response)
            objWebserviceManager.StopIndicator()
            if status == "success"{
                if let arr = response["data"] as? [[String:Any]]{
                    for obj in arr{
                        let obj = ModelReportReason.init(dict:obj)
                        self.arrReportReasonList.append(obj)
                    }
                }
            }else{
                let msg = response["message"] as? String ?? ""
                objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
            }
        }) { error in
            objWebserviceManager.StopIndicator()
            objActivity.stopActivity()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
    @IBAction func showScrollingNC() {
        self.view.endEditing(true)
        let scrollingNC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "BottomSheetVC") as! BottomSheetVC
        scrollingNC.strHeaderTile = "Report Reason"
        scrollingNC.modalPresentationStyle = .overCurrentContext
        let objGetData = self.arrReportReasonList.map { $0.title }
        if self.arrReportReasonList.count == 0{
            objAppShareData.showAlert(withMessage: "No reason found", type: alertType.bannerDark,on: self)
            return
        }
        objAppShareData.arrTBottomSheetModal = objGetData
        scrollingNC.callback = { [weak self] (str) in
                guard let strongSelf = self else {
                    return
                }
                strongSelf.txtSelectReason.text = strongSelf.arrReportReasonList[Int(str) ?? 0].title
        }
        self.present(scrollingNC, animated: false, completion: nil)
        /*let objGetData = self.arrReportReasonList.map { $0.title }
        if self.arrReportReasonList.count == 0{
            objAppShareData.showAlert(withMessage: "No reason found", type: alertType.bannerDark,on: self)
            return
        }
        objAppShareData.arrTBottomSheetModal = objGetData
        let scrollingNC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ScrollingNC") as!  UINavigationController
        let tableVc = scrollingNC.viewControllers[0] as! BottomSheetTableViewController
        tableVc.str = "Report Reason"
        tableVc.didSelectHandler = { [weak self] (str) in
            guard let strongSelf = self else {
                return
            }
            print(str)
            strongSelf.txtSelectReason.text = strongSelf.arrReportReasonList[Int(str) ?? 0].title
        }
        presentUsingHalfSheet(scrollingNC)
        */
    }
}


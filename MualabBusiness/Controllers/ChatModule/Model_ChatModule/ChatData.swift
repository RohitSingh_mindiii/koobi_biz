//
//  ChatData.swift
//  Appointment
//
//  Created by Apple on 11/06/18.
//  Copyright © 2018 MINDIII. All rights reserved.
//

import UIKit

class ChatData: Equatable {
    
    var strReadStatus = ""
    var imageLocal:UIImage = #imageLiteral(resourceName: "gallery_placeholder")
    var strMessage = ""
    var strSenderId = ""
    var strReceiverId = ""
    var strDateTime = ""
    var strOppName = ""
 
    var time: Double = 0.0
    var str_Key = ""
    var str_UId = ""
    var str_isImage = 0
    var str_lastSenderID = ""
    var str_message = ""
    var str_ProfileImage = ""

    var str_timestamp = ""
    var TimeStamp:Double = 0
    
    let dateFormatter : DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd hh:mm a"
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        return dateFormatter
    }()
    
    
    init?(dict : [String:Any],key:String){
            self.imageLocal = dict["imageLocal"] as? UIImage ?? #imageLiteral(resourceName: "gallery_placeholder")
            self.str_Key = key
            self.str_isImage = dict["messageType"] as? Int ?? 0
            self.str_lastSenderID = dict["senderId"] as? String ?? ""
            self.strMessage = dict["message"] as? String ?? ""
            self.str_UId = dict["senderId"] as? String ?? ""
            self.strOppName = dict["userName"] as? String ?? ""

            self.TimeStamp = dict["timestamp"] as? Double ?? 0
            self.strReceiverId = String(dict["reciverId"] as? Int ?? 0)
        
                if let readStatus = dict["readStatus"] as? Int{
                    var a = String(readStatus)
                        if a == ""{ a = "0" }
                self.strReadStatus = String(a)
                }else if let readStatus = dict["readStatus"] as? String{
                      self.strReadStatus = readStatus
                }
        
            let date = Date(timeIntervalSince1970: self.TimeStamp / 1000.0)
            self.str_timestamp = dateFormatter.string(from: date)
        
            if self.strMessage.contains(".gif"){
                imageLocal = UIImage.gif(url: self.strMessage) ?? #imageLiteral(resourceName: "gallery_placeholder")
            }
    }
    
    static func == (lhs: ChatData, rhs: ChatData) -> Bool {
        return lhs.str_Key == rhs.str_Key
    }
}

class ChatDataOpponent:NSObject{
    
    var strReadStatus = ""
    var strMessage = ""
    var strSenderId = ""
    var strReceiverId = ""
    var strDateTime = ""
 
    var time: Double = 0.0
    
    var str_Key = ""
    var str_UId = ""
    var str_isImage = 0
    var str_lastSenderID = ""
    var str_timestamp = ""
    var TimeStamp:Double = 0
    
    let dateFormatter : DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd hh:mm a"
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        return dateFormatter
    }()
    

    init?(dict : [String:Any],key:String){
        self.str_Key = key
        self.str_isImage = dict["messageType"] as? Int ?? 0
        self.str_lastSenderID = dict["senderId"] as? String ?? ""
        self.strMessage = dict["message"] as? String ?? ""
        self.str_UId = dict["senderId"] as? String ?? ""
        self.TimeStamp = dict["timestamp"] as? Double ?? 0
        self.strReceiverId = String(dict["reciverId"] as? Int ?? 0)
        
        //self.strReadStatus = String(dict["readStatus"] as? Int ?? 0)
        
        if let readStatus = dict["readStatus"] as? Int{
            self.strReadStatus = String(readStatus)
        }else if let readStatus = dict["readStatus"] as? String{
            self.strReadStatus = readStatus
        }
        
        let date = Date(timeIntervalSince1970: self.TimeStamp / 1000.0)
        self.str_timestamp = dateFormatter.string(from: date)
    }
    
}



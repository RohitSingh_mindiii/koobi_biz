//
//  CellAddRemoveGroupMember.swift
//  MualabCustomer
//
//  Created by Mindiii on 8/20/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

class CellAddRemoveGroupMember: UICollectionViewCell {
    @IBOutlet weak var viewCellBG: UIView!
    @IBOutlet weak var imgMember: UIImageView!
    @IBOutlet weak var btnDeleteMember: UIButton!
    @IBOutlet weak var lblMemberName: UILabel!
}

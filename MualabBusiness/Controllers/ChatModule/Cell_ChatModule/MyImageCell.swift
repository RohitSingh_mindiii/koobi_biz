//
//  MyImageself.swift
//  Appointment
//
//  Created by Apple on 11/06/18.
//  Copyright © 2018 MINDIII. All rights reserved.
//

import UIKit
import Kingfisher

class MyImageCell: UITableViewCell {

    @IBOutlet weak var heightDayDate: NSLayoutConstraint!
    @IBOutlet weak var viewMainBack: UIView!
    @IBOutlet weak var imgReadStatus: UIImageView!
    @IBOutlet weak var viewBlur: UIView!
    @IBOutlet weak var imgMySide: UIImageView!
    @IBOutlet weak var viewMyImage: UIView!
    @IBOutlet weak var lblMyMsgTime: UILabel!
    @IBOutlet weak var btnZoomImage: UIButton!
    
    @IBOutlet weak var lblMsgDay: UILabel!
    @IBOutlet weak var viewMsgDay: UIView!
    
    @IBOutlet weak var ImageIndicator: UIActivityIndicatorView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    
func cellDataParsing(objChat:ChatData,strDate:String,msgtime:String,resultDate:String,yesterDayDate:String){
        
        if objChat.strReadStatus == "0"{
            self.imgReadStatus.image = #imageLiteral(resourceName: "ico_chat_single_tick_gray")
        }else if objChat.strReadStatus == "1"{
            self.imgReadStatus.image = #imageLiteral(resourceName: "ico_chat_double_tick_gray")
        }else if objChat.strReadStatus == "2"{
            self.imgReadStatus.image = #imageLiteral(resourceName: "ico_chat_double_tick")
        }
        self.imgMySide.image = #imageLiteral(resourceName: "gallery_placeholder")
      if objChat.strMessage != "https://local"{
        self.ImageIndicator.isHidden = false
        self.ImageIndicator.startAnimating()
      }
        let urlImg = URL(string: objChat.strMessage)
        let urlReq = URLRequest(url:urlImg! , cachePolicy:.returnCacheDataElseLoad,timeoutInterval:0.0)
    
    if (objChat.strMessage.contains("content://media/external")
        || objChat.strMessage.contains("content://com.google.android")) {
        self.imgMySide.image = #imageLiteral(resourceName: "gallery_placeholder")
        self.ImageIndicator.isHidden = false
        self.ImageIndicator.startAnimating()
    }else{
    if objChat.strMessage.contains(".gif"){
        DispatchQueue.main.async {
        //let imageURL = UIImage.gif(url: objChat.strMessage)
        self.imgMySide.image = objChat.imageLocal
        self.ImageIndicator.stopAnimating()
        self.ImageIndicator.isHidden = true
        }
    }else if objChat.strMessage == "https://local"{
        self.ImageIndicator.stopAnimating()
        self.ImageIndicator.isHidden = true
        self.imgMySide.image = objChat.imageLocal
    }else{
        self.ImageIndicator.stopAnimating()
        self.ImageIndicator.isHidden = true
        
        if objChat.imageLocal != #imageLiteral(resourceName: "gallery_placeholder"){
            self.imgMySide.image = objChat.imageLocal
            return
        }
        //self.imgMySide.kf.setImage(with: urlImg!)
        let imgPlace = UIImage.init(named: "gallery_placeholder")//#imageLiteral(resourceName: "gallery_placeholder")
        var imageLocal:UIImage?
        if imageLocal == nil{
            imageLocal = #imageLiteral(resourceName: "gallery_placeholder")
        }
        let urlImg_1 = URL(string: objChat.strMessage)
        let identifier = "..."
        let resource = ImageResource(downloadURL: urlImg_1!, cacheKey: identifier)
        
        let processor = DownsamplingImageProcessor(size: self.imgMySide.frame.size)
            >> RoundCornerImageProcessor(cornerRadius: 0)
        self.imgMySide.kf.indicatorType = .activity
        self.imgMySide.kf.setImage(
            with: urlImg_1,
            placeholder: UIImage(named: "gallery_placeholder"),
            options: [
                .processor(processor),
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(1)),
                .cacheOriginalImage
            ])
        {
            result in
            switch result {
            case .success(let value):
                print("Task done for: \(value.source.url?.absoluteString ?? "")")
            case .failure(let error):
                print("Job failed: \(error.localizedDescription)")
            }
        }
        
        //self.imgMySide.kf.setImage(with: resource)
        //self.imgMySide.kf.setImage(with: resource, placeholder: imageLocal, options: nil, progressBlock: nil, completionHandler: nil)

        //         self.imgMySide.kf.setImage(with: Resource.init, placeholder: <#T##Placeholder?#>, options: <#T##KingfisherOptionsInfo?#>, progressBlock: <#T##DownloadProgressBlock?##DownloadProgressBlock?##(Int64, Int64) -> Void#>, completionHandler: <#T##((Result<RetrieveImageResult, KingfisherError>) -> Void)?##((Result<RetrieveImageResult, KingfisherError>) -> Void)?##(Result<RetrieveImageResult, KingfisherError>) -> Void#>)
        
        //self.imgMySide.kf.setImage(with: urlImg!, placeholder: imgPlace, options: nil, progressBlock: nil, completionHandler: nil)

//        self.imgMySide.kf.setImage(with: resource, placeholder: imgPlace, options: KingfisherOptionsInfoItem.cacheOriginalImage, progressBlock: { (receivedSize, totalSize) -> () in
//            print("Download Progress: \(receivedSize)/\(totalSize)")
//        }, completionHandler: { (image, error, imageURL) -> () in
//            //println("Downloaded and set!")
//            self.ImageIndicator.stopAnimating()
//            self.ImageIndicator.isHidden = true
//            self.imgMySide.image = image
//        })
        
       /*self.imgMySide.af_setImage(withURL: urlImg!, placeholderImage: #imageLiteral(resourceName: "gallery_placeholder"), progress: nil, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: false) { response in
          self.ImageIndicator.stopAnimating()
          self.ImageIndicator.isHidden = true
         //DispatchQueue.main.async {
          if let image = response.result.value {
            self.imgMySide.image = image
          }else{
            self.imgMySide.image = #imageLiteral(resourceName: "gallery_placeholder")
          }
         //}
      }*/
    }
    }
    
        /*
        self.imgMySide.af_setImage(withURLRequest: urlReq, placeholderImage: #imageLiteral(resourceName: "gallery_placeholder"), completion: { response in
            self.ImageIndicator.stopAnimating()
            self.ImageIndicator.isHidden = true
            if let image = response.result.value {
                self.imgMySide.image = image
            }else{
                self.imgMySide.image = #imageLiteral(resourceName: "gallery_placeholder")
            }
        })
        */
    
        if resultDate == strDate {
            self.lblMsgDay.text = "Today"
        }else if yesterDayDate == strDate{
            self.lblMsgDay.text = "Yesterday"
        }else{
            self.lblMsgDay.text = strDate
        }
        self.lblMyMsgTime.text = self.lblMsgDay.text! + " " + msgtime
 
}

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

//
//  ConfirmWalkingBookingVC.swift
//  MualabBusiness
//
//  Created by Mindiii on 2/14/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import DropDown
import CoreLocation
class ConfirmWalkingBookingVC: UIViewController ,UITableViewDelegate,UITableViewDataSource{
        @IBOutlet weak var constraintTableHeight: NSLayoutConstraint!
        @IBOutlet weak var lblHeaderOutCallInCall: UILabel!
        @IBOutlet weak var lblOutCallAddress: UILabel!
        @IBOutlet weak var btnOutCallAddress: UIButton!
        @IBOutlet weak var imgOutCallInCall: UIImageView!
        @IBOutlet weak var tblBookedServices: UITableView!
        
        @IBOutlet weak var imgVoucherList: UIImageView!
        @IBOutlet weak var imgCashCheckBox: UIImageView!
        @IBOutlet weak var btnCashCheckBox: UIButton!
        @IBOutlet weak var imgOnlineCheckBox: UIImageView!
        @IBOutlet weak var btnOnlineCheckBox: UIButton!
        @IBOutlet weak var viewOnline: UIView!
        @IBOutlet weak var lblTotalPrice: UILabel!
        @IBOutlet weak var lblRedTotalPrice: UILabel!
        @IBOutlet weak var viewRedTotalPrice: UIView!
        
        @IBOutlet weak var btnApplyVoucherCode: UIButton!
        @IBOutlet weak var txtVoucherCode: UITextField!
    var objSelectedCustomerDetails = ModelWalkingBooking(dict: ["":""])

        var dictVoucher = [:] as! [String:Any]
        var totalPrice = 0.0
        var commissionPresents = 0.0

        var discountPrice = 0.0
        var objArtistDetails = ArtistDetails(dict: ["":""])
        var objBookingServices = BookingServices()
        var arrBookedService = [BookingServices]()
        var isOutCallSelectedConfirm = false
        var isCash = false
        var isOnlinePaymentPossible = false
        var strMyAddress = ""
        var strLatitude = ""
        var strLongitude = ""
        
        override func viewDidLoad() {
            super.viewDidLoad()
            self.getAdminCimmission()
            self.callWebserviceForGetBookedServices()
        }
        override var preferredStatusBarStyle: UIStatusBarStyle {
            return .default
        }
        override func viewDidAppear(_ animated: Bool) {
            super.viewDidAppear(animated)
        }
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
            
            if objAppShareData.strVoucherCode.count>0{
                self.clearVoucherCode()
                self.txtVoucherCode.text = objAppShareData.strVoucherCode
                objAppShareData.strVoucherCode = ""
            }
            if objAppShareData.isForBookingOutCallAddress{
                objAppShareData.isForBookingOutCallAddress = false
                self.strLatitude = objAppShareData.latitudeBookingOutCallAddress
                self.strLongitude = objAppShareData.longitudeBookingOutCallAddress
                objAppShareData.latitudeBookingOutCallAddress = ""
                objAppShareData.longitudeBookingOutCallAddress = ""
                //self.checkArtistAvailabilityAtSelectedAdd()
                if objAppShareData.strBookingOutCallAddress.count>0{
                    self.lblOutCallAddress.text = objAppShareData.strBookingOutCallAddress
                    objAppShareData.strBookingOutCallAddress = ""
                }else{
                    self.lblOutCallAddress.text = self.strMyAddress
                }
            }
        }
    }
    
    //MARK: - button extension
    extension ConfirmWalkingBookingVC {
        
        @IBAction func btnCashCheckBoxAction(_ sender: UIButton) {
            if self.isCash{
                self.isCash = false
                self.imgCashCheckBox.image = UIImage.init(named:"inactiveBlack_check_box_ico")
                self.imgOnlineCheckBox.image = UIImage.init(named:"activeBlack_check_box_ico")
            }else{
                self.isCash = true
                self.imgCashCheckBox.image = UIImage.init(named:"activeBlack_check_box_ico")
                self.imgOnlineCheckBox.image = UIImage.init(named:"inactiveBlack_check_box_ico")
            }
        }
        /*
         @IBAction func btnOnlineCheckBoxAction(_ sender: UIButton) {
         if self.isCash{
         self.isCash = false
         self.imgCashCheckBox.image = UIImage.init(named:"inactiveBlack_check_box_ico")
         self.imgOnlineCheckBox.image = UIImage.init(named:"activeBlack_check_box_ico")
         }else{
         self.isCash = true
         self.imgCashCheckBox.image = UIImage.init(named:"activeBlack_check_box_ico")
         self.imgOnlineCheckBox.image = UIImage.init(named:"inactiveBlack_check_box_ico")
         }
         }
         */
        
        @IBAction func btnAddMoreAction(_ sender: UIButton) {
            self.navigationController?.popViewController(animated: true)
            //objAppShareData.showAlert(withMessage: "Under developement" , type: alertType.bannerDark,on: self)
        }
        @IBAction func btnConfirmBookingAction(_ sender: UIButton) {
            if self.isOutCallSelectedConfirm{
                if self.lblOutCallAddress.text == "" || self.lblOutCallAddress.text?.count == 0 || self.lblOutCallAddress.text == "Address"{
                    objAppShareData.showAlert(withMessage: "Please select address" , type: alertType.bannerDark,on: self)
                }else{
                    objWebserviceManager.StartIndicator()
                    self.checkArtistAvailabilityAtSelectedAdd()
                }
            }else{
                objWebserviceManager.StartIndicator()
                self.callWebserviceFor_ConfirmBooking()
            }
            if objArtistDetails.isAddressInArtistRange{
                //self.callWebserviceFor_ConfirmBooking()
            }
            //objAppShareData.showAlert(withMessage: "Under developement" , type: alertType.bannerDark,on: self)
        }
        @IBAction func btnVoucherListAction(_ sender: UIButton) {
            if self.imgVoucherList.image == UIImage.init(named:"ico_remove_added") {
                self.clearVoucherCode()
            }else{
                let sbNew: UIStoryboard = UIStoryboard(name: "BookingNew", bundle: Bundle.main)
                if let objVC = sbNew.instantiateViewController(withIdentifier:"VoucherListVC") as? VoucherListVC{
                    objVC.hidesBottomBarWhenPushed = true
                    objVC.fromWalkingBooking = true
                    navigationController?.pushViewController(objVC, animated: true)
                }
            }
        }
        @IBAction func btnOutCallAddressAction(_ sender: UIButton) {
            objAppShareData.isForBookingOutCallAddress = true
            let sb = UIStoryboard(name:"Main",bundle:Bundle.main)
            let objVC = sb.instantiateViewController(withIdentifier:"AddAddressVC") as! AddAddressVC
            objVC.objArtistDetails = self.objArtistDetails
            objVC.isOutcallOption = true
            self.navigationController?.pushViewController(objVC, animated: true)
        }
        @IBAction func btnApplyVouchercodeAction(_ sender: UIButton) {
            self.view.endEditing(true)
            if self.txtVoucherCode.text?.count == 0{
                objAppShareData.showAlert(withMessage:"Please enter voucher code",type: alertType.bannerDark, on: self)
            }else{
                self.callWebserviceForApplyVoucherCode()
            }
        }
        @IBAction func btnBackAction(_ sender: UIButton) {
            self.navigationController?.popViewController(animated: true)
        }
        func goToEditBooking(index:Int){
            let objBooking = self.arrBookedService[index]
            objAppShareData.isBookingFromService = true
            var objServiceForBooking = SubSubService.init(dict: [:])
            objServiceForBooking.serviceId = objBooking.objSubSubService.serviceId
            objServiceForBooking.subServiceId = objBooking.objSubSubService.subServiceId
            objServiceForBooking.subSubServiceId = objBooking.objSubSubService.subSubServiceId
            objServiceForBooking.staffIdForEdit = objBooking.staffId
            objServiceForBooking.strDateForEdit = objBooking.bookingDate
            objServiceForBooking.strSlotForEdit = objBooking.bookingTime
            objAppShareData.objServiceForEditBooking = objServiceForBooking
            self.navigationController?.popViewController(animated: true)
        }
        @IBAction func btnEditAction(_ sender: UIButton) {
            
            self.goToEditBooking(index: sender.tag)
        }
        @IBAction func btnEdit2Action(_ sender: UIButton) {
            
            self.goToEditBooking(index: sender.tag)
        }
        @IBAction func btnEdit3Action(_ sender: UIButton) {
            self.goToEditBooking(index: sender.tag)
        }
        @IBAction func btnDeleteAction(_ sender: UIButton) {
            self.clearVoucherCode()
            let objBooking = self.arrBookedService[sender.tag]
            let indexPath = IndexPath(row: sender.tag, section: 0)
            self.callWebserviceFor_deleteSelectedbookin(objBookingServices: objBooking, indexPath: indexPath)
        }
    }
    
    //MARK: - Custom methods Extension
    fileprivate extension ConfirmWalkingBookingVC{
        
        func gotoSearchBoardVC(){
            for controller in self.navigationController!.viewControllers as Array {
//                if controller.isKind(of: SearchBoardVC.self) {
//                    self.navigationController!.popToViewController(controller, animated: true)
//                    break
//                }
            }
        }
        
        func manageView(){
            self.viewRedTotalPrice.isHidden = true
            self.manageOutCallInCallView()
            self.managePaymentMethodView()
        }
        func clearVoucherCode(){
            self.dictVoucher = [:]
            self.btnApplyVoucherCode.setTitle("Apply", for: .normal)
            self.btnApplyVoucherCode.isUserInteractionEnabled = true
            self.btnApplyVoucherCode.setTitleColor(appColor, for: .normal)
            ////
            let doubleStrNN = String(format: "%.2f", ceil(self.totalPrice))
            ////
            //self.lblTotalPrice.text = "£" + String(self.totalPrice)
            self.lblTotalPrice.text = "£" + String(doubleStrNN)
            self.viewRedTotalPrice.isHidden = true
            self.imgVoucherList.image = UIImage.init(named: "forWordGray_ico")
            self.txtVoucherCode.text = ""
            self.txtVoucherCode.isUserInteractionEnabled = true
        }
        func managePaymentMethodView(){
            ////
            //self.isOnlinePaymentPossible = true
            ////
            if self.objArtistDetails.bankStatus == 1{
                self.isOnlinePaymentPossible = true
            }else{
                self.isOnlinePaymentPossible = false
            }
            self.isCash = true
            self.imgCashCheckBox.image = UIImage.init(named:"activeBlack_check_box_ico")
            self.imgOnlineCheckBox.image = UIImage.init(named:"inactiveBlack_check_box_ico")
            if self.isOnlinePaymentPossible{
                self.viewOnline.isHidden = false
                self.btnCashCheckBox.isUserInteractionEnabled = true
            }else{
                self.viewOnline.isHidden = true
                self.btnCashCheckBox.isUserInteractionEnabled = false
            }
        }
        func manageOutCallInCallView(){
            
            if let userInfonnn = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] {
                 print(userInfonnn)
                let strFullAddress = userInfonnn["address2"] as! String
                self.strMyAddress = strFullAddress
                self.strLatitude = userInfonnn["latitude"] as! String
                self.strLongitude = userInfonnn["longitude"] as! String
                //self.lblOutCallAddress.text = strFullAddress
            }else if let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.EditProfileAddress) as? Data{
                let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! User
                let addressComponent = userInfo.address
                
                if addressComponent.placeName.count > 0 && !addressComponent.placeName.contains("°"){
                    self.strMyAddress = addressComponent.placeName
                    self.lblOutCallAddress.text = addressComponent.placeName
                    self.strLatitude = addressComponent.latitude
                    self.strLongitude = addressComponent.longitude
                }else if addressComponent.fullAddress.count>0{
                    self.strMyAddress = addressComponent.fullAddress
                    self.strLatitude = addressComponent.latitude
                    self.strLongitude = addressComponent.longitude
                    //self.lblOutCallAddress.text = addressComponent.fullAddress
                }
            }
            
            if self.isOutCallSelectedConfirm{
                self.lblHeaderOutCallInCall.text = "Out Call"
                self.imgOutCallInCall.isHidden = false
                self.btnOutCallAddress.isUserInteractionEnabled = true
                self.lblOutCallAddress.text = self.strMyAddress
            }else{
                self.lblHeaderOutCallInCall.text = "In Call"
                self.imgOutCallInCall.isHidden = true
                self.btnOutCallAddress.isUserInteractionEnabled = false
                self.lblOutCallAddress.text = self.arrBookedService[0].addressForBooking
            }
        }
        func calculateTotalPrice(){
            var totalPrice = 0.0
            for obj in self.arrBookedService{
                if obj.strPrice == ""{
                    totalPrice = totalPrice + Double("0.0")!
                }else{
                    totalPrice = totalPrice + Double(obj.strPrice)!
                }
            }
            self.totalPrice = totalPrice
            let doubleStrNN = String(format: "%.2f", ceil(totalPrice))
            //self.lblTotalPrice.text = "£" + String(totalPrice)
            //self.lblRedTotalPrice.text = "£" + String(totalPrice)
            self.lblTotalPrice.text = "£" + doubleStrNN
            self.lblRedTotalPrice.text = "£" + doubleStrNN
        }
        
        func getDistancBetweenTwoPoint(strlat1 : String, strLong1 : String, strlat2 : String, strLong2 : String) -> Double{
            
            let lat1 = Double(strlat1) ?? 0
            let long1 = Double(strLong1) ?? 0
            
            let lat2 = Double(strlat2) ?? 0
            let long2 = Double(strLong2) ?? 0
            
            let coordinate1 = CLLocation(latitude: lat1, longitude: long1)
            let coordinate2 = CLLocation(latitude: lat2, longitude: long2)
            
            let distanceInMeters = coordinate1.distance(from: coordinate2) // result is in meters
            //1 meter =  0.000621371 mile
            let distanceInMiles = 0.000621371 * distanceInMeters
            
            return distanceInMiles
        }
        
        func checkArtistAvailabilityAtSelectedAdd(){
            
            let artistRadius = objArtistDetails.radius
            let distanceInMiles = getDistancBetweenTwoPoint(strlat1: objArtistDetails.latitude, strLong1: objArtistDetails.longitude, strlat2: strLatitude, strLong2: strLongitude)
            
            if distanceInMiles > artistRadius {
                objArtistDetails.isAddressInArtistRange = false
                objActivity.stopActivity()
                objAppShareData.showAlert(withMessage: "Selected artist services is not available at this location", type: alertType.bannerDark, on: self)
                objArtistDetails.userBookingSelectedAddress = ""
                objArtistDetails.userBookingSelectedAddressLatitude = ""
                objArtistDetails.userBookingSelectedAddressLongitude = ""
            }else{
                objArtistDetails.isAddressInArtistRange = true
                self.callWebserviceFor_ConfirmBooking()
            }
        }
    }
    
    //MARK: - Webservices Extension
    fileprivate extension ConfirmWalkingBookingVC{
        
        func callWebserviceFor_ConfirmBooking(){
            /*artistId:
             userId:
             bookingDate:
             bookingTime:
             location:
             totalPrice:
             paymentType: (1:online,2:offline)
             bookingType: (1:Incall,2:OutCall)*/
            
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            objActivity.startActivityIndicator()
            var paymentType = 0
            if self.isCash{
                paymentType = 2
            }else{
                paymentType = 1
            }
            var bookingType = 0
            if self.isOutCallSelectedConfirm{
                bookingType = 2
            }else{
                bookingType = 1
            }
            var strUserId = ""
            
            if let dict = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] {
                if let userId = dict["_id"] as? String {
                    strUserId = userId
                }
            }else{
                if let dict = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] {
                    if let userId = dict["_id"] as? String {
                    strUserId = userId
                    }
                    
                }
            }
            var commissionAmount = 0.0
            if self.commissionPresents > 0 {
//                if self.discountPrice > 0{
//                    let a = self.discountPrice/100
//                    commissionAmount =  a*Double(commissionPresents)
//                }else{
                    let a = self.totalPrice/100
                    commissionAmount =  a*Double(commissionPresents)
             //   }
            }
            
            print("commissionAmount = ",commissionAmount)
            print("totalPrice = ",self.totalPrice)
            print("commissionPresents = ",commissionPresents)
            if self.discountPrice > 0{
                self.discountPrice = self.discountPrice+commissionAmount
            }
            //else{
            //    self.totalPrice = self.totalPrice+commissionAmount
          //  }
            let jsonData = try! JSONSerialization.data(withJSONObject: self.dictVoucher, options: [])
            var decoded = String(data: jsonData, encoding: .utf8)!
            let objbooking = self.arrBookedService[0]
            ////
            if decoded == "{}"{
               decoded = ""
            }
            ////
            let parameters : Dictionary = [
                "artistId" : self.objArtistDetails._id,
                "userId" : self.objSelectedCustomerDetails._id,
                "discountPrice" : self.discountPrice,
                "bookingDate" : objbooking.bookingDate,
                "bookingTime" : objbooking.bookingTime,
                "location" : self.lblOutCallAddress.text!,
                "totalPrice" : self.totalPrice,
                "adminAmount" : String(commissionAmount),
                "adminCommision" : String(commissionPresents),
                "paymentType" : paymentType,
                "bookingType" : bookingType,
                "voucher" : decoded,
                "customerType":"walking"
                ] as [String : Any]
            print(parameters)
            objWebserviceManager.requestPost(strURL: WebURL.confirmBooking, params: parameters  , success: { response in
                
                objWebserviceManager.StopIndicator()
                let keyExists = response["responseCode"] != nil
                if  keyExists {
                    sessionExpireAlertVC(controller: self)
                }else{
                    
                    let strStatus =  response["status"] as? String ?? ""
                    let strMsg = response["message"] as? String ?? kErrorMessage
                    if strStatus == k_success{
                        var msg = "Your booking request has been successfully sent to artist. check booking history "
                        if let dict = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] {
                            let name = dict[UserDefaults.keys.firstName] as? String ?? ""
                            let lastName = dict[UserDefaults.keys.lastName] as? String ?? ""
                                msg = "Your booking has been made with "+name+" "+lastName+". Please ensure to arrive 5 minutes before scheduled time "
                            }
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                            _ = SweetAlert().showAlert("Congratulation!", subTitle: msg, style: AlertStyle.success, buttonTitle: "OK", buttonColor: appColor, action: { (isClicked) in
                             
                                     let bookingId = response["bookingId"] as? Int ?? 0
                                
                                let sb = UIStoryboard(name:"Booking",bundle:Bundle.main)
                                let objChooseType = sb.instantiateViewController(withIdentifier:"ConfirmBookingVC") as! ConfirmBookingVC
                                /*objChooseType.hidesBottomBarWhenPushed = true
                                objChooseType.strBookingId = String(bookingId)
                                objAppShareData.fromDetailPage = true
                                objChooseType.fromAppointment = true
                                self.navigationController?.pushViewController(objChooseType, animated: true)
                                */
                                objAppShareData.objAppdelegate.gotoTabBar(withAnitmation: false)
                            })
                        }
                    }else{
                        objAppShareData.showAlert(withMessage: strMsg, type: alertType.bannerDark,on: self)
                    }
                }
            }){ error in
                objWebserviceManager.StopIndicator()
                objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
            }
        }
        
        func callWebserviceFor_deleteSelectedbookin(objBookingServices : BookingServices, indexPath : IndexPath){
            
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            objActivity.startActivityIndicator()
            
            let parameters : Dictionary = [
                "bookingId" : objBookingServices.bookingId,
                ] as [String : Any]
            
            objWebserviceManager.requestPost(strURL: WebURL.deleteBookService, params: parameters  , success: { response in
                
                objWebserviceManager.StopIndicator()
                let keyExists = response["responseCode"] != nil
                if  keyExists {
                    sessionExpireAlertVC(controller: self)
                }else{
                    
                    let strStatus =  response["status"] as? String ?? ""
                    let strMsg = response["message"] as? String ?? kErrorMessage
                    if strStatus == k_success{
                        if self.arrBookedService.count==1{
                            self.navigationController?.popViewController(animated: true)
                            
                        }else{
                            self.callWebserviceForGetBookedServices()
                        }
                        /*
                         self.arrBookedService.remove(at: indexPath.row)
                         if self.arrBookedService.count>0{
                         self.tblBookedServices.deleteRows(at: [indexPath], with: .left)
                         self.manageTableViewHeight()
                         self.manageView()
                         self.calculateTotalPrice()
                         
                         }else{
                         self.navigationController?.popViewController(animated: true)
                         }
                         */
                    }else{
                        objAppShareData.showAlert(withMessage: strMsg, type: alertType.bannerDark,on: self)
                    }
                }
            }){ error in
                objWebserviceManager.StopIndicator()
                objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
            }
        }
        func callWebserviceForApplyVoucherCode(){
            //artistId:''
            //voucherCode:''
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            objActivity.startActivityIndicator()
            var strUserId : String = ""
            if  let userInfo = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]{
                if let userId = userInfo["_id"] as? String {
                    strUserId = userId
                }}
            let dicParam = [
                "artistId":strUserId,//objAppShareData.selectedOtherIdForProfile,
                "voucherCode": self.txtVoucherCode.text!
                ] as [String : Any]
            
            objServiceManager.requestPostForJson(strURL: WebURL.applyVoucher, params: dicParam, success: { response in
                print(response)
                objActivity.stopActivity()
                let keyExists = response["responseCode"] != nil
                if  keyExists {
                    sessionExpireAlertVC(controller: self)
                }else {
                    let strSucessStatus = response["status"] as? String ?? ""
                    if  strSucessStatus == "success"{
                        objActivity.stopActivity()
                        self.manageVoucherView(dict:response["data"] as! [String:Any])
                    }else{
                        let msg = response["message"] as? String ?? ""
                        objAppShareData.showAlert(withMessage: msg , type: alertType.bannerDark,on: self)
                    }}
            }) { error in
                objActivity.stopActivity()
                objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
            }
        }
        
        func manageVoucherView(dict:[String:Any]){
            self.dictVoucher = dict
            var discountType = 0
            if let discount = dict["discountType"] as? Int{
                discountType = discount
            }else if let discount = dict["discountType"] as? String{
                discountType = Int(discount)!
            }
            
            var amount = 0.0
            if let amnt = dict["amount"] as? Double{
                amount = amnt
            }else if let amnt = dict["amount"] as? Int{
                amount = Double(amnt)
            }else if let amnt = dict["amount"] as? Float{
                amount = Double(amnt)
            }else if let amnt = dict["amount"] as? String{
                amount = Double(amnt)!
            }
            if discountType == 2{
                let newPrice = self.totalPrice - (self.totalPrice*amount/100)
                if newPrice>0{
                    self.discountPrice = newPrice
                    let doubleStrNN = String(format: "%.2f", ceil(newPrice))
                    //self.lblTotalPrice.text = "£" + String(format: "%.2f", newPrice)
                    self.lblTotalPrice.text = "£" + String(format: "%.2f", doubleStrNN)
                }else{
                    self.discountPrice = 0.0
                    self.lblTotalPrice.text = "£" + "0"
                }
            }else{
                let newPrice = self.totalPrice-amount
                if newPrice>0{
                    self.discountPrice = newPrice
                    let doubleStrNN = String(format: "%.2f", ceil(newPrice))
                    //self.lblTotalPrice.text = "£" + String(format: "%.2f", newPrice)
                    self.lblTotalPrice.text = "£" + String(format: "%.2f", doubleStrNN)
                }else{
                    self.discountPrice = 0.0
                    self.lblTotalPrice.text = "£" + "0"
                }
            }
            self.btnApplyVoucherCode.setTitle("Applied", for: .normal)
            self.btnApplyVoucherCode.setTitleColor(UIColor.init(red: 0.0/255.0, green: 187.0/255.0, blue: 39.0/255.0, alpha: 1.0), for: .normal)
            self.btnApplyVoucherCode.isUserInteractionEnabled = false
            self.imgVoucherList.image = UIImage.init(named: "ico_remove_added")
            self.txtVoucherCode.isUserInteractionEnabled = false
            self.viewRedTotalPrice.isHidden = false
        }
        
        func callWebserviceForGetBookedServices(){
            
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            objActivity.startActivityIndicator()
            var strUserId : String = ""
            if  let userInfo = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]{
                if let userId = userInfo["_id"] as? String {
                    strUserId = userId
                }}
            let dicParam = [
                
                "artistId":strUserId,//objAppShareData.selectedOtherIdForProfile,
                "bookingType":"walking",
                "userId":objSelectedCustomerDetails._id ] as [String : Any]
            
            objServiceManager.requestPostForJson(strURL: WebURL.getBookedServices, params: dicParam, success: { response in
                print(response)
                self.arrBookedService.removeAll()
                objActivity.stopActivity()
                let keyExists = response["responseCode"] != nil
                if  keyExists {
                    sessionExpireAlertVC(controller: self)
                }else {
                    let strSucessStatus = response["status"] as? String ?? ""
                    if  strSucessStatus == "success"{
                        objActivity.stopActivity()
                        self.saveData(dict:response)
                    }else{
                        let msg = response["message"] as? String ?? ""
                        objAppShareData.showAlert(withMessage: msg , type: alertType.bannerDark,on: self)
                    }
                }
            }) { error in
                objActivity.stopActivity()
                objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
            }
        }
        
        func saveData(dict:[String:Any]){
            
            if let arr = dict["data"] as? [[String : Any]]{
 
                for dict in arr{
                    let objBookedService = BookingServices()
                    objBookedService.objSubSubService.serviceId = dict["serviceId"] as? Int ?? 0
                    objBookedService.objSubSubService.subServiceId = dict["subServiceId"] as? Int ?? 0
                    objBookedService.objSubSubService.subSubServiceId = dict["artistServiceId"] as? Int ?? 0
                    objBookedService.bookingId = dict["_id"] as? Int ?? 0
                    
                    objBookedService.staffId = dict["staffId"] as? Int ?? 0
                    objBookedService.staffName = dict["staffName"] as? String ?? ""
                    objBookedService.staffImage = dict["staffImage"] as? String ?? ""
                    objBookedService.objSubSubService.subSubServiceName = dict["artistServiceName"] as? String ?? ""
                    objBookedService.bookingDate = dict["bookingDate"] as? String ?? ""
                    objBookedService.bookingTime = dict["startTime"] as? String ?? ""
                    objBookedService.addressForBooking = dict["companyAddress"] as? String ?? ""
                    //objBookedService.price = dict["bookingPrice"] as? Double ?? 0.0
                    objBookedService.strPrice = dict["bookingPrice"] as? String ?? ""
                    self.arrBookedService.append(objBookedService)
                }
            }
            self.tblBookedServices.reloadData()
            self.manageTableViewHeight()
            self.manageView()
            self.calculateTotalPrice()
        }
        
        func manageTableViewHeight(){
            self.constraintTableHeight.constant = CGFloat(self.arrBookedService.count*260)
            //self.constraintTableHeight.constant = CGFloat(1*195)
            self.view.layoutIfNeeded()
        }
      
        func getAdminCimmission(){
            var myId = ""
            if  let userInfo = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] {
                if let id = userInfo[UserDefaults.keys.userId] as? String {
                    myId = id
                }
            }
            let parameters : Dictionary = [
                "artistId" :myId ] as [String : Any]
            
            objWebserviceManager.requestPost(strURL: WebURL.adminCommision, params: parameters  , success: { response in
                let keyExists = response["responseCode"] != nil
                if  keyExists {
                }else{
                    
                    let strStatus =  response["status"] as? String ?? ""
                    if strStatus == k_success{
                        if let commissionPre = response["cashCommision"] as? Int{
                            self.commissionPresents = Double(commissionPre)
                        }else if let commissionPre = response["cashCommision"] as? Double{
                            self.commissionPresents = Double(commissionPre)
                        }else if let commissionPre = response["cashCommision"] as? String{
                            self.commissionPresents = Double(commissionPre) ?? 0.0
                        }else if let commissionPre = response["cashCommision"] as? Float{
                            self.commissionPresents = Double(commissionPre)
                        }
                        
                        print(response)
                    }else{
                    }
                }
            }){ error in
            }
        }
        
    }
    
    // MARK: - Textfield Delegate Methods
    extension ConfirmWalkingBookingVC : UITextFieldDelegate{
        public func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        }
        
        func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            
            if textField == self.txtVoucherCode {
                if string == " "{
                    return false
                }
                if self.btnApplyVoucherCode.titleLabel?.text == "Applied"{
                    self.clearVoucherCode()
                }
                var searchString: String? = nil
                var newLength:Int = 0
                
                if (string.count ) != 0{
                    searchString = self.txtVoucherCode.text! + (string).uppercased()
                    newLength = (self.txtVoucherCode.text?.count)! + string.count - range.length
                }
                else {
                    searchString = (self.txtVoucherCode.text as NSString?)?.substring(to: (self.txtVoucherCode.text?.count)! - 1).uppercased()
                }
                if searchString?.count == 7 {
                    self.txtVoucherCode.text = searchString
                    searchString = nil
                    self.txtVoucherCode.endEditing(true)
                }
                else {
                }
                return newLength<=7
            }
            return true
        }
        func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            if textField == txtVoucherCode {
                txtVoucherCode.resignFirstResponder()
            }
            return true
        }
    }
    
    //MARK: - UITableview delegate
    extension ConfirmWalkingBookingVC {
        
        func numberOfSections(in tableView: UITableView) -> Int {
            return 1
        }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return self.arrBookedService.count
            //return 1
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
            let  cell = tableView.dequeueReusableCell(withIdentifier: "SubServiceCell", for: indexPath) as! SubServiceCell
            cell.indexPath = indexPath
            let objBookedService = self.arrBookedService[indexPath.row]
            if objBookedService.staffImage != "" {
                if let url = URL(string: objBookedService.staffImage){
                    //cell.imgProfie.af_setImage(withURL: url, placeholderImage:#imageLiteral(resourceName: "cellBackground"))
                    cell.imgProfie.sd_setImage(with: url, placeholderImage:#imageLiteral(resourceName: "cellBackground"))
                }
            }else{
                cell.imgProfie.image = #imageLiteral(resourceName: "cellBackground")
            }
            cell.lblName.text = objBookedService.objSubSubService.subSubServiceName
            cell.lblArtistName.text = objBookedService.staffName
            cell.lblPrice.text = objBookedService.strPrice
            ////
            let doubleStrNN = String(format: "%.2f", ceil(Double(cell.lblPrice.text!)!))
            cell.lblPrice.text = doubleStrNN
            ////
            cell.lblPrice.text = "£" + cell.lblPrice.text!
            
            let formatter  = DateFormatter()
            formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
            formatter.dateFormat = "yyyy-MM-dd"
            let date = formatter.date(from: objBookedService.bookingDate)
            formatter.dateFormat = "dd/MM/yyyy"
            let strDate = formatter.string(from: date ?? Date())
            cell.lblDuration.text = strDate + ", " + objBookedService.bookingTime
            //cell.lblDuration.text = objBookedService.bookingDate + ", " + objBookedService.bookingTime
            
            cell.btnProfile.tag = indexPath.row
            cell.btnProfile.superview?.tag = indexPath.section
            cell.btnEdit.tag = indexPath.row
            cell.btnEdit.superview?.tag = indexPath.section
            cell.btnEdit2.tag = indexPath.row
            cell.btnEdit2.superview?.tag = indexPath.section
            cell.btnEdit3.tag = indexPath.row
            cell.btnEdit3.superview?.tag = indexPath.section
            cell.btnEdit4.tag = indexPath.row
            cell.btnEdit4.superview?.tag = indexPath.section
            cell.btnDelete.tag = indexPath.row
            cell.btnDelete.superview?.tag = indexPath.section
            return cell
        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        }
}


//
//  WakingCustomerListVC.swift
//  MualabBusiness
//
//  Created by Mindiii on 2/12/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit


class cellWalkingCustomerList: UITableViewCell {
    @IBOutlet weak var lblContactNo:UILabel!
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var lblNameIcon:UILabel!

    override func awakeFromNib() {  super.awakeFromNib()   }}


class WakingCustomerListVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {
   
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var tblCustomerList:UITableView!
    @IBOutlet weak var heightTableView:NSLayoutConstraint!
    @IBOutlet weak var txtSearch:UITextField!
    @IBOutlet weak var txtFirstName:UITextField!
    @IBOutlet weak var txtLastName:UITextField!
    @IBOutlet weak var txtEmail:UITextField!
    @IBOutlet weak var txtContact:UITextField!
    @IBOutlet weak var viewTable:UIView!
    
    @IBOutlet weak var viewEmail:UIView!
    @IBOutlet weak var viewPhoneNumber:UIView!
    @IBOutlet weak var btnCheckBox:UIButton!
    
    var strSearchValue = ""
    var fromViewWillappear = false
    var isCheckBoxSelected = false
    
    var arrCustomerList = [ModelWalkingBooking]()
    var objSelected = ModelWalkingBooking(dict: ["":""])
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrCustomerList.count < 6{
            self.heightTableView.constant = CGFloat(40*self.arrCustomerList.count)+5
            if arrCustomerList.count == 0{
                self.heightTableView.constant = 0
            }
        }else{
            self.heightTableView.constant = 240
        }
        return arrCustomerList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let obj = self.arrCustomerList[indexPath.row]
        if let cell = tableView.dequeueReusableCell(withIdentifier: "cellWalkingCustomerList", for: indexPath) as? cellWalkingCustomerList{
            cell.lblContactNo.text = obj.phone+"  ("+(obj.firstName).capitalized+" "+obj.lastName+")"
            return cell
        }else{
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        if self.arrCustomerList.count >= indexPath.row+1{
        let obj = self.arrCustomerList[indexPath.row]
            objSelected = obj
        self.txtFirstName.text = obj.firstName
        self.txtLastName.text = obj.lastName
        self.txtEmail.text = obj.email
        self.txtContact.text = obj.phone
        self.viewTable.isHidden = true
        self.txtSearch.text = ""
        }
//         objAppShareData.selectedOtherIdForProfile = obj._id
//        let sbNew: UIStoryboard = UIStoryboard(name: "BookingNew", bundle: Bundle.main)
//        if let objVC = sbNew.instantiateViewController(withIdentifier:"BookingNewVC") as? BookingNewVC{
//            
//            objVC.objSelectedCustomerDetails = obj
//            objVC.hidesBottomBarWhenPushed = true
//            navigationController?.pushViewController(objVC, animated: true)
//        }
        
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.txtSearch.delegate = self
        self.txtFirstName.delegate = self
        self.txtLastName.delegate = self
        self.txtEmail.delegate = self
        self.txtContact.delegate = self
        self.tblCustomerList.delegate = self
        self.tblCustomerList.dataSource = self
        self.indicator.stopAnimating()
        self.addAccessoryView()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        ////
        self.isCheckBoxSelected = false
        ////
        if self.isCheckBoxSelected{
            self.btnCheckBox.setImage(UIImage.init(named: "checked"), for: .normal)
            self.viewEmail.isHidden = false
            self.viewPhoneNumber.isHidden = false
        }else{
            self.btnCheckBox.setImage(UIImage.init(named: "inactive_check_box_black"), for: .normal)
            self.viewEmail.isHidden = true
            self.viewPhoneNumber.isHidden = true
        }
        fromViewWillappear = true
        self.viewTable.isHidden = true
        self.txtSearch.text = ""
        self.txtFirstName.text = ""
        self.txtLastName.text = ""
        self.txtEmail.text = ""
        self.txtContact.text = ""
        self.indicator.stopAnimating()
        self.callWebserviceForCustomerList(andSearchText: "")
    }
}


//MARK: - button extension
extension WakingCustomerListVC{
    func callWebserviceForCustomerList(andSearchText:String){
            if !objServiceManager.isNetworkAvailable(){
                return
            }
        self.arrCustomerList.removeAll()
        self.indicator.startAnimating()

        objServiceManager.requestGet(strURL:WebURL.customerList, params: ["search":andSearchText as AnyObject], success: { response in
            
                let  status = response["status"] as? String ?? ""
                print(response)
                if status == "success"{
                    if let arr = response["data"] as? [[String:Any]] {
                        for obj in arr{
                            let newObj = ModelWalkingBooking.init(dict: obj)
                            var add = true
                            for obj2 in self.arrCustomerList{
                                if newObj.phone == obj2.phone{
                                    add = false
                                }
                            }
                            if add == true{
                            self.arrCustomerList.append(newObj)
                            }
                        }
                        if self.fromViewWillappear == false{
                        if self.arrCustomerList.count > 0{
                        self.viewTable.isHidden = false
                        }else{
                            self.viewTable.isHidden = false
                        }
                            if andSearchText == ""{
                                self.viewTable.isHidden = true

                            }
                        }
                        if self.fromViewWillappear == true{
                            self.fromViewWillappear = false
                            self.viewTable.isHidden = true
                        }
                        self.tblCustomerList.reloadData()
                    }
                    self.indicator.stopAnimating()

                }else{
                    if self.fromViewWillappear == false{
                    self.viewTable.isHidden = false
                    }
                    self.tblCustomerList.reloadData()
                    self.indicator.stopAnimating()

                }
            }) { error in
                self.viewTable.isHidden = true
                self.indicator.stopAnimating()
                self.tblCustomerList.reloadData()
                objActivity.stopActivity()
            }
    }
    
    func callWebserviceForAddCustomer(param:[String:Any]){
        if !objServiceManager.isNetworkAvailable(){
            return
        }
        var arrLocal = [ModelWalkingBooking]()
        objWebserviceManager.StartIndicator()
        objServiceManager.requestPost(strURL:WebURL.addCustomer, params: param, success: { response in
            let  status = response["status"] as? String ?? ""
            print(response)
            if status == "success"{
                if let arr = response["data"] as? [[String:Any]]{
                    for obj in arr{
                        let newObj = ModelWalkingBooking.init(dict: obj)
                         arrLocal.append(newObj)
                    }
                }else if let arr = response["data"] as? [String:Any]{
                        let newObj = ModelWalkingBooking.init(dict: arr)
                        arrLocal.append(newObj)
                }
                objWebserviceManager.StopIndicator()

                if arrLocal.count > 0{
                let obj = arrLocal[0]
                objAppShareData.selectedOtherIdForProfile = obj._id
                let sbNew: UIStoryboard = UIStoryboard(name: "BookingNew", bundle: Bundle.main)
                if let objVC = sbNew.instantiateViewController(withIdentifier:"BookingNewVC") as? BookingNewVC{
                    objVC.objSelectedCustomerDetails = obj
                    objVC.hidesBottomBarWhenPushed = true
                    self.navigationController?.pushViewController(objVC, animated: true)
                }
                }
            }else{
                objWebserviceManager.StopIndicator()
                let  message = response["message"] as? String ?? ""
                objAppShareData.showAlert(withMessage: message, type: alertType.banner, on: self)
            }
        }) { error in
            objWebserviceManager.StopIndicator()
            objActivity.stopActivity()
        }
    }
    
    
    
    
    

    
    
}
//MARK: - button extension
extension WakingCustomerListVC{
        @IBAction func btnBack(_ sender:Any){
            self.view.endEditing(true)
            self.navigationController?.popViewController(animated: true)
        }
    
        @IBAction func btnCheckBoxAction(_ sender:Any){
            self.txtEmail.text = ""
            self.txtContact.text = ""
            if self.isCheckBoxSelected{
                self.isCheckBoxSelected = false
                self.btnCheckBox.setImage(UIImage.init(named: "inactive_check_box_black"), for: .normal)
                self.viewEmail.isHidden = true
                self.viewPhoneNumber.isHidden = true
            }else{
                self.isCheckBoxSelected = true
                self.btnCheckBox.setImage(UIImage.init(named: "checked"), for: .normal)
                self.viewEmail.isHidden = false
                self.viewPhoneNumber.isHidden = false
            }
        }
 
        @IBAction func btnContinew(_ sender:Any){
            self.view.endEditing(true)
            objWebserviceManager.StartIndicator()

            txtFirstName.text = txtFirstName.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            txtLastName.text = txtLastName.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            txtEmail.text = txtEmail.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            txtContact.text = txtContact.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)

            if self.txtFirstName.text?.count == 0{
                objWebserviceManager.StopIndicator()
                objAppShareData.shakeViewField(self.txtFirstName)
//            }else if self.txtLastName.text?.count == 0{
//                objWebserviceManager.StopIndicator()
//                objAppShareData.shakeViewField(self.txtLastName)
            }else if self.txtEmail.text?.count == 0 && self.isCheckBoxSelected{
                objWebserviceManager.StopIndicator()
                objAppShareData.shakeViewField(self.txtEmail)
            }else if !(objValidationManager.isValidateEmail(strEmail: self.txtEmail.text!)) && self.isCheckBoxSelected{
                objWebserviceManager.StopIndicator()
                 objAppShareData.showAlert(withMessage: message.validation.email, type: alertType.banner, on: self)
            }else if self.txtContact.text?.count == 0 && self.isCheckBoxSelected{
                objWebserviceManager.StopIndicator()
                objAppShareData.shakeViewField(self.txtContact)
            }else{
                var param = ["firstName":self.txtFirstName.text ?? "",
                                       "lastName":self.txtLastName.text ?? "",
                                       "email":self.txtEmail.text ?? "",
                                       "userName": "",
                                       "phone":self.txtContact.text ?? "",]
                
                if objSelected.phone == self.txtContact.text ?? ""{
                    param["userName"] = objSelected.userName
                }
                self.callWebserviceForAddCustomer(param: param)
            }
        }
    
    @IBAction func btnHiddenTable(_ sender:Any){
        self.view.endEditing(true)
        self.viewTable.isHidden = true
    }
}



// MARK: - UITextfield Delegate
extension WakingCustomerListVC{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        fromViewWillappear = false
        let text = textField.text! as NSString
        if textField == self.txtSearch{
        if (text.length == 1)  && (string == "") {
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
            callWebserviceForCustomerList( andSearchText: "")
        }
        var substring: String = textField.text!
        substring = (substring as NSString).replacingCharacters(in: range, with: string)
        substring = substring.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        searchAutocompleteEntries(withSubstring: substring)
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let text = textField.text! as NSString

        
        if textField == self.txtSearch{
            textField.resignFirstResponder()
        }else if textField == self.txtFirstName{
            txtLastName.becomeFirstResponder()
        }else if textField == self.txtLastName{
            txtEmail.becomeFirstResponder()
        }else if textField == self.txtEmail{
            txtContact.becomeFirstResponder()
        }else if textField == self.txtContact{
            txtContact.resignFirstResponder()
        }else{
            textField.resignFirstResponder()
        }
                return true
    }
    
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
        callWebserviceForCustomerList(andSearchText: "")
        return true
    }
    
    // MARK: - searching operation
    func searchAutocompleteEntries(withSubstring substring: String) {
        if substring != "" {
            strSearchValue = substring
            // to limit network activity, reload half a second after last key press.
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
            self.perform(#selector(self.reload), with: nil, afterDelay: 0.5)
        }
    }
    
    @objc func reload() {
        callWebserviceForCustomerList(andSearchText: strSearchValue)
    }
    func addAccessoryView() -> Void {
        let keyboardDoneButtonView = UIToolbar()
        keyboardDoneButtonView.sizeToFit()
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonTapped))
        doneButton.tintColor = appColor
        keyboardDoneButtonView.items = [flexBarButton, doneButton]
        txtContact?.inputAccessoryView = keyboardDoneButtonView
        txtSearch?.inputAccessoryView = keyboardDoneButtonView
    }
    //
    @objc func doneButtonTapped() {
        // do you stuff with done here
        self.viewTable.isHidden = true
        self.txtContact?.resignFirstResponder()
        self.txtSearch?.resignFirstResponder()
    }
}

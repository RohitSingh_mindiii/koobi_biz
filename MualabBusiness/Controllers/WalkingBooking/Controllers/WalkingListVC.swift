//
//  WalkingListVC.swift
//  MualabBusiness
//
//  Created by Mindiii on 5/30/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit

class WalkingListVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {
 
    
    @IBOutlet weak var tblCustomerList:UITableView!
    @IBOutlet weak var txtSearchField:UITextField!
    @IBOutlet weak var lblNoDataFound:UILabel!

    @IBOutlet weak var txtFirstName:UITextField!
    @IBOutlet weak var txtLastName:UITextField!
    @IBOutlet weak var txtEmail:UITextField!
    @IBOutlet weak var txtContactNo:UITextField!
    
    @IBOutlet weak var viewEditInfo:UIView!

     var arrCustomerList = [ModelWalkingBooking]()
    var arrFilteredCustome = [ModelWalkingBooking]()
     var strSearchText = ""
    var objHoldData = ModelWalkingBooking(dict: ["":""])
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewEditInfo.isHidden = true

        self.txtSearchField.delegate = self
        self.tblCustomerList.delegate = self
        self.tblCustomerList.dataSource = self
        self.tblCustomerList.reloadData()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.callWebserviceForCustomerList()
        self.viewEditInfo.isHidden = true

        self.tblCustomerList.reloadData()
    }
    
    @IBAction func btnBack(_ sender:Any){
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnCancleEditInfo(_ sender:Any){
        self.view.endEditing(true)
       self.viewEditInfo.isHidden = true
    }
    @IBAction func btnContinew(_ sender:Any){
        self.view.endEditing(true)
        self.txtFirstName.text = self.txtFirstName.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        self.txtLastName.text = self.txtLastName.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        self.txtEmail.text = self.txtEmail.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        
        if self.txtFirstName.text?.count == 0{
            objAppShareData.shakeTextField(self.txtFirstName)
         }else if self.txtLastName.text?.count == 0{
            objAppShareData.shakeTextField(self.txtLastName)
         }else if self.txtEmail.text?.count == 0{
            objAppShareData.shakeTextField(self.txtEmail)
         }else if !(objValidationManager.isValidateEmail(strEmail: self.txtEmail.text!)){
            let strMessage = message.validation.email
            objAppShareData.showAlert(withMessage: strMessage, type: alertType.banner, on: self)
        }else{
            self.viewEditInfo.isHidden = true
            let dict = ["firstName":self.txtFirstName.text ?? "","lastName":self.txtLastName.text ?? "","email":self.txtEmail.text ?? "","id":objHoldData._id]
            self.callWebserviceForUpdateCustomerList(params: dict)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.arrFilteredCustome.count == 0{
            self.lblNoDataFound.isHidden = false
        }else{
            self.lblNoDataFound.isHidden = true
        }
        return  self.arrFilteredCustome.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "cellWalkingCustomerList", for: indexPath) as? cellWalkingCustomerList{
            let obj = self.arrFilteredCustome[indexPath.row]
            cell.lblContactNo.text = obj.phone
            cell.lblName.text = obj.firstName+" "+obj.lastName
            
            let a = obj.firstName.prefix(1)
            let b = obj.lastName.prefix(1)
            let c = (a )+(b)
           cell.lblNameIcon.text = c.uppercased()
            return cell
        }else{
            return UITableViewCell()
        }
    }
    
    
    
    @available(iOS 11.0, *)
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
    {
        let objReminder = self.arrFilteredCustome[indexPath.row]
        let Edit = UIContextualAction(style: .normal, title:  "", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
           self.objHoldData = objReminder
            self.txtFirstName.text = objReminder.firstName
            self.txtLastName.text = objReminder.lastName
            self.txtContactNo.text = objReminder.phone
            self.txtEmail.text = objReminder.email
            self.viewEditInfo.isHidden = false
            success(true)
        })
        
        Edit.image = #imageLiteral(resourceName: "edit")
        Edit.backgroundColor =  #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        return UISwipeActionsConfiguration(actions: [Edit])
    }
}


//MARK: - button extension
extension WalkingListVC{
    func callWebserviceForCustomerList(){
        if !objServiceManager.isNetworkAvailable(){
            return
        }
        
        let userInfo = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]  ?? ["":""]
        let myId = userInfo[UserDefaults.keys.id] as? String ?? ""
        let dict = ["artistId":myId]
        self.arrCustomerList.removeAll()
        self.arrFilteredCustome.removeAll()
        objActivity.startActivityIndicator()
        objServiceManager.requestGet(strURL:WebURL.walkingList, params: dict as [String : AnyObject], success: { response in
            let  status = response["status"] as? String ?? ""
            print(response)
            
            objActivity.stopActivity()
            if status == "success"{
                if let arr = response["data"] as? [[String:Any]] {
                    for obj in arr{
                        let newObj = ModelWalkingBooking.init(dict: obj)
                            self.arrCustomerList.append(newObj)
                         self.arrFilteredCustome.append(newObj)
                    }
                    self.tblCustomerList.reloadData()
            }
            }
        }) { error in
            objActivity.stopActivity()
            self.tblCustomerList.reloadData()
        }
}
    
    
    func callWebserviceForUpdateCustomerList(params:[String:Any]){
            if !objServiceManager.isNetworkAvailable(){
                return
            }
        objActivity.startActivityIndicator()
        objWebserviceManager.requestPost(strURL: WebURL.updateCustomer, params: params, success: { (response) in
                let  status = response["status"] as? String ?? ""
                print(response)
                objActivity.stopActivity()
                if status == "success"{
                    if let msg = response["message"] as? String{
                        objAppShareData.showAlert(withMessage: "Customer info updated successfully", type: alertType.bannerDark,on: self)
                    }
                   self.callWebserviceForCustomerList()
                }else{
                    if let msg = response["message"] as? String{
                        objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                    }
                }
            }) { error in
                objActivity.stopActivity()
                objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
            }
        }
    
}



// MARK: - UITextfield Delegate
extension WalkingListVC{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        
        let text = textField.text! as NSString
        
        if (text.length == 1)  && (string == "") {
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
            self.strSearchText = ""
            
            getArtistDataWith(andSearchText: "")
        }else{
            var substring: String = textField.text!
            substring = (substring as NSString).replacingCharacters(in: range, with: string)
            substring = substring.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            let a = substring.count
            if a > 20 && substring != ""{
                return false
            }else{
                searchAutocompleteEntries(withSubstring: substring)
            }
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.view.endEditing(true)
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
        self.strSearchText = ""
        getArtistDataWith( andSearchText: "")
        return true
    }
    
    // MARK: - searching operation
    func searchAutocompleteEntries(withSubstring substring: String) {
        if substring != "" {
            self.strSearchText = substring
            // to limit network activity, reload half a second after last key press.
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
            self.perform(#selector(self.reload), with: nil, afterDelay: 0.5)
        }
    }
    
    @objc func reload() {
        self.getArtistDataWith( andSearchText: strSearchText)
    }
    
    func getArtistDataWith(andSearchText: String) {
        
        let filtered = arrCustomerList.filter { $0.fullName.localizedCaseInsensitiveContains(andSearchText ?? "") }
        if andSearchText == ""{
            self.arrFilteredCustome = arrCustomerList
        }else{
            self.arrFilteredCustome = filtered
        }
        self.tblCustomerList.reloadData()
        if self.arrFilteredCustome.count > 0{
            self.tblCustomerList.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
        }
    }
}

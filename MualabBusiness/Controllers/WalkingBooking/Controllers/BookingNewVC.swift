//
//  ArtistServicesVC.swift
//  MualabBusiness
//
//  Created by mac on 29/05/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import DropDown
import HCSStarRatingView
import FSCalendar

class BookingNewVC : UIViewController,UITableViewDelegate,UITableViewDataSource , UICollectionViewDelegate, UICollectionViewDataSource,FSCalendarDataSource, FSCalendarDelegate, UIGestureRecognizerDelegate,FSCalendarDelegateAppearance{
    var arrToShow = [TimeSlot]()
    var somedays : Array = [Int]()

    fileprivate var item = 0
    fileprivate var arrServices : [Service] = []
    fileprivate var arrSubServices1 : [SubService] = []
    fileprivate var arrSubSubServicesInCall : [SubSubService] = []
    fileprivate var arrSubSubServicesOutCall : [SubSubService] = []
    var objServiceForBooking = SubSubService.init(dict: [:])
    var arrSelectedService = [BookingServices]()
    var objBookingServices = BookingServices()
    var objArtistDetails = ArtistDetails(dict: ["":""])
    var objSelectedCustomerDetails = ModelWalkingBooking(dict: ["":""])
    fileprivate var arrInCallStaff : [StaffServiceNew] = []
    fileprivate var arrOutCallStaff : [StaffServiceNew] = []
    var newDate = Date()
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var lblNoDataFound: UILabel!
    @IBOutlet weak var viewDetail: UIView!
    
    var strInCallOrOutCallFromService = ""
    fileprivate var arrOpeningTimes = [openingTimes]()
    //user detail outlat
    
    //table outlat
    @IBOutlet weak var tblBookingList: UITableView!
    
    fileprivate var kOpenSectionTag: Int = 0
    fileprivate var arrCategories = [String]()
    fileprivate var arrSubCategories=[String]()
    fileprivate var expandedSectionHeaderNumber: Int = -1
    
    //// New service module
    let dropDown = DropDown()
    fileprivate var isBusinessType = true
    @IBOutlet weak var scrollViewMain: UIScrollView!
    @IBOutlet weak var lblBusinessType: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var viewTable: UIView!
    @IBOutlet weak var btnBusinessType: UIButton!
    @IBOutlet weak var btnCategory: UIButton!
    @IBOutlet weak var imgBusinessTypeDown: UIImageView!
    @IBOutlet weak var imgCategoryDown: UIImageView!
    //@IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblUserContactNumber: UILabel!

    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var viewBottumTable: UIView!
    @IBOutlet weak var lblTableHeader: UILabel!
    @IBOutlet weak var HeightViewBottumTable: NSLayoutConstraint!

  //  @IBOutlet weak var lblReviewCount: UILabel!
 //   @IBOutlet weak var lblTodayOpeningTime: UILabel!
    
   // @IBOutlet weak var viewRating: HCSStarRatingView!
   // @IBOutlet weak var imgForOutCall: UIImageView!
    //@IBOutlet weak var viewForOutCall: UIView!
    fileprivate var isOutCallSelected = false
    @IBOutlet weak var heightTableView: NSLayoutConstraint!
    ////
    
    //Calendar
  //  @IBOutlet weak var btnForOutCall: UIButton!
    @IBOutlet weak var viewTimeSlots: UIView!
    @IBOutlet weak var imgVwDropDown: UIImageView!
    var dateSelected : Date = Date()
    @IBOutlet weak var calendar: FSCalendar!
    @IBOutlet weak var calendarHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightTableBookingConstraint: NSLayoutConstraint!
    @IBOutlet weak var dataHeightConstraint: NSLayoutConstraint!
    var selectedIndex = -1
    @IBOutlet weak var lblSelectedDete: UILabel!

    fileprivate lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        
        formatter.dateFormat = "yyyy/MM/dd"
        return formatter
    }()
    
    fileprivate lazy var scopeGesture: UIPanGestureRecognizer = {
        [unowned self] in
        let panGesture = UIPanGestureRecognizer(target: self.calendar, action: #selector(self.calendar.handleScopeGesture(_:)))
        panGesture.delegate = self
        panGesture.minimumNumberOfTouches = 1
        panGesture.maximumNumberOfTouches = 2
        return panGesture
        }()
    
    //Calendar
    @IBOutlet weak var lblSelectTime: UILabel!
    @IBOutlet weak var lblNoSlotFound: UILabel!
    @IBOutlet weak var viewStaff: UIView!
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var collectionVw: UICollectionView!
    @IBOutlet weak var collectionStaff: UICollectionView!

    //MARK: - refreshControl
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(self.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1)
        return refreshControl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewBottumTable.isHidden = true
        self.tblView.delegate = self
        self.tblView.dataSource = self
        // Register to receive notification in your class
        NotificationCenter.default.addObserver(self, selector: #selector(self.showBookingSessionAlertMessage), name: NSNotification.Name(rawValue: "SessionExpiredNotification"), object: nil)
        self.lblNoDataFound.isHidden = true
        self.viewDetail.isHidden = false;  self.viewTable.isHidden = false
        self.tblBookingList.delegate = self
        self.tblBookingList.dataSource = self
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        objLocationManager.getCurrentLocation()
        let nib = UINib(nibName: "CategoriesHeaderAddStaff", bundle: nil)
        self.tblBookingList.register(nib, forHeaderFooterViewReuseIdentifier: "CategoriesHeaderAddStaff")
        self.tblBookingList.reloadData()
        //callWebserviceForGetServices()
        self.configureCalendar()
        self.changeCalenderHeader(date: Date())
        dateSelected = Date()
        self.addGesturesToView()
    }
    func addGesturesToView() -> Void {
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.topToBottomSWIPE))
        swipeRight.direction = .down
        self.viewBottumTable.addGestureRecognizer(swipeRight)
    }
    
    @objc func topToBottomSWIPE() ->Void {
        self.view.endEditing(true)
        self.viewBottumTable.isHidden = true
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        self.scrollViewMain.contentOffset.y = 0
        self.view.layoutIfNeeded()
        
        if objAppShareData.isBookingFromService{
           self.objServiceForBooking = objAppShareData.objServiceForEditBooking
        }
        self.viewStaff.isHidden = true
        self.viewTimeSlots.isHidden = true
        
        kOpenSectionTag = 0
        item = 0
        expandedSectionHeaderNumber = -1
        self.tblBookingList.reloadData()
        self.lblNoDataFound.isHidden = true
        self.viewDetail.isHidden = false;  self.viewTable.isHidden = false

        self.callWebserviceForGetServices()
        
        //self.dataHeightConstraint.constant = self.view.bounds.height - 30
        dateSelected = Date()
        self.calendar.scope = .week
        self.setCalenderScopeWeek()
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        kOpenSectionTag = 0
        expandedSectionHeaderNumber = -1
        self.tblBookingList.reloadData()
        self.lblNoDataFound.isHidden = true
        self.viewDetail.isHidden = false;  self.viewTable.isHidden = false

        callWebserviceForGetServices()
    }
}

//MARK: - button extension
fileprivate extension BookingNewVC {
    func configureView(){
 
        self.lblUserName.text = objSelectedCustomerDetails.firstName+" "+objSelectedCustomerDetails.lastName
        
        self.lblUserContactNumber.text = self.objSelectedCustomerDetails.phone

        if self.arrSubSubServicesOutCall.count>0{
            //self.viewForOutCall.isHidden = false
        }else{
            //self.viewForOutCall.isHidden = false
            //self.viewForOutCall.isHidden = true
        }
        self.manageTableViewHeight()
    }
    
    func manageTableViewHeight(){
        if self.isOutCallSelected{
            if self.arrSubSubServicesOutCall.count<=1{
                self.heightTableView.constant = 90
            }else if self.arrSubSubServicesOutCall.count>=4{
                //self.heightTableView.constant = 270
                self.heightTableView.constant = CGFloat((self.arrSubSubServicesOutCall.count*60) + 30)
            }else{
                self.heightTableView.constant = CGFloat((self.arrSubSubServicesOutCall.count*60) + 30)
            }
        }else{
            if self.arrSubSubServicesInCall.count<=1{
                self.heightTableView.constant = 90
            }else if self.arrSubSubServicesInCall.count>=4{
                //self.heightTableView.constant = 270
                self.heightTableView.constant = CGFloat((self.arrSubSubServicesInCall.count*60) + 30)
            }else{
                self.heightTableView.constant = CGFloat((self.arrSubSubServicesInCall.count*60) + 30)
            }
        }

        self.view.layoutIfNeeded()
    }
    
    @objc func showBookingSessionAlertMessage(){
        objWebserviceManager.StopIndicator()
        let alertTitle = "Alert"
        let alertMessage = "Booking session has been expired. Please try again."
        let alertController = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
        let action1 = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction) in
          
            let userInfo = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]  ?? ["":""]
            let myId = userInfo[UserDefaults.keys.id] as? String ?? ""
            let params1 = ["artistId":myId,"bookingType":"walking"]
            self.callWebserviceForDeleteUnCompletedBooking(param: params1)
            self.navigationController?.popViewController(animated: true)
        }
        alertController.addAction(action1)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    func callWebserviceForDeleteUnCompletedBooking(param:[String:Any]){
        if !objServiceManager.isNetworkAvailable(){
            return
        }
        objServiceManager.requestPost(strURL:WebURL.deleteClientBookService, params: param, success: { response in
            objAppShareData.stopTimerForHoldBookings()
            objAppShareData.objAppdelegate.clearData()
            objAppShareData.objAppdelegate.gotoTabBar(withAnitmation: false)
            
            
        }) { error in
        }
    } 
}

//MARK: - UITableview delegate
extension BookingNewVC {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
        if self.arrSubSubServicesInCall.count>0 && self.arrSubSubServicesOutCall.count>0{
            return 2
        }else if (self.arrSubSubServicesInCall.count>0 && self.arrSubSubServicesOutCall.count==0) || (self.arrSubSubServicesInCall.count==0 && self.arrSubSubServicesOutCall.count>0){
            return 1
        }else{
           return 0
        }

        return 0
    }
    
    /*  Number of Rows  */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblView{
            if isBusinessType == true{
                self.HeightViewBottumTable.constant = CGFloat(self.arrServices.count*45)
                return self.arrServices.count
            }else{
                self.HeightViewBottumTable.constant = CGFloat(self.arrSubServices1.count*45)
                return self.arrSubServices1.count
            }
        }
        if self.isOutCallSelected{
           return self.arrSubSubServicesOutCall.count
        }else{
           return self.arrSubSubServicesInCall.count
        }
        return 0
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?{
        if tableView == tblView{
            return UITableView()
        }
        var subviewArray = Bundle.main.loadNibNamed("CategoriesHeaderAddStaff", owner: self, options: nil)
        let header = subviewArray?[0] as! CategoriesHeaderAddStaff
        header.lblName.textColor = UIColor.black
        header.lblBotumeLayer.backgroundColor = UIColor.clear
        return header
        //return nil
    }
    
    /* Create Cells */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tblView{
            if isBusinessType == true{
                let cellIdentifier = "CellBottumTableList"
                if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? CellBottumTableList{
                    let obj = self.arrServices[indexPath.row]
                    cell.lblTitle.text = obj.serviceName
                    return cell
                }
                return UITableViewCell()
            }else{
            let cellIdentifier = "CellBottumTableList"
            if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? CellBottumTableList{
                let obj = self.arrSubServices1[indexPath.row]
                cell.lblTitle.text = obj.subServiceName
                return cell
            }
                return UITableViewCell()
            }
            
            
        }else{
        let  cell = tableView.dequeueReusableCell(withIdentifier: "SubServiceCell", for: indexPath) as! SubServiceCell
        cell.indexPath = indexPath
        var objSubService = SubSubService.init(dict: [:])
        if self.isOutCallSelected{
            objSubService = arrSubSubServicesOutCall[indexPath.row]
            cell.lblPrice.text = String(objSubService.outCallPrice)
        }else{
            objSubService = arrSubSubServicesInCall[indexPath.row]
            cell.lblPrice.text = String(objSubService.inCallPrice)
        }
            ////
            let doubleStrNN = String(format: "%.2f", ceil(Double(cell.lblPrice.text!)!))
            cell.lblPrice.text = doubleStrNN
            ////
        cell.lblPrice.text = "£" + cell.lblPrice.text!
        cell.lblName.text = objSubService.subSubServiceName
        if objSubService.isSelected {
           cell.lblName.textColor = UIColor.init(red: 0.0/255.0, green: 211.0/255.0, blue: 201.0/255.0, alpha: 1.0)
            cell.imgTick.isHidden = false
        }else{
            cell.lblName.textColor = UIColor.black
            cell.imgTick.isHidden = true

        }
        cell.lblDuration.text = objSubService.completionTime
        if objSubService.completionTime.contains(":"){
            let arr = objSubService.completionTime.components(separatedBy: ":")
            if arr.count == 2{
                let strHr = arr[0]
                let strMin = arr[1]
                if strHr != "00"{
                    cell.lblDuration.text = strHr + " " + "hr" + " " + strMin + " " + "min"
                }else{
                    cell.lblDuration.text = strMin + " " + "min"
                }
            }
        }
 
        if self.isOutCallSelected{
            if objSubService.isOutCallStaff == 0{
                cell.lblDuration.isHidden = false
                cell.lblPrice.isHidden = false
            }else{
                cell.lblDuration.isHidden = true
                cell.lblPrice.isHidden = true
            }
        }else{
            if objSubService.isInCallStaff == 0{
                cell.lblDuration.isHidden = false
                cell.lblPrice.isHidden = false
            }else{
                cell.lblDuration.isHidden = true
                cell.lblPrice.isHidden = true
            }
        }
        return cell
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tblView{
            self.viewBottumTable.isHidden = true

                let index = indexPath.row
                if self.isBusinessType{
                    self.arrSubSubServicesOutCall.removeAll()
                    self.arrSubSubServicesInCall.removeAll()
                    let obj = self.arrServices[index]
                    self.arrSubServices1 = obj.arrSubServices
                    
                    if self.arrSubServices1.count>0{
                        let objMyCat = self.arrSubServices1[0]
                        
                        for obj in objMyCat.arrInCall{
                            obj.isSelected = false
                        }
                        for obj in objMyCat.arrOutCall{
                            obj.isSelected = false
                        }
                        self.arrSubSubServicesInCall = objMyCat.arrInCall
                        self.arrSubSubServicesOutCall = objMyCat.arrOutCall
                        self.lblCategory.text = objMyCat.subServiceName
                        self.objBookingServices.subServiceId =  objMyCat.subServiceId
                    }else{
                        self.lblCategory.text = "No category available"
                        self.objBookingServices.subServiceId =  0
                    }
                    self.lblBusinessType.text = self.arrServices[index].serviceName
                    DispatchQueue.main.async {
                        self.tblBookingList.reloadData()
                    }
                    self.objBookingServices.serviceId = obj.serviceId
                    if self.arrSubSubServicesOutCall.count == 0 && self.arrSubSubServicesInCall.count == 0{
                        //self.viewTable.isHidden = true
                        self.tblBookingList.isHidden = true
                        self.lblNoDataFound.isHidden = false
                        self.viewDetail.isHidden = true
                        self.viewTable.isHidden = true

                    }else{
                        //self.viewTable.isHidden = false
                        self.tblBookingList.isHidden = false
                        self.lblNoDataFound.isHidden = true
                        self.viewDetail.isHidden = false;  self.viewTable.isHidden = false

                    }
                    if self.arrSubServices1.count == 1 || self.arrSubServices1.count == 0{
                        self.imgCategoryDown.isHidden = true
                        self.btnCategory.isUserInteractionEnabled = false
                    }else{
                        self.imgCategoryDown.isHidden = false
                        self.btnCategory.isUserInteractionEnabled = true
                    }
                }else{
                    let objSub = self.arrSubServices1[indexPath.row]
                    for obj in objSub.arrInCall{
                        obj.isSelected = false
                    }
                    for obj in objSub.arrOutCall{
                        obj.isSelected = false
                    }
                    self.arrSubSubServicesInCall = objSub.arrInCall
                    self.arrSubSubServicesOutCall = objSub.arrOutCall
                    self.lblCategory.text =  self.arrSubServices1[indexPath.row].subServiceName
                    DispatchQueue.main.async {
                        self.tblBookingList.reloadData()
                    }
                    self.objBookingServices.subServiceId =  objSub.subServiceId
                }
                
                self.selectedIndex = -1
                self.dateSelected = Date()
                self.calendar.select(self.dateSelected)
                
                self.arrInCallStaff.removeAll()
                self.arrOutCallStaff.removeAll()
                self.arrToShow.removeAll()
                self.viewTimeSlots.isHidden = true
                self.viewStaff.isHidden = true
                self.objBookingServices.subSubServiceId = 0
                
                if self.isOutCallSelected{
                    if self.arrSubSubServicesOutCall.count == 0 {
                        //self.tblBookingList.isHidden = true
                        self.lblNoDataFound.isHidden = false
                        self.viewDetail.isHidden = true;  self.viewTable.isHidden = true

                    }else{
                        //self.tblBookingList.isHidden = false
                        self.lblNoDataFound.isHidden = true
                        self.viewDetail.isHidden = false;  self.viewTable.isHidden = false

                    }
                }else{
                    if self.arrSubSubServicesInCall.count == 0 {
                        //self.tblBookingList.isHidden = true
                        self.lblNoDataFound.isHidden = false
                        self.viewDetail.isHidden = true;  self.viewTable.isHidden = true

                    }else{
                        //self.tblBookingList.isHidden = false
                        self.lblNoDataFound.isHidden = true
                        self.viewDetail.isHidden = false;  self.viewTable.isHidden = false

                    }
                }
                
                self.arrInCallStaff.removeAll()
                self.arrOutCallStaff.removeAll()
                self.collectionStaff.reloadData()
                self.viewStaff.isHidden = true
                
                self.arrToShow.removeAll()
                self.collectionVw.reloadData()
                if self.arrToShow.count == 0 {
                    self.lblNoSlotFound.isHidden = false
                    self.lblSelectTime.isHidden = true
                    self.collectionVw.isHidden = true
                }else{
                    self.lblNoSlotFound.isHidden = true
                    self.lblSelectTime.isHidden = false
                    self.collectionVw.isHidden = false
                }
                self.manageTableViewHeight()
            
        }else{
        
        if self.isOutCallSelected {
            for obj in self.arrSubSubServicesOutCall{
                obj.isSelected = false
            }
            let obj = self.arrSubSubServicesOutCall[indexPath.row]
            obj.isSelected = true
            self.objBookingServices.price = obj.outCallPrice
            self.objBookingServices.subSubServiceId = obj.subSubServiceId
            self.objBookingServices.completionTime = obj.completionTime
            self.objBookingServices.objSubSubService = obj
        }else{
            for obj in self.arrSubSubServicesInCall{
                obj.isSelected = false
            }
            let obj = self.arrSubSubServicesInCall[indexPath.row]
            obj.isSelected = true
            self.objBookingServices.price = obj.inCallPrice
            self.objBookingServices.subSubServiceId = obj.subSubServiceId
            self.objBookingServices.completionTime = obj.completionTime
            self.objBookingServices.objSubSubService = obj
        }
        ////
        self.objBookingServices.staffId = 0
        ////
        //var serviceTime = ""
        if self.objBookingServices.objSubSubService.isStaff == 0{
            self.objBookingServices.staffId = 0
            self.arrInCallStaff.removeAll()
            self.arrOutCallStaff.removeAll()
            self.collectionStaff.reloadData()
            self.viewStaff.isHidden = true
            self.getlatestSlotDataFor(dateSelected: dateSelected)
        }else{
            self.arrInCallStaff.removeAll()
            self.arrOutCallStaff.removeAll()
            self.collectionStaff.reloadData()
            self.viewStaff.isHidden = false
            self.callWebserviceForGetStaff()
        }
        ////
        self.selectedIndex = -1
        ////
        self.calendar.select(dateSelected)
        self.tblBookingList.reloadData()
        }}
}

//MARK: - button extension
extension BookingNewVC {
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        if self.objArtistDetails.isAlreadyBooked == 1{
            self.showAlertMessage()
        }else{
        self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func btnContinueAction(_ sender: UIButton) {
        if selectedIndex == -1{
            objAppShareData.showAlert(withMessage: "Please select time slot" , type: alertType.bannerDark,on: self)
            return
        }
        if self.objArtistDetails.isAlreadyBooked == 1{
            if self.objBookingServices.objSubSubService.subSubServiceId == 0{
                self.goToConfirmBookingScreen()
            }else if self.objBookingServices.bookingTime.count == 0{
                self.goToConfirmBookingScreen()
            }else{
                self.callWebserviceForBookArtist()
            }
        }else{
            if self.objBookingServices.objSubSubService.subSubServiceId == 0{
                objAppShareData.showAlert(withMessage: "Please select service" , type: alertType.bannerDark,on: self)
            }else if self.objBookingServices.bookingTime.count == 0{
                objAppShareData.showAlert(withMessage: "Please select time slot" , type: alertType.bannerDark,on: self)
            }else{
                self.callWebserviceForBookArtist()
            }
        }
    }
//    @IBAction func btnProfileAction(_ sender: UIButton) {
//    }
    @IBAction func btnOutCallAction(_ sender: UIButton) {
        if self.objArtistDetails.isAlreadyBooked == 1{
            //objAppShareData.showAlert(withMessage: "Not able to change service type" , type: alertType.bannerDark,on: self)
            return
        }
        self.arrInCallStaff.removeAll()
        self.arrOutCallStaff.removeAll()
        self.arrToShow.removeAll()
        self.viewTimeSlots.isHidden = true
        self.viewStaff.isHidden = true
        self.selectedIndex = -1
        self.dateSelected = Date()
        self.calendar.select(dateSelected)
        self.objBookingServices.subSubServiceId = 0
        
        if self.isOutCallSelected{
          //  self.imgForOutCall.image = UIImage.init(named:"inactiveBlack_check_box_ico")
            self.isOutCallSelected = false
            for obj in self.arrSubSubServicesOutCall{
                obj.isSelected = false
            }
            if self.arrSubSubServicesInCall.count>0{
               self.lblNoDataFound.isHidden = true
                self.viewDetail.isHidden = false;  self.viewTable.isHidden = false

            }else{
                self.lblNoDataFound.isHidden = false
                self.viewDetail.isHidden = true;  self.viewTable.isHidden = true

            }
        }else{
           // self.imgForOutCall.image = UIImage.init(named:"activeBlack_check_box_ico")
            self.isOutCallSelected = true
            for obj in self.arrSubSubServicesInCall{
                obj.isSelected = false
            }
            if self.arrSubSubServicesOutCall.count>0{
                self.lblNoDataFound.isHidden = true
                self.viewDetail.isHidden = false;  self.viewTable.isHidden = false

            }else{
                self.lblNoDataFound.isHidden = false
                self.viewDetail.isHidden = true;  self.viewTable.isHidden = true

            }
        }
        
        self.arrInCallStaff.removeAll()
        self.arrOutCallStaff.removeAll()
        self.collectionStaff.reloadData()
        self.viewStaff.isHidden = true
        self.arrToShow.removeAll()
        self.collectionVw.reloadData()
        if self.arrToShow.count == 0 {
            self.lblNoSlotFound.isHidden = false
            self.lblSelectTime.isHidden = true
            self.collectionVw.isHidden = true
        }else{
            self.lblNoSlotFound.isHidden = true
            self.lblSelectTime.isHidden = false
            self.collectionVw.isHidden = false
        }
        
        DispatchQueue.main.async {
          self.tblBookingList.reloadData()
        }
        self.manageTableViewHeight()
    }
}

//MARK: - callWebserviceForGetServices extension
fileprivate extension BookingNewVC{
    func callWebserviceForGetServices(){
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        objActivity.startActivityIndicator()
   var strUserId = ""
        if  let userInfo = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] {
            if let id = userInfo["_id"] as? String {
                strUserId = id
            }
        }
        let dicParam = [
            "artistId":strUserId,//objAppShareData.selectedOtherIdForProfile
            "userId":objSelectedCustomerDetails._id,
            "bookingType":"walking"
            ] as [String : Any]
        
        objServiceManager.requestPostForJson(strURL: WebURL.artistService, params: dicParam, success: { response in
            print(response)
            self.arrServices.removeAll()
            objActivity.stopActivity()
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else {
                let strSucessStatus = response["status"] as? String ?? ""
                if  strSucessStatus == "success"{
                    objActivity.stopActivity()
                    self.saveData(dict:response)
                }else{
                    let msg = response["message"] as? String ?? ""
                    objAppShareData.showAlert(withMessage: msg , type: alertType.bannerDark,on: self)
                }}
        }) { error in
            objActivity.stopActivity()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
    func parseBusinessHourData(arrFinam: [AddStaffHours]){

    }
    
    func saveData(dict:[String:Any]){
        
        if let dict = dict["artistDetail"] as? [String : Any]{
            self.objArtistDetails = ArtistDetails.init(dict: dict)
            print(dict)
            self.objArtistDetails.responce = dict
            self.objArtistDetails.isAlreadyBooked = dict["isAlreadybooked"] as! Int 
            if (dict["busineshours"] as? [[String : Any]]) != nil{
                let strDate = dateFormatter.string(from: Date())
                let strDay = self.getDayFromSelectedDate(strDate: strDate)
                var dayOld = Int(strDay)
                var strOpeningTime = ""
                let arrHours = dict["busineshours"] as? [[String : Any]]
                
                var staffHours = [AddStaffHours]()
                if let arr = dict["busineshours"] as? [[String : Any]]{
                    for dict in arr{
                        let objSubService = AddStaffHours.init(dict: dict)
                        staffHours.append(objSubService)
                    }
                }
                
               //self.parseBusinessHourData(arrFinam: staffHours)
                for dict in arrHours!{
                    let day = dict["day"] as? Int
                    let strStartTime = dict["startTime"] as? String
                    let strEndTime = dict["endTime"] as? String
                    //dayOld = 5
                    if dayOld == day{
                        if strOpeningTime.count != 0{
                            strOpeningTime = strOpeningTime + " & " + strStartTime! + " - " + strEndTime!
                        }else{
                            strOpeningTime = strStartTime! + " - " + strEndTime!
                        }
                    }
                    
                    var newDay = day! + 2
                    if newDay == 8{
                        newDay = 1
                    }
                    self.somedays.append(newDay)
                }
                self.calendar.reloadData()
//                if strOpeningTime.count>0{
//                   self.lblTodayOpeningTime.text = strOpeningTime
//                }else{
//                   self.lblTodayOpeningTime.text = "N/A"
//                }
            }
        }
        if let dict = dict["artistServices"] as? [[String : Any]]{
            for dicService in dict{
                let obj = Service.init(dict: dicService)
                self.arrServices.append(obj)
            }
            self.arrServices = self.arrServices.sorted(by: { $0.serviceCount > $1.serviceCount})
        }

        var indexBusinessType = 0
        var indexCategory = 0
        var indexSubSubService = 0
        if objAppShareData.isBookingFromService{
            //for objService in self.arrServices{
            for i in 0..<self.arrServices.count {
                let objService = self.arrServices[i]
                if objService.serviceId == self.objServiceForBooking.serviceId{
                   indexBusinessType = i
                }
                for j in 0..<objService.arrSubServices.count {
                    let objSubService = objService.arrSubServices[j]
                    if objSubService.subServiceId == self.objServiceForBooking.subServiceId{
                        indexCategory = j
                    }
                    var objsuSubService = SubSubService.init(dict: [:])
                    if self.strInCallOrOutCallFromService == "In Call" {
                        self.isOutCallSelected = false
                        //self.imgForOutCall.image = UIImage.init(named:"inactiveBlack_check_box_ico")
                        for k in 0..<objSubService.arrInCall.count{
                            objsuSubService = objSubService.arrInCall[k]
                            indexSubSubService = k
                            if objsuSubService.subSubServiceId == self.objServiceForBooking.subSubServiceId{
                                
                                objsuSubService.isSelected = true
                                self.objBookingServices.subSubServiceId = objsuSubService.subSubServiceId
                                self.objBookingServices.completionTime = objsuSubService.completionTime
                                self.objBookingServices.objSubSubService = objsuSubService
                            }
                        }
                    }else{
                        self.isOutCallSelected = true
                     //   self.imgForOutCall.image = UIImage.init(named:"activeBlack_check_box_ico")
                        for k in 0..<objSubService.arrOutCall.count{
                            objsuSubService = objSubService.arrOutCall[k]
                            indexSubSubService = k
                            if objsuSubService.subSubServiceId == self.objServiceForBooking.subSubServiceId{
                                
                                objsuSubService.isSelected = true
                                self.objBookingServices.subSubServiceId = objsuSubService.subSubServiceId
                                self.objBookingServices.completionTime = objsuSubService.completionTime
                                self.objBookingServices.objSubSubService = objsuSubService
                            }
                        }
                    }
                }
            }
        }
    
        if self.arrServices.count > 0{
            if  self.arrServices[0].arrSubServices.count > 0{
                
            }
        }
        
        
    if arrServices.count > 0{
        let objMyData = self.arrServices[indexBusinessType]
        self.lblBusinessType.text = objMyData.serviceName
        self.arrSubServices1.removeAll()
        self.arrSubServices1 = objMyData.arrSubServices
       
        if self.arrSubServices1.count > indexCategory{
        let objMyCat = self.arrSubServices1[indexCategory]
        self.lblCategory.text = objMyCat.subServiceName
        self.arrSubSubServicesInCall = objMyCat.arrInCall
        self.arrSubSubServicesOutCall = objMyCat.arrOutCall
        self.objBookingServices.serviceId = objMyData.serviceId
        self.objBookingServices.subServiceId =  objMyCat.subServiceId
        }
    self.arrSelectedService.append(self.objBookingServices)
        
    DispatchQueue.main.async {
        self.tblBookingList.reloadData()
    }
        
            
            if self.arrServices.count == 1{
                self.imgBusinessTypeDown.isHidden = true
                self.btnBusinessType.isUserInteractionEnabled = false
            }else{
                self.imgBusinessTypeDown.isHidden = false
                self.btnBusinessType.isUserInteractionEnabled = true
            }
            if self.self.arrSubServices1.count == 1{
                self.imgCategoryDown.isHidden = true
                self.btnCategory.isUserInteractionEnabled = false
            }else{
                self.imgCategoryDown.isHidden = false
                self.btnCategory.isUserInteractionEnabled = true
            }
        }else{
            self.lblNoDataFound.isHidden = false
        self.viewDetail.isHidden = true;  self.viewTable.isHidden = true

            self.lblBusinessType.text = "No Business Available"
            self.lblCategory.text = "No Category Available"
            self.imgBusinessTypeDown.isHidden = true
            self.imgCategoryDown.isHidden = true
            self.btnCategory.isUserInteractionEnabled = false
            self.btnBusinessType.isUserInteractionEnabled = false
        }
       
        self.configureView()
        
        if !objAppShareData.isBookingFromService && self.objArtistDetails.isAlreadyBooked != 1{
        if self.objArtistDetails.serviceType == "1"{
         //  self.viewForOutCall.isHidden = true
        }else if self.objArtistDetails.serviceType == "2"{
        //    self.viewForOutCall.isHidden = false
            self.isOutCallSelected = true
//            self.imgForOutCall.image = UIImage.init(named:"activeBlack_check_box_ico")
//            self.btnForOutCall.isUserInteractionEnabled = false
        }else{
//            self.viewForOutCall.isHidden = false
               self.isOutCallSelected = false
//            self.imgForOutCall.image = UIImage.init(named:"inactiveBlack_check_box_ico")
//            self.btnForOutCall.isUserInteractionEnabled = true
        }
        }else if self.objArtistDetails.isAlreadyBooked == 1{
            if self.isOutCallSelected{
            }else{
             //  self.viewForOutCall.isHidden = true
            }
        }
        
        if self.isOutCallSelected{
            if self.arrSubSubServicesOutCall.count == 0 {
                self.lblNoDataFound.isHidden = false
                self.viewDetail.isHidden = true;  self.viewTable.isHidden = true

            }else{
                self.lblNoDataFound.isHidden = true
                self.viewDetail.isHidden = false;  self.viewTable.isHidden = false

            }
        }else{
            if self.arrSubSubServicesInCall.count == 0 {
                self.lblNoDataFound.isHidden = false
                self.viewDetail.isHidden = true;  self.viewTable.isHidden = true

            }else{
                self.lblNoDataFound.isHidden = true
                self.viewDetail.isHidden = false;  self.viewTable.isHidden = false

            }
        }
        
        if objAppShareData.isBookingFromService{
            //objAppShareData.isBookingFromService = false
            if self.objServiceForBooking.strDateForEdit.count>0{
            let formatter  = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            dateSelected = formatter.date(from: self.objServiceForBooking.strDateForEdit)!
            }
            if self.objBookingServices.objSubSubService.isStaff == 0{
                self.arrInCallStaff.removeAll()
                self.arrOutCallStaff.removeAll()
                self.collectionStaff.reloadData()
                self.viewStaff.isHidden = true
                self.getlatestSlotDataFor(dateSelected: dateSelected)
            }else{
                self.arrInCallStaff.removeAll()
                self.arrOutCallStaff.removeAll()
                self.collectionStaff.reloadData()
                self.viewStaff.isHidden = false
                self.callWebserviceForGetStaff()
            }
            ////
            //self.selectedIndex = -1
            ////
            self.calendar.select(dateSelected)
            //self.getlatestSlotDataFor(dateSelected: dateSelected)
        }
    }
}

//MARK:- Collection view Delegate Methods
extension BookingNewVC:UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.collectionStaff{
            if self.isOutCallSelected{
               return self.arrOutCallStaff.count
            }else{
               return self.arrInCallStaff.count
            }
        }else{
          return self.arrToShow.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        
        if collectionView == self.collectionStaff{
            let cell = (collectionView.dequeueReusableCell(withReuseIdentifier: "userCollectionCell", for:
                indexPath) as? userCollectionCell)!
            let deadlineTime = DispatchTime.now() + .seconds(1/4)
            DispatchQueue.main.asyncAfter(deadline: deadlineTime){
                cell.imgProfile.setBound()
            }
            var objSubService = StaffServiceNew.init(dict: [:])
            if !self.isOutCallSelected{
                objSubService = arrInCallStaff[indexPath.row]
                cell.lblPrice.text = String(objSubService.inCallPrice)
            }else{
                objSubService = arrOutCallStaff[indexPath.row]
                cell.lblPrice.text = String(objSubService.outCallPrice)
            }
            ////
            let doubleStrNN = String(format: "%.2f", ceil(Double(cell.lblPrice.text!)!))
            cell.lblPrice.text = doubleStrNN
            ////
            cell.lblPrice.text = "£" + cell.lblPrice.text!
            if objSubService.staffImage != "" {
                if let url = URL(string: objSubService.staffImage){
                    //cell.imgProfile.af_setImage(withURL: url, placeholderImage:#imageLiteral(resourceName: "cellBackground"))
                    cell.imgProfile.sd_setImage(with: url, placeholderImage:#imageLiteral(resourceName: "cellBackground"))
                }
            }else{
                cell.imgProfile.image = #imageLiteral(resourceName: "cellBackground")
            }
            cell.lblUserName.text = objSubService.staffName
            cell.lblTime.text = objSubService.completionTime
            if objSubService.completionTime.contains(":"){
                let arr = objSubService.completionTime.components(separatedBy: ":")
                if arr.count == 2{
                    let strHr = arr[0]
                    let strMin = arr[1]
                    if strHr != "00"{
                        cell.lblTime.text = strHr + " " + "hr" + " " + strMin + " " + "min"
                    }else{
                        cell.lblTime.text = strMin + " " + "min"
                    }
                }
            }
            
            if objSubService.isSelected{
               cell.lblUserName.textColor = appColor
                cell.imgProfile.layer.borderWidth = 2
                cell.imgProfile.layer.borderColor = #colorLiteral(red: 0, green: 0.851996243, blue: 0.8281483054, alpha: 1)

            }else{
                cell.imgProfile.layer.borderWidth = 1.5
                cell.imgProfile.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)

               cell.lblUserName.textColor = UIColor.black
            }
            //cell.btnProfile.accessibilityHint = objSubService.staffName
            //cell.btnProfile.tag = indexPath.row
            //cell.btnProfile.superview?.tag = indexPath.section
        return cell
       
        }else if collectionView == self.collectionVw{
        
            let cell = (collectionView.dequeueReusableCell(withReuseIdentifier: "CalenderCollectionCell", for:
            indexPath) as? CalenderCollectionCell)!
        
        let objTimeSlot = self.arrToShow[indexPath.row]
        cell.lblTime.text = objTimeSlot.strTimeSlot
        
            
            if selectedIndex == -1 {
                cell.imgWatch.image = UIImage.init(named:"watch_ico_black")
                cell.lblTime.textColor = UIColor.black
                cell.vwBg.layer.borderWidth = 1.0
                cell.vwBg.layer.borderColor = UIColor.black.cgColor
                cell.vwBg.backgroundColor = UIColor.white
                cell.isUserInteractionEnabled = true
            }else{
            
            if selectedIndex == indexPath.row {
                cell.imgWatch.image = UIImage.init(named:"watch_ico")
                cell.vwBg.backgroundColor = appColor
                cell.vwBg.layer.borderWidth = 0.0
                cell.lblTime.textColor = UIColor.white
                cell.isUserInteractionEnabled = true
            }else{
                cell.imgWatch.image = UIImage.init(named:"watch_ico_gray")
                cell.lblTime.textColor = UIColor.lightGray
                cell.vwBg.layer.borderWidth = 1.0
                cell.vwBg.layer.borderColor = UIColor.lightGray.cgColor
                cell.vwBg.backgroundColor = UIColor.white
                cell.isUserInteractionEnabled = false
        }
            }
        return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        setCalenderScopeWeek()

        if collectionView == self.collectionStaff{
            var objStaff = StaffServiceNew.init(dict: [:])
            if self.isOutCallSelected{
               objStaff = self.arrOutCallStaff[indexPath.row]
               self.objBookingServices.price = objStaff.outCallPrice
            }else{
               objStaff = self.arrInCallStaff[indexPath.row]
                self.objBookingServices.price = objStaff.inCallPrice
            }
            for obj in self.arrOutCallStaff{
                obj.isSelected = false
            }
            for obj in self.arrInCallStaff{
                obj.isSelected = false
            }
            objStaff.isSelected = true
            self.collectionStaff.reloadData()
            if indexPath.row == 0{
               self.objBookingServices.staffId = 0
            }else{
               self.objBookingServices.staffId = objStaff.staffId
            }
            self.objBookingServices.completionTime = objStaff.completionTime
            selectedIndex = -1
            if self.arrToShow.count>0{
                let index = IndexPath.init(row: 0, section: 0)
                self.collectionVw.scrollToItem(at: index, at: .top, animated: false)
            }
            self.getlatestSlotDataFor(dateSelected: dateSelected)
          //objAppShareData.showAlert(withMessage:"under developement",type: alertType.bannerDark, on: self)
        }else{
            
            
        let objTimeSlot = self.arrToShow[indexPath.row]
        if self.arrSelectedService.count > 0{
            
            let objBookingServices : BookingServices = self.arrSelectedService[0]
            if !objBookingServices.isBooked{
                objBookingServices.bookingDate =  getFormattedBookingDateFrom(dateSelected:dateSelected)
                objBookingServices.bookingDateForServer =  getFormattedBookingDateForServerFrom(dateSelected:dateSelected)
                objBookingServices.bookingTime = objTimeSlot.strTimeSlot
             
                self.tblBookingList.reloadData()
            }else{
                objAppShareData.showAlert(withMessage: "Service Already Added", type: alertType.bannerDark,on: self)
            }
        }
            if selectedIndex == -1{
                selectedIndex = indexPath.row
                self.collectionVw.reloadData()
            }else{
                selectedIndex = -1
                self.objBookingServices.bookingTime.removeAll()
                self.collectionVw.reloadData()
            }
        }
    }
    
    
    
   
    func getFormattedBookingDateForServerFrom(dateSelected : Date) -> String{
        
        let formatter  = DateFormatter()
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        formatter.dateFormat = "yyyy-MM-dd"
        let strDate = formatter.string(from: dateSelected)
        return strDate
    }
    
    func getFormattedBookingDateFrom(dateSelected : Date) -> String{
        
        let formatter  = DateFormatter()
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        formatter.dateFormat = "EE, d LLLL yyyy" //"EE, d LLLL yyyy hh:mm a"
        let strDate = formatter.string(from: dateSelected)
        return strDate
    }
    
    //ScrollView delegate method
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        self.view.endEditing(true)
    }
    
    @objc func btnSelectService(sender: UIButton!) {
        let objMyData  = self.arrServices[sender.tag]
        item = sender.tag
        //self.collectionView.reloadData()
        self.arrSubServices1.removeAll()
        self.arrSubServices1 = objMyData.arrSubServices
        self.tblBookingList.reloadData()
    }
}

//MARK:- New Service Module
extension BookingNewVC{
    @IBAction func btnDropDownAction(_ sender: UIButton) {
        if sender.tag == 0{
            isBusinessType = true
        }else{
            isBusinessType = false
        }
        
        var objGetData : [String] = []
        if isBusinessType{
            self.lblTableHeader.text = "Business Type"
            objGetData = self.arrServices.map { $0.serviceName }
            if self.arrServices.count == 0{
                objAppShareData.showAlert(withMessage: "No business type found", type: alertType.bannerDark,on: self)
                return
            }
        }else{
            self.lblTableHeader.text = "Category Type"
            objGetData = self.arrSubServices1.map { $0.subServiceName }
            if self.arrSubServices1.count == 0{
                objAppShareData.showAlert(withMessage: "No category found", type: alertType.bannerDark,on: self)
                return
            }
        }
        self.viewBottumTable.isHidden = false
        self.tblView.reloadData()
        }
    
    
    
    
    func configureCalendar(){
        if UIDevice.current.model.hasPrefix("iPad") {
            self.calendarHeightConstraint.constant = 400
        }
        self.calendar.delegate = self
        self.calendar.dataSource = self
        self.calendar.calendarHeaderView.isHidden = true
        self.calendar.scope = .week
        self.calendar.accessibilityIdentifier = "calendar"
    }
    
//    func changeCalenderHeader(date : Date){
//        let fmt = DateFormatter()
//        fmt.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
    
//        fmt.dateFormat = "dd MMMM yyyy"
//        let strCalenderHeader = fmt.string(from: date)
//        self.lblSelectedDete.text = strCalenderHeader
    
        
        
        func changeCalenderHeader(date : Date){
            let fmt = DateFormatter()
            fmt.dateFormat = "dd MMMM yyyy"
            let strCalenderHeader = fmt.string(from: date)
            let newDateForCheck = fmt.string(from: newDate)
            
            if strCalenderHeader == newDateForCheck{
                self.lblSelectedDete.text = strCalenderHeader
            }else{
                fmt.dateFormat = "MMMM yyyy"
                let strCalenderHeader2 = fmt.string(from: date)
                self.lblSelectedDete.text = strCalenderHeader2
            }
        }
    

    func setCalenderScopeWeek(){
        self.calendar.setScope(.week, animated: true)
        UIView.animate(withDuration: 0.4, animations: {
            self.imgVwDropDown.transform = CGAffineTransform(rotationAngle: (0.0 * CGFloat(Double.pi)) / 180.0)
        })
    }
    
    func setCalenderScopeMonth(){
 
        self.calendar.setScope(.month, animated: true)
        UIView.animate(withDuration: 0.4, animations: {
            self.imgVwDropDown.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) / 180.0)
        })
    }
    
    @IBAction func toggleCalenderAction(sender: AnyObject) {
        if self.calendar.scope == .month {
            setCalenderScopeWeek()
        } else {
            setCalenderScopeMonth()
        }
    }
    
    @IBAction func btnPreviousAction(sender: AnyObject) {
        let _calendar = Calendar.current
        var dateComponents = DateComponents()
        
        if self.calendar.scope == .month{
            dateComponents.month = -1 // For prev button -1, For next button 1
        }else{
            dateComponents.weekOfMonth = -1 // For prev button -1, For next button 1
        }
        
        calendar.currentPage = _calendar.date(byAdding: dateComponents, to: calendar.currentPage)!
        self.calendar.setCurrentPage(calendar.currentPage, animated: true)
    }
    
    @IBAction func btnNextAction(sender: AnyObject) {
        let _calendar = Calendar.current
        var dateComponents = DateComponents()
        if self.calendar.scope == .month{
            dateComponents.month = 1 // For prev button -1, For next button 1
        }else{
            dateComponents.weekOfMonth = 1 // For prev button -1, For next button 1
        }
        calendar.currentPage = _calendar.date(byAdding: dateComponents, to: calendar.currentPage)!
        self.calendar.setCurrentPage(calendar.currentPage, animated: true)
    }
    
    @IBAction func btnTodayClicked(_ sender: UIButton) {
        self.clickedTodayAction(firstTime:true)
        self.clickedTodayAction(firstTime:false)
    }
    
    func clickedTodayAction(firstTime:Bool){
        self.newDate = Date()
        if self.objBookingServices.subSubServiceId == 0{
            objAppShareData.showAlert(withMessage: "Please select service", type: alertType.bannerDark, on: self)
            return
        }
        selectedIndex = -1
        self.calendar.setCurrentPage(Date(), animated: false)
        dateSelected = Date()
        self.calendar.select(Date())
        
        if self.calendar.scope == .month {
            UIView.animate(withDuration: 0.4, animations: {
                //self.calendar.setCurrentPage(Date(), animated: false)
                self.setCalenderScopeWeek()
            })
        }
        if firstTime == true{
        self.callWebserviceForGetStaff()
        self.getlatestSlotDataFor(dateSelected: Date())
        }
        //self.clearFirstServiceSelectedTimeDate()
    }
    
}

// MARK:- FSCalendar delegate

extension BookingNewVC{
 
    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
        
        UIView.animate(withDuration: 0.6, animations: {
            DispatchQueue.main.async {
                self.calendarHeightConstraint.constant = bounds.height
                self.view.layoutIfNeeded()
            }
        })
        
    }
    
    /**
     Tells the delegate a date in the calendar is selected by tapping.
     */
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        let strDate1 = self.dateFormatter.string(from: date)
        let date1 = self.dateFormatter.date(from: strDate1)
        
        let strDate2 = self.dateFormatter.string(from: Date())
        let date2 = self.dateFormatter.date(from: strDate2)
        
        if(date1! == date2!){
            self.getlatestSlotDataFor(dateSelected: Date())
        }else{
            self.getlatestSlotDataFor(dateSelected: date)
        }
        selectedIndex = -1
        dateSelected = date
        self.callWebserviceForGetStaff()
        //self.clearFirstServiceSelectedTimeDate()
        
        ////// let selectedDates = calendar.selectedDates.map({self.dateFormatter.string(from: $0)})
        
        if monthPosition == .next || monthPosition == .previous {
            calendar.setCurrentPage(date, animated: true)
        }
        calendar.setCurrentPage(date, animated: true)
        if self.calendar.scope == .month {
            UIView.animate(withDuration: 0.4, animations: {
                //self.calendar.setCurrentPage(Date(), animated: false)
                self.setCalenderScopeWeek()
            })
        }
    }
    
    
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, fillDefaultColorFor date: Date) -> UIColor? {
        let myCalendar = Calendar(identifier: .gregorian)
        let weekDay = myCalendar.component(.weekday, from: date)
        if !somedays.contains(weekDay)
        {
            return UIColor.init(red: 153.0/255.0, green: 153.0/255.0, blue: 153.0/255.0, alpha: 1.0)
        }
        else{
            return nil
        }
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, titleDefaultColorFor date: Date) -> UIColor? {
        
        //        self.somedays = ["2017/06/03",
        //                    "2017/06/06",
        //                    "2017/06/12",
        //                    "2017/06/25"]
        let dateString : String = self.dateFormatter.string(from:date)
        let myCalendar = Calendar(identifier: .gregorian)
        let weekDay = myCalendar.component(.weekday, from: date)
        let day = myCalendar.component(.day, from: date)
        if !somedays.contains(weekDay)
        {
            return UIColor.white
        }
        else{
            return nil
        }
    }
    
    /**
     Tells the delegate the calendar is about to change the current page.
     */
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        self.changeCalenderHeader(date: calendar.currentPage)
    }
    
    /**
     Close past dates in FSCalendar
     */
    func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool{
        if self.objBookingServices.subSubServiceId == 0{
            objAppShareData.showAlert(withMessage: "Please select service", type: alertType.bannerDark, on: self)
            return false
        }
        let strDate1 = self.dateFormatter.string(from: date)
        let date1 = self.dateFormatter.date(from: strDate1)
        self.newDate = date1!

        let strDate2 = self.dateFormatter.string(from: Date())
        let date2 = self.dateFormatter.date(from: strDate2)
        
        if(date1! < date2!){
            objAppShareData.showAlert(withMessage: "You can't select previous date for booking", type: alertType.bannerDark, on: self)
            return false
        }else{
            return true
        }
    }
    
    @IBAction func btnConfirmBookingAction(_ sender: UIButton) {
  
    }
    
    func getDayFromSelectedDate(strDate:String)-> String{
        
        guard let weekDay = getDayOfWeek(strDate)else { return "" }
        switch weekDay {
        case 1:
            self.calendar.firstWeekday = 6+2
            
            return "6"//"Sun"
        case 2:
            self.calendar.firstWeekday = 0+2
            
            return "0"//Mon"
        case 3:
            self.calendar.firstWeekday = 1+2
            
            return "1"//Tue"
        case 4:
            self.calendar.firstWeekday = 2+2
            
            return "2"//"Wed"
        case 5:
            self.calendar.firstWeekday = 3+2
            
            return "3"//"Thu"
        case 6:
            self.calendar.firstWeekday = 4+2
            
            return "4"//"Fri"
        case 7:
            self.calendar.firstWeekday = 5+2
            
            return "5"//"Sat"
        default:
            self.calendar.firstWeekday = 0
            
            return "Day"
        }
        return ""
    }
    func getDayOfWeek(_ today:String) -> Int? {
        let formatter  = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        guard let todayDate = formatter.date(from: today) else { return nil }
        let myCalendar = Calendar(identifier: .gregorian)
        let weekDay = myCalendar.component(.weekday, from: todayDate)
        return weekDay
    }
    
    func getlatestSlotDataFor(dateSelected : Date){
        
        var str_latitude = ""
        var str_longitude = ""
        if objArtistDetails.isOutCallSelected{
            str_latitude =  objLocationManager.strlatitude ?? ""
            str_longitude = objLocationManager.strlongitude ?? ""
        }else{
            str_latitude =  objArtistDetails.latitude
            str_longitude = objArtistDetails.longitude
        }
        
        str_latitude =  objLocationManager.strlatitude ?? ""
        str_longitude = objLocationManager.strlongitude ?? ""
        
        let formatter  = DateFormatter()
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        formatter.dateFormat = "dd-MM-yyyy hh:mm a"// "yyyy-MM-dd"
        let strCurrentDate = formatter.string(from: dateSelected)
        
 
        let strSelectedTime = self.getTimeFromDate(strDate: strCurrentDate) //"hh:mm a"
        let strSelectedDate = self.getFormattedDateFromDate(strDate: strCurrentDate) //"yyyy-MM-dd"
        let strSelectedDay = self.getDayFromSelectedDate(strDate: strSelectedDate) //"0" = Mon"
        
        let totalMin = getTotalServiceTime()
        let strTotalServiceTime = "00:\(totalMin)"
        
        var strUserId : String = ""
        if  let userInfo = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] {
        if let id = userInfo["_id"] as? String {
            strUserId = id
        }
        }else{
               let userInfo = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]  ?? ["":""]
            if let id = userInfo["_id"] as? String {
                strUserId = id
            }
            
            
        }
        
        let strStaffId  = objBookingServices.staffId == 0 ? "0" : "\(objBookingServices.staffId)"
        
        var str_bookingId = ""
        var str_type = ""
        var bookStaffId = ""
        
 
            if self.arrSelectedService.count > 0{
                let objBookingServices : BookingServices = self.arrSelectedService[0]
                str_bookingId = "\(objBookingServices.bookingId)"
            }
            if str_bookingId == "0"{
                str_bookingId = ""
                str_type = ""
            }
       
            bookStaffId = objBookingServices.bookStaffId == 0 ? "" : "\(objBookingServices.bookStaffId)"
      //  }
        
        
        let parameters : Dictionary = [
        "artistServiceId":self.objBookingServices.objSubSubService.subSubServiceId,
            "artistId" : objArtistDetails._id,
            "day" : strSelectedDay,
            "date" : strSelectedDate,
            "currentTime" : strSelectedTime,
            "serviceTime" : strTotalServiceTime,
            //"serviceTime" : "03:30",
            "userId" : objSelectedCustomerDetails._id,//strUserId,
            "latitude" : str_latitude,
            "longitude" : str_longitude,
            "businessType" : objArtistDetails.businessType,
            "staffId" : strStaffId ,  //>> artistId
            "bookingTime" : "",
            "bookingDate" : "",
            "bookingType" : "walking",
            "bookingCount": "",
            "type" : str_type, //
            "bookingId" : str_bookingId, //
            "bookStaffId": bookStaffId, //old staff id id edit
            ] as [String : Any]
        print(parameters)
        self.callWebserviceForGet_ArtistTimeSlot(dict: parameters)
    }
    
    func getTotalServiceTime()-> Int{
        
        var totalMin = 0
        
        if self.arrSelectedService.count > 0{
            
            let objBookingServices : BookingServices = self.arrSelectedService[0]
            if self.isOutCallSelected{
                
                totalMin = getTotalServiceTime(strPreprationTime: objArtistDetails.outCallpreprationTime, strComplitionTime: objBookingServices.completionTime)
            }else{
                
                totalMin = getTotalServiceTime(strPreprationTime: objArtistDetails.inCallpreprationTime, strComplitionTime: objBookingServices.completionTime)
                
            }
        }
        return totalMin
    }
    func getTotalServiceTimeForBiz()-> Int{
        
        var totalMin = 0
        
        if self.arrSelectedService.count > 0{
            
            let objBookingServices : BookingServices = self.arrSelectedService[0]
            if self.isOutCallSelected{
                
                totalMin = getTotalServiceTime(strPreprationTime: "0", strComplitionTime: objBookingServices.completionTime)
            }else{
                
                totalMin = getTotalServiceTime(strPreprationTime: "0", strComplitionTime: objBookingServices.completionTime)
                
            }
        }
        return totalMin
    }
    func getEndTimeForStartTime(strStartTime:String)-> String{
        
        var strEndTime : String = ""
        let totalMin = getTotalServiceTime()
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        dateFormatter.dateFormat = "hh:mm a"
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        
        if let date = dateFormatter.date(from: strStartTime){
            // Start: Optional(2000-01-01 19:00:00 +0000)
            let calendar = Calendar.current
            if let date1 = calendar.date(byAdding: .minute, value: totalMin, to: date){
                strEndTime = dateFormatter.string(from: date1)
            }
        }
        return strEndTime
    }
    func getEndTimeForStartTimeForBiz(strStartTime:String)-> String{
        
        var strEndTime : String = ""
        let totalMin = getTotalServiceTimeForBiz()
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        dateFormatter.dateFormat = "hh:mm a"
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        
        if let date = dateFormatter.date(from: strStartTime){
            // Start: Optional(2000-01-01 19:00:00 +0000)
            let calendar = Calendar.current
            if let date1 = calendar.date(byAdding: .minute, value: totalMin, to: date){
                strEndTime = dateFormatter.string(from: date1)
            }
        }
        return strEndTime
    }
    func getTotalServiceTime(strPreprationTime:String, strComplitionTime:String)-> Int{
        let preprationTime :Int = getTimeInMinutFrom(strTime: strPreprationTime)
        let complitionTime :Int = getTimeInMinutFrom(strTime: strComplitionTime)
        let totalMin = preprationTime + complitionTime
        return totalMin
    }
    
    func getTimeInMinutFrom(strTime:String)-> Int{
        var TotalMin = 0
        var arrTime = strTime.components(separatedBy: ":")
        if arrTime.count >= 2{
            let strHours = arrTime[0]
            let strMin = arrTime[1]
            TotalMin =  Int(strHours)! * 60 + Int(strMin)!
        }
        return TotalMin
    }
    
    func callWebserviceForGet_ArtistTimeSlot(dict: [String : Any]){
        print(dict)
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        objActivity.startActivityIndicator()
        objWebserviceManager.requestPost(strURL: WebURL.artistTimeSlotNew, params: dict  , success: { response in
            
            objWebserviceManager.StopIndicator()
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else{
                
                let strStatus =  response["status"] as? String ?? ""
                let strMsg = response["message"] as? String ?? kErrorMessage
                if strStatus == k_success{
                    self.parseResponce(response:response)
                }else{
                    self.objBookingServices.bookingId = 0
                    
                    objAppShareData.showAlert(withMessage: strMsg, type: alertType.bannerDark,on: self)
                }
            }
        }){ error in
            self.objBookingServices.bookingId = 0
            objWebserviceManager.StopIndicator()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
    func parseResponce(response:[String : Any]){
        self.objBookingServices.bookingId = 0
        if let bookingId = response["bookingId"] as? Int{
           self.objBookingServices.bookingId = bookingId
        }
        self.arrToShow.removeAll()
        if let arr = response["timeSlots"] as? [String]{
            for strTimeSlot in arr{
                let objTimeSlot = TimeSlot.init(isSelected: false, strTimeSlot: strTimeSlot)
                self.arrToShow.append(objTimeSlot!);
            }
        }
        
        ////
        if objAppShareData.isBookingFromService{
            for i in 0..<self.arrToShow.count{
                let objSlot = self.arrToShow[i]
                if objSlot.strTimeSlot == self.objServiceForBooking.strSlotForEdit{
                    objSlot.isSelected = true
                    selectedIndex = i
                    self.objServiceForBooking.strSlotForEdit = ""
                }
            }
            objAppShareData.isBookingFromService = false
        }else{
           selectedIndex = -1
        }
        ////
        
        self.viewTimeSlots.isHidden = false
        self.collectionVw.reloadData()
        if self.arrToShow.count == 0 {
            self.lblNoSlotFound.isHidden = false
            self.lblSelectTime.isHidden = true
            self.collectionVw.isHidden = true
        }else{
            self.lblNoSlotFound.isHidden = true
            self.lblSelectTime.isHidden = false
            self.collectionVw.isHidden = false
        }
    }
    
    func getTimeFromDate(strDate:String)-> String{
        
        let formatter  = DateFormatter()
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        formatter.dateFormat = "dd-MM-yyyy hh:mm a"// "yyyy-MM-dd"
        guard let todayDate = formatter.date(from: strDate) else { return "" }
        formatter.dateFormat = "hh:mm a"
        let strTime = formatter.string(from: todayDate)
        
        return strTime
    }
    func getFormattedDateFromDate(strDate:String)-> String{
        
        let formatter  = DateFormatter()
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        formatter.dateFormat = "dd-MM-yyyy hh:mm a"// "yyyy-MM-dd"
        guard let todayDate = formatter.date(from: strDate) else { return "" }
        formatter.dateFormat = "yyyy-MM-dd"
        let strDate = formatter.string(from: todayDate)
        
        return strDate
    }
    
    func callWebserviceForGetStaff(){
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        objActivity.startActivityIndicator()
        
        let formatter  = DateFormatter()
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        formatter.dateFormat = "dd-MM-yyyy hh:mm a"// "yyyy-MM-dd"
        let strCurrentDate = formatter.string(from: dateSelected)
        let strSelectedDate = self.getFormattedDateFromDate(strDate: strCurrentDate) //"yyyy-MM-dd"
        let strSelectedDay = self.getDayFromSelectedDate(strDate: strSelectedDate)
        
        let dicParam = [
            "day":strSelectedDay,
            "staffId":"",
            "businessId":self.objArtistDetails._id,
        "artistServiceId":self.objBookingServices.objSubSubService.subSubServiceId
            ] as [String : Any]
        print(dicParam)
        objServiceManager.requestPostForJson(strURL: WebURL.serviceStaff, params: dicParam, success: { response in
            print(response)
            self.arrInCallStaff.removeAll()
            self.arrOutCallStaff.removeAll()
            objActivity.stopActivity()
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else {
                let strSucessStatus = response["status"] as? String ?? ""
                if  strSucessStatus == "success"{
                    objActivity.stopActivity()
                    self.saveDataStaff(dict:response)
                }else{
                }}
        }) { error in
            objActivity.stopActivity()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
    func saveDataStaff(dict:[String:Any]){
//        let dictService = dict["serviceInfo"] as! [String:Any]
//        self.objService.serviceId = (dictService["serviceId"] as? Int)!
//        self.objService.subServiceId = (dictService["subserviceId"] as? Int)!
//        self.objService.artistId = (dictService["artistId"] as? Int)!
        
        let arrStaff = dict["staffInfo"] as! [[String:Any]]
        for dictStaff in arrStaff{
            let objStaff = StaffServiceNew.init(dict: dictStaff)
            let strType = dictStaff["bookingType"] as? String ?? ""
            if strType == "Incall"{
                self.arrInCallStaff.append(objStaff)
            }else if strType == "Outcall"{
                self.arrOutCallStaff.append(objStaff)
            }else if strType == "Both"{
                self.arrInCallStaff.append(objStaff)
                self.arrOutCallStaff.append(objStaff)
            }
        }
        print(self.arrInCallStaff.count)
        print(self.arrOutCallStaff.count)
        if self.isOutCallSelected{
            if self.arrOutCallStaff.count == 1{
               self.arrOutCallStaff.removeAll()
               self.viewStaff.isHidden = true
            }else{
                if objAppShareData.isBookingFromService && self.objServiceForBooking.staffIdForEdit > 0{
                    for i in 0..<arrOutCallStaff.count{
                        let obj = arrOutCallStaff[i]
                        if obj.staffId == self.objServiceForBooking.staffIdForEdit{
                            obj.isSelected = true
                            self.objBookingServices.price = obj.outCallPrice
                            if i == 0{
                                self.objBookingServices.staffId = 0
                            }else{
                                self.objBookingServices.staffId = obj.staffId
                            }
                            self.objServiceForBooking.staffIdForEdit = 0
                        }else{
                            //obj.isSelected = false
                        }
                    }
                }else{
                    let obj = arrOutCallStaff[0]
                    obj.isSelected = true
                    self.objBookingServices.price = obj.outCallPrice
                }
                self.viewStaff.isHidden = false
            }
        }else{
            if self.arrInCallStaff.count == 1{
                self.arrInCallStaff.removeAll()
                self.viewStaff.isHidden = true
            }else{
                if objAppShareData.isBookingFromService && self.objServiceForBooking.staffIdForEdit > 0{
                    for i in 0..<arrInCallStaff.count{
                        let obj = arrInCallStaff[i]
                        if obj.staffId == self.objServiceForBooking.staffIdForEdit{
                            obj.isSelected = true
                            self.objBookingServices.price = obj.inCallPrice
                            if i == 0{
                                self.objBookingServices.staffId = 0
                            }else{
                                self.objBookingServices.staffId = obj.staffId
                            }
                            self.objServiceForBooking.staffIdForEdit = 0
                        }else{
                            //obj.isSelected = false
                        }
                    }
                }else{
                    let obj = arrInCallStaff[0]
                    obj.isSelected = true
                    self.objBookingServices.price = obj.inCallPrice
                }
                self.viewStaff.isHidden = false
            }
        }
        self.collectionStaff.reloadData()
        self.getlatestSlotDataFor(dateSelected: dateSelected)
    }
    
    func callWebserviceForBookArtist(){
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        if self.isOutCallSelected{
            self.strInCallOrOutCallFromService = "Out Call"
        }else{
            self.strInCallOrOutCallFromService = "In Call"
        }
        objActivity.startActivityIndicator()
        self.objBookingServices.endTime = getEndTimeForStartTime(strStartTime: self.objBookingServices.bookingTime)
        var endTimeBiz = getEndTimeForStartTimeForBiz(strStartTime: self.objBookingServices.bookingTime)
 
        var serviceType = 0
        var price = 0.0
        price = self.objBookingServices.price
        if self.isOutCallSelected{
            serviceType = 2
            //price = self.objBookingServices.objSubSubService.outCallPrice
        }else{
            serviceType = 1
            //price = self.objBookingServices.objSubSubService.inCallPrice
        }
        let formatter  = DateFormatter()
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        formatter.dateFormat = "yyyy-MM-dd"// "yyyy-MM-dd"
        let strDate = formatter.string(from: dateSelected)
        let dicParam = [
            "startTime":self.objBookingServices.bookingTime,
            "endTime":self.objBookingServices.endTime,
            "endTimeBiz":endTimeBiz,
            "artistId":self.objArtistDetails._id,
            "staff":self.objBookingServices.staffId,
            "serviceId":self.objBookingServices.objSubSubService.serviceId,
            "subServiceId":self.objBookingServices.objSubSubService.subServiceId,
            "artistServiceId":self.objBookingServices.objSubSubService.subSubServiceId,
            "bookingDate":self.objBookingServices.bookingDateForServer,
            "serviceType":serviceType,
            "bookingType":"walking",
            "price":price,
            "userId":objSelectedCustomerDetails._id,
            "bookingId":self.objBookingServices.bookingId,
            ] as [String : Any]
        print(dicParam)
        objServiceManager.requestPostForJson(strURL: WebURL.bookArtist, params: dicParam, success: { response in
            print(response)
            objActivity.stopActivity()
            let keyExists = response["responseCode"] != nil
            if keyExists {
                sessionExpireAlertVC(controller: self)
            }else {
                let strSucessStatus = response["status"] as? String ?? ""
                if strSucessStatus == "success"{
                objAppShareData.startTimerForHoldBookings()
                    objActivity.stopActivity()
                    self.objBookingServices.bookingTime = ""
                    self.goToConfirmBookingScreen()
                }else{
                   objAppShareData.showAlert(withMessage: "Service already added" , type: alertType.bannerDark,on: self)
                }
            }
        }) { error in
            objActivity.stopActivity()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    func goToConfirmBookingScreen(){
        let sbNew: UIStoryboard = UIStoryboard(name: "BookingNew", bundle: Bundle.main)
        if let objVC = sbNew.instantiateViewController(withIdentifier:"ConfirmWalkingBookingVC") as? ConfirmWalkingBookingVC{
            objVC.hidesBottomBarWhenPushed = true
            objVC.isOutCallSelectedConfirm = self.isOutCallSelected
            objVC.objArtistDetails = self.objArtistDetails
            objVC.objSelectedCustomerDetails = self.objSelectedCustomerDetails
            navigationController?.pushViewController(objVC, animated: true)
        }
    }
    
    func showAlertMessage(){
        
        let alertTitle = "Alert"
        let alertMessage = "Are you sure you want to permanently remove all selected services?"
        
        let alertController = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
        
        let action1 = UIAlertAction(title: "Yes", style: .default) { (action:UIAlertAction) in
            self.callWebserviceFor_deleteAllbooking()
        }
        
        let action2 = UIAlertAction(title: "No", style: .default) { (action:UIAlertAction) in
        }
        
        alertController.addAction(action1)
        alertController.addAction(action2)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func callWebserviceFor_deleteAllbooking() {
        if !objServiceManager.isNetworkAvailable(){
            return
        }
        var strUserId : String = ""
        if  let userInfo = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]{
            if let userId = userInfo["_id"] as? String {
                strUserId = userId
            }
        }else{
            if  let userInfo = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]{
                if let userId = userInfo["_id"] as? String {
                strUserId = userId
            }
            }
        }
        let parameters : Dictionary = [
            "userId" : objSelectedCustomerDetails._id,
            "bookingType":"walking"
            ] as [String : Any]
        
        objWebserviceManager.requestPost(strURL: WebURL.deleteUserBookService, params: parameters  , success: { response in
            let keyExists = response["responseCode"] != nil
            if  keyExists {
            }else{
                
                let strStatus =  response["status"] as? String ?? ""
                //let strMsg = response["message"] as? String ?? kErrorMessage
                if strStatus == k_success{
                    objAppShareData.stopTimerForHoldBookings()
                    self.navigationController?.popViewController(animated: false)
                }else{
                }
            }
        }){ error in
        }
    }
    
    @IBAction func btnHiddenBottumTable(_ sender: UIButton) {
        self.viewBottumTable.isHidden = true
    }
}

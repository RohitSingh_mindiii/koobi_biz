//
//  WakingCustomerListVC.swift
//  MualabBusiness
//
//  Created by Mindiii on 2/12/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit


class cellWalkingCustomerList: UITableViewCell {
    @IBOutlet weak var lblContactNo:UILabel!
    override func awakeFromNib() {  super.awakeFromNib()   }}


class WalkingCustomerListVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {
   
    @IBOutlet weak var tblCustomerList:UITableView!
    @IBOutlet weak var heightTableView:NSLayoutConstraint!
    @IBOutlet weak var txtSearch:UITextField!
    @IBOutlet weak var txtFirstName:UITextField!
    @IBOutlet weak var txtLastName:UITextField!
    @IBOutlet weak var txtEmail:UITextField!
    @IBOutlet weak var txtContact:UITextField!
    @IBOutlet weak var viewTable:UIView!
    var strSearchValue = ""
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "cellWalkingCustomerList", for: indexPath) as? cellWalkingCustomerList{
            cell.lblContactNo.text = "8253092657"
            return cell
        }else{
            return UITableViewCell()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.txtSearch.delegate = self
        self.viewTable.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.callWebserviceForCustomerList(andSearchText: "")
    }
}


//MARK: - button extension
extension WakingCustomerListVC{
    func callWebserviceForCustomerList(andSearchText:String){
            if !objServiceManager.isNetworkAvailable(){
                return
            }
            objServiceManager.requestGet(strURL:WebURL.customerList, params: ["search":"" as AnyObject], success: { response in
                let  status = response["status"] as? String ?? ""
                print(response)
                if status == "success"{
                }else{
                }
            }) { error in
                objActivity.stopActivity()
            }
    }
}
//MARK: - button extension
extension WakingCustomerListVC{
        @IBAction func btnBack(_ sender:Any){
            self.view.endEditing(true)
            self.navigationController?.popViewController(animated: true)
        }
    
        @IBAction func btnContinew(_ sender:Any){
            
        }
    
    @IBAction func btnHiddenTable(_ sender:Any){
        self.view.endEditing(true)
        self.viewTable.isHidden = true
    }
}



// MARK: - UITextfield Delegate
extension WakingCustomerListVC{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        
        let text = textField.text! as NSString
        
        if (text.length == 1)  && (string == "") {
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
            callWebserviceForCustomerList( andSearchText: "")
        }
        var substring: String = textField.text!
        substring = (substring as NSString).replacingCharacters(in: range, with: string)
        substring = substring.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        searchAutocompleteEntries(withSubstring: substring)
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let text = textField.text! as NSString
        self.txtSearch.resignFirstResponder()
        textField.resignFirstResponder()
        return true
    }
    
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
        callWebserviceForCustomerList(andSearchText: "")
        return true
    }
    
    // MARK: - searching operation
    func searchAutocompleteEntries(withSubstring substring: String) {
        if substring != "" {
            strSearchValue = substring
            // to limit network activity, reload half a second after last key press.
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
            self.perform(#selector(self.reload), with: nil, afterDelay: 0.5)
        }
    }
    
    @objc func reload() {
        callWebserviceForCustomerList(andSearchText: strSearchValue)
    }
}

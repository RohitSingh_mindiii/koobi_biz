//
//  ModelWalkingBooking.swift
//  MualabBusiness
//
//  Created by Mindiii on 2/12/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit

class ModelWalkingBooking: NSObject {
    var __v = ""
    var _id = ""
    var artistId = ""
    var crd = ""
    var email = ""
    var firstName = ""
    var lastName = ""
    var phone = ""
    var upd = ""
    var userName = ""
    var fullName = ""

    init(dict: [String : Any]) {
        upd = dict["upd"] as? String ?? ""
        phone = dict["phone"] as? String ?? ""
        lastName = dict["lastName"] as? String ?? ""
        firstName = dict["firstName"] as? String ?? ""
        userName = dict["userName"] as? String ?? ""
        email = dict["email"] as? String ?? ""
        fullName = firstName+" "+lastName+" "+phone
        if let _id1 = dict["_id"] as? String{
            _id = _id1
        }else if let _id1 = dict["_id"] as? Int{
            _id = String(_id1)
        }
        
        if let _id1 = dict["artistId"] as? String{
            artistId = _id1
        }else if let _id1 = dict["artistId"] as? Int{
            artistId = String(_id1)
        }
        
        if let _id1 = dict["crd"] as? String{
            crd = _id1
        }else if let _id1 = dict["crd"] as? Int{
            crd = String(_id1)
        }
 
        
      
        
    }
    
}
class StaffInfo {
    
    var holiday : Int = 0
    var staffId : Int = 0
    var job : String = ""
    var mediaAccess : String = ""
    var staffImage : String = ""
    var staffName : String = ""
    var arrStaffService : [StaffService] = []
    var arrStaffServiceId : [Int] = []
    
    var selectedPrice : Double = 0
    var selectedCompletionTime : String = ""
    
    init(dict : [String : Any]){
        
        if let valholiday = dict["holiday"] as? Int {
            holiday = valholiday
        }else{
            if let valholiday = dict["holiday"] as? String {
                holiday = Int (valholiday) ?? 0
            }
        }
        
        staffId = dict["staffId"] as? Int ?? 0
        job = dict["job"] as? String ?? ""
        mediaAccess = dict["mediaAccess"] as? String ?? ""
        staffImage = dict["staffImage"] as? String ?? ""
        staffName = dict["staffName"] as? String ?? ""
        
        if let arr = dict["staffService"] as? [[String : Any]]{
            for dict in arr{
                let objStaffService = StaffService.init(dict: dict)
                arrStaffService.append(objStaffService)
            }
        }
        
        if let arr = dict["staffServiceId"] as? [String]{
            for str in arr{
                let val = Int (str) ?? 0
                arrStaffServiceId.append(val)
            }
        }
    }
}


class StaffService {
    
    var serviceId : Int = 0
    var subServiceId : Int = 0
    var subSubServiceId : Int = 0
    
    var businessId : Int = 0   // staff owner id (Boss)
    var deleteStatus : Int = 0 // 1 = service exist , 0 = service deleted
    var artistId : Int = 0
    var staffId : Int = 0
    var status : Int = 0 // 1 = service is active, 0 = inactive
    
    var serviceName : String = ""
    var subServiceName : String = ""
    
    
    var inCallPrice : Double = 0
    var outCallPrice : Double = 0
    var subSubServiceName : String = ""
    var completionTime : String = ""
    var isSelected = false
    
    init(dict : [String : Any]){
        
        serviceId = dict["serviceId"] as? Int ?? 0
        subServiceId = dict["subserviceId"] as? Int ?? 0
        subSubServiceId = dict["artistServiceId"] as? Int ?? 0
        
        businessId = dict["businessId"] as? Int ?? 0
        deleteStatus = dict["deleteStatus"] as? Int ?? 0
        artistId = dict["artistId"] as? Int ?? 0
        staffId = dict["staffId"] as? Int ?? 0
        status = dict["status"] as? Int ?? 0
        
        subSubServiceName = dict["title"] as? String ?? ""
        completionTime = dict["completionTime"] as? String ?? ""
        
        //inCallPrice = dict["inCallPrice"] as? Double ?? 0
        //outCallPrice = dict["outCallPrice"] as? Double ?? 0
        
        if let val = dict["inCallPrice"] as? Double {
            inCallPrice = val
        }else{
            if let val = dict["inCallPrice"] as? String {
                inCallPrice = Double(val) ?? 0
            }
        }
        if let val = dict["outCallPrice"] as? Double {
            outCallPrice = val
        }else{
            if let val = dict["outCallPrice"] as? String {
                outCallPrice = Double(val) ?? 0
            }
        }
    }
}




class StaffServiceNew {
    var artistId : Int = 0
    var serviceId : Int = 0
    var subServiceId : Int = 0
    var subSubServiceId : Int = 0
    var subSubServiceName : String = ""
    var serviceDescription : String = ""
    
    var staffId : Int = 0
    var staffName : String = ""
    var staffImage : String = ""
    var staffJob : String = ""
    var completionTime : String = ""
    var inCallPrice : Double = 0.0
    var outCallPrice : Double = 0.0
    var isSelected = false
    
    init(dict : [String : Any]){
        staffId = dict["staffId"] as? Int ?? 0
        staffName = dict["staffName"] as? String ?? ""
        staffJob = dict["job"] as? String ?? ""
        inCallPrice = dict["inCallPrice"] as? Double ?? 0
        outCallPrice = dict["outCallPrice"] as? Double ?? 0
        completionTime = dict["completionTime"] as? String ?? ""
        staffImage = dict["staffImage"] as? String ?? ""
    }
}


 //
 //  CommentListVC.swift
 //  Mualab
 //
 //  Created by MINDIII on 10/24/17.
 //  Copyright © 2017 MINDIII. All rights reserved.
 
 
 import UIKit
 import Alamofire
 class CommentListVC: UIViewController,UITableViewDataSource,UITableViewDelegate,UITextViewDelegate {
 
    var selectedIndex : Int = 0
    var arrComntList = [commentUser]()
    var objHoldIndex = commentUser(dict: ["":""])
    var indexOfArray = 0
     var page: Int = 0
     var isOtherThanFeedScreen = false
 
 
     var objFeeds: feeds?
     fileprivate let writeComment = ""//"Write your comment"
     fileprivate var totalCount = 0
     fileprivate var myId:String = ""
 
     @IBOutlet weak var searchView: UIView!
     @IBOutlet weak var btnSearch: UIButton!
     @IBOutlet weak var txtSearch: UITextField!
     @IBOutlet weak var activity: UIActivityIndicatorView!
 
    @IBOutlet weak var viewCommentAction: UIView!
    @IBOutlet weak var viewEditCommentAction: UIView!

 //UIViews
 @IBOutlet weak var btnAddCommentView: UIView!
 //UITableViews
 @IBOutlet weak var tblComntList: UITableView!
 //UITextView
 @IBOutlet weak var txtComment: UITextView!
 //NSLayoutConstraints
 @IBOutlet weak var textViewHeight: NSLayoutConstraint!
 @IBOutlet weak var commentViewBottem: NSLayoutConstraint!
 //
 @IBOutlet weak var lblNoResults: UIView!
 
 fileprivate var strSearchValue : String = ""
 
 // MARK:- ViewLieCycles
 override func viewDidLoad() {
 super.viewDidLoad()
    self.viewCommentAction.isHidden = true
 searchView.layer.borderColor = colorGray.cgColor
 self.txtComment.delegate = self
 
 if self.isOtherThanFeedScreen {
    objFeeds = objAppShareData.arrFeedsForArtistData[selectedIndex]
 }else{
 objFeeds = objAppShareData.arrFeedsData[selectedIndex]
 }
 
 
 observeKeyboard()
 let tap = UITapGestureRecognizer(target: self, action: #selector(self.didTapOnTableView))
 tblComntList.addGestureRecognizer(tap)
 tblComntList.rowHeight = UITableView.automaticDimension
 tblComntList.estimatedRowHeight = 120
 
 if let dicUser = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] {
        self.myId = dicUser["_id"] as? String ?? ""

    }
 }
 
 override func didReceiveMemoryWarning() {
 super.didReceiveMemoryWarning()
 }
 
 override var preferredStatusBarStyle: UIStatusBarStyle {
 return .default
 }
 
 override func viewWillAppear(_ animated: Bool) {
 getCommentList(0)
    self.viewCommentAction.isHidden = true
 objAppShareData.fromLikeComment = true
 
 txtComment.text = writeComment
 super.viewWillAppear(animated)
 }
 
 override func viewDidAppear(_ animated: Bool) {
 super.viewDidAppear(animated)
 }
 
 // MARK: - Local methods
 @objc func didTapOnTableView( recognizer: UIGestureRecognizer) {
 view.endEditing(true)
 let text = txtComment.text! as NSString
 if (text.length == 0){
 //textViewHeight.constant = 40
 textViewHeight.constant = 41
 txtComment.text = writeComment
 }
 }
 
 func getCommentList(_ pageNo: Int) {
 page = pageNo
 let dicParam = ["feedId": "\(objFeeds?._id ?? 0)","page": pageNo, "limit": "20","userId":myId,"search":""] as [String : Any]
 
 callWebservice(for_GetCommentList: dicParam)
 }
 
 func postImage(image:UIImage){
 
 self.txtSearch.text = ""
 self.btnSearch.isHidden = false
 
 var dicUser = [:] as [String:Any]
 
 if let dict = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] {
 dicUser = dict
 }
 
 let dicParam = ["feedId":"\(self.objFeeds?._id ?? 0)",
                             "age":objAppShareData.getAge(from: dicUser["dob"] as? String ?? ""),
                             "gender":dicUser["gender"] as? String ?? "male",
                             "userId":"\(self.myId)",
                             "postUserId":"\(self.objFeeds?.userInfo?._id ?? 0)",
                             "type":"image",
                             "city":objLocationManager.currentCLPlacemark?.locality ?? "",
                             "state":objLocationManager.currentCLPlacemark?.administrativeArea ?? "",
                             "country":objLocationManager.currentCLPlacemark?.country ?? ""
                            ]
 self.callWebserviceFor_PostImage(dicParam: dicParam, image: image)
 
//         objLocationManager.getCurrentAdd(success: { address in
//         }, failure: { error in
//         })
    
   
    
 }
 }
 
 // MARK: - keyboard methods
 extension CommentListVC{
 func observeKeyboard() {
    NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
 }
 
 @objc func keyboardWillShow(_ notification: Notification) {
 let info = notification.userInfo
    let kbFrame = info?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
    let animationDuration = (info?[UIResponder.keyboardAnimationDurationUserInfoKey] ?? 0.0) as? TimeInterval
 let keyboardFrame: CGRect? = kbFrame?.cgRectValue
 let height: CGFloat? = keyboardFrame?.size.height
 commentViewBottem.constant = height!
 UIView.animate(withDuration: animationDuration ?? 0.0, animations: {() -> Void in
 self.view.layoutIfNeeded()
 })
 }
 
 @objc func keyboardWillHide(_ notification: Notification) {
 let info = notification.userInfo
    let animationDuration = (info?[UIResponder.keyboardAnimationDurationUserInfoKey] ?? 0.0) as! TimeInterval
 commentViewBottem.constant = 0
 UIView.animate(withDuration: animationDuration , animations: {() -> Void in
 self.view.layoutIfNeeded()
 })
 }
 }
 // MARK: - IBActions
 extension CommentListVC{
 
 @IBAction func btnBackAction(_ sender: Any) {
 navigationController?.popViewController(animated: true)
 self.view.endEditing(true)
 }
 
 @IBAction func btnSearchAction(_ sender: Any) {
 btnSearch.isHidden = true
 txtSearch.becomeFirstResponder()
 }
 
 @IBAction func btnImageAction(_ sender: UIButton) {
 self.view.endEditing(true)
 self.selectImage()
 }
 
 @IBAction func btnPostCommentAction(_ sender: Any) {
 view.endEditing(true)
 
 txtComment.text = txtComment.text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
 
 if (txtComment.text == writeComment) || (txtComment.text == ""){
 //textViewHeight.constant = 40
 textViewHeight.constant = 41
 self.txtComment.text = writeComment
 objAppShareData.showAlert(withMessage: "Please enter some text", type: alertType.bannerDark,on: self)
 
 }else {
 
 self.txtSearch.text = ""
 self.btnSearch.isHidden = false
 
 objWebserviceManager.StartIndicator()
 var dicUser = [:] as! [String:Any]
 if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.businessInfoDic){
 dicUser = dict
    }
 
 let dicParam = ["feedId":"\(self.objFeeds?._id ?? 0)",
 "comment": self.txtComment.text,
 "age":objAppShareData.getAge(from: dicUser["dob"] as? String ?? "0"),
 "gender":dicUser["gender"] as? String ?? "male",
 "userId":"\(self.myId)",
 "postUserId":"\(self.objFeeds?.userInfo?._id ?? 0)",
 "type":"text",
 
 "city":objLocationManager.currentCLPlacemark?.locality ?? "",
 "state":objLocationManager.currentCLPlacemark?.administrativeArea ?? "",
 "country":objLocationManager.currentCLPlacemark?.country ?? ""
 
 ] as [String : Any]
 
 self.callWebserviceFor_PostTextComment(dicParam: dicParam)
 
 }
 }
 
 @IBAction func btnLikeAction(_ sender: Any){
 
 let indexPath = IndexPath(row: (sender as AnyObject).tag, section: (sender as AnyObject).superview!!.tag)
 let cell = tblComntList.cellForRow(at: indexPath) as! usersCommonTableCell
 
 let indexPath1 = Int((sender as AnyObject).tag)
 let objUser = arrComntList[(indexPath1)]
 
 if objUser.likeStatus{
 objUser.likeStatus = false
 cell.btnLike?.isSelected = false
 objUser.commentLikeCount = objUser.commentLikeCount - 1
 //cell.btnLike?.setTitle(" \(objUser.commentLikeCount) Likes", for: .normal)
    if objUser.commentLikeCount > 1{
        cell.btnLike?.setTitle(" \(objUser.commentLikeCount) Likes", for: .selected)
    }else{
        cell.btnLike?.setTitle(" \(objUser.commentLikeCount) Like", for: .selected)
    }
 }else {
 objUser.likeStatus = true
 cell.btnLike?.isSelected = true
 objUser.commentLikeCount = objUser.commentLikeCount + 1
 //cell.btnLike?.setTitle(" \(objUser.commentLikeCount) Likes", for: .selected)
    if objUser.commentLikeCount > 1{
        cell.btnLike?.setTitle(" \(objUser.commentLikeCount) Likes", for: .selected)
    }else{
        cell.btnLike?.setTitle(" \(objUser.commentLikeCount) Like", for: .selected)
    }
 }
 
 var dicUser = [:] as! [String:Any]
 if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.businessInfoDic){
 dicUser = dict
 }
 
 let dicParam  = ["commentId": objUser._id,
 "userId":objUser.commentById,
 "likeById":self.myId,
 "age":objAppShareData.getAge(from: dicUser["dob"] as? String ?? ""),
 "gender":dicUser["gender"] as? String ?? "male",
 "type":"comment",
 "city":objLocationManager.currentCLPlacemark?.locality ?? "",
 "state":objLocationManager.currentCLPlacemark?.administrativeArea ?? "",
 "country":objLocationManager.currentCLPlacemark?.country ?? ""
 ]  as [String : Any]
 
 self.callWebservice(for_likeUnlikeComment: dicParam as Any as! [String : Any])
 
 
 
 }
 }
 // MARK: - Textview delegate methods
 extension CommentListVC{
 func textViewDidChange(_ textView: UITextView) {
 let fixedWidth: CGFloat = textView.frame.size.width
 let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat(MAXFLOAT)))
 //if (newSize.height <= 130) && newSize.height > 40 {
 if (newSize.height <= 130) && newSize.height > 41 {
 
 textViewHeight.constant = newSize.height
 }
 }
 
 func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool{
 if (range.length == 0){
 if (text == "") {
 //textViewHeight.constant = 40
 textViewHeight.constant = 41
 txtComment.text = writeComment
 txtComment.resignFirstResponder()
 }
 }
 return true
 }
 
 func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
 if (txtComment.text == writeComment) {
 txtComment.text = ""
 }
 return true
 }
 }
 // MARK: - Webservices
 extension CommentListVC{
 
 func callWebservice(for_GetCommentList dicParam: [AnyHashable: Any]){
 //objWebserviceManager.StartIndicator()
 var parameters = [String:Any]()
             parameters = dicParam as! [String : Any]
             objWebserviceManager.requestPost(strURL:WebURL.commentList, params: parameters , success: { (response) in
             
             print(response)
             objWebserviceManager.StopIndicator()
             self.activity.stopAnimating()
             let keyExists = response["responseCode"] != nil
             if  keyExists {
             sessionExpireAlertVC(controller: self)
             }else{
             if self.page == 0{
             self.arrComntList.removeAll()
             self.tblComntList.reloadData()
             }
             let strSucessStatus = response["status"] as? String ?? ""
             if strSucessStatus == k_success {
             
             self.totalCount = response["total"] as? Int ?? 0
             let arr = response["commentList"] as! [[String:Any]]
             for dict in arr{
             let obj = commentUser(dict: dict)
             self.arrComntList.append(obj!)
             }
             self.arrComntList = self.arrComntList.sorted(by: { $0._id < $1._id })
             self.objFeeds?.commentCount = self.arrComntList.count
             
             self.lblNoResults.isHidden = true
             self.tblComntList.reloadData()
             objWebserviceManager.StopIndicator()
             if self.page == 0{
             let ipath = IndexPath(row: self.arrComntList.count - 1, section: 0)
             self.tblComntList.scrollToRow(at: ipath, at: .top, animated: false)
             }
             }else {
             if self.arrComntList.count == 0 {
             self.lblNoResults.isHidden = false
             }
             }
             }}) { (error) in
         self.activity.stopAnimating()
         objWebserviceManager.StopIndicator()
         objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
 }
 }
 
 func callWebserviceFor_PostTextComment(dicParam:[String:Any]){
 
 objWebserviceManager.requestPostMultipartData(strURL: WebURL.addComment, params: dicParam, success: { response in
 self.activity.startAnimating()
 
 // objWebserviceManager.StopIndicator()
 let strSucessStatus = response["status"] as? String ?? ""
 if strSucessStatus == k_success{
 self.activity.stopAnimating()
 self.getCommentList(0)
 self.txtComment.text = self.writeComment
 //self.textViewHeight.constant = 40
 self.textViewHeight.constant = 41
 
 if let commentCount = response["commentCount"] as? Int{
 self.objFeeds?.commentCount = commentCount
 }else if let commentCount = response["commentCount"] as? String {
 self.objFeeds?.commentCount = Int(commentCount) ?? 0
 }
 print(response)
 } else{
 self.activity.stopAnimating()
 
 if let msg = response["message"] as? String{
 objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
 }
 }
 }) { error in
 objWebserviceManager.StopIndicator()
 self.activity.stopAnimating()
 objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
 }
 }
 
 func callWebserviceFor_PostImage(dicParam:[String:Any],image:UIImage){
 
 //self.activity.startAnimating()
 
 let url = WebURL.BaseUrl+WebURL.addComment
 var strAuth = ""
 if UserDefaults.standard.string(forKey:UserDefaults.keys.authToken)==nil {
 strAuth=""
 }else{
 strAuth=UserDefaults.standard.string(forKey:UserDefaults.keys.authToken)!
 }
 let headers = ["authtoken" : strAuth]
 Alamofire.upload(multipartFormData:{ multipartFormData in
 
    let imageData = image.jpegData(compressionQuality: 0.5)
 multipartFormData.append(imageData!,
 withName:"comment",
 fileName:"image.jpg",
 mimeType:"image/*")
 
 for (key, value) in dicParam {
 multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
 }},
 to:url,
 headers:headers,
 encodingCompletion: { encodingResult in
 switch encodingResult {
 case .success(let upload, _, _):
 upload.responseJSON { responseObject in
 
 if responseObject.result.isSuccess {
 self.activity.stopAnimating()
 
 do {
 self.activity.stopAnimating()
 
 let response = try JSONSerialization.jsonObject(with: responseObject.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
 
 let strSucessStatus = response["status"] as? String ?? ""
 if strSucessStatus == k_success{
 self.getCommentList(0)
 self.txtComment.text = self.writeComment
 //self.textViewHeight.constant = 40
 self.textViewHeight.constant = 41
 
 if let commentCount = response["commentCount"] as? Int{
 self.objFeeds?.commentCount = commentCount
 }else if let commentCount = response["commentCount"] as? String {
 self.objFeeds?.commentCount = Int(commentCount) ?? 0
 }
 } else{
 if let msg = response["message"] as? String {
 objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
 }
 
 }
 
 }catch{
 self.activity.stopAnimating()
 
 }
 }
 if responseObject.result.isFailure {
 self.activity.stopAnimating()
 
 //let error : Error = responseObject.result.error!
 objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
 //print(error)
 }
 }
 case .failure(let encodingError):
 //print(encodingError)
 objWebserviceManager.StopIndicator()
 objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
 }
 })
 }
 
 func callWebservice(for_likeUnlikeComment dicParam: [String: Any]) {
 objWebserviceManager.requestPost(strURL:WebURL.commentLike, params: dicParam, success: { response in
 let keyExists = response["responseCode"] != nil
 if  keyExists {
 sessionExpireAlertVC(controller: self)
 }else{
 let strSucessStatus = response["status"] as? String ?? ""
 if strSucessStatus == k_success{
 } else{
 if let msg = response["message"] as? String{
 objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
 }
 
 }}
 }) { error in
 objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
 }
 }
 func scrollViewDidScroll(_ scrollView: UIScrollView) {
 //self.view.endEditing(true)
 }
 }
 
 // MARK: - table view Delegate and Datasource
extension CommentListVC:UIGestureRecognizerDelegate{
 func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
 return arrComntList.count
 }
 
 func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
 
 let objUser = arrComntList[indexPath.row]
 var cellId = "CommentCell"
 if objUser.type == "text"{
 cellId = "CommentCell"
 }else{
 cellId = "CommentCellImage"
 }
 
 let cell = tblComntList.dequeueReusableCell(withIdentifier: cellId) as! usersCommonTableCell
 //cell.tblComments.reloadData()
 //cell.containerView?.layer.borderColor = colorGray.cgColor
 cell.lblUserName.text = objUser.userName
 cell.viewBlur.isHidden = true
 if objUser.type == "text"{
 cell.lblComment.text = objUser.comment
 }else{
 //cell.imgComment.image = UIImage(named: "gallery_placeholder"); cell.imgComment.af_setImage(withURL:URL(string:objUser.comment)! )
    cell.imgComment.sd_setImage(with: URL(string:objUser.comment)!, completed: nil)
 }
 cell.lblTime.text = objUser.timeElapsed
 cell.btnProfile.tag = indexPath.row
 cell.btnProfile.superview?.tag = indexPath.section
 cell.btnLike?.tag = indexPath.row
 cell.btnLike?.superview?.tag = indexPath.section
 cell.imgProfile.image = UIImage.customImage.user
 
    let lpgr = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
    lpgr.minimumPressDuration = 0.5
    lpgr.delaysTouchesBegan = true
    lpgr.delegate = self
    cell.addGestureRecognizer(lpgr)
 
 //        if objUser.profileImage.count>0{
 //            cell.imgProfile.af_setImage(withURL: URL(string: objUser.profileImage)!)
 //        }
    if objUser.profileImage.count>0{
     if let url = URL(string: objUser.profileImage){
        //cell.imgProfile.af_setImage(withURL: url, placeholderImage: UIImage(named: "cellBackground"))
        cell.imgProfile.sd_setImage(with: url, completed: nil)
     }
 }
 
 if objUser.likeStatus {
 cell.btnLike?.isSelected = true
    if objUser.commentLikeCount > 1{
        cell.btnLike?.setTitle(" \(objUser.commentLikeCount) Likes", for: .selected)
    }else{
        cell.btnLike?.setTitle(" \(objUser.commentLikeCount) Like", for: .selected)
    }
 }else {
 cell.btnLike?.isSelected = false
    if objUser.commentLikeCount > 1{
        cell.btnLike?.setTitle(" \(objUser.commentLikeCount) Likes", for: .normal)
    }else{
        cell.btnLike?.setTitle(" \(objUser.commentLikeCount) Like", for: .normal)
    }
    
 }
    
 if self.arrComntList.count < totalCount {
     if indexPath.row == arrComntList.count - (arrComntList.count - 5){
            page = page + 1
         getCommentList(page)
     }
 }
 return cell
 }
 
    @objc func handleLongPress(gestureReconizer: UILongPressGestureRecognizer) {
        if gestureReconizer.state != UIGestureRecognizer.State.ended {
            let touchPoint = gestureReconizer.location(in: self.tblComntList)
            if let indexPaths = tblComntList.indexPathForRow(at: touchPoint) {
                let obj = self.arrComntList[indexPaths.row]

                print("obj = ",obj.comment)

                if let dicUser = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] {
                    self.myId = dicUser["_id"] as? String ?? ""
                }
                if myId == String(obj.commentById){
                    if obj.type == "text"{
                       self.viewEditCommentAction.isHidden = false
                    }else{
                       self.viewEditCommentAction.isHidden = true
                    }
                    objHoldIndex = obj
                    indexOfArray = indexPaths.row
                    self.viewCommentAction.isHidden = false
                    if  let cell = tblComntList.cellForRow(at: indexPaths) as? usersCommonTableCell{
                        cell.viewBlur.isHidden = false
                    }
                }
            }
        }
}
    
    
 func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
 let objUser = arrComntList[indexPath.row]
 if objUser.type == "text" {
 return UITableView.automaticDimension
 }else {
 return self.tblComntList.frame.size.width * 0.75
 }
    }
    
    func webServiceForDeleteComment(param:[String:Any]){
        if !objServiceManager.isNetworkAvailable(){   return   }
        objWebserviceManager.StartIndicator()
         objWebserviceManager.requestPost(strURL: WebURL.deleteComment, params: param  , success: { response in
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                objWebserviceManager.StopIndicator()
            }else{
                let strStatus =  response["status"] as? String ?? ""
                if strStatus == k_success{
                    objWebserviceManager.StartIndicator()
                    self.viewWillAppear(true)    }else{
                    objWebserviceManager.StopIndicator()
                    if let msg = response["message"] as? String{    objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)  }}  }
        }){ error in
            objWebserviceManager.StopIndicator()
            showAlertVC(title: kAlertTitle, message: kErrorMessage, controller: self)
        } }
}

    
    
    
    
    
    

 
 //MARK: - UIImagePicker extension Methods
 extension CommentListVC:UIImagePickerControllerDelegate,UINavigationControllerDelegate{
 
 func selectImage() {
 let selectImage = UIAlertController(title: "Select Image", message: nil, preferredStyle: .actionSheet)
 let imagePicker = UIImagePickerController()
 imagePicker.delegate = self
 let btn0 = UIAlertAction(title: "Cancel", style: .cancel, handler: {(_ action: UIAlertAction) -> Void in
 })
 let btn1 = UIAlertAction(title: "Camera", style: .default, handler: {(_ action: UIAlertAction) -> Void in
 if UIImagePickerController.isSourceTypeAvailable(.camera) {
 imagePicker.sourceType = .camera
 // imagePicker.allowsEditing = true
    imagePicker.modalPresentationStyle = .fullScreen
 self.present(imagePicker, animated: true)
 }
 })
 let btn2 = UIAlertAction(title: "Photo Library", style: .default, handler: {(_ action: UIAlertAction) -> Void in
 
 if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
 imagePicker.sourceType = .photoLibrary
 // imagePicker.allowsEditing = true
    imagePicker.modalPresentationStyle = .fullScreen
 self.present(imagePicker, animated: true)
 }
 })
 selectImage.addAction(btn0)
 selectImage.addAction(btn1)
 selectImage.addAction(btn2)
 present(selectImage, animated: true)
 }
 
 internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
    let img = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
 self.cropImage(image: img)
 self.dismiss(animated: false, completion: nil)
 }
 
 private func imagePickerControllerDidCancel(picker: UIImagePickerController){
 dismiss(animated: true, completion: nil)
 }
 
 
 func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
 
 let scale = newWidth / image.size.width
 let newHeight = image.size.height * scale
 UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
 image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
 let newImage = UIGraphicsGetImageFromCurrentImageContext()
 UIGraphicsEndImageContext()
 
 return newImage!
 }
 }
 //MARK :- crop image into square
 extension CommentListVC:RSKImageCropViewControllerDelegate,RSKImageCropViewControllerDataSource {
 func imageCropViewControllerCustomMaskRect(_ controller: RSKImageCropViewController) -> CGRect {
 var maskSize: CGSize
 // calculate the width based on the screen size - 30px
 let screenRect: CGRect = UIScreen.main.bounds
 let width: CGFloat = screenRect.size.width - 20
 // 4:3, height will be 75% of the width
 let height: CGFloat = width * 0.75
 if controller.isPortraitInterfaceOrientation() {
 maskSize = CGSize(width: width, height: height)
 }
 else {
 maskSize = CGSize(width: 355, height: 266.25)
 }
 let viewWidth: CGFloat = controller.view.frame.width
 let viewHeight: CGFloat = controller.view.frame.height
 let maskRect = CGRect(x: (viewWidth - maskSize.width) * 0.5, y: (viewHeight - maskSize.height) * 0.5, width: maskSize.width, height: maskSize.height)
 return maskRect
 }
 
 func imageCropViewControllerCustomMaskPath(_ controller: RSKImageCropViewController) -> UIBezierPath {
 // If the image is not rotated, then the movement rect coincides with the mask rect.
 return UIBezierPath(rect: controller.maskRect)
 }
 
 func imageCropViewControllerCustomMovementRect(_ controller: RSKImageCropViewController) -> CGRect {
 // If the image is not rotated, then the movement rect coincides with the mask rect.
 return controller.maskRect
 }
 
 
 func imageCropViewControllerDidCancelCrop(_ controller: RSKImageCropViewController) {
 self.navigationController?.popViewController(animated: false)
 }
 
 func imageCropViewController(_ controller: RSKImageCropViewController, didCropImage croppedImage: UIImage, usingCropRect cropRect: CGRect, rotationAngle: CGFloat) {
 objServiceManager.StartIndicator()
 let resizedImg = self.resizeImage(image: croppedImage, newWidth: 600)
 self.postImage(image: resizedImg)
 self.navigationController?.popViewController(animated: false)
 }
 
 private func cropImage(image:UIImage){
 let cropVC = RSKImageCropViewController(image: image, cropMode:.custom)
 cropVC.delegate = self
 cropVC.dataSource = self
 self.navigationController?.pushViewController(cropVC, animated: false)
 }
 }
 
 
 // MARK: - UITextfield Delegate
 extension CommentListVC : UITextFieldDelegate{
 
 func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
 
 let text = textField.text! as NSString
 
 if (text.length == 1)  && (string == "") {
 NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
 getLikesUsersData(0, andSearchText: "")
 }
 var substring: String = textField.text!
 substring = (substring as NSString).replacingCharacters(in: range, with: string)
 substring = substring.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
 searchAutocompleteEntries(withSubstring: substring)
 return true
 }
 
 func textFieldShouldReturn(_ textField: UITextField) -> Bool {
 let text = textField.text! as NSString
 if (text.length == 0){
 btnSearch.isHidden = false
 }
 self.txtSearch.resignFirstResponder()
 textField.resignFirstResponder()
 return true
 }
 
 //    // MARK: - searching operation
 //    func searchAutocompleteEntries(withSubstring substring: String) {
 //        if !(substring == "") {
 //            getLikesUsersData(0, andSearchText: substring)
 //        }
 //    }
 
 func textFieldShouldClear(_ textField: UITextField) -> Bool {
 self.view.endEditing(true)
 
 NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
 getLikesUsersData(0, andSearchText: "")
 return true
 }
 
 // MARK: - searching operation
 func searchAutocompleteEntries(withSubstring substring: String) {
 if substring != "" {
 strSearchValue = substring
 // to limit network activity, reload half a second after last key press.
 NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
 self.perform(#selector(self.reload), with: nil, afterDelay: 0.5)
 }
 }
 
 @objc func reload() {
 getLikesUsersData(0, andSearchText: strSearchValue)
 }
 }
 
 // MARK:- Save data
 extension CommentListVC{
 
 func getLikesUsersData(_ pageNo: Int, andSearchText strText: String) {
 self.activity.startAnimating()
 page = pageNo
 let dicParam = ["feedId": objFeeds?._id ?? 0, "page": pageNo, "limit": "10", "search": strText.lowercased(),"userId":myId] as [String : Any]
 self.callWebservice(for_GetCommentList: dicParam)
 }
 
 @IBAction func btnProfileAction(_ sender: UIButton){
 let section = 0
 let row = (sender as AnyObject).tag
 let indexPath = IndexPath(row: row!, section: section)
 let objUser = arrComntList[indexPath.row]
 
 if !objServiceManager.isNetworkAvailable(){
 objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
 return
 }
 let dicParam = ["userName":objUser.userName]
 objServiceManager.requestPost(strURL: WebURL.profileByUserName, params: dicParam, success: { response in
 if response["status"] as? String ?? "" == "success"{
 var strId = ""
 var strType = ""
 if let dictUser = response["userDetail"] as? [String : Any]{
 let myId = dictUser["_id"] as? Int ?? 0
 strId = String(myId)
 strType = dictUser["userType"] as? String ?? ""
 }
 let dic = [
 "tabType" : "people",
 "tagId": strId,
 "userType":strType,
 "title": objUser.userName
 ] as [String : Any]
 self.openProfileForSelectedTagPopoverWithInfo(dict: dic)
 }
 }) { error in
 }
 /*
 let dic = [
 "tabType" : "people",
 "tagId": objUser.likeById,
 "userType":"user",
 "title": objUser.userName
 ] as [String : Any]
 self.openProfileForSelectedTagPopoverWithInfo(dict: dic)
 */
 }
 func openProfileForSelectedTagPopoverWithInfo(dict : [AnyHashable: Any]){
 
 var dictTemp : [AnyHashable : Any]?
 
 dictTemp = dict
 
 if let dict1 = dict as? [String:[String :Any]] {
 if let dict2 = dict1.first?.value {
 dictTemp = dict2
 }
 }
 
 guard let dictFinal = dictTemp as? [String : Any] else { return }
 
 var strUserType: String?
 var tagId : Int?
 
 if let userType = dictFinal["userType"] as? String{
 strUserType = userType
 
 if let idTag = dictFinal["tagId"] as? Int{
 tagId = idTag
 }else{
 if let idTag = dictFinal["tagId"] as? String{
 tagId = Int(idTag)
 }
 }
 
 if self.myId == String(tagId ?? 00) {
 //isNavigate = true
 objAppShareData.isOtherSelectedForProfile = false
 self.gotoProfileVC()
 return
 }
 
 if let strUserType = strUserType, let tagId =  tagId {
 
     objAppShareData.selectedOtherIdForProfile  = String(tagId)
     objAppShareData.isOtherSelectedForProfile = true
     objAppShareData.selectedOtherTypeForProfile = strUserType  //"artist" or "user"
     //isNavigate = true
     self.gotoProfileVC()
     }
 }
 }
 func gotoProfileVC (){
    
 self.view.endEditing(true)
 let sb: UIStoryboard = UIStoryboard(name: "ArtistProfile", bundle: Bundle.main)
     if let objVC = sb.instantiateViewController(withIdentifier:"SWRevealViewController") as? SWRevealViewController{
            objVC.hidesBottomBarWhenPushed = true
         navigationController?.pushViewController(objVC, animated: true)
     }
 }
  
    
    @IBAction func btnEditCommentAction(_ sender: UIButton){
        self.viewCommentAction.isHidden = true
        let objReminder = self.objHoldIndex
        let param = ["id":String(objReminder?._id ?? 0),"feedId":objReminder?.feedId]

        let sb: UIStoryboard = UIStoryboard(name: "Feed", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"CommentEditVC") as? CommentEditVC{
            objVC.objFeeds = objFeeds
            objVC.objHoldIndex =  self.objHoldIndex
            navigationController?.pushViewController(objVC, animated: false)
        }
    }
    
    @IBAction func btnDeleteCommentAction(_ sender: UIButton){
        self.viewCommentAction.isHidden = true
        let objReminder = self.objHoldIndex
        let params = ["id":String(objReminder?._id ?? 0) ?? "","feedId":objReminder?.feedId ?? ""]
       // objAppShareData.showAlert(withMessage: "Under development", type: alertType.bannerDark,on: self)
        deleteCommentAlert(param: params as [String : Any])
    }
    
    
    func deleteCommentAlert(param:[String:Any]){
        // Create the alert controller
        let alertController = UIAlertController(title: "Alert", message: "Are you sure you want to remove this comment?", preferredStyle: .alert)
        // Create the actions
        let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.arrComntList.remove(at: self.indexOfArray)
            self.tblComntList.reloadData()
            self.webServiceForDeleteComment(param: param)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            self.tblComntList.reloadData()
        }
        cancelAction.setValue(#colorLiteral(red: 1, green: 0.1490196078, blue: 0, alpha: 1), forKey: "titleTextColor")
        okAction.setValue(#colorLiteral(red: 0, green: 0.851996243, blue: 0.8281483054, alpha: 1), forKey: "titleTextColor")
        
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    
    
    @IBAction func btnCommentActionPopUpHiddden(_ sender: UIButton){
        self.viewCommentAction.isHidden = true
        self.tblComntList.reloadData()
 }
 
}
 
 





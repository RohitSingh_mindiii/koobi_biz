

//
//  LikesListVC.swift
//  Mualab
//
//  Created by MINDIII on 10/24/17.
//  Copyright © 2017 MINDIII. All rights reserved.
//

import UIKit
import Social
//import KRProgressHUD
import Alamofire
//import SwiftyJSON

class LikesListVC: UIViewController,UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate{
    
    var objFeeds : feeds?
    var arrLikeUsersList = [likeUser]()
    var page: Int = 0
    
    fileprivate var myId:String = ""
    //
    @IBOutlet weak var tblLikesUserList: UITableView!
    //
    @IBOutlet weak var searchView: UIView!
    //
    @IBOutlet weak var btnSearch: UIButton!
    //
    @IBOutlet weak var txtSearch: UITextField!
    //
    @IBOutlet weak var lblNoResults: UIView!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    fileprivate var strSearchValue : String = ""
    
    //MARK: View Life Cyles
    override func viewDidLoad() {
        super.viewDidLoad()
        setNeedsStatusBarAppearanceUpdate()
        searchView.layer.borderColor = colorGray.cgColor
        
        if let dicUser = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] {
            myId = dicUser["_id"] as? String ?? ""
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewWillAppear(_ animated: Bool) {
        objAppShareData.fromLikeComment = true
        
        super.viewWillAppear(animated)
        getLikesUsersData(0,andSearchText: "")
    }
    
}
// MARK:- Save data
extension LikesListVC{
    func getLikesUsersData(_ pageNo: Int, andSearchText strText: String) {
        self.activity.startAnimating()
        page = pageNo
        let dicParam = ["feedId": objFeeds!._id, "page": pageNo, "limit": "10", "search": strText.lowercased(),"userId":myId] as [String : Any]
        self.callWebservice(for_getFeedsLikeList: dicParam)
    }
}

// MARK:  - Webservices
extension LikesListVC{
    
    func callWebservice(for_getFeedsLikeList dicParam: [AnyHashable: Any]){
        //objActivity.startActivityIndicator()
        var parameters = [String:Any]()
        parameters = dicParam as! [String : Any]
        objWebserviceManager.requestPost(strURL:WebURL.likeList, params: parameters , success: { (response) in
            
            self.activity.stopAnimating()
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else{
                var strStatus:String? = ""
                if self.page == 0{
                    self.arrLikeUsersList.removeAll()
                    self.tblLikesUserList.reloadData()
                    
                    let strSucessStatus = response["status"] as? String ?? ""
                    strStatus = response["message"] as? String
                    if strSucessStatus == k_success {
                        let arr = response["likeList"] as! [[String:Any]]
                        for dict in arr{
                            let obj = likeUser.init(dict: dict)
                            self.arrLikeUsersList.append(obj!)
                        }
                        
                        self.lblNoResults.isHidden = true
                        if self.arrLikeUsersList.count == 0{
                            self.lblNoResults.isHidden = false
                        }
                        
                        self.tblLikesUserList.reloadData()
                        
                    }else{
                        if self.arrLikeUsersList.count == 0{
                            self.lblNoResults.isHidden = false
                        }
                    }
                }
                else{
                    if let msg = response["message"] as? String{
                        objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                    }
                    
                }
            } }) { (error) in
                self.activity.stopAnimating()
                objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.view.endEditing(true)
    }
    func callWebservice(for_Follow objUser: likeUser, andCell cell: usersCommonTableCell){
        
        let dicParam = ["followerId": objUser.likeById,
                        "userId":myId] as [String : Any]
        
        let followActivity = UIActivityIndicatorView()
        followActivity.tintColor = UIColor.white
        followActivity.color = UIColor.white
        followActivity.hidesWhenStopped = true
        followActivity.center = CGPoint(x: cell.btnFollow.frame.size.width / 2, y: cell.btnFollow.frame.size.height / 2)
        cell.btnFollow.addSubview(followActivity)
        followActivity.startAnimating()
        
        objWebserviceManager.requestPost(strURL:WebURL.followFollowing, params: dicParam , success: { response in
            followActivity.stopAnimating()
            followActivity.removeFromSuperview()
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else{
                let strSucessStatus = response["status"] as? String ?? ""
                if strSucessStatus == k_success{
                    if objUser.followerStatus {
                        cell.btnFollow.setTitle("Following", for: .normal)
                        cell.btnFollow.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
                        cell.btnFollow.layer.borderWidth = 1
                        cell.btnFollow.layer.borderColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1) //UIColor.theameColors.blueColor.cgColor
                        cell.btnFollow.backgroundColor = UIColor.clear
                    }else {
                        cell.btnFollow.setTitle("Follow", for: .normal)
                        cell.btnFollow.setTitleColor(UIColor.white, for: .normal)
                        cell.btnFollow.backgroundColor = appColor//UIColor.theameColors.pinkColor
                        cell.btnFollow.layer.borderWidth = 0
                    }
                }else{
                    if let msg = response["message"] as? String{
                        objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                    }
                    
                }
            }}) { error in
                
                objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
}

// MARK: - IBActions
extension LikesListVC{
    @IBAction func btnBackAction(_ sender: Any){
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSearchAction(_ sender: Any) {
        btnSearch.isHidden = true
        txtSearch.becomeFirstResponder()
    }
    @IBAction func btnProfileAction(_ sender: UIButton){
        let section = 0
        let row = (sender as AnyObject).tag
        let indexPath = IndexPath(row: row!, section: section)
        let objUser = arrLikeUsersList[indexPath.row]
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        let dicParam = ["userName":objUser.userName]
        objServiceManager.requestPost(strURL: WebURL.profileByUserName, params: dicParam, success: { response in
            if response["status"] as? String ?? "" == "success"{
                var strId = ""
                var strType = ""
                if let dictUser = response["userDetail"] as? [String : Any]{
                    let myId = dictUser["_id"] as? Int ?? 0
                    strId = String(myId)
                    strType = dictUser["userType"] as? String ?? ""
                }
                let dic = [
                    "tabType" : "people",
                    "tagId": strId,
                    "userType":strType,
                    "title": objUser.userName
                    ] as [String : Any]
                self.openProfileForSelectedTagPopoverWithInfo(dict: dic)
            }
        }) { error in
        }
        /*
         let dic = [
         "tabType" : "people",
         "tagId": objUser.likeById,
         "userType":"user",
         "title": objUser.userName
         ] as [String : Any]
         self.openProfileForSelectedTagPopoverWithInfo(dict: dic)
         */
    }
    func openProfileForSelectedTagPopoverWithInfo(dict : [AnyHashable: Any]){
        
        var dictTemp : [AnyHashable : Any]?
        
        dictTemp = dict
        
        if let dict1 = dict as? [String:[String :Any]] {
            if let dict2 = dict1.first?.value {
                dictTemp = dict2
            }
        }
        
        guard let dictFinal = dictTemp as? [String : Any] else { return }
        
        var strUserType: String?
        var tagId : Int?
        
        if let userType = dictFinal["userType"] as? String{
            strUserType = userType
            
            if let idTag = dictFinal["tagId"] as? Int{
                tagId = idTag
            }else{
                if let idTag = dictFinal["tagId"] as? String{
                    tagId = Int(idTag)
                }
            }
            
            if self.myId == String(tagId ?? 00) {
                //isNavigate = true
                objAppShareData.isOtherSelectedForProfile = false
                self.gotoProfileVC()
                return
            }
            
            if let strUserType = strUserType, let tagId =  tagId {
                
                objAppShareData.selectedOtherIdForProfile  = String(tagId)
                objAppShareData.isOtherSelectedForProfile = true
                objAppShareData.selectedOtherTypeForProfile = strUserType  //"artist" or "user"
                //isNavigate = true
                self.gotoProfileVC()
            }
        }
    }
    func gotoProfileVC (){
        self.view.endEditing(true)
        let sb: UIStoryboard = UIStoryboard(name: "ArtistProfile", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"SWRevealViewController") as? SWRevealViewController{
            objVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(objVC, animated: true)
        }
    }
    @IBAction func btnFollowAction(_ sender: Any){
        let section = 0
        let row = (sender as AnyObject).tag
        let indexPath = IndexPath(row: row!, section: section)
        let cell = tblLikesUserList.cellForRow(at: indexPath) as? usersCommonTableCell
        let objUser = arrLikeUsersList[indexPath.row]
        objUser.followerStatus = !objUser.followerStatus
        callWebservice(for_Follow: objUser, andCell: cell!)
    }
}

// MARK: - UITextfield Delegate
extension LikesListVC{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        
        let text = textField.text! as NSString
        
        if (text.length == 1)  && (string == "") {
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
            getLikesUsersData(0, andSearchText: "")
        }
        var substring: String = textField.text!
        substring = (substring as NSString).replacingCharacters(in: range, with: string)
        substring = substring.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        searchAutocompleteEntries(withSubstring: substring)
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let text = textField.text! as NSString
        if (text.length == 0){
            btnSearch.isHidden = false
        }
        self.txtSearch.resignFirstResponder()
        textField.resignFirstResponder()
        return true
    }
    
    //    // MARK: - searching operation
    //    func searchAutocompleteEntries(withSubstring substring: String) {
    //        if !(substring == "") {
    //            getLikesUsersData(0, andSearchText: substring)
    //        }
    //    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
        getLikesUsersData(0, andSearchText: "")
        return true
    }
    
    // MARK: - searching operation
    func searchAutocompleteEntries(withSubstring substring: String) {
        if substring != "" {
            strSearchValue = substring
            // to limit network activity, reload half a second after last key press.
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
            self.perform(#selector(self.reload), with: nil, afterDelay: 0.5)
        }
    }
    
    @objc func reload() {
        getLikesUsersData(0, andSearchText: strSearchValue)
    }
}

// MARK: - UITableview delegate and Datasources
extension LikesListVC{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrLikeUsersList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LikesCell") as! usersCommonTableCell
        let objUser = arrLikeUsersList[indexPath.row]
        
        if objUser.followerStatus {
            cell.btnFollow.setTitle("Following", for: .normal)
            cell.btnFollow.setTitleColor(#colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1), for: .normal)
            cell.btnFollow.layer.borderWidth = 1
            cell.btnFollow.layer.borderColor = UIColor.darkGray.cgColor
            cell.btnFollow.backgroundColor = UIColor.clear
        }else {
            cell.btnFollow.setTitle("Follow", for: .normal)
            cell.btnFollow.setTitleColor(UIColor.white, for: .normal)
            cell.btnFollow.backgroundColor = appColor//UIColor.theameColors.pinkColor
            cell.btnFollow.layer.borderWidth = 0
        }
        
        if String(objUser.likeById) == myId {
            cell.btnFollow.isHidden = true
        }
        cell.btnFollow.tag = indexPath.row
        cell.btnFollow.superview?.tag = indexPath.section
        cell.btnProfile.tag = indexPath.row
        cell.btnProfile.superview?.tag = indexPath.section
        cell.lblUserName.text = objUser.userName
        cell.imgProfile.image = UIImage.customImage.user
        //if objUser.profileImage.count > 0 {
        //                cell.imgProfile.af_setImage(withURL: URL(string: objUser.profileImage)!)
        //}
        if objUser.profileImage != "" {
            if let url = URL(string: objUser.profileImage){
                //cell.imgProfile.af_setImage(withURL: url, placeholderImage: UIImage(named: "cellBackground"))
                cell.imgProfile.sd_setImage(with: url, placeholderImage:#imageLiteral(resourceName: "cellBackground"))
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.view.endEditing(true)
    }
    
}



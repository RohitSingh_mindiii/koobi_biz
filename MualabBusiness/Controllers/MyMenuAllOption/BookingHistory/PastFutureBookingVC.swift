//
//  BookingDetailVC.swift
//  MualabCustomer
//  Created by Mac on 04/06/18.
//  Copyright © 2018 Mindiii. All rights reserved.

import UIKit
import DropDown

class PastFutureBookingVC: UIViewController, UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate {
   
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var txtSearchFilter: UITextField!
    @IBOutlet weak var btnHiddenSearch: UIButton!
     var strSearchText = ""
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var viewPastBooking: UIView!
    @IBOutlet weak var viewFutureBooking: UIView!
    @IBOutlet weak var tblPastBooking: UITableView!
    @IBOutlet weak var tblFutureBooking: UITableView!
    
    @IBOutlet weak var btnBookingHistory: UIButton!
    @IBOutlet weak var btnAppointments: UIButton!
    @IBOutlet weak var btnCancelBooking: UIButton!

    @IBOutlet weak var imgBookingHistory: UIImageView!
    @IBOutlet weak var imgAppointments: UIImageView!
    @IBOutlet weak var imgCancelBooking: UIImageView!

    @IBOutlet weak var activity: UIActivityIndicatorView!
    @IBOutlet weak var lblNoRecord: UIView!
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var viewBottumTable: UIView!
    
    
    
    var isBookingHistory = 1
    var selectedIndexPath : IndexPath?
    var arrPastBookingData : [PastFutureBookingData] = []
     var arrPastBookingDataAll : [PastFutureBookingData] = []
    
    var arrFutureBookingData : [PastFutureBookingData] = []
    var arrFutureBookingDataAll : [PastFutureBookingData] = []
    
    var arrCancelBookingData : [PastFutureBookingData] = []
    var arrCancelBookingDataAll : [PastFutureBookingData] = []
    
    var dropDown = DropDown()
    
    @IBOutlet weak var btnMainType: UIButton!
    @IBOutlet weak var btnAllType: UIButton!
    @IBOutlet weak var btnInCallType: UIButton!
    @IBOutlet weak var btnOutCallType: UIButton!
    @IBOutlet weak var viewAllType: UIView!
    @IBOutlet weak var viewInCallType: UIView!
    @IBOutlet weak var viewOutCallType: UIView!
    
    fileprivate var strUserId : String = ""
    
    fileprivate var pageNo: Int = 0
    fileprivate var totalCount = 0
    fileprivate var lastLoadedPage = 0
    
    fileprivate var strBookingType = ""
    
    fileprivate var pageNo2: Int = 0
    fileprivate var totalCount2 = 0
    fileprivate var lastLoadedPage2 = 0
    
    fileprivate var pageNo3: Int = 0
    fileprivate var totalCount3 = 0
    fileprivate var lastLoadedPage3 = 0
    fileprivate var fromLastTab = false

    fileprivate let pageSize = 20 // that's up to you, really
    fileprivate let preloadMargin = 5 // or whatever number that makes sense with your page size
    fileprivate let reviewTextLimit = 150
    
    //MARK: - refreshControl
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(self.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = appColor
        return refreshControl
    }()
    
    lazy var refreshControl2: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(self.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = appColor
        return refreshControl
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        self.strSearchText = ""
        self.txtSearchFilter.text = ""
        self.btnHiddenSearch.isHidden = true
        self.setuoDropDownAppearance()
        self.chengSegmentOnSelectedTag(sender: 1)
        //self.getBookingDataWith(0)
        self.addGesturesToView()
    }
    func addGesturesToView() -> Void {
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.topToBottomSWIPE))
        swipeRight.direction = .down
        self.viewBottumTable.addGestureRecognizer(swipeRight)
    }
    
    @objc func topToBottomSWIPE() ->Void {
        self.view.endEditing(true)
        self.viewBottumTable.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
   
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
   
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.tabBarController?.view.frame =  CGRect(x:0,y:0,width:Int(self.view.frame.width),height:viewHeightGloble)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.activity.stopAnimating()

        self.view.endEditing(true)
        selectedIndexPath = nil
        self.setuoDropDownAppearance()
        if objAppShareData.isFromNotification {
            
            var notifyId : Int?
            var strUserType: String?
            var strUserName: String?
            var strProfileImage: String?
            
            if let userInfo = objAppShareData.notificationInfoDict {
                
                if let notiId  = userInfo["notifyId"] as? Int{
                    notifyId = notiId
                }else{
                    if let notiId  = userInfo["notifyId"] as? String{
                        notifyId = Int(notiId)
                    }
                }
                
                if let userType = userInfo["userType"] as? String{
                    strUserType = userType
                }
                
                if let userName = userInfo["userName"] as? String{
                    strUserName = userName
                }
                
                if let img = userInfo["urlImageString"] as? String{
                    strProfileImage  = img
                }
                
            }
            
            if let notifyId = notifyId {
                
                var objPastFutureBookingData  : PastFutureBookingData = PastFutureBookingData(dict: ["":""])
                
                objPastFutureBookingData._id = notifyId
                objPastFutureBookingData.userName = strUserName ?? ""
                objPastFutureBookingData.profileImage = strProfileImage ?? ""
                
                self.gotoAppoitmentBookingVC(obj: objPastFutureBookingData)
                return
        
            }
 
        }
        self.getBookingDataWith(0)
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        selectedIndexPath = nil
        self.getBookingDataWith(0)
        //API Call
    }
}

//MARK: - custome method extension
extension PastFutureBookingVC {
    
    func configureView(){

        self.strBookingType = "All Type"
        self.viewAllType.isHidden = true
        self.viewInCallType.isHidden = true
        self.viewOutCallType.isHidden = true
        self.viewSearch.isHidden = true
        self.txtSearchFilter.delegate = self
        self.viewSearch.isHidden = true

        self.tblView.delegate = self
        self.tblView.dataSource = self
        self.viewBottumTable.isHidden = true
        if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]{
            if let userId = dict["_id"] as? String {
                self.strUserId =  userId
            }
        }else{
            let userInfo = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] ?? ["":""]
            if let id = userInfo["_id"] as? String {
                strUserId = id
            }
        }

        
        self.tblPastBooking.delegate = self
        self.tblPastBooking.dataSource = self
        self.tblFutureBooking.delegate = self
        self.tblFutureBooking.dataSource = self
        
        self.tblPastBooking.rowHeight = UITableView.automaticDimension;
        self.tblPastBooking.estimatedRowHeight = 77.0;
        
        self.tblPastBooking.addSubview(self.refreshControl)
        self.tblFutureBooking.addSubview(self.refreshControl2)
        
        self.lblNoRecord.isHidden = true
        
        //segmentController.selectedSegmentIndex = 0
        self.lblHeader.text = "Booking History"
        self.viewPastBooking.isHidden = false
        self.viewFutureBooking.isHidden = true
    }
}

// MARK: - UITextfield Delegate
extension PastFutureBookingVC{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        
        let text = textField.text! as NSString
        
        if (text.length == 1)  && (string == "") {
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
            self.strSearchText = ""
            self.btnHiddenSearch.isHidden = false
            self.activity.startAnimating()
            getArtistDataWith(andSearchText: "")
        }else{
            var substring: String = textField.text!
            substring = (substring as NSString).replacingCharacters(in: range, with: string)
            substring = substring.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            let a = substring.count
            if a > 20 && substring != ""{
                return false
            }else{
                self.activity.startAnimating()
                searchAutocompleteEntries(withSubstring: substring)
            }
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
        self.strSearchText = ""
        getArtistDataWith( andSearchText: "")
        return true
    }
    
    // MARK: - searching operation
    func searchAutocompleteEntries(withSubstring substring: String) {
        if substring != "" {
            self.strSearchText = substring
            // to limit network activity, reload half a second after last key press.
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
            self.perform(#selector(self.reload), with: nil, afterDelay: 0.5)
        }
    }
    
    @objc func reload() {
        self.getArtistDataWith( andSearchText: strSearchText)
    }
    
    func getArtistDataWith(andSearchText: String) {
        getBookingDataWith(0)
    }
}
//MARK: - btn Actions
extension PastFutureBookingVC {
    @IBAction func btnSearchOptionFilter(_ sender: UIButton) {
        self.view.endEditing(true)
        self.viewSearch.isHidden = !self.viewSearch.isHidden
        if self.strSearchText != "" || self.txtSearchFilter.text != ""{
            self.txtSearchFilter.text = ""
            self.strSearchText = ""
        getBookingDataWith(0)
        }
        self.btnHiddenSearch.isHidden = self.viewSearch.isHidden
    }
    
    @IBAction func btnHiddenSearchFilter(_ sender: UIButton) {
        if self.txtSearchFilter.text == ""  || self.strSearchText == ""{
            self.viewSearch.isHidden = true
        }
        self.btnHiddenSearch.isHidden = true
    }
    @IBAction func btnDropDownFilter(_ sender: UIButton) {
        self.view.endEditing(true)
        let arrBookingType = ["All Type","In Call","Out Call"]
        
        let objGetData = arrBookingType.map { $0 }
        guard objGetData.isEmpty == false else {
            return
        }
        self.viewBottumTable.isHidden = false
        self.tblView.reloadData()
    }
    
    
    
    
    @IBAction func btnBookingTypeAction(_ sender: UIButton){
        self.chengDropDownAction(tag: sender.tag)
    }
    
    
    func setuoDropDownAppearance()  {
        let appearance = DropDown.appearance()
        appearance.cellHeight = 40
        appearance.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        appearance.selectionBackgroundColor = UIColor(red: 0.6494, green: 0.8155, blue: 1.0, alpha: 0.2)
        appearance.separatorColor = UIColor.black// #colorLiteral(red: 0.8392156863, green: 0.8392156863, blue: 0.8392156863, alpha: 1)
        appearance.cornerRadius = 5
        appearance.shadowColor = UIColor.black //UIColor(white: 0.6, alpha: 1)
        appearance.shadowOpacity = 1
        appearance.shadowRadius = 1
        appearance.animationduration = 0.25
        appearance.textColor = .black
        appearance.textFont = UIFont (name: "Nunito-Regular", size: 14) ?? .systemFont(ofSize: 14)

    }
    func chengDropDownAction(tag:Int){
        if tag == 0{
            if self.strBookingType == "All Type"{
                self.viewAllType.isHidden = true
                self.viewInCallType.isHidden = false
                self.viewOutCallType.isHidden = false
            }else if self.strBookingType == "In Call"{
                self.viewAllType.isHidden = false
                self.viewInCallType.isHidden = true
                self.viewOutCallType.isHidden = false
            }else if self.strBookingType == "Out Call"{
                self.viewAllType.isHidden = false
                self.viewInCallType.isHidden = false
                self.viewOutCallType.isHidden = true
            }
        }else {
            if tag == 1{
                self.strBookingType = "All Type"
                self.arrFutureBookingData = self.arrFutureBookingDataAll
                self.arrPastBookingData = self.arrPastBookingDataAll
            }else if tag == 2{
                self.strBookingType = "In Call"
                let filteredArray = self.arrFutureBookingDataAll.filter(){ $0.strBookingType.contains("1") }
                self.arrFutureBookingData = filteredArray
                let filteredArrayPast = self.arrPastBookingDataAll.filter(){ $0.strBookingType.contains("1") }
                self.arrPastBookingData = filteredArrayPast
            }else if tag == 3{
                self.strBookingType = "Out Call"
                let filteredArray = self.arrFutureBookingDataAll.filter(){ $0.strBookingType.contains("2") }
                self.arrFutureBookingData = filteredArray
                let filteredArrayPast = self.arrPastBookingDataAll.filter(){ $0.strBookingType.contains("2") }
                self.arrPastBookingData = filteredArrayPast
            }
            if self.isBookingHistory == 1{
                self.tblPastBooking.reloadData()
                if self.arrPastBookingData.count == 0 {
                     self.lblNoRecord.isHidden = false
                }else{
                      self.tblPastBooking.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
                      self.lblNoRecord.isHidden = true
                }
            }else if self.isBookingHistory == 2{
                self.tblFutureBooking.reloadData()
                if self.arrFutureBookingData.count == 0 {
                    self.lblNoRecord.isHidden = false
                }else{
                    self.tblFutureBooking.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
                    self.lblNoRecord.isHidden = true
                }
            }else if self.isBookingHistory == 3{
                self.tblFutureBooking.reloadData()
                if self.arrCancelBookingData.count == 0 {
                    self.lblNoRecord.isHidden = false
                }else{
                    self.tblFutureBooking.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
                    self.lblNoRecord.isHidden = true
                }
            }
            self.viewAllType.isHidden = true
            self.viewInCallType.isHidden = true
            self.viewOutCallType.isHidden = true
        }
        self.btnMainType.setTitle(self.strBookingType, for: .normal)
    }
    
    @IBAction func btnBackAction(_ sender: Any){
        objAppShareData.clearNotificationData()
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnProfileAction(_ sender: Any) {
        self.view.endEditing(true)
        objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
    }
    
    @IBAction func segmentButtonPastFuture(_ sender: UIButton) {
        self.chengSegmentOnSelectedTag(sender: sender.tag)
    }

    func chengSegmentOnSelectedTag(sender:Int){
        self.activity.stopAnimating()
        fromLastTab = true
        self.viewAllType.isHidden = true
        self.viewInCallType.isHidden = true
        self.viewOutCallType.isHidden = true
        self.btnMainType.setTitle(self.strBookingType, for: .normal)

        if sender == 0 {
        
            
            if isBookingHistory == 1{
               return
            }
            self.strBookingType = "All Type"
            self.chengDropDownAction(tag: 1)

            self.arrPastBookingDataAll.removeAll()
            self.arrFutureBookingDataAll.removeAll()
//            self.tblPastBooking.reloadData()
//            self.tblFutureBooking.reloadData()


            objWebserviceManager.StartIndicator()

            self.imgAppointments.backgroundColor = UIColor.lightGray
            self.imgBookingHistory.backgroundColor = appColor
            self.btnBookingHistory.setTitleColor(appColor, for: .normal)
            self.btnAppointments.setTitleColor(UIColor.lightGray, for: .normal)
            
            self.btnCancelBooking.setTitleColor(UIColor.lightGray, for: .normal)
            self.imgCancelBooking.backgroundColor = UIColor.lightGray

            isBookingHistory = 1
            viewPastBooking.isHidden = false
            viewFutureBooking.isHidden = true
            self.lblNoRecord.isHidden = true
            if arrPastBookingData.count == 0{
                self.strBookingType = "All Type"
                self.getBookingDataWith(0)
                self.strBookingType = "All Type"
            }
        
        }else if sender == 1{
            if isBookingHistory == 2{
                return
            }
            self.chengDropDownAction(tag: 1)
            self.arrPastBookingDataAll.removeAll()
            self.arrFutureBookingDataAll.removeAll()
//            self.tblPastBooking.reloadData()
//            self.tblFutureBooking.reloadData()
            
            self.strBookingType = "All Type"
            
            objWebserviceManager.StartIndicator()
            
            self.imgBookingHistory.backgroundColor = UIColor.lightGray
            self.imgAppointments.backgroundColor = appColor
            self.btnBookingHistory.setTitleColor(UIColor.lightGray, for: .normal)
            self.btnAppointments.setTitleColor(appColor, for: .normal)
            
            self.btnCancelBooking.setTitleColor(UIColor.lightGray, for: .normal)
            self.imgCancelBooking.backgroundColor = UIColor.lightGray

            isBookingHistory = 2
            viewPastBooking.isHidden = true
            viewFutureBooking.isHidden = false
            self.lblNoRecord.isHidden = true
            if arrFutureBookingData.count == 0{
                self.strBookingType = "All Type"
                self.getBookingDataWith(0)
                self.strBookingType = "All Type"
            }
        }else if sender == 2{
            if isBookingHistory == 3{
                return
            }
            self.chengDropDownAction(tag: 1)
            self.arrCancelBookingData.removeAll()
            self.arrCancelBookingDataAll.removeAll()
//            self.tblPastBooking.reloadData()
//            self.tblFutureBooking.reloadData()
            
            self.strBookingType = "All Type"
            
            objWebserviceManager.StartIndicator()
            
            self.imgBookingHistory.backgroundColor = UIColor.lightGray
            self.imgAppointments.backgroundColor = UIColor.lightGray
            
            self.btnBookingHistory.setTitleColor(UIColor.lightGray, for: .normal)
            self.btnAppointments.setTitleColor(UIColor.lightGray, for: .normal)
            
            self.btnCancelBooking.setTitleColor(appColor, for: .normal)
            self.imgCancelBooking.backgroundColor = appColor

            isBookingHistory = 3
            viewPastBooking.isHidden = true
            viewFutureBooking.isHidden = false
            self.lblNoRecord.isHidden = true
            
            if arrCancelBookingData.count == 0{
                self.strBookingType = "All Type"
                self.getBookingDataWith(0)
                self.strBookingType = "All Type"
            }
        }
    }
}

// MARK: - table view Delegate and Datasource
extension PastFutureBookingVC : CellPastFutureRatingDelegate, CellFutureBookingDelegate, CellPastFutureReviewDelegate , CellSmallPastFutureReviewDelegate, CellSmallPastFutureRatingDelegate {
    
    // MARK: - Tableview delegate methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblPastBooking{
            return arrPastBookingData.count
        }else if tableView == tblView{
            return 3
        }else if tableView == tblFutureBooking{
            if isBookingHistory == 3{
               return self.arrCancelBookingData.count
            }else{
                return arrFutureBookingData.count
            }
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        if tableView == tblPastBooking {
            if self.arrPastBookingData.count < self.totalCount {
                let nextPage: Int = Int(indexPath.item / pageSize) + 1
                let preloadIndex = nextPage * pageSize - preloadMargin
                if (indexPath.item >= preloadIndex && lastLoadedPage < nextPage) {
                    self.getBookingDataWith(nextPage)
                }
            }
        }else if tableView == tblView{
            
        }else{
            if isBookingHistory == 3{
                if self.arrCancelBookingData.count < self.totalCount3 {
                    let nextPage: Int = Int(indexPath.item / pageSize) + 1
                    let preloadIndex = nextPage * pageSize - preloadMargin
                    if (indexPath.item >= preloadIndex && lastLoadedPage3 < nextPage) {
                        self.getBookingDataWith(nextPage)
                    }
                }
            }else{
                if self.arrFutureBookingData.count < self.totalCount2 {
                    let nextPage: Int = Int(indexPath.item / pageSize) + 1
                    let preloadIndex = nextPage * pageSize - preloadMargin
                    if (indexPath.item >= preloadIndex && lastLoadedPage2 < nextPage) {
                        self.getBookingDataWith(nextPage)
                    }
                }
            }
            
        }
        
        
        
        
if tableView == tblPastBooking {
            
            let obj  : PastFutureBookingData = arrPastBookingData[indexPath.row]
                    var objUser = UserDetail(dict: [:])
    
                if obj.arrUserDetail.count > 0{
                        objUser = obj.arrUserDetail[0]
                    }
            
       
                if indexPath != selectedIndexPath {
                    
                    let cellIdentifier = "CellSmallPastFutureReview"
                    if let cell : CellSmallPastFutureReview = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)! as? CellSmallPastFutureReview {
                        
                        cell.delegate = self
                        cell.indexPath = indexPath
                        obj.indexPath = indexPath
                        cell.vwArtistRating.value = CGFloat(obj.artistRating)
                       // cell.vwRating.value = CGFloat(obj.rating)
                        cell.setDataInCollection(obj: obj)
                        cell.lblDate.text = self.getFormattedBookingDateToShowFrom(strDate: obj.bookingDate)//obj.bookingDate
                        cell.lblDate.text = cell.lblDate.text! + ", " + obj.bookingTime
                        cell.lblAmount.text = "£" + obj.totalPrice
                        
                        cell.btnReview.isHidden = false

                        if obj.customerType == "walking"{
                            cell.btnReview.isHidden = true
                            cell.vwArtistRating.isHidden = true
                            if obj.arrClientInfo.count > 0 {
                                cell.lblName.text = obj.arrClientInfo[0].firstName+" "+obj.arrClientInfo[0].lastName
                            }
                            cell.imgVwProfile.image = #imageLiteral(resourceName: "cellBackground")
                            cell.lblWalkingStatus.isHidden = false
                        }else{
                            cell.lblWalkingStatus.isHidden = true
                            cell.imgVwProfile.image = #imageLiteral(resourceName: "cellBackground")
                            cell.lblName.text = obj.arrUserDetail[0].userName
                            let strImg = obj.arrUserDetail[0].profileImage
                            if strImg != "" {
                                if let url = URL(string: strImg){
                                    //cell.imgVwProfile.af_setImage(withURL: url, placeholderImage: UIImage(named: "cellBackground"))
                                    cell.imgVwProfile.sd_setImage(with: url, placeholderImage:#imageLiteral(resourceName: "cellBackground"))
                                }
                            }
                            cell.vwArtistRating.isHidden = false
                        }
                        if CGFloat(obj.artistRating) > 0 {
                            cell.btnReview.setTitle("Edit Review", for: .normal)
                            let DynamicView=UIView(frame: CGRect(x: 30, y: 20, width: cell.btnReview.frame.width-30, height: 1))
                            DynamicView.backgroundColor = UIColor.darkGray
                            cell.btnReview.addSubview(DynamicView)
                            cell.btnReview.backgroundColor = UIColor.clear
                            cell.btnReview.setTitleColor(UIColor.darkGray, for: .normal)
                            cell.btnReview.contentHorizontalAlignment = .right

                        }else{
                            cell.btnReview.setTitle("Give Review", for: .normal)
                        }
                        cell.btnProfile.tag = indexPath.row
                        cell.btnProfile.addTarget(self, action:#selector(btnProfileRedirectionAction1(sender:)) , for: .touchUpInside)
                        return cell
                    }else{
                        return UITableViewCell()
                    }
                    
                }else{
                    
                    let cellIdentifier = "CellPastFutureReview"
                    if let cell : CellPastFutureReview = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)! as? CellPastFutureReview {
                        
                        cell.delegate = self
                        cell.indexPath = indexPath
                        obj.indexPath = indexPath
                        cell.reviewLimit = reviewTextLimit
                        //cell.lblTextLimit.text = "\(cell.reviewLimit)"
                        cell.vwArtistRating.value = CGFloat(obj.artistRating)
                        cell.setDataInCollection(obj: obj)
                        //cell.lblTextLimit.isHidden = true
                        cell.lblDate.text = self.getFormattedBookingDateToShowFrom(strDate: obj.bookingDate)//obj.bookingDate
                        cell.lblDate.text = cell.lblDate.text! + ", " + obj.bookingTime
                        cell.lblAmount.text = "£" + obj.totalPrice
                       // cell.txtViewComment.text = ""
                        cell.txtViewComment.text = obj.reviewByArtist
                        cell.vwRating.value = CGFloat(obj.artistRating)
                        cell.btnReview.isHidden = false
                      
                        if obj.customerType == "walking"{
                            cell.btnReview.isHidden = true
                            if obj.arrClientInfo.count > 0 {
                                cell.lblName.text = obj.arrClientInfo[0].firstName+" "+obj.arrClientInfo[0].lastName
                            }
                            cell.imgVwProfile.image = #imageLiteral(resourceName: "cellBackground")
                        }else{
                            cell.imgVwProfile.image = #imageLiteral(resourceName: "cellBackground")
                            cell.lblName.text = obj.arrUserDetail[0].userName
                            let strImg = obj.arrUserDetail[0].profileImage
                            if strImg != "" {
                                if let url = URL(string: strImg){
                                    //cell.imgVwProfile.af_setImage(withURL: url, placeholderImage: UIImage(named: "cellBackground"))
                                    cell.imgVwProfile.sd_setImage(with: url, placeholderImage:#imageLiteral(resourceName: "cellBackground"))
                                }
                            }
                        }
                        cell.ratingLimit = obj.artistRating
                        if CGFloat(obj.artistRating) > 0 {
                            cell.btnReview.setTitle("Edit Review", for: .normal)
                            cell.vwRating.isUserInteractionEnabled = true
                            //cell.txtViewComment.isUserInteractionEnabled = false
                            let DynamicView=UIView(frame: CGRect(x: 30, y: 20, width: cell.btnReview.frame.width-30, height: 1))
                            DynamicView.backgroundColor = UIColor.darkGray
                            cell.btnReview.addSubview(DynamicView)
                            cell.btnReview.backgroundColor = UIColor.clear
                            cell.btnReview.setTitleColor(UIColor.darkGray, for: .normal)
                            cell.btnReview.contentHorizontalAlignment = .right
                        }else{
                            cell.vwRating.isUserInteractionEnabled = true
                           // cell.txtViewComment.isUserInteractionEnabled = true
                            cell.btnReview.setTitle("Give Review", for: .normal)
                        }
                        cell.btnProfile.tag = indexPath.row
                        cell.btnProfile.addTarget(self, action:#selector(btnProfileRedirectionAction2(sender:)) , for: .touchUpInside)
                        return cell
                    }else{
                        return UITableViewCell()
                    }                    
                }
}else if tableView == tblView{
    let cellIdentifier = "CellBottumTableList"
    if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? CellBottumTableList{
        let arrBookingType = ["All Type","In Call","Out Call"]
            let obj = arrBookingType[indexPath.row]
            cell.lblTitle.text = obj
        return cell
    }
    return UITableViewCell()
}else{
    var obj = PastFutureBookingData(dict:["":""])
    if self.isBookingHistory == 3{
        if indexPath.row+1 <= self.arrCancelBookingData.count{
          obj = self.arrCancelBookingData[indexPath.row]
        }else{
            return UITableViewCell()
        }
    }else{
        if indexPath.row+1 <= self.arrFutureBookingData.count{
            obj = self.arrFutureBookingData[indexPath.row]
        }else{
            return UITableViewCell()
        }
     }
    

            // bookStatus = 0: for pending , 1: for accept, 2: for reject or cancle, 3: for complete
            
            let cellIdentifier = "CellFutureBooking"
            if let cell : CellFutureBooking = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)! as? CellFutureBooking {
                
                cell.delegate = self
                cell.indexPath = indexPath
                obj.indexPath = indexPath
                                
                cell.lblName.text = obj.userName
                cell.lblDate.text = self.getFormattedBookingDateToShowFrom(strDate: obj.bookingDate)//obj.bookingDate
                cell.lblDate.text = cell.lblDate.text! + ", " + obj.bookingTime
                cell.lblAmount.text = "£" + obj.totalPrice
                
                if self.strBookingType == "All Type"{
                    cell.lblBookingType.isHidden = false
                    if obj.bookingType == 1{
                        cell.lblBookingType.text = "In Call"
                    }else{
                        cell.lblBookingType.text = "Out Call"
                    }
                }else{
                    cell.lblBookingType.isHidden = true
                }
                cell.setDataInCollection(obj: obj)
                cell.imgVwProfile.image = #imageLiteral(resourceName: "cellBackground")
                let strImg = obj.profileImage
                if strImg != "" {
                    if let url = URL(string: strImg){
                        //cell.imgVwProfile.af_setImage(withURL: url, placeholderImage: UIImage(named: "cellBackground"))
                        cell.imgVwProfile.sd_setImage(with: url, placeholderImage:#imageLiteral(resourceName: "cellBackground"))
                    }
                }
                
                if obj.arr_artistService.count > 0 {
                  cell.lblServiceCategory.text = obj.arr_artistService[0]
                }else{
                  cell.lblServiceCategory.text = "NA"
                }

                
                if obj.bookStatus == 0 {
                   cell.btnRequest.setTitle("Pending", for: .normal)
                    cell.btnRequest.setTitleColor(#colorLiteral(red: 0.9200255673, green: 0.4889633489, blue: 0.08423942367, alpha: 1), for: .normal)
                   cell.btnRequest.isHidden = false
                }else if obj.bookStatus == 1 {
                    cell.btnRequest.setTitle("Confirmed", for: .normal)
                    cell.btnRequest.setTitleColor(#colorLiteral(red: 0, green: 0.7333333333, blue: 0.1529411765, alpha: 1), for: .normal)
                    cell.btnRequest.isHidden = false
                }else if (obj.bookStatus == 3 && obj.paymentStatus == 0) {
                    cell.btnRequest.setTitle("Payment Pending", for: .normal)
                    cell.btnRequest.setTitleColor(#colorLiteral(red: 0.9200255673, green: 0.4889633489, blue: 0.08423942367, alpha: 1), for: .normal)
                    cell.btnRequest.isHidden = false
                }else if (obj.bookStatus == 5 || obj.paymentStatus == 4) {
                    cell.btnRequest.setTitle("In Progress", for: .normal)
                    cell.btnRequest.setTitleColor(#colorLiteral(red: 0, green: 0.7333333333, blue: 0.1529411765, alpha: 1), for: .normal)
                    cell.btnRequest.isHidden = false
                }else{
                   cell.btnRequest.isHidden = true
                }
                if obj.customerType == "walking"{
                    cell.lblBookingType.text = "Walk-in"
                    if obj.arrClientInfo.count > 0 {
                        cell.lblName.text = obj.arrClientInfo[0].firstName+" "+obj.arrClientInfo[0].lastName
                    }
                    cell.imgVwProfile.image = #imageLiteral(resourceName: "cellBackground")
                }else{
                    cell.imgVwProfile.image = #imageLiteral(resourceName: "cellBackground")
                    cell.lblName.text = obj.arrUserDetail[0].userName
                    let strImg = obj.arrUserDetail[0].profileImage
                    if strImg != "" {
                        if let url = URL(string: strImg){
                            //cell.imgVwProfile.af_setImage(withURL: url, placeholderImage: UIImage(named: "cellBackground"))
                            cell.imgVwProfile.sd_setImage(with: url, placeholderImage:#imageLiteral(resourceName: "cellBackground"))
                        }
                    }
                }
                cell.btnProfile.tag = indexPath.row
                cell.btnProfile.addTarget(self, action:#selector(btnProfileRedirectionAction3(sender:)) , for: .touchUpInside)
                
                return cell
            }else{
                return UITableViewCell()
            }
        }        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        //return
        var objPastFutureBookingData  : PastFutureBookingData = PastFutureBookingData(dict: ["":""])
        if tableView == tblPastBooking {
            objPastFutureBookingData = arrPastBookingData[indexPath.row]
            self.gotoAppoitmentBookingVC(obj: objPastFutureBookingData)
        }else if tableView == tblView{
            self.viewBottumTable.isHidden = true
            let arrBookingType = ["All Type","In Call","Out Call"]
            let item = arrBookingType[indexPath.row]
            if  item == "All Type"{
                self.chengDropDownAction(tag: 1)
            }else if item == "In Call"{
                self.chengDropDownAction(tag: 2)
            }else if item == "Out Call"{
                self.chengDropDownAction(tag: 3)
            }else{
                self.chengDropDownAction(tag: 1)
            }
        }else{
            if isBookingHistory == 3{
                objPastFutureBookingData = arrCancelBookingData[indexPath.row]
                self.gotoAppoitmentBookingVC(obj: objPastFutureBookingData)
            }else{
                objPastFutureBookingData = arrFutureBookingData[indexPath.row]
                self.gotoAppoitmentBookingVC(obj: objPastFutureBookingData)
            }
         

        }
    }
    
}

// MARK: - table view Delegate
extension PastFutureBookingVC {
    
    func btnDropDownTappedInRatingCell(at index:IndexPath){
        self.closeBigCell(at: index)
    }
    
    func btnDropDownTappedInSmallRatingCell(at index: IndexPath) {
        self.openBigCell(at: index)
    }
    
    func btnDropDownTappedInReviewCell(at index:IndexPath){
        self.closeBigCell(at: index)
    }
    
    func btnDropDownTappedInSmallReviewCell(at index: IndexPath) {
        self.openBigCell(at: index)
    }
    
    func btnSubmitTapped_ReviewCell(at index:IndexPath){
       
        // CellPastFutureReview
        
        if let cell : CellPastFutureReview = self.tblPastBooking.cellForRow(at: index) as? CellPastFutureReview {
            
            cell.txtViewComment.text = cell.txtViewComment.text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            
//            if cell.txtViewComment.text == "" {
//               objAppShareData.showAlert(withMessage: "Please enter some text", type: alertType.bannerDark, on: self)
//            }else
            if cell.vwRating.value == 0 {
               objAppShareData.showAlert(withMessage: "Please give rating", type: alertType.bannerDark, on: self)
            }else{
               
                cell.txtViewComment.endEditing(true)
                let rating = "\(cell.vwRating.value)"
                
               // objAppShareData.showAlert(withMessage: "Submitted Successfully", type: alertType.bannerDark, on: self)
                
                let obj = arrPastBookingData[index.row]
                
                var parameters = [String:Any]()
                
                var strUserId : String = ""
                if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]{
                    
                    if let userId = dict["_id"] as? String {
                        strUserId = userId
                    }
                }
                var type = "insert"
                if obj.artistRating != 0{
                    type = "edit"
                }
                
                parameters = [
                    "type":type,
                    "artistId" : obj.artistId,
                    "userId" : obj.userId,
                    "bookingId":obj._id,
                    "reviewByUser": "",
                    "reviewByArtist":cell.txtViewComment.text ?? "",
                    "rating": rating
                ]
                self.callWebserviceFor_addCommentAndRating(parameters : parameters, obj: obj)
            }
        }
    }
    
    func btnRebookTapped_ReviewCell(at index:IndexPath){
        objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
        self.closeBigCell(at: index)
    }
    
    func btnSendTapped(at index:IndexPath){
        objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
    }
    
    func btnRequestTapped(at index:IndexPath){
        
        let obj : PastFutureBookingData = arrFutureBookingData[index.row]
        
        if (obj.bookStatus == 3 && obj.paymentStatus == 0) {
            self.gotoPaymentInfoVC(obj : obj)
        }else if obj.bookStatus == 0 {
            self.gotoAppoitmentBookingVC(obj: obj)
        }
    }
    
    func openBigCell(at index:IndexPath){
        
        // in fact, was this very row selected,
        // and the user is clicking to deselect it...
        // if you don't want "click a selected row to deselect"
        // then on't include this clause.
        if selectedIndexPath == index
        {
            
            selectedIndexPath = nil
            self.tblPastBooking.reloadRows(at: [index], with: .none)
            //tableView.deselectRowAtIndexPath(indexPath, animated:false)
            return
        }
        
        
        // in fact, was some other row selected??
        // user is changing to this row? if so, also deselect that row
        if selectedIndexPath != nil
        {
            let pleaseRedrawMe = selectedIndexPath!
            // (note that it will be drawn un-selected
            // since we're chaging the 'selectedIndexPath' global)
            selectedIndexPath = index
            self.tblPastBooking.reloadRows(
                at: [pleaseRedrawMe, index],
                with:UITableView.RowAnimation.none)
            return;
        }
        
        // no previous selection.
        // simply select that new one the user just touched.
        // note that you can not use Apple's willDeselectRowAtIndexPath
        // functions ... because they are freaky
        selectedIndexPath = index
        self.tblPastBooking.reloadRows(at: [index], with: .none)
    }
    
    func closeBigCell(at index:IndexPath){
        
        // in fact, was this very row selected,
        // and the user is clicking to deselect it...
        // if you don't want "click a selected row to deselect"
        // then on't include this clause.
        if selectedIndexPath == index
        {
            selectedIndexPath = nil
            self.tblPastBooking.reloadRows(at: [index], with: .none)
            //tableView.deselectRowAtIndexPath(indexPath, animated:false)
            return
        }
        
        // in fact, was some other row selected??
        // user is changing to this row? if so, also deselect that row
        if selectedIndexPath != nil
        {
            let pleaseRedrawMe = selectedIndexPath!
            // (note that it will be drawn un-selected
            // since we're chaging the 'selectedIndexPath' global)
            selectedIndexPath = nil
            self.tblPastBooking.reloadRows(
                at: [pleaseRedrawMe, index],
                with:UITableView.RowAnimation.none)
            return;
        }
        selectedIndexPath = nil
        self.tblPastBooking.reloadRows(at: [index], with: .none)
    }
    
}

//MARK: - custome method extension
extension PastFutureBookingVC {
    
    func getBookingDataWith(_ page: Int) {
        
        var parameters = [String:Any]()
        
        var strBookingType : String = ""
        
         if self.isBookingHistory == 1 {
            self.lastLoadedPage = page
            self.pageNo = page
            strBookingType = "Past"
        }else if self.isBookingHistory == 2{
            self.lastLoadedPage2 = page
            self.pageNo2 = page
            strBookingType = ""
         }else  if self.isBookingHistory == 3{
            self.lastLoadedPage3 = page
            self.pageNo3 = page
            strBookingType = "Cancelled Booking"
        }
        
        parameters = [ "type": strBookingType, "search":self.strSearchText  ]
        self.webServiceCall_getBookingData(parameters: parameters)
    }
    
    
    func webServiceCall_getBookingData(parameters : [String:Any]){
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        
        //if self.segmentController.selectedSegmentIndex == 0 {
        if self.isBookingHistory  == 1{
            
            if  !self.refreshControl.isRefreshing && self.pageNo == 0 {
                self.activity.startAnimating()
            }
            if fromLastTab == true{
                 self.activity.stopAnimating()
                fromLastTab = false
            }
        }else if  self.isBookingHistory  == 2{
            
            if  !self.refreshControl2.isRefreshing && self.pageNo2 == 0 {
                self.activity.startAnimating()
            }
            if fromLastTab == true{
                self.activity.stopAnimating()
                fromLastTab = false
            }
        }else if  self.isBookingHistory  == 3{
            
            if  !self.refreshControl2.isRefreshing && self.pageNo3 == 0 {
                self.activity.startAnimating()
            }
            if fromLastTab == true{
                self.activity.stopAnimating()
                fromLastTab = false
            }
        }
        
        self.lblNoRecord.isHidden = true
        objWebserviceManager.requestPost(strURL:  WebURL.userBookingHistory, params: parameters as [String : AnyObject] , success: { response in
            print(response)
          //  objWebserviceManager.StopIndicator()
            self.activity.stopAnimating() 
            self.refreshControl.endRefreshing()
            self.refreshControl2.endRefreshing()
            
            self.arrPastBookingDataAll.removeAll()
            self.arrFutureBookingDataAll.removeAll()
            
            if self.isBookingHistory == 1  {
                
                if self.pageNo == 0 {
                    self.arrPastBookingData.removeAll()
                    self.tblPastBooking.reloadData()
                }
            }else if self.isBookingHistory == 2 {
                
                if self.pageNo2 == 0 {
                    self.arrFutureBookingData.removeAll()
                    self.tblFutureBooking.reloadData()
                }
            }else if self.isBookingHistory == 3 {
                if self.pageNo3 == 0 {
                    self.arrCancelBookingData.removeAll()
                    self.tblFutureBooking.reloadData()
                }
            }
            
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else{
                let strStatus =  response["status"] as? String ?? ""
              
                if strStatus == k_success{
                   
                    if self.isBookingHistory == 1 {
                       
                        if let totalCount = response["total"] as? Int{
                            self.totalCount = totalCount
                        }
                        //if let arr = response["Booking"] as? [[String : Any]]{
                        if let arr = response["data"] as? [[String : Any]]{
                            for dict in arr{
                                let obj = PastFutureBookingData.init(dict: dict)
                                self.arrPastBookingDataAll.append(obj)
                            }
                        }
                        
                    }else if self.isBookingHistory == 2 {
                        
                        if let totalCount = response["total"] as? Int{
                            self.totalCount2 = totalCount
                        }
                        //if let arr = response["Booking"] as? [[String : Any]]{
                        if let arr = response["data"] as? [[String : Any]]{
                            for dict in arr{
                                let obj = PastFutureBookingData.init(dict: dict)
                            self.arrFutureBookingDataAll.append(obj)
                            }
                      // self.arrFutureBookingDataAll.reverse()
                        }
                    
                    }else if self.isBookingHistory == 3 {
                        
                        if let totalCount = response["total"] as? Int{
                            self.totalCount3 = totalCount
                        }
                        //if let arr = response["Booking"] as? [[String : Any]]{
                        if let arr = response["data"] as? [[String : Any]]{
                            for dict in arr{
                                let obj = PastFutureBookingData.init(dict: dict)
                                self.arrCancelBookingDataAll.append(obj)
                            }
                            // self.arrFutureBookingDataAll.reverse()
                        }
                    }
                   
                    if self.strBookingType == "All Type"{
           

                        let filteredArray1 = self.arrFutureBookingDataAll.filter(){ $0.bookStatus != 2 }
                        self.arrFutureBookingDataAll = filteredArray1
                        self.arrFutureBookingData = self.arrFutureBookingDataAll
                        
                        let filteredArray2 = self.arrPastBookingDataAll.filter(){ $0.bookStatus != 2 }
                        self.arrPastBookingDataAll = filteredArray2
                        self.arrPastBookingData = self.arrPastBookingDataAll
                        
                        let filteredArray3 = self.arrCancelBookingDataAll.filter(){ $0.bookStatus == 2 }
                        self.arrCancelBookingDataAll = filteredArray3
                        self.arrCancelBookingData = self.arrCancelBookingDataAll
                        
                    }else if self.strBookingType == "In Call"{
                        
                        let filteredArray = self.arrFutureBookingDataAll.filter(){ $0.strBookingType.contains("1")   && $0.bookStatus != 2}
                        self.arrFutureBookingData = filteredArray
                        
                        let filteredArrayPast = self.arrPastBookingDataAll.filter(){ $0.strBookingType.contains("1")   && $0.bookStatus != 2}
                        self.arrPastBookingData = filteredArrayPast
                        
                        let filteredArrayCancel = self.arrCancelBookingDataAll.filter(){ $0.strBookingType.contains("1")  && $0.bookStatus == 2 }
                        self.arrCancelBookingData = filteredArrayCancel
                        
                    }else if self.strBookingType == "Out Call"{
                         let filteredArray = self.arrFutureBookingDataAll.filter(){ $0.strBookingType.contains("2")    && $0.bookStatus != 2}
                        self.arrFutureBookingData = filteredArray
                        let filteredArrayPast = self.arrPastBookingDataAll.filter(){ $0.strBookingType.contains("2")    && $0.bookStatus != 2}
                        self.arrPastBookingData = filteredArrayPast
                        let filteredArrayCancel = self.arrCancelBookingDataAll.filter(){ $0.strBookingType.contains("2")  && $0.bookStatus == 2 }
                        self.arrCancelBookingData = filteredArrayCancel
                    }
                }else{
                    
                    if strStatus == "fail"{
                        
                    }else{
                       
                        if let msg = response["message"] as? String{
                        objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                        }
                    }
                }
            }
            
            if self.isBookingHistory == 1{
                self.tblPastBooking.reloadData()
                if self.arrPastBookingData.count == 0 {
                    self.lblNoRecord.isHidden = false
                }else{
                    self.lblNoRecord.isHidden = true
                }
            }else if  self.isBookingHistory == 2{
                self.tblFutureBooking.reloadData()
                if self.arrFutureBookingData.count == 0 {
                    self.lblNoRecord.isHidden = false
                }else{
                    self.lblNoRecord.isHidden = true
                }
            }else if  self.isBookingHistory == 3{
                self.tblFutureBooking.reloadData()
                if self.arrCancelBookingData.count == 0 {
                    self.lblNoRecord.isHidden = false
                }else{
                    self.lblNoRecord.isHidden = true
                }
            }
            
            
        }) { (error) in
            self.activity.stopAnimating()
            self.refreshControl.endRefreshing()
            self.refreshControl2.endRefreshing()
            
            if self.isBookingHistory  == 1 {
                self.tblPastBooking.reloadData()
                if self.arrPastBookingData.count == 0 {
                    self.lblNoRecord.isHidden = false
                }else{
                    self.lblNoRecord.isHidden = true
                }
            }else  if self.isBookingHistory  == 2 {
                self.tblFutureBooking.reloadData()
                if self.arrFutureBookingData.count == 0 {
                    self.lblNoRecord.isHidden = false
                }else{
                    self.lblNoRecord.isHidden = true
                }
            }else  if self.isBookingHistory  == 3 {
                self.tblFutureBooking.reloadData()
                if self.arrCancelBookingData.count == 0 {
                    self.lblNoRecord.isHidden = false
                }else{
                    self.lblNoRecord.isHidden = true
                }
            }
            
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
    func callWebserviceFor_addCommentAndRating(parameters : [String:Any] , obj : PastFutureBookingData ){
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        
        self.activity.startAnimating()
        
        objWebserviceManager.requestPost(strURL: WebURL.bookingReviewRating, params: parameters  , success: { response in
            
            self.activity.stopAnimating()
            
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else{
                
                let strStatus =  response["status"] as? String ?? ""
                let strMsg = response["message"] as? String ?? kErrorMessage
                
                if strStatus == k_success{
                    obj.reviewStatus = 1
                    obj.isReviewAvailable = true
                    obj.reviewByUser = parameters["reviewByArtist"] as? String ?? ""
                    if let rating = parameters["rating"] as? String {
                        obj.artistRating = Int(Double(rating) ?? 0)
                    }
                    self.getBookingDataWith(0)
                    self.closeBigCell(at: obj.indexPath!)
                    objAppShareData.showAlert(withMessage: strMsg, type: alertType.bannerDark,on: self)
                }else{
                    objAppShareData.showAlert(withMessage: strMsg, type: alertType.bannerDark,on: self)
                }
            }
            
        }){ error in
           
            self.activity.stopAnimating()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
}

//MARK: - custome method extension
extension PastFutureBookingVC {
    
    func getFormattedBookingDateToShowFrom(strDate : String) -> String{
        
        if strDate == ""{
            return ""
        }
        
        let formatter  = DateFormatter()
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        formatter.dateFormat = "yyyy-MM-dd"
        if let date = formatter.date(from: strDate){
            formatter.dateFormat = "dd/MM/yyyy" //"dd MMMM yyyy"
            
        let strDate = formatter.string(from: date)
            return strDate
        }
        return ""
    }
    
    func gotoPaymentInfoVC(obj : PastFutureBookingData) {
        
//        self.view.endEditing(true)
//        let sb: UIStoryboard = UIStoryboard(name: "PaymentModule", bundle: Bundle.main)
//        if let objVC = sb.instantiateViewController(withIdentifier:"PaymentInfoVC") as? PaymentInfoVC {
//            objVC.objPastFutureBookingData = obj
//            objVC.hidesBottomBarWhenPushed = true
//            navigationController?.pushViewController(objVC, animated: true)
//        }
    }
    
    func gotoAppoitmentBookingVC(obj : PastFutureBookingData) {
        
        self.view.endEditing(true)
        let sb = UIStoryboard(name:"Booking",bundle:Bundle.main)
        let objChooseType = sb.instantiateViewController(withIdentifier:"ConfirmBookingVC") as! ConfirmBookingVC
        objChooseType.hidesBottomBarWhenPushed = true
        objChooseType.strBookingId =  String(obj._id)
        self.navigationController?.pushViewController(objChooseType, animated: true)

    }
    
    
    @objc func btnProfileRedirectionAction1(sender: UIButton!){
        let objUser = arrPastBookingData[sender.tag]
        self.profileRedirectionAPI(objUsers: objUser)
    }
    @objc func btnProfileRedirectionAction2(sender: UIButton!){
        let objUser = arrPastBookingData[sender.tag]
        self.profileRedirectionAPI(objUsers: objUser)
    }
    @objc func btnProfileRedirectionAction3(sender: UIButton!){
        let objUser = arrFutureBookingData[sender.tag]
        self.profileRedirectionAPI(objUsers: objUser)
    }
    
    func profileRedirectionAPI(objUsers:PastFutureBookingData){
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        if objUsers.customerType == "walking"{
            return
        }
        var objUser = UserDetail(dict: ["":""])
        if objUsers.arrUserDetail.count > 0 {
            objUser = objUsers.arrUserDetail[0]
        }
        
        let dicParam = ["userName":objUser.userName]
        objServiceManager.requestPost(strURL: WebURL.profileByUserName, params: dicParam, success: { response in
            if response["status"] as? String ?? "" == "success"{
                var strId = ""
                var strType = ""
                if let dictUser = response["userDetail"] as? [String : Any]{
                    let myId = dictUser["_id"] as? Int ?? 0
                    strId = String(myId)
                    strType = dictUser["userType"] as? String ?? ""
                }
                let dic = [
                    "tabType" : "people",
                    "tagId": strId,
                    "userType":strType,
                    "title": objUser.userName
                    ] as [String : Any]
                self.openProfileForSelectedTagPopoverWithInfo(dict: dic)
            }
        }) { error in
        }
    }
    func openProfileForSelectedTagPopoverWithInfo(dict : [AnyHashable: Any]){
        
        var dictTemp : [AnyHashable : Any]?
        
        dictTemp = dict
        
        if let dict1 = dict as? [String:[String :Any]] {
            if let dict2 = dict1.first?.value {
                dictTemp = dict2
            }
        }
        
        guard let dictFinal = dictTemp as? [String : Any] else { return }
        
        var strUserType: String?
        var tagId : Int?
        
        if let userType = dictFinal["userType"] as? String{
            strUserType = userType
            
            if let idTag = dictFinal["tagId"] as? Int{
                tagId = idTag
            }else{
                if let idTag = dictFinal["tagId"] as? String{
                    tagId = Int(idTag)
                }
            }
            let id = Int(self.strUserId)
            if id == tagId {
                //isNavigate = true
                objAppShareData.isOtherSelectedForProfile = false
                self.gotoProfileVC()
                return
            }
            
            if let strUserType = strUserType, let tagId =  tagId {
                
                objAppShareData.selectedOtherIdForProfile  = String(tagId)
                objAppShareData.isOtherSelectedForProfile = true
                objAppShareData.selectedOtherTypeForProfile = strUserType  //"artist" or "user"
                //isNavigate = true
                self.gotoProfileVC()
            }
        }
    }
    func gotoProfileVC (){
        self.view.endEditing(true)
        let sb: UIStoryboard = UIStoryboard(name: "ArtistProfile", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"SWRevealViewController") as? SWRevealViewController{
            objVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(objVC, animated: true)
        }
    }
    
    @IBAction func btnHiddenBottumTable(_ sender: UIButton) {
        self.viewBottumTable.isHidden = true
        
    }
}






//
//  ArtistServicesVC.swift
//  MualabBusiness
//
//  Created by mac on 29/05/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import DropDown
import CoreLocation

class BookingDetailVC : UIViewController,UITableViewDelegate,UITableViewDataSource{
    
    @IBOutlet weak var constraintTableHeight: NSLayoutConstraint!
    @IBOutlet weak var lblHeaderOutCallInCall: UILabel!
    @IBOutlet weak var imgArtist: UIImageView!
    @IBOutlet weak var lblOutCallAddress: UILabel!
    @IBOutlet weak var lblArtistName: UILabel!
    @IBOutlet weak var tblBookedServices: UITableView!
    @IBOutlet weak var lblTotalPrice: UILabel!
    @IBOutlet weak var lblRedTotalPrice: UILabel!
    @IBOutlet weak var lblBookingStatus: UILabel!
    @IBOutlet weak var lblPaymentStatus: UILabel!
    @IBOutlet weak var viewRedTotalPrice: UIView!
    @IBOutlet weak var viewVoucherCode: UIView!
    @IBOutlet weak var txtVoucherCode: UITextField!
    @IBOutlet weak var lblVoucherText: UILabel!
    
    @IBOutlet weak var viewReject: UIView!
    @IBOutlet weak var viewAddress: UIView!
    @IBOutlet weak var btnCompleteService: UIButton!
    
    var dictVoucher = [:] as! [String:Any]
    var totalPrice = 0.0
    var discountPrice = 0.0
    var objArtistDetails = ArtistDetails(dict: ["":""])
    var objBookingServices = BookingServices()
    var arrBookedService = [BookingInfo]()
    var arrBookedMainService = [Booking]()
    
    var isOutCallSelectedConfirm = false
    var strMyAddress = ""
    var strBookingId = ""
    var artistId = ""
    var myId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewVoucherCode.isHidden = true
        self.btnCompleteService.isHidden = true
        //self.imgArtist.layer.cornerRadius = 23
        self.imgArtist.layer.masksToBounds = true
        //        let userInfo = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]  ?? ["":""]
        //        self.myId = userInfo[UserDefaults.keys.id] as? String ?? ""
        //        let adminId = UserDefaults.standard.string(forKey: UserDefaults.keys.newAdminId) ?? ""
        //        if adminId == self.artistId || adminId == ""{
        //        }else{
        //            self.artistId = adminId
        //        }
        self.artistId = ""
        self.callWebserviceForGetBookedServices()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //        let userInfo = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]  ?? ["":""]
        //        self.artistId = userInfo[UserDefaults.keys.id] as? String ?? ""
        //        self.myId = userInfo[UserDefaults.keys.id] as? String ?? ""
        //
        //        let adminId = UserDefaults.standard.string(forKey: UserDefaults.keys.newAdminId) ?? ""
        //        if adminId == self.artistId || adminId == ""{
        //        }else{
        //            self.artistId = adminId
        //        }
        self.artistId = ""
    }
}

//MARK: - button extension
extension BookingDetailVC {
    @IBAction func btnTrackAction(_ sender: UIButton) {
        let sb = UIStoryboard(name:"BookingDetailModule",bundle:Bundle.main)
        let objChooseType = sb.instantiateViewController(withIdentifier:"TrackMapVC") as! TrackMapVC
        let objBooking = self.arrBookedMainService[0]
        let objService = self.arrBookedService[sender.tag]
        objChooseType.hidesBottomBarWhenPushed = true
        objChooseType.objBookedMainService = objBooking
        objChooseType.objBookedService = objService
        self.navigationController?.pushViewController(objChooseType, animated: true)
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        objAppShareData.isFromNotification = false
     self.navigationController?.popViewController(animated: true)
    }
}

//MARK: - Custom methods Extension
fileprivate extension BookingDetailVC{
    
    
    func manageView(){
        // self.viewRedTotalPrice.isHidden = true
        self.manageOutCallInCallView()
        self.managePaymentMethodView()
    }
    func clearVoucherCode(){
        self.dictVoucher = [:]
        let doubleStrNN = String(format: "%.2f", ceil(self.totalPrice))
        self.lblTotalPrice.text = "£" + doubleStrNN
        //self.lblTotalPrice.text = "£" + String(self.totalPrice)
        self.viewRedTotalPrice.isHidden = true
        self.txtVoucherCode.text = ""
        self.txtVoucherCode.isUserInteractionEnabled = true
    }
    func managePaymentMethodView(){
        
    }
    func manageOutCallInCallView(){
        
        if self.isOutCallSelectedConfirm{
            self.lblHeaderOutCallInCall.text = "Out Call"
            self.lblOutCallAddress.text = self.strMyAddress
        }else{
            self.lblHeaderOutCallInCall.text = "In Call"
        }
        if self.arrBookedMainService.count > 0{
            self.lblOutCallAddress.text = self.arrBookedMainService[0].location
            if self.arrBookedMainService[0].bookingType == "1"{
                self.lblHeaderOutCallInCall.text = "In Call"
            }else if self.arrBookedMainService[0].bookingType == "2"{
                self.lblHeaderOutCallInCall.text = "Out Call"
            }else{
                self.lblHeaderOutCallInCall.text = "Both"
            }
            let objAllBooking = self.arrBookedMainService[0]
            self.viewAddress.isHidden = true
            self.viewReject.isHidden = true
            
            //            let userInfo = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]  ?? ["":""]
            //            self.myId = userInfo[UserDefaults.keys.id] as? String ?? ""
            
            self.btnCompleteService.isHidden = true
            if objAllBooking.bookStatus == "5"{
                //if self.myId == self.artistId || self.artistId == ""   {
                var btnShow = true
                for obj in objAllBooking.arrBookingInfo{
                    if obj.status != "3" {
                        btnShow = false
                    }
                }
                if btnShow == true{
                    self.btnCompleteService.isHidden = false
                }
                //}
            }
            
            
            if objAllBooking.bookStatus == "0" {
                self.viewReject.isHidden = false
            }
            if objAllBooking.bookStatus == "0"{
                self.lblBookingStatus.text = "Pending"
                self.lblBookingStatus.textColor =  #colorLiteral(red: 0.9200255673, green: 0.4889633489, blue: 0.08423942367, alpha: 1)
            } else if objAllBooking.bookStatus == "1"{
                self.lblBookingStatus.text = "Confirmed"
                self.lblBookingStatus.textColor =  #colorLiteral(red: 0, green: 0.7333333333, blue: 0.1529411765, alpha: 1)
                //self.viewReject.isHidden = false
            }else if objAllBooking.bookStatus == "2" {
                self.lblBookingStatus.text = "Cancelled"
                self.lblBookingStatus.textColor =  #colorLiteral(red: 0.9607843137, green: 0.01568627451, blue: 0.01568627451, alpha: 1)
            }else if objAllBooking.bookStatus == "3" {
                self.lblBookingStatus.text = "Completed"
                self.lblBookingStatus.textColor =  #colorLiteral(red: 0, green: 0.7333333333, blue: 0.1529411765, alpha: 1)
                self.viewAddress.isHidden = false
            }else if objAllBooking.bookStatus == "5"{
                self.lblBookingStatus.text = "In progress"
                self.lblBookingStatus.textColor =  #colorLiteral(red: 0, green: 0.7333333333, blue: 0.1529411765, alpha: 1)
            }
            
            
            if objAllBooking.paymentType == "1"{
                self.lblPaymentStatus.text = "Online"
            }else{
                self.lblPaymentStatus.text = "Cash"
            }
        }
    }
    func calculateTotalPrice(){
        var totalPrice = 0.0
        for obj in self.arrBookedService{
            totalPrice = totalPrice + Double(obj.bookingPrice)!
        }
        self.totalPrice = totalPrice
        let doubleStrNN = String(format: "%.2f", ceil(self.totalPrice))
        self.lblTotalPrice.text = "£" + doubleStrNN
        //self.lblTotalPrice.text = "£" + String(totalPrice)
        //self.lblRedTotalPrice.text = "£" + String(totalPrice)
        self.lblRedTotalPrice.text = "£" + doubleStrNN
    }
}

//MARK: - Webservices Extension
fileprivate extension BookingDetailVC{
    
    
    
    func manageVoucherView(dict:[String:Any]){
        self.dictVoucher = dict
        var discountType = 0
        if let discount = dict["discountType"] as? Int{
            discountType = discount
        }else if let discount = dict["discountType"] as? String{
            discountType = Int(discount)!
        }
        var amount = 0.0
        if let amnt = dict["amount"] as? Double{
            amount = amnt
        }else if let amnt = dict["amount"] as? Int{
            amount = Double(amnt)
        }else if let amnt = dict["amount"] as? Float{
            amount = Double(amnt)
        }else if let amnt = dict["amount"] as? String{
            amount = Double(amnt)!
        }
        
        self.lblVoucherText.text = dict["voucherCode"] as? String ?? ""
        //        if discountType == 1{
        //            self.lblVoucherText.text = ""
        //        }else if discountType == 2 {
        //            self.lblVoucherText.text = ""
        //        }
        
        if discountType == 2{
            let newPrice = self.totalPrice - (self.totalPrice*amount/100)
            if newPrice>0{
                self.discountPrice = newPrice
                self.lblTotalPrice.text = "£" + String(format: "%.2f", newPrice)
            }else{
                self.discountPrice = 0.0
                self.lblTotalPrice.text = "£" + "0"
            }
            self.txtVoucherCode.text = "-"+String(amount)+"%"
        }else{
            let newPrice = self.totalPrice-amount
            if newPrice>0{
                self.discountPrice = newPrice
                self.lblTotalPrice.text = "£" + String(format: "%.2f", newPrice)
            }else{
                self.discountPrice = 0.0
                self.lblTotalPrice.text = "£" + "0"
            }
            self.txtVoucherCode.text = "-£"+String(amount)
        }
        self.txtVoucherCode.isUserInteractionEnabled = false
        self.viewRedTotalPrice.isHidden = false
        self.txtVoucherCode.textColor = #colorLiteral(red: 0, green: 0.7333333333, blue: 0.1529411765, alpha: 1)
    }
    
    func callWebserviceForGetBookedServices(){
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        objActivity.startActivityIndicator()
        
        let dicParam = [
            "bookingId":self.strBookingId
            ] as [String : Any]
        
        objServiceManager.requestPostForJson(strURL: WebURL.bookingDetail, params: dicParam, success: { response in
            print(response)
            self.arrBookedService.removeAll()
            objActivity.stopActivity()
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else {
                let strSucessStatus = response["status"] as? String ?? ""
                if  strSucessStatus == "success"{
                    objActivity.stopActivity()
                    print(response)
                    self.saveData(dicts:response)
                }else{
                    let msg = response["message"] as? String ?? ""
                    objAppShareData.showAlert(withMessage: msg , type: alertType.bannerDark,on: self)
                }}
        }) { error in
            objActivity.stopActivity()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
    func saveData(dicts:[String:Any]){
        self.arrBookedMainService.removeAll()
        self.arrBookedService.removeAll()
        if let strBookingt = dicts["data"] as? [String : Any]{
            let objBooking = Booking.init(dict: strBookingt)
            self.arrBookedMainService.append(objBooking)
            if arrBookedMainService.count > 0{
                self.arrBookedService = self.arrBookedMainService[0].arrBookingInfo
            }
           
            
            if objBooking.customerType == "walking"{
                if objBooking.arrClientInfo.count > 0 {
                    self.lblArtistName.text = objBooking.arrClientInfo[0].firstName+" "+objBooking.arrClientInfo[0].lastName
                }
                self.imgArtist.image = #imageLiteral(resourceName: "cellBackground")
            }else{
                if objBooking.arrUserDetail.count > 0{
                    let objUserInfo = objBooking.arrUserDetail[0]
                    self.lblArtistName.text = objUserInfo.userName
                    let url = URL(string: objUserInfo.profileImage )
                    if  url != nil {
                        //self.imgArtist.af_setImage(withURL: url!)
                        self.imgArtist.sd_setImage(with: url!, placeholderImage:#imageLiteral(resourceName: "cellBackground"))
                    }else{
                        self.imgArtist.image = #imageLiteral(resourceName: "cellBackground")
                    }
                }
            }
            self.calculateTotalPrice()
            self.viewRedTotalPrice.isHidden = true
            if let voucherCode = strBookingt["voucher"] as? [String:Any]{
                if voucherCode.count > 0{
                    self.viewVoucherCode.isHidden = false
                    self.manageVoucherView(dict: voucherCode)
                }else{
                    self.viewVoucherCode.isHidden = true
                }
            }
        }
        self.tblBookedServices.reloadData()
        self.manageTableViewHeight()
        self.manageView()
    }
    
    func manageTableViewHeight(){
        if self.arrBookedMainService.count > 0{
        if self.arrBookedMainService[0].bookStatus == "1" || self.arrBookedMainService[0].bookStatus == "5"{
            self.constraintTableHeight.constant = CGFloat(self.arrBookedService.count*260)
            for obj in self.arrBookedService{
                if obj.status != "0"{
                    self.constraintTableHeight.constant = CGFloat(self.arrBookedService.count*325)
                }
            }
        }else{
            self.constraintTableHeight.constant = CGFloat(self.arrBookedService.count*260)
        }
        //self.constraintTableHeight.constant = CGFloat(1*195)
        self.view.layoutIfNeeded()
    }
    }
}

//MARK: - UITableview delegate
extension BookingDetailVC {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrBookedService.count
        //return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let  cell = tableView.dequeueReusableCell(withIdentifier: "SubServiceCell", for: indexPath) as! SubServiceCell
        cell.indexPath = indexPath
        let objBookedService = self.arrBookedService[indexPath.row]
        let objMain = arrBookedMainService[0]
        
        cell.btnOnTheWay.isHidden = true
        cell.btnStartService.isHidden = true
        cell.btnEndServices.isHidden = true
        
        if objBookedService.staffImage != "" {
            if let url = URL(string: objBookedService.staffImage){
                //cell.imgProfie.af_setImage(withURL: url, placeholderImage:#imageLiteral(resourceName: "cellBackground"))
                cell.imgProfie.sd_setImage(with: url, placeholderImage:#imageLiteral(resourceName: "cellBackground"))
            }
        }else{
            cell.imgProfie.image = #imageLiteral(resourceName: "cellBackground")
        }
        cell.lblName.text = objBookedService.artistServiceName
        cell.lblArtistName.text = objBookedService.staffName
        cell.lblPrice.text = objBookedService.bookingPrice
        let doubleStrNN = String(format: "%.2f", ceil(Double(cell.lblPrice.text!)!))
        cell.lblPrice.text = doubleStrNN
        cell.lblPrice.text = "£" + cell.lblPrice.text!
        
        let formatter  = DateFormatter()
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        formatter.dateFormat = "yyyy-MM-dd"
        let date = formatter.date(from: objBookedService.bookingDate)
        formatter.dateFormat = "dd/MM/yyyy"
        let strDate = formatter.string(from: date!)
        cell.lblDuration.text = strDate + ", " + objBookedService.startTime + " - " + objBookedService.endTime
        if self.arrBookedMainService[0].bookStatus == "1" || self.arrBookedMainService[0].bookStatus == "5"{
            cell.viewStatus.isHidden = true
            for obj in self.arrBookedService{
                if obj.status != "0"{
                    cell.viewStatus.isHidden = false
                }
            }
        }else{
            cell.viewStatus.isHidden = true
        }
        
        let strStartTime =  objBookedService.startTime
        let dates = getTimeFromTime(strDate: objBookedService.bookingDate+" "+strStartTime)
        let formatters  = DateFormatter()
        formatters.dateFormat = "yyyy-MM-dd hh:mm a"
        formatters.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        formatters.timeZone = NSTimeZone(abbreviation: "UTC") as TimeZone?
        let dataDate = formatters.date(from: dates)!
        let currentDate = matchesTwoDate(dateA: dataDate, type: "pending")
        
        cell.lblStatusService.textColor = #colorLiteral(red: 0.9200255673, green: 0.4889633489, blue: 0.08423942367, alpha: 1)
        cell.lblStatusService.text = "Pending"
        if objMain.bookStatus == "1" || objMain.bookStatus == "3" || objMain.bookStatus == "5"{
            if objBookedService.status == "0"{
                cell.lblStatusService.textColor = #colorLiteral(red: 0, green: 0.7333333333, blue: 0.1529411765, alpha: 1)
                cell.lblStatusService.text = "Confirmed"
            }else if objBookedService.status == "1"{
                cell.lblStatusService.textColor = #colorLiteral(red: 0, green: 0.7333333333, blue: 0.1529411765, alpha: 1)
                cell.lblStatusService.text = "On the way"
            }else if objBookedService.status == "2"{
                cell.lblStatusService.textColor = #colorLiteral(red: 0, green: 0.851996243, blue: 0.8281483054, alpha: 1)
                cell.lblStatusService.text = "On going"
            }else if objBookedService.status == "3" && objMain.bookStatus == "5"{
                cell.lblStatusService.textColor = #colorLiteral(red: 0, green: 0.7333333333, blue: 0.1529411765, alpha: 1)
                cell.lblStatusService.text = "Service end"
            }else{
                cell.lblStatusService.textColor = #colorLiteral(red: 0, green: 0.7333333333, blue: 0.1529411765, alpha: 1)
                cell.lblStatusService.text = "Service end"
            }
        }
        
        /*
        if (objMain.bookStatus == "1" || objMain.bookStatus == "5" ) && (objMain.paymentType == "1" && objMain.paymentStatus == "1") || (objMain.paymentType == "2" && (objMain.bookStatus == "1" || objMain.bookStatus == "5")){
            
            if objBookedService.status == "0" && objBookedService.staffId == self.myId && objMain.bookingType == "2"{
                cell.btnOnTheWay.isHidden = false
            }
            if objBookedService.status != "2" && objBookedService.status != "3" && currentDate == true{
                cell.btnOnTheWay.isHidden = true
                cell.btnStartService.isHidden = false
            }else if objBookedService.status == "2"{
                cell.btnEndServices.isHidden = false
            }
        }
        */
        //cell.btnOnTheWay.isHidden = true
        cell.btnStartService.isHidden = true
        cell.btnEndServices.isHidden = true
        if objMain.bookingType == "2"{
            if objBookedService.status == "0" ||
                 objBookedService.status == "2" || objBookedService.status == "3"{
                cell.btnOnTheWay.isHidden = true
            }else if objBookedService.status == "1"{
                cell.btnOnTheWay.isHidden = false
            }else{
                cell.btnOnTheWay.isHidden = false
            }
        }
        
        cell.btnOnTheWay.tag = indexPath.row
        cell.btnOnTheWay.addTarget(self, action:#selector(btnOnTheWayTapped(sender:)) , for: .touchUpInside)
        cell.btnStartService.tag = indexPath.row
        cell.btnStartService.addTarget(self, action:#selector(btnStartServiecTapped(sender:)) , for: .touchUpInside)
        cell.btnEndServices.tag = indexPath.row
        cell.btnEndServices.addTarget(self, action:#selector(btnEndServiceTapped(sender:)) , for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
   
    @objc  func btnOnTheWayTapped(sender: UIButton!){
        let objBooking = arrBookedMainService[0]
        let objService = arrBookedService[sender.tag]
        if objBooking.arrUserDetail.count > 0 {
            let objUserInfo = objBooking.arrUserDetail[0]
            let userId = objUserInfo._id
            
            let param = [  "userId":objBooking.userId,
                           "bookingId":objBooking._id,
                           "id":objService._id,
                           "serviceName":objService.artistServiceName,
                           "paymentType":objBooking.paymentType,
                           "price":objService.bookingPrice,
                           "type":"on the way"]
            //callWebserviceForGet_AcceptRejectCount(dict: param, webURL: WebURL.bookingupdate)
        }
    }
    
    
    @objc  func btnStartServiecTapped(sender: UIButton!){
        let objBooking = arrBookedMainService[0]
        let objService = arrBookedService[sender.tag]
        if objBooking.arrUserDetail.count > 0 {
            let objUserInfo = objBooking.arrUserDetail[0]
            let userId = objUserInfo._id
            
            let param = [  "userId":objBooking.userId,//
                "bookingId":objBooking._id,//
                "id":objService._id,
                "serviceName":objService.artistServiceName,
                "paymentType":objBooking.paymentType,
                "price":objService.bookingPrice,
                "type":"start" ]
            //callWebserviceForGet_AcceptRejectCount(dict: param, webURL: WebURL.bookingupdate)
        }
    }
    
    
    @objc  func btnEndServiceTapped(sender: UIButton!){
        let objBooking = arrBookedMainService[0]
        let objService = arrBookedService[sender.tag]
        if objBooking.arrUserDetail.count > 0 {
            let objUserInfo = objBooking.arrUserDetail[0]
            let userId = objUserInfo._id
            
            let param = [  "userId":objBooking.userId,
                           "bookingId":objBooking._id,
                           "id":objService._id,
                           "serviceName":objService.artistServiceName,
                           "paymentType":objBooking.paymentType,
                           "price":objService.bookingPrice,
                           "type":"end" ]
            //callWebserviceForGet_AcceptRejectCount(dict: param, webURL: WebURL.bookingupdate)
        }
    }
}


//MARK: - BookingPendingCellDelegate btn Action
extension BookingDetailVC {
    @IBAction func btnCompleteServiceAction(_ sender: UIButton) {
        
        let objBooking = self.arrBookedMainService[0]
        let bookingId = objBooking._id
        let serviceId = objBooking.allServiceId
        let subServiceId = objBooking.allSubServiceId
        let artistServiceId = objBooking.allArtictServiceId
        
        if objBooking.arrUserDetail.count > 0 {
            
            let objUserInfo = objBooking.arrUserDetail[0]
            let userId = objUserInfo._id
            
            let param = [ "artistId":self.artistId,
                          "userId":objBooking.userId,
                          "bookingId":bookingId,
                          "serviceId":serviceId,
                          "subserviceId":subServiceId,
                          "artistServiceId":artistServiceId,
                          "type":"complete",
                          "paymentType":objBooking.paymentType
            ]
            callWebserviceForGet_AcceptRejectCount(dict: param, webURL: WebURL.bookingAction)
        }
    }
    
    
    @IBAction func btnAcceptAction(_ sender: UIButton) {
        
        let objBooking = self.arrBookedMainService[0]
        let bookingId = objBooking._id
        let serviceId = objBooking.allServiceId
        let subServiceId = objBooking.allSubServiceId
        let artistServiceId = objBooking.allArtictServiceId
        
        if objBooking.arrUserDetail.count > 0 {
            
            let objUserInfo = objBooking.arrUserDetail[0]
            let userId = objUserInfo._id
            
            let param = [ "artistId":self.artistId,
                          "userId":objBooking.userId,
                          "bookingId":bookingId,
                          "serviceId":serviceId,
                          "subserviceId":subServiceId,
                          "artistServiceId":artistServiceId,
                          "type":"accept"
            ]
            callWebserviceForGet_AcceptRejectCount(dict: param, webURL: WebURL.bookingAction)
        }
    }
    
    @IBAction func btnRejectAction(_ sender: UIButton) {
        let obj1 = arrBookedMainService[0]
        
        let  obj2 = obj1.arrUserDetail
        let objUserInfo = obj2[0]
        let userId = objUserInfo._id
        
        let BookingId = obj1._id
        let serviceId = obj1.allServiceId
        let subServiceId = obj1.allSubServiceId
        let artistServiceId = obj1.allArtictServiceId
        
        let param = ["artistId":self.artistId,
                     "userId":obj1.userId,
                     "bookingId":BookingId,
                     "serviceId":serviceId,
                     "subserviceId":subServiceId,
                     "artistServiceId":artistServiceId,
                     "type":"reject"]
        callWebserviceForGet_AcceptRejectCount(dict: param, webURL: WebURL.bookingAction)
    }
    
    func callWebserviceForGet_AcceptRejectCount(dict: [String : Any], webURL:String){
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        objActivity.startActivityIndicator()
        objServiceManager.requestPost(strURL: webURL, params: dict  , success: { response in
            objServiceManager.StopIndicator()
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else{
                let strStatus =  response["status"] as? String ?? ""
                let strMsg = response["message"] as? String ?? kErrorMessage
                if strStatus == k_success{
                    objAppShareData.showAlert(withMessage: strMsg, type: alertType.bannerDark, on: self)
                    self.callWebserviceForGetBookedServices()
                }else{
                    self.tblBookedServices.reloadData()
                    let msg = response["message"] as! String
                    objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                }
            }
        }){ error in
            objServiceManager.StopIndicator()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
}



//MARK: compair date and time
extension BookingDetailVC{
    func matchesTwoDate(dateA:Date, type:String) -> Bool{
        let currentDate = Date()
        let CurrentTimeZone = NSTimeZone(abbreviation: "GMT")
        let SystemTimeZone = NSTimeZone.system as NSTimeZone
        let currentGMTOffset: Int? = CurrentTimeZone?.secondsFromGMT(for: currentDate)
        let SystemGMTOffset: Int = SystemTimeZone.secondsFromGMT(for: currentDate)
        let interval = TimeInterval((SystemGMTOffset - currentGMTOffset!))
        let TodayDate = Date(timeInterval: interval, since: currentDate)
        
        let dateB:Date = TodayDate
        var addIndex = false
        
        switch dateA.compare(dateB) {
            
        case .orderedAscending:
            addIndex = true
            print(type+"Date A is later than date B")
        case .orderedDescending:
            addIndex = false
            print(type+"Date A is earlier than date B")
        case .orderedSame:
            addIndex = true
            print(type+"The two dates are the same")
        }
        return addIndex
    }
    
    func getTimeFromTime(strDate:String)-> String{
        let formatter  = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd hh:mm a"
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        let todayDate = formatter.date(from: strDate)
        let formatedDate: String = formatter.string(from: todayDate!)
        return formatedDate
    }
}




//
//  ArtistServicesVC.swift
//  MualabBusiness
//
//  Created by mac on 29/05/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import DropDown
import CoreLocation

class BookingDetailNewVC : UIViewController,UITableViewDelegate,UITableViewDataSource{
    
    @IBOutlet weak var constraintTableHeight: NSLayoutConstraint!
    @IBOutlet weak var lblHeaderOutCallInCall: UILabel!
    @IBOutlet weak var imgArtist: UIImageView!
    @IBOutlet weak var lblOutCallAddress: UILabel!
    @IBOutlet weak var lblArtistName: UILabel!
    @IBOutlet weak var tblBookedServices: UITableView!
    @IBOutlet weak var lblTotalPrice: UILabel!
    @IBOutlet weak var lblRedTotalPrice: UILabel!
    @IBOutlet weak var lblBookingStatus: UILabel!
    @IBOutlet weak var lblPaymentStatus: UILabel!
    @IBOutlet weak var viewRedTotalPrice: UIView!
    @IBOutlet weak var viewVoucherCode: UIView!
    @IBOutlet weak var txtVoucherCode: UITextField!
    @IBOutlet weak var lblVoucherText: UILabel!
    @IBOutlet weak var viewCancelBooking: UIView!

     var dictVoucher = [:] as! [String:Any]
     var totalPrice = 0.0
     var discountPrice = 0.0
     var objArtistDetails = ArtistDetails(dict: ["":""])
     var objBookingServices = BookingServices()
     var arrBookedService = [BookingInfo]()
     var arrBookedMainService = [Booking]()

     var isOutCallSelectedConfirm = false
     var strMyAddress = ""
     var strBookingId = ""

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewCancelBooking.isHidden = true
        self.viewVoucherCode.isHidden = true
        self.imgArtist.layer.cornerRadius = 28
        self.imgArtist.layer.masksToBounds = true
        self.callWebserviceForGetBookedServices()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
}

//MARK: - button extension
extension BookingDetailNewVC {
   
    @IBAction func btnCallAction(_ sender: UIButton) {
        objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
    }
    @IBAction func btnChatAction(_ sender: UIButton) {
        objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
    }
    @IBAction func btnDeleteOrTrackAction(_ sender: UIButton) {
        self.callWebserviceForRejectBooking()
        //objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
    }
    @IBAction func btnBackAction(_ sender: UIButton) {
    self.navigationController?.popViewController(animated: true)
    }

}

//MARK: - Custom methods Extension
fileprivate extension BookingDetailNewVC{
    

    func manageView(){
       //self.viewRedTotalPrice.isHidden = true
        self.manageOutCallInCallView()
        self.managePaymentMethodView()
    }
    func clearVoucherCode(){
        self.dictVoucher = [:]
        let doubleStrNN = String(format: "%.2f", ceil(self.totalPrice))
        //self.lblTotalPrice.text = "£" + String(self.totalPrice)
        self.lblTotalPrice.text = "£" + doubleStrNN
        self.viewRedTotalPrice.isHidden = true
        self.txtVoucherCode.text = ""
        self.txtVoucherCode.isUserInteractionEnabled = true
    }
    func managePaymentMethodView(){
        
    }
    
    func manageOutCallInCallView(){
       
        if self.isOutCallSelectedConfirm{
            self.lblHeaderOutCallInCall.text = "Out Call"
            self.lblOutCallAddress.text = self.strMyAddress
        }else{
            self.lblHeaderOutCallInCall.text = "In Call"
        }
        if self.arrBookedMainService.count > 0{
            self.lblOutCallAddress.text = self.arrBookedMainService[0].location
            if self.arrBookedMainService[0].bookingType == "1"{
                self.lblHeaderOutCallInCall.text = "In Call"
            }else if self.arrBookedMainService[0].bookingType == "2"{
                self.lblHeaderOutCallInCall.text = "Out Call"
            }else{
                self.lblHeaderOutCallInCall.text = "Both"
            }
            
            let objAllBooking = self.arrBookedMainService[0]
            if objAllBooking.bookStatus == "1"{
                self.lblBookingStatus.text = "Confirmed"
                self.lblBookingStatus.textColor =  #colorLiteral(red: 0, green: 0.7333333333, blue: 0.1529411765, alpha: 1)
            }else if objAllBooking.bookStatus == "2" {
                self.lblBookingStatus.text = "Cancelled"
                self.lblBookingStatus.textColor =  #colorLiteral(red: 0.9607843137, green: 0.01568627451, blue: 0.01568627451, alpha: 1)
            }else if objAllBooking.bookStatus == "3" {
                self.lblBookingStatus.text = "Completed"
                self.lblBookingStatus.textColor =  #colorLiteral(red: 0, green: 0.7333333333, blue: 0.1529411765, alpha: 1)
            }else{
                self.viewCancelBooking.isHidden = false
                self.lblBookingStatus.text = "Pending"
                self.lblBookingStatus.textColor =  #colorLiteral(red: 0, green: 0.7333333333, blue: 0.1529411765, alpha: 1)
            }
            if objAllBooking.paymentType == "1"{
                self.lblPaymentStatus.text = "Online"
            }else{
                self.lblPaymentStatus.text = "Cash"
            }
        }
    }
    
    func calculateTotalPrice(){
        var totalPrice = 0.0
        for obj in self.arrBookedService{
            totalPrice = totalPrice + Double(obj.bookingPrice)!
        }
        self.totalPrice = totalPrice
        let doubleStrNN = String(format: "%.2f", ceil(self.totalPrice))
        //self.lblTotalPrice.text = "£" + String(totalPrice)
        self.lblTotalPrice.text = "£" + doubleStrNN
        //self.lblRedTotalPrice.text = "£" + String(totalPrice)
        self.lblRedTotalPrice.text = "£" + doubleStrNN
    }
}

//MARK: - Webservices Extension
fileprivate extension BookingDetailNewVC{
    
    
    
    func manageVoucherView(dict:[String:Any]){
        self.dictVoucher = dict
        var discountType = 0
        if let discount = dict["discountType"] as? Int{
            discountType = discount
        }else if let discount = dict["discountType"] as? String{
            discountType = Int(discount)!
        }
        var amount = 0.0
        if let amnt = dict["amount"] as? Double{
            amount = amnt
        }else if let amnt = dict["amount"] as? Int{
            amount = Double(amnt)
        }else if let amnt = dict["amount"] as? Float{
            amount = Double(amnt)
        }else if let amnt = dict["amount"] as? String{
            amount = Double(amnt)!
        }
        self.lblVoucherText.text = dict["voucherCode"] as? String ?? ""
//        if discountType == 1{
//            self.lblVoucherText.text = ""
//        }else if discountType == 2 {
//            self.lblVoucherText.text = ""
//        }

        if discountType == 2{
            let newPrice = self.totalPrice - (self.totalPrice*amount/100)
            if newPrice>0{
                self.discountPrice = newPrice
                let doubleStrNN = String(format: "%.2f", ceil(self.discountPrice))
                //self.lblTotalPrice.text = "£" + String(newPrice)
                self.lblTotalPrice.text = "£" + doubleStrNN
            }else{
                self.discountPrice = 0.0
                self.lblTotalPrice.text = "£" + "0"
            }
            self.txtVoucherCode.text = "- " + String(amount) + "%"
        }else{
            let newPrice = self.totalPrice-amount
            if newPrice>0{
                self.discountPrice = newPrice
                let doubleStrNN = String(format: "%.2f", ceil(self.discountPrice))
                //self.lblTotalPrice.text = "£" + String(newPrice)
                self.lblTotalPrice.text = "£" + doubleStrNN
            }else{
                self.discountPrice = 0.0
                self.lblTotalPrice.text = "£" + "0"
            }
            self.txtVoucherCode.text = "- " + "£" + String(amount)
        }
        self.txtVoucherCode.isUserInteractionEnabled = false
        self.viewRedTotalPrice.isHidden = false
    }
    
    func callWebserviceForRejectBooking(){
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        objActivity.startActivityIndicator()
        /*artistId:
         type: (accept,reject,cancel,complete
         bookingId:
         serviceId:
         subserviceId:
         userId:
         artistServiceId:
         http://koobi.co.uk:3000/api/bookingAction
         */
        var strUserId = ""
        if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.userInfo){
            if let userId = dict["_id"] as? Int {
                strUserId = "\(userId)"
            }
        }else{
            let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
            if let id = userInfo["_id"] as? Int {
                strUserId = String(id)
            }
        }
        
        var arrServiceId = [String]()
        var arrSubServiceId = [String]()
        var arrSubSubServiceId = [String]()
        for objService in self.arrBookedService{
            arrServiceId.append(objService.serviceId)
            arrSubServiceId.append(objService.subServiceId)
        arrSubSubServiceId.append(objService.artistServiceId)
        }
        let serviceId = arrServiceId.joined(separator: ",")
        let subServiceId = arrSubServiceId.joined(separator: ",")
        let artistServiceId = arrSubSubServiceId.joined(separator: ",")
        let dicParam = [
            "bookingId":self.strBookingId,
            "type":"reject",
            "userId":strUserId,
            "serviceId":serviceId,
        "subserviceId":subServiceId,
        "artistServiceId":artistServiceId,
            ] as [String : Any]
        print(dicParam)
        
        objServiceManager.requestPostForJson(strURL: WebURL.bookingAction, params: dicParam, success: { response in
            print(response)
            self.arrBookedService.removeAll()
            objActivity.stopActivity()
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else {
                let strSucessStatus = response["status"] as? String ?? ""
                if  strSucessStatus == "success"{
                    objActivity.stopActivity()
                    print(response)
                self.navigationController?.popViewController(animated: true)
                }else{
                    let msg = response["message"] as? String ?? ""
                    objAppShareData.showAlert(withMessage: msg , type: alertType.bannerDark,on: self)
                }}
        }) { error in
            objActivity.stopActivity()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
    func callWebserviceForGetBookedServices(){
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        objActivity.startActivityIndicator()
        
        let dicParam = [
        "bookingId":self.strBookingId
            ] as [String : Any]
        
        objServiceManager.requestPostForJson(strURL: WebURL.bookingDetail, params: dicParam, success: { response in
            print(response)
            self.arrBookedService.removeAll()
            objActivity.stopActivity()
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else {
                let strSucessStatus = response["status"] as? String ?? ""
                if  strSucessStatus == "success"{
                    objActivity.stopActivity()
                    print(response)
                    self.saveData(dicts:response)
                }else{
                    let msg = response["message"] as? String ?? ""
                    objAppShareData.showAlert(withMessage: msg , type: alertType.bannerDark,on: self)
                }}
        }) { error in
            objActivity.stopActivity()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
    func saveData(dicts:[String:Any]){
        
        if let strBookingt = dicts["data"] as? [String : Any]{
                let objBooking = Booking.init(dict: strBookingt)
                self.arrBookedMainService.append(objBooking)
            if arrBookedMainService.count > 0{
                self.arrBookedService = self.arrBookedMainService[0].arrBookingInfo
            }
            if objBooking.arrUserDetail.count > 0{
                let objUserInfo = objBooking.arrUserDetail[0]
            self.lblArtistName.text = objUserInfo.userName
                let url = URL(string: objUserInfo.profileImage ?? "")
                if  url != nil {
                    //self.imgArtist.af_setImage(withURL: url!)
                    self.imgArtist.sd_setImage(with: url!, placeholderImage:#imageLiteral(resourceName: "cellBackground"))
                }else{
                    self.imgArtist.image = #imageLiteral(resourceName: "cellBackground")
                }
            }
            
            self.calculateTotalPrice()
            self.viewRedTotalPrice.isHidden = true
            if let voucherCode = strBookingt["voucher"] as? [String:Any]{
                if voucherCode.count > 0{
                    self.viewVoucherCode.isHidden = false
                    self.manageVoucherView(dict: voucherCode)
                }else{
                    self.viewVoucherCode.isHidden = true
                }
            }
        }
        self.tblBookedServices.reloadData()
        self.manageTableViewHeight()
        self.manageView()
    }
    
    func manageTableViewHeight(){
        self.constraintTableHeight.constant = CGFloat(self.arrBookedService.count*260)
        //self.constraintTableHeight.constant = CGFloat(1*195)
        self.view.layoutIfNeeded()
    }
}

//MARK: - UITableview delegate
extension BookingDetailNewVC {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrBookedService.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let  cell = tableView.dequeueReusableCell(withIdentifier: "SubServiceCell", for: indexPath) as! SubServiceCell
        cell.indexPath = indexPath
        let objBookedService = self.arrBookedService[indexPath.row]
        if objBookedService.staffImage != "" {
            if let url = URL(string: objBookedService.staffImage){
                //cell.imgProfie.af_setImage(withURL: url, placeholderImage:#imageLiteral(resourceName: "cellBackground"))
                cell.imgProfie.sd_setImage(with: url, placeholderImage:#imageLiteral(resourceName: "cellBackground"))
            }
        }else{
            cell.imgProfie.image = #imageLiteral(resourceName: "cellBackground")
        }
        cell.lblName.text = objBookedService.artistServiceName
        cell.lblArtistName.text = objBookedService.staffName
        cell.lblPrice.text = objBookedService.bookingPrice
        let doubleStrNN = String(format: "%.2f", ceil(Double(cell.lblPrice.text!)!))
        cell.lblPrice.text = doubleStrNN
        cell.lblPrice.text = "£" + cell.lblPrice.text!
        
        let formatter  = DateFormatter()
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        formatter.dateFormat = "yyyy-MM-dd"
        let date = formatter.date(from: objBookedService.bookingDate)
        formatter.dateFormat = "dd/MM/yyyy"
        let strDate = formatter.string(from: date!)
        cell.lblDuration.text = strDate + ", " + objBookedService.startTime + " - " + objBookedService.endTime
        //cell.lblDuration.text = objBookedService.bookingDate + ", " + objBookedService.bookingTime
               return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}


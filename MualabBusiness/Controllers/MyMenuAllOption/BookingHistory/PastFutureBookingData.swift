//
//  PastFutureBookingData.swift
//  MualabCustomer
//
//  Created by Mac on 07/06/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import Foundation

class PastFutureBookingData {
    
    var _id : Int = 0
    var artistId : Int = 0
    var bookStatus : Int = 0
    var bookingDate : String = ""
    var bookingTime : String = ""
    var location : String = ""
    var paymentStatus : Int = 0
    var paymentType : Int = 0
    var profileImage : String = ""
    var reviewStatus : Int = 0
    var timeCount : Int = 0
    var totalPrice : String = ""
    var userName : String = ""
    var rating : Double = 0
    var reviewByUser : String = ""
    var reviewByArtist : String = ""
    var bookingType : Int = 0
    var artistRating : Int = 0
    var strBookingType : String = ""
    var indexPath : IndexPath?
    var arr_artistService : [String] = []
    var arrUserDetail = [UserDetail]()
    var isReviewAvailable = false
    var userId = ""
    public var customerType = ""
    public var arrClientInfo : [ClientInfo] =  [ClientInfo]()

    
    
    init(dict : [String : Any]){
        /*
        artistDetail =             (
            {
                "_id" = 5;
                profileImage = "";
                userName = deepu;
            }
        );*/
        userId = String(dict["userId"] as? Int ?? 0)
        if let _id1 = dict["userId"] as? String{
            userId = _id1
        }
        let artistDetail = dict["artistDetail"] as? [[String:Any]] ?? [[:]]
        let dictArtist = artistDetail[0]
        profileImage = dictArtist["profileImage"] as? String ?? ""
        userName = dictArtist["userName"] as? String ?? ""
        _id = dict["_id"] as? Int ?? 0
        artistId = dict["artistId"] as? Int ?? 0
        artistRating = dict["artistRating"] as? Int ?? 0
        //bookStatus = dict["bookStatus"] as? Int ?? 0
        bookingDate = dict["bookingDate"] as? String ?? ""
        bookingTime = dict["bookingTime"] as? String ?? ""
        bookingType = dict["bookingType"] as? Int ?? 0
        strBookingType = String(bookingType)
        location = dict["location"] as? String ?? ""
        paymentStatus = dict["paymentStatus"] as? Int ?? 0
        paymentType = dict["paymentType"] as? Int ?? 0
        //profileImage = dict["profileImage"] as? String ?? ""
        timeCount = dict["timeCount"] as? Int ?? 0
        //userName = dict["userName"] as? String ?? ""
        reviewByUser = dict["reviewByUser"] as? String ?? ""
        reviewByArtist = dict["reviewByArtist"] as? String ?? ""
        
        if let rating = dict["userRating"] as? String {
            self.rating = Double(rating) ?? 0
        }else if let rating = dict["userRating"] as? Int {
            self.rating = Double(rating) 
        }
        
        if let total = dict["totalPrice"] as? String {
            let doubleValue = Double(total) ?? 0
            let doubleStr = String(format: "%.2f", ceil(doubleValue * 100)/100)
            self.totalPrice = doubleStr
        }
        
//        if let arr = dict["bookingInfo"] as? [[String:Any]]{
//            for dict in arr{
//                let strName = dict["artistServiceName"] as? String ?? ""
//                self.arr_artistService.append(strName)
//            }
//        }
        if let arr = dict["bookingInfo"] as? [[String:Any]]{
            for dict in arr{
                let strName = dict["subServiceName"] as? String ?? ""
                self.arr_artistService.append(strName)
            }
        }
        
        if let arr = dict["userDetail"] as? [[String:Any]]{
            for dict in arr{
                let obj = UserDetail.init(dict: dict)
                self.arrUserDetail.append(obj)
            }
        }
        
        if let arr = dict["client"] as? [[String : Any]]{
            for dict in arr{
                let obj = ClientInfo.init(dict: dict)
                self.arrClientInfo.append(obj)
            }
        }
        customerType = dict["customerType"] as? String ?? ""

        if let str = dict["bookStatus"] as? String {
            bookStatus = Int(str) ?? 0
        }
        if let bookStatusM = dict["bookStatus"] as? Int {
            bookStatus = bookStatusM
        }
        reviewStatus = dict["reviewStatus"] as? Int ?? 0
        
        if reviewByUser == "" {
            isReviewAvailable = false
        }else{
            isReviewAvailable = true
        }
    }
}

/*
{
    "_id" = 29;
    artistId = 2;
    artistName = pankaj;
    artistProfileImage = "http://koobi.co.uk:3000/uploads/profile/1527933785077.jpg";
    artistRating = 0;
    artistService =             (
        "Black hair color"
    );
    bookStatus = 3;
    bookingDate = "2018-06-22";
    bookingTime = "04:20 PM";
    location = "MINDIII Systems Pvt. Ltd., Main Road, Brajeshwari Extension, Pipliyahana, Indore, Madhya Pradesh, India";
    paymentStatus = 0;
    paymentType = 2;
    reviewByArtist = "";
    reviewByUser = "";
    reviewStatus = 0;
    timeCount = 980;
    totalPrice = "50.75";
    transjectionId = "";
    userId = 29;
    userName = rajesh;
    userProfileImage = "http://koobi.co.uk:3000/uploads/profile/1528203800948.jpg";
    userRating = 0;
}
*/

//
//  PaymentHistoryVC.swift
//  MualabBusiness
//
//  Created by Mindiii on 3/4/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import DropDown

class PaymentHistoryVC: UIViewController, UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate {
        
        @IBOutlet weak var viewSearch: UIView!
        @IBOutlet weak var txtSearchFilter: UITextField!
        @IBOutlet weak var btnHiddenSearch: UIButton!
        var strSearchText = ""
        @IBOutlet weak var lblHeader: UILabel!
        @IBOutlet weak var viewPastBooking: UIView!
        @IBOutlet weak var tblPastBooking: UITableView!
    
        @IBOutlet weak var btnBookingHistory: UIButton!
        @IBOutlet weak var btnAppointments: UIButton!
        @IBOutlet weak var btnCancelBooking: UIButton!

        @IBOutlet weak var imgBookingHistory: UIImageView!
        @IBOutlet weak var imgAppointments: UIImageView!
        @IBOutlet weak var imgCancelBooking: UIImageView!

        @IBOutlet weak var activity: UIActivityIndicatorView!
        @IBOutlet weak var lblNoRecord: UILabel!
        var isBookingHistory = 1
        var selectedIndexPath : IndexPath?
    
        var arrPastBookingData : [PastFutureBookingData] = []
        var arrPastBookingDataAll : [PastFutureBookingData] = []
        var arrFutureBookingData : [PastFutureBookingData] = []
        var arrFutureBookingDataAll : [PastFutureBookingData] = []
        var arrCancelBookingData : [PastFutureBookingData] = []
        var arrCancelBookingDataAll : [PastFutureBookingData] = []
    
        var dropDown = DropDown()
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var viewBottumTable: UIView!
    
        @IBOutlet weak var btnMainType: UIButton!
        @IBOutlet weak var btnAllType: UIButton!
        @IBOutlet weak var btnInCallType: UIButton!
        @IBOutlet weak var btnOutCallType: UIButton!
        @IBOutlet weak var viewAllType: UIView!
        @IBOutlet weak var viewInCallType: UIView!
        @IBOutlet weak var viewOutCallType: UIView!
        
        fileprivate var strUserId : String = ""
    
        fileprivate var pageNo: Int = 0
        fileprivate var totalCount = 0
        fileprivate var lastLoadedPage = 0
    
        fileprivate var strBookingType = ""
    
        fileprivate var pageNo2: Int = 0
        fileprivate var totalCount2 = 0
        fileprivate var lastLoadedPage2 = 0
    
        fileprivate var pageNo3: Int = 0
        fileprivate var totalCount3 = 0
        fileprivate var lastLoadedPage3 = 0
    
        fileprivate var fromLastTab = false
        
        fileprivate let pageSize = 20 // that's up to you, really
        fileprivate let preloadMargin = 5 // or whatever number that makes sense with your page size
        fileprivate let reviewTextLimit = 150
        
        //MARK: - refreshControl
        lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
            refreshControl.addTarget(self, action:
                #selector(self.handleRefresh(_:)),
                                     for: UIControl.Event.valueChanged)
            refreshControl.tintColor = appColor
            return refreshControl
        }()
        
        lazy var refreshControl2: UIRefreshControl = {
            let refreshControl = UIRefreshControl()
            refreshControl.addTarget(self, action:
                #selector(self.handleRefresh(_:)),
                                     for: UIControl.Event.valueChanged)
            refreshControl.tintColor = appColor
            return refreshControl
        }()
        
        
        override func viewDidLoad() {
            super.viewDidLoad()
            configureView()
            
            self.tblView.delegate = self
            self.tblView.dataSource = self
            self.viewBottumTable.isHidden = true
            
            self.strSearchText = ""
            self.txtSearchFilter.text = ""
            self.btnHiddenSearch.isHidden = true
            self.setuoDropDownAppearance()
            self.getBookingDataWith(0)
            self.addGesturesToView()
    }
    func addGesturesToView() -> Void {
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.topToBottomSWIPE))
        swipeRight.direction = .down
        self.viewBottumTable.addGestureRecognizer(swipeRight)
    }
    
    @objc func topToBottomSWIPE() ->Void {
        self.view.endEditing(true)
        self.viewBottumTable.isHidden = true
    }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
        }
        
        override var preferredStatusBarStyle: UIStatusBarStyle {
            return .default
        }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.tabBarController?.view.frame =  CGRect(x:0,y:0,width:Int(self.view.frame.width),height:viewHeightGloble)
    }
    
        override func viewWillAppear(_ animated: Bool) {
            self.activity.stopAnimating()
            self.view.endEditing(true)
            selectedIndexPath = nil
            self.setuoDropDownAppearance()
            if objAppShareData.isFromNotification {
                
                var notifyId : Int?
                var strUserType: String?
                var strUserName: String?
                var strProfileImage: String?
                
                if let userInfo = objAppShareData.notificationInfoDict {
                    
                    if let notiId  = userInfo["notifyId"] as? Int{
                        notifyId = notiId
                    }else{
                        if let notiId  = userInfo["notifyId"] as? String{
                            notifyId = Int(notiId)
                        }
                    }
                    
                    if let userType = userInfo["userType"] as? String{
                        strUserType = userType
                    }
                    
                    if let userName = userInfo["userName"] as? String{
                        strUserName = userName
                    }
                    
                    if let img = userInfo["urlImageString"] as? String{
                        strProfileImage  = img
                    }
                    
                }
                
                if let notifyId = notifyId {
                    
                    var objPastFutureBookingData  : PastFutureBookingData = PastFutureBookingData(dict: ["":""])
                    
                    objPastFutureBookingData._id = notifyId
                    objPastFutureBookingData.userName = strUserName ?? ""
                    objPastFutureBookingData.profileImage = strProfileImage ?? ""
                    
                    self.gotoAppoitmentBookingVC(obj: objPastFutureBookingData)
                    return
                    
                }
                
            }
            self.getBookingDataWith(0)
        }
        
        @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
            selectedIndexPath = nil
            self.getBookingDataWith(0)
            //API Call
        }
    }
    
    //MARK: - custome method extension
    extension PaymentHistoryVC {
        
        func configureView(){
            
            self.strBookingType = "All Type"
            self.viewAllType.isHidden = true
            self.viewInCallType.isHidden = true
            self.viewOutCallType.isHidden = true
            self.viewSearch.isHidden = true
            self.txtSearchFilter.delegate = self
            self.viewSearch.isHidden = true
            
            if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]{
                if let userId = dict["_id"] as? String {
                    self.strUserId =  userId
                }
            }else{
                let userInfo = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] ?? ["":""]
                if let id = userInfo["_id"] as? String {
                    strUserId = id
                }
            }
            
            self.tblPastBooking.delegate = self
            self.tblPastBooking.dataSource = self
            
            self.tblPastBooking.rowHeight = UITableView.automaticDimension;
            self.tblPastBooking.estimatedRowHeight = 77.0;
            
            self.tblPastBooking.addSubview(self.refreshControl)
            
            self.lblNoRecord.isHidden = true
            
            //segmentController.selectedSegmentIndex = 0
            self.lblHeader.text = "Payment History"
            self.viewPastBooking.isHidden = false
        }
    }
    
    // MARK: - UITextfield Delegate
    extension PaymentHistoryVC{
        func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
            
            let text = textField.text! as NSString
            
            if (text.length == 1)  && (string == "") {
                NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
                self.strSearchText = ""
                self.btnHiddenSearch.isHidden = false
                self.activity.startAnimating()
                getArtistDataWith(andSearchText: "")
            }else{
                var substring: String = textField.text!
                substring = (substring as NSString).replacingCharacters(in: range, with: string)
                substring = substring.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                let a = substring.count
                if a > 20 && substring != ""{
                    return false
                }else{
                    self.activity.startAnimating()
                    searchAutocompleteEntries(withSubstring: substring)
                }
            }
            return true
        }
        
        func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            textField.resignFirstResponder()
            return true
        }
        
        func textFieldShouldClear(_ textField: UITextField) -> Bool {
            self.view.endEditing(true)
            
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
            self.strSearchText = ""
            getArtistDataWith( andSearchText: "")
            return true
        }
        
        // MARK: - searching operation
        func searchAutocompleteEntries(withSubstring substring: String) {
            if substring != "" {
                self.strSearchText = substring
                // to limit network activity, reload half a second after last key press.
                NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
                self.perform(#selector(self.reload), with: nil, afterDelay: 0.5)
            }
        }
        
        @objc func reload() {
            self.getArtistDataWith( andSearchText: strSearchText)
        }
        
        func getArtistDataWith(andSearchText: String) {
            getBookingDataWith(0)
        }
    }

    //MARK: - btn Actions
    extension PaymentHistoryVC {
        @IBAction func btnSearchOptionFilter(_ sender: UIButton) {
            self.view.endEditing(true)
            self.viewSearch.isHidden = !self.viewSearch.isHidden
            if self.strSearchText != "" || self.txtSearchFilter.text != ""{
                self.txtSearchFilter.text = ""
                self.strSearchText = ""
                getBookingDataWith(0)
            }
            self.btnHiddenSearch.isHidden = self.viewSearch.isHidden
        }
        
        @IBAction func btnHiddenSearchFilter(_ sender: UIButton) {
            if self.txtSearchFilter.text == ""  || self.strSearchText == ""{
                self.viewSearch.isHidden = true
            }
            self.btnHiddenSearch.isHidden = true
        }
        
        @IBAction func btnDropDownFilter(_ sender: UIButton) {
            self.view.endEditing(true)
            let arrBookingType = ["All Type","In Call","Out Call"]
            
            let objGetData = arrBookingType.map { $0 }
            guard objGetData.isEmpty == false else {
                return
            }
            self.viewBottumTable.isHidden = false
            self.tblView.reloadData()
        }
        
        @IBAction func btnBookingTypeAction(_ sender: UIButton){
            self.chengDropDownAction(tag: sender.tag)
        }
        
        func setuoDropDownAppearance()  {
            let appearance = DropDown.appearance()
            appearance.cellHeight = 40
            appearance.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            appearance.selectionBackgroundColor = UIColor(red: 0.6494, green: 0.8155, blue: 1.0, alpha: 0.2)
            appearance.separatorColor = UIColor.black// #colorLiteral(red: 0.8392156863, green: 0.8392156863, blue: 0.8392156863, alpha: 1)
            appearance.cornerRadius = 5
            appearance.shadowColor = UIColor.black //UIColor(white: 0.6, alpha: 1)
            appearance.shadowOpacity = 1
            appearance.shadowRadius = 1
            appearance.animationduration = 0.25
            appearance.textColor = .black
            appearance.textFont = UIFont (name: "Nunito-Regular", size: 14) ?? .systemFont(ofSize: 14)
        }
        func chengDropDownAction(tag:Int){
            if tag == 0{
                if self.strBookingType == "All Type"{
                    self.viewAllType.isHidden = true
                    self.viewInCallType.isHidden = false
                    self.viewOutCallType.isHidden = false
                }else if self.strBookingType == "In Call"{
                    self.viewAllType.isHidden = false
                    self.viewInCallType.isHidden = true
                    self.viewOutCallType.isHidden = false
                }else if self.strBookingType == "Out Call"{
                    self.viewAllType.isHidden = false
                    self.viewInCallType.isHidden = false
                    self.viewOutCallType.isHidden = true
                }
            }else {
                if tag == 1{
                    self.strBookingType = "All Type"
                    self.arrFutureBookingData = self.arrFutureBookingDataAll
                    self.arrPastBookingData = self.arrPastBookingDataAll
                    self.arrCancelBookingData = self.arrCancelBookingDataAll
                }else if tag == 2{
                    self.strBookingType = "In Call"
                    let filteredArray = self.arrFutureBookingDataAll.filter(){ $0.strBookingType.contains("1") }
                    self.arrFutureBookingData = filteredArray
                    let filteredArrayPast = self.arrPastBookingDataAll.filter(){ $0.strBookingType.contains("1") }
                    self.arrPastBookingData = filteredArrayPast
                    let filteredArrayPast2 = self.arrCancelBookingDataAll.filter(){ $0.strBookingType.contains("1") }
                    self.arrCancelBookingData = filteredArrayPast2
                }else if tag == 3{
                    self.strBookingType = "Out Call"
                    let filteredArray = self.arrFutureBookingDataAll.filter(){ $0.strBookingType.contains("2") }
                    self.arrFutureBookingData = filteredArray
                    let filteredArrayPast = self.arrPastBookingDataAll.filter(){ $0.strBookingType.contains("2") }
                    self.arrPastBookingData = filteredArrayPast
                    let filteredArrayPast2 = self.arrCancelBookingDataAll.filter(){ $0.strBookingType.contains("2") }
                    self.arrCancelBookingData = filteredArrayPast2
                }
                if self.isBookingHistory == 1{
                    self.tblPastBooking.reloadData()
                    if self.arrPastBookingData.count == 0 {
                        self.lblNoRecord.isHidden = false
                    }else{
                        self.lblNoRecord.isHidden = true
                        self.tblPastBooking.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)

                    }
                }else  if self.isBookingHistory == 2{
                    self.tblPastBooking.reloadData()
                    if self.arrFutureBookingData.count == 0 {
                        self.lblNoRecord.isHidden = false
                    }else{
                        self.lblNoRecord.isHidden = true
                        self.tblPastBooking.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
                    }
                }else  if self.isBookingHistory == 3{
                    self.tblPastBooking.reloadData()
                    if self.arrCancelBookingData.count == 0 {
                        self.lblNoRecord.isHidden = false
                    }else{
                        self.lblNoRecord.isHidden = true
                        self.tblPastBooking.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
                    }
                }
                self.viewAllType.isHidden = true
                self.viewInCallType.isHidden = true
                self.viewOutCallType.isHidden = true
            }
            self.btnMainType.setTitle(self.strBookingType, for: .normal)
        }
        
        @IBAction func btnBackAction(_ sender: Any){
            objAppShareData.clearNotificationData()
            navigationController?.popViewController(animated: true)
        }
        
        @IBAction func btnProfileAction(_ sender: Any) {
            self.view.endEditing(true)
            objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
        }
        
        @IBAction func segmentButtonPastFuture(_ sender: UIButton) {
            self.activity.stopAnimating()
            fromLastTab = true
            self.viewAllType.isHidden = true
            self.viewInCallType.isHidden = true
            self.viewOutCallType.isHidden = true
            self.btnMainType.setTitle(self.strBookingType, for: .normal)
            
            if sender.tag == 0 {
                if isBookingHistory == 1{
                    return
                }
                isBookingHistory = 1

                self.strBookingType = "All Type"
                self.chengDropDownAction(tag: 1)
                
                self.arrPastBookingDataAll.removeAll()
                self.arrFutureBookingDataAll.removeAll()
                self.arrCancelBookingDataAll.removeAll()
                self.tblPastBooking.reloadData()
                objWebserviceManager.StartIndicator()
                
                self.imgAppointments.backgroundColor = UIColor.lightGray
                self.imgBookingHistory.backgroundColor = appColor
                self.btnBookingHistory.setTitleColor(appColor, for: .normal)
                self.btnAppointments.setTitleColor(UIColor.lightGray, for: .normal)
                self.btnCancelBooking.setTitleColor(UIColor.lightGray, for: .normal)
                self.imgCancelBooking.backgroundColor = UIColor.lightGray
                viewPastBooking.isHidden = false
                self.lblNoRecord.isHidden = true
                if arrPastBookingData.count == 0{
                    self.strBookingType = "All Type"
                    self.getBookingDataWith(0)
                    self.strBookingType = "All Type"
                }
                
            }else if sender.tag == 1{
                if isBookingHistory == 2{
                    return
                }
                isBookingHistory = 2
                
                self.chengDropDownAction(tag: 1)
                self.arrPastBookingDataAll.removeAll()
                self.arrFutureBookingDataAll.removeAll()
                self.arrCancelBookingDataAll.removeAll()

                self.tblPastBooking.reloadData()
                
                self.strBookingType = "All Type"
                
                objWebserviceManager.StartIndicator()
                
                self.imgBookingHistory.backgroundColor = UIColor.lightGray
                self.imgAppointments.backgroundColor = appColor
                self.btnBookingHistory.setTitleColor(UIColor.lightGray, for: .normal)
                self.btnAppointments.setTitleColor(appColor, for: .normal)
                self.btnCancelBooking.setTitleColor(UIColor.lightGray, for: .normal)
                self.imgCancelBooking.backgroundColor = UIColor.lightGray
                viewPastBooking.isHidden = false
                self.lblNoRecord.isHidden = true
                if arrFutureBookingData.count == 0{
                    self.strBookingType = "All Type"
                    self.getBookingDataWith(0)
                    self.strBookingType = "All Type"
                }
            }else if sender.tag == 2{
                if isBookingHistory == 3 {
                    return
                }
                isBookingHistory = 3
                
                self.chengDropDownAction(tag: 1)
                self.arrPastBookingDataAll.removeAll()
                self.arrFutureBookingDataAll.removeAll()
                self.arrCancelBookingDataAll.removeAll()
                self.tblPastBooking.reloadData()
                
                self.strBookingType = "All Type"
                
                objWebserviceManager.StartIndicator()
                
                self.imgBookingHistory.backgroundColor = UIColor.lightGray
                self.imgAppointments.backgroundColor = UIColor.lightGray
                self.btnBookingHistory.setTitleColor(UIColor.lightGray, for: .normal)
                self.btnAppointments.setTitleColor(UIColor.lightGray, for: .normal)
                self.btnCancelBooking.setTitleColor(appColor, for: .normal)
                self.imgCancelBooking.backgroundColor = appColor
                
                viewPastBooking.isHidden = false
                self.lblNoRecord.isHidden = true
                if arrCancelBookingData.count == 0{
                    self.strBookingType = "All Type"
                    self.getBookingDataWith(0)
                    self.strBookingType = "All Type"
                }
            }
        }
    }
    
    // MARK: - table view Delegate and Datasource
    extension PaymentHistoryVC  {
        
        // MARK: - Tableview delegate methods
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            if tableView == tblView{
                return 3
            }else{
            if isBookingHistory == 1{
                return self.arrPastBookingData.count
            }else if isBookingHistory == 2{
                return self.arrFutureBookingData.count
            }else if isBookingHistory == 3{
                return self.arrCancelBookingData.count
            }else{
                return 0
                }
                
            }
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
        {
            if tableView == tblPastBooking {
                if isBookingHistory == 1{
                    if self.arrPastBookingData.count < self.totalCount {
                        let nextPage: Int = Int(indexPath.item / pageSize) + 1
                        let preloadIndex = nextPage * pageSize - preloadMargin
                        if (indexPath.item >= preloadIndex && lastLoadedPage < nextPage) {
                            self.getBookingDataWith(nextPage)
                        }
                    }
                }else if  isBookingHistory == 2{
                    if self.arrFutureBookingData.count < self.totalCount2 {
                        let nextPage: Int = Int(indexPath.item / pageSize) + 1
                        let preloadIndex = nextPage * pageSize - preloadMargin
                        if (indexPath.item >= preloadIndex && lastLoadedPage2 < nextPage) {
                            self.getBookingDataWith(nextPage)
                        }
                    }
                }else if  isBookingHistory == 3{
                    if self.arrCancelBookingData.count < self.totalCount3 {
                        let nextPage: Int = Int(indexPath.item / pageSize) + 1
                        let preloadIndex = nextPage * pageSize - preloadMargin
                        if (indexPath.item >= preloadIndex && lastLoadedPage3 < nextPage) {
                            self.getBookingDataWith(nextPage)
                        }
                    }
                }
              
            
         
            var  obj   = PastFutureBookingData(dict: ["":""])
            
                var objUser = UserDetail(dict: [:])
            
                if  isBookingHistory == 1{
                    obj   =  self.arrPastBookingData[indexPath.row]
                }else if isBookingHistory == 2 {
                    obj   =  self.arrFutureBookingData[indexPath.row]
                }else if isBookingHistory == 3 {
                    obj   =  self.arrCancelBookingData[indexPath.row]
                }
            
                if obj.arrUserDetail.count > 0{
                    objUser = obj.arrUserDetail[0]
                }

             let cellIdentifier = "CellSmallPastFutureReview"
                    if let cell : CellSmallPastFutureReview = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)! as? CellSmallPastFutureReview {
                        
                        cell.indexPath = indexPath
                        obj.indexPath = indexPath
                        cell.setDataInCollection(obj: obj)
                        cell.lblDate.text = self.getFormattedBookingDateToShowFrom(strDate: obj.bookingDate)//obj.bookingDate
                        cell.lblDate.text = cell.lblDate.text! + ", " + obj.bookingTime
                        cell.lblAmount.text = "£" + obj.totalPrice
                        if obj.customerType == "walking"{
                            if obj.arrClientInfo.count > 0 {
                                cell.lblName.text = obj.arrClientInfo[0].firstName+" "+obj.arrClientInfo[0].lastName
                            }
                            cell.imgVwProfile.image = #imageLiteral(resourceName: "cellBackground")
                        }else{
                            cell.imgVwProfile.image = #imageLiteral(resourceName: "cellBackground")
                            cell.lblName.text = objUser.userName
                            let strImg = objUser.profileImage
                            if strImg != "" {
                                if let url = URL(string: strImg){
                                    //cell.imgVwProfile.af_setImage(withURL: url, placeholderImage: UIImage(named: "cellBackground"))
                                    cell.imgVwProfile.sd_setImage(with: url, placeholderImage:#imageLiteral(resourceName: "cellBackground"))
                                }
                            }
                        }
                        
                        cell.btnProfile.tag = indexPath.row
                        cell.btnProfile.addTarget(self, action:#selector(btnProfileRedirectionAction1(sender:)) , for: .touchUpInside)
                        return cell
                    }else{
                        return UITableViewCell()
                    }
                    }else if tableView == tblView{
                        let cellIdentifier = "CellBottumTableList"
                        if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? CellBottumTableList{
                            let arrBookingType = ["All Type","In Call","Out Call"]
                            let obj = arrBookingType[indexPath.row]
                            cell.lblTitle.text = obj
                            return cell
                        }else{
                            return UITableViewCell()
                        }
                }else{
                        return UITableViewCell()
                    }
            
        }
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
            //return
            if tableView == tblPastBooking{
        var objPastFutureBookingData  : PastFutureBookingData = PastFutureBookingData(dict: ["":""])
        if isBookingHistory == 1 {
                objPastFutureBookingData = arrPastBookingData[indexPath.row]
                self.gotoAppoitmentBookingVC(obj: objPastFutureBookingData)
            }else   if isBookingHistory == 2{
                objPastFutureBookingData = arrFutureBookingData[indexPath.row]
                self.gotoAppoitmentBookingVC(obj: objPastFutureBookingData)
            }else   if isBookingHistory == 3{
                objPastFutureBookingData = arrCancelBookingData[indexPath.row]
                self.gotoAppoitmentBookingVC(obj: objPastFutureBookingData)
            }
                
            }else if tableView == tblView{
                    self.viewBottumTable.isHidden = true
                    let arrBookingType = ["All Type","In Call","Out Call"]
                    let item = arrBookingType[indexPath.row]
                    if  item == "All Type"{
                        self.chengDropDownAction(tag: 1)
                    }else if item == "In Call"{
                        self.chengDropDownAction(tag: 2)
                    }else if item == "Out Call"{
                        self.chengDropDownAction(tag: 3)
                    }else{
                        self.chengDropDownAction(tag: 1)
                    }
                }
            }
        
        
        
        @available(iOS 11.0, *)
        func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
        {
            
            
            
            let Delete = UIContextualAction(style: .normal, title:  "", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
                var objSer = PastFutureBookingData(dict: ["":""])
                if self.isBookingHistory  == 1 {
                    objSer = self.arrPastBookingData[indexPath.row]
                }else if self.isBookingHistory == 2{
                    objSer = self.arrFutureBookingData[indexPath.row]
                }else if self.isBookingHistory == 3{
                    objSer = self.arrCancelBookingData[indexPath.row]
                }
                let businessId = objSer._id
                self.callWebserviceDeleteServices(Ids: String(businessId))
                success(true)
            })
            Delete.image = #imageLiteral(resourceName: "delete")
            Delete.backgroundColor =  #colorLiteral(red: 0.9137254902, green: 0.2117647059, blue: 0.2274509804, alpha: 1)
            if self.isBookingHistory == 1{
            return UISwipeActionsConfiguration(actions: [Delete])
            }else if self.isBookingHistory == 2{
                return UISwipeActionsConfiguration(actions: [])
            }else{
                return UISwipeActionsConfiguration(actions: [])
            }
        }
        
        func callWebserviceDeleteServices(Ids:String){
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            objActivity.startActivityIndicator()
            let dict = ["bookingId":Ids]
            objServiceManager.requestPost(strURL: WebURL.paymentHistoryDelete, params: dict as [String : AnyObject], success: { response in
                if  response["status"] as? String == "success"{
                    self.getBookingDataWith(0)
                }else{
                    self.getBookingDataWith(0)
                }
            }) { error in
                self.getBookingDataWith(0)
                objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
            }
        }
    }

    
    //MARK: - custome method extension
    extension PaymentHistoryVC {
        func getBookingDataWith(_ page: Int) {
            var parameters = [String:Any]()
            var strBookingType : String = ""
            if self.isBookingHistory == 1{
                self.lastLoadedPage = page
                self.pageNo = page
                strBookingType = "Past"
            }else if self.isBookingHistory == 2{
                self.lastLoadedPage2 = page
                self.pageNo2 = page
                strBookingType = ""
            }else if self.isBookingHistory == 3{
                self.lastLoadedPage3 = page
                self.pageNo3 = page
                strBookingType = ""
            }
            var payType = ""
            if isBookingHistory == 1{
                payType = "paid"
            }else if isBookingHistory == 2{
                payType = "unPaid"
            }else  if isBookingHistory == 3{
                payType = "unPaid"
            }
            parameters = [
                "search":self.strSearchText,
                "paymentType":payType
            ]
            self.webServiceCall_getBookingData(parameters: parameters)
        }
        
        
        func webServiceCall_getBookingData(parameters : [String:Any]){
            
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            
            //if self.segmentController.selectedSegmentIndex == 0 {
            if self.isBookingHistory == 1 {
                if  !self.refreshControl.isRefreshing && self.pageNo == 0 {
                    if isBookingHistory == 1{
                    self.activity.startAnimating()
                    }
                }
                if fromLastTab == true{
                    self.activity.stopAnimating()
                    fromLastTab = false
                }
            }else if self.isBookingHistory == 2{
                
                if  !self.refreshControl2.isRefreshing && self.pageNo2 == 0 {
                  //  self.activity.startAnimating()
                }
                if fromLastTab == true{
                    self.activity.stopAnimating()
                    fromLastTab = false
                }
            }else if self.isBookingHistory == 3{
                if  !self.refreshControl2.isRefreshing && self.pageNo3 == 0 {
                  //  self.activity.startAnimating()
                }
                if fromLastTab == true{
                    self.activity.stopAnimating()
                    fromLastTab = false
                }
            }
            
            self.lblNoRecord.isHidden = true
            objWebserviceManager.requestPost(strURL:  WebURL.userBookingHistory, params: parameters as [String : AnyObject] , success: { response in
                print(response)
                //  objWebserviceManager.StopIndicator()
                self.activity.stopAnimating()
                self.refreshControl.endRefreshing()
                self.refreshControl2.endRefreshing()
                
                self.arrPastBookingDataAll.removeAll()
                self.arrFutureBookingDataAll.removeAll()
                self.arrCancelBookingDataAll.removeAll()
                
                if self.isBookingHistory == 1{
                    
                    if self.pageNo == 0 {
                        self.arrPastBookingData.removeAll()
                        self.tblPastBooking.reloadData()
                    }
                }else if self.isBookingHistory == 2{
                    if self.pageNo2 == 0 {
                        self.arrFutureBookingData.removeAll()
                        self.tblPastBooking.reloadData()
                    }
                }else if self.isBookingHistory == 3{
                    if self.pageNo3 == 0 {
                        self.arrCancelBookingData.removeAll()
                        self.tblPastBooking.reloadData()
                    }
                }
                
                let keyExists = response["responseCode"] != nil
                if  keyExists {
                    sessionExpireAlertVC(controller: self)
                }else{
                    let strStatus =  response["status"] as? String ?? ""
                    
                    if strStatus == k_success{
                        if self.isBookingHistory == 1{
                            if let totalCount = response["total"] as? Int{
                                self.totalCount = totalCount
                            }
                            //if let arr = response["Booking"] as? [[String : Any]]{
                            if let arr = response["data"] as? [[String : Any]]{
                                for dict in arr{
                                    let obj = PastFutureBookingData.init(dict: dict)
                                    self.arrPastBookingDataAll.append(obj)
                                }
                                //self.arrPastBookingDataAll.reverse()
                            }
                        }else  if self.isBookingHistory == 2{
                            if let totalCount = response["total"] as? Int{
                                self.totalCount2 = totalCount
                            }
                            //if let arr = response["Booking"] as? [[String : Any]]{
                            if let arr = response["data"] as? [[String : Any]]{
                                for dict in arr{
                                    let obj = PastFutureBookingData.init(dict: dict)
                                    self.arrFutureBookingDataAll.append(obj)
                                }
                               // self.arrFutureBookingDataAll.reverse()
                            }
                        }else  if self.isBookingHistory == 3{
                            if let totalCount = response["total"] as? Int{
                                self.totalCount3 = totalCount
                            }
                            //if let arr = response["Booking"] as? [[String : Any]]{
                            if let arr = response["data"] as? [[String : Any]]{
                                for dict in arr{
                                    let obj = PastFutureBookingData.init(dict: dict)
                                    self.arrCancelBookingDataAll.append(obj)
                                }
                               // self.arrCancelBookingDataAll.reverse()
                            }
                        }
                        
                        
                        
                        if self.strBookingType == "All Type"{
                            
                            
                            let filteredArray1 = self.arrFutureBookingDataAll.filter(){ $0.bookStatus != 2 }
                            self.arrFutureBookingDataAll = filteredArray1
                            self.arrFutureBookingData = self.arrFutureBookingDataAll
                            
                            let filteredArray2 = self.arrPastBookingDataAll.filter(){ $0.bookStatus != 2 }
                            self.arrPastBookingDataAll = filteredArray2
                            self.arrPastBookingData = self.arrPastBookingDataAll
                            
                            let filteredArray3 = self.arrCancelBookingDataAll.filter(){ $0.bookStatus == 2 }
                            self.arrCancelBookingDataAll = filteredArray3
                            self.arrCancelBookingData = self.arrCancelBookingDataAll
                            
                        }else if self.strBookingType == "In Call"{
                            
                            let filteredArray = self.arrFutureBookingDataAll.filter(){ $0.strBookingType.contains("1")   && $0.bookStatus != 2}
                            self.arrFutureBookingData = filteredArray
                            
                            let filteredArrayPast = self.arrPastBookingDataAll.filter(){ $0.strBookingType.contains("1")   && $0.bookStatus != 2}
                            self.arrPastBookingData = filteredArrayPast
                            
                            let filteredArrayCancel = self.arrCancelBookingDataAll.filter(){ $0.strBookingType.contains("1")  && $0.bookStatus == 2 }
                            self.arrCancelBookingData = filteredArrayCancel
                            
                        }else if self.strBookingType == "Out Call"{
                            let filteredArray = self.arrFutureBookingDataAll.filter(){ $0.strBookingType.contains("2")    && $0.bookStatus != 2}
                            self.arrFutureBookingData = filteredArray
                            let filteredArrayPast = self.arrPastBookingDataAll.filter(){ $0.strBookingType.contains("2")    && $0.bookStatus != 2}
                            self.arrPastBookingData = filteredArrayPast
                            let filteredArrayCancel = self.arrCancelBookingDataAll.filter(){ $0.strBookingType.contains("2")  && $0.bookStatus == 2 }
                            self.arrCancelBookingData = filteredArrayCancel
                        }
                        
                        
                        
                        self.tblPastBooking.reloadData()
                    
                    }else{
                        if strStatus == "fail"{
                        }else{
                            if let msg = response["message"] as? String{
                                objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                            }
                        }
                    }
                }
                
                if self.isBookingHistory == 1{
                    self.tblPastBooking.reloadData()
                    if self.arrPastBookingData.count == 0 {
                        self.lblNoRecord.isHidden = false
                    }else{
                        self.lblNoRecord.isHidden = true
                    }
                }else if self.isBookingHistory == 2{
                    self.tblPastBooking.reloadData()
                    if self.arrFutureBookingData.count == 0 {
                        self.lblNoRecord.isHidden = false
                    }else{
                        self.lblNoRecord.isHidden = true
                    }
                }else if self.isBookingHistory == 3{
                    self.tblPastBooking.reloadData()
                    if self.arrCancelBookingData.count == 0 {
                        self.lblNoRecord.isHidden = false
                    }else{
                        self.lblNoRecord.isHidden = true
                    }
                }
                
            }) { (error) in
                self.activity.stopAnimating()
                self.refreshControl.endRefreshing()
                self.refreshControl2.endRefreshing()
                
                if self.isBookingHistory == 1 {
                    self.tblPastBooking.reloadData()
                    if self.arrPastBookingData.count == 0 {
                        self.lblNoRecord.isHidden = false
                    }else{
                        self.lblNoRecord.isHidden = true
                    }
                }else if self.isBookingHistory == 2{
                    self.tblPastBooking.reloadData()
                    if self.arrFutureBookingData.count == 0 {
                        self.lblNoRecord.isHidden = false
                    }else{
                        self.lblNoRecord.isHidden = true
                    }
                }else if self.isBookingHistory == 3{
                    self.tblPastBooking.reloadData()
                    if self.arrCancelBookingData.count == 0 {
                        self.lblNoRecord.isHidden = false
                    }else{
                        self.lblNoRecord.isHidden = true
                    }
                }
                
                objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
            }
        }
        
    }
    
    //MARK: - custome method extension
    extension PaymentHistoryVC {
        
        func getFormattedBookingDateToShowFrom(strDate : String) -> String{
            
            if strDate == ""{
                return ""
            }
            
            let formatter  = DateFormatter()
            formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
            formatter.dateFormat = "yyyy-MM-dd"
            if let date = formatter.date(from: strDate){
                formatter.dateFormat = "dd/MM/yyyy" //"dd MMMM yyyy"
                
                let strDate = formatter.string(from: date)
                return strDate
            }
            return ""
        }
        
        func gotoAppoitmentBookingVC(obj : PastFutureBookingData) {
            self.view.endEditing(true)
            let sb = UIStoryboard(name:"Booking",bundle:Bundle.main)
            let objChooseType = sb.instantiateViewController(withIdentifier:"ConfirmBookingVC") as! ConfirmBookingVC
            objChooseType.hidesBottomBarWhenPushed = true
            objChooseType.strBookingId =  String(obj._id)
            objChooseType.fromPaymentVC = true
            self.navigationController?.pushViewController(objChooseType, animated: true)
            
        }
        
        
        
        
        
        @objc func btnProfileRedirectionAction1(sender: UIButton!){
            if  isBookingHistory == 1{
               let objUser  =  self.arrPastBookingData[sender.tag]
                self.profileRedirectionAPI(objUsers: objUser)
            }else  if  isBookingHistory == 2{
             let objUser =  self.arrFutureBookingData[sender.tag]
                self.profileRedirectionAPI(objUsers: objUser)
            }else  if  isBookingHistory == 3{
                let objUser =  self.arrCancelBookingData[sender.tag]
                self.profileRedirectionAPI(objUsers: objUser)
            }
 
        }
        
        
        func profileRedirectionAPI(objUsers:PastFutureBookingData){
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            if objUsers.customerType == "walking"{
                return
            }
            var objUser = UserDetail(dict: ["":""])
            if objUsers.arrUserDetail.count > 0 {
                objUser = objUsers.arrUserDetail[0]
            }
            
            let dicParam = ["userName":objUser.userName]
            objServiceManager.requestPost(strURL: WebURL.profileByUserName, params: dicParam, success: { response in
                if response["status"] as? String ?? "" == "success"{
                    var strId = ""
                    var strType = ""
                    if let dictUser = response["userDetail"] as? [String : Any]{
                        let myId = dictUser["_id"] as? Int ?? 0
                        strId = String(myId)
                        strType = dictUser["userType"] as? String ?? ""
                    }
                    let dic = [
                        "tabType" : "people",
                        "tagId": strId,
                        "userType":strType,
                        "title": objUser.userName
                        ] as [String : Any]
                    self.openProfileForSelectedTagPopoverWithInfo(dict: dic)
                }
            }) { error in
            }
        }
        func openProfileForSelectedTagPopoverWithInfo(dict : [AnyHashable: Any]){
            
            var dictTemp : [AnyHashable : Any]?
            
            dictTemp = dict
            
            if let dict1 = dict as? [String:[String :Any]] {
                if let dict2 = dict1.first?.value {
                    dictTemp = dict2
                }
            }
            
            guard let dictFinal = dictTemp as? [String : Any] else { return }
            
            var strUserType: String?
            var tagId : Int?
            
            if let userType = dictFinal["userType"] as? String{
                strUserType = userType
                
                if let idTag = dictFinal["tagId"] as? Int{
                    tagId = idTag
                }else{
                    if let idTag = dictFinal["tagId"] as? String{
                        tagId = Int(idTag)
                    }
                }
                let id = Int(self.strUserId)
                if id == tagId {
                    //isNavigate = true
                    objAppShareData.isOtherSelectedForProfile = false
                    self.gotoProfileVC()
                    return
                }
                
                if let strUserType = strUserType, let tagId =  tagId {
                    
                    objAppShareData.selectedOtherIdForProfile  = String(tagId)
                    objAppShareData.isOtherSelectedForProfile = true
                    objAppShareData.selectedOtherTypeForProfile = strUserType  //"artist" or "user"
                    //isNavigate = true
                    self.gotoProfileVC()
                }
            }
        }
        func gotoProfileVC (){
            self.view.endEditing(true)
            let sb: UIStoryboard = UIStoryboard(name: "ArtistProfile", bundle: Bundle.main)
            if let objVC = sb.instantiateViewController(withIdentifier:"SWRevealViewController") as? SWRevealViewController{
                objVC.hidesBottomBarWhenPushed = true
                navigationController?.pushViewController(objVC, animated: true)
            }
        }
        @IBAction func btnHiddenBottumTable(_ sender: UIButton) {
            self.viewBottumTable.isHidden = true
        }
}

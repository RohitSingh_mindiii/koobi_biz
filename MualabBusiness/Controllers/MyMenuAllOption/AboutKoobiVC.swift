//
//  AboutKoobiVC.swift
//  MualabBusiness
//
//  Created by Mindiii on 3/14/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit

class AboutKoobiVC: UIViewController,UIWebViewDelegate {
    
    
    @IBOutlet weak var webViewTC: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.webViewTC.delegate = self
        self.webViewTC.scalesPageToFit = true
        let url = URL(string: WebURL.AboutUs)
        let requestObj = URLRequest(url: url!)
        self.webViewTC.loadRequest(requestObj as URLRequest)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.tabBarController?.view.frame =  CGRect(x:0,y:0,width:Int(self.view.frame.width),height:viewHeightGloble)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.viewConfigure()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func webViewDidStartLoad(_ webView: UIWebView){
        objWebserviceManager.StartIndicator()
    }
    func webViewDidFinishLoad(_ webView: UIWebView)
    {
        objWebserviceManager.StopIndicator()
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        objWebserviceManager.StopIndicator()
    }
}

//MARK:- Custome Methods extension
extension AboutKoobiVC{
    @IBAction func btnBack(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    func viewConfigure(){
    }
    
    
    func changeString(userName:String){
        let colorBlack = UIColor.black
        
        let a = "We invite you to join"
        let b = userName
        let c = "as a staff member Accept the invitation and get started login in biz app with the same social credential"
        
        
        let StrName = NSMutableAttributedString(string: a + " ", attributes: [NSAttributedString.Key.font:UIFont(name: "Nunito-Regular", size: 17.0)!])
        
        StrName.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.lightGray, range: NSRange(location:0,length:StrName.length))
        
        let StrName1 = NSMutableAttributedString(string: b + " ", attributes: [NSAttributedString.Key.font:UIFont(name: "Nunito-SemiBold", size: 17.0)!])
        
        StrName1.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSRange(location:0,length:StrName1.length))
        
        
        let StrName2 = NSMutableAttributedString(string: c + " ", attributes: [NSAttributedString.Key.font:UIFont(name: "Nunito-Regular", size: 17.0)!])
        
        StrName2.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.lightGray, range: NSRange(location:0,length:StrName2.length))
        
        
        let combination = NSMutableAttributedString()
        
        combination.append(StrName)
        combination.append(StrName1)
        combination.append(StrName2)
        // self.lblDescription.attributedText = combination
    }
}


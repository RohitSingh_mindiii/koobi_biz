//
//  CellSettingList.swift
//  MualabBusiness
//
//  Created by Mindiii on 2/7/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit

class CellSettingList: UITableViewCell {
    
    @IBOutlet weak var imgSettingOption: UIImageView!
    @IBOutlet weak var lblSettingOption: UILabel!
    @IBOutlet weak var switchAction: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

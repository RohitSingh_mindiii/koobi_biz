//
//  SettingListVC.swift
//  MualabBusiness
//
//  Created by Mindiii on 2/7/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import Firebase
class SettingListVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
 
    
    @IBOutlet weak var tblSettingList: UITableView!
    
    var notificationStatus = 0
    var ref: DatabaseReference!
    var messages: [DataSnapshot]! = []
    fileprivate var _refHandle: DatabaseHandle?
   // var arrSettingOptionText1 = ["Notification Settings","Profile Settings","Help & support","Blocked Account","About us","Terms & Conduction","Privacy Policy","Reset Password"]
    var arrSettingOptionText = ["Notification",
                                "Terms & Conditions",
                                //"Help & support",
                                //"About us",
                                "Privacy Policy",
                                "Reset Password"]

    var arrSettingOptionImage1 = [#imageLiteral(resourceName: "notification_ico"),#imageLiteral(resourceName: "user_ico_setting"),#imageLiteral(resourceName: "helf_ico"),#imageLiteral(resourceName: "blocked_account_ico"),#imageLiteral(resourceName: "about_us_ico_setting"),#imageLiteral(resourceName: "term_ico"),#imageLiteral(resourceName: "lock_ico_setting"),#imageLiteral(resourceName: "lock_ico_setting")]
    var arrSettingOptionImage = [#imageLiteral(resourceName: "notification_ico"),#imageLiteral(resourceName: "term_ico"),
                                 //#imageLiteral(resourceName: "helf_ico"),
                                 //#imageLiteral(resourceName: "about_us_ico_setting"),
                                 #imageLiteral(resourceName: "policy_ico_setting"),#imageLiteral(resourceName: "lock_ico_setting")]

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblSettingList.delegate = self
        self.tblSettingList.dataSource = self
        
        if let str = UserDefaults.standard.value(forKey: UserDefaults.keys.notificationStatus) as? Int{
            self.notificationStatus = str
        }else if let str = UserDefaults.standard.value(forKey: UserDefaults.keys.notificationStatus) as? String{
            self.notificationStatus = Int(str)!
        }
        
        self.tblSettingList.reloadData()
        
        // Do any additional setup after loading the view.
    }
    
    func changeMuteStatus(){
        
        let calendarDate = ServerValue.timestamp()
        var dict:[String:Any] = [:]
        if self.notificationStatus == 0{
            self.notificationStatus = 1
//            dict = [    "isOnline":0,
//                        "lastActivity":calendarDate,
//                    "firebaseToken":objAppShareData.firebaseToken
//            ]
            dict = [    "isOnline":0,
                        "lastActivity":calendarDate,
                        "firebaseToken":""
            ]
        }else{
            self.notificationStatus = 0
            dict = ["isOnline":0,
                    "lastActivity":calendarDate,
                    "firebaseToken":""
            ]
        }
        let myId = UserDefaults.standard.string(forKey: UserDefaults.keys.userId) ?? ""
        Database.database().reference().child("users").child(myId).updateChildValues(dict)
        self.tblSettingList.reloadData()
        self.callWebserviceFor_UpdateNotificationStatus()
        
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrSettingOptionText.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell  = tableView.dequeueReusableCell(withIdentifier: "CellSettingList", for: indexPath)as? CellSettingList{
            cell.lblSettingOption.text = self.arrSettingOptionText[indexPath.row]
            if indexPath.row == 0{
                cell.switchAction.isHidden = false
                }else{
                cell.switchAction.isHidden = true
                }
            if self.notificationStatus == 0{
                cell.switchAction.setImage(#imageLiteral(resourceName: "off_icon"), for: .normal)
            }else{
                cell.switchAction.setImage(#imageLiteral(resourceName: "on_ico"), for: .normal)
            }
            cell.switchAction.tag = indexPath.row
            cell.switchAction.addTarget(self, action:#selector(switchAction(sender:)) , for: .touchUpInside)
            cell.imgSettingOption.image = self.arrSettingOptionImage[indexPath.row]
            return cell
        }else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.arrSettingOptionText[indexPath.row] == "Reset Password"{
            let sb: UIStoryboard = UIStoryboard(name: "Setting", bundle: Bundle.main)
            if let objVC = sb.instantiateViewController(withIdentifier:"ChangePasswordVC") as? ChangePasswordVC{
                self.navigationController?.pushViewController(objVC, animated: true)
            }
        }else if self.arrSettingOptionText[indexPath.row] == "Terms & Conditions"{
            let viewController = UIStoryboard(name: "Main",bundle: nil).instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
            viewController.strComeFrom = "Terms & Conditions"
            self.navigationController?.pushViewController(viewController, animated: true)
        }else if self.arrSettingOptionText[indexPath.row] == "Privacy Policy"{
            let viewController = UIStoryboard(name: "Main",bundle: nil).instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
            viewController.strComeFrom = "Privacy Policy"
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    @objc  func switchAction(sender: UIButton!){
        self.changeMuteStatus()
//        if let cell = tblSettingList.cellForRow(at: indexPath as IndexPath) as? CellSettingList{
//            cell.switchAction.isSelected = !cell.switchAction.isSelected
//        }
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

// MARK: - Webservice calls
extension SettingListVC{
    
    func callWebserviceFor_UpdateNotificationStatus() {
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        var token = ""
        if self.notificationStatus == 0{
            token = ""
        }else{
            token = objAppShareData.firebaseToken
        }
        let parameters : Dictionary = [
            "notificationStatus" : self.notificationStatus,
            "firebaseToken": token
            ] as [String : Any]
        
        objWebserviceManager.requestPost(strURL: WebURL.updateRecord, params: parameters  , success: { response in
            let keyExists = response["responseCode"] != nil
            if  keyExists {
            }else{
                
                let strStatus =  response["status"] as? String ?? ""
                if strStatus == k_success{
                    UserDefaults.standard.setValue(self.notificationStatus, forKey: UserDefaults.keys.notificationStatus)
                    UserDefaults.standard.synchronize()
                }else{
                }
            }
        }){ error in
        }
    }
}

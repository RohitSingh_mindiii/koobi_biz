//
//  SetTimeVC.swift
//  MualabBusiness
//
//  Created by Mac on 04/12/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

class SetTimeVC: UIViewController,UIPickerViewDelegate, UIPickerViewDataSource {
    
 
   fileprivate var Hours = [Any]()
   fileprivate var Minute = [Any]()
   fileprivate var selectedSegmentIndex: Int = 0
   fileprivate var selectedHours = ""
   fileprivate var selectedMinute = ""
   fileprivate var bookingType = ""
   fileprivate var bookingSelectionType = ""
    
    fileprivate var InCallHours = ""
    fileprivate var InCallMinurte = ""
    fileprivate var OutCallHours = ""
    fileprivate var OutCallMinurte = ""
    
    var isEditingView = true

    @IBOutlet weak var viewBtnSubmit: UIView!

    @IBOutlet weak var pickerHours: UIPickerView!
    @IBOutlet weak var pickerMinute: UIPickerView!
    @IBOutlet weak var viewSwipeAction: UIView!

    @IBOutlet weak var btnIncall: UIButton!
    @IBOutlet weak var btnoutcall:UIButton!
    
    @IBOutlet weak var stackOfButton:UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadDefaultsParameters()
     }
    
    override func viewWillAppear(_ animated: Bool) {
        self.viewConfigure()
        self.pickerHours.isUserInteractionEnabled = isEditingView
        self.pickerMinute.isUserInteractionEnabled = isEditingView
        self.viewBtnSubmit.isHidden = !isEditingView
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 }
//MARK:- Custome method extension
extension SetTimeVC{
    func viewConfigure(){
        
        if objAppShareData.objModelBusinessSetup.outcallTimeHours == "HH" || objAppShareData.objModelBusinessSetup.outcallTimeHours == "hh" || objAppShareData.objModelBusinessSetup.outcallTimeHours == ""{
            objAppShareData.objModelBusinessSetup.outcallTimeHours = "00"
        }
        
        if objAppShareData.objModelBusinessSetup.incallTimeHours == "HH" || objAppShareData.objModelBusinessSetup.incallTimeHours == "hh" || objAppShareData.objModelBusinessSetup.incallTimeHours == ""{
            objAppShareData.objModelBusinessSetup.incallTimeHours = "00"
        }
        
        if objAppShareData.objModelBusinessSetup.outcallTimeMinute == "MM" || objAppShareData.objModelBusinessSetup.outcallTimeMinute == "mm" || objAppShareData.objModelBusinessSetup.outcallTimeMinute == ""{
            objAppShareData.objModelBusinessSetup.outcallTimeMinute = "00"
        }
        
        if objAppShareData.objModelBusinessSetup.incallTimeMinute == "MM" || objAppShareData.objModelBusinessSetup.incallTimeMinute == "mm" || objAppShareData.objModelBusinessSetup.incallTimeMinute == ""{
            objAppShareData.objModelBusinessSetup.incallTimeMinute = "00"
        }
        
        
        
        
        
        let businessType = UserDefaults.standard.string(forKey: UserDefaults.keys.mainServiceType)
        self.stackOfButton.isHidden = true
        bookingSelectionType = "Incall"
        if businessType == "1"{//incall
            self.bookingType = "Incall"
            self.selectIncall()
        }else if businessType == "2"{//outcall
            self.selectOutcall()
            self.bookingType = "Outcall"
        }else if businessType == "3"{//Both
            self.selectIncall()
            self.bookingType = "Both"
            self.stackOfButton.isHidden = false
            self.addGesturesToView()
        }else{//none
            self.selectIncall()
            self.bookingType = "Both"
            self.stackOfButton.isHidden = false
            self.addGesturesToView()
        }
        
        
        
        InCallHours = objAppShareData.objModelBusinessSetup.incallTimeHours
        InCallMinurte = objAppShareData.objModelBusinessSetup.incallTimeMinute
        OutCallHours = objAppShareData.objModelBusinessSetup.outcallTimeHours
        OutCallMinurte = objAppShareData.objModelBusinessSetup.outcallTimeMinute
    }
    
    
    
    
    func selectIncall(){
        bookingSelectionType = "Incall"
        self.btnIncall.setTitleColor(appColor, for: .normal)
        self.btnoutcall.setTitleColor(UIColor.black, for: .normal)
        
        if InCallHours == ""{
            let b = String(objAppShareData.objModelBusinessSetup.incallTimeHours.last ?? "0")
            pickerHours.selectRow(Int(b)!, inComponent: 0, animated: true)
        }else{
            let b = String(InCallHours.last ?? "0")
            pickerHours.selectRow(Int(b)!, inComponent: 0, animated: true)
        }
        
        if InCallMinurte == ""{
            let a = String(objAppShareData.objModelBusinessSetup.incallTimeMinute.first ?? "0")
            pickerMinute.selectRow(Int(a)!, inComponent: 0, animated: true)
        }else{
            let a = String(InCallMinurte.first ?? "0")
            pickerMinute.selectRow(Int(a)!, inComponent: 0, animated: true)
        }


    }
    
    func selectOutcall(){
        bookingSelectionType = "Outcall"
        
        self.btnIncall.setTitleColor(UIColor.black, for: .normal)
        self.btnoutcall.setTitleColor(appColor, for: .normal)
        
        
        
        if self.OutCallHours == ""{
            
            let b = String(objAppShareData.objModelBusinessSetup.outcallTimeHours.last ?? "0")
            pickerHours.selectRow(Int(b)!, inComponent: 0, animated: true)
        }else{
            let b = String(OutCallHours.last ?? "0")
            pickerHours.selectRow(Int(b)!, inComponent: 0, animated: true)
        }
        
        if OutCallMinurte == ""{
            let a = String(objAppShareData.objModelBusinessSetup.outcallTimeMinute.first ?? "0")
            pickerMinute.selectRow(Int(a)!, inComponent: 0, animated: true)
        }else{
            let a = String(OutCallMinurte.first ?? "0")
            pickerMinute.selectRow(Int(a)!, inComponent: 0, animated: true)
        }
        
        
//
//        let a = String(objAppShareData.objModelBusinessSetup.outcallTimeMinute.first ?? "0")
//
//        pickerHours.selectRow(Int(objAppShareData.objModelBusinessSetup.outcallTimeHours)!, inComponent: 0, animated: true)
//        pickerMinute.selectRow(Int(a)!, inComponent: 0, animated: true)
     }
    
}



//MARK: - Up down Swipe
fileprivate extension SetTimeVC{
    func addGesturesToView() -> Void {

        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.leftToRight))
        swipeRight.direction = .right
        self.viewSwipeAction.addGestureRecognizer(swipeRight)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.rightToLeft))
        swipeLeft.direction = .left
        self.viewSwipeAction.addGestureRecognizer(swipeLeft)
  
    }
    
    
    @objc func leftToRight() ->Void {
        self.selectIncall()
    }
    
    @objc func rightToLeft() ->Void {
        self.selectOutcall()
    }
    
}
//MARK:- Button extension
extension SetTimeVC{
    @IBAction func btnBackAction(_ sender:Any){
        InCallMinurte = ""
        InCallHours = ""
        OutCallMinurte = ""
        OutCallHours = ""
       self.navigationController?.popViewController(animated: true)
     }
    
    @IBAction func btnIncallAction(_ sender:Any){
        self.selectIncall()
    }
    
    @IBAction func btnOutcallAction(_ sender:Any){
        self.selectOutcall()
    }
    
    @IBAction func btnContinewAction(_ sender:Any){
//        let inHours = objAppShareData.objModelBusinessSetup.incallTimeHours
//        let inMinute = objAppShareData.objModelBusinessSetup.incallTimeMinute
//        let ocHours = objAppShareData.objModelBusinessSetup.outcallTimeHours
//        let ocMinute = objAppShareData.objModelBusinessSetup.outcallTimeMinute
        
        
        if self.bookingType == "Incall"{
            if InCallHours == "03" &&  InCallMinurte != "00"{
                objAppShareData.showAlert(withMessage: "Please select valid incall time", type: alertType.banner, on: self)
                return
            }
        }
        if self.bookingType == "Outcall"{
            if OutCallHours == "03" &&  OutCallMinurte != "00"{
                objAppShareData.showAlert(withMessage: "Please select valid outcall time", type: alertType.banner, on: self)
                return
            }
        }
        
        
        var check = true
        if self.bookingType == "Incall"{
//            if InCallHours == "00" && InCallMinurte == "00"{
//                check = false
//                objAppShareData.showAlert(withMessage: "Please select incall time", type: alertType.banner, on: self)
//            }else{
                objAppShareData.objModelBusinessSetup.incallTimeHours = self.InCallHours
                objAppShareData.objModelBusinessSetup.incallTimeMinute = self.InCallMinurte
                objAppShareData.objModelBusinessSetup.inCallpreprationTime = self.InCallHours+":"+self.InCallMinurte
                objAppShareData.objModelBusinessSetup.outCallpreprationTime = "00:00"
            //}
            
        }else if self.bookingType == "Outcall"{
//            if OutCallHours == "00" && OutCallMinurte == "00"{
//                check = false
//                objAppShareData.showAlert(withMessage: "Please select outcall time", type: alertType.banner, on: self)
//            }else{
                objAppShareData.objModelBusinessSetup.outcallTimeHours = self.OutCallHours
                objAppShareData.objModelBusinessSetup.outcallTimeMinute = self.OutCallMinurte
                objAppShareData.objModelBusinessSetup.outCallpreprationTime = self.OutCallHours+":"+self.OutCallMinurte
                objAppShareData.objModelBusinessSetup.inCallpreprationTime = "00:00"
            //}
        }else{
//            if InCallHours == "00" && InCallMinurte == "00"{
//                check = false
//                objAppShareData.showAlert(withMessage: "Please select incall time", type: alertType.banner, on: self)
//
//            }else if OutCallHours == "00" && OutCallMinurte == "00"{
//                check = false
//                objAppShareData.showAlert(withMessage: "Please select outcall time", type: alertType.banner, on: self)
//
//            }else{
                
                objAppShareData.objModelBusinessSetup.incallTimeHours = self.InCallHours
                objAppShareData.objModelBusinessSetup.incallTimeMinute = self.InCallMinurte
                objAppShareData.objModelBusinessSetup.outcallTimeHours = self.OutCallHours
                objAppShareData.objModelBusinessSetup.outcallTimeMinute = self.OutCallMinurte
            
objAppShareData.objModelBusinessSetup.inCallpreprationTime = self.InCallHours+":"+self.InCallMinurte
 objAppShareData.objModelBusinessSetup.outCallpreprationTime = self.OutCallHours+":"+self.OutCallMinurte
            
            //}
        }
        
        if check == true{
            InCallMinurte = ""
            InCallHours = ""
            OutCallMinurte = ""
            OutCallHours = ""
            self.navigationController?.popViewController(animated: true)
        }
    }
}

//MARK:- Method for expiry month
extension SetTimeVC{
    
    func NumberOfHours() -> [Any] {
        return ["00","01", "02","03"]
    }
    
    func NumberOfMinute() -> [Any] {
        
         return ["00","10","20","30","40","50"]
    }
    
    func loadDefaultsParameters() {
        Hours = NumberOfHours()
        Minute = NumberOfMinute()
 
        pickerHours.delegate = self
        pickerHours.dataSource = self
        pickerMinute.delegate = self
        pickerMinute.dataSource = self
     }
}
//MARK:- Method for expiry month
extension SetTimeVC {
// MARK:- Picker View Delegates
func numberOfComponents(in pickerView: UIPickerView) -> Int {
    return 1
}
func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    if pickerView == pickerHours{
        return Hours.count
    }
    else if pickerView == pickerMinute{
        return Minute.count
    }else {
        return 0
    }
}

func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    
    if pickerView == pickerHours{
        let hourseName: String = Hours[row] as! String
        selectedHours = hourseName
        return hourseName
    }
    else  if pickerView == pickerMinute{
        let minuteName: String = Minute[row] as! String
        let str = "\(minuteName)"
        selectedMinute = str
        return str
    }else{
        return ""
    }
}
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        var pickerLabel: UILabel? = (view as? UILabel)
        if pickerLabel == nil {
            pickerLabel = UILabel()
            pickerLabel?.font = UIFont(name: "Nunito-Medium", size: 30)
            pickerLabel?.textAlignment = .center
            pickerLabel?.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
            pickerLabel?.shadowColor = UIColor.clear
         }
        
        if pickerView == pickerHours{
//             if self.bookingType == "Both"{
//                if bookingSelectionType == "Incall"{
                    pickerLabel?.text = Hours[row] as? String ?? "00"
//                    InCallHours = Hours[row] as? String ?? "00"
//                }else{
//                    pickerLabel?.text = Hours[row] as? String ?? "00"
//                    OutCallHours = Hours[row] as? String ?? "00"
//                }
//            }else if bookingType == "Incall"{
//                InCallHours = Hours[row] as? String ?? "00"
//                pickerLabel?.text = Hours[row] as? String ?? "00"
//
//                OutCallHours = "00"
//                OutCallMinurte = "00"
//
//            }else if bookingType == "Outcall"{
//                InCallHours = "00"
//                InCallMinurte = "00"
//                pickerLabel?.text = Hours[row] as? String ?? "00"
//                OutCallHours = Hours[row] as? String ?? "00"
      //      }
        }
        if pickerView == pickerMinute{
//            if self.bookingType == "Both"{
//                if bookingSelectionType == "Incall"{
                    pickerLabel?.text = Minute[row] as? String ?? "00"
//                    InCallMinurte = Minute[row] as? String ?? "00"
//                }else{
//                    pickerLabel?.text = Minute[row] as? String ?? "00"
//                    OutCallMinurte = Minute[row] as? String ?? "00"
//                }
//            }else if bookingType == "Incall"{
//                pickerLabel?.text = Minute[row] as? String ?? "00"
//                InCallMinurte = Minute[row] as? String ?? "00"
//                OutCallHours = "00"
//                OutCallMinurte = "00"
//
//            }else if bookingType == "Outcall"{
//                InCallHours = "00"
//                InCallMinurte = "00"
//                pickerLabel?.text = Minute[row] as? String ?? "00"
//                OutCallMinurte = Minute[row] as? String ?? "00"
//            }
        }
         return pickerLabel!
     }
 
func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    

    if pickerView == pickerHours{

    if self.bookingType == "Both"{
        if bookingSelectionType == "Incall"{
            InCallHours = Hours[row] as? String ?? "00"
            print("InCallHours = ",InCallHours)
            if InCallHours == "03"{
                InCallMinurte = "00"
                pickerMinute.selectRow(0, inComponent: 0, animated: true)
            }
          }else{
             OutCallHours = Hours[row] as? String ?? "00"
            print("OutCallHours = ",OutCallHours)
            if OutCallHours == "03"{
                OutCallMinurte = "00"
                pickerMinute.selectRow(0, inComponent: 0, animated: true)
            }
         }
        
    }else if bookingType == "Incall"{
        InCallHours = Hours[row] as? String ?? "00"
        print("InCallHours = ",InCallHours)
         OutCallHours = "00"
        OutCallMinurte = "00"
        if InCallHours == "03"{
            InCallMinurte = "00"
            pickerMinute.selectRow(0, inComponent: 0, animated: true)
        }
    }else if bookingType == "Outcall"{
        InCallHours = "00"
        InCallMinurte = "00"
        OutCallHours = Hours[row] as? String ?? "00"
        print("OutCallHours = ",OutCallHours)
        if OutCallHours == "03"{
            OutCallMinurte = "00"
            pickerMinute.selectRow(0, inComponent: 0, animated: true)
        }
     }
    
     }
    if pickerView == pickerMinute{
        if self.bookingType == "Both"{
            if bookingSelectionType == "Incall"{
                 InCallMinurte = Minute[row] as? String ?? "00"
                print("InCallMinurte = ",InCallMinurte)
                if InCallHours == "03"{
                    InCallMinurte = "00"
                    pickerMinute.selectRow(0, inComponent: 0, animated: true)
                }
            }else{
                 OutCallMinurte = Minute[row] as? String ?? "00"
                print("OutCallMinurte = ",OutCallMinurte)
                if OutCallHours == "03"{
                    OutCallMinurte = "00"
                    pickerMinute.selectRow(0, inComponent: 0, animated: true)
                }
            }
        }else if bookingType == "Incall"{
             InCallMinurte = Minute[row] as? String ?? "00"
            print("InCallMinurte = ",InCallMinurte)

            OutCallHours = "00"
            OutCallMinurte = "00"
            if InCallHours == "03"{
                InCallMinurte = "00"
                pickerMinute.selectRow(0, inComponent: 0, animated: true)
            }
            
        }else if bookingType == "Outcall"{
            InCallHours = "00"
            InCallMinurte = "00"
            print("InCallMinurte = ",InCallMinurte)
             OutCallMinurte = Minute[row] as? String ?? "00"
            if OutCallHours == "03"{
                OutCallMinurte = "00"
                pickerMinute.selectRow(0, inComponent: 0, animated: true)
            }
        }
    }
    

  
}
}

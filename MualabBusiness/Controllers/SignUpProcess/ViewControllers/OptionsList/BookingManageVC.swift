//
//  BookingManageVC.swift
//  MualabBusiness
//
//  Created by Mindiii on 4/9/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit

class BookingManageVC: UIViewController {

    @IBOutlet weak var btnAutomatic:UIButton!
    @IBOutlet weak var btnManual:UIButton!
    var bookingSetting = ""
    var fromSignUp = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bookingSetting = objAppShareData.objModelBusinessSetup.bookingSetting
        self.btnAutomatic.backgroundColor = UIColor.clear
        self.btnManual.backgroundColor = UIColor.clear
    }
    override func viewWillAppear(_ animated: Bool) {
        self.configureView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        if fromSignUp == false{
        self.tabBarController?.view.frame =  CGRect(x:0,y:0,width:Int(self.view.frame.width),height:viewHeightGloble)
        }
    }
    
    
    func configureView(){
        let bookingSetting = objAppShareData.objModelBusinessSetup.bookingSetting
        var strType = ""
        if bookingSetting == "0"{
            self.automaticManage()
        }else if bookingSetting == "1"{
            self.manualManage()
        }else{
            self.automaticManage()
            objAppShareData.objModelBusinessSetup.bookingSetting = "0"
        }
    }
    @IBAction func btnBack(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnAutomatic(_ sender:Any){
        self.automaticManage()
    }
    @IBAction func btnManual(_ sender:Any){
        self.manualManage()
    }
    
    func automaticManage(){
        self.btnAutomatic.setTitleColor(UIColor.white, for: .normal)
        self.btnManual.setTitleColor(UIColor.black, for: .normal)
        self.btnAutomatic.backgroundColor = #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1)
        self.btnManual.backgroundColor = UIColor.clear
        self.bookingSetting = "0"
    }
    func manualManage(){
        self.btnAutomatic.setTitleColor(UIColor.black, for: .normal)
        self.btnManual.setTitleColor(UIColor.white, for: .normal)
        self.btnAutomatic.backgroundColor = UIColor.clear
        self.btnManual.backgroundColor = #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1)
        self.bookingSetting = "1"
    }
    
    @IBAction func btnContinew(_ sender:Any){
        let a  = self.bookingSetting
        if a != "0" && a != "1"{
            objAppShareData.showAlert(withMessage: "Please select booking accept type.", type: alertType.banner, on: self)
        }else{
            objAppShareData.objModelBusinessSetup.bookingSetting = bookingSetting
            UserDefaults.standard.setValue(bookingSetting, forKey: UserDefaults.keys.bookingSetting)
            if fromSignUp == true{
                objAppShareData.isComingFrom = "BookingSetting"
                self.navigationController?.popViewController(animated: true)
            }else{
                let dict = ["bookingSetting":a]
                self.callWebserviceForRegistration(dict: dict)
            }
        }
        
        
    }
    
    
    
    func callWebserviceForRegistration(dict:[String:Any]){
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        objActivity.startActivityIndicator()
        objServiceManager.requestPost(strURL: WebURL.updateRecord, params: dict, success:{ response in
            if response["status"] as? String == "success"{
                if let dictUserDetail = response["user"] as? [String:Any]{
                    objAppShareData.showAlert(withMessage: "Booking option changed successfully", type: alertType.banner, on: self)
                    objAppShareData.objAppdelegate.parseBusinessSignUpDetail(dictOfInfo: dictUserDetail)
                    self.navigationController?.popViewController(animated: true)

                }else if let dictUserDetail = response["users"] as? [String:Any]{
                    objAppShareData.showAlert(withMessage: "Booking option changed successfully", type: alertType.banner, on: self)
                    objAppShareData.objAppdelegate.parseBusinessSignUpDetail(dictOfInfo: dictUserDetail)
                    self.navigationController?.popViewController(animated: true)
                }
            }else{
                let msg = response["message"] as! String
                objAppShareData.showAlert(withMessage: msg, type: alertType.banner, on: self)
            }
        }) { error in
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
}

//
//  PaymentOptionVC.swift
//  MualabBusiness
//
//  Created by Mindiii on 5/7/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit

class PaymentOptionVC: UIViewController {

    @IBOutlet weak var btnCard:UIButton!
    @IBOutlet weak var btnCash:UIButton!
    @IBOutlet weak var btnBoth:UIButton!
    
    fileprivate var paymentType = ""
    fileprivate var bankStatus = "0"
    fileprivate var bookingCount = "0"
    fileprivate var myId = ""
    
    var fromSignUp = true

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let dictUser = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]{
            print("dictUser = ",dictUser)
            if let id = dictUser[UserDefaults.keys.bankStatus] as? String{
                bankStatus = id
            }else if let id = dictUser[UserDefaults.keys.bankStatus] as? Int{
                bankStatus = String(id)
            }
            
            if let id = dictUser[UserDefaults.keys.userId] as? String{
                myId = id
            }else if let id = dictUser[UserDefaults.keys.userId] as? Int{
                myId = String(id)
        }
        }
        if fromSignUp == false{
            let dict = ["businessId":myId]
            self.callWebserviceForChackBooking(dict: dict)
        }
        paymentType = objAppShareData.objModelBusinessSetup.paymentType
        self.btnCard.backgroundColor = UIColor.clear
        self.btnCash.backgroundColor = UIColor.clear
        self.btnBoth.backgroundColor = UIColor.clear
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.configureView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        if fromSignUp == false{
            self.tabBarController?.view.frame =  CGRect(x:0,y:0,width:Int(self.view.frame.width),height:viewHeightGloble)
        }
    }
    
    func configureView(){
        let paymentType = objAppShareData.objModelBusinessSetup.paymentType
        var strType = ""
        if paymentType == "1"{
            self.manageToOnlineCard()
        }else if paymentType == "2"{
            self.manageToCash()
        }else if paymentType == "3"{
            self.manageToBoth()
        }
    }
    
    @IBAction func btnBack(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCard(_ sender:Any){
        if fromSignUp == false && objAppShareData.objModelBusinessSetup.paymentType != "1"{
            if bankStatus == "0" {
                objAppShareData.showAlert(withMessage: "Please setup your payment info before changing your payment option", type: alertType.banner, on: self)
            } else  if   self.bookingCount != "0" {
                objAppShareData.showAlert(withMessage: "Please complete all your bookings before changing payment option", type: alertType.banner, on: self)
                return
            }else{
                self.manageToOnlineCard()
            }
        }else{
            self.manageToOnlineCard()
        }
    }
    
    @IBAction func btnCash(_ sender:Any){
        if fromSignUp == false && objAppShareData.objModelBusinessSetup.paymentType != "2"{
            if  bookingCount != "0" {
                objAppShareData.showAlert(withMessage: "Please complete all your bookings before changing payment option", type: alertType.banner, on: self)
                return
            }else{
                self.manageToCash()
            }
        }else{
            self.manageToCash()
        }
        
        
       
    }
    @IBAction func btnBoth(_ sender:Any){
        if fromSignUp == false && objAppShareData.objModelBusinessSetup.paymentType != "3"{
            if bankStatus == "0" {
                objAppShareData.showAlert(withMessage: "Please setup your payment info before changing your payment option", type: alertType.banner, on: self)
                return
            }else{
                self.manageToBoth()
            }
        }else{
            self.manageToBoth()
        }
    }
    
    func manageToOnlineCard(){
        self.btnCard.setTitleColor(UIColor.white, for: .normal)
        self.btnCash.setTitleColor(UIColor.black, for: .normal)
        self.btnBoth.setTitleColor(UIColor.black, for: .normal)
        self.btnCard.backgroundColor = #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1)
        self.btnCash.backgroundColor = UIColor.clear
        self.btnBoth.backgroundColor = UIColor.clear
        self.paymentType = "1"
    }
    
    func manageToCash(){
        self.btnCard.setTitleColor(UIColor.black, for: .normal)
        self.btnCash.setTitleColor(UIColor.white, for: .normal)
        self.btnBoth.setTitleColor(UIColor.black, for: .normal)
        self.btnCard.backgroundColor = UIColor.clear
        self.btnBoth.backgroundColor = UIColor.clear
        self.btnCash.backgroundColor = #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1)
        self.paymentType = "2"
    }
    
    func manageToBoth(){
        self.btnCard.setTitleColor(UIColor.black, for: .normal)
        self.btnCash.setTitleColor(UIColor.black, for: .normal)
        self.btnBoth.setTitleColor(UIColor.white, for: .normal)
        self.btnCard.backgroundColor = UIColor.clear
        self.btnCash.backgroundColor = UIColor.clear
        self.btnBoth.backgroundColor = #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1)
        self.paymentType = "3"
    }
    
    @IBAction func btnContinew(_ sender:Any){
        let a  = self.paymentType
        if a != "1" && a != "2" && a != "3"{
            objAppShareData.showAlert(withMessage: "Please select payment type.", type: alertType.banner, on: self)
        }else{
            objAppShareData.objModelBusinessSetup.paymentType = self.paymentType
            objAppShareData.isComingFrom = "Payment"
          //  UserDefaults.standard.setValue(paymentType, forKey: UserDefaults.keys.paymentType)
          //  if fromSignUp == true{
                self.navigationController?.popViewController(animated: true)
//            }else{
//                let dict = ["payOption":a]
//                self.callWebserviceForRegistration(dict: dict)
//            }
        }
    }
    
    
    
    func callWebserviceForRegistration(dict:[String:Any]){
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        
        objActivity.startActivityIndicator()
        objServiceManager.requestPost(strURL: WebURL.updateRecord, params: dict, success:{ response in
            if response["status"] as? String == "success"{
                if let dictUserDetail = response["user"] as? [String:Any]{
                    objAppShareData.showAlert(withMessage: "Payment type changed successfully", type: alertType.banner, on: self)
                    objAppShareData.objAppdelegate.parseBusinessSignUpDetail(dictOfInfo: dictUserDetail)
                    self.navigationController?.popViewController(animated: true)
                }else if let dictUserDetail = response["users"] as? [String:Any]{
                    objAppShareData.showAlert(withMessage: "Payment type changed successfully", type: alertType.banner, on: self)
                    objAppShareData.objAppdelegate.parseBusinessSignUpDetail(dictOfInfo: dictUserDetail)
                    self.navigationController?.popViewController(animated: true)
                }
            }else{
                let msg = response["message"] as! String
                objAppShareData.showAlert(withMessage: msg, type: alertType.banner, on: self)
            }
        }) { error in
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
    
    func callWebserviceForChackBooking(dict:[String:Any]){
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        
        objActivity.startActivityIndicator()
        objServiceManager.requestPost(strURL: WebURL.bookingCount, params: dict, success:{ response in
            objActivity.stopActivity()

            if response["status"] as? String == "success"{
                if let bData = response["bData"] as? String {
                    self.bookingCount = bData
                }else if let bData = response["bData"] as? Int {
                    self.bookingCount = String(bData)
                }
            }else{
                let msg = response["message"] as! String
            }
        }) { error in
            objActivity.stopActivity()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
}

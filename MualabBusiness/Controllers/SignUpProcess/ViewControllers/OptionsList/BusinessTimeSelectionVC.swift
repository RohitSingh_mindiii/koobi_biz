//
//  BusinessTimeSelectionVC.swift
//  MualabBusiness
//
//  Created by Mac on 03/12/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

class BusinessTimeSelectionVC: UIViewController {

    @IBOutlet weak var btnIncall:UIButton!
    @IBOutlet weak var btnOutcall:UIButton!
    @IBOutlet weak var btnBoth:UIButton!
 
    var bookType = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.btnBoth.backgroundColor = UIColor.clear
        self.btnIncall.backgroundColor = UIColor.clear
        self.btnOutcall.backgroundColor = UIColor.clear
    }

    func configureView(){
        //let businessType = UserDefaults.standard.string(forKey: UserDefaults.keys.mainServiceType)
        let businessType = objAppShareData.objModelBusinessSetup.serviceType
        var strType = ""
        if businessType == "1"{
            self.incallMethod()
        }else if businessType == "2"{
            self.outcallMethod()
         }else if businessType == "3" || businessType == "0"{
            self.bothcallMethod()
         }else{
            self.nonMethod()
        }
        // UserDefaults.standard.setValue("1", forKey: UserDefaults.keys.mainServiceType)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.endEditing(true)
        self.configureView()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


    
    func incallMethod(){
       // UserDefaults.standard.setValue("1", forKey: UserDefaults.keys.mainServiceType)
        objAppShareData.objModelBusinessSetup.radius = "0"
        //objAppShareData.objModelBusinessSetup.serviceType = "1"
        bookType = "1"
        self.btnBoth.setTitleColor(UIColor.black, for: .normal)
        self.btnIncall.setTitleColor(UIColor.white, for: .normal)
        self.btnOutcall.setTitleColor(UIColor.black, for: .normal)
        
        self.btnBoth.backgroundColor = UIColor.clear
        self.btnIncall.backgroundColor = #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1)
        self.btnOutcall.backgroundColor = UIColor.clear
    }
    func outcallMethod(){
       // UserDefaults.standard.setValue("2", forKey: UserDefaults.keys.mainServiceType)
        //objAppShareData.objModelBusinessSetup.serviceType = "2"
bookType = "2"
        self.btnBoth.setTitleColor(UIColor.black, for: .normal)
        self.btnIncall.setTitleColor(UIColor.black, for: .normal)
        self.btnOutcall.setTitleColor(UIColor.white, for: .normal)
        
        self.btnBoth.backgroundColor = UIColor.clear
        self.btnIncall.backgroundColor = UIColor.clear
        self.btnOutcall.backgroundColor = #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1)
    }
    
    func bothcallMethod(){
    //    UserDefaults.standard.setValue("3", forKey: UserDefaults.keys.mainServiceType)
        
      //  objAppShareData.objModelBusinessSetup.serviceType = "3"
        bookType = "3"
        self.btnBoth.setTitleColor(UIColor.white, for: .normal)
        self.btnIncall.setTitleColor(UIColor.black, for: .normal)
        self.btnOutcall.setTitleColor(UIColor.black, for: .normal)

        self.btnBoth.backgroundColor = #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1)
        self.btnIncall.backgroundColor = UIColor.clear
        self.btnOutcall.backgroundColor = UIColor.clear
    }
    
    func nonMethod(){
 
        self.btnBoth.setTitleColor(UIColor.black, for: .normal)
        self.btnIncall.setTitleColor(UIColor.black, for: .normal)
        self.btnOutcall.setTitleColor(UIColor.black, for: .normal)
        
        self.btnBoth.backgroundColor = UIColor.clear
        self.btnIncall.backgroundColor = UIColor.clear
        self.btnOutcall.backgroundColor = UIColor.clear
    }
    
    @IBAction func btnOutcall(_ sender:Any){
        self.outcallMethod()
    }
    @IBAction func btnBoth(_ sender:Any){
       self.bothcallMethod()
    }
    @IBAction func btnIncall(_ sender:Any){
        self.incallMethod()
    }
    
    @IBAction func btnBack(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnContinew(_ sender:Any){
        let a  = bookType
        
        if a != "1" && a != "2" && a != "3"{
            objAppShareData.showAlert(withMessage: "Please select booking type.", type: alertType.banner, on: self)
        }else{
            objAppShareData.objModelBusinessSetup.serviceType = bookType
            objAppShareData.isComingFrom = "Booking"
            self.navigationController?.popViewController(animated: true)
        }
        UserDefaults.standard.setValue(bookType, forKey: UserDefaults.keys.mainServiceType)


    }

}

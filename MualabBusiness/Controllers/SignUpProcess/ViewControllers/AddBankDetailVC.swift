//
//  AddBankDetailVC.swift
//  MualabBusiness
//
//  Created by Mac on 05/02/2018 .
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import CoreData
class AddBankDetailVC: UIViewController {
    fileprivate var arrCountry = [[String:String]]()
    @IBOutlet weak var lblHeader:UILabel!

    @IBOutlet weak var txtFirstName:UITextField!
    @IBOutlet weak var txtLastName:UITextField!
    @IBOutlet weak var txtDOB:UITextField!
    @IBOutlet weak var txtRNumber:UITextField!
    @IBOutlet weak var txtAcNumber:UITextField!
    @IBOutlet weak var txtConfirmAcNumber:UITextField!
    @IBOutlet weak var txtCountryCode:UITextField!
    @IBOutlet weak var txtPostalCode:UITextField!
    @IBOutlet weak var txtSSNLast:UITextField!
    @IBOutlet weak var DOBPicker : UIDatePicker!
    @IBOutlet weak var countryCodePicker : UIPickerView!
    @IBOutlet weak var bottomConstraint : NSLayoutConstraint!
    @IBOutlet weak var bottomOfCountryCode : NSLayoutConstraint!
    var managedContext:NSManagedObjectContext?

    @IBOutlet weak var btnSkip:UIButton!
    @IBOutlet weak var btnContinue:UIButton!
    @IBOutlet weak var viewProgress:UIView!
    var address = ""
    var city = ""
    var dob = ""
}

//MARK: - View Hirarchy
extension AddBankDetailVC{
    
    override func viewDidLoad() {
        super.viewDidLoad()

        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        
        managedContext = appDelegate.persistentContainer.viewContext
        
        DOBPicker.date = Date().addingTimeInterval(-365*12*24*60*60)
        DOBPicker.maximumDate = Date().addingTimeInterval(-365*12*24*60*60)
        self.arrCountry = [
            ["name":"United States",
            "code":"US"],
            ["name":"United Kingdom",
            "code":"GB"]]
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
       
        
        self.tabBarController?.view.frame =  CGRect(x:0,y:0,width:Int(self.view.frame.width),height:viewHeightGloble)
    }
    override func viewWillAppear(_ animated: Bool) {
       
        super.viewWillAppear(animated)
        if  let dict = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfoDic) as? [String:Any]{
            self.txtFirstName.text = dict[UserDefaults.myDetail.firstName] as? String ?? ""
            self.txtLastName.text = dict[UserDefaults.myDetail.lastName] as? String ?? ""
//            address = dict[UserDefaults.myDetail.address] as? String ?? ""
//            if address.count == 0{
//               address = "Indore"
//            }
            address = "Indore"
            city = address
            dob = dict[UserDefaults.myDetail.dob] as? String ?? ""
//            if dob.count == 0{
//               dob = "07-07-1991"
//            }else{
//                let arr = dob.components(separatedBy: "/")
//                if arr.count > 0{
//                   dob = arr.joined(separator: "-")
//                }
//            }
            dob = "07-07-1991"
        }
        
        if objAppShareData.editSelfServices == true{
            self.viewProgress.isHidden = true
            self.btnSkip.isHidden = true
            self.btnContinue.setTitle("Continue", for: .normal)
            self.lblHeader.text = "Payment Set Up"
            self.callWebserviceForGetBankDetail()

        }else{
            self.viewProgress.isHidden = false
            self.btnSkip.isHidden = false
            self.btnContinue.setTitle("Continue", for: .normal)
            UserDefaults.standard.setValue("4", forKey: UserDefaults.keys.navigationVC)
            self.lblHeader.text = "Banking Info"
            if objAppShareData.objModelBusinessSetup.paymentType != "2"{
                self.btnSkip.isHidden = true
            }
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.configureView()
        }
        
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
}
//MARK: - Local methods
fileprivate extension AddBankDetailVC{
    
    func configureView(){
        
        self.txtFirstName.layer.sublayerTransform = CATransform3DMakeTranslation(5,0,0)
        self.txtLastName.layer.sublayerTransform = CATransform3DMakeTranslation(5,0,0)
        self.txtDOB.layer.sublayerTransform = CATransform3DMakeTranslation(5,0,0)
        self.txtRNumber.layer.sublayerTransform = CATransform3DMakeTranslation(5,0,0)
        self.txtAcNumber.layer.sublayerTransform = CATransform3DMakeTranslation(5,0,0)
        self.txtConfirmAcNumber.layer.sublayerTransform = CATransform3DMakeTranslation(5,0,0)
        self.txtCountryCode.layer.sublayerTransform = CATransform3DMakeTranslation(5,0,0)
        self.txtPostalCode.layer.sublayerTransform = CATransform3DMakeTranslation(5,0,0)
        self.txtSSNLast.layer.sublayerTransform = CATransform3DMakeTranslation(5,0,0)
        
        self.bottomConstraint.constant = -self.view.frame.size.height/2
    }
    
    func gotoAddStaffVc() {
        callWebserviceForUpdateFinalStep()
    }
}
//MARK: - api Skip Button
fileprivate extension AddBankDetailVC{
    func callWebserviceForUpdateFinalStep(){
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        var businessId = ""
        if let dictUser = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfoDic) as? [String:Any]{
            if let busId = dictUser[UserDefaults.keys.business_id] as? String{
                businessId = busId
            }else if let busId = dictUser[UserDefaults.keys.business_id] as? Int{
                businessId = String(busId)
            }
        }
        print(businessId)
        objActivity.startActivityIndicator()
        objServiceManager.requestPost(strURL: WebURL.skipPage, params: ["business_id":businessId], success: { response in
            print(response)
            if  response["status"] as! String == "success"{
                //appDelegate.showBooking()
                if objAppShareData.editSelfServices == true{
                    self.navigationController?.popViewController(animated: true)
                }else{
                UserDefaults.standard.set(true, forKey: UserDefaults.keys.isLoggedIn)
                    var dic = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfoDic) as? [String:Any] ?? ["":""]
                    dic[UserDefaults.keys.isDocument] = "3"
                    UserDefaults.standard.set(dic, forKey: UserDefaults.keys.userInfoDic)
                UserDefaults.standard.synchronize()
                appDelegate.gotoTabBar(withAnitmation: true)

                }
            }else{
                let msg = response["message"] as? String ?? ""
                objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
            }
        }) { error in
            // objAppShareData.showDeafultAlert(withTitle: message.error.title, message: message.error.wrong, on: self)
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }

}


//MARK: - IBActions
fileprivate extension AddBankDetailVC{
    @IBAction func btnBack(_ sender:Any){
        self.view.endEditing(true)
        
        var navigationCheck = false
        if objAppShareData.editSelfServices == true{
            self.navigationController?.popViewController(animated: true)
        }else{
        UserDefaults.standard.setValue("1", forKey: UserDefaults.keys.navigationVC)
        
        let controllers = self.navigationController?.viewControllers
        for vc in controllers! {
            if vc is  ServicesListVC{
                navigationCheck = true
                _ = self.navigationController?.popToViewController(vc as! ServicesListVC, animated: true)
            }
        }
        
        if navigationCheck == false{
            let sb = UIStoryboard(name:"SignUpProcess",bundle:Bundle.main)
            let objChooseType = sb.instantiateViewController(withIdentifier:"AddBusinessTypeVC") as! AddBusinessTypeVC
            self.navigationController?.pushViewController(objChooseType, animated: false)
        }
        }
    }
    @IBAction func btnSelectDOB(_ sender:Any){
        self.bottomConstraint.constant = 0
        self.view.endEditing(true)
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    @IBAction func btnSkip(_ sender:Any){
        UserDefaults.standard.set("3", forKey: UserDefaults.keys.isDocument)
        self.gotoAddStaffVc()
    }
    
    @IBAction func btnContinue(_ sender:Any){
        var invalid = false
        if self.txtFirstName.text?.count == 0{
            objAppShareData.shakeViewField(self.txtFirstName)
            invalid = true
        }else if self.txtLastName.text?.count == 0{
            objAppShareData.shakeViewField(self.txtLastName)
            invalid = true
        }else if self.txtRNumber.text?.count == 0{
            objAppShareData.shakeViewField(self.txtRNumber)
            invalid = true
        }else if self.txtAcNumber.text?.count == 0{
            objAppShareData.shakeViewField(self.txtAcNumber)
            invalid = true
        }
        if !invalid {
            callWebserviceForAddBankDetail()
        }
    }
    @IBAction func btnSelectCountry(_ sender:Any){
        self.bottomOfCountryCode.constant = 0
        self.view.endEditing(true)
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    @IBAction func btnDoneCancleDOBSelection(_ sender:UIButton){
        if sender.tag == 1 {
            self.txtDOB.text = objAppShareData.dateFormat(forAPI: self.DOBPicker.date)
            self.txtRNumber.becomeFirstResponder()
        }
        
        self.bottomConstraint.constant = -self.view.frame.size.height/2
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func btnDoneCancleCountrySelection(_ sender:UIButton){
        if sender.tag == 1 {
            let row = self.countryCodePicker.selectedRow(inComponent:0)
            let dic = self.arrCountry[row]
            self.txtCountryCode.text = dic["code"]
            self.txtPostalCode.becomeFirstResponder()
        }
        self.bottomOfCountryCode.constant = -self.view.frame.size.height/2
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
}
//MARK: - UITextField Delegate
extension AddBankDetailVC:UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        if textField == self.txtFirstName || textField == self.txtLastName{
            if let range = string.rangeOfCharacter(from: NSCharacterSet.letters){
                return true
            } else if let range = string.rangeOfCharacter(from: NSCharacterSet.whitespaces){
                return true
            } else if string == "" || string == "."{
                return true
            }else {
                return false
            }
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.txtFirstName{
            txtLastName.becomeFirstResponder()
        }else if textField == self.txtLastName{
            self.btnSelectDOB(0)
            //txtRNumber.becomeFirstResponder()
        }else if textField == self.txtRNumber{
            txtAcNumber.becomeFirstResponder()
        }else if textField == self.txtAcNumber{
            self.txtConfirmAcNumber.becomeFirstResponder()
        }else if textField == self.txtConfirmAcNumber{
            self.btnSelectCountry(0)
        }else if textField == self.txtPostalCode{
            self.txtSSNLast.becomeFirstResponder()
        }else{
            textField.resignFirstResponder()
        }
        return true
    }
    
}
// MARK: - UIPickerViewDataSource and UIPickerViewDelegate
extension AddBankDetailVC:UIPickerViewDelegate,UIPickerViewDataSource{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.arrCountry.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let dic = self.arrCountry[row]
        return dic["name"]
    }
}
//MARK: - Webservices
fileprivate extension AddBankDetailVC{
    
    func callWebserviceForAddBankDetail(){
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        objActivity.startActivityIndicator()
        
        var userDict = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfoDic) as? [String:Any] ?? ["":""]
        var businessID = userDict[UserDefaults.keys.business_id]as? String ?? ""
        let dob = userDict[UserDefaults.keys.dob]as? String ?? ""
        let country = userDict[UserDefaults.keys.country]as? String ?? ""
        let address = userDict[UserDefaults.keys.address]as? String ?? ""
        let city = userDict[UserDefaults.keys.city]as? String ?? ""
        /*
         business_id:11
         first_name:
         last_name:
         account_id:
         account_no:
         routing_number:
         dob:
         country:
         currency:
         address:
         city:
         postalCode:
         */
//        if let dictUser = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]{
//            if let busId = dictUser[UserDefaults.keys.userId] as? String{
//                userId = busId
//            }else if let busId = dictUser[UserDefaults.keys.userId] as? Int{
//                userId = String(busId)
//            }
//        }
        /*firstName:m
        lastName:rah
        country:gb
        currency:gbp
        accountNo:
        routingNumber:
        userId:54
        dob:07-07-1991
        address:
        city:
        postalCode:
        */
       
               
                
                
                
                
                
                
                
                
        let dicParam = ["first_name":self.txtFirstName.text!,
                        "last_name":self.txtLastName.text!,
                        "country":country,
                        "currency":"gbp",
                        "account_id":"",
                        "business_id":businessID,
                       // "userId":userId,
                        "routing_number":self.txtRNumber.text!,
                        "account_no":self.txtAcNumber.text!,
                        "dob":dob,
                        "address":address,
                        "city":city,
                        "postalCode":"SW1W 0NY"
        ]
        print(dicParam)
        
        objServiceManager.requestPost(strURL: WebURL.addBankDetail, params: dicParam, success: { response in
            if  response["status"] as! String == "success"{
                self.removePropertyData()
                if objAppShareData.editSelfServices == true{
                    self.navigationController?.popViewController(animated: true)
                }else{
                UserDefaults.standard.set("3", forKey: UserDefaults.keys.isDocument)
                self.gotoAddStaffVc()
                }
            }else{
                let msg = response["message"] as! String
                    objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
            }
        }) { error in
            // objAppShareData.showDeafultAlert(withTitle: message.error.title, message: message.error.wrong, on: self)
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    func removePropertyData(){
        let fetchRequest: NSFetchRequest<BusinessType> = BusinessType.fetchRequest()
        do {
            let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest as! NSFetchRequest<NSFetchRequestResult>)
            try managedContext?.execute(deleteRequest)
        } catch {
        }
    }
    
    
    
    
    func callWebserviceForGetBankDetail(){
        var userId = ""
        if let dictUser = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]{
            if let busId = dictUser[UserDefaults.keys.userId] as? String{
                userId = busId
            }else if let busId = dictUser[UserDefaults.keys.userId] as? Int{
                userId = String(busId)
            }
        }
        let param = ["userId":userId]
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        objActivity.startActivityIndicator()
        objServiceManager.requestPost(strURL: WebURL.bankAccountDetail, params:param, success: { response in
            objServiceManager.StopIndicator()
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else{
                let strStatus =  response["status"] as? String ?? ""
                if strStatus == k_success || strStatus == "sucess"{                    
                    if let dict = response["data"] as? [String:Any]{
                            if let FName = dict["firstName"] as? String{
                                self.txtFirstName.text = FName
                            }
                            if let LName = dict["lastName"] as? String{
                                self.txtLastName.text = LName
                            }
                            if let LName = dict["routingNumber"] as? String{
                                self.txtRNumber.text = LName
                            }else if let LName = dict["routingNumber"] as? Int{
                                self.txtRNumber.text = String(LName)
                            }
                        
                        if let LName = dict["accountNo"] as? String{
                            self.txtAcNumber.text = LName
                        }else if let LName = dict["accountNo"] as? Int{
                            self.txtAcNumber.text = String(LName)
                        }
                    }
                    
                    
                    
                 }else{
                    let msg = response["message"] as? String ?? ""
                 }
            }
        }){ error in
            objServiceManager.StopIndicator()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
}


//
//  AddCategoryTypeVC.swift
//  MualabBusiness
//
//  Created by Mac on 15/11/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import CoreData

class AddCategoryTypeVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    var managedContext:NSManagedObjectContext?

    fileprivate var arrBusinessType = [ModelAddBusinessType]()
    var cellSelectId = ""
    var businessId = ""
    //Rohit
    var strBusinessTypeID = ""
    var strBusinessTypeName = ""
    @IBOutlet weak var tblBusinessLIst:UITableView!
    @IBOutlet weak var lblNoDataFound:UIView!
var screenNo = 0
    
     @IBOutlet weak var btnContinue:UIButton!
    @IBOutlet weak var viewProgress:UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let userDict = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfoDic) as? [String:Any] ?? ["":""]
        self.businessId = userDict[UserDefaults.keys.business_id]as? String ?? ""
        self.cellSelectId = ""
        self.manageVCRedirection()
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        managedContext =
            appDelegate.persistentContainer.viewContext
        
        self.tblBusinessLIst.delegate = self
        self.tblBusinessLIst.dataSource = self
        self.tblBusinessLIst.reloadData()
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        self.cellSelectId = ""
        if objAppShareData.editSelfServices == true{
            self.viewProgress.isHidden = true
            self.btnContinue.setTitle("Save & Continue", for: .normal)
        }else{
            self.viewProgress.isHidden = false
            self.btnContinue.setTitle("Continue", for: .normal)
         }
        self.callWebserviceForGetServices()
    }
    
    func manageVCRedirection(){
        if screenNo == 4{
            screenNo = 0
            let sb = UIStoryboard(name:"SignUpProcess",bundle:Bundle.main)
            let objChooseType = sb.instantiateViewController(withIdentifier:"SearchAddCategoryTypeVC") as! SearchAddCategoryTypeVC
            objChooseType.idBusiness = self.businessId
            self.navigationController?.pushViewController(objChooseType, animated: false)
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
//MARK: - Webservices
fileprivate extension AddCategoryTypeVC{
  internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    //Rohit work on 13/Feb/2020
    if arrBusinessType.count != 0{
        let objSer = arrBusinessType[0]
        self.lblNoDataFound.isHidden = true
        return objSer.arrSubCategory.count
    }else{
        self.lblNoDataFound.isHidden = false
        return 0
    }
//        if arrBusinessType.count > 0{
//            self.lblNoDataFound.isHidden = true
//        }else{
//            self.lblNoDataFound.isHidden = false
//        }
//        return arrBusinessType.count
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BusinessTypeTableCell") as! BusinessTypeTableCell
        let objSer = arrBusinessType[0]
        print(objSer)
       // let objSer = arrBusinessType[indexPath.row]
        let objCategory = objSer.arrSubCategory[indexPath.row]
        
       // cell.lblServiceName.text = objSer.subServiceName.capitalized
        cell.lblServiceName.text = objCategory.subCategoryName.capitalized
        if objSer.status == "0"{
            cell.viewStatusManage.isHidden = false
        }else{
            cell.viewStatusManage.isHidden = true
        }
        
        if self.cellSelectId == objSer.id{
            cell.imgCheck.isHidden = true
            cell.lblServiceName.textColor = appColor
        }else{
            cell.imgCheck.isHidden = true
            cell.lblServiceName.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        }
        
        cell.btnChackUnchack.tag = indexPath.row
        cell.btnChackUnchack.addTarget(self, action:#selector(btnChackUnchackAction(sender:)) , for: .touchUpInside)
        
        return cell
    }
    
    @objc func btnChackUnchackAction(sender: UIButton!){
        self.view.endEditing(true)
        if self.arrBusinessType.count != 0{
            let objArr = self.arrBusinessType[0]
            let objMyEventsData = objArr.arrSubCategory[sender.tag]
            
            let indexPath = IndexPath(row:(sender as AnyObject).tag, section: (sender as AnyObject).superview!!.tag)
            let cell = (tblBusinessLIst?.cellForRow(at: indexPath) as? BusinessTypeTableCell)!
//            if objMyEventsData.status != "0"{
//            if self.cellSelectId == objMyEventsData.id{
//                self.cellSelectId = ""
//            }else{
//                self.cellSelectId = objMyEventsData.id
//            }
//        }
                if objMyEventsData.categoryStatus != "0"{
                if self.cellSelectId == objMyEventsData.subCategoryID{
                    self.cellSelectId = ""
                }else{
                    self.cellSelectId = objMyEventsData.subCategoryID
                }
            }
  
            objAppShareData.objModelFinalSubCategory.param[objAppShareData.objModelFinalSubCategory.categoryName] = objMyEventsData.subCategoryName
            objAppShareData.objModelFinalSubCategory.param[objAppShareData.objModelFinalSubCategory.strCategoryId] = objMyEventsData.subCategoryName
            
//objAppShareData.objModelFinalSubCategory.param[objAppShareData.objModelFinalSubCategory.categoryName] = objMyEventsData.subServiceName
//objAppShareData.objModelFinalSubCategory.param[objAppShareData.objModelFinalSubCategory.strCategoryId] = objMyEventsData.subServiceId
            

            
        self.tblBusinessLIst.reloadData()
        self.continueWork()
        }else{
            objAppShareData.showAlert(withMessage: "Your category request is pending", type: alertType.bannerDark,on: self)
        }
    }
    internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
//        let sb = UIStoryboard(name:"SignUpProcess",bundle:Bundle.main)
//        let objChooseType = sb.instantiateViewController(withIdentifier:"CategoriesVC") as! CategoriesVC
        //objChooseType.selectedIndex = indexPath.row
        //objChooseType.callOption = self.callOption
        // self.navigationController?.pushViewController(objChooseType, animated: true)
    }
    @available(iOS 11.0, *)

   internal func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
    {
        let Delete = UIContextualAction(style: .normal, title:  "", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            if self.arrBusinessType.count != 0{
              //  let objSer = self.arrBusinessType[indexPath.row]
                let objSer = self.arrBusinessType[0]
                let obj = objSer.arrSubCategory[indexPath.row]
                          // let businessIds = objSer.subServiceId
                           let categoryID = obj.subCategoryID
                           self.deleteCell(mainBookingId: self.businessId, categoryId: categoryID)
                           success(true)
            }
                       })
                       Delete.image = #imageLiteral(resourceName: "delete")
                       Delete.backgroundColor =  #colorLiteral(red: 0.9137254902, green: 0.2117647059, blue: 0.2274509804, alpha: 1)
                       return UISwipeActionsConfiguration(actions: [Delete])
            
           
    }

}

//MARK: - Webservices
fileprivate extension AddCategoryTypeVC{
    @IBAction func btnContinew(_ sender:Any){
        self.continueWork()
    }
    func continueWork(){
        if self.cellSelectId == ""{
            objAppShareData.showAlert(withMessage: "Please select any category", type: alertType.bannerDark,on: self)
        }else{
            let sb = UIStoryboard(name:"SignUpProcess",bundle:Bundle.main)
            let objChooseType = sb.instantiateViewController(withIdentifier:"ServicesListVC") as! ServicesListVC
            self.navigationController?.pushViewController(objChooseType, animated: true)
        }
    }
    
    @IBAction func btnBack(_ sender:Any){
        self.popToViewContruller()
    }
    
    @IBAction func btnForword(_ sender:Any){
    }
    
    @IBAction func btnLogout(_ sender:UIButton){
        appDelegate.logout()
    }
    
    @IBAction func btnAddBusinessType(_ sender:Any){
        let sb = UIStoryboard(name:"SignUpProcess",bundle:Bundle.main)
        let objChooseType = sb.instantiateViewController(withIdentifier:"SearchAddCategoryTypeVC") as! SearchAddCategoryTypeVC
        objChooseType.idBusiness = self.businessId
        objChooseType.businessTypeID = self.strBusinessTypeID
        objChooseType.strBusnTypNameForCategory = self.strBusinessTypeName
        self.navigationController?.pushViewController(objChooseType, animated: true)
    }
    
    @IBAction func btnAddCategoryType(_ sender:Any){
        let businessType = UserDefaults.standard.string(forKey: UserDefaults.keys.mainServiceType)
        
        let newObj = objAppShareData.objModelFinalSubCategory
        
        let businessName = newObj.param[newObj.bookingName]
        let CategoryName = newObj.param[newObj.categoryName]
        let businessId = newObj.param[newObj.strBusinessId]
        let CategoryId = newObj.param[newObj.strCategoryId]

        let a = objAppShareData.objModelFinalSubCategory
        a.param = ["":""]
        var strType = ""
        if businessType == "1"{
            strType = "Incall"
        }else if businessType == "2"{
            strType = "Outcall"
        }else{
            strType = "Both"
            //self.viewBookingType.isHidden = true
        }
        a.param[a.bookingType] = strType
        a.param[a.bookingName] = businessName
        a.param[a.categoryName] = CategoryName
        a.param[a.strBusinessId] = businessId
        a.param[a.strCategoryId] = CategoryId
        
        
        if self.cellSelectId == ""{
            objAppShareData.showAlert(withMessage: "Please select any category", type: alertType.bannerDark,on: self)
        }else{
            let sb = UIStoryboard(name:"SignUpProcess",bundle:Bundle.main)
            let objChooseType = sb.instantiateViewController(withIdentifier:"AddServicesVC") as! AddServicesVC
            objChooseType.fromCategory = true
            self.navigationController?.pushViewController(objChooseType, animated: true)
        }
    }
}

//MARK: - Webservices
fileprivate extension AddCategoryTypeVC{
    
    func callWebserviceForGetServices(){
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        objActivity.startActivityIndicator()
        self.arrBusinessType.removeAll()
        
        //Changes in
        let finalUrl = "\(WebURL.myBusinessTypeShow)\(businessId)"
       // let finalUrl = "\(WebURL.allBusinessTypeSearchByID)business_type=\(strBusinessTypeName)"
        print(finalUrl)
        
        objServiceManager.requestGetNew(strURL: finalUrl, params: nil, strCustomValidation: "", success: { response in
            self.arrBusinessType.removeAll()
            
            if  response["status"] as? String == "success"{
                //self.saveData(dict:response)
                if let data = response["businessTypes"] as? [[String:Any]]{
                    for obj in data{
                        let myObj = ModelAddBusinessType.init(dict: obj)
                        self.arrBusinessType.append(myObj)
                    }
                }
                self.tblBusinessLIst.reloadData()
            }else{
                self.tblBusinessLIst.reloadData()
                
            }
        }) { error in
            self.tblBusinessLIst.reloadData()
            // objAppShareData.showDeafultAlert(withTitle: message.error.title, message: message.error.wrong, on: self)
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
    //Rohit Woking on 13/Feb/2020
//    func callWebserviceForGetServices(){
//        if !objServiceManager.isNetworkAvailable(){
//            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
//            return
//        }
//        objActivity.startActivityIndicator()
//        let dict = ["categoryId":self.businessId]
//        objServiceManager.requestGet(strURL: WebURL.mysubCategoryBusinessId, params: dict as [String : AnyObject], success: { response in
//            self.arrBusinessType.removeAll()
//
//            if  response["status"] as? String == "success"{
//                //self.saveData(dict:response)
//                if let data = response["data"] as? [[String:Any]]{
//                    for obj in data{
//                        let myObj = ModelAddBusinessType.init(dict: obj)
//                        self.arrBusinessType.append(myObj)
//                    }
//                }
//                self.tblBusinessLIst.reloadData()
//            }else{
//                self.tblBusinessLIst.reloadData()
//
//            }
//        }) { error in
//            self.tblBusinessLIst.reloadData()
//// objAppShareData.showDeafultAlert(withTitle: message.error.title, message: message.error.wrong, on: self)
//            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
//        }
//    }
}
//MARK: - Navigation method
extension AddCategoryTypeVC{
    func popToViewContruller(){
        let controllers = self.navigationController?.viewControllers
        for vc in controllers! {
            if vc is AddBusinessTypeVC {
                _ = self.navigationController?.popToViewController(vc as! AddBusinessTypeVC, animated: true)
            }
        }
    }
    
    
    
    func deleteCell(mainBookingId:String,categoryId:String){
        let fetchRequest: NSFetchRequest<BusinessType> = BusinessType.fetchRequest()
        fetchRequest.predicate =  NSPredicate(format: "businessId = %@",mainBookingId)
        do {
            var allData = false
            let objArray = try managedContext?.fetch(fetchRequest)
            if (objArray?.count)! > 0{
                for object in objArray!{
                    if object.businessId == mainBookingId {
                        let fetchRequestCategory: NSFetchRequest<CategoryType> = CategoryType.fetchRequest()
                        fetchRequestCategory.predicate =  NSPredicate(format: "categoryId = %@",mainBookingId)
                        
                        let arrCategory = Array(object.mutableSetValue(forKey: "categorytypes")) as? [CategoryType] ?? []
                        
                        if (arrCategory.count) == 1{
                            managedContext?.delete(object)
                        }else{
                            for objCat in arrCategory{
                                if objCat.categoryId == categoryId{
                                    managedContext?.delete(objCat)
                                 }
                            }
                        }
 
                    }
                }
            }
            try managedContext?.save()
            //callWebservicefor delete business type and category
            self.callWebserviceDeleteServices(Ids: categoryId)

        } catch _ {
            // error handling
        }
    }
    
    
    
    
    
    func callWebserviceDeleteServices(Ids:String){
        if !objServiceManager.isNetworkAvailable(){
        objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
        return
        }
    objActivity.startActivityIndicator()
   // let dict = ["id":Ids]
        let dict = ["business_id":businessId,"category_id":Ids]
    objServiceManager.requestPost(strURL: WebURL.deleteCategory, params: dict as [String : AnyObject], success: { response in
    self.arrBusinessType.removeAll()
    
    if  response["status"] as? String == "success"{
        self.callWebserviceForGetServices()
    }else{
        self.tblBusinessLIst.reloadData()
    }
    }) { error in
    self.tblBusinessLIst.reloadData()
     objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
    }
    }
}


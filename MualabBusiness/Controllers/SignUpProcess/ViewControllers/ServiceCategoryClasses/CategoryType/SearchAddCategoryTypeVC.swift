//
//  SearchAddCategoryTypeVC.swift
//  MualabBusiness
//
//  Created by Mac on 15/11/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

class SearchAddCategoryTypeVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {
    @IBOutlet weak var indicate: UIActivityIndicatorView!
    @IBOutlet weak var btnContinue:UIButton!

    
    
    fileprivate var arrBusinessType = [ModelAddBusinessType]()
    var idBusiness = ""
    var businessTypeID = ""
    var cellSelectId = ""
    //Rohit
    var strBusnTypNameForCategory = ""
    fileprivate var strText = ""
    @IBOutlet weak var tblBusinessLIst:UITableView!
    @IBOutlet weak var ViewNoDataFound:UIView!
    @IBOutlet weak var txtSearchName:UITextField!
    @IBOutlet weak var lblNoDataFound:UIView!
    @IBOutlet weak var btnContinew:UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.indicate.stopAnimating()

        self.txtSearchName.delegate = self
        self.tblBusinessLIst.delegate = self
        self.tblBusinessLIst.dataSource = self
        self.cellSelectId = ""
        self.tblBusinessLIst.reloadData()
        
 
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        self.cellSelectId = ""
        self.indicate.stopAnimating()
        if objAppShareData.editSelfServices == true{
            self.btnContinew.setTitle("Save & Continue", for: .normal)
        }else{
            self.btnContinew.setTitle("Continue", for: .normal)
        }
       // self.callWebserviceForGetServices(param: ["search":self.strText,"categoryId":self.idBusiness])
         self.callWebserviceForGetServices(strSearch: "", strBusinessID: self.idBusiness, strBusinessType: "")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
extension SearchAddCategoryTypeVC:UIScrollViewDelegate{
    func scrollViewDidScroll(_ scrollView: UIScrollView){
        self.view.endEditing(true)
    }
}
//MARK: - Webservices
fileprivate extension SearchAddCategoryTypeVC{
    
    
    @IBAction func btnBack(_ sender:Any){
        self.backToLast()
    }
    @IBAction func btnForword(_ sender:Any){
    }
    @IBAction func btnLogout(_ sender:UIButton){
        appDelegate.logout()
    }
    func backToLast(){
        self.navigationController?.popViewController(animated: true)
    }
    func backToNewVC(){
//        let sb = UIStoryboard(name:"SignUpProcess",bundle:Bundle.main)
//        let objChooseType = sb.instantiateViewController(withIdentifier:"AddCategoryTypeVC") as! AddCategoryTypeVC
//        objChooseType.businessId = self.idBusiness
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnContinew(_ sender:Any){
        self.continueWork()

    }
    func continueWork()  {
        var name = ""
        if self.ViewNoDataFound.isHidden == false{
            if self.txtSearchName.text == "" {
                objAppShareData.showAlert(withMessage: "Please enter any category", type: alertType.bannerDark,on: self)
            }else{
                name = self.txtSearchName.text ?? ""
            }
        }else{
            
            if self.cellSelectId == "" && self.txtSearchName.text != "" {
                name = self.txtSearchName.text ?? ""
            }else if self.cellSelectId != ""{
                for obj in self.arrBusinessType{
                    //Rohit
                    for subObj in obj.arrSubCategory{
                        if self.cellSelectId == subObj.subCategoryID{
                            name = subObj.subCategoryName
                            //name = obj.title
                        }
                    }
                    
                }
            }else if self.txtSearchName.text != ""  {
                name = self.txtSearchName.text ?? ""
            }else{
                objAppShareData.showAlert(withMessage: "Please select any category", type: alertType.bannerDark,on: self)
            }
        }
            if name != ""{
                self.callWebserviceForAddBusiness(param: ["category":name,"business_id":self.idBusiness,"business_type_id":self.businessTypeID])
            }
        
    }
    @IBAction func btnAdd(_ sender:Any){
        
        self.txtSearchName.text = txtSearchName.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        if (self.txtSearchName.text?.count)! > 0 {
            let str = self.txtSearchName.text ?? ""
            self.txtSearchName.text = ""
            self.callWebserviceForAddBusiness(param: ["category":str,"business_id":self.idBusiness,"business_type_id":self.businessTypeID])
        }
    }
}

// MARK: - UITextfield Delegate
extension SearchAddCategoryTypeVC{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        
        let text = textField.text! as NSString
        
        if (text.length == 1)  && (string == "") {
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
            self.strText = ""
            getArtistDataWith(andSearchText: "")
        }else{
            var substring: String = textField.text!
            substring = (substring as NSString).replacingCharacters(in: range, with: string)
            substring = substring.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            let a = substring.count
            if a > 50 && substring != ""{
                return false
            }else{
                searchAutocompleteEntries(withSubstring: substring)
            }
         }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
        self.strText = ""
        getArtistDataWith( andSearchText: "")
        return true
    }
    
 
    // MARK: - searching operation
    func searchAutocompleteEntries(withSubstring substring: String) {
        if substring != "" {
            self.strText = substring
            // to limit network activity, reload half a second after last key press.
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
            self.perform(#selector(self.reload), with: nil, afterDelay: 0.5)
        }
    }
    
    @objc func reload() {
        self.getArtistDataWith( andSearchText: strText)
    }
    
    func getArtistDataWith(andSearchText: String) {
     //   self.callWebserviceForGetServices(param: ["search": andSearchText.lowercased() ,"categoryId":self.idBusiness])
        print(andSearchText.uppercased())
        print(andSearchText.capitalized)
        
        self.callWebserviceForGetServices(strSearch: andSearchText.capitalized, strBusinessID: self.idBusiness, strBusinessType: "")
     }
}

//MARK: - Webservices
  extension SearchAddCategoryTypeVC{
    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       // DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(100)) {
        self.btnContinew.isHidden = false

        if arrBusinessType.count != 0{
            if self.arrBusinessType.count > 0{
                 self.ViewNoDataFound.isHidden = true
                 self.lblNoDataFound.isHidden = true

             }else if self.strText == ""{
                 self.ViewNoDataFound.isHidden = true
                 self.lblNoDataFound.isHidden = false
                 self.btnContinew.isHidden = true

             }else if self.strText != ""{
                 self.ViewNoDataFound.isHidden = false
                 self.lblNoDataFound.isHidden = true
                 self.btnContinew.isHidden = true
             }else{
                 self.ViewNoDataFound.isHidden = true
                 self.lblNoDataFound.isHidden = false
             }
                 
            // }
            let arrObj = arrBusinessType[0]
            return arrObj.arrSubCategory.count
            // return arrBusinessType.count
        }else{
            if self.arrBusinessType.count > 0{
                self.ViewNoDataFound.isHidden = true
                self.lblNoDataFound.isHidden = true

            }else if self.strText == ""{
                self.ViewNoDataFound.isHidden = true
                self.lblNoDataFound.isHidden = false
                self.btnContinew.isHidden = true

            }else if self.strText != ""{
                self.ViewNoDataFound.isHidden = false
                self.lblNoDataFound.isHidden = true
                self.btnContinew.isHidden = true
            }else{
                self.ViewNoDataFound.isHidden = true
                self.lblNoDataFound.isHidden = false
            }
            return 0
        }
        
        
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BusinessTypeTableCell") as! BusinessTypeTableCell
       //Rohit work on 13/Feb/2020
        let arrObj = arrBusinessType[0]
        let objSer = arrObj.arrSubCategory[indexPath.row]
       // let objSer = arrBusinessType[indexPath.row]
       // cell.lblServiceName.text = objSer.title
        cell.lblServiceName.text = objSer.subCategoryName
        
//        if self.cellSelectId == objSer.id{
//            cell.imgCheck.isHidden = false
//            cell.lblServiceName.textColor = appColor
//        }else{
//            cell.imgCheck.isHidden = true
//            cell.lblServiceName.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
//        }
        
        if self.cellSelectId == objSer.subCategoryID{
            cell.imgCheck.isHidden = false
            cell.lblServiceName.textColor = appColor
        }else{
            cell.imgCheck.isHidden = true
            cell.lblServiceName.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        }
        
        cell.btnChackUnchack.tag = indexPath.row
        cell.btnChackUnchack.addTarget(self, action:#selector(btnChackUnchackAction(sender:)) , for: .touchUpInside)
        
        return cell
    }
    
    @objc func btnChackUnchackAction(sender: UIButton!){
        self.view.endEditing(true)
        if self.arrBusinessType.count != 0{
            // let objMyEventsData = self.arrBusinessType[sender.tag]
            let objArr = self.arrBusinessType[0]
            let objMyEventsData = objArr.arrSubCategory[sender.tag]
            let indexPath = IndexPath(row:(sender as AnyObject).tag, section: (sender as AnyObject).superview!!.tag)
            let cell = (tblBusinessLIst?.cellForRow(at: indexPath) as? BusinessTypeTableCell)!
            
            if self.cellSelectId == objMyEventsData.subCategoryID{
                self.cellSelectId = ""
            }else{
                self.cellSelectId = objMyEventsData.subCategoryID
            }
            self.tblBusinessLIst.reloadData()
            self.continueWork()
        }
    }
    
    
    internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
//        let sb = UIStoryboard(name:"SignUpProcess",bundle:Bundle.main)
//        let objChooseType = sb.instantiateViewController(withIdentifier:"CategoriesVC") as! CategoriesVC
        //objChooseType.selectedIndex = indexPath.row
        //objChooseType.callOption = self.callOption
        // self.navigationController?.pushViewController(objChooseType, animated: true)
    }
}
//MARK: - Webservices
fileprivate extension SearchAddCategoryTypeVC{
    
    
    func callWebserviceForGetServices(strSearch: String, strBusinessID: String, strBusinessType: String){
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        self.indicate.startAnimating()
        
        let dictParamn = ["category":strSearch,"business_type":strBusinessType,"business_id":strBusinessID]as [String : AnyObject]
    
        objServiceManager.requestGetNew(strURL: WebURL.searchAndAddCategory, params: dictParamn, strCustomValidation: "", success: { response in
            self.arrBusinessType.removeAll()
            if  response["status"] as? String == "success"{
                //self.saveData(dict:response)
                if let data = response["businessTypes"] as? [[String:Any]]{
                    for obj in data{
                        let myObj = ModelAddBusinessType.init(dict: obj)
                        self.arrBusinessType.append(myObj)
                    }
                }
                self.tblBusinessLIst.reloadData()
                    self.indicate.stopAnimating()
    
                }else{
                    self.tblBusinessLIst.reloadData()
                    self.indicate.stopAnimating()
    
                }
            }) { error in
                self.tblBusinessLIst.reloadData()
                self.indicate.stopAnimating()
    
                // objAppShareData.showDeafultAlert(withTitle: message.error.title, message: message.error.wrong, on: self)
                objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
            }
        }
    
    //Rohit work on 13/Feb/2020
//    func callWebserviceForGetServices(param:[String:Any]){
//        if !objServiceManager.isNetworkAvailable(){
//            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
//            return
//        }
//        //objActivity.startActivityIndicator()
//        self.indicate.startAnimating()
//
//
//        let dict = ["categoryId":self.idBusiness,"search":self.strText]
//        objServiceManager.requestGetNew(strURL: WebURL.allSubcategorySearchBusinessId, params: dict as [String : AnyObject], strCustomValidation: "", success: { response in
//            self.arrBusinessType.removeAll()
//            if  response["status"] as? String == "success"{
//                //self.saveData(dict:response)
//                if let data = response["data"] as? [[String:Any]]{
//                    for obj in data{
//                        let myObj = ModelAddBusinessType.init(dict: obj)
//                        self.arrBusinessType.append(myObj)
//                    }
//                }
//                self.tblBusinessLIst.reloadData()
//                self.indicate.stopAnimating()
//
//            }else{
//                self.tblBusinessLIst.reloadData()
//                self.indicate.stopAnimating()
//
//            }
//        }) { error in
//            self.tblBusinessLIst.reloadData()
//            self.indicate.stopAnimating()
//
//            // objAppShareData.showDeafultAlert(withTitle: message.error.title, message: message.error.wrong, on: self)
//            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
//        }
//    }
    
    
    func callWebserviceForAddBusiness(param:[String:Any]){
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        //   objActivity.startActivityIndicator()
        self.indicate.startAnimating()

        objServiceManager.requestPost(strURL: WebURL.addSubCategory, params: param as [String : AnyObject], success: { response in
            // self.arrBusinessType.removeAll()
            self.indicate.stopAnimating()

            if  response["status"] as? String == "success"{
                let msg = response["message"] as? String ?? ""
                self.backToNewVC()
                objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
            }else{
                let msg = response["message"] as? String ?? ""
                
                objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                //self.callWebserviceForGetServices(param:[:])
                 self.callWebserviceForGetServices(strSearch: "", strBusinessID: "", strBusinessType: "")
                self.tblBusinessLIst.reloadData()
            }
        }) { error in
            self.tblBusinessLIst.reloadData()
            // objAppShareData.showDeafultAlert(withTitle: message.error.title, message: message.error.wrong, on: self)
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
}


extension String {
  func replace(string:String, replacement:String) -> String {
      return self.replacingOccurrences(of: string, with: replacement, options: NSString.CompareOptions.literal, range: nil)
  }

  func removeWhitespace() -> String {
      return self.replace(string: " ", replacement: "")
  }
}

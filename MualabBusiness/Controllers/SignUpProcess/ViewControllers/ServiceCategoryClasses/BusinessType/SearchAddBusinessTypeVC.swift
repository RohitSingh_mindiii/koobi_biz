//
//  SearchAddBusinessTypeVC.swift
//  MualabBusiness
//
//  Created by Mac on 15/11/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

class SearchAddBusinessTypeVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {
    
    @IBOutlet weak var btnContinew:UIButton!

    @IBOutlet weak var indicate: UIActivityIndicatorView!
    fileprivate var arrBusinessType = [ModelAddBusinessType]()
    var cellSelectId = ""
    @IBOutlet weak var tblBusinessLIst:UITableView!
    @IBOutlet weak var ViewNoDataFound:UIView!
    @IBOutlet weak var txtSearchName:UITextField!
    @IBOutlet weak var lblNoDataFound:UIView!
    fileprivate var strText = ""
    
    var strGetBusinessID = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.indicate.stopAnimating()
        self.txtSearchName.delegate = self
        self.tblBusinessLIst.delegate = self
        self.tblBusinessLIst.dataSource = self
        self.cellSelectId = ""
        self.tblBusinessLIst.reloadData()
        let userDict = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfoDic) as? [String:Any] ?? ["":""]
        strGetBusinessID = userDict[UserDefaults.keys.business_id] as! String
        print(strGetBusinessID)
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        self.cellSelectId = ""
        self.indicate.stopAnimating()
        
        if objAppShareData.editSelfServices == true{
            self.btnContinew.setTitle("Save & Continue", for: .normal)
        }else{
            self.btnContinew.setTitle("Continue", for: .normal)
        }
        self.callWebserviceForGetServices(str: strText)
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 }
//MARK: - Webservices
fileprivate extension SearchAddBusinessTypeVC{
    
    
    @IBAction func btnBack(_ sender:Any){
        objAppShareData.fromDeleteButton = true
        self.view.endEditing(true)
        self.backToLast()
    }
    
    func backToLast(){
      self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnForword(_ sender:Any){
        
    }
    
    @IBAction func btnContinew(_ sender:Any){
        self.continueWork()
    }
    func continueWork(){
        self.view.endEditing(true)
        var name = ""
        if self.ViewNoDataFound.isHidden == false{
            if self.txtSearchName.text == "" {
                objAppShareData.showAlert(withMessage: "Please select any business type", type: alertType.bannerDark,on: self)
            }else{
                name = self.txtSearchName.text ?? ""
            }
        }else{
            
            if self.cellSelectId == "" && self.txtSearchName.text != "" {
                name = self.txtSearchName.text ?? ""
            }else if self.cellSelectId != ""{
                for obj in self.arrBusinessType{
                    if self.cellSelectId == obj.id{
                        name = obj.title
                    }
                }
            }else if self.txtSearchName.text != ""  {
                name = self.txtSearchName.text ?? ""
            }else{
                objAppShareData.showAlert(withMessage: "Please select any business type", type: alertType.bannerDark,on: self)
            }
        }
            if name != ""{
                self.callWebserviceForAddBusiness(param: ["business_type":name, "business_id":strGetBusinessID])
            }
        
    }
    @IBAction func btnAdd(_ sender:Any){
        
        self.txtSearchName.text = txtSearchName.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        if (self.txtSearchName.text?.count)! > 0 {
            let str = self.txtSearchName.text ?? ""
            self.txtSearchName.text = ""
        self.callWebserviceForAddBusiness(param: ["business_type":str, "business_id":strGetBusinessID])
        }
    }
}


// MARK: - UITextfield Delegate
extension SearchAddBusinessTypeVC{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        
        let text = textField.text! as NSString
        
        if (text.length == 1)  && (string == "") {
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
            self.strText = ""
            getArtistDataWith(andSearchText: "")
        }else{
            var substring: String = textField.text!
            substring = (substring as NSString).replacingCharacters(in: range, with: string)
            substring = substring.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            let a = substring.count
            if a > 50 && substring != ""{
                return false
            }else{
                searchAutocompleteEntries(withSubstring: substring)
            }
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
        self.strText = ""
        getArtistDataWith( andSearchText: "")
        return true
    }
    
    // MARK: - searching operation
    func searchAutocompleteEntries(withSubstring substring: String) {
        if substring != "" {
            self.strText = substring
            // to limit network activity, reload half a second after last key press.
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
            self.perform(#selector(self.reload), with: nil, afterDelay: 0.5)
        }
    }
    
    @objc func reload() {
        
        self.getArtistDataWith( andSearchText: strText)
    }
    
    func getArtistDataWith(andSearchText: String) {
        var  parameters = [ "search": andSearchText.lowercased() ]
        //self.callWebserviceForGetServices(param: parameters)
        self.callWebserviceForGetServices(str: strText)
    }
}
//MARK: - Webservices
 extension SearchAddBusinessTypeVC{
    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.btnContinew.isHidden = false
        
        if self.arrBusinessType.count > 0{
            self.ViewNoDataFound.isHidden = true
            self.lblNoDataFound.isHidden = true
            
        }else if self.strText == ""{
            self.ViewNoDataFound.isHidden = true
            self.lblNoDataFound.isHidden = false
            self.btnContinew.isHidden = true
            
        }else if self.strText != ""{
            self.ViewNoDataFound.isHidden = false
            self.lblNoDataFound.isHidden = true
            self.btnContinew.isHidden = true
        }else{
            self.ViewNoDataFound.isHidden = true
            self.lblNoDataFound.isHidden = false
        }
        return arrBusinessType.count
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BusinessTypeTableCell") as! BusinessTypeTableCell
        let objSer = arrBusinessType[indexPath.row]
        cell.lblServiceName.text = objSer.title
        
      
        if self.cellSelectId == objSer.id{
            cell.imgCheck.isHidden = false
            cell.lblServiceName.textColor = appColor
        }else{
            cell.imgCheck.isHidden = true
            cell.lblServiceName.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)

        }
        
       
        
        cell.btnChackUnchack.tag = indexPath.row
        cell.btnChackUnchack.addTarget(self, action:#selector(btnChackUnchackAction(sender:)) , for: .touchUpInside)
        
        return cell
    }
    
    @objc func btnChackUnchackAction(sender: UIButton!){
        self.view.endEditing(true)
        let objMyEventsData = self.arrBusinessType[sender.tag]
        
        let indexPath = IndexPath(row:(sender as AnyObject).tag, section: (sender as AnyObject).superview!!.tag)
        let cell = (tblBusinessLIst?.cellForRow(at: indexPath) as? BusinessTypeTableCell)!
        
        if self.cellSelectId == objMyEventsData.id{
            self.cellSelectId = ""
        }else{
            self.cellSelectId = objMyEventsData.id
        }
        self.tblBusinessLIst.reloadData()
        self.continueWork()

    }
    internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
//        let sb = UIStoryboard(name:"SignUpProcess",bundle:Bundle.main)
//        let objChooseType = sb.instantiateViewController(withIdentifier:"CategoriesVC") as! CategoriesVC
        //objChooseType.selectedIndex = indexPath.row
        //objChooseType.callOption = self.callOption
        // self.navigationController?.pushViewController(objChooseType, animated: true)
    }
}
//MARK: - Webservices
fileprivate extension SearchAddBusinessTypeVC{
    
    //Rohit Work on 12/feb/2020
//    func callWebserviceForGetServices(param:[String:Any]){
//        if !objServiceManager.isNetworkAvailable(){
//            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
//            return
//        }
//     //   objActivity.startActivityIndicator()
//        self.indicate.startAnimating()
//        objServiceManager.requestGet(strURL: WebURL.allBusinessTypeSearch, params: param as [String : AnyObject], success: { response in
//            self.arrBusinessType.removeAll()
//            if  response["status"] as? String == "success"{
//                //self.saveData(dict:response)
//                if let data = response["data"] as? [[String:Any]]{
//                    for obj in data{
//                        let myObj = ModelAddBusinessType.init(dict: obj)
//                        self.arrBusinessType.append(myObj)
//                    }
//                }
//                self.tblBusinessLIst.reloadData()
//                self.indicate.stopAnimating()
//            }else{
//                self.tblBusinessLIst.reloadData()
//                self.indicate.stopAnimating()
//
//            }
//        }) { error in
//            self.tblBusinessLIst.reloadData()
//            self.indicate.stopAnimating()
//            self.callWebserviceForGetServices(param: ["search":""])
//
//            // objAppShareData.showDeafultAlert(withTitle: message.error.title, message: message.error.wrong, on: self)
//            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
//        }
//    }

    
    func callWebserviceForGetServices(str:String){
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        
        let finalUrl = "\(WebURL.allBusinessTypeSearchByType)business_type=\(str)"
        print(finalUrl)
     
        self.indicate.startAnimating()
        objServiceManager.requestGetNew(strURL: finalUrl, params: nil, strCustomValidation: "", success: { response in
            self.arrBusinessType.removeAll()
            if  response["status"] as? String == "success"{
                //self.saveData(dict:response)
                if let data = response["businessTypes"] as? [[String:Any]]{
                    for obj in data{
                        let myObj = ModelAddBusinessType.init(dict: obj)
                        self.arrBusinessType.append(myObj)
                    }
                }
                self.tblBusinessLIst.reloadData()
                self.indicate.stopAnimating()
            }else{
                self.tblBusinessLIst.reloadData()
                self.indicate.stopAnimating()

            }
        }) { error in
            self.tblBusinessLIst.reloadData()
            self.indicate.stopAnimating()
           // self.callWebserviceForGetServices(param: ["search":""])
           // self.callWebserviceForGetServices(str: self.strText)
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
    
    func callWebserviceForAddBusiness(param:[String:Any]){
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        //   objActivity.startActivityIndicator()
        
        objServiceManager.requestPost(strURL: WebURL.addBusinessType, params: param as [String : AnyObject], success: { response in
           // self.arrBusinessType.removeAll()
            if  response["status"] as? String == "success"{
                let msg = response["message"] as? String ?? ""
                self.backToLast()
                objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
            }else{
                let msg = response["message"] as? String ?? ""
                objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                
               // self.callWebserviceForGetServices(param:[:])
                self.callWebserviceForGetServices(str: self.strText)
                self.tblBusinessLIst.reloadData()
             }
        }) { error in
            self.tblBusinessLIst.reloadData()
            // objAppShareData.showDeafultAlert(withTitle: message.error.title, message: message.error.wrong, on: self)
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
}
extension SearchAddBusinessTypeVC:UIScrollViewDelegate{
    func scrollViewDidScroll(_ scrollView: UIScrollView){
        self.view.endEditing(true)
    }
}

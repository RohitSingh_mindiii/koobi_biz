//
//  AddBusinessTypeVC.swift
//  MualabBusiness
//
//  Created by Mac on 15/11/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import CoreData

class AddBusinessTypeVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    var managedContext:NSManagedObjectContext?

    fileprivate var arrBusinessType = [ModelAddBusinessType]()
    var callOption: Int = 0
    var screenNo = 0
    @IBOutlet weak var tblBusinessLIst:UITableView!
    @IBOutlet weak var lblNoDataFound:UIView!
    var cellSelectId = ""
 
    @IBOutlet weak var btnSkip:UIButton!
    @IBOutlet weak var btnContinue:UIButton!
    @IBOutlet weak var viewProgress:UIView!
    var fromBusinessAdminVC = false
    var addBusiness = false
    var strGetBusinessID = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        objAppShareData.fromDeleteButton = false
        
        let userDict = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfoDic) as? [String:Any] ?? ["":""]
        strGetBusinessID = userDict[UserDefaults.keys.business_id] as! String
        
        
        if fromBusinessAdminVC == true{
            self.fromBusinessAdminVC = false
            let sb = UIStoryboard(name:"SignUpProcess",bundle:Bundle.main)
            let objChooseType = sb.instantiateViewController(withIdentifier:"ServicesListVC") as! ServicesListVC
            self.navigationController?.pushViewController(objChooseType, animated: false)
        }
        if addBusiness == true{
            let sb = UIStoryboard(name:"SignUpProcess",bundle:Bundle.main)
            let objChooseType = sb.instantiateViewController(withIdentifier:"SearchAddBusinessTypeVC") as! SearchAddBusinessTypeVC
            self.navigationController?.pushViewController(objChooseType, animated: false)
    }
        self.tblBusinessLIst.delegate = self
        self.tblBusinessLIst.dataSource = self
        self.manageVCRedirection()
        self.cellSelectId = ""
        self.tblBusinessLIst.reloadData()
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        managedContext =
            appDelegate.persistentContainer.viewContext
         // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.cellSelectId = ""

        if fromBusinessAdminVC == true{
           self.fromBusinessAdminVC = false
            let sb = UIStoryboard(name:"SignUpProcess",bundle:Bundle.main)
            let objChooseType = sb.instantiateViewController(withIdentifier:"ServicesListVC") as! ServicesListVC
            self.navigationController?.pushViewController(objChooseType, animated: false)
        }else{
        if objAppShareData.editSelfServices == true{
            self.viewProgress.isHidden = true
            self.btnSkip.isHidden = true
            self.btnContinue.setTitle("Save & Continue", for: .normal)
        }else{
            self.viewProgress.isHidden = false
            self.btnSkip.isHidden = false
            self.btnContinue.setTitle("Continue", for: .normal)
            UserDefaults.standard.setValue("3", forKey: UserDefaults.keys.navigationVC)
        }
        self.callWebserviceForGetServices()
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func manageVCRedirection(){
        if screenNo == 1{
            let sb = UIStoryboard(name:"SignUpProcess",bundle:Bundle.main)
            let objChooseType = sb.instantiateViewController(withIdentifier:"SearchAddBusinessTypeVC") as! SearchAddBusinessTypeVC
            self.screenNo = 0
            self.navigationController?.pushViewController(objChooseType, animated: false)
        }else if screenNo == 4{
            let sb = UIStoryboard(name:"SignUpProcess",bundle:Bundle.main)
            let objChooseType = sb.instantiateViewController(withIdentifier:"AddCategoryTypeVC") as! AddCategoryTypeVC
            objChooseType.businessId = self.cellSelectId
            objChooseType.screenNo = self.screenNo
            self.screenNo = 0
            self.navigationController?.pushViewController(objChooseType, animated: false)
        }
    }
 }

//MARK: - Webservices
 extension AddBusinessTypeVC{
    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrBusinessType.count > 0{
            self.lblNoDataFound.isHidden = true
        }else{
            self.lblNoDataFound.isHidden = false
         }
        return arrBusinessType.count
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BusinessTypeTableCell") as! BusinessTypeTableCell
        let objSer = arrBusinessType[indexPath.row]
        cell.lblServiceName.text = objSer.serviceName.capitalized
        if objSer.status == "0"{
            cell.viewStatusManage.isHidden = false
            cell.viewAddCategory.isHidden = true
        }else{
            cell.viewStatusManage.isHidden = true
            cell.viewAddCategory.isHidden = false
        }
        if self.cellSelectId == objSer.serviceId{
            cell.imgCheck.isHidden = true
            cell.lblServiceName.textColor = appColor
        }else{
            cell.imgCheck.isHidden = true
            cell.lblServiceName.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        }

        cell.btnChackUnchack.tag = indexPath.row
        cell.btnChackUnchack.addTarget(self, action:#selector(btnChackUnchackAction(sender:)) , for: .touchUpInside)
        return cell
    }
    
    internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){

        
    }
    
    @available(iOS 11.0, *)
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
    {
        let Delete = UIContextualAction(style: .normal, title:  "", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            let objSer = self.arrBusinessType[indexPath.row]
          //  let businessId = objSer.serviceId
            let businessId = objSer.id
            self.deleteCell(mainBookingId: businessId)
             success(true)
        })
        Delete.image = #imageLiteral(resourceName: "delete")
        Delete.backgroundColor =  #colorLiteral(red: 0.9137254902, green: 0.2117647059, blue: 0.2274509804, alpha: 1)
        return UISwipeActionsConfiguration(actions: [Delete])
    }
    
    
    @objc func btnChackUnchackAction(sender: UIButton!){
        self.view.endEditing(true)
        let objMyEventsData = self.arrBusinessType[sender.tag]
        
        let indexPath = IndexPath(row:(sender as AnyObject).tag, section: (sender as AnyObject).superview!!.tag)
        let cell = (tblBusinessLIst?.cellForRow(at: indexPath) as? BusinessTypeTableCell)!
        
        if self.cellSelectId == objMyEventsData.serviceId{
            self.cellSelectId = ""
        }else{
            self.cellSelectId = objMyEventsData.serviceId
        }
        
        objAppShareData.objModelFinalSubCategory.param[objAppShareData.objModelFinalSubCategory.bookingName] = objMyEventsData.serviceName
        objAppShareData.finalServiceTypeName = objMyEventsData.serviceName 
       // objAppShareData.objModelFinalSubCategory.param[objAppShareData.objModelFinalSubCategory.strBusinessId] = objMyEventsData.serviceId
        objAppShareData.objModelFinalSubCategory.param[objAppShareData.objModelFinalSubCategory.strBusinessId] = objMyEventsData.id
        
         self.tblBusinessLIst.reloadData()
      //  self.continueWork()
        if screenNo == 5{
            let sb = UIStoryboard(name:"SignUpProcess",bundle:Bundle.main)
            let objChooseType = sb.instantiateViewController(withIdentifier:"SearchAddCategoryTypeVC") as! SearchAddCategoryTypeVC
            objChooseType.idBusiness = self.cellSelectId
            objChooseType.businessTypeID = objMyEventsData.id
            self.navigationController?.pushViewController(objChooseType, animated: true)
        }else{
            let sb = UIStoryboard(name:"SignUpProcess",bundle:Bundle.main)
            let objChooseType = sb.instantiateViewController(withIdentifier:"AddCategoryTypeVC") as! AddCategoryTypeVC
            //objChooseType.businessId = self.cellSelectId
            objChooseType.strBusinessTypeID = objMyEventsData.id
            objChooseType.strBusinessTypeName = objMyEventsData.serviceName
            
            objChooseType.screenNo = 0
            self.navigationController?.pushViewController(objChooseType, animated: true)
      }
    }
}
//MARK: - Webservices
fileprivate extension AddBusinessTypeVC{
    
    
    @IBAction func btnBack(_ sender:Any){
        
        var navigationCheck = false
        if objAppShareData.editSelfServices == true{
            self.navigationController?.popViewController(animated: true)
        }else{
        let controllers = self.navigationController?.viewControllers
        for vc in controllers! {
            if vc is  AddBusinessDetailVC{
                navigationCheck = true
                _ = self.navigationController?.popToViewController(vc as! AddBusinessDetailVC, animated: true)
            }
        }
        
        if navigationCheck == false{
            let sb = UIStoryboard(name:"SignUpProcess",bundle:Bundle.main)
            let objChooseType = sb.instantiateViewController(withIdentifier:"AddBusinessDetailVC") as! AddBusinessDetailVC
            self.navigationController?.pushViewController(objChooseType, animated: false)
        }
        }
     }
    @IBAction func btnForword(_ sender:Any){
 
    }
    
    @IBAction func btnAddBusinessType(_ sender:Any){
        let sb = UIStoryboard(name:"SignUpProcess",bundle:Bundle.main)
        let objChooseType = sb.instantiateViewController(withIdentifier:"SearchAddBusinessTypeVC") as! SearchAddBusinessTypeVC
         self.navigationController?.pushViewController(objChooseType, animated: true)
    }
    @IBAction func btnAddCategoryType(_ sender:Any){
       
        self.addCategory()
    }
    func addCategory(){
        if self.arrBusinessType.count == 0{
            objAppShareData.showAlert(withMessage: "Please add business type", type: alertType.bannerDark,on: self)
        }else if self.cellSelectId == ""{
            objAppShareData.showAlert(withMessage: "Please select any business type", type: alertType.bannerDark,on: self)
        }else{
            let sb = UIStoryboard(name:"SignUpProcess",bundle:Bundle.main)
            let objChooseType = sb.instantiateViewController(withIdentifier:"SearchAddCategoryTypeVC") as! SearchAddCategoryTypeVC
            objChooseType.idBusiness = self.cellSelectId
             self.navigationController?.pushViewController(objChooseType, animated: true)
        }
    }
    @IBAction func btnContinew(_ sender:Any){
        self.continueWork()
    }
    func continueWork(){
        if self.arrBusinessType.count == 0{
              objAppShareData.showAlert(withMessage: "Please add business type", type: alertType.bannerDark,on: self)
        }else if self.cellSelectId == ""{
           
            objAppShareData.showAlert(withMessage: "Please select any business type", type: alertType.bannerDark,on: self)
        }else{
            let sb = UIStoryboard(name:"SignUpProcess",bundle:Bundle.main)
            let objChooseType = sb.instantiateViewController(withIdentifier:"AddCategoryTypeVC") as! AddCategoryTypeVC
            objChooseType.businessId = self.cellSelectId

            self.navigationController?.pushViewController(objChooseType, animated: true)
        }
        
    }
    @IBAction func btnSkip(_ sender:Any){
        let sb = UIStoryboard(name:"SignUpProcess",bundle:Bundle.main)
        let objChooseType = sb.instantiateViewController(withIdentifier:"ServicesListVC") as! ServicesListVC
         self.navigationController?.pushViewController(objChooseType, animated: true)
    }

    
    
    
}
//MARK: - Webservices
fileprivate extension AddBusinessTypeVC{
    
   func callWebserviceForGetServices(){
           if !objServiceManager.isNetworkAvailable(){
               objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
               return
           }
           objActivity.startActivityIndicator()
           self.arrBusinessType.removeAll()
             let finalUrl = "\(WebURL.myBusinessTypeShow)\(strGetBusinessID)"
        //  let finalUrl = "\(WebURL.allBusinessTypeSearchByID)business_id=\(strGetBusinessID)"
           print(finalUrl)
   
           objServiceManager.requestGetNew(strURL: finalUrl, params: nil, strCustomValidation: "", success: { response in
   
               print(response)
               if  response["status"] as? String == "success"{
                   //self.saveData(dict:response)
                   if let data = response["businessTypes"] as? [[String:Any]]{
                       for obj in data{
                         let myObj = ModelAddBusinessType.init(dict: obj)
                           self.arrBusinessType.append(myObj)
                       }
                   }
                 self.tblBusinessLIst.reloadData()
                   objAppShareData.fromDeleteButton = true
               }else{
                   self.tblBusinessLIst.reloadData()
                   if objAppShareData.fromDeleteButton != true &&  self.addBusiness == false {
                       objAppShareData.fromDeleteButton = true
                       let sb = UIStoryboard(name:"SignUpProcess",bundle:Bundle.main)
                       let objChooseType = sb.instantiateViewController(withIdentifier:"SearchAddBusinessTypeVC") as! SearchAddBusinessTypeVC
                       self.navigationController?.pushViewController(objChooseType, animated: false)
                   }
                   self.addBusiness = false
               }
           }) { error in
               self.tblBusinessLIst.reloadData()
   
               // objAppShareData.showDeafultAlert(withTitle: message.error.title, message: message.error.wrong, on: self)
               objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
           }
   }
    
    
    /*
     func callWebserviceForGetServices(str:String){
         if !objServiceManager.isNetworkAvailable(){
             objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
             return
         }
         
         let finalUrl = "\(WebURL.allBusinessTypeSearch)business_type=\(str)"
         print(finalUrl)
      
         self.indicate.startAnimating()
         objServiceManager.requestGetNew(strURL: finalUrl, params: nil, strCustomValidation: "", success: { response in
             self.arrBusinessType.removeAll()
             if  response["status"] as? String == "success"{
                 //self.saveData(dict:response)
                 if let data = response["businessTypes"] as? [[String:Any]]{
                     for obj in data{
                         let myObj = ModelAddBusinessType.init(dict: obj)
                         self.arrBusinessType.append(myObj)
                     }
                 }
                 self.tblBusinessLIst.reloadData()
                 self.indicate.stopAnimating()
             }else{
                 self.tblBusinessLIst.reloadData()
                 self.indicate.stopAnimating()

             }
         }) { error in
             self.tblBusinessLIst.reloadData()
             self.indicate.stopAnimating()
            // self.callWebserviceForGetServices(param: ["search":""])
            // self.callWebserviceForGetServices(str: self.strText)
             objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
         }
     }
     */
    
    
    //Rohit Worked on 12/Feb/2020
//    func callWebserviceForGetServices(){
//        if !objServiceManager.isNetworkAvailable(){
//            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
//            return
//        }
//        objActivity.startActivityIndicator()
//        self.arrBusinessType.removeAll()
//
//        let finalUrl = "\(WebURL.myBusinessType)\(strGetBusinessID)"
//
//        objServiceManager.requestGetNew(strURL: finalUrl, params: nil, strCustomValidation: "", success: { response in
//
//            print(response)
//            if  response["status"] as? String == "success"{
//                //self.saveData(dict:response)
//                if let data = response["data"] as? [[String:Any]]{
//                    for obj in data{
//                      let myObj = ModelAddBusinessType.init(dict: obj)
//                        self.arrBusinessType.append(myObj)
//                    }
//                }
//              self.tblBusinessLIst.reloadData()
//                objAppShareData.fromDeleteButton = true
//            }else{
//                self.tblBusinessLIst.reloadData()
//                if objAppShareData.fromDeleteButton != true &&  self.addBusiness == false {
//                    objAppShareData.fromDeleteButton = true
//                    let sb = UIStoryboard(name:"SignUpProcess",bundle:Bundle.main)
//                    let objChooseType = sb.instantiateViewController(withIdentifier:"SearchAddBusinessTypeVC") as! SearchAddBusinessTypeVC
//                    self.navigationController?.pushViewController(objChooseType, animated: false)
//                }
//                self.addBusiness = false
//            }
//        }) { error in
//            self.tblBusinessLIst.reloadData()
//
//            // objAppShareData.showDeafultAlert(withTitle: message.error.title, message: message.error.wrong, on: self)
//            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
//        }
//}
    
    
    
    
    func deleteCell(mainBookingId:String){
        let fetchRequest: NSFetchRequest<BusinessType> = BusinessType.fetchRequest()
        fetchRequest.predicate =  NSPredicate(format: "businessId = %@",mainBookingId)
        do {
            var allData = false
            let objArray = try managedContext?.fetch(fetchRequest)
            if (objArray?.count)! > 0{
                for object in objArray!{
                    if object.businessId == mainBookingId {
                        managedContext?.delete(object)
                    }
                }
            }
        try managedContext?.save()
            self.callWebserviceDeleteServices(Ids:mainBookingId)
         } catch _ {
            // error handling
        }
    }
    
    
    func callWebserviceDeleteServices(Ids:String){
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        objActivity.startActivityIndicator()
        let dict = ["business_type_id":Ids,"business_id":strGetBusinessID]
        objServiceManager.requestPost(strURL: WebURL.deleteBussinessType, params: dict as [String : AnyObject], success: { response in
            self.arrBusinessType.removeAll()
            
            if  response["status"] as? String == "success"{
                self.callWebserviceForGetServices()
            }else{
                self.tblBusinessLIst.reloadData()
            }
        }) { error in
            self.tblBusinessLIst.reloadData()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
}

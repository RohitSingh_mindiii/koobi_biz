//
//  AddServiceTimeVC.swift
//  MualabBusiness
//
//  Created by Mindiii on 2/16/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit

class AddServiceTimeVC:  UIViewController,UIPickerViewDelegate, UIPickerViewDataSource {
        
        
        fileprivate var Hours = [Any]()
        fileprivate var Minute = [Any]()
        fileprivate var selectedSegmentIndex: Int = 0
        fileprivate var selectedHours = ""
        fileprivate var selectedMinute = ""
        fileprivate var bookingType = ""
    
        fileprivate var InCallHours = ""
        fileprivate var InCallMinurte = ""
    
        @IBOutlet weak var viewBtnSubmit: UIView!
        @IBOutlet weak var pickerHours: UIPickerView!
        @IBOutlet weak var pickerMinute: UIPickerView!

        
        override func viewDidLoad() {
            super.viewDidLoad()
            self.loadDefaultsParameters()
        }
        
        override func viewWillAppear(_ animated: Bool) {
            self.viewConfigure()
        }
        
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
    }
    //MARK:- Custome method extension
    extension AddServiceTimeVC{
        func viewConfigure(){
            let time =  objAppShareData.objModelFinalSubCategory.param[objAppShareData.objModelFinalSubCategory.completionTime] ?? "00:00"
            let arrTime = time.components(separatedBy: ":")
            if arrTime.count  > 1{
                objAppShareData.objModelBusinessSetup.incallTimeHours =  "\(arrTime[0] )"
                objAppShareData.objModelBusinessSetup.incallTimeMinute =  "\(arrTime[1])"
                self.InCallHours = objAppShareData.objModelBusinessSetup.incallTimeHours
                self.InCallMinurte = objAppShareData.objModelBusinessSetup.incallTimeMinute
                self.selectIncall()
            }else{
                InCallHours  = "00"
                InCallHours  = "00"
                objAppShareData.objModelBusinessSetup.incallTimeMinute = "00"
                objAppShareData.objModelBusinessSetup.incallTimeMinute = "00"
                self.selectIncall()
            }

        }
        
        
        func selectIncall(){
            if InCallHours == ""{
                let b = String(objAppShareData.objModelBusinessSetup.incallTimeHours.last ?? "0")
                pickerHours.selectRow(Int(b)!, inComponent: 0, animated: true)
            }else{
                let b = String(InCallHours.last ?? "0")
                pickerHours.selectRow(Int(b)!, inComponent: 0, animated: true)
            }
            
            if InCallMinurte == ""{
                let a = String(objAppShareData.objModelBusinessSetup.incallTimeMinute.first ?? "0")
                pickerMinute.selectRow(Int(a)!, inComponent: 0, animated: true)
            }else{
                let a = String(InCallMinurte.first ?? "0")
                pickerMinute.selectRow(Int(a)!, inComponent: 0, animated: true)
            }
        }
    }
    //MARK:- Button extension
    extension AddServiceTimeVC{
        @IBAction func btnBackAction(_ sender:Any){
            InCallMinurte = ""
            InCallHours = ""
            self.navigationController?.popViewController(animated: true)
        }
        
        @IBAction func btnContinewAction(_ sender:Any){
            if self.bookingType == "Incall"{
                if InCallHours == "03" &&  InCallMinurte != "00"{
                    objAppShareData.showAlert(withMessage: "Please select valid service completion time", type: alertType.banner, on: self)
                    return
                }
            }
                if InCallHours == "00" && InCallMinurte == "00"{
                    objAppShareData.showAlert(withMessage: "Please select service completion time", type: alertType.banner, on: self)
                }else{
                    objAppShareData.objModelBusinessSetup.incallTimeHours = self.InCallHours
                    objAppShareData.objModelBusinessSetup.incallTimeMinute = self.InCallMinurte
                    objAppShareData.objModelFinalSubCategory.param[objAppShareData.objModelFinalSubCategory.completionTime] = self.InCallHours+":"+self.InCallMinurte
                    self.navigationController?.popViewController(animated: true)
                }
        }
    }

    //MARK:- Method for expiry month
    extension AddServiceTimeVC{
        
        func NumberOfHours() -> [Any] {
            return ["00","01", "02","03"]
        }
        
        func NumberOfMinute() -> [Any] {
            return ["00","10","20","30","40","50"]
        }
        
        func loadDefaultsParameters() {
            Hours = NumberOfHours()
            Minute = NumberOfMinute()
            pickerHours.delegate = self
            pickerHours.dataSource = self
            pickerMinute.delegate = self
            pickerMinute.dataSource = self
        }
    }
    //MARK:- Method for expiry month
    extension AddServiceTimeVC {
        // MARK:- Picker View Delegates
        func numberOfComponents(in pickerView: UIPickerView) -> Int {
            return 1
        }
        func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
            if pickerView == pickerHours{
                return Hours.count
            }
            else if pickerView == pickerMinute{
                return Minute.count
            }else {
                return 0
            }
        }
        
        func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
            
            if pickerView == pickerHours{
                let hourseName: String = Hours[row] as! String
                selectedHours = hourseName
                return hourseName
            }
            else  if pickerView == pickerMinute{
                let minuteName: String = Minute[row] as! String
                let str = "\(minuteName)"
                selectedMinute = str
                return str
            }else{
                return ""
            }
        }
        
        func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
            
            var pickerLabel: UILabel? = (view as? UILabel)
            if pickerLabel == nil {
                pickerLabel = UILabel()
                pickerLabel?.font = UIFont(name: "Nunito-Medium", size: 30)
                pickerLabel?.textAlignment = .center
                pickerLabel?.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
                pickerLabel?.shadowColor = UIColor.clear
            }
            
            
            
            if pickerView == pickerHours{
                pickerLabel?.text = Hours[row] as? String ?? "00"
                self.InCallHours = pickerLabel?.text ?? "00"
            }
            if pickerView == pickerMinute{
                pickerLabel?.text = Minute[row] as? String ?? "00"
                self.InCallMinurte = pickerLabel?.text ?? "00"
            }
            return pickerLabel!
        }
        
        func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
            
            
            if pickerView == pickerHours{
                    InCallHours = Hours[row] as? String ?? "00"
                    if InCallHours == "03"{
                        InCallMinurte = "00"
                        pickerMinute.selectRow(0, inComponent: 0, animated: true)
                    }
            }
            if pickerView == pickerMinute{
                    InCallMinurte = Minute[row] as? String ?? "00"
                    if InCallHours == "03"{
                        InCallMinurte = "00"
                        pickerMinute.selectRow(0, inComponent: 0, animated: true)
                    }
            }
        }
}

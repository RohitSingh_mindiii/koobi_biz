//
//  ServicesListVC.swift
//  MualabBusiness
//
//  Created by Mac on 19/11/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import DropDown
import CoreData

class ServicesListVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
  
    @IBOutlet weak var tblBusinessLIst:UITableView!
    @IBOutlet weak var lblNoDataFound:UIView!
    fileprivate var arrBusinessType = [ModelAddBusinessType]()
    var businessTypeCount = 0
    @IBOutlet weak var btnSkip:UIButton!
    @IBOutlet weak var btnContinue:UIButton!
    @IBOutlet weak var viewProgress:UIView!
    
    @IBOutlet weak var btnIncall:UIButton!
    @IBOutlet weak var btnOutcall:UIButton!
    @IBOutlet weak var viewBtnInOut: UIView!
    
    @IBOutlet weak var viewBtnIncallBottum:UIView!
    @IBOutlet weak var viewBtnOutcallBottum:UIView!
    
    @IBOutlet weak var viewIncallSet:UIView!
    @IBOutlet weak var viewOutcallSet:UIView!
    @IBOutlet weak var viewButtonSet:UIView!

    var managedContext:NSManagedObjectContext?
    var arrBusinessTypeList = [ModelBusinessTypeList]()
    var arrBusinessTypeListTable = [ModelBusinessTypeList]()
    
    var arrIncallList = [ModelBusinessTypeList]()
    var arrOutcallList = [ModelBusinessTypeList]()
    
    var navigate = true
    fileprivate var serType = "B"
    
     override func viewDidLoad() {
        super.viewDidLoad()
        serType = "I"
        self.viewButtonSet.isHidden = true
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        managedContext = appDelegate.persistentContainer.viewContext
        
        self.tblBusinessLIst.delegate = self
        self.tblBusinessLIst.dataSource = self
        //self.tblBusinessLIst.reloadData()
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadDataForDeleteCell), name: NSNotification.Name(rawValue: "Delete"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadDataForEditCell), name: NSNotification.Name(rawValue: "Edit"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadDataForDetailCell), name: NSNotification.Name(rawValue: "Detail"), object: nil)
     }
    
 
    @objc func reloadDataForDeleteCell(notification: Notification){
        self.deleteCell(objModel: objAppShareData.objModelServicesList)
    }
    @objc func reloadDataForDetailCell(notification: Notification){
        if objAppShareData.manageNavigation == false{
            objAppShareData.manageNavigation = true

        if navigate == false{
            return
        }
        let o = objAppShareData.objModelServicesList
        let a = objAppShareData.objModelFinalSubCategory
        a.param = [:]
        a.param[a.serviceName] = o.serviceName
        a.param[a.strBusinessId] = o.mainBookingId
        a.param[a.strCategoryId] = o.mainCategoryId
        a.param[a.bookingType] = o.bookingType
        a.param[a.incallPrice] = o.price
        a.param[a.outCallPrice] = o.outcallPrice
        a.param[a.descriptions] = o.describe
        a.param[a.completionTime] = o.time
            
            let sb = UIStoryboard(name:"SignUpProcess",bundle:Bundle.main)
            let objChooseType = sb.instantiateViewController(withIdentifier:"EditServiceVC") as! EditServiceVC
            objChooseType.objModelClass = objAppShareData.objModelServicesList
            objChooseType.viewType = "YES"
            if navigate == true{
                navigate = false
                self.navigationController?.pushViewController(objChooseType, animated: true)
            } 
    }
    }
    @objc func reloadDataForEditCell(notification: Notification){
        self.EditDetailCell(objModel: objAppShareData.objModelServicesList, strViewType: "NO")
    }
    override func viewWillAppear(_ animated: Bool) {
        
        navigate = true
        let businessType = UserDefaults.standard.string(forKey: UserDefaults.keys.mainServiceType)
       self.viewBtnInOut.isHidden = true
       var strType = ""
        self.viewIncallSet.isHidden = false
        self.viewOutcallSet.isHidden = false
        if businessType == "1"{
            strType = "Incall"
            self.serType = "I"
            self.viewOutcallSet.isHidden = true
            self.viewIncallSet.isHidden = false
        }else if businessType == "2"{
            strType = "Outcall"
            self.serType = "O"
            self.viewIncallSet.isHidden = true
            self.viewOutcallSet.isHidden = false
        }else{
            self.serType = "I"
            strType = "Both"
            self.viewBtnInOut.isHidden = false
        }
        
        objAppShareData.objModelFinalSubCategory.param[objAppShareData.objModelFinalSubCategory.bookingType] = strType
        
        if self.serType == "I" {
            self.btnIncall.setTitleColor(#colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1), for: .normal)
            self.btnOutcall.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            self.viewBtnIncallBottum.backgroundColor = #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1)
            self.viewBtnOutcallBottum.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        }else{
            self.btnOutcall.setTitleColor(#colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1), for: .normal)
            self.btnIncall.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            self.viewBtnOutcallBottum.backgroundColor = #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1)
            self.viewBtnIncallBottum.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        }
        
        //.get(String(self.callOption),forKey: UserDefaults.keys.mainServiceType)
        self.fetchParcelObject(api: true)
        
        
        if objAppShareData.editSelfServices == true{
            self.viewProgress.isHidden = true
            self.btnSkip.isHidden = true
            self.btnContinue.setTitle("Save & Continue", for: .normal)
        }else{
            self.viewProgress.isHidden = false
            self.btnSkip.isHidden = false
            self.btnContinue.setTitle("Continue", for: .normal)
        }
        self.callWebserviceForGetBusinessType()
    }
    
 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

//MARK:- extension for tableview delegate
extension ServicesListVC {
    func numberOfSections(in tableView: UITableView) -> Int
    {
        if self.arrBusinessTypeListTable.count > 0{
            self.lblNoDataFound.isHidden = true
        }else{
            self.lblNoDataFound.isHidden = false
        }
        return self.arrBusinessTypeListTable.count
    }
    
//    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int){
//        view.tintColor = UIColor.white
//        let header = view as! UITableViewHeaderFooterView
//        header.textLabel?.textColor = UIColor.black
//
//    }
    
    
    
     func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.white
        
        let headerLabel = UILabel(frame: CGRect(x: 20, y: 2, width:
            tableView.bounds.size.width, height: tableView.bounds.size.height))
        headerLabel.font = UIFont(name: "Nunito-Medium", size: 17)
        headerLabel.textColor = UIColor.black
        headerLabel.text = self.tableView(self.tblBusinessLIst, titleForHeaderInSection: section)
        headerLabel.sizeToFit()
        headerView.addSubview(headerLabel)
        
        return headerView
    }
     func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        return self.arrBusinessTypeListTable[section].businessName.capitalized
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let arrDevelopers = self.arrBusinessTypeListTable[section].arrBusinessList
         return arrDevelopers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "BusinessListTypeTableCell", for: indexPath) as? BusinessListTypeTableCell{
            let obj  = arrBusinessTypeListTable[indexPath.section].arrBusinessList[indexPath.row]
            cell.lblCategoryName.text = obj.categoryName
            cell.arrService = obj.arrServices
            print("height = ",CGFloat(30+(obj.arrServices.count*45)))
            cell.setNeedsLayout()
            
            cell.arrBusinessCategory.append(arrBusinessTypeList[indexPath.section].businessName)
            cell.arrBusinessCategory.append(obj.categoryName)
            cell.serType = serType
            cell.tblServiceList.reloadData()
            return cell
        }else{
           return UITableViewCell()
        }
    }
   }


//MARK:- extension for tableview delegate
extension ServicesListVC {

@IBAction func btnContinew(_ sender:Any){
    self.SendDataOnServer()
}
    @IBAction func btnSkip(_ sender:Any){
        let sb = UIStoryboard(name:"SignUpProcess",bundle:Bundle.main)
        let objChooseType = sb.instantiateViewController(withIdentifier:"AddBankDetailVC") as! AddBankDetailVC
        self.navigationController?.pushViewController(objChooseType, animated: true)
    }
@IBAction func btnBack(_ sender:Any){
    let controllers = self.navigationController?.viewControllers
    var navigate = false
    if objAppShareData.editSelfServices == true{

        for vc in controllers! {
            if vc is BusinessAdminLiastVC {
                navigate = true
                objAppShareData.fromDeleteButton = true
                _ = self.navigationController?.popToViewController(vc as! BusinessAdminLiastVC, animated: true)
            }
        }
    }else{
        for vc in controllers! {
            if vc is AddCategoryTypeVC {
                navigate = true
                _ = self.navigationController?.popToViewController(vc as! AddCategoryTypeVC, animated: true)
            }
        }
        if navigate == false{
            self.navigationController?.popViewController(animated: true)
        }
    }
    }
 

    
@IBAction func btnAddCategoryType(_ sender:Any){
   // self.popToViewContruller()
    self.viewButtonSet.isHidden = true
    //   self.popToViewContruller()
    let sb = UIStoryboard(name:"SignUpProcess",bundle:Bundle.main)
    var screenNumbers = 0

    if self.arrBusinessType.count == 0{
        screenNumbers = 1
        let objChooseType = sb.instantiateViewController(withIdentifier:"SearchAddBusinessTypeVC") as! SearchAddBusinessTypeVC
        self.navigationController?.pushViewController(objChooseType, animated: false)
    }else if self.arrBusinessType.count  == 1{
        let objChooseType = sb.instantiateViewController(withIdentifier:"SearchAddCategoryTypeVC") as! SearchAddCategoryTypeVC
        objChooseType.idBusiness = self.arrBusinessType[0].serviceId
        self.navigationController?.pushViewController(objChooseType, animated: false)
    }else{
        let objChooseType = sb.instantiateViewController(withIdentifier:"AddBusinessTypeVC") as! AddBusinessTypeVC
        objChooseType.screenNo = 5
        self.navigationController?.pushViewController(objChooseType, animated: false)
    }
    
  
}
    
    @IBAction func btnAddBusinessType(_ sender:Any){
        self.viewButtonSet.isHidden = true
        //   self.popToViewContruller()
        let sb = UIStoryboard(name:"SignUpProcess",bundle:Bundle.main)
        let objChooseType = sb.instantiateViewController(withIdentifier:"SearchAddBusinessTypeVC") as! SearchAddBusinessTypeVC
        self.navigationController?.pushViewController(objChooseType, animated: false)
    }
    
@IBAction func btnAddServiceType(_ sender:Any){
//    objAppShareData.showAlert(withMessage: "Under development", type: alertType.bannerDark,on: self)
//    return
    self.viewButtonSet.isHidden = true
    objAppShareData.objModelFinalSubCategory.param = [:]
    let businessType = UserDefaults.standard.string(forKey: UserDefaults.keys.mainServiceType)
    var strType = ""
    if businessType == "1"{
        strType = "Incall"
    }else if businessType == "2"{
        strType = "Outcall"
    }else{
        strType = "Both"
    }
    if strType == "Incall"{
    }else if strType == "Outcall"{
    }else{
        strType = "Both"
    }
    objAppShareData.objModelFinalSubCategory.param[objAppShareData.objModelFinalSubCategory.bookingType] =
    strType
            let sb = UIStoryboard(name:"SignUpProcess",bundle:Bundle.main)
            let objChooseType = sb.instantiateViewController(withIdentifier:"AddServicesVC") as! AddServicesVC
                objChooseType.fromCategory = false

            self.navigationController?.pushViewController(objChooseType, animated: true)
    
}}


extension ServicesListVC{
    
    func fetchParcelObject(api:Bool){
        self.arrBusinessTypeList.removeAll()
        self.arrBusinessTypeListTable.removeAll()
        arrMain.removeAll()
         let fetchRequest: NSFetchRequest<BusinessType> = BusinessType.fetchRequest()
 
        do {
            let result = try managedContext?.fetch(fetchRequest)
            
            for data in result ?? [] {
                var arrServices = [[String:Any]]()

                print(data.businessName ?? "")
                var arrayCate = [[String:Any]]()
                
                let arrCategory = Array(data.mutableSetValue(forKey: "categorytypes")) as? [CategoryType] ?? []
                for objCat in arrCategory{
                    print(objCat.categoryName ?? "")
                    var arrayServices = [[String:Any]]()
                    let arrService = Array(objCat.mutableSetValue(forKey: "services")) as? [Services] ?? []
                    for objSer in arrService{
                        
                        let dict = [
                                     "mainBookingId":data.businessId,
                                     "mainCategoryId":objCat.categoryId,
                                     "_id":objSer.serviceId,
                                     "serviceName":objSer.serviceName,
                                     "price":objSer.price,
                                     "incallPrice":objSer.incallPrice,
                                     "outcallPrice":objSer.outcallPrice,
                                     "bookingType":objSer.bookingType,
                                     "describe":objSer.describe,
                                     "time":objSer.time]
                        // let obj = ModelServicesList.init(dict: dict)
                        arrayServices.append(dict)
                        arrServices.append(dict)
                    }
                    let a = objCat.categoryId ?? ""
                    let b = objCat.categoryName ?? ""
                    let c = data.businessName ?? ""
                    let dict1 = ["categoryId":a,
                                 "categoryName":b,
                                 "businessName":c,
                                 "arrServices":arrayServices] as [String : Any]
                    if arrayServices.count > 0{
                    arrayCate.append(dict1)
                    }
                    let objCate = ModelCategoryList(dict: dict1)
                    
                    //self.tblBusinessLIst.reloadData()
                 //   self.tblReloadDataForIncallOutcall()

                }
                //self.tblBusinessLIst.reloadData()
               // self.tblReloadDataForIncallOutcall()

                let c = data.businessName ?? ""
                let d = data.businessId ?? ""
                let dict2 = ["businessName":c,
                             "businessId":d,
                             "arrServices":arrServices,
                             "arrCategory":arrayCate] as [String : Any]
                let objModelBusinessTypeList = ModelBusinessTypeList(dict: dict2)
            
                if objModelBusinessTypeList.arrBusinessList.count > 0{
                    arrMain.append(objModelBusinessTypeList)
                }
                
                if objModelBusinessTypeList.arrServiceList.count > 0{
                self.arrBusinessTypeList.append(objModelBusinessTypeList)
                }
                
               
            }
            if self.arrBusinessTypeList.count == 0 && api == true {
                self.callWebserviceForUpdateData()
            }
            //self.tblBusinessLIst.reloadData()
            self.tblReloadDataForIncallOutcall()

        } catch {
            self.tblReloadDataForIncallOutcall()
        }
    }
    
    
    func EditDetailCell(objModel:ModelServicesList,strViewType:String){
        if objAppShareData.manageNavigation == false{
        objAppShareData.manageNavigation = true
        let o = objModel
        let a = objAppShareData.objModelFinalSubCategory
        a.param = [:]
        a.param[a.serviceName] = o.serviceName
        a.param[a.strBusinessId] = o.mainBookingId
        a.param[a.strCategoryId] = o.mainCategoryId
        a.param[a.bookingType] = o.bookingType
        a.param[a.incallPrice] = o.price
        a.param[a.outCallPrice] = o.outcallPrice
        a.param[a.descriptions] = o.describe
        a.param[a.completionTime] = o.time
        
        let sb = UIStoryboard(name:"SignUpProcess",bundle:Bundle.main)
        let objChooseType = sb.instantiateViewController(withIdentifier:"EditServiceVC") as! EditServiceVC
        objChooseType.objModelClass = objModel
        objChooseType.viewType = strViewType
        objChooseType.serviceName = objModel.serviceName
        if navigate == true{
            navigate = false
        self.navigationController?.pushViewController(objChooseType, animated: true)
        }
        }
    }
    
    func deleteCell(objModel:ModelServicesList){
        if objModel.bookingType == "Both"{
           self.saveDataInLocalDataBase(objModelClass: objModel)
        }else{
         let fetchRequest: NSFetchRequest<Services> = Services.fetchRequest()
        fetchRequest.predicate =  NSPredicate(format: "mainBookingId = %@", objModel.mainBookingId)
        do {
            var allData = false
            let objArray = try managedContext?.fetch(fetchRequest)
            if (objArray?.count)! > 0{
                for object in objArray!{
                    if object.mainBookingId == objModel.mainBookingId &&  object.mainCategoryId == objModel.mainCategoryId && object.serviceName == objModel.serviceName{
                        managedContext?.delete(object)

                        if arrMain.count == 1{
                            if arrMain[0].arrBusinessList.count == 1{
                                if arrMain[0].arrBusinessList[0].arrServices.count == 1{
                                   allData = true
                                }
                            }
                        }
                    }
                }
            }
            try managedContext?.save()
            if allData == true{
                self.removePropertyData()
            }
            self.fetchParcelObject(api:false)
        } catch _ {
            // error handling
         }
        }
     }
    
     func SendDataOnServer(){
        let fetchRequest: NSFetchRequest<Services> = Services.fetchRequest()
        var arrSend = [ModelServicesList]()
        do {
            let result = try managedContext?.fetch(fetchRequest)
            for data in result ?? [] {
                let dict1 = [ "_id":data.serviceId,
                             "mainBookingId":data.mainBookingId,
                             "mainCategoryId":data.mainCategoryId,
                             "serviceName":data.serviceName,
                             "incallPrice":data.incallPrice ?? "0.0",
                             "outcallPrice":data.outcallPrice ?? "0.0",
                             "price":data.price ?? "0.0",
                             "bookingType":data.bookingType,
                             "describe":data.describe,
                             "time":data.time]
                let objModel = ModelServicesList.init(dict: dict1)
                arrSend.append(objModel)
                if arrSend.count == result?.count{
                    sendArray(arrSend: arrSend)
                }
            }
            if result?.count == 0{
                sendArray(arrSend: arrSend)
            }
        }catch {
        }
    }
    func sendArray(arrSend:[ModelServicesList]){
        var serviceJosnArray = [[String: Any]]()
        var serviceJosn = [String: Any]()
        let userDict = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfoDic) as? [String:Any] ?? ["":""]
        let businessID = userDict[UserDefaults.keys.business_id]as? String ?? ""
        let userID = userDict[UserDefaults.keys.userId]as? String ?? ""
        let staffID = userDict[UserDefaults.keys.staff_id]as? String ?? ""
        for obj in arrSend{
            let objSub = obj
            
            var a = ""
            a = objSub.mainBookingId
            
            var priceIC = objSub.incallPrice
            if objSub.incallPrice != ""{
                let b = float_t(objSub.incallPrice) ?? 0.0
                priceIC = String(b)
            }else{
                priceIC = "0.00"
            }
            var priceOC = objSub.outcallPrice
            if objSub.outcallPrice != ""{
                let b = float_t(objSub.outcallPrice) ?? 0.0
                priceOC = String(b)
            }else{
                priceOC = "0.0"
            }
            /*
             {"business_id":"10","category_id":"6","service":"abc","description":"spice","in_call_price":"10","out_call_price":"20","completion_time":"20","created_by":"8"},
             */
            
           
            
            serviceJosn["business_id"] = businessID
            serviceJosn["category_id"] = objSub.mainCategoryId
            serviceJosn["service"] = objSub.serviceName
            serviceJosn["description"] = objSub.describe
            serviceJosn["in_call_price"] = priceIC
            serviceJosn["out_call_price"] = priceOC
            serviceJosn["completion_time"] = objSub.time
            serviceJosn["created_by"] = userID
            
//            serviceJosn["serviceId"] = a
//            serviceJosn["subserviceId"] = objSub.mainCategoryId
//            serviceJosn["title"] = objSub.serviceName
//            serviceJosn["description"] = objSub.describe
//            serviceJosn["inCallPrice"] = priceIC
//            serviceJosn["outCallPrice"] = priceOC
//            serviceJosn["completionTime"] = objSub.time
            serviceJosnArray.append(serviceJosn)
        }
        
        var objectString = ""
        if let objectData = try? JSONSerialization.data(withJSONObject: serviceJosnArray, options: JSONSerialization.WritingOptions(rawValue: 0)) {
            objectString = String(data: objectData, encoding: .utf8)!
        }
 
        let dict = ["artistService":objectString,"business_id":businessID,"staff_id":staffID]
        print(dict)
        self.callWebserviceForUpdateRange(dicParam: dict)
    }
    
    func alert(str:String){
        //objAppShareData.showAlert(withMessage: str, type: alertType.bannerDark,on: self)
        if objAppShareData.editSelfServices == true{
            let controllers = self.navigationController?.viewControllers
            for vc in controllers! {
                if vc is  BusinessAdminLiastVC{
                   // navigationCheck = true
                    _ = self.navigationController?.popToViewController(vc as! BusinessAdminLiastVC, animated: true)
                }
            }
        }else{
        let sb = UIStoryboard(name:"SignUpProcess",bundle:Bundle.main)
        let objVC = sb.instantiateViewController(withIdentifier:"AddBankDetailVC") as! AddBankDetailVC
        self.navigationController?.pushViewController(objVC, animated:true)
        }
    }
    func callWebserviceForUpdateRange(dicParam:[String:Any]){
        
        if self.arrBusinessTypeListTable.count == 0{
            objAppShareData.showAlert(withMessage: "Please add services", type: alertType.bannerDark,on: self)
            return
        }
        
                if !objServiceManager.isNetworkAvailable(){
                    objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                    return
                }
        objActivity.startActivityIndicator()
 
        objServiceManager.requestPost(strURL: WebURL.addArtistService, params: dicParam, success: { response in
            let  status = response["status"] as? String ?? ""
            print(response)
            if status == "success"{
                 if let arr = response["data"] as? [[String:Any]]{
                    
                        self.alert(str:(response["message"] as? String ?? ""))
//                    }else{
//                        objAppShareData.showAlert(withMessage: "Plese add service", type: alertType.bannerDark,on: self)
//                    }
                }
                
            }else{
                objAppShareData.showAlert(withMessage: (response["message"] as? String ?? ""), type: alertType.bannerDark,on: self)
            }
        }) { error in
            objActivity.stopActivity()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
         }
     }
}

//MARK: - Navigation method
extension ServicesListVC{
    func popToViewContruller(){
        let controllers = self.navigationController?.viewControllers
        for vc in controllers! {
            if vc is AddBusinessTypeVC {
                objAppShareData.fromDeleteButton = false
                _ = self.navigationController?.popToViewController(vc as! AddBusinessTypeVC, animated: true)
            }
        }
    }
}


//MARK: - call api for get service list from backecd server and set on local server

extension ServicesListVC{
    
    func callWebserviceForUpdateData(){
        
                if !objServiceManager.isNetworkAvailable(){
                    objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                    return
                }
        
        let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] ?? ["":""]
        let userId = decoded[UserDefaults.keys.userId] as? String ?? ""
        objActivity.startActivityIndicator()
        arrMain.removeAll()
        objServiceManager.requestPostForJson(strURL: WebURL.artistService, params: ["artistId":userId], success: { response in
            print(response)
 
            let status = response["status"] as? String ?? ""
            if status == "success"{
                
                
                if let arrArtistServices = response["artist_services"] as? [[String:Any]]{
                    for dict1 in arrArtistServices {
                        var ids = ""
                        if let id = dict1["serviceId"] as? Int { ids = String(id)
                        }else if let id = dict1["serviceId"] as? String{ ids = id
                        }
                        let businessName = dict1["serviceName"] as? String ?? ""
                        
                        var arrServices = [[String:Any]]()
                        var arrayCate = [[String:Any]]()
                       
                        if let arrSer = dict1["subServies"] as? [[String:Any]]{
                            for dict11 in arrSer{
                                var arrayServices = [[String:Any]]()
                                var catId = ""
                                if let cid = dict11["subServiceId"] as? String{
                                    catId = cid
                                }else if let cid = dict11["subServiceId"] as? Int{
                                    catId = String(cid)
                                }
                                
                                
                                if let arrArtistServices = dict11["artistservices"] as? [[String:Any]]{
                                    for dict111 in arrArtistServices{
                                        
                                        
                                        
                                        
                                        
                                        
                                        var outCallPrice = "0.0"
                                        if let id = dict111["outCallPrice"] as? String {
                                            outCallPrice = String(id)
                                            //print("Value = Int = ",outCallPrice)
                                        }else if let id = dict111["outCallPrice"] as? double_t{
                                            outCallPrice = String(id)
                                            //print("Value = String = ",outCallPrice)
                                        }else if let id = dict111["outCallPrice"] as? float_t{
                                            outCallPrice = String(id)
                                            //print("Value = float_t = ",outCallPrice)
                                        }else if let id = dict111["outCallPrice"] as? Int{
                                            outCallPrice = String(id)
                                            //print("Value = double_t = ",outCallPrice)
                                        }else if let id = dict111["outCallPrice"] as? Double{
                                            outCallPrice = String(id)
                                            //print("Value = Double = ",outCallPrice)
                                        }
                                        
                                        
                                        
                                        
                                        var inCallPrice = "0.0"
                                        if let id = dict111["inCallPrice"] as? String{
                                            inCallPrice = String(id)
                                            //print("Value = String = ",inCallPrice)
                                        }else if let id = dict111["inCallPrice"] as? double_t {
                                            inCallPrice = String(id)
                                            print("double_t = Int = ",inCallPrice)
                                        }else if let id = dict111["inCallPrice"] as? float_t{
                                            inCallPrice = String(id)
                                            //print("Value = float_t = ",inCallPrice)
                                        }else if let id = dict111["inCallPrice"] as? Int{
                                            inCallPrice = String(id)
                                            //print("Value = double_t = ",inCallPrice)
                                        }else if let id = dict111["inCallPrice"] as? Double{
                                            inCallPrice = String(id)
                                            //print("Value = Double = ",inCallPrice)
                                        }
                                        
                                        var idService = ""
                                        if let id = dict1["_id"] as? Int { idService = String(id)
                                        }else if let id = dict1["_id"] as? String{ idService = id
                                        }
                                        
                                       
                                        
                               let dictA = ["price":inCallPrice,
                                            "incallPrice":inCallPrice,
                                            "outcallPrice":outCallPrice,
                                            "_id":idService,
                                            "mainBookingId":ids,
                                            "mainCategoryId":catId,
                                            "serviceName":dict111["title"] as? String ?? "",
                                            "bookingType":dict111["bookingType"] as? String ?? "",
                                            "describe":dict111["description"] as? String ?? "",
                                            "time":dict111["completionTime"] as? String ?? ""] as [String : Any]
                                        // let obj = ModelServicesList.init(dict: dict)
                                        arrayServices.append(dictA)
                                        arrServices.append(dictA)
                                    }
                                }
                                let catName = dict11["subServiceName"] as? String ?? ""
                                let c = dict1["serviceName"] ?? ""
                                let dict1 = ["categoryId":catId,
                                             "categoryName":catName,
                                             "businessName":businessName,
                                             "arrServices":arrayServices] as [String : Any]
                                 if arrayServices.count > 0{
                                    arrayCate.append(dict1)
                                }
                             }
                         }
                         let dict2 = ["businessName":businessName,
                                     "businessId":ids,
                                     "arrServices":arrServices,
                                     "arrCategory":arrayCate] as [String : Any]
                        
                        let objModelBusinessTypeList = ModelBusinessTypeList(dict: dict2)
                        
                        if objModelBusinessTypeList.arrBusinessList.count > 0{
                            arrMain.append(objModelBusinessTypeList)
                        }
                        if objModelBusinessTypeList.arrBusinessList.count > 0{
                            self.arrBusinessTypeList.append(objModelBusinessTypeList)
                        }

                    }
                    if arrMain.count > 0{
                        //self.tblBusinessLIst.reloadData()
                        self.tblReloadDataForIncallOutcall()
                        self.parseExpensesList()
                    }
                 }
                //self.tblBusinessLIst.reloadData()
                self.tblReloadDataForIncallOutcall()

                }else{
                //self.tblBusinessLIst.reloadData()
                self.tblReloadDataForIncallOutcall()

             }
        }) { error in
            objActivity.stopActivity()
            //self.tblBusinessLIst.reloadData()
            self.tblReloadDataForIncallOutcall()


         }
     }
}

extension ServicesListVC{
    
    func parseExpensesList(){
        self.removePropertyData()
        for dict in arrMain{
            let objBusinessType = BusinessType(context: self.managedContext!)
            objBusinessType.businessName = dict.businessName
            objBusinessType.businessId = dict.businessId
            
            let arrCategory = dict.arrBusinessList
            for objDict in arrCategory{
                let objCategory = CategoryType(context: self.managedContext!)
                objCategory.categoryName = objDict.categoryName
                objCategory.categoryId = objDict.categoryId
                
                let arrService = objDict.arrServices
                for objDict1 in arrService{
                    let objServices = Services(context: self.managedContext!)
                    objServices.serviceId = objDict1.id
                    let id1 = dict.businessId
                    let id2 = objDict.categoryId
                    objServices.mainBookingId = id1
                    objServices.mainCategoryId = id2
                    objServices.serviceName = objDict1.serviceName
                    objServices.price = objDict1.price
                    objServices.incallPrice = objDict1.incallPrice
                    objServices.outcallPrice = objDict1.outcallPrice
                    objServices.bookingType = objDict1.bookingType
                    objServices.describe = objDict1.describe
                    objServices.time = objDict1.time
                    objCategory.addToServices(objServices)
                }
                objBusinessType.addToCategorytypes(objCategory)
            }
        }
        self.tblReloadDataForIncallOutcall()

        do {
            try managedContext?.save()
            //self.tblBusinessLIst.reloadData()
            self.tblReloadDataForIncallOutcall()

         } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    func removePropertyData(){
        let fetchRequest: NSFetchRequest<BusinessType> = BusinessType.fetchRequest()
        do {
            let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest as! NSFetchRequest<NSFetchRequestResult>)
            try managedContext?.execute(deleteRequest)
            
        } catch {
            
        }
    }
    
 //////////////////////////////////////////////////manage parsing acording to incall outcall
    
    func tblReloadDataForIncallOutcall(){
        arrIncallList.removeAll()
        arrOutcallList.removeAll()
        arrBusinessTypeListTable.removeAll()
        for obj in self.arrBusinessTypeList{
            var arrInCat = [[String:Any]]()
            var arrOutCat = [[String:Any]]()
            let businessName = obj.businessName
            let ids = obj.businessId
            var arrServices = [[String:Any]]()

            for obj1 in obj.arrBusinessList{
                var arrInServices = [[String:Any]]()
                var arrOutServices = [[String:Any]]()
                let catId = obj1.categoryId
                let catName = obj1.categoryName
                for obj2 in obj1.arrServices{
                    let dict = [
                        "mainBookingId":obj.businessId,
                        "mainCategoryId":obj1.categoryId,
                        "_id":obj2.id,
                        "serviceName":obj2.serviceName,
                        "price":obj2.price,
                        "incallPrice":obj2.incallPrice,
                        "outcallPrice":obj2.outcallPrice,
                        "bookingType":obj2.bookingType,
                        "describe":obj2.describe,
                        "time":obj2.time]
                    if obj2.bookingType == "Incall" || obj2.bookingType == "Both"{
                        arrInServices.append(dict)
                    }
                    if obj2.bookingType == "Outcall" || obj2.bookingType == "Both"{
                        arrOutServices.append(dict)
                    }
                    
                    arrServices.append(dict)
                }


                var dict1 = ["categoryId":catId,
                             "categoryName":catName,
                             "businessName":businessName,
                             "arrServices":arrOutServices] as [String : Any]
                
                if arrInServices.count > 0 {
                    dict1["arrServices"] = arrInServices
                    arrInCat.append(dict1)
                }
                
                if arrOutServices.count > 0 {
                    dict1["arrServices"] = arrOutServices
                    arrOutCat.append(dict1)
                }
            }
            
            var dict2 = ["businessName":businessName,
                         "businessId":ids,
                         "arrServices":arrServices,
                         "arrCategory":arrOutCat] as [String : Any]
            
            if arrInCat.count > 0 {
                
                dict2["arrCategory"] = arrInCat
                let objSubSerInc = ModelBusinessTypeList(dict: dict2)
                arrIncallList.append(objSubSerInc)
                
            }
            if arrOutCat.count > 0 {
                dict2["arrCategory"] = arrOutCat
                let objSubSerOut = ModelBusinessTypeList(dict: dict2)
                arrOutcallList.append(objSubSerOut)
                
            }
        }
        print("arrIncallList = ",self.arrIncallList.count)
        print("arrOutcallList = ",self.arrOutcallList.count)
        
        
        if serType == "I"{
            arrBusinessTypeListTable = self.arrIncallList
        }else if serType == "O" {
            arrBusinessTypeListTable = self.arrOutcallList
        }else{
            arrBusinessTypeListTable = arrBusinessTypeList
        }
        
        
        
        
        if self.serType == "I" {
            self.btnIncall.setTitleColor(#colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1), for: .normal)
            self.btnOutcall.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            self.viewBtnIncallBottum.backgroundColor = #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1)
            self.viewBtnOutcallBottum.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        }else{
            self.btnOutcall.setTitleColor(#colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1), for: .normal)
            self.btnIncall.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            self.viewBtnOutcallBottum.backgroundColor = #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1)
            self.viewBtnIncallBottum.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        }
        
        
        let dict = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] ?? ["":""]
        let serviceType = dict[UserDefaults.keys.serviceType] as? String ?? ""
        
        if serviceType == "0" || serviceType == "3"{
        }else if serviceType == "1"{
            self.arrOutcallList.removeAll()
        }else if serviceType == "2"{
            self.arrIncallList.removeAll()
        }
        
        if serType == "I"{
            arrBusinessTypeListTable = self.arrIncallList
        }else if serType == "O" {
            arrBusinessTypeListTable = self.arrOutcallList
        }else{
            arrBusinessTypeListTable = arrBusinessTypeList
        }
        
        if self.arrIncallList.count == 0 && self.arrOutcallList.count == 0{
            self.removePropertyData()
        }
       self.tblBusinessLIst.reloadData()
    }
    
    
    @IBAction func btnIncall(_ sender:Any){
        serType = "I"
        if serType == "I"{
            arrBusinessTypeListTable = self.arrIncallList
        }else if serType == "O" {
            arrBusinessTypeListTable = self.arrOutcallList
        }else{
            arrBusinessTypeListTable = arrBusinessTypeList
        }
        if self.serType == "I" {
            self.btnIncall.setTitleColor(#colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1), for: .normal)
            self.btnOutcall.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            self.viewBtnIncallBottum.backgroundColor = #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1)
            self.viewBtnOutcallBottum.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        }else{
            self.btnOutcall.setTitleColor(#colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1), for: .normal)
            self.btnIncall.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            self.viewBtnOutcallBottum.backgroundColor = #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1)
            self.viewBtnIncallBottum.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        }
        self.tblBusinessLIst.reloadData()
    }
    
    @IBAction func btnOutcall(_ sender:Any){
        serType = "O"
        if serType == "I"{
            arrBusinessTypeListTable = self.arrIncallList
        }else if serType == "O" {
            arrBusinessTypeListTable = self.arrOutcallList
        }else{
            arrBusinessTypeListTable = arrBusinessTypeList
        }
        if self.serType == "I" {
            self.btnIncall.setTitleColor(#colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1), for: .normal)
            self.btnOutcall.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            self.viewBtnIncallBottum.backgroundColor = #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1)
            self.viewBtnOutcallBottum.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        }else{
            self.btnOutcall.setTitleColor(#colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1), for: .normal)
            self.btnIncall.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            self.viewBtnOutcallBottum.backgroundColor = #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1)
            self.viewBtnIncallBottum.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        }
  
            
        self.tblBusinessLIst.reloadData()
    }
    
    
    
    
    
    func saveDataInLocalDataBase(objModelClass:ModelServicesList){
        var mainPrice = objModelClass.incallPrice
        var bType = ""
        
        if self.serType == "I" {
            mainPrice = objModelClass.outcallPrice ?? "0.0"
            bType = "Outcall"
        }else if self.serType == "O"{
            mainPrice = objModelClass.incallPrice ?? "0.0"
            bType = "Incall"
        }
       
        let a = [["_id":objModelClass.mainCategoryId,
                  "mainBookingId":objModelClass.mainBookingId,
                  "mainCategoryId":objModelClass.mainCategoryId,
                  "serviceName":objModelClass.serviceName,
                  "price":float_t(mainPrice) ?? 0.0,
                  "bookingType":bType,
                  "describe":objModelClass.describe,
                  "time":objModelClass.time]]
        
        if self.serType == "O"{
            objModelClass.outcallPrice = "0.0"
        }else if self.serType == "I"{
            objModelClass.incallPrice = "0.0"
        }
        
        
        let fetchRequest: NSFetchRequest<Services> = Services.fetchRequest()
        fetchRequest.predicate =  NSPredicate(format: "mainBookingId = %@", objModelClass.mainBookingId)
        do {
            let objArray = try managedContext?.fetch(fetchRequest)
            if (objArray?.count)! > 0{
                     for object in objArray!{
                            if object.mainBookingId == objModelClass.mainBookingId &&  object.mainCategoryId == objModelClass.mainCategoryId && object.serviceName == objModelClass.serviceName{
                                 object.bookingType = bType
                                object.price = mainPrice
                                object.outcallPrice = objModelClass.outcallPrice
                                object.incallPrice = objModelClass.incallPrice
                                try managedContext?.save()
                            }
                    }
            }
        try managedContext?.save()
            self.viewWillAppear(false)
        } catch _ {
            // error handling
        }
    }
    
    
}
/////////////////


//MARK: - Webservices
fileprivate extension ServicesListVC{
    func addGesturesToView() -> Void {
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.topToBottomSWIPE))
        swipeRight.direction = .down
        self.viewButtonSet.addGestureRecognizer(swipeRight)
    }
    
    @objc func topToBottomSWIPE() ->Void {
        self.view.endEditing(true)
        self.viewButtonSet.isHidden = true
    }
    @IBAction func btnPlus(_ sender:Any){
        self.viewButtonSet.isHidden = false
    }
    
    @IBAction func btnHiddenViewButtonSet(_ sender:Any){
        self.viewButtonSet.isHidden = true
    }
    
    func callWebserviceForGetBusinessType(){
        arrBusinessType.removeAll()
        objActivity.startActivityIndicator()
        if !objServiceManager.isNetworkAvailable(){
            objActivity.stopActivity()
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        objServiceManager.requestGet(strURL: WebURL.myBusinessType, params: nil, success: { response in
            if  response["status"] as? String == "success"{
                if let data = response["data"] as? [[String:Any]]{
                    self.businessTypeCount = data.count
                    for dict in data{
                        let obj = ModelAddBusinessType.init(dict: dict)
                        self.arrBusinessType.append(obj)
                    }
                    objActivity.stopActivity()
                }
            }else{
                objActivity.stopActivity()
            }
        }) { error in
            objActivity.stopActivity()
        }
}
}

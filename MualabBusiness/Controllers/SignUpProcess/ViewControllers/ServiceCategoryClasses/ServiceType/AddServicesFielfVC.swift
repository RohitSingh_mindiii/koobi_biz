//
//  AddServicesFielfVC.swift
//  MualabBusiness
//
//  Created by Mac on 20/11/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import DropDown

var arrMain = [ModelBusinessTypeList]()

class AddServicesFielfVC: UIViewController,UITextFieldDelegate,UITextViewDelegate,UITableViewDataSource,UITableViewDelegate {
    let dropDown = DropDown()
    @IBOutlet weak var viewServiceName:UIView!
    @IBOutlet weak var viewBookingType:UIView!
    @IBOutlet weak var viewPrice:UIView!
    @IBOutlet weak var viewPriceOutCall:UIView!
    @IBOutlet weak var btnContinue:UIButton!

    @IBOutlet weak var viewServiceDescription:UIView!
    @IBOutlet weak var viewTime:UIView!
    @IBOutlet weak var timePicker : UIDatePicker!
    @IBOutlet weak var viewTimePicker : UIView!
    @IBOutlet weak var txtServiceName:UITextField!
    @IBOutlet weak var txtBookingType:UITextField!
    @IBOutlet weak var txtPrice:UITextField!
    @IBOutlet weak var txtOutCall:UITextField!
    @IBOutlet weak var tblCompany: UITableView!
    @IBOutlet weak var viewBottumTable: UIView!
    @IBOutlet weak var txtServiceDescription:UITextView!
    @IBOutlet weak var txtTime:UITextField!
    
    var viewType = ""
 
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblCompany.delegate = self
        self.tblCompany.dataSource = self
        self.viewBottumTable.isHidden = true
        self.DelegateCalling()
        self.viewTimePicker.isHidden = true
        self.timePicker.countDownDuration = 600
        self.timePicker.addTarget(self, action: #selector(self.datePickedValueChanged(sender:)), for: UIControl.Event.valueChanged)
        self.addAccesorryToKeyBoard()
        self.addGesturesToView()
    }
    func addGesturesToView() -> Void {
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.topToBottomSWIPE))
        swipeRight.direction = .down
        self.viewBottumTable.addGestureRecognizer(swipeRight)
    }
    
    @objc func topToBottomSWIPE() ->Void {
        self.view.endEditing(true)
        self.viewBottumTable.isHidden = true
    }
    
    @objc func datePickedValueChanged (sender: UIDatePicker) {
        if (self.timePicker.countDownDuration > 10800) { //5400 seconds = 1h30min
            self.timePicker.countDownDuration = 10800; //Defaults to 3 hour
        }
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.viewBottumTable.isHidden = true

        if objAppShareData.editSelfServices == true{
            self.btnContinue.setTitle("Save & Continue", for: .normal)
        }else{
            self.btnContinue.setTitle("Continue", for: .normal)
        }
        self.hiddenView()
        self.parceLocalData()
    }

}
//MARK: - Custome method Extension
fileprivate extension AddServicesFielfVC{
    func DelegateCalling(){
        self.txtTime.delegate = self
        self.txtPrice.delegate = self
        self.txtOutCall.delegate = self
        self.txtServiceDescription.delegate = self
        self.txtServiceName.delegate = self
        self.txtBookingType.delegate = self
    }
    
    
    func hiddenView(){
        
        self.viewServiceName.isHidden = true
        self.viewBookingType.isHidden = true
        self.viewPrice.isHidden = true
        self.viewServiceDescription.isHidden = true
        self.viewTime.isHidden = true
        self.viewTime.isHidden = true
        self.viewPriceOutCall.isHidden = true
        
        
        if self.viewType == "Name"{
            self.viewServiceName.isHidden = false
        }else if self.viewType == "Booking"{
            self.viewBookingType.isHidden = false
        }else if self.viewType == "Price"{
            self.viewPrice.isHidden = false
            self.viewPriceOutCall.isHidden = false
        }else if self.viewType == "Description"{
            self.viewServiceDescription.isHidden = false
        }else if self.viewType == "Time"{
            self.viewTime.isHidden = false
        }else{
            self.viewServiceName.isHidden = false
            self.viewBookingType.isHidden = false
            self.viewPrice.isHidden = false
            self.viewServiceDescription.isHidden = false
            self.viewTime.isHidden = false
         }
    }
}

//MARK: - textfield Extension
 extension AddServicesFielfVC{
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        var numberOfChars:Int = 0
        let newText = (txtServiceDescription.text as NSString).replacingCharacters(in: range, with: text)
        numberOfChars = newText.count
        
//        if numberOfChars >= 200 && text != ""{
//            return false
//        }else{
//
//        }
        
        //return numberOfChars < 200
        return true
    }

    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        
        let text = textField.text! as NSString
        if (text.length == 1)  && (string == "") {
        }else{
            var substring: String = textField.text!
            substring = (substring as NSString).replacingCharacters(in: range, with: string)
            substring = substring.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            let a = substring.count
             if textField == self.txtServiceDescription {
                if a > 200 && substring != ""{
                    return false
                }
            }else if textField == self.txtServiceName {
                if a > 50 && substring != ""{
                    return false
                }
            }else if textField == self.txtPrice || textField == self.txtOutCall {
                let numberOfDots = substring.components(separatedBy: ".")
                if numberOfDots.count > 1{
                    if numberOfDots[1].count == 3 && string != ""{
                        return false
                    }
                }
                if a > 7 && substring != ""{
                    return false
                }else{
                
                let isNumber = CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: string))
                let withDecimal = (
                    string == NumberFormatter().decimalSeparator &&
                        textField.text?.contains(string) == false
                )
                let maxLength = 7
                let currentString: NSString = textField.text! as NSString
                let newString: NSString =
                    currentString.replacingCharacters(in: range, with: string) as NSString
                return newString.length <= maxLength && isNumber || withDecimal
                }
                }else{
                return true
             }
             return true
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if self.viewType != ""{
        if textField == self.txtServiceName{
            txtServiceName.resignFirstResponder()
        }
        if textField == self.txtBookingType{
            txtServiceName.resignFirstResponder()
        }
        if textField == self.txtServiceDescription{
            txtServiceDescription.resignFirstResponder()
        }
        if textField == self.txtPrice{
            txtPrice.resignFirstResponder()
        }
        if textField == self.txtTime{
            txtTime.resignFirstResponder()
        }
    }else{
            if textField == self.txtServiceName{
                txtServiceName.becomeFirstResponder()
            }else if textField == self.txtBookingType{
                txtServiceName.becomeFirstResponder()
            }else if textField == self.txtPrice{
                txtPrice.becomeFirstResponder()
            }else if textField == self.txtServiceDescription{
                txtServiceDescription.becomeFirstResponder()
            }else if textField == self.txtTime{
                txtTime.resignFirstResponder()
            }
        }
       
        return true
    }
}
//MARK: - Button Extension
fileprivate extension AddServicesFielfVC{
    @IBAction func btnContinew(_ sender:Any){
        saveData()
     }
    @IBAction func btnBack(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnTime(_ sender:Any){
        self.viewTimePicker.isHidden = false
    }
  
    func finalDict(){
        let obj = objAppShareData.objModelFinalSubCategory
        let dict = obj.param
    }
    
    func saveData(){
        
        self.txtServiceName.text = removeWS(textfield: self.txtServiceName)
        self.txtOutCall.text = removeWS(textfield: self.txtOutCall)
        self.txtPrice.text = removeWS(textfield: self.txtPrice)
        ////
        let doubleStr = String(format: "%.2f", Double(self.txtPrice.text!) ?? 0.0)
        self.txtPrice.text = doubleStr
        let doubleStrMM = String(format: "%.2f", Double(self.txtOutCall.text!) ?? 0.0)
        self.txtOutCall.text = doubleStrMM
        ////
        
        self.txtServiceDescription.text = self.txtServiceDescription.text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)

        var chack = true
        if self.viewType == "Name"{
            if self.txtServiceName.text == ""{
                chack = false
                objAppShareData.showAlert(withMessage: "Please enter service name", type: alertType.bannerDark,on: self)
             }else{
                objAppShareData.objModelFinalSubCategory.param[objAppShareData.objModelFinalSubCategory.serviceName] = self.txtServiceName.text ?? ""
              }

        }else if self.viewType == "Booking"{
            if self.txtBookingType.text == ""{
                chack = false
                 objAppShareData.showAlert(withMessage: "Please enter business type", type: alertType.bannerDark,on: self)
             }else{
                objAppShareData.objModelFinalSubCategory.param[objAppShareData.objModelFinalSubCategory.bookingType] = self.txtBookingType.text ?? ""
            }
        }else if self.viewType == "Price"{
 
let priceType = objAppShareData.objModelFinalSubCategory.param[objAppShareData.objModelFinalSubCategory.bookingType]
            let priceDouble = Double(self.txtPrice.text!) ?? 0.0
            let priceOutDouble = Double(self.txtOutCall.text!) ?? 0.0
            if priceType == "Incall"{
                if self.txtPrice.text == "" || self.txtPrice.text == "."{
                    chack = false
                    objAppShareData.showAlert(withMessage: "Please enter incall price", type: alertType.bannerDark,on: self)
                }else{
                    if float_t(self.txtPrice.text!) == 0{
                        chack = false
                        objAppShareData.showAlert(withMessage: "Please enter incall price", type: alertType.bannerDark,on: self)
                    }else if priceDouble < 5.0 {
                        chack = false
                       objAppShareData.showAlert(withMessage: "Please enter a amount that is no lesser than £5", type: alertType.bannerDark,on: self)
                    }else{
                    objAppShareData.objModelFinalSubCategory.param[objAppShareData.objModelFinalSubCategory.incallPrice] = self.txtPrice.text ?? ""
                    }
                }
            }else if priceType == "Outcall"{
                if self.txtOutCall.text == "" || self.txtOutCall.text == "."{
                    chack = false
                     objAppShareData.showAlert(withMessage: "Please enter outcall price", type: alertType.bannerDark,on: self)
                 }else{
                    if float_t(self.txtOutCall.text!) == 0{
                        chack = false

                        objAppShareData.showAlert(withMessage: "Please enter outcall price", type: alertType.bannerDark,on: self)
                    }else if priceOutDouble < 5.0 {
                        chack = false
                        objAppShareData.showAlert(withMessage: "Please enter a amount that is no lesser than £5", type: alertType.bannerDark,on: self)
                    }else{
                        objAppShareData.objModelFinalSubCategory.param[objAppShareData.objModelFinalSubCategory.outCallPrice] = self.txtOutCall.text ?? ""
                    }
                }
            }else{
                
                if self.txtPrice.text == ""{
                    chack = false
                    objAppShareData.showAlert(withMessage: "Please enter price", type: alertType.bannerDark,on: self)
                }else if self.txtOutCall.text == ""{
                    chack = false
                    objAppShareData.showAlert(withMessage: "Please enter outcall price", type: alertType.bannerDark,on: self)
                }else if self.txtPrice.text != "" && float_t(self.txtPrice.text!) == 0  || self.txtPrice.text == "."{
                    chack = false
                    objAppShareData.showAlert(withMessage: "Please enter incall price", type: alertType.bannerDark,on: self)
                }else if  self.txtOutCall.text != "" && float_t(self.txtOutCall.text!) == 0 || self.txtOutCall.text == "."{
                    chack = false
                    objAppShareData.showAlert(withMessage: "Please enter outcall price", type: alertType.bannerDark,on: self)
                }else if (priceOutDouble < 5.0) || (priceDouble < 5.0){
                    chack = false
                    objAppShareData.showAlert(withMessage: "Please enter a amount that is no lesser than £5", type: alertType.bannerDark,on: self)
                }else{
                    objAppShareData.objModelFinalSubCategory.param[objAppShareData.objModelFinalSubCategory.incallPrice] = self.txtPrice.text ?? ""
                    objAppShareData.objModelFinalSubCategory.param[objAppShareData.objModelFinalSubCategory.outCallPrice] = self.txtOutCall.text ?? ""
                }
                
                
                }
            } else if self.viewType == "Description"{
            if self.txtServiceDescription.text == ""{
                chack = false

                objAppShareData.showAlert(withMessage: "Please enter description", type: alertType.bannerDark,on: self)

            }else{
                objAppShareData.objModelFinalSubCategory.param[objAppShareData.objModelFinalSubCategory.descriptions] = self.txtServiceDescription.text ?? ""
            }
        }else if self.viewType == "Time"{
            if self.txtTime.text == ""{
                chack = false

                objAppShareData.showAlert(withMessage: "Please select completion time", type: alertType.bannerDark,on: self)

            }else{
                objAppShareData.objModelFinalSubCategory.param[objAppShareData.objModelFinalSubCategory.completionTime] = self.txtTime.text ?? ""
            }
        }else{
            
        }
        print(objAppShareData.objModelFinalSubCategory.param)
        if chack == true{
            self.navigationController?.popViewController(animated: true)
        }

    }

    
    
    func parceLocalData(){
        if viewType == "Price"{
            self.priceViewManage()
        }
        self.txtServiceName.text = objAppShareData.objModelFinalSubCategory.param[objAppShareData.objModelFinalSubCategory.serviceName]
         
        self.txtOutCall.text = objAppShareData.objModelFinalSubCategory.param[objAppShareData.objModelFinalSubCategory.outCallPrice]
        self.txtPrice.text = objAppShareData.objModelFinalSubCategory.param[objAppShareData.objModelFinalSubCategory.incallPrice]
        self.txtBookingType.text = objAppShareData.objModelFinalSubCategory.param[objAppShareData.objModelFinalSubCategory.bookingType]
        self.txtServiceDescription.text = objAppShareData.objModelFinalSubCategory.param[objAppShareData.objModelFinalSubCategory.descriptions]
        self.txtTime.text = objAppShareData.objModelFinalSubCategory.param[objAppShareData.objModelFinalSubCategory.completionTime]
    }
    
    
    func priceViewManage(){
            let priceType = objAppShareData.objModelFinalSubCategory.param[objAppShareData.objModelFinalSubCategory.bookingType]
            self.viewPriceOutCall.isHidden = true
            self.viewPrice.isHidden = true
            
            if priceType == "Incall"{
                self.viewPrice.isHidden = false
            }else if priceType == "Outcall"{
                self.viewPriceOutCall.isHidden = false
            }else if priceType == "Both"{
                self.viewPriceOutCall.isHidden = false
                self.viewPrice.isHidden = false
            }else{
                self.viewPriceOutCall.isHidden = true
                self.viewPrice.isHidden = false
        }
    }
}

//MARK: - popup and dropdown button
fileprivate extension AddServicesFielfVC{
    func setuoDropDownAppearance()  {
        let appearance = DropDown.appearance()
        appearance.cellHeight = 40
        appearance.backgroundColor = UIColor(white: 1, alpha: 1)
        appearance.selectionBackgroundColor = UIColor(red: 0.6494, green: 0.8155, blue: 1.0, alpha: 0.2)
        appearance.separatorColor = UIColor(white: 0.7, alpha: 0.8)
        appearance.cornerRadius = 10
        appearance.shadowColor = UIColor(white: 0.6, alpha: 1)
        appearance.shadowOpacity = 0.9
        appearance.shadowRadius = 25
        appearance.animationduration = 0.25
        appearance.textColor = .darkGray
    }
    
    @IBAction func btnBookingType(_ sender: UIButton) {
         let businessType = UserDefaults.standard.string(forKey: UserDefaults.keys.mainServiceType)
        var strType = ""
        if businessType == "1"{
            strType = "Incall"
        }else if businessType == "2"{
            strType = "Outcall"
        }else{
            strType = "Both"
        }
        if strType == "Incall"{
        }else if strType == "Outcall"{
        }else{
            strType = "Both"
        }
       if strType == "Both"{
        let arrNew = ["Both","Incall","Outcall"]
        self.view.endEditing(true)
        let objGetData = arrNew.map { $0 }
        guard objGetData.isEmpty == false else {
            objAppShareData.showAlert(withMessage: "No booking type found", type: alertType.bannerDark,on: self)
            
            return
        }
        
        self.tblCompany.reloadData()
        self.viewBottumTable.isHidden = false
        }
}
}


//MARK:- phonepad return keyboard
extension AddServicesFielfVC{
    
    @IBAction func btnDoneTimeSelection(_ sender:UIButton){
        if sender.tag == 0{
            let time = self.timePicker.date
            let components = Calendar.current.dateComponents([.hour, .minute], from: time)
            let hour = components.hour!
            let minute = components.minute!
            var sH = String(hour)
            var sM = String(minute)
            if sH.count == 1{
                sH = "0" + sH
            }
            if sM.count == 1{
                sM = "0" + sM
            }
            if Int(sH) ?? 00 >= 3 {
                sH = "03"
                sM = "00"
            }
                self.txtTime.text = "\(sH):\(sM)"
            
        }
        self.viewTimePicker.isHidden = true
//        self.timePickerBottem.constant = -self.view.frame.size.height/1.5
//        UIView.animate(withDuration: 0.5) {
//            self.view.layoutIfNeeded()
//        }
    }

    
    
    func addAccesorryToKeyBoard(){
        let keyboardDoneButtonView = UIToolbar()
        keyboardDoneButtonView.sizeToFit()
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style:.plain, target: self, action: #selector(self.resignKeyBoard))
        doneButton.tintColor = appColor
        
        keyboardDoneButtonView.items = [flexBarButton, doneButton]
        
        txtPrice.inputAccessoryView = keyboardDoneButtonView
        txtOutCall.inputAccessoryView = keyboardDoneButtonView
        txtServiceDescription.inputAccessoryView = keyboardDoneButtonView
    }
    @objc func resignKeyBoard(){
        txtOutCall.endEditing(true)
        txtPrice.endEditing(true)
        txtServiceDescription.endEditing(true)
        self.view.endEditing(true)
     }
    
    
    
    func removeWS(textfield:UITextField) -> String {
    var text = textfield.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    return text
    }
    
    
}
extension AddServicesFielfVC{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "CellBottumTableList"
        let array =  ["Both","Incall","Outcall"]
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? CellBottumTableList{
            cell.lblTitle.text = array[indexPath.row]
            return cell
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let array =  ["Both","Incall","Outcall"]
        self.viewBottumTable.isHidden = true
        self.txtBookingType.text = array[indexPath.row]
        let a = objAppShareData.objModelFinalSubCategory
        a.param[a.outCallPrice]  =  ""
        a.param[a.incallPrice] = ""
        objAppShareData.objModelFinalSubCategory.param[objAppShareData.objModelFinalSubCategory.bookingType] = self.txtBookingType.text ?? ""
    }
    @IBAction func btnHiddenTable(_ sender:Any){
        self.viewBottumTable.isHidden = true
    }
}

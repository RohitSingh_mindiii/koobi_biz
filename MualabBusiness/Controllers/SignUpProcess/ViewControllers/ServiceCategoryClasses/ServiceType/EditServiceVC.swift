//
//  EditServiceVC.swift
//  MualabBusiness
//
//  Created by Mac on 22/11/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import DropDown
import CoreData

class EditServiceVC: UIViewController,UITextFieldDelegate,UITextViewDelegate {
    
    let dropDown = DropDown()
    var businessName = ""
    var categoryName = ""
    
    var objModelClass = ModelServicesList(dict: [:])

        @IBOutlet weak var lblHeader : UILabel!
        @IBOutlet weak var timePicker : UIDatePicker!

        @IBOutlet weak var lblDescription:UILabel!
        @IBOutlet weak var txtOnlyPrice:UITextField!
        @IBOutlet weak var btnContinue:UIButton!

    
    
        @IBOutlet weak var img1:UIImageView!
        @IBOutlet weak var img2:UIImageView!
        @IBOutlet weak var img3:UIImageView!

    
        @IBOutlet weak var viewBusins:UIView!
        @IBOutlet weak var viewCategory:UIView!
        @IBOutlet weak var viewOnlyPrice: UIView!
        @IBOutlet weak var viewTimePicker : UIView!
        @IBOutlet weak var viewSupper:UIView!
        @IBOutlet weak var viewBookingType:UIView!
        @IBOutlet weak var viewPrice:UIView!
        @IBOutlet weak var viewPriceOutCall:UIView!
    
        @IBOutlet weak var txtBusinessName:UITextField!
        @IBOutlet weak var txtCategoryName:UITextField!
        @IBOutlet weak var txtServiceName:UITextField!
        @IBOutlet weak var txtBookingType:UITextField!
        @IBOutlet weak var txtPrice:UITextField!
        @IBOutlet weak var txtOutCall:UITextField!
    
        @IBOutlet weak var txtServiceDescription:UITextView!
        @IBOutlet weak var txtTime:UITextField!
   
    var serviceName = ""

    var managedContext:NSManagedObjectContext?

        var viewType = ""
        
        override func viewDidLoad() {
            super.viewDidLoad()
            guard let appDelegate =
                UIApplication.shared.delegate as? AppDelegate else {
                    return
            }
            managedContext =
                appDelegate.persistentContainer.viewContext
            self.DelegateCalling()
            self.viewTimePicker.isHidden = true
            self.timePicker.countDownDuration = 600
            
            self.timePicker.addTarget(self, action: #selector(self.datePickedValueChanged(sender:)), for: UIControl.Event.valueChanged)
            
            self.addAccesorryToKeyBoard()
        }
    
        @objc func datePickedValueChanged (sender: UIDatePicker) {
            if (self.timePicker.countDownDuration > 10800) { //5400 seconds = 1h30min
                self.timePicker.countDownDuration = 10800; //Defaults to 3 hour
            }
        }
 
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
        
        override func viewWillAppear(_ animated: Bool) {
            if objAppShareData.editSelfServices == true{
                self.btnContinue.setTitle("Save & Continue", for: .normal)
            }else{
                self.btnContinue.setTitle("Continue", for: .normal)
            }
            //DispatchQueue.main.async {
                self.parseDataFromLocalDataBase()
            //}
            if viewType == "YES"{
                self.lblHeader.text = "Service Details"
                self.viewSupper.isHidden = false
                self.txtServiceDescription.isHidden = true
                self.lblDescription.isHidden = false
             }else{
                self.lblHeader.text = "Edit Service"
                self.viewSupper.isHidden = true
                self.txtServiceDescription.isHidden = false
                self.lblDescription.isHidden = true
             }
            self.configureView()
        }
    
    
    
    func configureView(){
        if viewType == "YES"{
            self.lblHeader.text = "Service Details"
            self.viewSupper.isHidden = false
            self.img1.isHidden = true
            self.img2.isHidden = true
            self.img3.isHidden = true
            self.viewBusins.isHidden = false
            self.viewCategory.isHidden = false
        }else{
            self.viewBusins.isHidden = true
            self.viewCategory.isHidden = true
            self.lblHeader.text = "Edit Service"
            self.viewSupper.isHidden = true
            self.img1.isHidden = false
            self.img2.isHidden = false
            self.img3.isHidden = false
        }
     }
 }

    //MARK: - Custome method Extension
    fileprivate extension EditServiceVC{
        func DelegateCalling(){
            self.txtTime.delegate = self
            self.txtPrice.delegate = self
            self.txtOutCall.delegate = self

            self.txtServiceDescription.delegate = self
            self.txtServiceName.delegate = self
            self.txtBookingType.delegate = self
        }
     }
    
    //MARK: - textfield Extension
    extension EditServiceVC{
        func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
            
            var numberOfChars:Int = 0
            let newText = (txtServiceDescription.text as NSString).replacingCharacters(in: range, with: text)
            numberOfChars = newText.count
            
            if numberOfChars >= 200 && text != ""{
                return false
            }else{
            }
            
            return numberOfChars < 200
        }
        func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
            
            let text = textField.text! as NSString
            if (text.length == 1)  && (string == "") {
            }else{
                var substring: String = textField.text!
                substring = (substring as NSString).replacingCharacters(in: range, with: string)
                substring = substring.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                let a = substring.count
                if textField == self.txtServiceDescription {
                    if a > 200 && substring != ""{
                        return false
                    }
                }else if textField == self.txtServiceName {
                    if a > 50 && substring != ""{
                        return false
                    }
 
                }else if textField == self.txtPrice || textField == self.txtOutCall {
                    let numberOfDots = substring.components(separatedBy: ".")
                    if numberOfDots.count > 1{
                        if numberOfDots[1].count == 3 && string != ""{
                            return false
                        }
                    }
                    let isNumber = CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: string))
                    let withDecimal = (
                        string == NumberFormatter().decimalSeparator &&
                            textField.text?.contains(string) == false
                    )
                    let maxLength = 7
                    let currentString: NSString = textField.text! as NSString
                    let newString: NSString =
                        currentString.replacingCharacters(in: range, with: string) as NSString
                    return newString.length <= maxLength && isNumber || withDecimal
                }else{
                    return true
                }
                return true
                
            }
            return true
        }
        
        func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            textField.resignFirstResponder()

                if textField == self.txtServiceName{
                    txtServiceName.resignFirstResponder()
                }else if textField == self.txtPrice{
                    txtPrice.resignFirstResponder()
                }else if textField == self.txtOutCall{
                    txtServiceDescription.resignFirstResponder()
                }else if textField == self.txtServiceDescription{
                    txtServiceDescription.resignFirstResponder()
                }else if textField == self.txtServiceDescription{
                    txtServiceDescription.resignFirstResponder()
            }
            return true
        }
    }
    //MARK: - Button Extension
    fileprivate extension EditServiceVC{
        @IBAction func btnContinew(_ sender:Any){
            saveData()
        }
        
        @IBAction func btnBack(_ sender:Any){
        objAppShareData.objModelFinalSubCategory.param = [:]
            objAppShareData.manageNavigation = false
            self.backVC()
        }
        
        func alert(str:String){
            self.backVC()
            objAppShareData.showAlert(withMessage: str, type: alertType.bannerDark,on: self)
        }
        func backVC(){
            objAppShareData.manageNavigation = false
            objAppShareData.objModelFinalSubCategory.param = [:]
            self.navigationController?.popViewController(animated: true)
         }
        
//        @IBAction func btnTime(_ sender:Any){
//            self.viewTimePicker.isHidden = false
//        }
        
        func finalDict(){
            let obj = objAppShareData.objModelFinalSubCategory
            let dict = obj.param
            print(dict)
        }
 
       func parseDataFromLocalDataBase(){
        let businessType = UserDefaults.standard.string(forKey: UserDefaults.keys.mainServiceType)
        var strType = ""
        if businessType == "1"{
            strType = "Incall"
        }else if businessType == "2"{
            strType = "Outcall"
        }else{
            strType = "Both"
        }
        if strType == "Incall"{
        }else if strType == "Outcall"{
        }else{
            strType = "Both"
            self.viewBookingType.isHidden = false
        }

        
        let a = objAppShareData.objModelFinalSubCategory
        
        objModelClass.time = a.param[a.completionTime] ?? "00:00"
        objModelClass.bookingType = a.param[a.bookingType] ?? ""
        objModelClass.incallPrice = a.param[a.incallPrice] ?? "0.0"
        objModelClass.outcallPrice = a.param[a.outCallPrice] ?? "0.0"
        
        self.txtBookingType.text = objModelClass.bookingType
        self.txtServiceName.text = objModelClass.serviceName
        self.txtServiceDescription.text = objModelClass.describe
        self.lblDescription.text = objModelClass.describe

        self.txtTime.text = objModelClass.time
 
        self.txtBusinessName.text = objAppShareData.objEditServiceVC.businessName
        self.txtCategoryName.text = objAppShareData.objEditServiceVC.categoryName

        if objModelClass.bookingType == "Both"{
            objModelClass.price = a.param[a.incallPrice] ?? "0.0"
        }else if objModelClass.bookingType == "Incall"{
            objModelClass.price = a.param[a.incallPrice] ?? "0.0"
        }else if objModelClass.bookingType == "Outcall"{
            objModelClass.price = a.param[a.outCallPrice] ?? "0.0"
        }
 
            self.txtOutCall.text = objModelClass.outcallPrice
            self.txtPrice.text = objModelClass.price
            self.txtOnlyPrice.text = objModelClass.price
        
        /////
        let doubleStr = String(format: "%.2f", ceil(Double(objModelClass.outcallPrice)!))
        self.txtOutCall.text = doubleStr
        
        let doubleStrNN = String(format: "%.2f", ceil(Double(objModelClass.price)!))
        self.txtPrice.text = doubleStrNN
        self.txtOnlyPrice.text = doubleStrNN
        /////
        
        print("objModelClass.bookingType = ",objModelClass.bookingType)

        self.viewBookingType.isHidden = false
        self.viewPrice.isHidden = false
        
        
        if viewType == "YES"{
            self.viewOnlyPrice.isHidden = true
            self.viewBookingType.isHidden = false
            if objModelClass.bookingType == "Both"{
                self.viewPriceOutCall.isHidden = false
                self.viewPrice.isHidden = false
            }else if objModelClass.bookingType == "Incall"{
                self.viewPriceOutCall.isHidden = true
                 self.viewPrice.isHidden = false
            }else if objModelClass.bookingType == "Outcall"{
                self.viewPrice.isHidden = true
                self.viewPriceOutCall.isHidden = false
            }
            
//            let inCall = "£"+(self.txtPrice.text ?? "")
//            let outCall = "£"+(self.txtOutCall.text ?? "")
            
            let inCall = self.txtPrice.text ?? ""
            let outCall = self.txtOutCall.text ?? ""
            self.txtPrice.text = inCall
            self.txtOutCall.text = outCall
            
            /////
            let doubleStr = String(format: "%.2f", ceil(Double(self.txtPrice.text!)!))
            self.txtPrice.text = doubleStr
            let doubleStrNN = String(format: "%.2f", ceil(Double(self.txtOutCall.text!)!))
            self.txtOutCall.text = doubleStrNN
            ////
        }else{
            self.viewBookingType.isHidden = false

            if objModelClass.bookingType == "Both"{
                self.viewBookingType.isHidden = false
                self.txtOnlyPrice.text = objModelClass.price
                /////
                let doubleStr = String(format: "%.2f", ceil(Double(self.txtOnlyPrice.text!)!))
                self.txtOnlyPrice.text = doubleStr
                ////
            }else if objModelClass.bookingType == "Incall"{
                self.txtOnlyPrice.text = objModelClass.incallPrice
                /////
                let doubleStr = String(format: "%.2f", ceil(Double(self.txtOnlyPrice.text!)!))
                self.txtOnlyPrice.text = doubleStr
                ////
            }else if objModelClass.bookingType == "Outcall"{
                self.txtOnlyPrice.text = objModelClass.outcallPrice
                /////
                let doubleStr = String(format: "%.2f", ceil(Double(self.txtOnlyPrice.text!)!))
                self.txtOnlyPrice.text = doubleStr
                ////
            }
            self.viewPriceOutCall.isHidden = true
            self.viewPrice.isHidden = true
            self.viewOnlyPrice.isHidden = false
        }
        
    }
        
        func saveData(){
            
            let a = objAppShareData.objModelFinalSubCategory
            
            let outcallPrice = a.param[a.outCallPrice] ?? "0.0"
            let incallPrice = a.param[a.incallPrice] ?? "0.0"

            
            self.txtServiceName.text = removeWS(textfield: self.txtServiceName)
            self.txtOutCall.text = removeWS(textfield: self.txtOutCall)
            self.txtPrice.text = removeWS(textfield: self.txtPrice)
            self.txtServiceDescription.text = self.txtServiceDescription.text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            var chack = true
            let doublePrice = Double(incallPrice) ?? 0.0
            if self.txtServiceName.text == ""{
                    chack = false
                    objAppShareData.showAlert(withMessage: "Please enter business name", type: alertType.bannerDark,on: self)
            }else if self.txtBookingType.text == ""{
                    chack = false
                    objAppShareData.showAlert(withMessage: "Please enter business type", type: alertType.bannerDark,on: self)
             }else if self.txtOnlyPrice.text == "" || self.txtOnlyPrice.text == nil{
                chack = false
                objAppShareData.showAlert(withMessage: "Please enter price", type: alertType.bannerDark,on: self)
            }else if self.txtOnlyPrice.text != ""{
                let a = float_t(self.txtOnlyPrice.text ?? "0.0")
                if a == 0{
                    chack = false
                    objAppShareData.showAlert(withMessage: "Please enter price", type: alertType.bannerDark,on: self)
                }
            }else if doublePrice < 5.0 {
                chack = false
                objAppShareData.showAlert(withMessage: "Please enter a amount that is no lesser than £5", type: alertType.bannerDark,on: self)
            }
            let incal = float_t(incallPrice)
            let outcal = float_t(outcallPrice)
            if self.txtBookingType.text == "Both"{
                if incal == 0 || outcal == 0{
                    chack = false
                    objAppShareData.showAlert(withMessage: "Please enter price", type: alertType.bannerDark,on: self)
                }
            }else if self.txtBookingType.text == "Both" && (incal! < 5.0 || outcal! < 5.0){
                chack = false
                objAppShareData.showAlert(withMessage: "Please enter a amount that is no lesser than £5", type: alertType.bannerDark,on: self)
            }
            if self.txtTime.text == "" || self.txtTime.text == "00:00"{
                chack = false
                objAppShareData.showAlert(withMessage: "Please select time", type: alertType.bannerDark,on: self)
            }
            if self.txtServiceDescription.text == ""{
                    chack = false
                    objAppShareData.showAlert(withMessage: "Please enter description", type: alertType.bannerDark,on: self)
            }

            if chack == true{
                self.saveDataInLocalDataBase()
            }
            
        }
        
      func saveDataInLocalDataBase(){
        var mainPrice = objModelClass.incallPrice
        if self.txtBookingType.text == "Outcall"{
            mainPrice = objModelClass.outcallPrice
        }

        let a = [["_id":objModelClass.mainCategoryId,
                  "mainBookingId":objModelClass.mainBookingId,
                  "mainCategoryId":objModelClass.mainCategoryId,
                  "serviceName":self.txtServiceName.text ?? "",
                  "price":float_t(mainPrice) ?? 0.0,
                  "bookingType":self.txtBookingType.text ?? "",
                  "describe":self.txtServiceDescription.text ?? "",
                  "time":self.txtTime.text ?? ""]]
        
        if self.txtBookingType.text == "Incall"{
            objModelClass.outcallPrice = "0.0"
        }else if self.txtBookingType.text == "Outcall"{
            objModelClass.incallPrice = "0.0"
        }
        
        
        let fetchRequest: NSFetchRequest<Services> = Services.fetchRequest()
        fetchRequest.predicate =  NSPredicate(format: "mainBookingId = %@", objModelClass.mainBookingId)
        do {
            let objArray = try managedContext?.fetch(fetchRequest)
            if (objArray?.count)! > 0{
                
                var addOrNot = ""
            for object in objArray!{
                if serviceName.capitalized == (self.txtServiceName.text ?? "").capitalized{
                    break
                }else if object.mainBookingId == objModelClass.mainBookingId &&  object.mainCategoryId == objModelClass.mainCategoryId && object.serviceName?.capitalized == (self.txtServiceName.text ?? "").capitalized  {
                    addOrNot = "Already"
                    objAppShareData.showAlert(withMessage: "Service already addad", type: alertType.bannerDark,on: self)
                    break
                }
                }
                
                
                
                
                if addOrNot == ""{
                for object in objArray!{
                    
                    if serviceName.capitalized == (self.txtServiceName.text ?? "").capitalized{
                        if object.mainBookingId == objModelClass.mainBookingId &&  object.mainCategoryId == objModelClass.mainCategoryId && object.serviceName?.capitalized == serviceName.capitalized{
                            addOrNot = "Add"
                            object.bookingType = self.txtBookingType.text ?? ""
                            object.price = mainPrice
                            object.outcallPrice = objModelClass.outcallPrice
                            object.incallPrice = objModelClass.incallPrice
                            object.describe = self.txtServiceDescription.text ?? ""
                            object.time = self.txtTime.text ?? ""
                            object.serviceName = self.txtServiceName.text ?? ""
                            try managedContext?.save()
                            self.alert(str: "Service updated successfully")
                            break
                            
                        }
                    }else if object.mainBookingId == objModelClass.mainBookingId &&  object.mainCategoryId == objModelClass.mainCategoryId && object.serviceName?.capitalized == (self.txtServiceName.text ?? "").capitalized  {
                        addOrNot = "Already"
                        objAppShareData.showAlert(withMessage: "Service already addad", type: alertType.bannerDark,on: self)
                        
                        break
                        
                    }else  if object.mainBookingId == objModelClass.mainBookingId &&  object.mainCategoryId == objModelClass.mainCategoryId && object.serviceName?.capitalized == serviceName.capitalized  {
                        addOrNot = "Add"
                        object.bookingType = self.txtBookingType.text ?? ""
                        object.price = mainPrice
                        object.outcallPrice = objModelClass.outcallPrice
                        object.incallPrice = objModelClass.incallPrice
                        object.describe = self.txtServiceDescription.text ?? ""
                        object.time = self.txtTime.text ?? ""
                        object.serviceName = self.txtServiceName.text ?? ""
                        try managedContext?.save()
                        self.alert(str: "Service updated successfully")
                        break
                    }
                }
                }
                
                
                
                
            }
            try managedContext?.save()
             //self.alert(str: "Service updated successfully")
            } catch _ {
                // error handling
             }
        }
        
       func saveDataInModel(){
        objModelClass.serviceName = self.txtServiceName.text ?? ""
        objModelClass.describe = self.txtServiceDescription.text ?? ""
        }
}

    
    //MARK: - popup and dropdown button
    fileprivate extension EditServiceVC{
        func setuoDropDownAppearance()  {
            let appearance = DropDown.appearance()
            appearance.cellHeight = 40
            appearance.backgroundColor = UIColor(white: 1, alpha: 1)
            appearance.selectionBackgroundColor = UIColor(red: 0.6494, green: 0.8155, blue: 1.0, alpha: 0.2)
            appearance.separatorColor = UIColor(white: 0.7, alpha: 0.8)
            appearance.cornerRadius = 10
            appearance.shadowColor = UIColor(white: 0.6, alpha: 1)
            appearance.shadowOpacity = 0.9
            appearance.shadowRadius = 25
            appearance.animationduration = 0.25
            appearance.textColor = .darkGray
        }
        @IBAction func btnPrice(_ sender:Any){
            self.view.endEditing(true)
            saveDataInModel()
            self.gotoNextVC(strType: "Price")
        }
        
  
        @IBAction func btnTime(_ sender:Any){
            self.view.endEditing(true)
            saveDataInModel()
          //  self.gotoNextVC(strType: "Time")
            objAppShareData.manageNavigation = false

            let sb = UIStoryboard(name:"SignUpProcess",bundle:Bundle.main)
            let objChooseType = sb.instantiateViewController(withIdentifier:"AddServiceTimeVC") as! AddServiceTimeVC
            //objChooseType.viewType = strType
            self.navigationController?.pushViewController(objChooseType, animated: true)
        }
        @IBAction func btnBookingType(_ sender: UIButton) {
            self.view.endEditing(true)
            saveDataInModel()
            self.gotoNextVC(strType: "Booking")
   }
        
        
        func gotoNextVC(strType:String){
            objAppShareData.manageNavigation = false
                let sb = UIStoryboard(name:"SignUpProcess",bundle:Bundle.main)
                let objChooseType = sb.instantiateViewController(withIdentifier:"AddServicesFielfVC") as! AddServicesFielfVC
                objChooseType.viewType = strType
                self.navigationController?.pushViewController(objChooseType, animated: true)
            
        }
    }
    
    
    //MARK:- phonepad return keyboard
    extension EditServiceVC{
        
        @IBAction func btnDoneTimeSelection(_ sender:UIButton){
            if sender.tag == 0{
                let time = self.timePicker.date
                let components = Calendar.current.dateComponents([.hour, .minute], from: time)
                let hour = components.hour!
                let minute = components.minute!
                var sH = String(hour)
                var sM = String(minute)
                if sH.count == 1{
                    sH = "0" + sH
                }
                if sM.count == 1{
                    sM = "0" + sM
                }
                
                if Int(sH) ?? 00 >= 3 {
                    sH = "03"
                    sM = "00"
                }
                self.txtTime.text = "\(sH):\(sM)"
            }
            self.viewTimePicker.isHidden = true
        }
        
        func addAccesorryToKeyBoard(){
            let keyboardDoneButtonView = UIToolbar()
            keyboardDoneButtonView.sizeToFit()
            let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
            let doneButton = UIBarButtonItem(title: "Done", style:.plain, target: self, action: #selector(self.resignKeyBoard))
            doneButton.tintColor = appColor
            
            keyboardDoneButtonView.items = [flexBarButton, doneButton]
            
            txtPrice.inputAccessoryView = keyboardDoneButtonView
            txtOutCall.inputAccessoryView = keyboardDoneButtonView
            txtServiceDescription.inputAccessoryView = keyboardDoneButtonView
        }
        @objc func resignKeyBoard(){
            txtOutCall.endEditing(true)
            txtPrice.endEditing(true)
            txtServiceDescription.endEditing(true)
            self.view.endEditing(true)
        }
        
        
        
        func removeWS(textfield:UITextField) -> String {
            var text = textfield.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            return text
        }
        
        
}

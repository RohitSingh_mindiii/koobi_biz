//
//  OutcallOptionVC.swift
//  MualabBusiness
//
//  Created by Mac on 17/01/5018 .
//  Copyright © 5018 Mindiii. All rights reserved.
//

import UIKit
import CoreGraphics
import GoogleMaps

class OutcallOptionVC: UIViewController {
    var callOption: Int = 1
    fileprivate var tappedTimeTag: Int = 0
    //@IBOutlet weak var timePicker : UIDatePicker!

    var fromAddress = ""
    var radiusLocal = ""
    var addressData = false
    @IBOutlet weak var radiusSlider:customSlider!
    @IBOutlet weak var lblRadius1:UILabel!
var address = ""
    @IBOutlet weak var lblRadiusType:UILabel!
    @IBOutlet weak var lblHeader:UILabel!
    @IBOutlet weak var lblSecondHeading:UILabel!

    @IBOutlet weak var txtPlaceName:UITextField!
    @IBOutlet weak var lblPlaceName:UILabel!

    @IBOutlet weak var btnContinue : UIButton!
    @IBOutlet weak var radiusView : UIStackView!
    @IBOutlet weak var mapView:GMSMapView!
    var paramAddDict = ["":""]
}

// MARK: - View hirarchy
extension OutcallOptionVC{
    override func viewDidLoad() {
        super.viewDidLoad()
        

        addressData = true
        //Hold old address
        self.paramAddDict = objAppShareData.paramAddress
        self.addAccessoryView()
        self.addGasturesToViews()
        self.radiusSlider.setThumbImage(UIImage.init(named: "dotAppColor"),for:.normal)
 
        //self.radiusSlider.setThumbImage(UIImage.init(named: "gubbara_ico"),for:.highlighted)
        self.radiusSlider.setThumbImage(UIImage.init(named: "gubbara_ico_PINNew"),for:.highlighted)
            self.radiusSlider.minimumTrackTintColor = appColor

        self.txtPlaceName.text = self.address
        self.lblPlaceName.text = self.address
    }
 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        radiusLocal = objAppShareData.objModelBusinessSetup.radius
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.setAddress()
        }
        self.configureView()
        self.setDesign()
        
     }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        ////
        let thumbImagenn = textToImage(drawText: "", inImage: UIImage.init(named: "dotAppColor")!, atPoint: CGPoint.init(x:14,y:8))
        self.radiusSlider.setThumbImage(thumbImagenn, for:.normal)
        ////
        ////
        let thumbImage = textToImage(drawText: "", inImage: UIImage.init(named: "dotAppColor")!, atPoint: CGPoint.init(x:14,y:8))
        self.radiusSlider.setThumbImage(thumbImage, for:.reserved)
        ////
    }
    
    func setDesign(){
        if  fromAddress == "0"{
            self.radiusView.isHidden = true
            self.lblHeader.text = "Address"
            self.lblSecondHeading.text = "Set Business Address"
            objAppShareData.objModelBusinessSetup.radius = "0"
        }else if fromAddress == "1"{
            self.radiusView.isHidden = true
            self.lblHeader.text = "Address"
            self.lblSecondHeading.text = "Set Business Address"
        }else if fromAddress == "2"{
            self.radiusView.isHidden = false
            self.lblHeader.text = "Area Coverage"
            self.lblSecondHeading.text = "Set Your Outcall Coverage"
        }else{
            
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
}

//MARK: - Local Methods
fileprivate extension OutcallOptionVC{
    func configureView(){
        if objAppShareData.objModelBusinessSetup.radius == ""{
            objAppShareData.objModelBusinessSetup.radius = "0"
        }
        
        self.setDeafultsData()
        self.lblRadius1.text = "\(String(format: "%.0f", self.radiusSlider.value * 21))"
        objAppShareData.objModelBusinessSetup.radius = self.lblRadius1.text ?? "0"
        self.btnContinue.layer.cornerRadius = self.btnContinue.frame.size.height/2
        
        let radius = self.radiusSlider.value*21*1609.34
        self.showRadiusOnMAP(WithRadius:Double(radius))

    }
    
    func setDeafultsData(){
        self.radiusSlider.value = (Float(objAppShareData.objModelBusinessSetup.radius))!/21

        if let dic = UserDefaults.standard.value(forKey: UserDefaults.keys.outCallData) as? [String:Any]{
 
            
            let serviceType = dic["serviceType"] as! Int
            self.callOption = serviceType
 
            
            switch serviceType {
                case callOptions.inCall:
                    
                    self.radiusView.isHidden = true
                case callOptions.outCall:
 
                    self.radiusView.isHidden = false
                case callOptions.both:
 
                    self.radiusView.isHidden = false
                default:
                    self.callOption = 1
 
                    self.radiusView.isHidden = true
            }
         }
     }
    
    func addAccessoryView() -> Void {
        let keyboardDoneButtonView = UIToolbar()
        keyboardDoneButtonView.sizeToFit()
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonTapped))
        doneButton.tintColor = appColor
        keyboardDoneButtonView.items = [flexBarButton, doneButton]
       // txtRadius.inputAccessoryView = keyboardDoneButtonView
    }
    //
    @objc func doneButtonTapped() {
 
    }
    //Add text to UIImage
    func textToImage(drawText text: String, inImage image: UIImage, atPoint point: CGPoint) -> UIImage {
        let textColor = UIColor.white
        let textFont = UIFont(name:"Roboto-Regular", size: 11)!
        var t = text
        if t == "21"{
            t = "20"
        }
        let scale = UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(image.size, false, scale)
        
        let textFontAttributes = [
            NSAttributedString.Key.font: textFont,
            NSAttributedString.Key.foregroundColor: textColor,
            ] as [NSAttributedString.Key : Any]
        image.draw(in: CGRect(origin: CGPoint.zero, size: image.size))
        
        var rect = CGRect(origin: point, size: image.size)
        if text.count == 1{
            rect = CGRect(origin: CGPoint(x:19,y:8), size: image.size)
        }else if text.count == 2 {
            rect = CGRect(origin: CGPoint(x:16,y:8), size: image.size)
        }else{
            rect = CGRect(origin: point, size: image.size)
        }
        t.draw(in: rect, withAttributes: textFontAttributes)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
 
func gotoServicesVC(animated:Bool){
    let sb = UIStoryboard(name:"SignUpProcess",bundle:Bundle.main)
    let objChooseType = sb.instantiateViewController(withIdentifier:"AddBusinessTypeVC") as! AddBusinessTypeVC
 //   objChooseType.callOption = self.callOption
    self.navigationController?.pushViewController(objChooseType, animated: true)
}
    
    func setAddress(){
        if objAppShareData.fromAddressVC == false{
           if let userInfo = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]{
            self.txtPlaceName.text = userInfo[UserDefaults.keys.address] as? String ?? ""
            if addressData == true{
                addressData = false
                self.txtPlaceName.text = userInfo["address2"] as? String ?? ""//objAppShareData.objModelBusinessSetup.address
                self.lblPlaceName.text = userInfo["address2"] as? String ?? ""//objAppShareData.objModelBusinessSetup.address
                }
            }
        }else{
          let param = objAppShareData.paramAddress
            if param["address2"] != ""{
            self.txtPlaceName.text = param["address2"]
            self.lblPlaceName.text = param["address2"]

            if addressData == true{
                addressData = false
                self.txtPlaceName.text = param["address"]
                self.lblPlaceName.text = param["address"]
            }
        }
        }
            if self.txtPlaceName.text == ""{
                self.txtPlaceName.text = self.address
                self.lblPlaceName.text = self.address
        }
        
    }
    
    func addGasturesToViews(){
        let selectPlaceTap = UITapGestureRecognizer(target: self, action: #selector(handleSelectPlaceTap(gestureRecognizer:)))
        txtPlaceName.addGestureRecognizer(selectPlaceTap)
 
    }
    //Radius Mathod
    func showRadiusOnMAP(WithRadius radius:Double){
        
        let rad: Double = Double(radius + radius / 2)
        let scale: Double = rad / 500
        let zoomLevel = 15 - log(scale) / log(2)
        
        self.mapView.clear()
        var latitudes = "22.7051382"
        var longitudes = "75.9090618"
        
        if objAppShareData.fromAddressVC == false{
            let dict = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] ?? ["":""]
            latitudes = dict[UserDefaults.keys.latitude] as? String ?? "22.7051382"
            longitudes = dict[UserDefaults.keys.longitude] as? String ?? "75.9090618"
        }else{
            latitudes = objAppShareData.paramAddress[UserDefaults.keys.latitude] ?? "22.7051382"
            longitudes = objAppShareData.paramAddress[UserDefaults.keys.longitude] ?? "75.9090618"
        }
 
        if latitudes == "" || latitudes == "0" || longitudes == "" || longitudes == "0"{
            latitudes =  "22.7051382"
            longitudes = "75.9090618"
        }
        
        let center = CLLocationCoordinate2D(latitude: Double(latitudes)!, longitude: Double(longitudes)!)
        //Create center position
        let camera = GMSCameraPosition.camera(withLatitude:center.latitude,longitude: center.longitude,zoom:Float(zoomLevel))
        self.mapView.camera = camera
        //Create Cricle
        self.mapView.backgroundColor = #colorLiteral(red: 0.4451736992, green: 0.4451736992, blue: 0.4451736992, alpha: 1)
        let circ = GMSCircle(position: center, radius:radius)
        circ.fillColor = #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 0.2504280822)//UIColor.theameColors.pinkColor.withAlphaComponent(0.3)
        circ.strokeColor = #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1)//UIColor.theameColors.pinkColor
        circ.strokeWidth = 1
 
        circ.map = self.mapView
 
        let markerView = UIView(frame:CGRect(x:0,y:0, width: 40, height: 40))
        let img = UIImageView(frame:CGRect(x:0,y:0, width: markerView.frame.size.width-7, height: markerView.frame.size.height))
        img.image  =  UIImage.init(named: "map_icoCurrent")
        //markerView.layer.cornerRadius = 7
        markerView.addSubview(img)
        markerView.layer.masksToBounds = true
        markerView.backgroundColor = UIColor.theameColors.darkColor
        markerView.backgroundColor = UIColor.clear
        
        let marker = GMSMarker(position:center)
        marker.groundAnchor = CGPoint(x:0.5,y:0.5)
        marker.iconView = markerView
        marker.map = self.mapView
    }
}
//MARK: - UITapGesttureActions
fileprivate extension OutcallOptionVC{
    
    @objc func handleSelectPlaceTap(gestureRecognizer: UIGestureRecognizer) {
        
        let sb = UIStoryboard(name:"Main",bundle:Bundle.main)
        let objVC = sb.instantiateViewController(withIdentifier:"AddAddressVC") as! AddAddressVC
            objVC.isOutcallOption = true
        objVC.fromRadiusVC = true

        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    @objc func handleTimeLabelTap(gestureRecognizer: UIGestureRecognizer) {
 
        self.tappedTimeTag = (gestureRecognizer.view?.tag)!
        self.view.endEditing(true)
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
}
//MARK: - IBActions
fileprivate extension OutcallOptionVC{
    
    @IBAction func sliderTappedAction(sender: UITapGestureRecognizer)
    {
             if radiusSlider.isHighlighted { return }
         let point = sender.location(in: radiusSlider)
            let percentage = Float(point.x / 2)//CGRect.width(radiusSlider.bounds))
            let delta = percentage * (radiusSlider.maximumValue - radiusSlider.minimumValue)
            let value = radiusSlider.minimumValue + delta
            radiusSlider.setValue(value, animated: true)
        self.mapView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
       
    }
    
    @IBAction func sliderValueChanged(_ sender: UISlider) {
        let aSlider = sender
        let strForThumbImage = String(format: "%.0f", aSlider.value * 21)
        //let thumbImage = textToImage(drawText: strForThumbImage, inImage: UIImage.init(named: "gubbara_ico")!, atPoint: CGPoint.init(x:12,y:8))
        let thumbImage = textToImage(drawText: strForThumbImage, inImage: UIImage.init(named: "gubbara_ico_PINNew")!, atPoint: CGPoint.init(x:12,y:0))
        aSlider.setThumbImage(thumbImage, for:.highlighted)
        
        if aSlider.value*21 > 20{
            self.lblRadius1.text = "20"//20+ miles"
            objAppShareData.objModelBusinessSetup.radius = self.lblRadius1.text ?? "0"

         }else{
            self.lblRadius1.text = "\(strForThumbImage)"
            objAppShareData.objModelBusinessSetup.radius = self.lblRadius1.text ?? "0"

          //  self.lblRadius2.text = "\(strForThumbImage) miles"
        }
        //self.txtRadius.text = strForThumbImage
        let radius = aSlider.value*21*1609.34
        self.mapView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.showRadiusOnMAP(WithRadius:Double(radius))
    }

    
    @IBAction func btnBack(_ sender:Any){
        if radiusLocal == ""{
            objAppShareData.objModelBusinessSetup.radius = "0"
        }else{
            objAppShareData.objModelBusinessSetup.radius = radiusLocal
        }
        objAppShareData.paramAddress = paramAddDict
        objAppShareData.fromAddressVC  = false
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnSelectAddress(_ sender:UIButton){
        let sb = UIStoryboard(name:"Main",bundle:Bundle.main)
        let objVC = sb.instantiateViewController(withIdentifier:"AddAddressVC") as! AddAddressVC
        objVC.isOutcallOption = true
        objVC.fromRadiusVC = true
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    @IBAction func btnSelectCallOption(_ sender:UIButton){
//        self.btnBoth.isSelected = false
//        self.btnIncall.isSelected = false
//        self.btnOutCall.isSelected = false
        self.callOption = sender.tag+1
        switch sender.tag {
        case 0:
            self.radiusView.isHidden = true
        case 1:
            self.radiusView.isHidden = false
        case 2:
            self.radiusView.isHidden = false
        default:
            print("NO Option")
        }
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    
    
    @IBAction func btnContinue(_ sender:Any){
        print(objAppShareData.objModelBusinessSetup.radius)
        objAppShareData.isComingFrom = "Radius"
        self.txtPlaceName.text = self.txtPlaceName.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        if self.txtPlaceName.text  ==  ""{
            objAppShareData.showAlert(withMessage: "Please enter business address", type: alertType.banner, on: self)
         }else{
            callWebserviceForUpdateRecord()
        }
    }
  
}
//UITextfield Delegate
extension OutcallOptionVC:UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let maxLength = 3
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        if newString.integerValue <= 100 && newString.integerValue > 2 {
            self.lblRadius1.text = "\(newString)"
            objAppShareData.objModelBusinessSetup.radius = self.lblRadius1.text ?? "0"

           // self.lblRadius2.text = "\(newString) miles"
            self.radiusSlider.value = newString.floatValue/100
        }
        return newString.length <= maxLength
        
    }
}
//Webservices
fileprivate extension OutcallOptionVC{
    
    func callWebserviceForUpdateRange() {
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        objActivity.startActivityIndicator()
        var iTime = ""
        var oTime = ""
 
        switch self.callOption {
            case 1:
                oTime = ""
            case 2:
                iTime = ""
            case 3:
                print("noth")
            default:
                print("NO Option")
        }
        
        let dicParam = ["radius":String(round(Double(self.radiusSlider.value*51))),
                        "serviceType":self.callOption,
                        "inCallpreprationTime":iTime,
                        "outCallpreprationTime":oTime] as [String : Any]
        UserDefaults.standard.setValue(String(self.callOption),forKey: UserDefaults.keys.mainServiceType)

        
        objServiceManager.requestPost(strURL: WebURL.updateRange, params: dicParam, success: { response in
            if response["status"] as! String == "success"{
                UserDefaults.standard.set(dicParam, forKey: UserDefaults.keys.outCallData)
                UserDefaults.standard.synchronize()
                self.gotoServicesVC(animated:true)
            }else{
                let msg = response["message"] as! String
                if msg == message.invalidToken{
                    objAppShareData.showAlert(withMessage: "", type: alertType.sessionExpire,on: self)
                }else{
                    objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                }
            }
        }) { error in
            //objAppShareData.showDeafultAlert(withTitle: message.error.title, message: message.error.wrong, on: self)
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    func callWebserviceForUpdateRecord(){
        
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }

        
        if objAppShareData.fromAddressVC == true{
            var arrLocation = [String]()
            arrLocation.append(objAppShareData.paramAddress[UserDefaults.keys.latitude] ?? "22.7051382")
            arrLocation.append(objAppShareData.paramAddress[UserDefaults.keys.longitude] ?? "75.909061")
            var jsonStringPeopleTagIds : String = ""
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: arrLocation, options: JSONSerialization.WritingOptions.prettyPrinted)
            if let JSONString = String(data: jsonData, encoding: String.Encoding.utf8) {
                jsonStringPeopleTagIds = JSONString
            }
        } catch {
        }

        
        objAppShareData.objModelBusinessSetup.address = objAppShareData.paramAddress[UserDefaults.keys.address] ?? ""
        objAppShareData.objModelBusinessSetup.address2 = objAppShareData.paramAddress[UserDefaults.keys.address2] ?? ""
        objAppShareData.objModelBusinessSetup.city = objAppShareData.paramAddress[UserDefaults.keys.city] ?? ""
        objAppShareData.objModelBusinessSetup.state = objAppShareData.paramAddress[UserDefaults.keys.state] ?? ""
        objAppShareData.objModelBusinessSetup.country = objAppShareData.paramAddress[UserDefaults.keys.country] ?? ""
        objAppShareData.objModelBusinessSetup.location = jsonStringPeopleTagIds
        objAppShareData.objModelBusinessSetup.latitude = objAppShareData.paramAddress[UserDefaults.keys.latitude] ?? ""
        objAppShareData.objModelBusinessSetup.longitude = objAppShareData.paramAddress[UserDefaults.keys.longitude] ?? ""
        objAppShareData.objModelBusinessSetup.buildinngNo = objAppShareData.paramAddress[UserDefaults.keys.buildingNumber] ?? ""
        objAppShareData.objModelBusinessSetup.postalCode = objAppShareData.paramAddress["postalCode"] ?? ""

        self.navigationController?.popViewController(animated: true)
        }else{
//            if let userInfo = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]{
            //Rohit
            if let userInfo = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfoDic) as? [String:Any]{

            var arrLocation = [String]()
            arrLocation.append(userInfo[UserDefaults.keys.latitude]  as? String ??  "22.7051382")
            arrLocation.append(userInfo[UserDefaults.keys.longitude]  as? String ??  "75.909061")
            var jsonStringPeopleTagIds : String = ""
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: arrLocation, options: JSONSerialization.WritingOptions.prettyPrinted)
                if let JSONString = String(data: jsonData, encoding: String.Encoding.utf8) {
                    jsonStringPeopleTagIds = JSONString
                }
            } catch {
            }
            
            
            objAppShareData.objModelBusinessSetup.address = userInfo[UserDefaults.keys.address] as? String ?? ""
            objAppShareData.objModelBusinessSetup.address2 = userInfo[UserDefaults.keys.address2] as? String ?? ""
            objAppShareData.objModelBusinessSetup.city = userInfo[UserDefaults.keys.city] as? String ?? ""
            objAppShareData.objModelBusinessSetup.state = userInfo[UserDefaults.keys.state] as? String ?? ""
            objAppShareData.objModelBusinessSetup.country = userInfo[UserDefaults.keys.country] as? String ?? ""
            objAppShareData.objModelBusinessSetup.location = jsonStringPeopleTagIds
            objAppShareData.objModelBusinessSetup.latitude = userInfo[UserDefaults.keys.latitude] as? String ?? ""
            objAppShareData.objModelBusinessSetup.longitude = userInfo[UserDefaults.keys.longitude] as? String ?? ""
            objAppShareData.objModelBusinessSetup.buildinngNo = userInfo[UserDefaults.keys.buildingNumber] as? String ?? ""
                
//            objAppShareData.objModelBusinessSetup.postalCode = userInfo["postalCode"] as? String ?? ""
            
            self.navigationController?.popViewController(animated: true)
            }
        }
      }
}

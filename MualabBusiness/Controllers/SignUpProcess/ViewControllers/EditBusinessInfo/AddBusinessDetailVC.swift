//
//  AddBusinessDetailVC.swift
//  MualabBusiness
//
//  Created by Mindiii on 1/12/19.
//  Copyright © 2019 Mindiii. All rights reserved.
// Step - 2 SIGNUP Process

import UIKit

class AddBusinessDetailVC: UIViewController,UITextFieldDelegate{
        
        @IBOutlet weak var lblBookinkType:UILabel!
        @IBOutlet weak var lblBookingSetting:UILabel!
        @IBOutlet weak var lblRadiusOfArea:UILabel!
        @IBOutlet weak var lblBusinessAddress:UILabel!
        @IBOutlet weak var lblLocationAddress:UILabel!
        @IBOutlet weak var txtBusinessEmail:UITextField!
        @IBOutlet weak var lblCompletionTime:UILabel!
        @IBOutlet weak var lblCompletionTimeOutCall:UILabel!
        @IBOutlet weak var lblPaymentType:UILabel!

        @IBOutlet weak var lblDynamicAddressTytle:UILabel!
        @IBOutlet weak var txtBusinessName:UITextField!
        @IBOutlet weak var txtPhoneNumber : UITextField!
        
        @IBOutlet weak var viewRadiurOfArea : UIView!
        @IBOutlet weak var viewReturnLocationAdd : UIView!
        @IBOutlet weak var viewBusinessAddress : UIView!
    
        @IBOutlet weak var btnBusinessType:UIButton!
        @IBOutlet weak var btnBookingSetting:UIButton!
        @IBOutlet weak var btnBusinessAddress:UIButton!
        @IBOutlet weak var btnLocation:UIButton!
        @IBOutlet weak var btnRadius:UIButton!
        @IBOutlet weak var btnTime:UIButton!
    
    
        var strUserId: String = ""
        
        fileprivate var strLocalCCode: String = ""
        
        override func viewDidLoad() {
            super.viewDidLoad()
            objAppShareData.fromAddressVC = false
            self.txtPhoneNumber.delegate = self
            self.txtBusinessEmail.delegate = self
            self.txtBusinessName.delegate = self
            //Rohit Working on 11/Feb/2020
          //  let dict = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfoDic) as? [String:Any] ?? ["":""]
           // print(dict)
//objAppShareData.paramAddress = dict as? [String:String] ?? ["":""]
          //  print(dict)
             //   self.parseBusinessSignUpDetail(dictOfInfo: dict)
                var businessType = UserDefaults.standard.string(forKey: UserDefaults.keys.mainServiceType) ?? ""
            if businessType == "0"{
                UserDefaults.standard.setValue("3", forKey: UserDefaults.keys.mainServiceType)
                businessType = "3"
            }
                objAppShareData.objModelBusinessSetup.serviceType = businessType
       
            //Rohit
           // objAppShareData.objAppdelegate.parseBusinessSignUpDetail(dictOfInfo: dict)
            self.addAccesorryToKeyBoard()
            self.callWSGetBusinessInfo()
//            self.txtBusinessEmail.text = dict[UserDefaults.keys.email] as? String ?? ""
//            self.txtPhoneNumber.text = dict[UserDefaults.keys.contactNo] as? String ?? ""
//            self.txtBusinessName.text = dict[UserDefaults.keys.businessName] as? String ?? ""
//            self.lblRadiusOfArea.text = "0 Miles"
            
            // Do any additional setup after loading the view.
            // self.setBusinessInfo()
        }
        
        
//        func parseDataFromLocal(){
//            self.txtBusinessEmail.text = objAppShareData.objModelBusinessSetup.businessEmail
//            self.txtBusinessName.text = objAppShareData.objModelBusinessSetup.businessName
//            self.txtPhoneNumber.text = objAppShareData.objModelBusinessSetup.businessContactNo
//
//            self.lblBusinessAddress.text = objAppShareData.objModelBusinessSetup.address
//            self.lblLocationAddress.text = objAppShareData.objModelBusinessSetup.address2
//            self.lblRadiusOfArea.text = objAppShareData.objModelBusinessSetup.radius+" Miles"
//            if  objAppShareData.objModelBusinessSetup.radius == ""{
//                self.lblRadiusOfArea.text = "0 Miles"
//            }
//        }
        
        override func viewWillAppear(_ animated: Bool) {
            
            UserDefaults.standard.setValue("2", forKey: UserDefaults.keys.navigationVC)

          //  let dict = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfoDic) as? [String:Any] ?? ["":""]
           // let id = dict[UserDefaults.keys.userId] as? String ?? ""

            if objAppShareData.objModelBusinessSetup.serviceType == "0" || objAppShareData.objModelBusinessSetup.serviceType == ""{
                objAppShareData.objModelBusinessSetup.serviceType  = "3"
            }

            /*
             self.lblBusinessAddress.text = objAppShareData.paramAddress["address2"] ?? ""
             self.lblLocationAddress.text = objAppShareData.paramAddress["address2"] ?? ""
             */
              //  self.lblBusinessAddress.text = objAppShareData.paramAddress["address"] ?? ""
              //  self.lblLocationAddress.text = objAppShareData.paramAddress["address"] ?? ""
//
//                self.lblRadiusOfArea.text = objAppShareData.objModelBusinessSetup.radius+" Miles"
//                self.configureView(businessType: objAppShareData.objModelBusinessSetup.serviceType)
//            if  objAppShareData.objModelBusinessSetup.radius == ""{
//                self.lblRadiusOfArea.text = "0 Miles"
//            }
////
////        Set Payment Type
//            if objAppShareData.objModelBusinessSetup.paymentType == "1"{
//                self.lblPaymentType.text = "Card"
//                objAppShareData.objModelBusinessSetup.paymentType = "1"
//            }else if objAppShareData.objModelBusinessSetup.paymentType == "2"{
//                self.lblPaymentType.text = "Cash"
//                objAppShareData.objModelBusinessSetup.paymentType = "2"
//            }else if objAppShareData.objModelBusinessSetup.paymentType == "3"{
//                self.lblPaymentType.text = "Both"
//                objAppShareData.objModelBusinessSetup.paymentType = "3"
//            }else{
////                objAppShareData.objModelBusinessSetup.paymentType = "1"
////                self.lblPaymentType.text = "Card"
//
//                self.lblPaymentType.text = "Cash"
//                objAppShareData.objModelBusinessSetup.paymentType = "2"
//            }
            
            
            self.serviceBreakTime()
            setDataOnController(strComingFrom: objAppShareData.isComingFrom)
            
            //self.buttonHiddenShowManage()
           
        }
    
    
    func setDataOnController(strComingFrom: String){
        
        if strComingFrom == "Address"{
            self.lblBusinessAddress.text = objAppShareData.paramAddress["address"] ?? ""
            self.lblLocationAddress.text = objAppShareData.paramAddress["address"] ?? ""
        }
        
        if strComingFrom == "Payment"{
            let paymentType = objAppShareData.objModelBusinessSetup.paymentType
            if paymentType == "1"{
                self.lblPaymentType.text = "Card"
                objAppShareData.objModelBusinessSetup.paymentType = "1"
            }else if paymentType == "2"{
                self.lblPaymentType.text = "Cash"
                objAppShareData.objModelBusinessSetup.paymentType = "2"
            }else if paymentType == "3"{
                self.lblPaymentType.text = "Both"
                objAppShareData.objModelBusinessSetup.paymentType = "3"
            }else{
                self.lblPaymentType.text = "Cash"
                objAppShareData.objModelBusinessSetup.paymentType = "2"
            }
        }
        
        if strComingFrom == "Radius"{
            self.lblRadiusOfArea.text = objAppShareData.objModelBusinessSetup.radius+" Miles"
            self.configureView(businessType: objAppShareData.objModelBusinessSetup.serviceType)
            if  objAppShareData.objModelBusinessSetup.radius == ""{
                self.lblRadiusOfArea.text = "0 Miles"
            }
        }
        
        if strComingFrom == "Booking"{
            let serviceType = objAppShareData.objModelBusinessSetup.serviceType
            if serviceType == "1"{
                self.lblBookinkType.text = "In Call"
                objAppShareData.objModelBusinessSetup.serviceType = "1"
            }else if serviceType == "2"{
                self.lblBookinkType.text = "Out Call"
                objAppShareData.objModelBusinessSetup.serviceType = "2"
            }else if serviceType == "3"{
                self.lblBookinkType.text = "In Call/Out Call"
                objAppShareData.objModelBusinessSetup.serviceType = "3"
            }else{
                self.lblBookinkType.text = "In Call/Out Call"
                objAppShareData.objModelBusinessSetup.serviceType = "3"
            }
            
            self.configureView(businessType: "2")
        }
        
        if strComingFrom == "BookingSetting"{
            let bookingOption = objAppShareData.objModelBusinessSetup.bookingSetting
            if bookingOption == "1"{
                self.lblBookingSetting.text = "Automatic"
                objAppShareData.objModelBusinessSetup.bookingSetting = "1"
            }else if bookingOption == "2"{
                self.lblBookingSetting.text = "Manual"
                objAppShareData.objModelBusinessSetup.bookingSetting = "2"
            }else{
                self.lblBookingSetting.text = "Automatic"
                objAppShareData.objModelBusinessSetup.bookingSetting = "1"
            }
        }
        
         self.buttonHiddenShowManage()
    }
    
    
    
    func setBusinessInfo(){
        
        let userDict = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfoDic) as? [String:Any] ?? ["":""]
        print(userDict)
        
        self.txtBusinessName.text = userDict[UserDefaults.keys.businessName] as? String ?? ""
        self.txtBusinessEmail.text = userDict[UserDefaults.keys.email] as? String ?? ""
        self.txtPhoneNumber.text = userDict[UserDefaults.keys.contactNo] as? String
        
        
         let serviceType = userDict[UserDefaults.keys.serviceType] as? String ?? ""
         if serviceType == "1"{
            self.lblBookinkType.text = "In Call"
            objAppShareData.objModelBusinessSetup.serviceType = "1"
        }else if serviceType == "2"{
            self.lblBookinkType.text = "Out Call"
            objAppShareData.objModelBusinessSetup.serviceType = "2"
        }else if serviceType == "3"{
            self.lblBookinkType.text = "In Call/Out Call"
            objAppShareData.objModelBusinessSetup.serviceType = "3"
        }else{
            self.lblBookinkType.text = "In Call/Out Call"
            objAppShareData.objModelBusinessSetup.serviceType = "3"
        }
        
        self.configureView(businessType: "2")

         let bookingOption = userDict["\(UserDefaults.keys.bookingSetting)"] as? String ?? ""
        if bookingOption == "1"{
            self.lblBookingSetting.text = "Automatic"
            objAppShareData.objModelBusinessSetup.bookingSetting = "1"
        }else if bookingOption == "2"{
            self.lblBookingSetting.text = "Manual"
            objAppShareData.objModelBusinessSetup.bookingSetting = "2"
        }else{
            self.lblBookingSetting.text = "Automatic"
            objAppShareData.objModelBusinessSetup.bookingSetting = "1"
        }
        
//        let paymentOption = userDict["\(UserDefaults.keys.payOption)"] as? String ?? ""
//        if paymentOption == "1"{
//            self.lblPaymentType.text = "Card"
//            objAppShareData.objModelBusinessSetup.paymentType = "1"
//        }else if paymentOption == "2"{
//            self.lblPaymentType.text = "Cash"
//            objAppShareData.objModelBusinessSetup.paymentType = "2"
//        }else if paymentOption == "3"{
//            self.lblPaymentType.text = "Both"
//            objAppShareData.objModelBusinessSetup.paymentType = "3"
//        }else{
//            self.lblPaymentType.text = "Cash"
//            objAppShareData.objModelBusinessSetup.paymentType = "2"
//        }
        
        
        //set Address
        self.lblBusinessAddress.text = userDict[UserDefaults.keys.address] as? String ?? ""
        self.lblLocationAddress.text = userDict[UserDefaults.keys.address] as? String ?? ""
        objAppShareData.objModelBusinessSetup.address = self.lblLocationAddress.text ?? ""
        objAppShareData.objModelBusinessSetup.city = userDict[UserDefaults.keys.city] as? String ?? ""
        objAppShareData.objModelBusinessSetup.state = userDict[UserDefaults.keys.state] as? String ?? ""
        objAppShareData.objModelBusinessSetup.country = userDict[UserDefaults.keys.country] as? String ?? ""
        objAppShareData.objModelBusinessSetup.latitude = userDict[UserDefaults.keys.latitude] as? String ?? ""
        objAppShareData.objModelBusinessSetup.longitude = userDict[UserDefaults.keys.longitude] as? String ?? ""
        self.lblRadiusOfArea.text = userDict[UserDefaults.keys.radius] as? String ?? "0 Miles"
        objAppShareData.objModelBusinessSetup.address = self.lblRadiusOfArea.text ?? "0 Miles"
        
        if self.self.lblBusinessAddress.text == ""{
            self.lblBusinessAddress.text = objAppShareData.paramAddress["address"] ?? ""
            self.lblLocationAddress.text = objAppShareData.paramAddress["address"] ?? ""
        }
        
        self.lblRadiusOfArea.text = objAppShareData.objModelBusinessSetup.radius+" Miles"
        self.configureView(businessType: objAppShareData.objModelBusinessSetup.serviceType)
        if  objAppShareData.objModelBusinessSetup.radius == ""{
            self.lblRadiusOfArea.text = "0 Miles"
        }
        //
        // Set Payment Type
        if objAppShareData.objModelBusinessSetup.paymentType == "1"{
            self.lblPaymentType.text = "Card"
            objAppShareData.objModelBusinessSetup.paymentType = "1"
        }else if objAppShareData.objModelBusinessSetup.paymentType == "2"{
            self.lblPaymentType.text = "Cash"
            objAppShareData.objModelBusinessSetup.paymentType = "2"
        }else if objAppShareData.objModelBusinessSetup.paymentType == "3"{
            self.lblPaymentType.text = "Both"
            objAppShareData.objModelBusinessSetup.paymentType = "3"
        }else{
            //                objAppShareData.objModelBusinessSetup.paymentType = "1"
            //                self.lblPaymentType.text = "Card"
            
            self.lblPaymentType.text = "Cash"
            objAppShareData.objModelBusinessSetup.paymentType = "2"
        }
        
        self.buttonHiddenShowManage()
        
    }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
        }
    }

extension AddBusinessDetailVC {
    func buttonHiddenShowManage(){
        let obj = objAppShareData.objModelBusinessSetup
        self.btnBusinessType = self.buttonWhiteColor(btn: btnBusinessType)
        self.btnBookingSetting = self.buttonClearColor(btn: btnBookingSetting)
        self.btnBusinessAddress = self.buttonWhiteColor(btn: btnBusinessAddress)
        self.btnLocation = self.buttonWhiteColor(btn: btnLocation)
        self.btnRadius = self.buttonWhiteColor(btn: btnRadius)
        self.btnTime = self.buttonWhiteColor(btn: btnTime)
        
        if objAppShareData.objModelBusinessSetup.bookingSetting == "0"{
            self.lblBookingSetting.text = "Automatic"
        }else if objAppShareData.objModelBusinessSetup.bookingSetting == "1"{
            self.lblBookingSetting.text = "Manual"
        }else{
            self.lblBookingSetting.text = ""
        }
        
        if self.lblBookinkType.text != "" && self.lblBookinkType.text  != nil {
            self.btnBusinessType = self.buttonClearColor(btn: btnBusinessType)
        }
        
        if self.lblBusinessAddress.text != "" {
            self.btnBusinessAddress = self.buttonClearColor(btn: btnBusinessAddress)
        }
        
        if self.lblLocationAddress.text != "" {
            self.btnLocation = self.buttonClearColor(btn: btnLocation)
        }
        if obj.radius != "" &&  obj.radius  != "0"  {
            self.btnRadius = self.buttonClearColor(btn: btnRadius)
        }
        
        self.lblRadiusOfArea.text = objAppShareData.objModelBusinessSetup.radius
        
        if obj.inCallpreprationTime != "00:00" || obj.outCallpreprationTime  != "00:00" {
            self.btnTime = self.buttonClearColor(btn: btnTime)
        }
    }
    
    func buttonClearColor(btn:UIButton) -> UIButton{
        btn.backgroundColor = UIColor.clear
        btn.setTitleColor(UIColor.clear, for: .normal)
        return btn
    }
    func buttonWhiteColor(btn:UIButton)  -> UIButton{
        btn.backgroundColor = UIColor.white
        btn.setTitleColor(UIColor.black, for: .normal)
        return btn
    }
    
}
    //MARK: - Custome method extension
    extension AddBusinessDetailVC{
        func configureView(businessType:String){
            if businessType == "1"{
                self.viewBusinessAddress.isHidden = false
                self.viewRadiurOfArea.isHidden = true
                self.viewReturnLocationAdd.isHidden = true
                self.lblBookinkType.text = "In Call"
                self.lblDynamicAddressTytle.text = "Enter Business Address"
                self.btnBusinessAddress.setTitle("Enter Business Address", for: .normal)

            }else if businessType == "2"{
                self.lblDynamicAddressTytle.text = "Business Address / Returning Location"//"Starting Location Address"
                self.btnLocation.setTitle("Business Address / Returning Location", for: .normal)
                self.viewBusinessAddress.isHidden = true
                self.viewRadiurOfArea.isHidden = false
                self.viewReturnLocationAdd.isHidden = false
                self.lblBookinkType.text = "Out Call"
            }else if businessType == "3"{
                self.lblDynamicAddressTytle.text = "Business Address / Returning Location"
                self.btnLocation.setTitle("Business Address / Returning Location", for: .normal)

                self.viewBusinessAddress.isHidden = true
                self.viewRadiurOfArea.isHidden = false
                self.viewReturnLocationAdd.isHidden = false
                self.lblBookinkType.text = "In Call / Out Call"
            }else{
                self.viewBusinessAddress.isHidden = true
                self.viewRadiurOfArea.isHidden = true
                self.viewReturnLocationAdd.isHidden = true
            }
        }
        func serviceBreakTime() {
            
            self.lblCompletionTimeOutCall.isHidden = true
            self.lblCompletionTime.isHidden = true
            if objAppShareData.objModelBusinessSetup.serviceType == "1"{
                self.lblCompletionTime.isHidden = false
                
                let arr1 = objAppShareData.objModelBusinessSetup.inCallpreprationTime.components(separatedBy: ":")
                if arr1.count > 1{
                    if arr1[0] == "00"{
                        self.lblCompletionTime.text = "Incall : "+arr1[1]+" Minutes"
                    }else{
                        if arr1[1] == "00"{
                            self.lblCompletionTime.text = "Incall : "+arr1[0]+" Hours "
                        }else{
                            self.lblCompletionTime.text = "Incall : "+arr1[0]+" Hours "+arr1[1]+" Minutes"
                        }
                    }
                }
                //                self.lblCompletionTime.text = "Incall  : "+objAppShareData.objModelBusinessSetup.inCallpreprationTime
            }else if objAppShareData.objModelBusinessSetup.serviceType == "2"{
                self.lblCompletionTimeOutCall.isHidden = false
                
                self.lblCompletionTimeOutCall.text = "Outcall : "+objAppShareData.objModelBusinessSetup.outCallpreprationTime
                let arr1 = objAppShareData.objModelBusinessSetup.outCallpreprationTime.components(separatedBy: ":")
                if arr1.count > 1{
                    if arr1[0] == "00"{
                        self.lblCompletionTimeOutCall.text = "Outcall : "+arr1[1]+" Minutes"
                    }else{
                        if arr1[1] == "00"{
                            self.lblCompletionTimeOutCall.text = "Outcall : "+arr1[0]+" Hours "
                        }else{
                            self.lblCompletionTimeOutCall.text = "Outcall : "+arr1[0]+" Hours "+arr1[1]+" Minutes"
                        }
                    }
                }
            }else{
                self.lblCompletionTimeOutCall.isHidden = false
                self.lblCompletionTime.isHidden = false
                var a =  "Incall : "+objAppShareData.objModelBusinessSetup.inCallpreprationTime
                var b = "Outcall : "+objAppShareData.objModelBusinessSetup.outCallpreprationTime
                
                let arr1 = objAppShareData.objModelBusinessSetup.inCallpreprationTime.components(separatedBy: ":")
                if arr1.count > 1{
                    if arr1[0] == "00"{
                        a = "Incall : "+arr1[1]+" Minutes"
                    }else{
                        if arr1[1] == "00"{
                            a = "Incall : "+arr1[0]+" Hours "
                        }else{
                            a = "Incall : "+arr1[0]+" Hours "+arr1[1]+" Minutes"
                        }
                    }
                }
                let arr2 = objAppShareData.objModelBusinessSetup.outCallpreprationTime.components(separatedBy: ":")
                if arr2.count > 1{
                    if arr2[0] == "00"{
                        b = "Outcall : "+arr2[1]+" Minutes"
                    }else{
                        if arr2[1] == "00"{
                            b = "Outcall : "+arr2[0]+" Hours "
                        }else{
                            b = "Outcall : "+arr2[0]+" Hours "+arr2[1]+" Minutes"
                        }
                    }
                    self.lblCompletionTime.text = a
                    self.lblCompletionTimeOutCall.text = b
                }
            }
        }
        
        
        
        func addAccesorryToKeyBoard(){
            let keyboardDoneButtonView = UIToolbar()
            keyboardDoneButtonView.sizeToFit()
            let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
            let doneButton = UIBarButtonItem(title: "Done", style:.plain, target: self, action: #selector(self.resignKeyBoard))
            doneButton.tintColor = appColor
            
            keyboardDoneButtonView.items = [flexBarButton, doneButton]
            
            txtPhoneNumber.inputAccessoryView = keyboardDoneButtonView
        }
        @objc func resignKeyBoard(){
            txtPhoneNumber.endEditing(true)
            self.view.endEditing(true)
        }
        
        func addressVC(strFromAddress:String){
            let sb = UIStoryboard(name:"SignUpProcess",bundle:Bundle.main)
            let objChooseType = sb.instantiateViewController(withIdentifier:"OutcallOptionVC") as! OutcallOptionVC
            objChooseType.fromAddress = strFromAddress
            objChooseType.address = self.lblBusinessAddress.text ?? ""
            self.navigationController?.pushViewController(objChooseType, animated:true)
        }
    }
    
    //MARK: - textfield method extension
    extension AddBusinessDetailVC{
        func textFieldShouldReturn(_ textField: UITextField) -> Bool{
            if textField == self.txtBusinessName{
                txtBusinessEmail.becomeFirstResponder()
            }else if textField == self.txtBusinessEmail{
                txtPhoneNumber.becomeFirstResponder()
            }else if textField == self.txtPhoneNumber{
                txtPhoneNumber.resignFirstResponder()
            }
            return true
        }
    }
    
    
    //MARK: - button extension
    extension AddBusinessDetailVC{
        @IBAction func btnBack(_ sender:Any){
            self.view.endEditing(true)
            
            var navigationCheck = false
            UserDefaults.standard.setValue("1", forKey: UserDefaults.keys.navigationVC)
            
            let controllers = self.navigationController?.viewControllers
            for vc in controllers! {
                if vc is  EditMyInfo{
                    navigationCheck = true
                    _ = self.navigationController?.popToViewController(vc as! EditMyInfo, animated: true)
                }
            }
            
            if navigationCheck == false{
                let sb = UIStoryboard(name:"SignUpProcess",bundle:Bundle.main)
                let objChooseType = sb.instantiateViewController(withIdentifier:"EditMyInfo") as! EditMyInfo
                self.navigationController?.pushViewController(objChooseType, animated: false)
            }
        }
        
        @IBAction func btnBookingType(_ sender:Any){
            self.view.endEditing(true)
            let sb = UIStoryboard(name:"SignUpProcess",bundle:Bundle.main)
            let objChooseType = sb.instantiateViewController(withIdentifier:"BusinessTimeSelectionVC") as! BusinessTimeSelectionVC
            self.navigationController?.pushViewController(objChooseType, animated: true)
        }
        
        @IBAction func btnContinew(_ sender:Any){
            self.view.endEditing(true)
            
            //self.GotoVerifyPhoneVC()
            // return
            var invalid = false
            var strMessage = ""
            txtBusinessEmail.text = txtBusinessEmail.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            txtBusinessName.text = txtBusinessName.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            
            txtPhoneNumber.text = txtPhoneNumber.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            let businessType =  objAppShareData.objModelBusinessSetup.serviceType
            
            if businessType == "" || businessType == "0"{
                invalid = true
                objAppShareData.showAlert(withMessage: "Please select booking type", type: alertType.banner, on: self)
            }else if objAppShareData.objModelBusinessSetup.bookingSetting == "" || objAppShareData.objModelBusinessSetup.bookingSetting == nil{
                invalid = true
                objAppShareData.showAlert(withMessage: "Please select booking setting", type: alertType.banner, on: self)
            }else if self.txtBusinessName.text?.count == 0{
                objAppShareData.shakeTextField(self.txtBusinessName)
                invalid = true
            }else if self.txtPhoneNumber.text?.count == 0{
                objAppShareData.shakeTextField(self.txtPhoneNumber)
                invalid = true
            }else if self.txtBusinessEmail.text?.count == 0{
                objAppShareData.shakeTextField(self.txtBusinessEmail)
                invalid = true
            }else if !(objValidationManager.isValidateEmail(strEmail: self.txtBusinessEmail.text!)){
                strMessage = message.validation.email
                invalid = true
            }else if businessType == "1" && objAppShareData.objModelBusinessSetup.address == ""{
                invalid = true
                objAppShareData.showAlert(withMessage: "Please enter business address", type: alertType.banner, on: self)
            }else if businessType == "2" && objAppShareData.objModelBusinessSetup.address == ""{
                invalid = true
                objAppShareData.showAlert(withMessage: "Please enter return location address", type: alertType.banner, on: self)
            }else if businessType == "2" &&  objAppShareData.objModelBusinessSetup.radius == "0" {
                invalid = true
                objAppShareData.showAlert(withMessage: "Please select radius of area coverage", type: alertType.banner, on: self)
            }else if businessType == "3" && objAppShareData.objModelBusinessSetup.address == ""{
                invalid = true
                objAppShareData.showAlert(withMessage: "Please enter business address", type: alertType.banner, on: self)
                
            }else if businessType == "3" && (objAppShareData.objModelBusinessSetup.radius == "0" || objAppShareData.objModelBusinessSetup.radius == ""  || objAppShareData.objModelBusinessSetup.radius == " "){
                invalid = true
                objAppShareData.showAlert(withMessage: "Please select radius of area coverage", type: alertType.banner, on: self)
            }else if objAppShareData.objModelBusinessSetup.businessHours != "1"{
                    invalid = true
                    objAppShareData.showAlert(withMessage: "Please select business operation hours", type: alertType.banner, on: self)
            }
//            else if businessType == "1" && objAppShareData.objModelBusinessSetup.inCallpreprationTime == "00:00"{
//                invalid = true
//                objAppShareData.showAlert(withMessage: "Please select break between appointments time", type: alertType.banner, on: self)
//            }else if businessType == "2" && objAppShareData.objModelBusinessSetup.outCallpreprationTime == "00:00"{
//                invalid = true
//                objAppShareData.showAlert(withMessage: "Please select break between appointments time", type: alertType.banner, on: self)
//            }else if businessType == "3" && objAppShareData.objModelBusinessSetup.inCallpreprationTime == "00:00"{
//                invalid = true
//                objAppShareData.showAlert(withMessage: "Please select break between appointments time", type: alertType.banner, on: self)
//            }else if businessType == "3" && objAppShareData.objModelBusinessSetup.outCallpreprationTime == "00:00"{
//                invalid = true
//                objAppShareData.showAlert(withMessage: "Please select break between appointments time", type: alertType.banner, on: self)
//            }
            
            
            if invalid && strMessage.count>0 {
                objAppShareData.showAlert(withMessage: strMessage, type: alertType.banner, on: self)
            }else if invalid == false{
                self.view.endEditing(true)
                
                if objAppShareData.fromAddressVC == true || (objAppShareData.paramAddress[UserDefaults.keys.address] ?? "" != "") {
                    objAppShareData.objModelBusinessSetup.location = objAppShareData.paramAddress["name"] ?? ""
                    objAppShareData.objModelBusinessSetup.address2 = objAppShareData.paramAddress[UserDefaults.keys.address]   ?? ""
                    objAppShareData.objModelBusinessSetup.country = objAppShareData.paramAddress[UserDefaults.keys.country]  ?? ""
                    objAppShareData.objModelBusinessSetup.state = objAppShareData.paramAddress[UserDefaults.keys.state]   ?? ""
                    objAppShareData.objModelBusinessSetup.city = objAppShareData.paramAddress[UserDefaults.keys.city]   ?? ""
                    objAppShareData.objModelBusinessSetup.address = objAppShareData.paramAddress[UserDefaults.keys.address]   ?? ""
                    objAppShareData.objModelBusinessSetup.latitude = objAppShareData.paramAddress[UserDefaults.keys.latitude]   ?? ""
                    objAppShareData.objModelBusinessSetup.longitude = objAppShareData.paramAddress[UserDefaults.keys.longitude]   ?? ""
                }
                
//             let businessContactNo = objAppShareData.objModelBusinessSetup.businessContactNo
                let inCallpreprationTime = objAppShareData.objModelBusinessSetup.inCallpreprationTime
                let outCallpreprationTime = objAppShareData.objModelBusinessSetup.outCallpreprationTime
//             let businessEmail = objAppShareData.objModelBusinessSetup.businessEmail
//             let businessName = objAppShareData.objModelBusinessSetup.businessName
//             let location = objAppShareData.objModelBusinessSetup.location
               // let address2 = objAppShareData.objModelBusinessSetup.address2
//                let country = objAppShareData.objModelBusinessSetup.country
//                let state = objAppShareData.objModelBusinessSetup.state
//                let city = objAppShareData.objModelBusinessSetup.city
//                let radius = objAppShareData.objModelBusinessSetup.radius
//                let address = objAppShareData.objModelBusinessSetup.address
//                let latitude = objAppShareData.objModelBusinessSetup.latitude
//                let longitude = objAppShareData.objModelBusinessSetup.longitude
                
                //Rohit work on 11/Feb/2020
                let userDict = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfoDic) as? [String:Any] ?? ["":""]
                                   print(userDict)
                let country:String = userDict["\(UserDefaults.keys.country)"] as! String
                print(country)
                let state:String =  userDict["\(UserDefaults.keys.state)"] as! String
                let city:String = userDict["\(UserDefaults.keys.city)"] as! String
                let radius:String = objAppShareData.objModelBusinessSetup.radius
                let address:String = userDict["\(UserDefaults.keys.address)"] as! String
                var latitude = ""
                var longitude = ""
                
                if let lati = userDict["\(UserDefaults.keys.latitude)"] as? String{
                    latitude = lati
                }else if let lati = userDict["\(UserDefaults.keys.latitude)"] as? Int{
                    latitude = "\(lati)"
                }
           
                if let longi:String = userDict["\(UserDefaults.keys.longitude)"] as? String{
                    longitude = longi
                }else  if let longi = userDict["\(UserDefaults.keys.longitude)"] as? Int{
                    longitude = "\(longi)"
                }
                
                
//             let incallTimeHours = objAppShareData.objModelBusinessSetup.incallTimeHours
//             let incallTimeMinute = objAppShareData.objModelBusinessSetup.incallTimeMinute
//             let outcallTimeHours = objAppShareData.objModelBusinessSetup.outcallTimeHours
//             let outcallTimeMinute = objAppShareData.objModelBusinessSetup.outcallTimeMinute
                let serviceType = objAppShareData.objModelBusinessSetup.serviceType
                let bookingSetting = objAppShareData.objModelBusinessSetup.bookingSetting
                //Rohit
                let dict = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfoDic) as? [String:Any] ?? ["":""]
                var id = ""
                if let id1 = dict[UserDefaults.myDetail.Userid] as? Int{
                    id = String(id1)
                }else if let id1 = dict[UserDefaults.myDetail.Userid] as? String{
                    id = id1
                }
                var staffId = ""
                var businessId = ""
                if let staff_id = dict["staff_id"]as? String{
                    staffId = staff_id
                    
                }
                if let business_id = dict["business_id"]as? String{
                    businessId = business_id
                    
                }


//                let paramSend = ["businessContactNo":self.txtPhoneNumber.text ?? "",
//                                                "inCallpreprationTime":inCallpreprationTime,
//                                                "outCallpreprationTime":outCallpreprationTime,
//                                                "businessEmail":self.txtBusinessEmail.text ?? "",
//                                                "businessName":self.txtBusinessName.text ?? "",
//                                                "serviceType":serviceType,
//                                                "isDocument":"1",
//                                                "location":"aap ke hisab se dekh lo",
//                                                "address2":address2,
//                                                "country":country,
//                                                "state":state,
//                                                "city":city,
//                                                "radius":radius,
//                                                "address":address,
//                                                "latitude":latitude,
//                                                "longitude":longitude,
//                                                "payOption":objAppShareData.objModelBusinessSetup.paymentType,
//                                                "bookingSetting":bookingSetting]
                
                let paramSend = ["business_contact_no":self.txtPhoneNumber.text ?? "",
                                 "in_call_prepration_time":inCallpreprationTime,
                                 "out_call_prepration_time":outCallpreprationTime,
                                 "business_email":self.txtBusinessEmail.text ?? "",
                                 "business_name":self.txtBusinessName.text ?? "",
                                 "service_type":serviceType,
                                 "business_type":"1",
                               //  "is_document":"1",
                               //  "location":"aap ke hisab se dekh lo",
                                 //  "address2":address2,
                                 "country":country,
                                 "state":state,
                                 "city":city,
                                 "radius":radius,
                                 "address":address,
                                 "latitude":latitude,
                                 "longitude":longitude,
                                 "pay_option":objAppShareData.objModelBusinessSetup.paymentType,
                                 "booking_setting":bookingSetting,
                                 "user_id":String(id),
                    "business_id":businessId,
                    "staff_id":staffId]
                
                callWebserviceForRegistration(dict: paramSend)                
            }
        }
        
        func callWebserviceForRegistration(dict:[String:Any]){
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            print(dict)
            objActivity.startActivityIndicator()
            objServiceManager.requestPost(strURL: WebURL.updateBusinessInfo, params: dict, success:{ response in
               
                if response["status"] as? String == "success"{
                    if let dictUserDetail = response["businessInfo"] as? [String:Any]{
                        let message = response["message"] as? String ?? ""
                        //objAppShareData.showAlert(withMessage: message, type: alertType.banner, on: self)
                        //Rohit => work on 7/Feb/2020
                        // objAppShareData.objAppdelegate.parseBusinessSignUpDetail(dictOfInfo: dictUserDetail)
                        objAppShareData.objAppdelegate.SaveBusinessDetails(dictOfBusinessInfo: dictUserDetail)
                         self.gotoNextVC(bizCount: 1)
                        // Rohit => Work On 11/Feb/2020
//                        if let bizCount = dictUserDetail["biz_count"] as? Int{
//                            if bizCount == 0{
//                                self.gotoNextVC(bizCount: 0)
//                            }else{
//                                self.gotoNextVC(bizCount: 1)
//                            }
//                        }else  if let bizCount = dictUserDetail["biz_count"] as? String{
//                            if bizCount == "0"{
//                                self.gotoNextVC(bizCount: 0)
//                            }else{
//                                self.gotoNextVC(bizCount: 1)
//                            }
//                        }
                    }else if let dictUserDetail = response["businessInfo"] as? [String:Any]{
                        let message = response["message"] as? String ?? ""
                        //objAppShareData.showAlert(withMessage: message, type: alertType.banner, on: self)
                        //Rohit => work on 7/Feb/2020
                       // objAppShareData.objAppdelegate.parseBusinessSignUpDetail(dictOfInfo: dictUserDetail)
                        objAppShareData.objAppdelegate.SaveBusinessDetails(dictOfBusinessInfo: dictUserDetail)
                        self.gotoNextVC(bizCount: 1)
                    }
                }else{
                    let msg = response["message"] as! String
                    objAppShareData.showAlert(withMessage: msg, type: alertType.banner, on: self)
                }
            }) { error in
                objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
            }
        }
        
        
        
        func gotoNextVC(bizCount:Int){
            let sb = UIStoryboard(name:"SignUpProcess",bundle:Bundle.main)
            let objChooseType = sb.instantiateViewController(withIdentifier:"AddBusinessTypeVC") as! AddBusinessTypeVC
            if bizCount == 0{
                objChooseType.addBusiness = true
            }else{
                objChooseType.addBusiness = false
            }
            self.navigationController?.pushViewController(objChooseType, animated:false)
        }
        
        @IBAction func btnBusinessHours(_ sender:Any){
            self.view.endEditing(true)
            objAppShareData.editSelfServices = false
            let sb = UIStoryboard(name:"SignUpProcess",bundle:Bundle.main)
            let objChooseType = sb.instantiateViewController(withIdentifier:"BusinessHoursVC") as! BusinessHoursVC
            self.navigationController?.pushViewController(objChooseType, animated:true)
        }
        
        
        @IBAction func btnBookingSetting(_ sender:Any){
            self.view.endEditing(true)
            let sb = UIStoryboard(name:"SignUpProcess",bundle:Bundle.main)
            let objChooseType = sb.instantiateViewController(withIdentifier:"BookingManageVC") as! BookingManageVC
            self.navigationController?.pushViewController(objChooseType, animated:true)
        }
        
        @IBAction func btnPaymentType(_ sender:Any){
            self.view.endEditing(true)
            let sb = UIStoryboard(name:"SignUpProcess",bundle:Bundle.main)
            let objChooseType = sb.instantiateViewController(withIdentifier:"PaymentOptionVC") as! PaymentOptionVC
            self.navigationController?.pushViewController(objChooseType, animated:true)
        }
        
        @IBAction func btnBusinessAddress(_ sender:Any){
            self.view.endEditing(true)
            let  businessType =  objAppShareData.objModelBusinessSetup.serviceType
            if businessType == ""{
                objAppShareData.showAlert(withMessage: "Please select booking type", type: alertType.banner, on: self)
            }else{
                let sb = UIStoryboard(name:"Main",bundle:Bundle.main)
                let objVC = sb.instantiateViewController(withIdentifier:"AddAddressVC") as! AddAddressVC
                objVC.isOutcallOption = true
                objVC.fromRadiusVC = false
                self.navigationController?.pushViewController(objVC, animated: true)
                //self.addressVC(strFromAddress: "0")
            }
        }
        
        @IBAction func btnRadiusOfAreaCoverage(_ sender:Any){
            self.view.endEditing(true)
            let  businessType =  objAppShareData.objModelBusinessSetup.serviceType
            if businessType == ""{
                objAppShareData.showAlert(withMessage: "Please select booking type", type: alertType.banner, on: self)
            }else if self.lblBusinessAddress.text == ""{
                objAppShareData.showAlert(withMessage: "Please enter business address", type: alertType.banner, on: self)
            }else{
                self.addressVC(strFromAddress: "2")
            }
        }
        
        @IBAction func btnReturnLocationAddress(_ sender:Any){
            self.view.endEditing(true)
            
            let  businessType = objAppShareData.objModelBusinessSetup.serviceType
           
            if businessType == ""{
                objAppShareData.showAlert(withMessage: "Please select booking type", type: alertType.banner, on: self)
            }else{
                
                let sb = UIStoryboard(name:"Main",bundle:Bundle.main)
                let objVC = sb.instantiateViewController(withIdentifier:"AddAddressVC") as! AddAddressVC
                objVC.isOutcallOption = true
                objVC.fromRadiusVC = false
                self.navigationController?.pushViewController(objVC, animated: true)
                // self.addressVC(strFromAddress: "1")
            }
        }
        
        @IBAction func btnBusinessBreakAppoinments(_ sender:Any){
            
            self.view.endEditing(true)
            let businessType = objAppShareData.objModelBusinessSetup.serviceType
            if businessType == "1" ||  businessType == "2" ||  businessType == "3"{
                let sb = UIStoryboard(name:"SignUpProcess",bundle:Bundle.main)
                let objChooseType = sb.instantiateViewController(withIdentifier:"SetTimeVC") as! SetTimeVC
                let dict = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] ?? ["":""]
                     objChooseType.isEditingView = true
                self.navigationController?.pushViewController(objChooseType, animated:true)
            }else{
                objAppShareData.showAlert(withMessage: "Please select booking type", type: alertType.banner, on: self)
            }
        }
    }
    
    fileprivate extension AddBusinessDetailVC{
        
        func parseBusinessSignUpDetail(dictOfInfo:[String:Any]){
            var  v = ""
            if let id = dictOfInfo["__v"] as? String{
                v = id
            }else if let id = dictOfInfo["__v"] as? Int{
                v = String(id)
            }
            
//            var id1 = ""
//            if let ids = dictOfInfo["_id"] as? String{
//                id1 = ids
//            }else if let ids = dictOfInfo["_id"] as? Int{
//                id1 = String(ids)
//            }else if let ids = dictOfInfo["_Id"] as? String{
//                id1 = String(ids)
//            }else if let ids = dictOfInfo["_Id"] as? Int{
//                id1 = String(ids)
//            }
            
            var id1 = ""
            if let ids = dictOfInfo["user_id"] as? String{
                id1 = ids
            }else if let ids = dictOfInfo["user_id"] as? Int{
                id1 = String(ids)
            }else if let ids = dictOfInfo["user_id"] as? String{
                id1 = String(ids)
            }else if let ids = dictOfInfo["user_id"] as? Int{
                id1 = String(ids)
            }
            
            var bankStatus = ""
            if let id = dictOfInfo["bank_status"] as? String{
                bankStatus = id
            }else if let id = dictOfInfo["bank_status"] as? Int{
                bankStatus = String(id)
            }
            
            var certificateCount = ""
            if let id = dictOfInfo["certificate_count"] as? String{
                certificateCount = id
            }else if let id = dictOfInfo["certificate_count"] as? Int{
                certificateCount = String(id)
            }
            
            var chatId = ""
            if let id = dictOfInfo["chatId"] as? String{
                chatId = id
            }else if let id = dictOfInfo["chatId"] as? Int{
                chatId = String(id)
            }
            
            var deviceType = ""
            if let id = dictOfInfo["device_type"] as? String{
                deviceType = id
            }else if let id = dictOfInfo["device_type"] as? Int{
                deviceType = String(id)
            }
            
            var followersCount = ""
            if let id = dictOfInfo["followers_count"] as? String{
                followersCount = id
            }else if let id = dictOfInfo["followers_count"] as? Int{
                followersCount = String(id)
            }
            
            var followingCount = ""
            if let id = dictOfInfo["following_count"] as? String{
                followingCount = id
            }else if let id = dictOfInfo["following_count"] as? Int{
                followingCount = String(id)
            }
            
            var isDocument = ""
            if let id = dictOfInfo["is_document"] as? String{
                isDocument = id
            }else if let id = dictOfInfo["is_document"] as? Int{
                isDocument = String(id)
            }
            
            
            
            var radius = "0"
            if let id = dictOfInfo["radius"] as? String{
                radius = id
            }else if let id = dictOfInfo["radius"] as? Int{
                radius = String(id)
            }
            
            var serviceType = ""
            if let id = dictOfInfo["service_type"] as? String{
                serviceType = id
            }else if let id = dictOfInfo["service_type"] as? Int{
                serviceType = String(id)
            }
            var businessHours = ""
            if let id = dictOfInfo["business_hours"] as? String{
                businessHours = id
            }else if let id = dictOfInfo["business_hours"] as? Int{
                businessHours = String(id)
            }
            
            
            var bookingSetting = ""
            if let id = dictOfInfo["booking_setting"] as? String{
                bookingSetting = id
            }else if let id = dictOfInfo["booking_setting"] as? Int{
                bookingSetting = String(id)
            }
            
            
            var address = ""
            address = dictOfInfo["address"] as? String ?? ""
            
            var address2 = ""
            address2 = dictOfInfo["address2"] as? String ?? ""
            
            
            
            var businessName = "";
            businessName = dictOfInfo["business_name"] as? String ?? ""
            
            var businessType = ""
            businessType = dictOfInfo["business_type"] as? String ?? ""
            
            var businesspostalCode = ""
            businesspostalCode = dictOfInfo["business_postal_code"] as? String ?? ""
            
            var city = ""
            city = dictOfInfo["city"] as? String ?? ""
            
            var contactNo = ""
            contactNo = dictOfInfo["contact_no"] as? String ?? ""
            
            var country = ""
            country = dictOfInfo["country"] as? String ?? ""
            
            var countryCode = ""
            countryCode = dictOfInfo["country_code"] as? String ?? ""
            
            var crd = ""
            crd = dictOfInfo["crd"] as? String ?? ""
            
            var deviceToken = ""
            deviceToken = dictOfInfo["device_token"] as? String ?? ""
            
            var dob = ""
            dob = dictOfInfo["dob"] as? String ?? ""
            
            var email = ""
            email = dictOfInfo["email"] as? String ?? ""
            
            var firebaseToken = ""
            firebaseToken = dictOfInfo["firebase_token"] as? String ?? ""
            
            var firstName = ""
            firstName = dictOfInfo["first_name"] as? String ?? ""
            
            var gender = ""
            gender = dictOfInfo["gender"] as? String ?? ""
            
            var inCallpreprationTime = "00:00"
            inCallpreprationTime = dictOfInfo["in_call_prepration_time"] as? String ?? "00:00"
            
            
            var socialType = ""
            socialType = dictOfInfo["social_type"] as? String ?? ""
            
            var state = ""
            state = dictOfInfo["state"] as? String ?? ""
            
            var upd = ""
            upd = dictOfInfo["upd"] as? String ?? ""
            
            var userName = ""
            userName = dictOfInfo["user_name"] as? String ?? ""
            
            var userType = ""
            userType = dictOfInfo["user_type"] as? String ?? ""
            
            var profileImage = ""
            profileImage = dictOfInfo["profile_image"] as? String ?? ""
            
            var password = ""
            password = dictOfInfo["password"] as? String ?? ""
            
            var outCallpreprationTime = "00:00"
            outCallpreprationTime = dictOfInfo["out_call_prepration_time"] as? String ?? "00:00"
            
            var longitude = ""
            longitude = dictOfInfo["longitude"] as? String ?? ""
            
            var latitude = ""
            latitude = dictOfInfo["latitude"] as? String ?? ""
            
            var lastName = ""
            lastName = dictOfInfo["last_name"] as? String ?? ""
            
            var businessEmail = ""
            businessEmail = dictOfInfo["business_email"] as? String ?? ""
            
            var businessContactNo = ""
            businessContactNo = dictOfInfo["business_contact_no"] as? String ?? ""
            
            var location = ""
            location = dictOfInfo["location"] as? String ?? ""
            
            var incallTimeHours = "00"
            var incallTimeMinute = "00"
            
            if inCallpreprationTime == ""{
                inCallpreprationTime = "00:00"
            }
            let a = inCallpreprationTime.components(separatedBy: ":")
            incallTimeHours = "\(a[0])"
            incallTimeMinute =  "\(a[1])"
            
            var outcallTimeHours = "00"
            var outcallTimeMinute = "00"
            
            if outCallpreprationTime == ""{
                outCallpreprationTime = "00:00"
            }
            let b = outCallpreprationTime.components(separatedBy: ":")
            outcallTimeHours =  "\(b[0])"
            outcallTimeMinute = "\(b[1])"
            
            UserDefaults.standard.setValue(serviceType, forKey: UserDefaults.keys.mainServiceType)
            
            objAppShareData.objModelBusinessSetup.serviceType = serviceType
            objAppShareData.objModelBusinessSetup.businessContactNo = businessContactNo
            objAppShareData.objModelBusinessSetup.inCallpreprationTime = inCallpreprationTime
            objAppShareData.objModelBusinessSetup.outCallpreprationTime = outCallpreprationTime
            objAppShareData.objModelBusinessSetup.businessEmail = businessEmail
            objAppShareData.objModelBusinessSetup.businessName = businessName
            objAppShareData.objModelBusinessSetup.location = location
            objAppShareData.objModelBusinessSetup.address2 = address2
            objAppShareData.objModelBusinessSetup.country = country
            objAppShareData.objModelBusinessSetup.state = state
            objAppShareData.objModelBusinessSetup.city = city
            objAppShareData.objModelBusinessSetup.radius = radius
            objAppShareData.objModelBusinessSetup.address = address
            objAppShareData.objModelBusinessSetup.latitude = latitude
            objAppShareData.objModelBusinessSetup.longitude = longitude
            objAppShareData.objModelBusinessSetup.incallTimeHours = incallTimeHours
            objAppShareData.objModelBusinessSetup.incallTimeMinute = incallTimeMinute
            objAppShareData.objModelBusinessSetup.outcallTimeHours = outcallTimeHours
            objAppShareData.objModelBusinessSetup.outcallTimeMinute = outcallTimeMinute
            objAppShareData.objModelBusinessSetup.businessHours = businessHours
            objAppShareData.objModelBusinessSetup.bookingSetting = bookingSetting
            self.configureView(businessType: serviceType)
          //  self.parseDataFromLocal()
        }
        
        //Get User Saved Information
        func callWSGetBusinessInfo(){
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            let userDict = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfoDic) as? [String:Any] ?? ["":""]
            let businessID = userDict[UserDefaults.keys.business_id]as? String ?? ""
            let finalURL = "\(WebURL.getBusinessSavedInfo)\(businessID)"
            objWebserviceManager.requestGetNew(strURL: finalURL, params: nil, strCustomValidation: "", success: { (response) in
                print(response)
                
                let status = response["status"]as? String ?? ""
                
                if status == "success"{
                    
                    if let arrData = response["artistRecord"]as? [[String: Any]]{
                        
                        if arrData.count != 0{
                            
                            for dict in arrData{
                                //Set Business Name
                                if let businessName = dict["business_name"]as? String{
                                    self.txtBusinessName.text = businessName
                                }
                                //Set Business Email
                                if let businessEmail = dict["business_email"]as? String{
                                    self.txtBusinessEmail.text = businessEmail
                                }
                                //Set Busines Contact No
                                if let businessContactNo = dict["business_contact_no"]as? String{
                                    self.txtPhoneNumber.text = businessContactNo
                                }
                       //============================================================================//
                                var businessServiceType = ""
                                if let serviceType = dict["service_type"]as? String{
                                    businessServiceType = serviceType
                                }else if let serviceType = dict["service_type"]as? Int{
                                    businessServiceType = "\(serviceType)"
                                }
                                
                                if businessServiceType == "1"{
                                    self.lblBookinkType.text = "In Call"
                                   // objAppShareData.objModelBusinessSetup.serviceType = "1"
                                }else if businessServiceType == "2"{
                                    self.lblBookinkType.text = "Out Call"
                                   // objAppShareData.objModelBusinessSetup.serviceType = "2"
                                }else if businessServiceType == "3"{
                                    self.lblBookinkType.text = "In Call/Out Call"
                                   // objAppShareData.objModelBusinessSetup.serviceType = "3"
                                }else{
                                    self.lblBookinkType.text = "In Call/Out Call"
                                  //  objAppShareData.objModelBusinessSetup.serviceType = "3"
                                }
                                self.configureView(businessType: "2")
              //============================================================================//
                                var businessBookingOption = ""
                                if let bookingOption = dict["booking_setting"]as? String{
                                    businessBookingOption = bookingOption
                                }else if let bookingOption = dict["booking_setting"]as? Int{
                                    businessBookingOption = "\(bookingOption)"
                                }
                                
                                if businessBookingOption == "1"{
                                    self.lblBookingSetting.text = "Automatic"
                                    objAppShareData.objModelBusinessSetup.bookingSetting = "1"
                                }else if businessBookingOption == "2"{
                                    self.lblBookingSetting.text = "Manual"
                                    objAppShareData.objModelBusinessSetup.bookingSetting = "2"
                                }else{
                                    self.lblBookingSetting.text = "Automatic"
                                    objAppShareData.objModelBusinessSetup.bookingSetting = "1"
                                }
                                
                //============================================================================//
                                
                                if let businessAddress = dict["address"]as? String{
                                    self.lblBusinessAddress.text = businessAddress
                                    self.lblLocationAddress.text = businessAddress
                                    objAppShareData.objModelBusinessSetup.address = businessAddress
                                }
               //============================================================================//
                                if let radius = dict["radius"]as? String{
                                   // self.lblRadiusOfArea.text = radius
                                    objAppShareData.objModelBusinessSetup.radius = radius
                                }else if let radius = dict["radius"]as? Int{
                                    objAppShareData.objModelBusinessSetup.radius = "\(radius)"
                                   // self.lblRadiusOfArea.text = "\(radius)"
                                }
                                self.configureView(businessType: objAppShareData.objModelBusinessSetup.serviceType)
              //============================================================================//
                                
                                var businessPaymentType = ""
                                if let paymentType = dict["pay_option"]as? String{
                                    businessPaymentType = paymentType
                                }else if let paymentType = dict["pay_option"]as? Int{
                                    businessPaymentType = "\(paymentType)"
                                }
                                
                                // Set Payment Type
                                if businessPaymentType == "1"{
                                    self.lblPaymentType.text = "Card"
                                   // objAppShareData.objModelBusinessSetup.paymentType = "1"
                                }else if businessPaymentType == "2"{
                                    self.lblPaymentType.text = "Cash"
                                   // objAppShareData.objModelBusinessSetup.paymentType = "2"
                                }else if businessPaymentType == "3"{
                                    self.lblPaymentType.text = "Both"
                                   // objAppShareData.objModelBusinessSetup.paymentType = "3"
                                }else{
                                    self.lblPaymentType.text = "Cash"
                                  //  objAppShareData.objModelBusinessSetup.paymentType = "2"
                                }
                    //============================================================================//
                                
                                if let incallTime = dict["in_call_prepration_time"]as? String{
                                    objAppShareData.objModelBusinessSetup.inCallpreprationTime = incallTime
                                }
                                
                                if let outcall = dict["out_call_prepration_time"]as? String{
                                    objAppShareData.objModelBusinessSetup.outCallpreprationTime = outcall
                                }
                   //============================================================================//
                                
                                if (dict["business_hour"]as? [[String:Any]]) != nil{
                                    objAppShareData.objModelBusinessSetup.businessHours = "1"
                                }else{
                                    objAppShareData.objModelBusinessSetup.businessHours = "0"
                                }
                                
                                self.serviceBreakTime()
                                self.buttonHiddenShowManage()
                                
                            }
                            
                        }else{
                            
                        }
                        
                    }else{
                        
                    }
                    
                }else{
                    
                }
                
                
            }) { (Error) in
                print(Error)
            }
            
        }
}





//
//  EditMyInfo.swift
//  MualabBusiness
//
//  Created by Mac on 30/11/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

class EditMyInfo: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var viewPicker:UIView!
    @IBOutlet weak var DOBPicker : UIDatePicker!
    @IBOutlet weak var txtFirstName:UITextField!
    @IBOutlet weak var txtLastName:UITextField!
    @IBOutlet weak var txtDOB:UITextField!
    @IBOutlet weak var txtUserName:UITextField!
    @IBOutlet weak var txtContNumb:UITextField!
    @IBOutlet weak var txtEmail:UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
 
        self.viewPicker.isHidden = true
        self.txtFirstName.delegate = self
        self.txtLastName.delegate = self
        self.txtDOB.delegate = self
        self.txtUserName.delegate = self
        self.txtContNumb.delegate = self
        self.txtEmail.delegate = self
       self.DOBPicker.maximumDate = Date()
        
     }
    override func viewWillAppear(_ animated: Bool) {
        self.viewConfigure()
        let navigationVC = UserDefaults.standard.string(forKey: UserDefaults.keys.navigationVC) ?? "1"

        if navigationVC == "2"{
            let sb = UIStoryboard(name:"SignUpProcess",bundle:Bundle.main)
            let objChooseType = sb.instantiateViewController(withIdentifier:"AddBusinessDetailVC") as! AddBusinessDetailVC
            self.navigationController?.pushViewController(objChooseType, animated: false)
        }else if navigationVC == "3"{
             let sb = UIStoryboard(name:"SignUpProcess",bundle:Bundle.main)
            let objChooseType = sb.instantiateViewController(withIdentifier:"AddBusinessTypeVC") as! AddBusinessTypeVC
            self.navigationController?.pushViewController(objChooseType, animated: false)
        }else if navigationVC == "4"{
             let sb = UIStoryboard(name:"SignUpProcess",bundle:Bundle.main)
            let objChooseType = sb.instantiateViewController(withIdentifier:"AddBankDetailVC") as! AddBankDetailVC
            self.navigationController?.pushViewController(objChooseType, animated: false)
        }else{
          UserDefaults.standard.setValue("1", forKey: UserDefaults.keys.navigationVC)
        }

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
//MARK: - Custome method extension
fileprivate extension EditMyInfo{
    func viewConfigure(){
 
        
        if  let dict = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfoDic) as? [String:Any]{
            print(dict)
        
        self.txtFirstName.text = dict[UserDefaults.myDetail.firstName] as? String ?? ""
        self.txtLastName.text = dict[UserDefaults.myDetail.lastName] as? String ?? ""
        self.txtUserName.text = dict[UserDefaults.myDetail.userName] as? String ?? ""
        self.txtEmail.text = dict[UserDefaults.myDetail.email] as? String ?? ""
        self.txtContNumb.text = (dict[UserDefaults.myDetail.countryCode] as? String ?? "")+" - "+(dict[UserDefaults.myDetail.contactNo] as? String ?? "")
        self.txtDOB.text = dict[UserDefaults.myDetail.dob] as? String ?? ""
        }
        
    }
}

//MARK: - button extension
 extension EditMyInfo{
func textFieldShouldReturn(_ textField: UITextField) -> Bool{
    if textField == self.txtFirstName{
        txtLastName.becomeFirstResponder()
    }else if textField == self.txtLastName{
        txtLastName.resignFirstResponder()
    }
    return true
}
}
//MARK: - button extension
fileprivate extension EditMyInfo{
    
    @IBAction func btnBack(_ sender:Any){
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func btnContinew(_ sender:Any){
        self.view.endEditing(true)

        var invalid = false
        var strMessage = ""
        
        txtFirstName.text = txtFirstName.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        txtLastName.text = txtLastName.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        txtDOB.text = txtDOB.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        txtUserName.text = txtUserName.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        if self.txtFirstName.text?.count == 0{
            invalid = true
            objAppShareData.shakeViewField(self.txtFirstName)
        }else if self.txtLastName.text?.count == 0{
            invalid = true
            objAppShareData.shakeViewField(self.txtLastName)
        }else if !(objValidationManager.isValidateName(strFullname: self.txtFirstName.text!)){
            strMessage = message.validation.firstName
            invalid = true
        }else if !(objValidationManager.isValidateName(strFullname: self.txtLastName.text!)){
            strMessage = message.validation.lastName
            invalid = true
        }else if self.txtDOB.text?.count == 0{
            invalid = true
            objAppShareData.shakeViewField(self.txtDOB)
        }else if self.txtUserName.text?.count == 0{
            invalid = true
            objAppShareData.shakeViewField(self.txtUserName)
        }else if (self.txtDOB.text?.count)! < 4{
            invalid = true
            strMessage = message.validation.dob
        }else if (self.txtUserName.text?.count)! < 4{
            invalid = true
            strMessage = message.validation.userNameLength
        }else if self.txtUserName.text?.range(of:" ") != nil{
            invalid = true
            strMessage = message.validation.userNameSpace
        }
        if invalid && strMessage.count>0 {
            objAppShareData.showAlert(withMessage: strMessage, type: alertType.banner, on: self)
        }else if !invalid{
            self.view.endEditing(true)
            self.callWebserviceForRegistration()
        }
    }
    

    
    @IBAction func btnSelectDOB(_ sender:Any){
 
        self.view.endEditing(true)
        self.viewPicker.isHidden = false
        
        if self.txtDOB.text != ""{
            let a = self.txtDOB.text?.components(separatedBy: "/")
            if a?.count == 3 {
            self.DOBPicker.date = objAppShareData.setDateFromString(time:self.txtDOB.text!)
            }
        }
    }
    
    @IBAction func btnDoneCancleDOBSelection(_ sender:UIButton){
        self.view.endEditing(true)

        if sender.tag == 1 {
            self.txtDOB.text = objAppShareData.dateFormatToShow(forAPI: self.DOBPicker.date)
            self.txtDOB.text = objAppShareData.dateFormatToShowDace(forAPI: self.DOBPicker.date)
           //strDOBForServer = objAppShareData.dateFormat2(forAPI: self.DOBPicker.date)
         }
        
        //if self.txtDOB.text != ""{
        //    let dateFormatter = DateFormatter()
        //   dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
        //    dateFormatter.timeZone = NSTimeZone(abbreviation: "UTC") as TimeZone!
        //   let dataDate = dateFormatter.date(from: self.txtDOB.text!+" 12:00:00" as! String)!
        // self.DOBPicker.date = dataDate
        //}

        self.viewPicker.isHidden = true
        
    }
}

//MARK: - call webservice for update my data
extension EditMyInfo{
    
    func callWebserviceForRegistration(){
        if !objServiceManager.isNetworkAvailable(){
        objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        objActivity.startActivityIndicator()
        let dict = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfoDic) as? [String:Any] ?? ["":""]
        
       let strDOBForServer = objAppShareData.dateFormat2(forAPI: self.DOBPicker.date)
        
        print(dict)

        var id = ""
        if let id1 = dict[UserDefaults.myDetail.Userid] as? Int{
            id = String(id1)
        }else if let id1 = dict[UserDefaults.myDetail.Userid] as? String{
            id = id1
        }
        let dicParam = [//"user_name":self.txtUserName.text ?? "",
                        "first_name":self.txtFirstName.text ?? "",
                        "last_name":self.txtLastName.text ?? "",
                        "user_id":String(id),
                       // "email":self.txtEmail.text ?? "",
                       // "is_document":"1",
                        "dob":strDOBForServer] as [String : Any]
        print(dicParam)
        
        objWebserviceManager.requestPost(strURL: WebURL.updateMyInfo, params: dicParam, success: { (response) in
            
     //      objServiceManager.uploadMultipartData(strURL: WebURL.updateMyInfo, params: dicParam as [String : AnyObject], imageData:Data(), fileName: "file.jpg", key: "", mimeType: "image/*", success: { response in
            
            
            if response["status"] as? String == "success"{
                // objAppShareData.showAlert(withMessage: "Register successfully , App under development", on: self)
                 
                if let dictUserDetail = response["data"] as? [String:Any]{
                    
                    //
                    var userDict = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfoDic) as? [String:Any] ?? ["":""]
                    print(userDict)
                    
                    if let firstName = dictUserDetail["first_name"]as? String{
                        userDict["\(UserDefaults.keys.firstName)"] = firstName
                    }
                    if let lastName = dictUserDetail["last_name"]as? String{
                       userDict["\(UserDefaults.keys.lastName)"] = lastName
                    }
                    if let dob = dictUserDetail["dob"]as? String{
                       userDict["\(UserDefaults.keys.dob)"] = dob
                    }
                
                    UserDefaults.standard.setValue(userDict, forKey: UserDefaults.keys.userInfoDic)

                    
                   // objAppShareData.objAppdelegate.parseBusinessSignUpDetail(dictOfInfo: dictUserDetail)
                    self.gotoNextVC()
                 }
            }else{
                let msg = response["message"] as! String
                objAppShareData.showAlert(withMessage: msg, type: alertType.banner, on: self)
            }
        }) { error in
            //objAppShareData.showDeafultAlert(withTitle: message.error.title, message: message.error.wrong, on: self)
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
  func  gotoNextVC(){
    let sb = UIStoryboard(name:"SignUpProcess",bundle:Bundle.main)
    let objChooseType = sb.instantiateViewController(withIdentifier:"AddBusinessDetailVC") as! AddBusinessDetailVC
    let businessType = UserDefaults.standard.string(forKey: UserDefaults.keys.mainServiceType) ?? ""

    if businessType == "" ||  businessType == "0" {
        UserDefaults.standard.setValue("3", forKey: UserDefaults.keys.mainServiceType)
    }
    self.navigationController?.pushViewController(objChooseType, animated: true)
    }
    
}

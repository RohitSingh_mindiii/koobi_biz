//
//  BusinessHoursVC.swift
//  MualabBusiness
//
//  Created by Mac on 03/01/2018 .
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import Foundation

fileprivate enum day:Int {
    case Monday
    case Tuesday
    case Wednesday
    case Thursday
    case Friday
    case Saturday
    case Sunday
    
    var string: String {
        return String(describing: self)
    }
}

class BusinessHoursVC: UIViewController {
    
    @IBOutlet weak var startTimePicker : UIDatePicker!
    @IBOutlet weak var endTimePicker : UIDatePicker!
    
    @IBOutlet weak var timePickerBottem : NSLayoutConstraint!
    
    @IBOutlet weak var tblBusinessHours : UITableView!
    
    @IBOutlet weak var lblTimes : UILabel!
    @IBOutlet weak var lblDay : UILabel!
    
    @IBOutlet weak var btnNext : UIButton!
    @IBOutlet weak var viewProgress : UIView!
 
    
    fileprivate var arrOpeningTimes = [openingTimes]()
    
    fileprivate var tblTag = Int()
    fileprivate var timeTag = Int()
    fileprivate var staffId = String()
    fileprivate var businessId = String()
    
}

//MARK: - View hirarchy
extension BusinessHoursVC{
    override func viewDidLoad() {
        super.viewDidLoad()
        makeDeafultsDay()
     //   callWebserviceForGetBusinessProfile()
        // Do any additional setup after loading the view.
        
        
         let dict = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfoDic) as? [String:Any] ?? ["":""]
        print(dict)
        
        if let staff_id = dict["staff_id"]as? String{
            self.staffId = staff_id
            print(self.staffId)
        }
        if let business_id = dict["business_id"]as? String{
            self.businessId = business_id
            print(self.businessId)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //makeDeafultsDay()
        customTimePickers()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.configureView()
        }
        
        if objAppShareData.editSelfServices == true{
            self.viewProgress.isHidden = true
             self.btnNext.setTitle("Done", for: .normal)
           }else{
            self.tblBusinessHours.isUserInteractionEnabled = true
            self.viewProgress.isHidden = false
             self.btnNext.setTitle("Continue", for: .normal)
            self.btnNext.isHidden = false
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
}
//MARK: -Local Methods
fileprivate extension BusinessHoursVC{
    func configureView() {
        self.timePickerBottem.constant = -self.view.frame.size.height/1.5
        self.btnNext.layer.cornerRadius = self.btnNext.frame.size.height/2
    }
    
    func customTimePickers()  {
        //startTimePicker.setValue(UIColor.theameColors.pinkColor, forKeyPath: "textColor")
        // endTimePicker.setValue(UIColor.theameColors.pinkColor, forKeyPath: "textColor")
        startTimePicker.date = self.setDateFrom("10:00 AM")
        endTimePicker.minimumDate = startTimePicker.date.addingTimeInterval(1800.0)
        endTimePicker.date = self.setDateFrom("07:00 PM")
        startTimePicker.addTarget(self, action: #selector(self.dateChanged(_:)), for: .valueChanged)
        endTimePicker.addTarget(self, action: #selector(self.dateChanged(_:)), for: .valueChanged)
    }
    
    //UIDatePicker methods
    @objc func dateChanged(_ sender: UIDatePicker) {
        endTimePicker.minimumDate = startTimePicker.date.addingTimeInterval(1800.0)
        //        if sender == startTimePicker{
        //            endTimePicker.date = sender.date.addingTimeInterval(600.0)
        //            print(sender.date)
        //        }else{
        //            print(sender.date)
        //        }
        self.lblTimes.text = "From: \(self.timeFormat(forAPI: self.startTimePicker.date)) To: \(self.timeFormat(forAPI: self.endTimePicker.date))"
    }
    
    func makeDeafultsDay() {
        self.arrOpeningTimes.removeAll()
        let arrDays = ["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"]
        for day in arrDays{
            let objTimeSloat = timeSloat.init(startTime:"10:00 AM",endTime:"07:00 PM")
            let arr = [objTimeSloat] as! [timeSloat]
            var objOpenings:openingTimes?
//            if day == "Saturday" || day == "Sunday"{
//                let ar = [timeSloat]()
//                objOpenings = openingTimes.init(open: false, day: day, times: ar)
//            }else{
//                objOpenings = openingTimes.init(open: true, day: day, times: arr)
//            }
            if day == "Sunday"{
                let ar = [timeSloat]()
                objOpenings = openingTimes.init(open: false, day: day, times: ar)
            }else{
                objOpenings = openingTimes.init(open: true, day: day, times: arr)
            }
            
            self.arrOpeningTimes.append(objOpenings!)
        }
        self.tblBusinessHours.reloadData()
    }
    
    func validateTimeSloat(startTime:Date,endTime:Date) -> Bool {
        
        let sTimePicker = self.timeFormat(forAPI: self.startTimePicker.date)
        let eTimePicker = self.timeFormat(forAPI: self.endTimePicker.date)
        
        let sTimeForCheck = self.setDateFrom(sTimePicker)
        let eTimeForCheck = self.setDateFrom(eTimePicker)
        
        if sTimeForCheck > startTime && eTimeForCheck < endTime{
            return false
        }else if sTimeForCheck >= startTime && sTimeForCheck <= endTime{
            return false
        }else if eTimeForCheck >= startTime && eTimeForCheck <= endTime {
            return false
        }else if sTimeForCheck < startTime && eTimeForCheck > endTime{
            return false
        }
        return true
        
    }
    //MARK: - validate time for final submission
    
    func validateTimeSloatFinal(startTime:Date,endTime:Date,checkStartTime:Date,checkEndTime:Date) -> Bool {
        
        let sTimePicker = self.timeFormat(forAPI: checkStartTime)
        let eTimePicker = self.timeFormat(forAPI:checkEndTime)
        
        let sTimeForCheck = self.setDateFrom(sTimePicker)
        let eTimeForCheck = self.setDateFrom(eTimePicker)
        
        if sTimeForCheck > startTime && eTimeForCheck < endTime{
            return false
        }else if sTimeForCheck >= startTime && sTimeForCheck <= endTime{
            return false
        }else if eTimeForCheck >= startTime && eTimeForCheck <= endTime {
            return false
        }else if sTimeForCheck < startTime && eTimeForCheck > endTime{
            return false
        }
        return true
    }
    
    //MARK: - Open Time Picker
    
    func openTimePicker(tableTag:Int,timeArrayIndex:Int) {
        
        self.timePickerBottem.constant = 0;
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
        self.tblTag = tableTag
        self.timeTag = timeArrayIndex
        
        let objOpenings = self.arrOpeningTimes[self.tblTag]
        self.lblDay.text = objOpenings.strDay
        let times = objOpenings.arrTimeSloats [self.timeTag]
        self.lblTimes.text = "From: \(times.strStartTime) To: \(times.strEndTime)"
        self.startTimePicker.date = self.setDateFrom(times.strStartTime)
        self.endTimePicker.date = self.setDateFrom(times.strEndTime)
    }
    
    //MARK: - Remove time sloat
    func deleteTimeSloat(tableTag:Int,timeArrayIndex:Int) {
        self.btnCancelTimeSelection(0)
        let objOpening = self.arrOpeningTimes[tableTag]
        objOpening.arrTimeSloats.remove(at: timeArrayIndex)
        self.tblBusinessHours.reloadData()
    }
    
    func gotoCallOptionVC(animated:Bool){
        objAppShareData.objModelBusinessSetup.businessHours = "1"
        self.navigationController?.popViewController(animated: true)
    }
    //MARK: - Make String for API
    func jsonFromArray(arrBusinessHours:[openingTimes]) ->String{
        var arr = [[String:Any]]()
        var dataString = ""
        /*
         [{"staff_id":"9","business_id":"10","day":"0","start_time":"10:00 AM","end_time":"11:00 AM","status":"1"},
         */
        for (index,openings) in arrBusinessHours.enumerated(){
            for time in openings.arrTimeSloats{
                let dic = ["day":index,
                           "start_time":time.strStartTime,
                           "end_time":time.strEndTime,
                           "staff_id":self.staffId,
                           "business_id":self.businessId,
                           "status":Int(truncating:NSNumber.init(value: openings.isOpen))] as [String : Any]
                arr.append(dic)
            }
        }
        //JSONSerialization.data(withJSONObject: object, options: .prettyPrinted), encoding: .utf8 )!)
        //JSONSerialization.WritingOptions(rawValue: 0)
        if let objectData = try? JSONSerialization.data(withJSONObject: arr, options: JSONSerialization.WritingOptions(rawValue: 0)) {
            let objectString = String(data: objectData,encoding: .utf8)
            dataString = objectString!.data(using: .utf8)!.prettyPrintedJSONString! as String
            print(dataString)
           // dataString = dataString.data(using: .utf8)!.prettyPrintedJSONString! as String
            
            return dataString
        }
        return dataString
    }
    func removeTimeObjectsArray(){
        for obj in self.arrOpeningTimes{
            obj.arrTimeSloats.removeAll()
            obj.isOpen = false
        }
    }
}
//MARK : - Save Methods
fileprivate extension BusinessHoursVC{
    
    func saveData(_ dicResponse:[String:Any]){
        print(dicResponse)
        let arr = dicResponse["businessHour"] as! [[String:Any]]
        let bankStatus = dicResponse["bankStatus"] as! Bool
        UserDefaults.standard.set(bankStatus, forKey: UserDefaults.keys.bankStatus)
        UserDefaults.standard.synchronize()
        if arr.count>0{
            self.removeTimeObjectsArray()
            for dic in arr{
                if let d = dic["day"] as? Int{
                    for (index,openings) in self.arrOpeningTimes.enumerated(){
                        if d == index{
                            openings.isOpen = true
                            openings.strDay = (day(rawValue:index)?.string)!
                            let objTimeSloat = timeSloat.init(startTime:dic["startTime"] as! String,endTime:dic["endTime"] as! String)
                            openings.arrTimeSloats.append(objTimeSloat!)
                        }
                    }
                }
            }
            self.tblBusinessHours.reloadData()
        }
    }
}
//MARK : - IBAction
fileprivate extension BusinessHoursVC{
    
    @IBAction func btnDone( _ sender:Any){
        
        let objOpenings = self.arrOpeningTimes[self.tblTag]
        let objTimeSloat = objOpenings.arrTimeSloats[self.timeTag]
        
        for (index, times) in (objOpenings.arrTimeSloats.enumerated()){
            if index != self.timeTag{
                if !validateTimeSloat(startTime: self.setDateFrom(times.strStartTime), endTime: self.setDateFrom(times.strEndTime)){
                    print("Invalid Date")
                    objAppShareData.showAlert(withMessage: "Time slot in the same day cannot intersect.", type: alertType.bannerDark, on: self)
                    return
                }
            }
        }
        
        objTimeSloat.strStartTime = self.timeFormat(forAPI: self.startTimePicker.date)
        objTimeSloat.strEndTime = self.timeFormat(forAPI: self.endTimePicker.date)
        
        //Update table
        let section = 0
        let row = self.tblTag
        let indexPath = IndexPath(row: row, section: section)
        let cell = tblBusinessHours.cellForRow(at: indexPath) as! BusinessHoursTableCell
        cell.tblTime?.reloadData()
        
        self.btnCancelTimeSelection(0)
        
    }
    
    @IBAction func btnCancelTimeSelection(_ sender:Any){
        
        self.timePickerBottem.constant = -self.view.frame.size.height/1.5;
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    @IBAction func btnBack(_ sender:Any){
        objAppShareData.editSelfServices = false
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnIsSelecteDay(_ sender:UIButton){
        
        let section = 0
        let row = sender.tag
        let indexPath = IndexPath(row: row, section: section)
        let cell = tblBusinessHours.cellForRow(at:indexPath) as! BusinessHoursTableCell
        
        let objOpening = self.arrOpeningTimes[row]

        if objOpening.isOpen{
            objOpening.isOpen = false
            cell.btnIsOpen?.isSelected = false
            objOpening.arrTimeSloats.removeAll()
        }else{
            cell.btnIsOpen?.isSelected = true
            objOpening.isOpen = true
            let objTimeSloat = timeSloat.init(startTime: "10:00 AM",endTime: "07:00 PM")
            objOpening.arrTimeSloats.append(objTimeSloat!)
        }
        self.btnCancelTimeSelection(0)
        self.tblBusinessHours.reloadData()
        //cell.tblTime?.reloadData()
    }
    
    @IBAction func btnAddTimeSloat(_ sender:UIButton){
        
        let row = sender.tag
        let objOpening = self.arrOpeningTimes[row]
        if objOpening.arrTimeSloats.count < 2{
            let objTimeSloat = timeSloat.init(startTime:"10:00 AM",endTime:"07:00 PM")
            objOpening.arrTimeSloats.append(objTimeSloat!)
            self.tblBusinessHours.reloadData()
        }else{
            objAppShareData.showAlert(withMessage: "Max time slot reached", type: alertType.bannerDark, on: self)
        }
    }
    
    @IBAction func btnLogout(_ sender:UIButton){
        appDelegate.logout()
    }
    
    @IBAction func btnNext(_ sender:UIButton){
        
        var checkForNext = false
        for objOpen in self.arrOpeningTimes{
            let objTimeSloat = objOpen.arrTimeSloats
            if objOpen.arrTimeSloats.count > 0 {
                checkForNext = true
            }
            if objTimeSloat.count > 1{
                let time1 = objTimeSloat[0]
                let time2 = objTimeSloat[1]
                if !validateTimeSloatFinal(startTime: self.setDateFrom(time1.strStartTime),endTime: self.setDateFrom(time1.strEndTime), checkStartTime:self.setDateFrom(time2.strStartTime),checkEndTime:self.setDateFrom(time2.strEndTime)){
                    print("Invalid Date")
                    objAppShareData.showAlert(withMessage: "Time slot in the same day cannot intersect.", type: alertType.bannerDark, on: self)
                    return
                }
            }
        }
        if checkForNext{
            self.callWebserviceForAddBusinessHours()
        }else{
            objAppShareData.showAlert(withMessage: "Select at least one day operation hours", type: alertType.bannerDark, on: self)
            return
        }
    }
}
//Webservices
fileprivate extension BusinessHoursVC{
    
    func callWebserviceForAddBusinessHours() {
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        objActivity.startActivityIndicator()
        let dicParam = ["business_hour":self.jsonFromArray(arrBusinessHours: self.arrOpeningTimes),"business_id":self.businessId,"staff_id":self.staffId]
        print(dicParam)
        objServiceManager.requestPost(strURL: WebURL.addBusinessHour, params: dicParam, success: { response in
            print(response)
            if response["status"] as? String == "success"{
                if let dic = response["message"] as? [[String:Any]]{
                    UserDefaults.standard.set(dic, forKey: UserDefaults.keys.businessHour)
                    UserDefaults.standard.synchronize()
                    var staffCounts = 0
                        if let staffCount = response["staff_count"] as? Int{
                            staffCounts = staffCount
                        }
                        
                    if objAppShareData.editSelfServices == true && staffCounts > 0 {
                        objAppShareData.showAlert(withMessage: "If you edit operation hours it will also change at the staff's side accordingly.", type: alertType.bannerDark,on: self)                        
                        let sb = UIStoryboard(name:"StaffList",bundle:Bundle.main)
                        if let objVC = sb.instantiateViewController(withIdentifier:"UserListAddStaffVC") as? UserListAddStaffVC{
                            objVC.hidesBottomBarWhenPushed = true
                            self.navigationController?.pushViewController(objVC, animated: true)
                        }
                    } else{
                        self.gotoCallOptionVC(animated: true)
                    }
                }
            }else{
                let msg = response["message"] as? String ?? ""
                objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
            }
        }) { error in
            print(error)
            //objAppShareData.showDeafultAlert(withTitle: message.error.title, message: message.error.wrong, on: self)
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
    func callWebserviceForGetBusinessProfile(){
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        
        objActivity.startActivityIndicator()
        objServiceManager.requestGetForJson(strURL: WebURL.getBusinessProfile, params: [:], success: { response in
            if  response["status"] as! String == "success"{
                print(response)
                let rs = response["artist_record"] as! NSArray
                let dic = rs[0] as! [String:Any]
                self.saveData(dic)
            }else{
                let msg = response["message"] as! String
                
                objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
            }
        }) { error in
            
            // objAppShareData.showDeafultAlert(withTitle: message.error.title, message: message.error.wrong, on: self)
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
}
//MARK: - UITableViewDelegate and UITableViewDataSource
extension BusinessHoursVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOpeningTimes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblBusinessHours.dequeueReusableCell(withIdentifier: "BusinessHoursTableCell") as! BusinessHoursTableCell
        
        let objOpenings = self.arrOpeningTimes[indexPath.row]
        if objOpenings.arrTimeSloats.count == 0{
            cell.lblDay?.textColor = UIColor.lightGray
        }else{
            cell.lblDay?.textColor =  UIColor.black
        }
        cell.lblDay?.text = objOpenings.strDay
        cell.btnIsOpen?.isSelected = objOpenings.isOpen
        cell.arrTime = objOpenings.arrTimeSloats
        cell.delegate = self
        if objOpenings.isOpen{
            cell.btnAdd?.isHidden = false
            cell.lblNotWorking?.isHidden = true
        }else{
            cell.btnAdd?.isHidden = true
            cell.lblNotWorking?.isHidden = false
        }
        //
        cell.tblTime?.tag = indexPath.row
        cell.btnIsOpen?.tag = indexPath.row
        cell.btnAdd?.tag = indexPath.row
        //
        cell.tblTimeHeight?.constant = CGFloat((objOpenings.arrTimeSloats.count * 26)+26)+8
        
        if objOpenings.arrTimeSloats.count == 0 || objOpenings.arrTimeSloats.count == 1{
            cell.tblTimeHeight?.constant = 35
        }
        if objOpenings.arrTimeSloats.count == 2 {
            cell.tblTimeHeight?.constant = 62
        }
        
        cell.tblTime?.isUserInteractionEnabled = true
         cell.tblTime?.reloadData()
        return cell
    }
}

//MARK: - BusinessHoursTableCellDelegate

extension BusinessHoursVC : BusinessHoursTableCellDelegate{
    
    func removeTimeSloat(withSenderTable tableView: UITableView, timeArrayIndex: Int) {
        
        self.deleteTimeSloat(tableTag: tableView.tag, timeArrayIndex: timeArrayIndex)
    }
    
    func selectedTimeSloat(withSenderTable tableView:UITableView, timeArrayIndex:Int){
        
        openTimePicker(tableTag: tableView.tag, timeArrayIndex: timeArrayIndex)
    }
}


//MARK: - Date formates

fileprivate extension BusinessHoursVC {
    
    //MARK: - Date time format
    func timeFormat(forAPI date: Date) -> String {
        let formatter = DateFormatter()
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        formatter.dateFormat = "hh:mm a"
        let formattedTime: String = formatter.string(from: date)
        return formattedTime.uppercased()
    }
    
    // Set deafult Times from string
    func setDateFrom(_ time: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        dateFormatter.dateFormat = "hh:mm a"
        let date = dateFormatter.date(from: time)
        
        return date!
    }
}





//Json Encoding
extension Data {
    var prettyPrintedJSONString: NSString? { /// NSString gives us a nice sanitized debugDescription
        guard let object = try? JSONSerialization.jsonObject(with: self, options: []),
              let data = try? JSONSerialization.data(withJSONObject: object, options: [.prettyPrinted]),
              let prettyPrintedString = NSString(data: data, encoding: String.Encoding.utf8.rawValue) else { return nil }

        return prettyPrintedString
    }
}

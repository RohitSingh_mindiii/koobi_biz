//
//  ModelServicesList.swift
//  MualabBusiness
//
//  Created by Mac on 19/11/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

class ModelBusinessTypeList:NSObject{
    var businessName = ""
    var businessId = ""
    
    var arrServiceList = [ModelServicesList]()
    var arrBusinessList = [ModelCategoryList]()
    
    var countBusinessList = 0

    
    init(dict: [String : Any]) {
    businessName = dict["businessName"] as? String ?? ""
    businessId = dict["businessId"] as? String ?? ""

        if let array = dict["arrCategory"] as? [[String:Any]]{
            for obj in array{
                let object = ModelCategoryList.init(dict: obj)
                    self.arrBusinessList.append(object)
                    countBusinessList = countBusinessList+(object.arrServices.count)
            }
        }
 
        if let array = dict["arrServices"] as? [[String:Any]]{
            for obj in array{
                let object = ModelServicesList.init(dict: obj)
                self.arrServiceList.append(object)
            }
        }
}
}



class ModelCategoryList: NSObject {
    var categoryName = ""
    var businessName = ""

    var categoryId = ""

    var arrServices = [ModelServicesList]()
    
    
    
    init(dict: [String : Any]) {
        categoryName = dict["categoryName"] as? String ?? ""
        businessName = dict["businessName"] as? String ?? ""

        categoryId = dict["categoryId"] as? String ?? ""

        if let array = dict["arrServices"] as? [[String:Any]]{
        for obj in array{
            let object = ModelServicesList.init(dict: obj)
            self.arrServices.append(object)
        }
        }
    }
}


 

class ModelServicesList: NSObject {
    var id = ""
    var serviceName = ""
    var price = "0.00"
    var bookingType = ""
    var describe = ""
    var bookingTypeOld = ""

    var time = ""
    var mainBookingId = ""
    var mainCategoryId = ""
    var incallPrice = "0.00"
    var outcallPrice = "0.00"
    init(dict: [String : Any]) {
        if let status = dict["_id"] as? String{
            id = status
        }else if let status = dict["_id"] as? Int{
            id = String(status)
        }
        if let status = dict["bookingTypeOld"] as? String{
            bookingTypeOld = status
        }else if let status = dict["bookingTypeOld"] as? Int{
            bookingTypeOld = String(status)
        }
        if let status = dict["incallPrice"] as? String{
            incallPrice = status
        }else if let status = dict["incallPrice"] as? Int{
            incallPrice = String(status)
        }
        
        if let status = dict["outcallPrice"] as? String{
            outcallPrice = status
        }else if let status = dict["outcallPrice"] as? Int{
            outcallPrice = String(status)
        }
        
        
        if let status = dict["mainBookingId"] as? String{
            mainBookingId = status
        }else if let status = dict["mainBookingId"] as? Int{
            mainBookingId = String(status)
        }
        if let status = dict["mainCategoryId"] as? String{
            mainCategoryId = status
        }else if let status = dict["mainCategoryId"] as? Int{
            mainCategoryId = String(status)
        }
        
        if let status = dict["price"] as? String{
            price = status
        }else if let status = dict["price"] as? Int{
            price = String(status)
        }
        
        if let status = dict["bookingType"] as? String{
            bookingType = status
        }else if let status = dict["bookingType"] as? Int{
            bookingType = String(status)
        }
        if let status = dict["describe"] as? String{
            self.describe = status
        }else if let status = dict["describe"] as? Int{
            self.describe = String(status)
        }
        
        if let status = dict["time"] as? String{
            self.time = status
        }else if let status = dict["time"] as? Int{
            self.time = String(status)
        }
 
        serviceName = dict["serviceName"] as? String ?? ""
    }
}


class ModelFinelSubCategory: NSObject {
    
    let bookingType = "bookingType"
    let bookingName = "bookingName"
    let categoryName = "categoryName"

    let serviceName = "serviceName"
    let descriptions = "descriptions"
    let incallPrice = "incallPrice"
    let outCallPrice = "outCallPrice"
    let completionTime = "completionTime"
    let bookingTypeOld = "bookingTypeOld"
    let strBusinessId = "strBusinessId"
    let strCategoryId = "strCategoryId"
    let strSubSubServiceId = "strSubSubServiceId"

    var param = ["bookingType":"",
                 "bookingTypeOld":"",
                 "strBusinessId":"",
                 "strCategoryId":"",
                 "bookingName":"",
                 "categoryName":"",
                 "serviceName":"",
                 "descriptions":"",
                 "incallPrice":"",
                 "outCallPrice":"",
                 "strSubSubServiceId":"",
                 "completionTime":""]
}

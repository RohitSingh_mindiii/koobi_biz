//
//  ModelAddBusinessType.swift
//  MualabBusiness
//
//  Created by Mac on 15/11/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

class ModelAddBusinessType: NSObject {
    public var v = ""
    public var id = ""
    public var artistId = ""
    public var bookingCount = ""
    public var deleteStatus = ""
    public var serviceId = ""
    public var serviceName = ""
    public var status = ""
    
    
    public var active_hash = ""
    public var created_date = ""
    public var role_id = ""
    public var title = ""
    public var type = ""
    public var updated_date = ""
    public var arrSubCategory = [SubCategoryModal]()
    

    var subServiceId = ""
    var subServiceName = ""
    
    public var strCreatUserStatus = false
    
    init(dict: [String : Any]) {
        strCreatUserStatus = false
        if let status = dict["__v"] as? String{
            v = status
        }else if let status = dict["__v"] as? Int{
            v = String(status)
        }
        
        if let status = dict["sub_service_id"] as? String{
            subServiceId = status
        }else if let status = dict["sub_service_id"] as? Int{
            subServiceId = String(status)
        }
        
        if let status = dict["sub_service_name"] as? String{
            subServiceName = status
        }else if let status = dict["sub_service_name"] as? Int{
            subServiceName = String(status)
        }
        
        //InUse
        if let status = dict["bt_id"] as? String{
            id = status
        }else if let status = dict["bt_id"] as? Int{
            id = String(status)
        }
        if let status = dict["artist_id"] as? String{
            artistId = status
        }else if let status = dict["artist_id"] as? Int{
            artistId = String(status)
        }

        if let status = dict["booking_count"] as? String{
            bookingCount = status
        }else if let status = dict["booking_count"] as? Int{
            bookingCount = String(status)
        }

        if let status = dict["delete_status"] as? String{
            deleteStatus = status
        }else if let status = dict["delete_status"] as? Int{
            deleteStatus = String(status)
        }

        if let status = dict["service_id"] as? String{
            serviceId = status
        }else if let status = dict["service_id"] as? Int{
            serviceId = String(status)
        }
        if let status = dict["status"] as? String{
            self.status = status
        }else if let status = dict["status"] as? Int{
            self.status = String(status)
        }
        
        if let status = dict["active_hash"] as? String{
            self.active_hash = status
        }else if let status = dict["active_hash"] as? Int{
            self.active_hash = String(status)
        }


        if let status = dict["created_date"] as? String{
            self.created_date = status
        }else if let status = dict["created_date"] as? Int{
            self.created_date = String(status)
        }
        
        if let status = dict["role_id"] as? String{
            self.role_id = status
        }else if let status = dict["role_id"] as? Int{
            self.role_id = String(status)
        }
        if let status = dict["type"] as? String{
            self.type = status
        }else if let status = dict["type"] as? Int{
            self.type = String(status)
        }
        if let status = dict["updated_date"] as? String{
            self.updated_date = status
        }else if let status = dict["updated_date"] as? Int{
            self.updated_date = String(status)
        }
 
      //InUse
        title = dict["business_type"] as? String ?? ""
        serviceName = dict["business_type"] as? String ?? ""

        /*
         if let arrOptions = dictQuestions["option"]as? [[String:Any]]{
             for dictOptions in arrOptions{
                 let objOptions = SurveyQueOptionsImages.init(dictQuestions: dictOptions)
                 self.arrOptionsModal.append(objOptions)
             }
         }
         */
        //get Sub Category
        if let arrSubCategorys = dict["category"]as? [[String:Any]]{
            for dict in arrSubCategorys{
                let objSubCategory = SubCategoryModal.init(dictSubCategory: dict)
                self.arrSubCategory.append(objSubCategory)
            }
        }
        
       // serviceName = dict["service_name"] as? String ?? ""
    }
}


class SubCategoryModal: NSObject{
    
       public var subCategoryID = ""
       public var subCategoryName = ""
       public var categoryStatus = ""
       public var  catDeleteStatus = ""
    
    
    init(dictSubCategory: [String : Any]) {
        
        
        if let categoryID = dictSubCategory["category_id"] as? String{
            self.subCategoryID = categoryID
        }else if let categoryID = dictSubCategory["category_id"] as? Int{
            self.subCategoryID = String(categoryID)
        }
        
        if let categoryName = dictSubCategory["category"] as? String{
            self.subCategoryName = categoryName
        }
        
        if let deleteStatus = dictSubCategory["delete_status"] as? String{
            self.catDeleteStatus = deleteStatus
        }else if let deleteStatus = dictSubCategory["delete_status"] as? Int{
            self.catDeleteStatus = String(deleteStatus)
        }
        
        if let status = dictSubCategory["status"] as? String{
            self.categoryStatus = status
        }else if let status = dictSubCategory["status"] as? Int{
            self.categoryStatus = String(status)
        }
        
    }
    
}

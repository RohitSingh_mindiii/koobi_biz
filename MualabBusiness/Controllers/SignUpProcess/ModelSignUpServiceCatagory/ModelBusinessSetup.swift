//
//  ModelBusinessSetup.swift
//  MualabBusiness
//
//  Created by Mac on 05/12/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

class ModelBusinessSetup: NSObject {
    var businessContactNo = ""
    var inCallpreprationTime = "00:00"
    var outCallpreprationTime = "00:00"
    var businessEmail = ""
    var businessName = ""
    var countryCode = ""
    var serviceType = ""
    var bookingSetting = ""
    var paymentType = "1"

    var location = ""
    var address2 = ""
    var country = ""
    var state = ""
    var city = ""
    var radius = "0"
    var address = ""
    var latitude = "00"
    var longitude = "00"
    var trackingLatitude = "00"
    var trackingLongitude = "00"
    var trackingAddress = "00"

    
    var incallTimeHours = "00"
    var incallTimeMinute = "00"
    var outcallTimeHours = "00"
    var outcallTimeMinute = "00"
    var businessHours = ""
    var buildinngNo = ""
    var postalCode = ""


}

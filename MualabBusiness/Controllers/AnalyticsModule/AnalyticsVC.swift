//
//  AnalyticsVC.swift
//  MualabBusiness
//
//  Created by Mindiii on 2/25/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import Charts
import Firebase

class AnalyticsVC: UIViewController,ChartViewDelegate {
    
    ////
    @IBOutlet weak var viewChatbadgeCount: UIView!
    @IBOutlet weak var lblChatbadgeCount: UILabel!
    ////
    
    @IBOutlet weak var ViewSaleSwipe: UIView!
    @IBOutlet weak var ViewBookingSwipe: UIView!
    @IBOutlet weak var lblNoChart: UILabel!
    
    @IBOutlet weak var viewPicker:UIView!
    @IBOutlet weak var DOBPicker : UIDatePicker!
    
    //Notification count
    @IBOutlet weak var viewNotificationCount: UIView!
    @IBOutlet weak var lblNotificationCount: UILabel!
    
    @IBOutlet weak var widthCompleted : NSLayoutConstraint!
    @IBOutlet weak var widthRemaining : NSLayoutConstraint!
    @IBOutlet weak var widthCancelled : NSLayoutConstraint!
    
    @IBOutlet weak var lblGraphCompleted : UILabel!
    @IBOutlet weak var lblGraphRemaining : UILabel!
    @IBOutlet weak var lblGraphCancelled : UILabel!
    var ref: DatabaseReference!
    @IBOutlet weak var PieChartView: PieChartView!
    
    @IBOutlet weak var PageSale: UIPageControl!
    @IBOutlet weak var PageBooking: UIPageControl!
    
    @IBOutlet weak var lblSalesType: UILabel!
    @IBOutlet weak var lblBookingType: UILabel!
    
    @IBOutlet weak var viewSaleDateOption: UIView!
    @IBOutlet weak var viewBookingDateOption: UIView!
    
    @IBOutlet weak var lblSaleTotalAmount: UILabel!
    
    @IBOutlet weak var imgSaleTotalProfitLossAmount: UIImageView!
    
    @IBOutlet weak var lblSaleTotalProfitLossAmount: UILabel!
    
    @IBOutlet weak var lblSaleCompairDateTimeShow: UILabel!
    @IBOutlet weak var lblBookingCompairDateTimeShow: UILabel!
    
    @IBOutlet weak var txtSaleFromDate: UITextField!
    @IBOutlet weak var txtSaleToDate: UITextField!
    @IBOutlet weak var txtBookingFromDate: UITextField!
    @IBOutlet weak var txtBookingToDate: UITextField!
    
    
    @IBOutlet weak var lblSaleOnlineTotalAmount: UILabel!
    @IBOutlet weak var lblSaleOfflineTotalAmount: UILabel!
    
    @IBOutlet weak var lblBookingCardTotalAmount: UILabel!
    @IBOutlet weak var lblBookingCashTotalAmount: UILabel!
    
    @IBOutlet weak var imgSaleOnlineTotalProfitLossAmount: UIImageView!
    @IBOutlet weak var imgSaleOfflineTotalProfitLossAmount: UIImageView!
    
    @IBOutlet weak var imgBookingCardTotalProfitLossAmount: UIImageView!
    @IBOutlet weak var imgBookingCashTotalProfitLossAmount: UIImageView!
    
    @IBOutlet weak var lblSaleOnlineTotalProfitLossAmount: UILabel!
    @IBOutlet weak var lblSaleOfflineTotalProfitLossAmount: UILabel!
    
    @IBOutlet weak var lblBookingCardTotalProfitLossAmount: UILabel!
    @IBOutlet weak var lblBookingCashTotalProfitLossAmount: UILabel!
    
    @IBOutlet weak var lblTotalBookingSale: UILabel!
    @IBOutlet weak var lblCancelledBookingSale: UILabel!
    
    @IBOutlet weak var imgTotalBookingSale: UIImageView!
    @IBOutlet weak var imgCancelledBookingSale: UIImageView!
    
    @IBOutlet weak var lblBookingTotalSaleProfitLossAmount: UILabel!
    @IBOutlet weak var lblCancelledBookingTotalSaleProfitLossAmount: UILabel!
    
    fileprivate var dateForField = 1
    
    
    fileprivate var arrTodayBooking = [ModelAnalityck]()
    fileprivate var arrTodaySale = [ModelAnalityck]()
    
    fileprivate var arrWeekBooking = [ModelAnalityck]()
    fileprivate var arrWeekSale = [ModelAnalityck]()
    
    fileprivate var arrMonthBooking = [ModelAnalityck]()
    fileprivate var arrMonthSale = [ModelAnalityck]()
    
    fileprivate var arrCustomeBooking = [ModelAnalityck]()
    fileprivate var arrCustomeSale = [ModelAnalityck]()
    
    fileprivate var arrTodayBookingPriviousData = [ModelAnalityck]()
    fileprivate var arrTodaySalePriviousData = [ModelAnalityck]()
    
    fileprivate var arrWeekBookingPriviousData = [ModelAnalityck]()
    fileprivate var arrWeekSalePriviousData = [ModelAnalityck]()
    
    fileprivate var arrMonthBookingPriviousData = [ModelAnalityck]()
    fileprivate var arrMonthSalePriviousData = [ModelAnalityck]()
    
    fileprivate var arrCustomeBookingPriviousData = [ModelAnalityck]()
    fileprivate var arrCustomeSalePriviousData = [ModelAnalityck]()
    
    var arrPageHold = [String]()
    
    override func viewDidLoad() {
        self.arrPageHold.removeAll()
        super.viewDidLoad()
        ref = Database.database().reference()
        self.badgeCount()
        self.viewNotificationCount.isHidden = true
        self.PieChartView.isUserInteractionEnabled = false
        self.lblNoChart.isHidden = true
        self.viewPicker.isHidden = true
        //      self.configureView(arrUnitsSold: [0.0,0.0])
        self.addGesturesToView()
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "refreshChatBadgeCount"), object: nil, queue: nil) { [weak self](Notification) in
            self?.refreshChatBadgeCount(Notification)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        objAppShareData.fromDetailPage = false
        
        self.arrPageHold.removeAll()
        
        
        self.arrTodaySale.removeAll()
        self.arrTodaySalePriviousData.removeAll()
        
        self.arrWeekSale.removeAll()
        self.arrWeekSalePriviousData.removeAll()
        
        self.arrMonthSale.removeAll()
        self.arrMonthSalePriviousData.removeAll()
        
        self.arrCustomeSale.removeAll()
        self.arrCustomeSalePriviousData.removeAll()
        
        self.arrTodayBooking.removeAll()
        self.arrTodayBookingPriviousData.removeAll()
        
        self.arrWeekBooking.removeAll()
        self.arrWeekBookingPriviousData.removeAll()
        
        self.arrMonthSale.removeAll()
        self.arrMonthBookingPriviousData.removeAll()
        
        self.arrCustomeBooking.removeAll()
        self.arrCustomeBookingPriviousData.removeAll()
        
        
        // self.PieChartView.delegate = self
        //  self.configureView(arrUnitsSold: [9.0,1.0])
        
        //  self.endDate = objAppShareData.dateFormatToShow1(forAPI: self.DOBPicker.date)
        
        let priviousDate  = Calendar.current.date(byAdding: .day, value: -7, to: Date())
        let startDate = objAppShareData.dateFormatToShow1(forAPI: priviousDate ?? Date())
        let endDate = objAppShareData.dateFormatToShow1(forAPI: Date())
        
        let param = ["startDate":endDate,  "endDate":endDate, "previewsStartDate":startDate,  "previewsEndDate":startDate]
        self.callWebserviceForGet_AnalityckData(dict: param, dataFor: "ALL")
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshChatBadgeCount"), object: nil)
    }
}


//MARK: - Up down Swipe
fileprivate extension AnalyticsVC{
    func addGesturesToView() -> Void {
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.leftToRightSaleSwipe))
        swipeRight.direction = .right
        self.ViewSaleSwipe.addGestureRecognizer(swipeRight)
        
        let swipeRightNew = UISwipeGestureRecognizer(target: self, action: #selector(self.leftToRightBookingSwipe))
        swipeRightNew.direction = .right
        self.ViewBookingSwipe.addGestureRecognizer(swipeRightNew)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.rightToLeftSaleSwipe))
        swipeLeft.direction = .left
        self.ViewSaleSwipe.addGestureRecognizer(swipeLeft)
        
        let swipeLeftNew = UISwipeGestureRecognizer(target: self, action: #selector(self.rightToLeftBookingSwipe))
        swipeLeftNew.direction = .left
        self.ViewBookingSwipe.addGestureRecognizer(swipeLeftNew)
    }
    
    @objc func rightToLeftSaleSwipe() ->Void {
        
        if PageSale.currentPage == 0{
            
            var mondaysDate: Date {
                return Calendar(identifier: .iso8601).date(from: Calendar(identifier: .iso8601).dateComponents([.yearForWeekOfYear, .weekOfYear], from: Date()))!
            }
            let startDate = objAppShareData.dateFormatToShow1(forAPI: mondaysDate)
            let endDate = objAppShareData.dateFormatToShow1(forAPI:Date())
            let priviousEndDate  = Calendar.current.date(byAdding: .day, value: -1, to: mondaysDate )
            let priviousStartDate  = Calendar.current.date(byAdding: .day, value: -6, to: priviousEndDate ?? Date())
            let preStartDate = objAppShareData.dateFormatToShow1(forAPI: priviousStartDate ?? Date())
            let preEndDate = objAppShareData.dateFormatToShow1(forAPI: priviousEndDate ?? Date())
            let param = ["startDate":startDate, "endDate":endDate, "previewsStartDate":preStartDate,  "previewsEndDate":preEndDate]
            
            var callAPI = true
            for obj in self.arrPageHold{  if "SW" == obj{  callAPI = false   }  }
            if callAPI == true{  self.callWebserviceForGet_AnalityckData(dict: param, dataFor: "SW")
            }else{
                self.setSaleGraphToday(dataFor: "SW", arrCurrent: self.arrWeekSale, arrPrevious:  self.arrWeekSalePriviousData)
            }
            
        }else   if PageSale.currentPage == 1{
            
            let startOfMonthValue = startOfMonth(startDate: Date())
            let endDate = objAppShareData.dateFormatToShow1(forAPI:Date())
            let startDate = objAppShareData.dateFormatToShow1(forAPI:startOfMonthValue)
            let priviousEndDate  = Calendar.current.date(byAdding: .day, value: -1, to: startOfMonthValue )
            let priviousStartDate  = startOfMonth(startDate: priviousEndDate ?? Date())
            let preStartDate = objAppShareData.dateFormatToShow1(forAPI: priviousStartDate )
            let preEndDate = objAppShareData.dateFormatToShow1(forAPI: priviousEndDate ?? Date())
            let param = ["startDate":startDate, "endDate":endDate,  "previewsStartDate":preStartDate,  "previewsEndDate":preEndDate]
            //   self.callWebserviceForGet_AnalityckData(dict: param, dataFor: "SM")
            
            var callAPI = true
            for obj in self.arrPageHold{  if "SM" == obj{  callAPI = false   }  }
            if callAPI == true{   self.callWebserviceForGet_AnalityckData(dict: param, dataFor: "SM")
            }else{
                self.setSaleGraphToday(dataFor: "SM", arrCurrent: self.arrMonthSale, arrPrevious:  self.arrMonthSalePriviousData)
            }
            
        }else   if PageSale.currentPage == 2{
            
            if self.txtSaleFromDate.text == "From" || self.txtSaleFromDate.text == "" || self.txtSaleFromDate.text == nil{
                self.removeDataFromArray(dataFor: "SD")
                self.setDataOnPageNo(dataFor: "SD")
            }else{
                let fromDate  = objAppShareData.setDateFromString1(time:self.txtSaleFromDate.text!)
                let toDate = objAppShareData.setDateFromString1(time:self.txtSaleToDate.text!)
                let startDate = objAppShareData.dateFormatToShow1(forAPI: fromDate)
                let endDate = objAppShareData.dateFormatToShow1(forAPI:toDate)
                let day = checkDifferenceToDate(strDate: fromDate, endDate: toDate)+1
                let priviousStartDate  = Calendar.current.date(byAdding: .day, value: -day, to: fromDate)
                let priviousEndDate  = Calendar.current.date(byAdding: .day, value: -1, to: fromDate )
                let preStartDate = objAppShareData.dateFormatToShow1(forAPI: priviousStartDate ?? Date())
                let preEndDate = objAppShareData.dateFormatToShow1(forAPI: priviousEndDate ?? Date())
                let param = ["startDate":startDate, "endDate":endDate,  "previewsStartDate":preStartDate,   "previewsEndDate":preEndDate]
                // self.callWebserviceForGet_AnalityckData(dict: param, dataFor: "SD")
                
                var callAPI = true
                for obj in self.arrPageHold{  if "SD" == obj{  callAPI = false   }  }
                if callAPI == true{   self.callWebserviceForGet_AnalityckData(dict: param, dataFor: "SD")
                }else{
                    self.setSaleGraphToday(dataFor: "SD", arrCurrent: self.arrCustomeSale, arrPrevious:  self.arrCustomeSalePriviousData)
                }
            }
        }
    }
    
    
    func checkDifferenceToDate (strDate:Date,endDate:Date) -> Int{
        var timeDays = 0
        var secondsBetween1: TimeInterval? = nil
        secondsBetween1 = endDate.timeIntervalSince(strDate)
        let secondsBetween = Int(secondsBetween1!)
        let numberOfDays: Int = secondsBetween / 86400
        if numberOfDays > 0 {
            timeDays = numberOfDays
        }
        return timeDays
    }
    
    
    
    @objc func rightToLeftBookingSwipe() ->Void {
        if self.PageBooking.currentPage == 0{
            
            var mondaysDate: Date {
                return Calendar(identifier: .iso8601).date(from: Calendar(identifier: .iso8601).dateComponents([.yearForWeekOfYear, .weekOfYear], from: Date()))!
            }
            let startDate = objAppShareData.dateFormatToShow1(forAPI: mondaysDate)
            let endDate = objAppShareData.dateFormatToShow1(forAPI:Date())
            let priviousEndDate  = Calendar.current.date(byAdding: .day, value: -1, to: mondaysDate )
            let priviousStartDate  = Calendar.current.date(byAdding: .day, value: -6, to: priviousEndDate ?? Date())
            let preStartDate = objAppShareData.dateFormatToShow1(forAPI: priviousStartDate ?? Date())
            let preEndDate = objAppShareData.dateFormatToShow1(forAPI: priviousEndDate ?? Date())
            let param = ["startDate":startDate, "endDate":endDate,  "previewsStartDate":preStartDate,  "previewsEndDate":preEndDate]
            // self.callWebserviceForGet_AnalityckData(dict: param, dataFor: "BW")
            
            var callAPI = true
            for obj in self.arrPageHold{  if "BW" == obj{  callAPI = false   }  }
            if callAPI == true{   self.callWebserviceForGet_AnalityckData(dict: param, dataFor: "BW")
            }else{
                self.setBookingGraphToday(dataFor: "BW", arrCurrent: self.arrWeekBooking, arrPrevious:  self.arrWeekBookingPriviousData)
            }
            
        }else   if PageBooking.currentPage == 1{
            
            let startOfMonthValue = startOfMonth(startDate: Date())
            let endDate = objAppShareData.dateFormatToShow1(forAPI:Date())
            let startDate = objAppShareData.dateFormatToShow1(forAPI:startOfMonthValue)
            let priviousEndDate  = Calendar.current.date(byAdding: .day, value: -1, to: startOfMonthValue )
            let priviousStartDate  = startOfMonth(startDate: priviousEndDate ?? Date())
            let preStartDate = objAppShareData.dateFormatToShow1(forAPI: priviousStartDate )
            let preEndDate = objAppShareData.dateFormatToShow1(forAPI: priviousEndDate ?? Date())
            let param = ["startDate":startDate,  "endDate":endDate, "previewsStartDate":preStartDate,  "previewsEndDate":preEndDate]
            // self.callWebserviceForGet_AnalityckData(dict: param, dataFor: "BM")
            
            var callAPI = true
            for obj in self.arrPageHold{  if "BM" == obj{  callAPI = false   }  }
            if callAPI == true{   self.callWebserviceForGet_AnalityckData(dict: param, dataFor: "BM")
            }else{
                self.setBookingGraphToday(dataFor: "BM", arrCurrent: self.arrMonthBooking, arrPrevious:  self.arrMonthBookingPriviousData)
            }
            
        }else   if PageBooking.currentPage == 2{
            
            if self.txtBookingFromDate.text == "From" || self.txtBookingFromDate.text == "" || self.txtBookingFromDate.text == nil{
                self.removeDataFromArray(dataFor: "BD")
                self.setDataOnPageNo(dataFor: "BD")
            }else{
                let fromDate  = objAppShareData.setDateFromString1(time:self.txtBookingFromDate.text!)
                let toDate = objAppShareData.setDateFromString1(time:self.txtBookingToDate.text!)
                let startDate = objAppShareData.dateFormatToShow1(forAPI: fromDate)
                let endDate = objAppShareData.dateFormatToShow1(forAPI:toDate)
                let day = checkDifferenceToDate(strDate: fromDate, endDate: toDate)+1
                let priviousStartDate  = Calendar.current.date(byAdding: .day, value: -day, to: fromDate)
                let priviousEndDate  = Calendar.current.date(byAdding: .day, value: -1, to: fromDate )
                let preStartDate = objAppShareData.dateFormatToShow1(forAPI: priviousStartDate ?? Date())
                let preEndDate = objAppShareData.dateFormatToShow1(forAPI: priviousEndDate ?? Date())
                let param = ["startDate":startDate,  "endDate":endDate,  "previewsStartDate":preStartDate, "previewsEndDate":preEndDate]
                //   self.callWebserviceForGet_AnalityckData(dict: param, dataFor: "BD")
                
                var callAPI = true
                for obj in self.arrPageHold{  if "BD" == obj{  callAPI = false   }  }
                if callAPI == true{   self.callWebserviceForGet_AnalityckData(dict: param, dataFor: "BD")
                }else{
                    self.setBookingGraphToday(dataFor: "BD", arrCurrent: self.arrCustomeBooking, arrPrevious:  self.arrCustomeBookingPriviousData)
                }
                
            }
        }
    }
    
    
    @objc func leftToRightSaleSwipe() ->Void {
        self.view.endEditing(true)
        if self.PageSale.currentPage == 3{
            
            let startOfMonthValue = startOfMonth(startDate: Date())
            let endDate = objAppShareData.dateFormatToShow1(forAPI:Date())
            let startDate = objAppShareData.dateFormatToShow1(forAPI:startOfMonthValue)
            let priviousEndDate  = Calendar.current.date(byAdding: .day, value: -1, to: startOfMonthValue )
            let priviousStartDate  = startOfMonth(startDate: priviousEndDate ?? Date())
            let preStartDate = objAppShareData.dateFormatToShow1(forAPI: priviousStartDate )
            let preEndDate = objAppShareData.dateFormatToShow1(forAPI: priviousEndDate ?? Date())
            let param = ["startDate":startDate,  "endDate":endDate,  "previewsStartDate":preStartDate,  "previewsEndDate":preEndDate]
            // self.callWebserviceForGet_AnalityckData(dict: param, dataFor: "SM")
            self.setSaleGraphToday(dataFor: "SM", arrCurrent: self.arrMonthSale, arrPrevious:  self.arrMonthSalePriviousData)
            
        }else if self.PageSale.currentPage == 2{
            
            var mondaysDate: Date {  return Calendar(identifier: .iso8601).date(from: Calendar(identifier: .iso8601).dateComponents([.yearForWeekOfYear, .weekOfYear], from: Date()))!  }
            let startDate = objAppShareData.dateFormatToShow1(forAPI: mondaysDate)
            let endDate = objAppShareData.dateFormatToShow1(forAPI:Date())
            let priviousEndDate  = Calendar.current.date(byAdding: .day, value: -1, to: mondaysDate )
            let priviousStartDate  = Calendar.current.date(byAdding: .day, value: -6, to: priviousEndDate ?? Date())
            let preStartDate = objAppShareData.dateFormatToShow1(forAPI: priviousStartDate ?? Date())
            let preEndDate = objAppShareData.dateFormatToShow1(forAPI: priviousEndDate ?? Date())
            let param = ["startDate":startDate,  "endDate":endDate,  "previewsStartDate":preStartDate,  "previewsEndDate":preEndDate]
            // self.callWebserviceForGet_AnalityckData(dict: param, dataFor: "SW")
            self.setSaleGraphToday(dataFor: "SW", arrCurrent: self.arrWeekSale, arrPrevious:  self.arrWeekSalePriviousData)
            
        }else if self.PageSale.currentPage == 1{
            
            let priviousDate  = Calendar.current.date(byAdding: .day, value: -7, to: Date())
            let startDate = objAppShareData.dateFormatToShow1(forAPI: priviousDate ?? Date())
            let endDate = objAppShareData.dateFormatToShow1(forAPI: Date())
            let param = ["startDate":endDate, "endDate":endDate, "previewsStartDate":startDate, "previewsEndDate":startDate]
            //self.callWebserviceForGet_AnalityckData(dict: param, dataFor: "ST")
            self.setSaleGraphToday(dataFor: "ST", arrCurrent: self.arrTodaySale, arrPrevious:  self.arrTodaySalePriviousData)
            
        }
    }
    
    
    @objc func leftToRightBookingSwipe() ->Void {
        if self.PageBooking.currentPage == 3{
            
            let startOfMonthValue = startOfMonth(startDate: Date())
            let endDate = objAppShareData.dateFormatToShow1(forAPI:Date())
            let startDate = objAppShareData.dateFormatToShow1(forAPI:startOfMonthValue)
            let priviousEndDate  = Calendar.current.date(byAdding: .day, value: -1, to: startOfMonthValue )
            let priviousStartDate  = startOfMonth(startDate: priviousEndDate ?? Date())
            let preStartDate = objAppShareData.dateFormatToShow1(forAPI: priviousStartDate )
            let preEndDate = objAppShareData.dateFormatToShow1(forAPI: priviousEndDate ?? Date())
            let param = ["startDate":startDate,  "endDate":endDate, "previewsStartDate":preStartDate,  "previewsEndDate":preEndDate]
            // self.callWebserviceForGet_AnalityckData(dict: param, dataFor: "BM")
            self.setBookingGraphToday(dataFor: "BM", arrCurrent: self.arrMonthBooking, arrPrevious:  self.arrMonthBookingPriviousData)
            
        }else if self.PageBooking.currentPage == 2{
            
            var mondaysDate: Date {
                return Calendar(identifier: .iso8601).date(from: Calendar(identifier: .iso8601).dateComponents([.yearForWeekOfYear, .weekOfYear], from: Date()))!
            }
            let startDate = objAppShareData.dateFormatToShow1(forAPI: mondaysDate)
            let endDate = objAppShareData.dateFormatToShow1(forAPI:Date())
            let priviousEndDate  = Calendar.current.date(byAdding: .day, value: -1, to: mondaysDate )
            let priviousStartDate  = Calendar.current.date(byAdding: .day, value: -6, to: priviousEndDate ?? Date())
            let preStartDate = objAppShareData.dateFormatToShow1(forAPI: priviousStartDate ?? Date())
            let preEndDate = objAppShareData.dateFormatToShow1(forAPI: priviousEndDate ?? Date())
            let param = ["startDate":startDate, "endDate":endDate,  "previewsStartDate":preStartDate,  "previewsEndDate":preEndDate]
            //self.callWebserviceForGet_AnalityckData(dict: param, dataFor: "BW")
            self.setBookingGraphToday(dataFor: "BW", arrCurrent: self.arrWeekBooking, arrPrevious:  self.arrWeekBookingPriviousData)
            
        }else if self.PageBooking.currentPage == 1{
            
            let priviousDate  = Calendar.current.date(byAdding: .day, value: -7, to: Date())
            let startDate = objAppShareData.dateFormatToShow1(forAPI: priviousDate ?? Date())
            let endDate = objAppShareData.dateFormatToShow1(forAPI: Date())
            let param = ["startDate":endDate, "endDate":endDate, "previewsStartDate":startDate, "previewsEndDate":startDate]
            //self.callWebserviceForGet_AnalityckData(dict: param, dataFor: "BT")
            self.setBookingGraphToday(dataFor: "BT", arrCurrent: self.arrTodayBooking, arrPrevious:  self.arrTodayBookingPriviousData)
        }
    }
}


//MARK: - custome method extension
extension AnalyticsVC{
    
    func configureView(arrUnitsSold:[Double]){
        self.viewPicker.isHidden = true
        let money = arrUnitsSold
        //  let track = ["% Online", "% Offline"]
        let track = ["", ""]
        
        var entries = [PieChartDataEntry]()
        
        //   let dataEntry1 = PieChartDataEntry(value: Double(i), label: dataPoints[i], data:  dataPoints[i] as AnyObject)
        
        for (index, value) in money.enumerated() {
            let entry = PieChartDataEntry()
            entry.y = value
            entry.label = track[index]
            entries.append( entry)
        }
        
        // 3. chart setup
        let set = PieChartDataSet( values: entries, label: "Pie Chart")
        // this is custom extension method. Download the code for more details.
        let colors  = [appColor,#colorLiteral(red: 0.5019607843, green: 0.6666666667, blue: 1, alpha: 1)]
        PieChartView.tintColor = UIColor.black
        set.colors = colors
        PieChartView.noDataText = "No data available"
        PieChartView.entryLabelColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        // user interaction
        PieChartView.isUserInteractionEnabled = false
        PieChartView.holeRadiusPercent = 0
        PieChartView.legend.textColor = UIColor.black
        
        PieChartView.transparentCircleColor = UIColor.clear
        let data = PieChartData(dataSet: set)
        data.setDrawValues(false)
        // data.setValueTextColor(UIColor.black)
        PieChartView.data = data
        
    }
    
    
    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {
        if let dataSet = chartView.data?.dataSets[ highlight.dataSetIndex] {
            let sliceIndex: Int = dataSet.entryIndex( entry: entry)
            print( "Selected slice index: \( sliceIndex)")
        }
    }
    
    func setDataOnPageNo(dataFor:String){
        
        
        if dataFor == "ALL"{
            self.setSaleGraphToday(dataFor:dataFor, arrCurrent: self.arrTodaySale, arrPrevious:  self.arrTodaySalePriviousData)
            self.setBookingGraphToday(dataFor:dataFor, arrCurrent: self.arrTodayBooking, arrPrevious: self.arrTodayBookingPriviousData)
        }else if dataFor == "ST"{
            self.setSaleGraphToday(dataFor:dataFor, arrCurrent: self.arrTodaySale, arrPrevious:  self.arrTodaySalePriviousData)
        }else if dataFor == "SW"  {
            self.setSaleGraphToday(dataFor:dataFor, arrCurrent: self.arrWeekSale, arrPrevious:  self.arrWeekSalePriviousData)
        }else if  dataFor == "SM" {
            self.setSaleGraphToday(dataFor:dataFor, arrCurrent: self.arrMonthSale, arrPrevious:  self.arrMonthSalePriviousData)
        }else if  dataFor == "SD"  {
            self.setSaleGraphToday(dataFor:dataFor, arrCurrent: self.arrCustomeSale, arrPrevious:  self.arrCustomeSalePriviousData)
        }else if  dataFor == "BT"  {
            self.setBookingGraphToday(dataFor:dataFor, arrCurrent: self.arrTodayBooking, arrPrevious: self.arrTodayBookingPriviousData)
        }else if   dataFor == "BW" {
            self.setBookingGraphToday(dataFor:dataFor, arrCurrent: self.arrWeekBooking, arrPrevious: self.arrWeekBookingPriviousData)
        }else if  dataFor == "BM"  {
            self.setBookingGraphToday(dataFor:dataFor, arrCurrent: self.arrMonthBooking, arrPrevious: self.arrMonthBookingPriviousData)
        }else if  dataFor == "BD" {
            self.setBookingGraphToday(dataFor:dataFor, arrCurrent: self.arrCustomeBooking, arrPrevious: self.arrCustomeBookingPriviousData)
        }
    }
}


//MARK: Set Sale For Today
extension AnalyticsVC{
    
    func setSaleGraphToday(dataFor:String,arrCurrent:[ModelAnalityck], arrPrevious:[ModelAnalityck]){
        
        self.viewSaleDateOption.isHidden = true
        self.PageSale.currentPage = 0
        
        let totalBooking = arrCurrent.count
        var completedBooking = 0
        var remainingBooking = 0
        var cancelledBooking = 0
        var totalPrice = 0.0
        var onlineTotalPrice = 0.0
        var offlineTotalPrice = 0.0
        
        
        for obj in arrCurrent{
            
            if obj.bookStatus == 3{
                completedBooking =  completedBooking+1
                totalPrice = obj.totalPrice+totalPrice
                //                if obj.customerType == "walking"{
                //                    offlineTotalPrice = obj.totalPrice+offlineTotalPrice
                //                }else if obj.customerType == "online"{
                //                    onlineTotalPrice = obj.totalPrice+onlineTotalPrice
                //                }
                
                if obj.paymentType == 1{
                    onlineTotalPrice = obj.totalPrice+onlineTotalPrice
                }else {
                    offlineTotalPrice = obj.totalPrice+offlineTotalPrice
                }
                
            }else if obj.bookStatus == 2{
                cancelledBooking =  cancelledBooking+1
            }else{
                remainingBooking =  remainingBooking+1
            }
        }
        
        let totalBookingPrivious = arrPrevious.count
        var completedBookingPrivious = 0
        var remainingBookingPrivious = 0
        var cancelledBookingPrivious = 0
        var totalPricePrivious = 0.0
        var onlineTotalPricePrivious = 0.0
        var offlineTotalPricePrivious = 0.0
        
        for obj in arrPrevious{
            if obj.bookStatus == 3{
                completedBookingPrivious =  completedBookingPrivious+1
                
                totalPricePrivious = obj.totalPrice+totalPricePrivious
                
                //                if obj.customerType == "walking"{
                //                    offlineTotalPricePrivious = obj.totalPrice+offlineTotalPricePrivious
                //                }else{
                //                    onlineTotalPricePrivious = obj.totalPrice+onlineTotalPricePrivious
                //                }
                
                if obj.paymentType == 1{
                    onlineTotalPricePrivious = obj.totalPrice+onlineTotalPricePrivious
                }else{
                    offlineTotalPricePrivious = obj.totalPrice+offlineTotalPricePrivious
                }
                
            }else if obj.bookStatus == 2{
                cancelledBookingPrivious =  cancelledBookingPrivious+1
            }else{
                remainingBookingPrivious =  remainingBookingPrivious+1
            }
        }
        
        if arrCurrent.count == 0 && arrPrevious.count == 0{
            self.lblNoChart.isHidden = false
        }else{
            self.lblNoChart.isHidden = true
        }
        
        if  totalBooking > 0{
            let onineIncume =  totalPrice/100
            let offineIncume = totalPrice/100
            let online = onlineTotalPrice/onineIncume
            let offline = offlineTotalPrice/offineIncume
            if onineIncume == 0 && offineIncume == 0{
                self.lblNoChart.isHidden = false
            }
            self.configureView(arrUnitsSold: [Double(online),Double(offline)])
        }else{
            self.configureView(arrUnitsSold: [0.0,0.0])
        }
        
        
        
        self.lblSaleTotalAmount.text = "£"+String(format: "%.2f", totalPrice)
        self.lblSaleOnlineTotalAmount.text = "£"+String(format: "%.2f", onlineTotalPrice)
        self.lblSaleOfflineTotalAmount.text = "£"+String(format: "%.2f", offlineTotalPrice)
        
        if dataFor == "ST" || dataFor == "ALL"{
            self.PageSale.currentPage = 0
            self.lblSalesType.text = "Today's Sales"
            self.lblSaleCompairDateTimeShow.text = "(Comparison to last "+loadDayName(forDate: Date())+")"
        }else if dataFor == "SW"{
            self.PageSale.currentPage = 1
            self.lblSalesType.text = "Week Sales"
            self.lblSaleCompairDateTimeShow.text = "(Comparison to last week)"
        }else if dataFor == "SM"{
            self.PageSale.currentPage = 2
            self.lblSalesType.text = "Month Sales"
            self.lblSaleCompairDateTimeShow.text = "(Comparison to last month)"
        }else if dataFor == "SD"{
            self.PageSale.currentPage = 3
            self.viewSaleDateOption.isHidden = false
            self.lblSalesType.text = "Sales"
            let a = "(Comparison from "
            let b = self.txtSaleFromDate.text ?? ""
            let c = " To "
            let d = self.txtSaleToDate.text ?? ""
            let e = ")"
            if b == ""{
                self.lblSaleCompairDateTimeShow.text  = ""
            }else{
                self.lblSaleCompairDateTimeShow.text  =  a+b+c+d+e
            }
        }
        
        if totalPrice > totalPricePrivious{
            self.lblSaleTotalProfitLossAmount.text = "+"+String(format: "%.2f", totalPrice-totalPricePrivious)
            self.imgSaleTotalProfitLossAmount.image = #imageLiteral(resourceName: "green_arrow_ico")
            self.lblSaleTotalProfitLossAmount.textColor = #colorLiteral(red: 0, green: 0.7294117647, blue: 0.2705882353, alpha: 1)
        } else  if  totalPrice < totalPricePrivious {
            self.lblSaleTotalProfitLossAmount.text = "-"+String(format: "%.2f",totalPricePrivious-totalPrice)
            self.imgSaleTotalProfitLossAmount.image = #imageLiteral(resourceName: "red_arrow_ico")
            self.lblSaleTotalProfitLossAmount.textColor = #colorLiteral(red: 1, green: 0, blue: 0.1921568627, alpha: 1)
        } else {
            self.lblSaleTotalProfitLossAmount.text = String(format: "%.2f",totalPrice-totalPricePrivious)
            self.imgSaleTotalProfitLossAmount.image = #imageLiteral(resourceName: "green_arrow_ico")
            self.lblSaleTotalProfitLossAmount.textColor = #colorLiteral(red: 0, green: 0.7294117647, blue: 0.2705882353, alpha: 1)
        }
        
        if onlineTotalPrice > onlineTotalPricePrivious{
            self.lblSaleOnlineTotalProfitLossAmount.text = "+"+String(format: "%.2f",onlineTotalPrice-onlineTotalPricePrivious)
            self.imgSaleOnlineTotalProfitLossAmount.image = #imageLiteral(resourceName: "green_arrow_ico")
            self.lblSaleOnlineTotalProfitLossAmount.textColor = #colorLiteral(red: 0, green: 0.7294117647, blue: 0.2705882353, alpha: 1)
        } else if onlineTotalPrice < onlineTotalPricePrivious {
            self.lblSaleOnlineTotalProfitLossAmount.text = "-"+String(format: "%.2f",onlineTotalPricePrivious-onlineTotalPrice)
            self.imgSaleOnlineTotalProfitLossAmount.image = #imageLiteral(resourceName: "red_arrow_ico")
            self.lblSaleOnlineTotalProfitLossAmount.textColor = #colorLiteral(red: 1, green: 0, blue: 0.168627451, alpha: 1)
        }else {
            self.lblSaleOnlineTotalProfitLossAmount.text = String(format: "%.2f",onlineTotalPrice-onlineTotalPricePrivious)
            self.imgSaleOnlineTotalProfitLossAmount.image = #imageLiteral(resourceName: "green_arrow_ico")
            self.lblSaleOnlineTotalProfitLossAmount.textColor = #colorLiteral(red: 0, green: 0.7294117647, blue: 0.2705882353, alpha: 1)
        }
        
        if offlineTotalPrice > offlineTotalPricePrivious{
            self.lblSaleOfflineTotalProfitLossAmount.text = "+"+String(format: "%.2f",offlineTotalPrice-offlineTotalPricePrivious)
            self.imgSaleOfflineTotalProfitLossAmount.image = #imageLiteral(resourceName: "green_arrow_ico")
            self.lblSaleOfflineTotalProfitLossAmount.textColor = #colorLiteral(red: 0, green: 0.7294117647, blue: 0.2705882353, alpha: 1)
        } else if offlineTotalPrice < offlineTotalPricePrivious {
            self.lblSaleOfflineTotalProfitLossAmount.text = "-"+String(format: "%.2f",offlineTotalPricePrivious-offlineTotalPrice)
            self.imgSaleOfflineTotalProfitLossAmount.image = #imageLiteral(resourceName: "red_arrow_ico")
            self.lblSaleOfflineTotalProfitLossAmount.textColor = #colorLiteral(red: 1, green: 0, blue: 0.137254902, alpha: 1)
        } else {
            self.lblSaleOfflineTotalProfitLossAmount.text = String(format: "%.2f",offlineTotalPrice-offlineTotalPricePrivious)
            self.imgSaleOfflineTotalProfitLossAmount.image = #imageLiteral(resourceName: "green_arrow_ico")
            self.lblSaleOfflineTotalProfitLossAmount.textColor = #colorLiteral(red: 0, green: 0.7294117647, blue: 0.2705882353, alpha: 1)
        }
        
    }
    
}

//MARK: Set Booking For Today
extension AnalyticsVC{
    
    func setBookingGraphToday(dataFor:String,arrCurrent:[ModelAnalityck], arrPrevious:[ModelAnalityck]){
        
        let totalBooking = arrCurrent.count
        var completedBooking = 0
        var remainingBooking = 0
        var cancelledBooking = 0
        var totalPrice = 0.0
        var onlineTotalPrice = 0
        var offlineTotalPrice = 0
        
        for obj in arrCurrent{
            if obj.bookStatus == 3{
                completedBooking =  completedBooking+1
                
                totalPrice = obj.totalPrice+totalPrice
                
            }else if obj.bookStatus == 2{
                cancelledBooking =  cancelledBooking+1
            }else{
                remainingBooking =  remainingBooking+1
            }
            
            if obj.customerType == "walking"{
                offlineTotalPrice = 1+offlineTotalPrice
            }else{
                onlineTotalPrice = 1+onlineTotalPrice
            }
        }
        
        let totalBookingPrivious = arrPrevious.count
        var completedBookingPrivious = 0
        var remainingBookingPrivious = 0
        var cancelledBookingPrivious = 0
        var totalPricePrivious = 0.0
        var onlineTotalPricePrivious = 0
        var offlineTotalPricePrivious = 0
        
        for obj in arrPrevious{
            if obj.bookStatus == 3{
                completedBookingPrivious =  completedBookingPrivious+1
                
                totalPricePrivious = obj.totalPrice+totalPricePrivious
                
                
            }else if obj.bookStatus == 2{
                cancelledBookingPrivious =  cancelledBookingPrivious+1
            }else{  remainingBookingPrivious =  remainingBookingPrivious+1
                
            }
            if obj.customerType == "walking"{
                offlineTotalPricePrivious = 1+offlineTotalPricePrivious
            }else{
                onlineTotalPricePrivious = 1+onlineTotalPricePrivious
            }
        }
        
        if totalBooking == 0{
            self.widthCompleted.constant = CGFloat(0)
            self.widthCancelled.constant = CGFloat(0)
            self.widthRemaining.constant = CGFloat(0)
        }else{
            let totalWidth = Int(self.view.frame.width-80)//120
            let widthRatio = totalWidth/totalBooking
            self.widthCompleted.constant = CGFloat(widthRatio*completedBooking)
            self.widthRemaining.constant = CGFloat(widthRatio*remainingBooking)
            self.widthCancelled.constant = CGFloat(widthRatio*cancelledBooking)
        }
        
        self.lblGraphCompleted.text = String(completedBooking)
        self.lblGraphRemaining.text = String(remainingBooking)
        self.lblGraphCancelled.text = String(cancelledBooking)
        
        
        
        
        self.lblTotalBookingSale.text = String(arrCurrent.count)
        self.lblCancelledBookingSale.text = String(cancelledBooking)
        
        self.viewBookingDateOption.isHidden = true
        
        if dataFor == "BT"  || dataFor == "ALL"{
            
            self.PageBooking.currentPage = 0
            self.lblBookingType.text = "Today's Bookings"
            self.lblBookingCompairDateTimeShow.text = "(Comparison to last "+loadDayName(forDate: Date())+")"
            self.lblBookingCompairDateTimeShow.textAlignment = .center
            
        }else if dataFor == "BW"{
            
            self.PageBooking.currentPage = 1
            self.lblBookingType.text = "Week Bookings"
            self.lblBookingCompairDateTimeShow.text = "(Comparison to last week)"
            self.lblBookingCompairDateTimeShow.textAlignment = .center
            
        }else if dataFor == "BM"{
            
            self.PageBooking.currentPage = 2
            self.lblBookingType.text = "Month Bookings"
            self.lblBookingCompairDateTimeShow.text = "(Comparison to last month)"
            self.lblBookingCompairDateTimeShow.textAlignment = .center
            
        }else if dataFor == "BD"{
            
            self.PageBooking.currentPage = 3
            self.lblBookingType.text = "Bookings"
            self.viewBookingDateOption.isHidden = false
            self.lblSalesType.text = "Sales"
            
            let a = "(Comparison from "
            let b = self.txtBookingFromDate.text ?? ""
            let c = " To "
            let d = self.txtBookingToDate.text ?? ""
            let e = ")"
            
            if b == ""{
                self.lblBookingCompairDateTimeShow.text  = ""
            }else{
                self.lblBookingCompairDateTimeShow.text  =  a+b+c+d+e
            }
            
            self.lblBookingCompairDateTimeShow.textAlignment = .center
            
        }
        
        if totalBooking > totalBookingPrivious {
            self.lblBookingTotalSaleProfitLossAmount.text = "+"+String(totalBooking-totalBookingPrivious)
            self.imgTotalBookingSale.image = #imageLiteral(resourceName: "green_arrow_ico")
            self.lblBookingTotalSaleProfitLossAmount.textColor = #colorLiteral(red: 0, green: 0.7294117647, blue: 0.2705882353, alpha: 1)
        } else if  totalBooking < totalBookingPrivious  {
            self.lblBookingTotalSaleProfitLossAmount.text = "-"+String(totalBookingPrivious-totalBooking)
            self.imgTotalBookingSale.image = #imageLiteral(resourceName: "red_arrow_ico")
            self.lblBookingTotalSaleProfitLossAmount.textColor = #colorLiteral(red: 0.9803921569, green: 0, blue: 0.09411764706, alpha: 1)
        } else {
            self.lblBookingTotalSaleProfitLossAmount.text = String(totalBooking-totalBookingPrivious)
            self.imgTotalBookingSale.image = #imageLiteral(resourceName: "green_arrow_ico")
            self.lblBookingTotalSaleProfitLossAmount.textColor = #colorLiteral(red: 0, green: 0.7294117647, blue: 0.2705882353, alpha: 1)
        }
        
        if cancelledBooking > cancelledBookingPrivious{
            self.lblCancelledBookingTotalSaleProfitLossAmount.text = "+"+String(cancelledBooking-cancelledBookingPrivious)
            self.imgCancelledBookingSale.image = #imageLiteral(resourceName: "red_arrowUpSide")
            self.lblCancelledBookingTotalSaleProfitLossAmount.textColor = #colorLiteral(red: 0.9803921569, green: 0, blue: 0.09411764706, alpha: 1)
        } else if  cancelledBooking < cancelledBookingPrivious {
            self.lblCancelledBookingTotalSaleProfitLossAmount.text = String(cancelledBooking-cancelledBookingPrivious)
            self.imgCancelledBookingSale.image = #imageLiteral(resourceName: "red_arrow_ico")
            self.lblCancelledBookingTotalSaleProfitLossAmount.textColor = #colorLiteral(red: 0.9803921569, green: 0, blue: 0.09411764706, alpha: 1)
        }else {
            self.lblCancelledBookingTotalSaleProfitLossAmount.text = String(cancelledBooking-cancelledBookingPrivious)
            self.imgCancelledBookingSale.image = #imageLiteral(resourceName: "red_arrowUpSide")
            self.lblCancelledBookingTotalSaleProfitLossAmount.textColor = #colorLiteral(red: 0.9803921569, green: 0, blue: 0.09411764706, alpha: 1)
        }
        
        
        self.lblBookingCardTotalAmount.text = String(onlineTotalPrice)
        self.lblBookingCashTotalAmount.text = String(offlineTotalPrice)
        
        if onlineTotalPrice > onlineTotalPricePrivious{
            self.lblBookingCardTotalProfitLossAmount.text = "+"+String(onlineTotalPrice-onlineTotalPricePrivious)
            self.imgBookingCardTotalProfitLossAmount.image = #imageLiteral(resourceName: "green_arrow_ico")
            self.lblBookingCardTotalProfitLossAmount.textColor = #colorLiteral(red: 0, green: 0.7294117647, blue: 0.2705882353, alpha: 1)
        } else if onlineTotalPrice < onlineTotalPricePrivious {
            self.lblBookingCardTotalProfitLossAmount.text = "-"+String(onlineTotalPricePrivious-onlineTotalPrice)
            self.imgBookingCardTotalProfitLossAmount.image = #imageLiteral(resourceName: "red_arrow_ico")
            self.lblBookingCardTotalProfitLossAmount.textColor = #colorLiteral(red: 1, green: 0, blue: 0.168627451, alpha: 1)
        }else {
            self.lblBookingCardTotalProfitLossAmount.text = String(onlineTotalPrice-onlineTotalPricePrivious)
            self.imgBookingCardTotalProfitLossAmount.image = #imageLiteral(resourceName: "green_arrow_ico")
            self.lblBookingCardTotalProfitLossAmount.textColor = #colorLiteral(red: 0, green: 0.7294117647, blue: 0.2705882353, alpha: 1)
        }
        
        if offlineTotalPrice > offlineTotalPricePrivious{
            self.lblBookingCashTotalProfitLossAmount.text = "+"+String(offlineTotalPrice-offlineTotalPricePrivious)
            self.imgBookingCashTotalProfitLossAmount.image = #imageLiteral(resourceName: "green_arrow_ico")
            self.lblBookingCashTotalProfitLossAmount.textColor = #colorLiteral(red: 0, green: 0.7294117647, blue: 0.2705882353, alpha: 1)
        } else if offlineTotalPrice < offlineTotalPricePrivious {
            self.lblBookingCashTotalProfitLossAmount.text = "-"+String(offlineTotalPricePrivious-offlineTotalPrice)
            self.imgBookingCashTotalProfitLossAmount.image = #imageLiteral(resourceName: "red_arrow_ico")
            self.lblBookingCashTotalProfitLossAmount.textColor = #colorLiteral(red: 1, green: 0, blue: 0.137254902, alpha: 1)
        } else {
            self.lblBookingCashTotalProfitLossAmount.text = String(offlineTotalPrice-offlineTotalPricePrivious)
            self.imgBookingCashTotalProfitLossAmount.image = #imageLiteral(resourceName: "green_arrow_ico")
            self.lblBookingCashTotalProfitLossAmount.textColor = #colorLiteral(red: 0, green: 0.7294117647, blue: 0.2705882353, alpha: 1)
        }
        
        
    }
}


//MARK: - Button method extension
extension AnalyticsVC{
    @objc func refreshChatBadgeCount(_ objNotify: Notification){
        if objChatShareData.chatBadgeCount == 0{
            self.viewChatbadgeCount.isHidden = true
        }else{
            self.lblChatbadgeCount.text = String(objChatShareData.chatBadgeCount)
            self.viewChatbadgeCount.isHidden = false
        }
    }
    @IBAction func btnChatAction(_ sender: UIButton) {
        let sb: UIStoryboard = UIStoryboard(name: "Chat", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"ChatHistoryVC") as? ChatHistoryVC{
            objVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(objVC, animated: true)
        }
    }
    
    @IBAction func btnSaleFromDate(_ sender: UIButton) {
        dateForField = 1
        self.DOBPicker.minimumDate = objAppShareData.setDateFromString1(time:"01/01/1985")
        self.viewPicker.isHidden = false
        self.DOBPicker.maximumDate = Date()
        
    }
    @IBAction func btnSaleToDate(_ sender: UIButton) {
        self.DOBPicker.maximumDate = Date()
        
        dateForField = 2
        if self.txtSaleFromDate.text != "" && self.txtSaleFromDate.text != nil && self.txtSaleFromDate.text != "From"{
            self.DOBPicker.minimumDate = objAppShareData.setDateFromString1(time:self.txtSaleFromDate.text!)
        }
        if self.txtSaleFromDate.text == "" || self.txtSaleFromDate.text == nil ||  self.txtSaleFromDate.text == "From"{
            objAppShareData.showAlert(withMessage: "Please select From date", type: alertType.banner, on: self)
        }else{
            self.viewPicker.isHidden = false
        }
    }
    
    @IBAction func btnBookingFromDate(_ sender: UIButton) {
        dateForField = 3
        self.DOBPicker.minimumDate = objAppShareData.setDateFromString1(time:"01/01/1985")
        self.DOBPicker.maximumDate = Date()
        
        self.viewPicker.isHidden = false
    }
    
    @IBAction func btnBookingToDate(_ sender: UIButton) {
        dateForField = 4
        self.DOBPicker.maximumDate = Date()
        if self.txtBookingFromDate.text != "" && self.txtBookingFromDate.text != nil && self.txtBookingFromDate.text != "From"{
            self.DOBPicker.minimumDate = objAppShareData.setDateFromString1(time:self.txtBookingFromDate.text!)
        }
        if self.txtBookingFromDate.text == "" || self.txtBookingFromDate.text == nil ||  self.txtBookingFromDate.text == "From"{
            objAppShareData.showAlert(withMessage: "Please select From date", type: alertType.banner, on: self)
        }else{
            self.viewPicker.isHidden = false
        }
        
    }
    
    
    @IBAction func btnDoneCancleDOBSelection(_ sender:UIButton){
        self.view.endEditing(true)
        if sender.tag == 1 {
            
            if dateForField == 1{
                self.txtSaleFromDate.text = objAppShareData.dateFormatToShowDace(forAPI: self.DOBPicker.date)
                self.txtSaleToDate.text = "To"
            }else  if dateForField == 2{
                self.txtSaleToDate.text = objAppShareData.dateFormatToShowDace(forAPI: self.DOBPicker.date)
                
                self.viewPicker.isHidden = true
                
                let fromDate  = objAppShareData.setDateFromString1(time:self.txtSaleFromDate.text!)
                let toDate = objAppShareData.setDateFromString1(time:self.txtSaleToDate.text!)
                let startDate = objAppShareData.dateFormatToShow1(forAPI: fromDate)
                let endDate = objAppShareData.dateFormatToShow1(forAPI:toDate)
                
                let day = checkDifferenceToDate(strDate: fromDate, endDate: toDate)+1
                
                let priviousStartDate  = Calendar.current.date(byAdding: .day, value: -day, to: fromDate)
                let priviousEndDate  = Calendar.current.date(byAdding: .day, value: -1, to: fromDate )
                
                let preStartDate = objAppShareData.dateFormatToShow1(forAPI: priviousStartDate ?? Date())
                let preEndDate = objAppShareData.dateFormatToShow1(forAPI: priviousEndDate ?? Date())
                
                let param = ["startDate":startDate,
                             "endDate":endDate,
                             "previewsStartDate":preStartDate,
                             "previewsEndDate":preEndDate]
                
                self.callWebserviceForGet_AnalityckData(dict: param, dataFor: "SD")
                
            }else  if dateForField == 3{
                self.txtBookingFromDate.text = objAppShareData.dateFormatToShowDace(forAPI: self.DOBPicker.date)
                self.txtBookingToDate.text = "To"
            }else  if dateForField == 4{
                self.txtBookingToDate.text = objAppShareData.dateFormatToShowDace(forAPI: self.DOBPicker.date)
                
                self.viewPicker.isHidden = true
                let fromDate  = objAppShareData.setDateFromString1(time:self.txtBookingFromDate.text!)
                let toDate = objAppShareData.setDateFromString1(time:self.txtBookingToDate.text!)
                let startDate = objAppShareData.dateFormatToShow1(forAPI: fromDate)
                let endDate = objAppShareData.dateFormatToShow1(forAPI:toDate)
                
                let day = checkDifferenceToDate(strDate: fromDate, endDate: toDate)+1
                
                let priviousStartDate  = Calendar.current.date(byAdding: .day, value: -day, to: fromDate)
                let priviousEndDate  = Calendar.current.date(byAdding: .day, value: -1, to: fromDate )
                let preStartDate = objAppShareData.dateFormatToShow1(forAPI: priviousStartDate ?? Date())
                let preEndDate = objAppShareData.dateFormatToShow1(forAPI: priviousEndDate ?? Date())
                let param = ["startDate":startDate, "endDate":endDate,   "previewsStartDate":preStartDate,   "previewsEndDate":preEndDate]
                
                self.callWebserviceForGet_AnalityckData(dict: param, dataFor: "BD")
            }
        }
        self.viewPicker.isHidden = true
    }
}

//MARK: - extension webservicess
fileprivate extension AnalyticsVC{
    func removeDataFromArray(dataFor:String){
        if dataFor == "ALL"{
            self.arrTodaySale.removeAll()
            self.arrTodaySalePriviousData.removeAll()
            self.arrTodayBooking.removeAll()
            self.arrTodayBookingPriviousData.removeAll()
        }else if dataFor == "ST" {
            self.arrTodaySale.removeAll()
            self.arrTodaySalePriviousData.removeAll()
        }else  if dataFor == "SW"  {
            self.arrWeekSale.removeAll()
            self.arrWeekSalePriviousData.removeAll()
        }else  if dataFor == "SM"  {
            self.arrMonthSale.removeAll()
            self.arrMonthSalePriviousData.removeAll()
        }else  if  dataFor == "SD" {
            self.arrCustomeSale.removeAll()
            self.arrCustomeSalePriviousData.removeAll()
        } else if dataFor == "BT"   {
            self.arrTodayBooking.removeAll()
            self.arrTodayBookingPriviousData.removeAll()
        }else  if dataFor == "BW"  {
            self.arrWeekBooking.removeAll()
            self.arrWeekBookingPriviousData.removeAll()
        }else  if  dataFor == "BM"  {
            self.arrMonthBooking.removeAll()
            self.arrMonthBookingPriviousData.removeAll()
        }else  if  dataFor == "BD"{
            self.arrCustomeBooking.removeAll()
            self.arrCustomeBookingPriviousData.removeAll()
        }
    }
    
    
    func appendCurrentData(dataFor:String, obj:ModelAnalityck){
        if dataFor == "ALL"{
            self.arrTodaySale.append(obj)
            self.arrTodayBooking.append(obj)
        }else if dataFor == "ST" {
            self.arrTodaySale.append(obj)
        }else  if dataFor == "SW"  {
            self.arrWeekSale.append(obj)
        }else  if dataFor == "SM"  {
            self.arrMonthSale.append(obj)
        }else  if  dataFor == "SD" {
            self.arrCustomeSale.append(obj)
        } else if dataFor == "BT"   {
            self.arrTodayBooking.append(obj)
        }else  if dataFor == "BW"  {
            self.arrWeekBooking.append(obj)
        }else  if  dataFor == "BM"  {
            self.arrMonthBooking.append(obj)
        }else  if  dataFor == "BD"{
            self.arrCustomeBooking.append(obj)
        }
    }
    
    
    
    func appendPreviousData(dataFor:String, obj:ModelAnalityck){
        if dataFor == "ALL"{
            self.arrTodaySalePriviousData.append(obj)
            self.arrTodayBookingPriviousData.append(obj)
        }else if dataFor == "ST" {
            self.arrTodaySalePriviousData.append(obj)
        }else  if dataFor == "SW"  {
            self.arrWeekSalePriviousData.append(obj)
        }else  if dataFor == "SM"  {
            self.arrMonthSalePriviousData.append(obj)
        }else  if  dataFor == "SD" {
            self.arrCustomeSalePriviousData.append(obj)
        } else if dataFor == "BT"   {
            self.arrTodayBookingPriviousData.append(obj)
        }else  if dataFor == "BW"  {
            self.arrWeekBookingPriviousData.append(obj)
        }else  if  dataFor == "BM"  {
            self.arrMonthBookingPriviousData.append(obj)
        }else  if  dataFor == "BD"{
            self.arrCustomeBookingPriviousData.append(obj)
        }
    }
    
    
    
    func callWebserviceForGet_AnalityckData(dict: [String : Any],dataFor:String){
        self.removeDataFromArray(dataFor:dataFor)
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        
        objActivity.startActivityIndicator()
        objServiceManager.requestPost(strURL: WebURL.bookingAnalytics, params: dict  , success: { response in
            objServiceManager.StopIndicator()
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else{
                let strStatus =  response["status"] as? String ?? ""
                if strStatus == k_success{
                    var add = true
                    for aa in self.arrPageHold{
                        if aa == dataFor{
                            add = false
                        }
                    }
                    if add == true{
                        self.arrPageHold.append(dataFor)
                    }
                    if let arr = response["previousData"] as? [[String:Any]]{
                        for dict in arr{
                            let obj1 = ModelAnalityck.init(dict: dict)
                            self.appendPreviousData(dataFor: dataFor, obj: obj1)
                        }
                    }
                    
                    if let arr = response["data"] as? [[String:Any]]{
                        for dict in arr{
                            let objA = ModelAnalityck.init(dict: dict)
                            self.appendCurrentData(dataFor: dataFor, obj: objA)
                        }
                        self.setDataOnPageNo(dataFor:dataFor)
                    }
                }else{
                    self.setDataOnPageNo(dataFor:dataFor)
                    let msg = response["message"] as! String
                    objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                }
            }
        }){ error in
            objServiceManager.StopIndicator()
            self.setDataOnPageNo(dataFor:dataFor)
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
}

//MARK: - for date and day value
extension AnalyticsVC{
    func loadDayName(forDate date: Date) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat  = "EEEE" // "EE" to get short style
        let dayInWeek = dateFormatter.string(from: date)
        return dayInWeek
    }
    
    func startOfMonth(startDate:Date) -> Date {
        return Calendar.current.date(from: Calendar.current.dateComponents([.year, .month], from: startDate))!
    }
    
    func endOfMonth(startDate:Date) -> Date {
        return Calendar.current.date(byAdding: DateComponents(month: 1, day: -1), to: startDate)!
    }
    
    @IBAction func btnProfileOption(_ sender: UIButton) {
        self.view.endEditing(true)
        let sb: UIStoryboard = UIStoryboard(name: "Notification", bundle: Bundle.main)
        let objChooseType = sb.instantiateViewController(withIdentifier:"NotificationVC") as! NotificationVC
        objChooseType.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(objChooseType, animated: true)
    }
    
    func badgeCount(){
        
        var strMyId = ""
        if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.businessInfoDic){
            if let userId = dict["_id"] as? String {
                strMyId = userId
            }
            if strMyId != ""{
                ref.child("socialBookingBadgeCount").child( UserDefaults.standard.string(forKey: UserDefaults.keys.userId) ?? strMyId).observe(.value, with: { (snapshot) in
                    
                    let dict = snapshot.value as? [String:Any]
                    
                    var bagCount = "0"
                    if let count = dict?["businessInvitationCount"] as? Int {
                        if count >= 100{
                            bagCount = "99+"
                        }else{
                            bagCount = String(count)
                        }
                    }else if let count = dict?["businessInvitationCount"] as? String {
                        bagCount = count
                    }
                    if bagCount == "0"{
                    }else{
                    }
                    
                    var WalkinhbagCount = "0"
                    if let count = dict?["commissionCount"] as? Int {
                        if count >= 100{
                            WalkinhbagCount = "99+"
                        }else{
                            WalkinhbagCount = String(count)
                        }
                    }else if let count = dict?["commissionCount"] as? String {
                        WalkinhbagCount = count
                    }
                    if WalkinhbagCount == "0"{
                        
                    }else{
                        
                    }
                    
                })
                
                var strMyId = ""
                if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.businessInfoDic){
                    if let userId = dict["_id"] as? String {
                        strMyId = userId
                    }
                    ref.child("businessBookingBadgeCount").child(strMyId).observe(.value, with: { [weak self] (snapshot) in
                        guard let strongSelf = self else {
                            return
                        }
                        let dict = snapshot.value as? [String:Any]
                        print(dict)
                        var totalCount = 0
                        if let count = dict?["socialCount"] as? Int {
                            totalCount = count+totalCount
                        }else if let count = dict?["socialCount"] as? String {
                            totalCount = (Int(count) ?? 0)+totalCount
                        }
                        if let count = dict?["bookingCount"] as? Int {
                            totalCount = count+totalCount
                        }else if let count = dict?["bookingCount"] as? String {
                            totalCount = (Int(count) ?? 0)+totalCount
                        }
                        if totalCount == 0{
                            self?.viewNotificationCount.isHidden = true
                            self?.lblNotificationCount.text =  "0"
                        }else{
                            self?.viewNotificationCount.isHidden = false
                            self?.lblNotificationCount.text =  String(totalCount)
                            if  totalCount >= 100{
                                self?.lblNotificationCount.text = "99+"
                            }
                        }
                    })
                }
            }
        }
    }
}


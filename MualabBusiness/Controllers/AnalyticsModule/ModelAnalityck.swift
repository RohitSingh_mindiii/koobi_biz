//
//  ModelAnalityck.swift
//  MualabBusiness
//
//  Created by Mindiii on 2/27/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit

class ModelAnalityck: NSObject {
    
    public var artistId = ""
    public var _id = ""
    public var artistRating = 0
    public var artistRatingCrd = 0
    public var bookStatus = 0
    public var bookingTime = "";
    public var bookingType = 0
    public var clientId = ""
    public var crd = ""
    public var customerType = ""
    
    public var latitude = ""
    public var location = ""
    public var longitude = ""
    public var reviewByArtist = "";
    public var reviewByUser = "";
    public var paymentTime = ""
    public var transjectionId = "";
    public var upd = "";
    public var userId = ""
    
    public var deleteStatus = 0
    public var discountPrice = 0
    public var userRating = 0
    public var userRatingCrd = 0
    public var reviewStatus = 0;
    public var timeCount = 0;
    public var totalPrice = 0.0
    public var paymentStatus = 0
    public var paymentType = 0
    
    
    init(dict: [String : Any]){
        
        _id = String(dict["_id"] as? Int ?? 0)
        if let status = dict["_id"] as? String{
            _id = status
        }

        artistId = String(dict["artistId"] as? Int ?? 0)
        if let status = dict["artistId"] as? String{
            artistId = status
        }
        
        artistRating = dict["artistRating"] as? Int ?? 0
        if let bkstatus = dict["artistRating"] as? String {
            self.artistRating = Int(bkstatus) ?? 0
        }
        artistRatingCrd = dict["artistRatingCrd"] as? Int ?? 0
        if let bkstatus = dict["artistRatingCrd"] as? String {
            self.artistRatingCrd = Int(bkstatus) ?? 0
        }
        self.bookStatus = dict["bookStatus"] as? Int ?? 0
        if let bkstatus = dict["bookStatus"] as? String {
            self.bookStatus = Int(bkstatus) ?? 0
        }
        
        bookingType = dict["bookingType"] as? Int ?? 0
        if let bkstatus = dict["bookingType"] as? String {
            self.bookingType = Int(bkstatus) ?? 0
        }
        
        deleteStatus = dict["deleteStatus"] as? Int ?? 0
        if let bkstatus = dict["deleteStatus"] as? String {
            self.deleteStatus = Int(bkstatus) ?? 0
        }
        discountPrice = dict["discountPrice"] as? Int ?? 0
        if let bkstatus = dict["discountPrice"] as? String {
            self.discountPrice = Int(bkstatus) ?? 0
        }
        userRating = dict["userRating"] as? Int ?? 0
        if let bkstatus = dict["userRating"] as? String {
            self.userRating = Int(bkstatus) ?? 0
        }
        userRatingCrd = dict["userRatingCrd"] as? Int ?? 0
        if let bkstatus = dict["userRatingCrd"] as? String {
            self.userRatingCrd = Int(bkstatus) ?? 0
        }
        timeCount = dict["timeCount"] as? Int ?? 0
        if let bkstatus = dict["timeCount"] as? String {
            self.timeCount = Int(bkstatus) ?? 0
        }
        
       if let totalPrice = dict["totalPrice"] as? Double {
            self.totalPrice = totalPrice
       }else if let totalPrice = dict["totalPrice"] as? Float {
        self.totalPrice = Double(totalPrice)
       }else if let totalPrice = dict["totalPrice"] as? Int {
        self.totalPrice = Double(totalPrice)
       }else if let totalPrice = dict["totalPrice"] as? String {
        self.totalPrice = Double(totalPrice) ?? 0.0
        }
        
        paymentStatus = dict["paymentStatus"] as? Int ?? 0
        if let bkstatus = dict["paymentStatus"] as? String {
            self.paymentStatus = Int(bkstatus) ?? 0
        }
        
        paymentType = dict["paymentType"] as? Int ?? 0
        if let bkstatus = dict["paymentType"] as? String {
            self.paymentType = Int(bkstatus) ?? 0
        }
        reviewStatus = dict["reviewStatus"] as? Int ?? 0
        if let bkstatus = dict["reviewStatus"] as? String {
            self.reviewStatus = Int(bkstatus) ?? 0
        }
        bookingTime = dict["bookingTime"] as? String ?? ""
        latitude = dict["latitude"] as? String ?? ""
        location = dict["location"] as? String ?? ""
        longitude = dict["longitude"] as? String ?? ""
        reviewByUser = dict["reviewByUser"] as? String ?? ""
        reviewByArtist = dict["reviewByArtist"] as? String ?? ""
        paymentTime = dict["paymentTime"] as? String ?? ""
         upd = dict["upd"] as? String ?? ""
 
        userId = String(dict["userId"] as? Int ?? 0)
        if let status = dict["userId"] as? String{
            userId = status
        }
        customerType = String(dict["customerType"] as? Int ?? 0)
        if let status = dict["customerType"] as? String{
            customerType = status
        }
        
        transjectionId = String(dict["transjectionId"] as? Int ?? 0)
        if let status = dict["transjectionId"] as? String{
            transjectionId = status
        }
      
}
}

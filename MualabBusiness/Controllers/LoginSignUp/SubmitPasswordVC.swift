//
//  SubmitPasswordVC.swift
//  MualabBusiness
//
//  Created by Mac on 28/12/2017 .
//  Copyright © 2017 Mindiii. All rights reserved.
//

import UIKit
import CoreData
import  Firebase
class SubmitPasswordVC: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var btnContinue : UIButton!
    @IBOutlet weak var btnShowPassword : UIButton!
    @IBOutlet weak var btnConfirmPassword : UIButton!
    var managedContext:NSManagedObjectContext?

    @IBOutlet weak var lblLogin:UILabel!
    
    @IBOutlet weak var txtPassword:UITextField!
    @IBOutlet weak var txtConfirmPassword:UITextField!
    
    var userImage : UIImage?
    
}

//MARK: - View Hirarchy
extension SubmitPasswordVC{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        managedContext =
            appDelegate.persistentContainer.viewContext
        self.txtConfirmPassword.delegate = self
        self.txtPassword.delegate = self
        addGasturesToViews()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.configureView()
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

//MARK: - Local Methods
fileprivate extension SubmitPasswordVC{
    func configureView(){
        self.btnContinue.layer.cornerRadius = self.btnContinue.frame.size.height/2
    }
    
    func addGasturesToViews() {
        
        let loginTap = UITapGestureRecognizer(target: self, action: #selector(handleLoginTap(gestureRecognizer:)))
        lblLogin.addGestureRecognizer(loginTap)
        
        // let countryCodeTap = UITapGestureRecognizer(target: self, action: #selector(handleCountryCodeTap(gestureRecognizer:)))
        // lblCountryCode.addGestureRecognizer(countryCodeTap)
    }
    
    func saveUserData(dict:[String:Any]) {
        self.removePropertyData()
//        var fAddress = dict[Keys.user.address] as! String
//
//        if fAddress.range(of: dict[Keys.user.addressComponent.city] as! String) != nil{
//            fAddress.removeSubrange(fAddress.range(of: dict[Keys.user.addressComponent.city] as! String)!)
//        }
//        if fAddress.range(of: dict[Keys.user.addressComponent.country] as! String) != nil{
//            fAddress.removeSubrange(fAddress.range(of: dict[Keys.user.addressComponent.country] as! String)!)
//        }
//        //Rohit
////        if fAddress.range(of: dict[Keys.user.addressComponent.postalCode] as! String) != nil{
////            fAddress.removeSubrange(fAddress.range(of: dict[Keys.user.addressComponent.postalCode] as! String)!)
////        }
//        if fAddress.range(of: dict[Keys.user.addressComponent.state] as! String) != nil{
//            fAddress.removeSubrange(fAddress.range(of: dict[Keys.user.addressComponent.state] as! String)!)
//        }
//
//        let commaSet = CharacterSet.init(charactersIn:", \n")
//        fAddress = fAddress.trimmingCharacters(in:commaSet)
//        let ad = Address.init(locality:fAddress,
//                              address2:"",
//                              city:dict[Keys.user.addressComponent.city] as! String,
//                              state: dict[Keys.user.addressComponent.state] as! String,
//                             // postalCode: dict[Keys.user.addressComponent.postalCode] as! String,
//                              country:dict[Keys.user.addressComponent.country] as! String,
//                              placeName:"",
//                              fullAddress:dict[Keys.user.address] as! String,
//                              latitude: dict[Keys.user.addressComponent.latitude] as? Int,
//                              longitude:dict[Keys.user.addressComponent.longitude] as? Int)
//
      //  let userInfo = User.init(address:ad)
        
        //Rohit
//        if let userInf = dict["user_id"] as? Int{
//            userInfo.userId = NSNumber(value: userInf)
//        }else if let userInf = dict["user_id"] as? String{
//            userInfo.userId = NSNumber(value: Int(userInf) ?? 0)
//        }
       // userInfo.userId = dict[Keys.user.userId] as! NSNumber
        
      //  userInfo.bankStatus = Bool(truncating:dict[Keys.user.bankStatus] as! NSNumber)
      //  userInfo.bio = dict[Keys.user.bio] as! String
      //  userInfo.businessName=dict[Keys.user.businessName] as! String
  //      let archivedObject = NSKeyedArchiver.archivedData(withRootObject: userInfo)
        
//        let dicParam = ["radius":dict["radius"],
//                        "serviceType":dict["serviceType"],
//                        "inCallpreprationTime":dict["inCallpreprationTime"],
//                        "outCallpreprationTime":dict["outCallpreprationTime"]] as [String : Any]
        
      //  UserDefaults.standard.setValue(dicParam,forKey: UserDefaults.keys.outCallData)
//        UserDefaults.standard.setValue(archivedObject,forKey: UserDefaults.keys.userInfo)
//        UserDefaults.standard.set(dict[UserDefaults.keys.authToken], forKey: UserDefaults.keys.authToken)
//        UserDefaults.standard.set(dict, forKey: UserDefaults.keys.userData)
//
//        UserDefaults.standard.synchronize()
    }
}

//MARK: - UITapGesttureActions
fileprivate extension SubmitPasswordVC{
    
    @objc func handleLoginTap(gestureRecognizer: UIGestureRecognizer) {
        self.navigationController?.popToRootViewController(animated: true)
    }
}
//MARK: - IBActions
fileprivate extension SubmitPasswordVC{
    @IBAction func btnBackAction(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnShowPassword(_ sender:UIButton){
        if self.btnShowPassword.isSelected {
            self.btnShowPassword.isSelected = false
            self.txtPassword.isSecureTextEntry = true
        }else{
            self.btnShowPassword.isSelected = true
            self.txtPassword.isSecureTextEntry = false
        }
    }
    
    
    @IBAction func btnConfirmPassword(_ sender:UIButton){
        if self.btnConfirmPassword.isSelected {
            self.btnConfirmPassword.isSelected = false
            self.txtConfirmPassword.isSecureTextEntry = true
        }else{
            self.btnConfirmPassword.isSelected = true
            self.txtConfirmPassword.isSecureTextEntry = false
        }
    }
    
    
    @IBAction func btnContinue(_ sender:UIButton){
        var invalid = false
        var strMessage = ""
        
        txtPassword.text = txtPassword.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        txtConfirmPassword.text = txtConfirmPassword.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        if self.txtPassword.text?.count == 0{
            invalid = true
            objAppShareData.shakeViewField(self.txtPassword)
        }else if !(objValidationManager.isPasswordContainsCap(self.txtPassword.text!)) || !(objValidationManager.isPasswordContainsNum(self.txtPassword.text!)) || (self.txtPassword.text?.count)! < 8{
            invalid = true
            strMessage = message.validation.validPassword
        }else if self.txtConfirmPassword.text?.count == 0{
            invalid = true
            objAppShareData.shakeViewField(self.txtConfirmPassword)
        }else if self.txtPassword.text! != self.txtConfirmPassword.text!{
            invalid = true
            strMessage = "Password and confirm password should be same"
        }
        
        if invalid && strMessage.count>0 {
            objAppShareData.showAlert(withMessage: strMessage, type: alertType.banner, on: self)
        }else if !invalid{
            self.view.endEditing(true)
          //  self.saveUserData()
            self.callWebserviceForRegistration()
        }
    }
}
//MARK: - Webservices
fileprivate extension SubmitPasswordVC{
    
    func callWebserviceForRegistration(){
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        objActivity.startActivityIndicator()
        let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
        let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! User
        
        let dicParam = ["user_name":userInfo.userName.lowercased(),
                        "first_name":userInfo.firstName,
                        "last_name":userInfo.lastName,
                        "email":userInfo.email,
                        "password":self.txtPassword.text!,
                        "country_code":userInfo.countryCode,
                        "contact_no":userInfo.contactNumber,
                        "dob":userInfo.DOB,
                        "gender":"1",
                        //"user_type":"artist",
                        //"business_type":userInfo.type,
                        //"deviceType":"1",
                        //"deviceToken":objAppShareData.firebaseToken,
                        "firebase_token":objAppShareData.firebaseToken] as [String : AnyObject]
        print(dicParam)
        var imgData:Data?
        
        if let data = self.userImage{
            imgData = data.jpegData(compressionQuality: 0.1)
        }
        
        objServiceManager.uploadMultipartData(strURL: WebURL.registration, params: dicParam, imageData:imgData, fileName: "file.jpg", key: "profile_image", mimeType: "image/*", success: { response in

            if response["status"] as! String == "success"{
                UserDefaults.standard.setValue("1", forKey: UserDefaults.keys.navigationVC)

               // objAppShareData.showAlert(withMessage: "Register successfully , App under development", on: self)
                let dictUserDetail = response["users"] as! [String:Any]
                self.writeUserDataToFireBase(dict: dictUserDetail)

                objAppShareData.clearUserData()
                let busuinesstype = dictUserDetail["business_type"] as? String ?? ""
                UserDefaults.standard.setValue(busuinesstype,forKey: UserDefaults.keys.businessNameType)
                //Rohit Work on 7/Feb/2020
                //self.saveUserData(dict: dictUserDetail)
                
                let strToken = dictUserDetail["auth_token"] as? String ?? ""
                UserDefaults.standard.set(strToken, forKey: UserDefaults.keys.authToken)
                UserDefaults.standard.setValue(1, forKey: UserDefaults.keys.notificationStatus)

                UserDefaults.standard.set(dictUserDetail, forKey: UserDefaults.keys.userInfoDic)
                UserDefaults.standard.set(true, forKey:  UserDefaults.keys.isLoggedIn)
                let doc = dictUserDetail["is_document"] as? Int ?? 0
                //MARK:- Save User Data in UserDefault
                //Rohit => work on 07/Feb/2020
                objAppShareData.objAppdelegate.saveUserInfoDetail(dictOfInfo: dictUserDetail)
                
               // objAppShareData.objAppdelegate.parseBusinessSignUpDetail(dictOfInfo: dictUserDetail)
                //Rohit
//                var myId = ""
//                if let doc =  dictUserDetail["user_id"] as? String {
//                    myId = doc
//                }else if let  doc =  dictUserDetail["user_id"] as? Int{
//                    myId = String(doc)
//                }
//                UserDefaults.standard.set(myId, forKey: UserDefaults.keys.newAdminId)
                UserDefaults.standard.set(String(doc), forKey: UserDefaults.keys.isDocument)
                
                if let id = dictUserDetail["user_id"] as? String{
                    UserDefaults.standard.set(id, forKey:  UserDefaults.keys.myId)
                }else if let id = dictUserDetail["user_id"] as? Int{
                    UserDefaults.standard.set(String(id), forKey:  UserDefaults.keys.myId)
                }
                UserDefaults.standard.set(dictUserDetail["user_name"] as? String ?? "", forKey:  UserDefaults.keys.myName)
                UserDefaults.standard.set(dictUserDetail["profile_image"] as? String ?? "", forKey:  UserDefaults.keys.myImage)
                UserDefaults.standard.synchronize()
                
                strAuthToken = UserDefaults.standard.string(forKey:UserDefaults.keys.authToken)!
                appDelegate.showSignUpProcess()
            }else{
                let msg = response["message"] as! String
                objAppShareData.showAlert(withMessage: msg, type: alertType.banner, on: self)
            }
        }) { error in
            //objAppShareData.showDeafultAlert(withTitle: message.error.title, message: message.error.wrong, on: self)
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
          }
    }
}
//MARK: - UITextFieldDelegate
extension SubmitPasswordVC {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        if textField == self.txtPassword{
            txtConfirmPassword.becomeFirstResponder()
        }else if textField == self.txtConfirmPassword{
            txtConfirmPassword.resignFirstResponder()
        }
        return true
    }
    
    func removePropertyData(){
        let fetchRequest: NSFetchRequest<BusinessType> = BusinessType.fetchRequest()
        do {
            let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest as! NSFetchRequest<NSFetchRequestResult>)
            try managedContext?.execute(deleteRequest)
         } catch {
         }
    }
}

//MARK: - FireBaseRegistration Method
extension SubmitPasswordVC {
    
    func writeUserDataToFireBase(dict:[String : Any]){
        let calendarDate = ServerValue.timestamp()
        
        var id = 0
        if let ids = dict["_id"] as? Int{
            id = ids
        }else if let ids = dict["_id"] as? String{
            id = Int(ids)!
        }
        
        var img = dict["profileImage"] as? String ?? ""
        if (dict["profileImage"] as? String ?? "") != ""{
        }else{
            img = "http://koobi.co.uk:3000/uploads/default_user.png"
        }
        
        let dict1 = ["firebaseToken":checkForNULL(obj: dict["firebaseToken"]),
                     "isOnline":checkForNULL(obj:1),
                     "lastActivity":checkForNULL(obj:calendarDate),
                     "profilePic":checkForNULL(obj:img),
                     "uId":checkForNULL(obj:id),
                     "userName":checkForNULL(obj:dict["userName"]) ]
//        let dict1 = ["firebaseToken":"",
//                     "isOnline":checkForNULL(obj:1),
//                     "lastActivity":checkForNULL(obj:calendarDate),
//                     "profilePic":checkForNULL(obj:img),
//                     "uId":checkForNULL(obj:id),
//                     "userName":checkForNULL(obj:dict["userName"]) ]
        
        let ref = Database.database().reference()
        let myId:String = String(dict["_id"] as? Int ?? 0000000)
        ref.child("users").child(myId).setValue(dict1)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            objWebserviceManager.StopIndicator()
        }
    }
}

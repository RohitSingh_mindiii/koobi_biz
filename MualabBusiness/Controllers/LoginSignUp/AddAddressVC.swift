//
//  AddAddressVC.swift
//  MualabBusiness
//
//  Created by Mac on 08/01/2018 .
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import GooglePlacePicker
import GoogleMaps
import Alamofire

fileprivate enum check : String {
    
    case administrative_area_level_2,
    administrative_area_level_1,
    country,
    postal_code,
    locality
}

class AddAddressVC: UIViewController{//},GMSPlacePickerViewControllerDelegate {
    
    @IBOutlet weak var viewRightButton:UIView?
    @IBOutlet weak var txtCity:UITextField?
    @IBOutlet weak var txtLocality:UITextView?
    @IBOutlet weak var txtPostalCode:UITextField?
    @IBOutlet weak var txtState:UITextField?
    @IBOutlet weak var txtCountry:UITextField?
    @IBOutlet weak var txtAddress2:UITextField?
    @IBOutlet weak var txtPostCode:UITextField?
    @IBOutlet weak var txtHouseNumber:UITextField?
    @IBOutlet weak var viewOutcallDetail:UIView?
    @IBOutlet weak var viewBigPostCode:UIView?
    @IBOutlet weak var txtMainPostCode:UITextField?
    @IBOutlet weak var tblAddress:UITableView!
    @IBOutlet weak var activityIndicator:UIActivityIndicatorView?
    
    var arrAddress = [String]()
    var arrPlaceName = [String]()
    var fromRadiusVC = false
    fileprivate var latitude = ""
    fileprivate var longitude = ""
    fileprivate var fullAddress = ""
    fileprivate var placeName = ""
    var isOutcallOption = false
 
    @IBOutlet weak var height:NSLayoutConstraint?
    @IBOutlet weak var textViewHeight: NSLayoutConstraint!
    var objArtistDetails = ArtistDetails(dict: ["":""])

}


//MARK: - View Hirarchy
extension AddAddressVC{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblAddress.estimatedRowHeight = 50.0
        self.tblAddress.rowHeight = UITableView.automaticDimension
        self.addAccessoryView()
        self.setAddress()
        
        self.txtLocality?.text =  objAppShareData.objModelBusinessSetup.address
        self.resizeTextView(textView: self.txtLocality!,string:objAppShareData.objModelBusinessSetup.address)
        self.txtCity?.text =  objAppShareData.objModelBusinessSetup.city
        self.txtState?.text = objAppShareData.objModelBusinessSetup.state
        self.txtCountry?.text =  objAppShareData.objModelBusinessSetup.country
        self.txtAddress2?.text =  objAppShareData.objModelBusinessSetup.location
        self.txtPostalCode?.text =  objAppShareData.objModelBusinessSetup.postalCode
       
        self.tblAddress.estimatedRowHeight = 50.0
        self.tblAddress.rowHeight = UITableView.automaticDimension
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewBigPostCode!.isHidden = true
        self.txtPostCode?.text = ""
        if fromRadiusVC == true{
            self.viewOutcallDetail?.isHidden = true
        }else{
            self.viewOutcallDetail?.isHidden = false
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.configureView()
          //  self.height?.constant = (self.txtCity?.frame.size.height)!
        }
    }
    
    
    
    func getDistancBetweenTwoPoint(strlat1 : String, strLong1 : String, strlat2 : String, strLong2 : String) -> Double{
        
        let lat1 = Double(strlat1) ?? 0
        let long1 = Double(strLong1) ?? 0
        
        let lat2 = Double(strlat2) ?? 0
        let long2 = Double(strLong2) ?? 0
        
        let coordinate1 = CLLocation(latitude: lat1, longitude: long1)
        let coordinate2 = CLLocation(latitude: lat2, longitude: long2)
        
        let distanceInMeters = coordinate1.distance(from: coordinate2) // result is in meters
        //1 meter =  0.000621371 mile
        let distanceInMiles = 0.000621371 * distanceInMeters
        
        return distanceInMiles
    }
    func checkArtistAvailabilityAtSelectedAdd(){
        
        let artistRadius = objArtistDetails.radius
        let distanceInMiles = getDistancBetweenTwoPoint(strlat1: objArtistDetails.latitude, strLong1: objArtistDetails.longitude, strlat2: self.latitude, strLong2: self.longitude)
        
        if distanceInMiles > artistRadius {
            objArtistDetails.isAddressInArtistRange = false
            objAppShareData.showAlert(withMessage: "Selected artist services is not available at this location", type: alertType.bannerDark, on: self)
            
            objArtistDetails.userBookingSelectedAddress = ""
            objArtistDetails.userBookingSelectedAddressLatitude = ""
            objArtistDetails.userBookingSelectedAddressLongitude = ""
            
        }else{
            if self.fullAddress.count > 0{
                objAppShareData.strBookingOutCallAddress = self.fullAddress
            }else{
                objAppShareData.strBookingOutCallAddress = (self.txtLocality?.text)!
            }
            objAppShareData.latitudeBookingOutCallAddress = self.latitude
            objAppShareData.longitudeBookingOutCallAddress = self.longitude
            self.btnBack(0)
        }
    }
}
//MARK: - Local Methods
fileprivate extension AddAddressVC{
    func configureView() {
        if !isOutcallOption{
             self.setAddress()
        }
        self.viewRightButton?.isHidden = true
        self.txtLocality?.tintColor = UIColor.white
    }
    
    func makeAddressWithPlace(place: GMSPlace) {
        var city = ""
        var country = ""
        var postalCode = ""
        var state = ""
        self.latitude = String.init(place.coordinate.latitude)
        self.longitude = String.init(place.coordinate.longitude)
        objAppShareData.objModelBusinessSetup.latitude = self.latitude
        objAppShareData.objModelBusinessSetup.longitude = self.longitude
        self.placeName = place.name!
        if let arrAdd = place.addressComponents {
            for address in arrAdd{
                
                switch address.type  {
                    
                case check.locality.rawValue:
                    city = address.name
                    
                case check.administrative_area_level_1.rawValue:
                    state = address.name
                    
                case check.postal_code.rawValue:
                    postalCode = address.name
                    
                case check.country.rawValue:
                    country = address.name
                    
                default:
                print("No Case")
                }
            }
            
        }
        var fAddress = place.formattedAddress ?? ""
        self.fullAddress = fAddress
        if fAddress.range(of: city) != nil{
            //fAddress.removeSubrange(fAddress.range(of: city)!)
        }
        if fAddress.range(of: country) != nil{
            //fAddress.removeSubrange(fAddress.range(of: country)!)
        }
        if fAddress.range(of: postalCode) != nil {
            //fAddress.removeSubrange(fAddress.range(of: postalCode)!)
        }
        if fAddress.range(of: state) != nil {
            //fAddress.removeSubrange(fAddress.range(of: state)!)
        }
        
        let commaSet = CharacterSet.init(charactersIn:", \n")
        fAddress = fAddress.trimmingCharacters(in:commaSet)
        if fAddress.count > 0{
            self.txtLocality?.text = "\(fAddress)"
            self.resizeTextView(textView: self.txtLocality!,string: fAddress)
        }
        if city.count > 0 {self.txtCity?.text = city}
        if postalCode.count > 0{self.txtPostalCode?.text = postalCode}
        if country.count > 0{self.txtCountry?.text = country}
        if state.count > 0 {self.txtState?.text = state}
        
        
        if fAddress.count > 0{
            //self.txtLocality?.text = "\(fAddress)"
            if self.placeName.count>0{
                self.txtLocality?.text = self.placeName + ", " + fAddress
            }else{
                self.txtLocality?.text = "\(fAddress)"
            }}
        let add =  self.txtLocality?.text ?? ""
        let PostCode =  self.txtPostCode?.text ?? ""
        let name =  self.placeName

          let param = ["city": city ?? "", "state":state ?? "", "country":country ?? "", "locality":"\(fAddress)" ?? "",
            "postalCode":PostCode,
            "address2":add,"address":add, "latitude":String(self.latitude) ?? "",
            "longitude":String(self.longitude) ?? "",
            "name":name]
//        objAppShareData.objModelBusinessSetup.address2 = ""
        objAppShareData.paramAddress = param
        //Enable Text fields
        self.txtCity?.isEnabled = true
        self.txtState?.isEnabled = true
        self.txtCountry?.isEnabled = true
        self.txtAddress2?.isEnabled = true
        self.txtLocality?.isEditable = true
        self.txtPostalCode?.isEnabled = true
      
    }

    func resizeTextView(textView:UITextView , string:String){
        
        //textView.font = UIFont.init(name:"Roboto-Light",size: 15.0)
        textView.font = UIFont.init(name:"Nunito-Regular",size: 18.0)
        textView.translatesAutoresizingMaskIntoConstraints = true
        textView.sizeToFit()
        textView.frame.size.width = self.view.frame.size.width - 40
        textView.isScrollEnabled = false
        textViewHeight.constant = textView.frame.size.height
    }
    
    func saveAddress(){
        objAppShareData.fromAddressVC = true
        //Rohit work on 07/feb/2020
        
        var userDict = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfoDic) as? [String:Any] ?? ["":""]
        
        if self.txtCity?.text != "" {
            userDict["\(UserDefaults.keys.city)"] = self.txtCity?.text
        }
        
        if self.txtState?.text != "" {
            userDict["\(UserDefaults.keys.state)"] = self.txtState?.text
        }
        
        if self.txtCountry?.text != "" {
            userDict["\(UserDefaults.keys.country)"] = self.txtCountry?.text
        }
        
        if self.txtLocality?.text != "" {
            userDict["\(UserDefaults.keys.address)"] = self.txtLocality?.text
        }
        
        if self.latitude != "" {
            userDict["\(UserDefaults.keys.latitude)"] = self.latitude
        }
        
        if self.longitude != "" {
            userDict["\(UserDefaults.keys.longitude)"] = self.longitude
        }
        
        
        UserDefaults.standard.setValue(userDict, forKey: UserDefaults.keys.userInfoDic)
        
        print(userDict)
        
        
//        let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
//        if let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? User{
//        if objAppShareData.isForBookingOutCallAddress{
//            self.checkArtistAvailabilityAtSelectedAdd()
//        }else{
//        // let addressComponent = userInfo.address
//            userInfo.address.city = (self.txtCity?.text ?? "")
//            userInfo.address.locality = (self.txtLocality?.text ?? "")
//            //Rohit
//      //  userInfo.address.postalCode = (self.txtPostalCode?.text)!
//            userInfo.address.state = (self.txtState?.text ?? "")
//            userInfo.address.country = (self.txtCountry?.text ?? "")
//            userInfo.address.address2 = (self.txtLocality?.text ?? "")
//
//        self.longitude = objAppShareData.objModelBusinessSetup.longitude
//        self.latitude = objAppShareData.objModelBusinessSetup.latitude
//
//        if self.latitude == ""{
//           self.latitude = "22.7051382"
//        }
//        if self.longitude == ""{
//            self.longitude = "75.9090618"
//        }
//        userInfo.address.longitude = self.longitude
//        userInfo.address.latitude = self.latitude
//        userInfo.address.buildingNumber = (self.txtHouseNumber?.text)!
//        if self.fullAddress.count > 0{
//            userInfo.address.fullAddress = self.placeName + ", " + self.fullAddress
//        }else{
//            userInfo.address.fullAddress = userInfo.address.locality
//        }
//        userInfo.address.placeName = self.placeName
//        if fromRadiusVC != true{
//         objAppShareData.objModelBusinessSetup.address =  userInfo.address.fullAddress
//        objAppShareData.objModelBusinessSetup.address2 =  userInfo.address.fullAddress
//        objAppShareData.objModelBusinessSetup.city = userInfo.address.city
//        objAppShareData.objModelBusinessSetup.state = userInfo.address.state
//        objAppShareData.objModelBusinessSetup.country = userInfo.address.country
//        objAppShareData.objModelBusinessSetup.location = ""
//        objAppShareData.objModelBusinessSetup.latitude = userInfo.address.latitude
//        objAppShareData.objModelBusinessSetup.longitude = userInfo.address.longitude
//        objAppShareData.objModelBusinessSetup.buildinngNo = userInfo.address.buildingNumber
//            //Rohit
//       // objAppShareData.objModelBusinessSetup.postalCode = userInfo.address.postalCode
//        }
//
//            let city =  self.txtCity?.text ?? ""
//            let postalCode =   self.txtPostalCode?.text ?? ""
//            let country =   self.txtCountry?.text ?? ""
//            let state = self.txtState?.text ?? ""
//            let add =  self.txtLocality?.text ?? ""
//            let PostCode =  self.txtPostCode?.text ?? ""
//            let name =  self.placeName
//
//            let param = ["city": city ?? "", "state":state ?? "", "country":country ?? "", "locality":"\(add)" ?? "",
//                         "postalCode":PostCode,
//                         "address2":add, "latitude":String(self.latitude) ?? "",
//                         "longitude":String(self.longitude) ?? "",
//                         "name":name]
//            //objAppShareData.objModelBusinessSetup.address2 = ""
//            objAppShareData.paramAddress = param
//
//        let archivedObject = NSKeyedArchiver.archivedData(withRootObject: userInfo)
//        UserDefaults.standard.setValue(archivedObject,forKey: UserDefaults.keys.userInfo)
//        }
//        UserDefaults.standard.synchronize()
//
//       /* }else{
//            let decodedN = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
//            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decodedN) as! [String:Any]
//                   if objAppShareData.isForBookingOutCallAddress{
//                       self.checkArtistAvailabilityAtSelectedAdd()
//                   }else{
//                   // let addressComponent = userInfo.address
//                   userInfo.address.city = (self.txtCity?.text)!
//                   userInfo.address.locality = (self.txtLocality?.text)!
//                   userInfo.address.postalCode = (self.txtPostalCode?.text)!
//                   userInfo.address.state = (self.txtState?.text)!
//                   userInfo.address.country = (self.txtCountry?.text)!
//                   userInfo.address.address2 = (self.txtLocality?.text)!
//
//                   self.longitude = objAppShareData.objModelBusinessSetup.longitude
//                   self.latitude = objAppShareData.objModelBusinessSetup.latitude
//
//                   if self.latitude == ""{
//                      self.latitude = "22.7051382"
//                   }
//                   if self.longitude == ""{
//                       self.longitude = "75.9090618"
//                   }
//                   userInfo.address.longitude = self.longitude
//                   userInfo.address.latitude = self.latitude
//                   userInfo.address.buildingNumber = (self.txtHouseNumber?.text)!
//                   if self.fullAddress.count > 0{
//                       userInfo.address.fullAddress = self.placeName + ", " + self.fullAddress
//                   }else{
//                       userInfo.address.fullAddress = userInfo.address.locality
//                   }
//                   userInfo.address.placeName = self.placeName
//                   if fromRadiusVC != true{
//                    objAppShareData.objModelBusinessSetup.address =  userInfo.address.fullAddress
//                   objAppShareData.objModelBusinessSetup.address2 =  userInfo.address.fullAddress
//                   objAppShareData.objModelBusinessSetup.city = userInfo.address.city
//                   objAppShareData.objModelBusinessSetup.state = userInfo.address.state
//                   objAppShareData.objModelBusinessSetup.country = userInfo.address.country
//                   objAppShareData.objModelBusinessSetup.location = ""
//                   objAppShareData.objModelBusinessSetup.latitude = userInfo.address.latitude
//                   objAppShareData.objModelBusinessSetup.longitude = userInfo.address.longitude
//                   objAppShareData.objModelBusinessSetup.buildinngNo = userInfo.address.buildingNumber
//                   objAppShareData.objModelBusinessSetup.postalCode = userInfo.address.postalCode
//                   }
//
//                       let city =  self.txtCity?.text ?? ""
//                       let postalCode =   self.txtPostalCode?.text ?? ""
//                       let country =   self.txtCountry?.text ?? ""
//                       let state = self.txtState?.text ?? ""
//                       let add =  self.txtLocality?.text ?? ""
//                       let PostCode =  self.txtPostCode?.text ?? ""
//                       let name =  self.placeName
//
//                       let param = ["city": city ?? "", "state":state ?? "", "country":country ?? "", "locality":"\(add)" ?? "",
//                                    "postalCode":PostCode,
//                                    "address2":add, "latitude":String(self.latitude) ?? "",
//                                    "longitude":String(self.longitude) ?? "",
//                                    "name":name]
//                       //        objAppShareData.objModelBusinessSetup.address2 = ""
//                       objAppShareData.paramAddress = param
//
//                   let archivedObject = NSKeyedArchiver.archivedData(withRootObject: userInfo)
//                   UserDefaults.standard.setValue(archivedObject,forKey: UserDefaults.keys.userInfo)
//                   }
//                   UserDefaults.standard.synchronize()
//                   */
//        }
        self.btnBackAction()
    }
    
    func btnBackAction(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func setAddress(){
        self.txtPostalCode?.text = ""
        if let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as? Data{
        if let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? User{
        let addressComponent = userInfo.address
        if addressComponent.fullAddress.count>0{
            //self.txtLocality?.text = addressComponent.locality
            self.txtLocality?.text = addressComponent.address2
            self.resizeTextView(textView: self.txtLocality!,string: addressComponent.locality)
            self.txtCity?.text = addressComponent.city
            self.txtState?.text = addressComponent.state
            //Rohit
           // self.txtPostalCode?.text = addressComponent.postalCode
            self.txtCountry?.text = addressComponent.country
            self.txtAddress2?.text = addressComponent.address2
            if self.placeName.count>0{
                self.txtLocality?.text =  self.placeName + ", " + addressComponent.fullAddress
            }
            objAppShareData.objModelBusinessSetup.latitude = addressComponent.latitude
            objAppShareData.objModelBusinessSetup.longitude = addressComponent.longitude
        }
          }
    }
    }
    func addAccessoryView() -> Void {
        let keyboardDoneButtonView = UIToolbar()
        keyboardDoneButtonView.sizeToFit()
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonTapped))
        doneButton.tintColor = appColor
        keyboardDoneButtonView.items = [flexBarButton, doneButton]
        txtLocality?.inputAccessoryView = keyboardDoneButtonView
    }
    //
    @objc func doneButtonTapped() {
        // do you stuff with done here
        self.txtLocality?.resignFirstResponder()
    }
}
//MARK: - IBActions
fileprivate extension AddAddressVC{
    @IBAction func btnBack(_ sender:Any){
        objAppShareData.fromAddressVC = false
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSave(_ sender:Any){
        var invalid = false
        //        if self.txtHouseNumber?.text?.count == 0{
        //            invalid = true
        //        }else if self.txtCity?.text?.count == 0{
        //            invalid = true
        //        }else
        if self.txtLocality?.text.count == 0 || (self.txtLocality?.text)! == "Address" {
            invalid = true
            //        }else if self.txtPostalCode?.text?.count == 0{
            //            invalid = true
        }else{
            invalid = false
        }
        
        if invalid{
            objAppShareData.showAlert(withMessage:message.validation.required, type: alertType.banner, on: self)
        }else{
            //self.txtPostalCode?.isEnabled = false
            self.txtLocality?.isEditable = false
            self.txtLocality?.isScrollEnabled = true
            saveAddress()
        }
    }
}
//MARK: - UITextView Delegate
extension AddAddressVC:UITextViewDelegate{
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool{
        let txt = textView.text! as NSString
        
        if (txt.length == 0){
            if (text == "") {
                textViewHeight.constant = 44
                textView.resignFirstResponder()
            }
        }
        return true
    }
    
    func textViewDidChange(_ textView: UITextView) {
        let fixedWidth: CGFloat = textView.frame.size.width
        let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat(MAXFLOAT)))
        if newSize.height>44{
            textViewHeight.constant = newSize.height
        }
    }
}

//MARK: - UITextfield delegate
extension AddAddressVC:UITextFieldDelegate{
    
    @IBAction func btnRightAction(_ sender:UIButton){
        self.view.endEditing(true)
        self.arrAddress.removeAll()
        self.arrPlaceName.removeAll()
        self.tblAddress.reloadData()
        
        let fullName = self.txtPostCode?.text ?? ""
        let fullNameArr = fullName.components(separatedBy: " ")
        if fullNameArr.count > 1{
            let name = fullNameArr[0]
            let surname = fullNameArr[1]
            let total = name+surname
            self.callAPI_ForGetAddressFrom(postCode:total)
        }else{
            self.callAPI_ForGetAddressFrom(postCode:(self.txtPostCode?.text!)!)
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        self.txtPostCode?.text? = (self.txtPostCode?.text?.capitalized)!
        if self.txtPostCode?.text?.count == 0 && string.count>0{
            self.viewRightButton?.isHidden = false
        }else if self.txtPostCode?.text?.count == 1 && string.count == 0{
            self.viewRightButton?.isHidden = true
        }else{
            self.viewRightButton?.isHidden = false
        }
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if self.txtPostCode?.text?.count != 0{
            self.viewRightButton?.isHidden = false
        }else{
            self.viewRightButton?.isHidden = true
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        //        if self.txtPostCode?.text?.count != 0{
        //            self.viewRightButton?.isHidden = false
        //        }else{
        //            self.viewRightButton?.isHidden = true
        //        }
        //self.viewRightButton?.isHidden = true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        textField.resignFirstResponder()
        if textField == self.txtPostCode{
            let fullName    = self.txtPostCode?.text ?? ""
            let fullNameArr = fullName.components(separatedBy: " ")
            if fullNameArr.count > 1{
                let name    = fullNameArr[0]
                let surname = fullNameArr[1]
                let total = name+surname
                self.callAPI_ForGetAddressFrom(postCode:total)
            }else{
                self.callAPI_ForGetAddressFrom(postCode:textField.text!)
            }
        }
        return true
    }
}

extension AddAddressVC: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        self.makeAddressWithPlace(place: place)
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}
//MARK: -  Google Placepicker
extension AddAddressVC:GMSPlacePickerViewControllerDelegate {
    
    @IBAction func pickPlace(_ sender: UIButton) {
        /*let config = GMSPlacePickerConfig(viewport: nil)
        let placePicker = GMSPlacePickerViewController(config: config)
        placePicker.delegate = self
        present(placePicker, animated: true, completion: nil)
        */
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        autocompleteController.modalPresentationStyle = .fullScreen
        present(autocompleteController, animated: true, completion: nil)
    }
    
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        // Dismiss the place picker, as it cannot dismiss itself.
        self.makeAddressWithPlace(place: place)
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        // Dismiss the place picker, as it cannot dismiss itself.
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func getAddressFrom(Location location:CLLocation){
        let geocoder = GMSGeocoder()
        geocoder.reverseGeocodeCoordinate(location.coordinate) { (response, error) in
            //
            var city = ""
            var country = ""
            var postalCode = ""
            var fAddress = ""
            self.latitude = String.init(location.coordinate.latitude)
            self.longitude = String.init(location.coordinate.longitude)
            //
            
            for res in (response?.results())!{
                /*if city.count == 0{
                    city = res.locality!
                }
                if country.count == 0{
                    country = res.country!
                }
                //                if postalCode.count == 0{
                //                    postalCode = res.postalCode!
                //                }
                if fAddress.count == 0{
                    self.placeName = (res.lines?.first)!
                    fAddress = (res.lines?.joined(separator: ", "))!
                }
                   - 4 : GMSAddress {
                 coordinate: (53.546974, -2.128602)
                 lines: Oldham OL9, UK
                 locality: Oldham
                 administrativeArea: England
                 postalCode: OL9
                 country: United Kingdom
                 }
                */
                self.arrAddress.append((res.lines?.joined(separator: ", "))!)
                self.arrPlaceName.append((res.lines?.first)!)
            }
            
            self.tblAddress.reloadData()
            self.tblAddress.contentSize.height = self.tblAddress.contentSize.height+250
            self.view.layoutIfNeeded()
            //
            
            /*if fAddress.count > 0{
                self.viewRightButton?.isHidden = true
                self.txtLocality?.text = "\(fAddress)"
                self.resizeTextView(textView: self.txtLocality!,string: fAddress)
            }
            if city.count > 0 {self.txtCity?.text = city}
            if postalCode.count > 0{self.txtPostalCode?.text = postalCode}
            if country.count > 0{self.txtCountry?.text = country}
            
            //Enable Text fields
            self.txtCity?.isEnabled = true
            self.txtState?.isEnabled = true
            self.txtCountry?.isEnabled = true
            self.txtAddress2?.isEnabled = true
            self.txtLocality?.isEditable = true
            //self.txtPostalCode?.isEnabled = true
            */
            self.activityIndicator?.stopAnimating()
            objWebserviceManager.StopIndicator()
        }
    }
}

//MARkK: - Call Postcode API
fileprivate extension AddAddressVC{
    func callAPI_ForGetAddressFrom(postCode code:String){
        //objActivity.startActivityIndicator()
        self.activityIndicator?.startAnimating()
        objWebserviceManager.StartIndicator()
        let url = WebURL.postcode+code
        Alamofire.request(url).response { response in
            print(response)
            //self.activityIndicator?.stopAnimating()
            
            let convertedString = String(data: response.data!, encoding: String.Encoding.utf8) // the data will be converted to the string
            
            if let dict = objServiceManager.convertToDictionary(text: convertedString!){
                
                if let result = dict["result"] as? [String:Any]{
                    let location = CLLocation.init(latitude: result["latitude"] as! CLLocationDegrees, longitude: result["longitude"] as! CLLocationDegrees)
                    self.getAddressFrom(Location: location)
                }else{
                    self.txtCity?.text = ""
                    self.txtLocality?.text = "Address"
                    self.txtAddress2?.text = ""
                    self.txtPostCode?.text = ""
                    objAppShareData.showAlert(withMessage: "Please enter valid post code", type: alertType.banner, on: self)
                    self.activityIndicator?.stopAnimating()
                    objWebserviceManager.StopIndicator()
                }
            }else{
                objWebserviceManager.StopIndicator()
                self.activityIndicator?.stopAnimating()
                self.txtCity?.text = ""
                self.txtLocality?.text = "Address"
                self.txtAddress2?.text = ""
                self.txtPostCode?.text = ""
                objAppShareData.showAlert(withMessage: "Please enter valid post code", type: alertType.banner, on: self)
            }
        }
    }
}

// MARK: - table view Delegate and Datasource
extension AddAddressVC : UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - Tableview delegate methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrAddress.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
            if let cell : CellSocialNotificationDetail = tableView.dequeueReusableCell(withIdentifier: "CellSocialNotificationDetail")! as? CellSocialNotificationDetail {
                cell.lblMsg.text  = self.arrAddress[indexPath.row]
                return cell
            }else{
                return UITableViewCell()
            }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        //self.txtLocality?.text = self.arrAddress[indexPath.row]
        self.placeName = self.arrPlaceName[indexPath.row]
        self.makeAddressWithPlacePostCode(address: self.arrAddress[indexPath.row])
        //self.resizeTextView(textView: self.txtLocality!,string: self.arrAddress[indexPath.row])
        self.txtMainPostCode!.text = self.txtPostCode!.text
        self.viewBigPostCode!.isHidden = true
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    @IBAction func btnCancelBigPopUp(_ sender:Any){
        self.view.endEditing(true)
        self.viewBigPostCode!.isHidden = true
    }
    @IBAction func btnPostCodeForBigPopUp(_ sender:Any){
        self.txtPostCode?.text = ""
        self.txtPostCode?.becomeFirstResponder()
        self.viewBigPostCode!.isHidden = false
        //self.tblAddress.isHidden = true
        self.arrAddress.removeAll()
        self.arrPlaceName.removeAll()
        self.tblAddress.reloadData()
    }

            func makeAddressWithPlacePostCode(address: String) {
                var city = ""
                var country = ""
                var postalCode = ""
                var state = ""
                //self.latitude = String.init(place.coordinate.latitude)
                //self.longitude = String.init(place.coordinate.longitude)
                objAppShareData.objModelBusinessSetup.latitude = self.latitude
                objAppShareData.objModelBusinessSetup.longitude = self.longitude
                //self.placeName = place.name!
    //            if let arrAdd = place.addressComponents {
    //                for address in arrAdd{
    //
    //                    switch address.type  {
    //
    //                    case check.locality.rawValue:
    //                        city = address.name
    //
    //                    case check.administrative_area_level_1.rawValue:
    //                        state = address.name
    //
    //                    case check.postal_code.rawValue:
    //                        postalCode = address.name
    //
    //                    case check.country.rawValue:
    //                        country = address.name
    //
    //                    default:
    //                    print("No Case")
    //                    }
    //                }
    //
    //            }
                var fAddress = address
                self.fullAddress = fAddress
                if fAddress.range(of: city) != nil{
                    //fAddress.removeSubrange(fAddress.range(of: city)!)
                }
                if fAddress.range(of: country) != nil{
                    //fAddress.removeSubrange(fAddress.range(of: country)!)
                }
                if fAddress.range(of: postalCode) != nil {
                    //fAddress.removeSubrange(fAddress.range(of: postalCode)!)
                }
                if fAddress.range(of: state) != nil {
                    //fAddress.removeSubrange(fAddress.range(of: state)!)
                }
                
                let commaSet = CharacterSet.init(charactersIn:", \n")
                fAddress = fAddress.trimmingCharacters(in:commaSet)
                if fAddress.count > 0{
                    self.txtLocality?.text = "\(fAddress)"
                    self.resizeTextView(textView: self.txtLocality!,string: fAddress)
                }
                if city.count > 0 {self.txtCity?.text = city}
                if postalCode.count > 0{self.txtPostalCode?.text = postalCode}
                if country.count > 0{self.txtCountry?.text = country}
                if state.count > 0 {self.txtState?.text = state}
                
                
                if fAddress.count > 0{
                    //self.txtLocality?.text = "\(fAddress)"
                    if self.placeName.count>0{
                        self.txtLocality?.text = self.placeName + ", " + fAddress
                    }else{
                        self.txtLocality?.text = "\(fAddress)"
                    }}
                let add = self.txtLocality?.text ?? ""
                let PostCode = self.txtPostCode?.text ?? ""
                let name = self.placeName

                  let param = ["city": city ?? "", "state":state ?? "", "country":country ?? "", "locality":"\(fAddress)" ?? "",
                    "postalCode":PostCode,
                    "address2":add,"address":add, "latitude":String(self.latitude) ?? "",
                    "longitude":String(self.longitude) ?? "",
                    "name":name]
        //        objAppShareData.objModelBusinessSetup.address2 = ""
                objAppShareData.paramAddress = param
                //Enable Text fields
                self.txtCity?.isEnabled = true
                self.txtState?.isEnabled = true
                self.txtCountry?.isEnabled = true
                self.txtAddress2?.isEnabled = true
                self.txtLocality?.isEditable = true
                self.txtPostalCode?.isEnabled = true
              
            }
}

//
//  ManageAccountVC.swift
//  MualabBusiness
//
//  Created by Mac on 18/12/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import CoreData
class ManageAccountVC: UIViewController {
    @IBOutlet weak var lblDescription : UILabel!
    var managedContext:NSManagedObjectContext?

    
    var dictOfUserData = [String:Any]()

    override func viewDidLoad() {
        super.viewDidLoad()
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        managedContext =
            appDelegate.persistentContainer.viewContext
        
         // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        self.removePropertyData()
        let username = dictOfUserData["userName"] as? String ?? ""
       self.changeString(userName: " @"+username)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnBackAction(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
     }
    
    @IBAction func btnManageAction(_ sender:Any){
       self.callWebServiceForgotPassword()

    }
    
    @IBAction func btnCrearNewAction(_ sender:Any){
        let controllers = self.navigationController?.viewControllers
        for vc in controllers! {
            if vc is  EmailPhoneVC{
                 _ = self.navigationController?.popToViewController(vc as! EmailPhoneVC, animated: true)
            }
        }
     }
    
    func changeString(userName:String){
        let colorBlack = UIColor.black
        
        let a = "You already have an existing social account using this email and phone number! would you like to merge your existing social profile"
        let b = userName
        let c = "with your biz account or create a new account for your biz profile?"
        
        let StrName = NSMutableAttributedString(string: a + " ", attributes: [NSAttributedString.Key.font:UIFont(name: "Nunito-Regular", size: 17.0)!])
        
        StrName.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.lightGray, range: NSRange(location:0,length:StrName.length))
        
        let StrName1 = NSMutableAttributedString(string: b + " ", attributes: [NSAttributedString.Key.font:UIFont(name: "Nunito-SemiBold", size: 17.0)!])
        
        StrName1.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSRange(location:0,length:StrName1.length))
        
        let StrName2 = NSMutableAttributedString(string: c + " ", attributes: [NSAttributedString.Key.font:UIFont(name: "Nunito-Regular", size: 17.0)!])
        
        StrName2.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.lightGray, range: NSRange(location:0,length:StrName2.length))
        
        let combination = NSMutableAttributedString()
        
        combination.append(StrName)
        combination.append(StrName1)
        combination.append(StrName2)
        self.lblDescription.attributedText = combination
    }

}

//MARK: - Webservices
fileprivate extension ManageAccountVC{
    
         func callWebServiceForgotPassword(){
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            objActivity.startActivityIndicator()
            let authToken = dictOfUserData["authToken"] as? String ?? ""
            UserDefaults.standard.set(authToken, forKey: UserDefaults.keys.authToken)

            let dicParam = ["businessType":"independent",
                            "userType" : "artist", ]
            
            objServiceManager.requestPost(strURL: WebURL.updateRecord, params: dicParam, success: { response in
                objActivity.stopActivity()
                if response["status"] as! String == "success"{
                    let msg1 = response["message"] as? String ?? ""
                    let dictOfUserData2 = response["user"] as? [String:Any] ?? ["":""]
                    let busuinesstype = dictOfUserData2["businessType"] as? String ?? ""
                    UserDefaults.standard.setValue(busuinesstype,forKey: UserDefaults.keys.businessNameType)
                    let strToken = dictOfUserData2["authToken"] as? String ?? ""
                    UserDefaults.standard.set(strToken, forKey: UserDefaults.keys.authToken)
                    UserDefaults.standard.set(dictOfUserData2, forKey: UserDefaults.keys.userInfoDic)
                    UserDefaults.standard.set(true, forKey:  UserDefaults.keys.isLoggedIn)
                    UserDefaults.standard.synchronize()
                    
                    
                    objAppShareData.objAppdelegate.parseBusinessSignUpDetail(dictOfInfo: dictOfUserData2)
                    
                    var dict = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] ?? ["":""]
                        dict[UserDefaults.keys.isDocument] = "0"
                        UserDefaults.standard.set(dict, forKey: UserDefaults.keys.userInfoDic)
                        UserDefaults.standard.set(dict, forKey: UserDefaults.keys.businessInfoDic)
                        
                    
                   UserDefaults.standard.setValue("1", forKey: UserDefaults.keys.navigationVC)
                    appDelegate.showSignUpProcess()
                }else{
                    let msg = response["message"] as! String
                    objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark, on: self)
                }
            }) { error in
                objActivity.stopActivity()
                objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
            }
        }
 }
//MARK: - local data
extension ManageAccountVC{
    
    func removePropertyData(){
        let fetchRequest: NSFetchRequest<BusinessType> = BusinessType.fetchRequest()
        do {
            let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest as! NSFetchRequest<NSFetchRequestResult>)
            try managedContext?.execute(deleteRequest)
            
        } catch {
            
        }
    }
}

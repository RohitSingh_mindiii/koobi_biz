//
//  alertVC.swift
//  MualabBusiness
//
//  Created by Mac on 05/01/2018 .
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

fileprivate enum type:String {
    case noNetwork,
    error,
    banner,
    bannerDark,
    accountActive,
    sessionExpire
}

class alertVC: UIViewController {
    var alertMessage:String?
    var alertType:String?
    
    @IBOutlet weak var lblMessage:UILabel!
    @IBOutlet weak var lblNotiMessage:UILabel!
    @IBOutlet weak var lblBannerTitle:UILabel?
    @IBOutlet weak var lblTitle:UILabel!
    
    @IBOutlet weak var imgWarring:UIImageView!
    @IBOutlet weak var alertBigView:UIView!
    @IBOutlet weak var alertBannerView:UIView!

    @IBOutlet weak var btnRetry: UIButton!
    
    @IBOutlet weak var alert1Top : NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblNotiMessage.text = alertMessage ?? ""
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        objActivity.stopActivity()
        btnRetry.setTitle("Retry", for: .normal)
        showAlert()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
fileprivate extension alertVC{
    func showAlert(){
        switch self.alertType {
        case type.noNetwork.rawValue?:do{
            
            self.alertBigView.isHidden = false
            self.lblTitle.text = "Wake up your connection!"
            self.lblMessage.text = message.noNetwork
            self.imgWarring.image = UIImage.customImage.noNetwork
            btnRetry.setTitle("Try again", for: .normal)
            }
            
        case type.error.rawValue?:do{

            self.alertBigView.isHidden = false
            self.lblTitle.text = "Internal server error!"
            self.lblMessage.text = message.error.wrong
            self.imgWarring.image = UIImage.customImage.error
            btnRetry.setTitle("Try again", for: .normal)
            }
        case type.accountActive.rawValue?:do{
            
            self.alertBigView.isHidden = false
            self.lblTitle.text = "Account inactive"
            self.lblMessage.text = message.accountActive
            self.imgWarring.image = UIImage.customImage.accountActive
            btnRetry.setTitle("Contact", for: .normal)
            }
        case type.banner.rawValue?:do{
            btnRetry.setTitle("Retry", for: .normal)

            self.alertBigView.isHidden = true
            alert1Top.constant = 0
            alertBannerView.backgroundColor = appColor.withAlphaComponent(1)
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.5) {
                self.dismiss(animated: false, completion: nil)
            }
        }
        case type.bannerDark.rawValue?:do{
            btnRetry.setTitle("Retry", for: .normal)

            self.alertBigView.isHidden = true
            alert1Top.constant = 0
            alertBannerView.backgroundColor = appColor.withAlphaComponent(1)//UIColor.theameColors.darkColor.withAlphaComponent(0.8)
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.5) {
                self.dismiss(animated: false, completion: nil)
            }
            }
        case type.sessionExpire.rawValue?:do{
            
            self.alertBigView.isHidden = false
            self.lblTitle.text = "Session expired"
            self.lblMessage.text = message.sessionExpire
            self.imgWarring.image = UIImage.customImage.sessionExpire
            btnRetry.setTitle("Ok", for: .normal)

            }
        default:
            do{
                self.alertBigView.isHidden = false
                self.lblTitle.text = "Internal server error!"
                self.lblMessage.text = message.error.wrong
                self.imgWarring.image = UIImage.customImage.error
                btnRetry.setTitle("Try again", for: .normal)

            }
        }
    }
}

fileprivate extension alertVC{
    @IBAction func btnRetry(_ sender:Any){
        self.dismiss(animated: false, completion: nil)
        if self.alertType == type.sessionExpire.rawValue{
            appDelegate.logout()
        }
    }
    @IBAction func btnBack(_ sender:Any){
        self.dismiss(animated: false, completion: nil)
    }
}

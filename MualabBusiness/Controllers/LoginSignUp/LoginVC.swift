    //
    //  ViewController.swift
    //  MualabBusiness
    //
    //  Created by Mac on 21/12/2017 .
    //  Copyright © 2017 Mindiii. All rights reserved.
    //
    
    import UIKit
    import CoreData
    import Firebase
    import Fabric
    import Crashlytics
    
    class LoginVC: UIViewController,UITextFieldDelegate {
        var managedContext:NSManagedObjectContext?

        @IBOutlet weak var btnLogin : UIButton!
        @IBOutlet weak var btnShowPassword : UIButton!
        
        @IBOutlet weak var txtEmail : UITextField!
        @IBOutlet weak var txtPassword : UITextField!
        
        @IBOutlet weak var socialLoginStack : UIStackView!
        
        @IBOutlet weak var lblSignUp : UILabel!
        var strMSG = ""
        
        // live dav
        @IBOutlet weak var btnLive : UIButton!
        @IBOutlet weak var btnDav : UIButton!
        @IBAction func btnLive(_ sender:Any){
           // self.live()
        }
        @IBAction func btnDav(_ sender:Any){
          //  self.dev()
        }
        func dev(){
            self.btnLive.setTitleColor(UIColor.white, for: .normal)
            self.btnDav.setTitleColor(appColor, for: .normal)
            let BaseUrlDev =  "http://koobi.co.uk:8042/api/"
            //WebURL.BaseUrl = BaseUrlDev
            UserDefaults.standard.set(BaseUrlDev, forKey: UserDefaults.keys.baseURL)
        }
        
        func live(){
            self.btnLive.setTitleColor(appColor, for: .normal)
            self.btnDav.setTitleColor(UIColor.white, for: .normal)
            let BaseUrlLive =  "http://koobi.co.uk:3000/api/"
            //WebURL.BaseUrl = BaseUrlLive
            UserDefaults.standard.set(BaseUrlLive, forKey: UserDefaults.keys.baseURL)
        }
    }
    var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    //MARK: - View hirarchy
    extension LoginVC {
        
        override func viewDidLoad() {
            super.viewDidLoad()
            guard let appDelegate =
                UIApplication.shared.delegate as? AppDelegate else {
                    return
            }
            managedContext =
                appDelegate.persistentContainer.viewContext
            
            self.txtEmail.delegate = self
            self.txtPassword.delegate = self
            addGasturesToViews()
            // Do any additional setup after loading the view, typically from a nib.
        }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
        
        override func viewWillAppear(_ animated: Bool) {
            super.viewDidAppear(animated)
            objAppShareData.selectedTab = 0
//           let usl =  UserDefaults.standard.string(forKey: UserDefaults.keys.baseURL)
//            if usl == "http://koobi.co.uk:3000/api/"{
//                self.live()
//            }else{
//                self.dev()
//            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.configureView()
            }
            
        }
        override func viewWillDisappear(_ animated: Bool) {
            if objAppShareData.objLoginVC.strMSG != "" {
                objAppShareData.showAlert(withMessage: objAppShareData.objLoginVC.strMSG, type: alertType.banner, on: self)
                objAppShareData.objLoginVC.strMSG = ""
            }
            
        }
       
        override var preferredStatusBarStyle: UIStatusBarStyle {
            return .lightContent
        }
        
    }
    //MARK: - Local Methods
    fileprivate extension LoginVC{
        
        func configureView(){
            self.btnLogin.layer.cornerRadius = self.btnLogin.frame.size.height/2
            objAppShareData.clearUserData()
        }
        
        func addGasturesToViews() {
            let socialLoginTap = UITapGestureRecognizer(target: self, action: #selector(handleSocialLoginTap(gestureRecognizer:)))
            socialLoginStack.addGestureRecognizer(socialLoginTap)
            
            let signUpTap = UITapGestureRecognizer(target: self, action: #selector(handleSignUpTap(gestureRecognizer:)))
            lblSignUp.addGestureRecognizer(signUpTap)
        }
        
        func saveUserData(dict:[String:Any]) {
            self.removePropertyData()
            var fAddress = dict[Keys.user.address] as? String ?? ""
            
            if fAddress.range(of: dict[Keys.user.addressComponent.city] as? String ?? "") != nil{
                fAddress.removeSubrange(fAddress.range(of: dict[Keys.user.addressComponent.city] as! String)!)
            }
            if fAddress.range(of: dict[Keys.user.addressComponent.country] as? String ?? "") != nil{
                fAddress.removeSubrange(fAddress.range(of: dict[Keys.user.addressComponent.country] as! String)!)
            }
//            if fAddress.range(of: dict[Keys.user.addressComponent.postalCode] as? String ?? "") != nil{
//                fAddress.removeSubrange(fAddress.range(of: dict[Keys.user.addressComponent.postalCode] as! String)!)
//            }
            if fAddress.range(of: dict[Keys.user.addressComponent.state] as? String ?? "") != nil{
                fAddress.removeSubrange(fAddress.range(of: dict[Keys.user.addressComponent.state] as? String ?? "")!)
            }
            
            let commaSet = CharacterSet.init(charactersIn:", \n")
            fAddress = fAddress.trimmingCharacters(in:commaSet)
            let ad = Address.init(locality:fAddress,
                                  address2:"",
                                  city:dict[Keys.user.addressComponent.city] as? String ?? "",
                                  state: dict[Keys.user.addressComponent.state] as? String ?? "",
                                  //Rohit
                                 // postalCode: dict[Keys.user.addressComponent.postalCode] as? String ?? "",
                                  country:dict[Keys.user.addressComponent.country] as? String ?? "",
                                  placeName:"",
                                  fullAddress:dict[Keys.user.address] as? String ?? "",
                                  latitude: dict[Keys.user.addressComponent.latitude] as? String ?? "",
                                  longitude:dict[Keys.user.addressComponent.longitude] as? String ?? "")
            
            let userInfo = User.init(address:ad)
            //Rohit
//            if let userInf = dict[Keys.user.userId] as? Int{
//                userInfo.userId = NSNumber(value: userInf)
//            }else if let userInf = dict[Keys.user.userId] as? String{
//                userInfo.userId = NSNumber(value: Int(userInf) ?? 0)
//            }
//            if let userInf = dict["user_id"] as? String{
//               // userInfo.userId = Int(userInf)
//            }else if let userInf = dict["user_id"] as? Int{
//                //userInfo.userId = userInf
//            }
            if let userInf = dict[Keys.user.userId] as? Int{
                userInfo.userId = NSNumber(value: userInf)
            }else if let userInf = dict[Keys.user.userId] as? String{
                userInfo.userId = NSNumber(value: Int(userInf) ?? 0)
            }
            
            UserDefaults.standard.setValue(String(describing: userInfo.userId),forKey: UserDefaults.keys.userId)
            if let bankStatus = dict[Keys.user.bankStatus] as? NSNumber{
                userInfo.bankStatus = Bool(truncating: bankStatus)
            }
            // userInfo.bio = dict[Keys.user.bio] as! String
            userInfo.businessName = dict[Keys.user.businessName] as? String ?? ""
            print(userInfo.businessName)
            let archivedObject = NSKeyedArchiver.archivedData(withRootObject: userInfo)
            
//            let dicParam = ["radius":dict["radius"],
//                            "serviceType":dict["service_type"],
//                            "inCallpreprationTime":dict["in_call_prepration_time"],
//                            "outCallpreprationTime":dict["out_call_prepration_time"]] as [String : Any]
            
          //  UserDefaults.standard.setValue(dicParam,forKey: UserDefaults.keys.outCallData)
            UserDefaults.standard.setValue(archivedObject,forKey: UserDefaults.keys.userInfo)
            UserDefaults.standard.set(dict[UserDefaults.keys.authToken], forKey: UserDefaults.keys.authToken)
            UserDefaults.standard.set(dict, forKey: UserDefaults.keys.userData)

            if let bankStatus = dict["bank_status"] as? Bool{
                 UserDefaults.standard.set(bankStatus, forKey: UserDefaults.keys.bankStatus)
                UserDefaults.standard.synchronize()
            }
           
            
            
            let doc = dict["is_document"] as? Int ?? 0

            UserDefaults.standard.set(String(doc), forKey: UserDefaults.keys.isDocument)
            UserDefaults.standard.synchronize()

            if doc == 3 {
                UserDefaults.standard.set(true, forKey: UserDefaults.keys.isLoggedIn)
                UserDefaults.standard.synchronize()
                appDelegate.gotoTabBar(withAnitmation: true)
            }else{
                appDelegate.showSignUpProcess()
            }
        }
    }
    
    //MARK: - UITapGesttureActions
    fileprivate extension LoginVC{
        
        @objc func handleSocialLoginTap(gestureRecognizer: UIGestureRecognizer) {
            
            // let sb = UIStoryboard(name:"Main",bundle:Bundle.main)
            //  let objChooseType = sb.instantiateViewController(withIdentifier:"ChooseTypeVC") as! ChooseTypeVC
            // self.navigationController?.pushViewController(objChooseType, animated: true)
            
        }
        
        @objc func handleSignUpTap(gestureRecognizer: UIGestureRecognizer) {
            
            let sb = UIStoryboard(name:"Main",bundle:Bundle.main)
            let objChooseType = sb.instantiateViewController(withIdentifier:"EmailPhoneVC") as! EmailPhoneVC
            UserDefaults.standard.setValue("0", forKey: UserDefaults.keys.businessType)
            objChooseType.isBusiness = true
            self.navigationController?.pushViewController(objChooseType, animated: true)
            
        }
    }
    //MARK: - IBActions
    fileprivate extension LoginVC {
        
        @IBAction func btnLogin(_ sender:Any){
            
            //Crashlytics.sharedInstance().crash()
            
            self.view.endEditing(true)
            var invalid = false
            
            txtEmail.text = txtEmail.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            txtPassword.text = txtPassword.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            
            if txtEmail.text?.count == 0{
                invalid = true
                objAppShareData.shakeTextField(txtEmail)
            }else if txtPassword.text?.count == 0{
                invalid = true
                objAppShareData.shakeTextField(txtPassword)
            }
            if invalid {
                
            }else {
                self.view.endEditing(true)
                self.callWebserviceForLogin()
            }
        }
        @IBAction func btnClickHereForSocialAppAction(_ sender:Any){
            self.view.endEditing(true)
            if let url = URL(string: "https://apps.apple.com/us/app/koobi/id1358302019?ls=1"), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
        @IBAction func btnForgotPassword(_ sender:Any){
            self.view.endEditing(true)
            let sb = UIStoryboard(name:"Main",bundle:Bundle.main)
            let objChooseType = sb.instantiateViewController(withIdentifier:"ForgotPassVC") as! ForgotPassVC
            objChooseType.modalPresentationStyle = .overFullScreen
            self.navigationController?.pushViewController(objChooseType, animated: true)
        }
        
        @IBAction func btnShowPassword(_ sender:UIButton){
            if self.btnShowPassword.isSelected {
                self.btnShowPassword.isSelected = false
                self.txtPassword.isSecureTextEntry = true
            }else{
                self.btnShowPassword.isSelected = true
                self.txtPassword.isSecureTextEntry = false
            }
        }
    }
    //Webservices
    fileprivate extension LoginVC{
        
        func callWebserviceForLogin() {
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            objActivity.startActivityIndicator()
            let dicParam = ["user_name":self.txtEmail.text!.lowercased(),
                            "password":self.txtPassword.text!,
                          //  "device_type":"1",
                          //  "device_token":objAppShareData.firebaseToken,
                            "firebase_token":objAppShareData.firebaseToken,
                           // "userType":"artist",
                         //   "appType":"biz"
            ]
            
            objServiceManager.requestPost(strURL: WebURL.login, params: dicParam, success: { response in
                if response["status"] as! String == "success"{
                    
                    if let dictUserDetail = response["users"] as? [String : Any]{
                        self.writeUserDataToFireBase(dict: dictUserDetail)
                        var document = ""
                        
                        if let doc =  dictUserDetail["is_document"] as? String {
                        document = doc
                        }else if let  doc =  dictUserDetail["is_document"] as? Int{
                            document = String(doc)
                        }
                        let name =  dictUserDetail["business_name"] as? String ?? ""
                        UserDefaults.standard.setValue(dictUserDetail["notificationStatus"], forKey: UserDefaults.keys.notificationStatus)
                        if document != "3"{
                            if document == "0"{
                                UserDefaults.standard.setValue("1",forKey:UserDefaults.keys.navigationVC)
                            }else if document == "1" && name == ""{
                                UserDefaults.standard.setValue("2",forKey:UserDefaults.keys.navigationVC)
                            }else if document == "1" && name != ""{
                                UserDefaults.standard.setValue("3",forKey:UserDefaults.keys.navigationVC)
                            }else if document == "2"{
                                UserDefaults.standard.setValue("4",forKey:UserDefaults.keys.navigationVC)
                            }
                        }
                        //MARK:- Save User Data in UserDefault
                        //Rohit = 07/Feb/2020
                        objAppShareData.objAppdelegate.saveUserInfoDetail(dictOfInfo: dictUserDetail)
                        //objAppShareData.objAppdelegate.parseBusinessSignUpDetail(dictOfInfo: dictUserDetail)
                        
                        let busuinesstype = dictUserDetail["business_type"] as? String ?? ""
                        UserDefaults.standard.setValue(busuinesstype,forKey: UserDefaults.keys.businessNameType)
                        //Rohit
                        self.saveUserData(dict: dictUserDetail)
                        
                        let strToken = dictUserDetail["auth_token"] as? String ?? ""
                        UserDefaults.standard.set(strToken, forKey: UserDefaults.keys.authToken)
                       
                        //
                        UserDefaults.standard.set(dictUserDetail, forKey: UserDefaults.keys.userInfoDic)
                        //
                        var myId = ""
                        if let doc = dictUserDetail["_id"] as? String {
                            myId = doc
                        }else if let  doc =  dictUserDetail["_id"] as? Int{
                            myId = String(doc)
                        }
                        UserDefaults.standard.set(myId, forKey: UserDefaults.keys.newAdminId)
                        UserDefaults.standard.set(true, forKey:  UserDefaults.keys.isLoggedIn)
                        if let id = dictUserDetail["_id"] as? String{
                            UserDefaults.standard.set(id, forKey:  UserDefaults.keys.myId)
                        }else if let id = dictUserDetail["_id"] as? Int{
                            UserDefaults.standard.set(String(id), forKey:  UserDefaults.keys.myId)
                        }
                        UserDefaults.standard.set(dictUserDetail["user_name"] as? String ?? "", forKey:  UserDefaults.keys.myName)
                        UserDefaults.standard.set(dictUserDetail["profile_image"] as? String ?? "", forKey:  UserDefaults.keys.myImage)
                                            
                        UserDefaults.standard.synchronize()
                        strAuthToken = UserDefaults.standard.string(forKey:UserDefaults.keys.authToken)!
                    }else{
                    }
                }else{
                    let msg = response["message"] as! String
                    objAppShareData.showAlert(withMessage: msg, type: alertType.banner, on: self)
                }
            }) { error in
                //objAppShareData.showDeafultAlert(withTitle: message.error.title, message: message.error.wrong, on: self)
                objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
            }
        }
    }
    //MARK: - UITextField Delegate
    extension LoginVC {
        
        func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
            return true
        }
        
        
        func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            if textField == self.txtEmail{
                txtPassword.becomeFirstResponder()
            }else if textField == self.txtPassword{
                txtPassword.resignFirstResponder()
            }
            return true
        }
    }
    ///////
    
    //MARK: - local data
    extension LoginVC{
        
        func removePropertyData(){
            let fetchRequest: NSFetchRequest<BusinessType> = BusinessType.fetchRequest()
            do {
                let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest as! NSFetchRequest<NSFetchRequestResult>)
                try managedContext?.execute(deleteRequest)
            } catch {
            }
        }
    }

    
    //MARK: - FireBaseRegistration Method
    extension LoginVC {
        
        func writeUserDataToFireBase(dict:[String : Any]){
            let calendarDate = ServerValue.timestamp()
            
            var id = 0
            if let ids = dict["_id"] as? Int{
                id = ids
            }else if let ids = dict["_id"] as? String{
                id = Int(ids)!
            }
            
            var img = dict["profileImage"] as? String ?? ""
            if (dict["profileImage"] as? String ?? "") != ""{
            }else{
                img = "http://koobi.co.uk:3000/uploads/default_user.png"
            }
            var strToken = ""
            if let str = dict["notificationStatus"] as? String{
                if str == "0"{
                    strToken = ""
                }else{
                    strToken = dict["firebaseToken"] as! String
                }
            }
            if let str = dict["notificationStatus"] as? Int{
                if str == 0{
                    //notificationStatus
                    strToken = ""
                }else{
                    strToken = dict["firebaseToken"] as! String
                }
            }
            ////
            //strToken = ""
            //strToken = dict["firebaseToken"] as! String
            ////
            let dict1 = ["firebaseToken":strToken,
                         "isOnline":checkForNULL(obj:1),
                         "lastActivity":checkForNULL(obj:calendarDate),
                         "profilePic":checkForNULL(obj:img),
                         "uId":checkForNULL(obj:id),
                         "userName":checkForNULL(obj:dict["userName"]) ]
  
            let ref = Database.database().reference()
            let myId:String = String(dict["_id"] as? Int ?? 0000000)
            ref.child("users").child(myId).setValue(dict1)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                objWebserviceManager.StopIndicator()
            }
    }
    }


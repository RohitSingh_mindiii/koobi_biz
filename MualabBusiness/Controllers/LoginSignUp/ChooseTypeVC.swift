//
//  ChooseTypeVC.swift
//  MualabBusiness
//
//  Created by Mac on 22/12/2017 .
//  Copyright © 2017 Mindiii. All rights reserved.
//

import UIKit

class ChooseTypeVC: UIViewController {
    @IBOutlet weak var btnIndependent : UIButton!
    @IBOutlet weak var btnBusiness : UIButton!
    
}
//MARK: - View Hirarchy
extension ChooseTypeVC{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.configureView()
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

//MARK: - Local Methods
fileprivate extension ChooseTypeVC{
    func configureView(){
        self.btnIndependent.layer.cornerRadius = self.btnIndependent.frame.size.height/2
        self.btnBusiness.layer.cornerRadius = self.btnBusiness.frame.size.height/2
    }
}
//MARK: - IBActions
fileprivate extension ChooseTypeVC{
    
    @IBAction func btnBackAction(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnChooseType(_ sender:UIButton){
        
        let sb = UIStoryboard(name:"Main",bundle:Bundle.main)
        let objEmailPhoneVC = sb.instantiateViewController(withIdentifier:"EmailPhoneVC") as! EmailPhoneVC
        if sender.tag == 0{
            UserDefaults.standard.setValue("1", forKey: UserDefaults.keys.businessType)
            objEmailPhoneVC.isBusiness = false
        }else{
            UserDefaults.standard.setValue("0", forKey: UserDefaults.keys.businessType)
            objEmailPhoneVC.isBusiness = true
        }
        self.navigationController?.pushViewController(objEmailPhoneVC, animated: true)
    }
}

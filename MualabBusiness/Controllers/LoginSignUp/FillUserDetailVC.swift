//
//  FillUserDetailVC.swift
//  MualabBusiness
//
//  Created by Mac on 23/12/2017 .
//  Copyright © 2017 Mindiii. All rights reserved.
//

import UIKit

class FillUserDetailVC: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var imgProfile : UIImageView!
    @IBOutlet weak var imgCam : UIImageView!
    @IBOutlet weak var profileBackView : UIView!
    ////
    @IBOutlet weak var viewScreenShot : UIView!
    @IBOutlet weak var lblScreenShot : UILabel!
    ////
    fileprivate var userImage:UIImage?
    fileprivate var strDOBForServer: String?

    @IBOutlet weak var lblLogin:UILabel?
    
    @IBOutlet weak var btnContinue : UIButton!
    
    @IBOutlet weak var txtFirstName : UITextField!
    @IBOutlet weak var txtLastName : UITextField!
    @IBOutlet weak var txtDOB : UITextField!
    @IBOutlet weak var txtUserName : UITextField!

    @IBOutlet weak var DOBPicker : UIDatePicker!
    @IBOutlet weak var viewDOB : UIView!

}
//MARK: - View Hirarchy
extension FillUserDetailVC {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.txtFirstName.delegate = self
        self.txtLastName.delegate = self
        self.txtDOB.delegate = self
        self.txtUserName.delegate = self
        self.viewDOB.isHidden = true
        self.DOBPicker.maximumDate = Date()

        addGasturesToViews()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //self.DOBPicker.date = Date()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.configureView()
        }
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
//MARK: - Local Methods
fileprivate extension FillUserDetailVC{
    func configureView(){
        self.btnContinue.layer.cornerRadius = self.btnContinue.frame.size.height/2
        self.imgProfile.layer.cornerRadius = self.imgProfile.frame.size.width/2
        self.profileBackView.layer.cornerRadius = self.profileBackView.frame.size.width/2
    }
    
    func addGasturesToViews() {
        
        let loginTap = UITapGestureRecognizer(target: self, action: #selector(handleLoginTap(gestureRecognizer:)))
        lblLogin?.addGestureRecognizer(loginTap)
        
        let imgCamTap = UITapGestureRecognizer(target: self, action: #selector(handleImgCamTap(gestureRecognizer:)))
        imgCam.addGestureRecognizer(imgCamTap)
    }
    func selectProfileImage() {
        let selectImage = UIAlertController(title: "Select Profile Image", message: nil, preferredStyle: .actionSheet)
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        let btn0 = UIAlertAction(title: "Cancel", style: .cancel, handler: {(_ action: UIAlertAction) -> Void in
        })
        let btn1 = UIAlertAction(title: "Camera", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                imagePicker.sourceType = .camera
                imagePicker.showsCameraControls = true
                imagePicker.allowsEditing = true;
                imagePicker.modalPresentationStyle = .fullScreen
                self.present(imagePicker, animated: true)
            }
        })
        let btn2 = UIAlertAction(title: "Photo Library", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
                imagePicker.sourceType = .photoLibrary
                imagePicker.allowsEditing = true;
                imagePicker.modalPresentationStyle = .fullScreen
                self.present(imagePicker, animated: true)
            }
        })
        selectImage.addAction(btn0)
        selectImage.addAction(btn1)
        selectImage.addAction(btn2)
        present(selectImage, animated: true)
    }
    func saveUserData(){
        let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
        let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! User
        
        userInfo.firstName = self.txtFirstName.text!
        userInfo.lastName = self.txtLastName.text!
        userInfo.DOB = objAppShareData.dateFormat2(forAPI: self.DOBPicker.date)
        userInfo.userName = self.txtUserName.text!
        
        
        let archivedObject = NSKeyedArchiver.archivedData(withRootObject: userInfo)
        UserDefaults.standard.setValue(archivedObject,forKey: UserDefaults.keys.userInfo)
        UserDefaults.standard.synchronize()
        self.callWebserviceForCheckUserName()
    }
    func GotoSubmitPasswordVC(){
        let sb = UIStoryboard(name:"Main",bundle:Bundle.main)
        let objVC = sb.instantiateViewController(withIdentifier:"SubmitPasswordVC") as! SubmitPasswordVC
        if let img = self.userImage{
            objVC.userImage = img
        }
        self.navigationController?.pushViewController(objVC, animated: true)
    }
}

//MARK: - UITapGestureActions
fileprivate extension FillUserDetailVC{
    
    @objc func handleLoginTap(gestureRecognizer: UIGestureRecognizer) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @objc func handleImgCamTap(gestureRecognizer: UIGestureRecognizer) {
        selectProfileImage()
    }
    @IBAction func btnImageAction(_ sender:Any){
        selectProfileImage()
    }
}
//MARK: - IBAction
fileprivate extension FillUserDetailVC{
    
    @IBAction func btnSelectDOB(_ sender:Any){
        self.view.endEditing(true)
        self.viewDOB.isHidden = false
    }
    
    @IBAction func btnDoneCancleDOBSelection(_ sender:UIButton){
        if sender.tag == 1 { 
            self.txtDOB.text = objAppShareData.dateFormatAccourdingToMe(forAPI: self.DOBPicker.date,formet:"dd/MM/yyyy")
            //self.txtDOB.text = objAppShareData.dateFormatToShow(forAPI: self.DOBPicker.date)
            strDOBForServer = objAppShareData.dateFormat2(forAPI: self.DOBPicker.date)
            
        }
        if self.txtDOB.text != ""{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
            dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
            dateFormatter.timeZone = NSTimeZone(abbreviation: "UTC") as TimeZone?
            let dataDate = dateFormatter.date(from: self.txtDOB.text!+" 12:00:00" as! String)!
            self.DOBPicker.date = dataDate
        }
         self.viewDOB.isHidden = true
    }
    func makeDefaultUserImage(){
        //self.userImage
        self.lblScreenShot.text = self.txtUserName.text!.getAcronyms(separator: "").uppercased();
        self.viewScreenShot.clipsToBounds = true
        self.userImage = self.viewScreenShot.takeScreenshot()
        print("test")
    }
    
    @IBAction func btnContinue(_ sender:Any){
        var invalid = false
        var strMessage = ""
        
        txtFirstName.text = txtFirstName.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        txtLastName.text = txtLastName.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        txtDOB.text = txtDOB.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        txtUserName.text = txtUserName.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        if self.txtFirstName.text?.count == 0{
            invalid = true
            objAppShareData.shakeViewField(self.txtFirstName)
        }else if self.txtLastName.text?.count == 0{
            invalid = true
            objAppShareData.shakeViewField(self.txtLastName)
        }else if self.txtDOB.text?.count == 0{
            invalid = true
            objAppShareData.shakeViewField(self.txtDOB)
        }else if self.txtUserName.text?.count == 0{
            invalid = true
            objAppShareData.shakeViewField(self.txtUserName)
        }else if (self.txtDOB.text?.count)! < 4{
            invalid = true
            strMessage = message.validation.dob
        }else if (self.txtUserName.text?.count)! < 4{
            invalid = true
            strMessage = message.validation.userNameLength
        }else if self.txtUserName.text?.range(of:" ") != nil{
            invalid = true
            strMessage = message.validation.userNameSpace
        }
        if invalid && strMessage.count>0 {
            objAppShareData.showAlert(withMessage: strMessage, type: alertType.banner, on: self)
        }else if !invalid{
            self.view.endEditing(true)
            if self.imgProfile.image == UIImage.init(named: "user_icon"){
                self.makeDefaultUserImage()
            }else{
            }
            self.saveUserData()
        }
    }
    @IBAction func btnBackAction(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    
}
//MARK: - UIImagePickerDelegate
extension FillUserDetailVC:UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let pickedImage = info[UIImagePickerController.InfoKey.editedImage] as! UIImage
        self.userImage = pickedImage
        imgProfile.image = pickedImage
        picker.dismiss(animated: true)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
      //  isFromImagePicker = true
        dismiss(animated: true)
    }
}

//MARK: - UITextField Delegate
extension FillUserDetailVC{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        
        if textField == self.txtFirstName || textField == self.txtLastName{
            
            if let range = string.rangeOfCharacter(from: NSCharacterSet.letters){
                return true
            } else if let range = string.rangeOfCharacter(from: NSCharacterSet.whitespaces){
                return true
            } else if string == "" || string == "."{
                return true
            }else {
                return false
            }
            
        }
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.txtFirstName{
            txtLastName.becomeFirstResponder()
        }else if textField == self.txtLastName{
            txtUserName.becomeFirstResponder()
        }else if textField == self.txtUserName{
            txtUserName.resignFirstResponder()
        }
        return true
    }
    
}
//MARK: - Webservices
fileprivate extension FillUserDetailVC{
    
    func callWebserviceForCheckUserName(){
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        objActivity.startActivityIndicator()
        let dicParam = ["user_name":self.txtUserName.text!.lowercased()]
        
        objServiceManager.requestPost(strURL: WebURL.checkUser, params: dicParam, success: { response in
            if response["status"] as! String == "success"{
                self.GotoSubmitPasswordVC()
            }else{
                //let msg = response["message"] as! String
                objAppShareData.showAlert(withMessage: "That username is already being used", type: alertType.banner, on: self)
            }
        }) { error in
            
            //objAppShareData.showDeafultAlert(withTitle: message.error.title, message: message.error.wrong, on: self)
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
}

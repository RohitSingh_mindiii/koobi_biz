//
//  ForgotPassVC.swift
//  MualabCustomer
//
//  Created by Mac on 13/11/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit


class ForgotPassVC: UIViewController {
    @IBOutlet weak var btnContinue : UIButton!
    @IBOutlet weak var imgLogo : UIImageView!
    @IBOutlet weak var viewUsername:UIView!
    @IBOutlet weak var lblHintText:UILabel!
    @IBOutlet weak var txtUsername : UITextField!
 }

//MARK: - View Hirarchy
extension ForgotPassVC {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.txtUsername.delegate = self
        self.configureView()
      }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.configureView()
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

//MARK: - Local Methods
fileprivate extension ForgotPassVC{
      func configureView(){
         self.btnContinue.layer.cornerRadius = self.btnContinue.frame.size.height/2
        self.imgLogo.layer.cornerRadius = self.imgLogo.frame.size.height/2
    }
 }
//MARK:- IBActions
fileprivate extension ForgotPassVC{
 
    @IBAction func btnBack(_ sender:Any){
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnSignin(_ sender:Any){
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnContinue(_ sender:Any){
        self.view.endEditing(true)
          txtUsername.text = txtUsername.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
             if self.txtUsername.text?.count == 0{
                objAppShareData.shakeTextField(self.txtUsername)
            }else{
                self.callWebServiceForgotPassword()
            }
     }
 }



//MARK: - UITableView Delegate and Datasource
extension ForgotPassVC:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.txtUsername{
            self.txtUsername.resignFirstResponder()
        }
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        
        if !string.canBeConverted(to: String.Encoding.ascii){
            return false
        }
         UIView.animate(withDuration: 0.4) {
            self.view.layoutIfNeeded()
        }
        return true
    }
}


//MARK: - callWebServiceForgotPassword
extension ForgotPassVC{
    func callWebServiceForgotPassword(){
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        objActivity.startActivityIndicator()
        let dicParam = ["email":self.txtUsername.text!.lowercased()]
        
        objServiceManager.requestPost(strURL: webUrl.ForgotPassword, params: dicParam, success: { response in
            objActivity.stopActivity()
            if response["status"] as! String == "success"{
                let msg1 = response["message"] as? String ?? ""
                objAppShareData.showAlert(withMessage: msg1, type: alertType.bannerDark, on: self)
                //DispatchQueue.main.asyncAfter(deadline: .now() + 3) { // in half a second...
                    self.navigationController?.popViewController(animated: true)
                //}
            }else{
                if  let msg = response["message"] as? String{
                objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark, on: self)
                }else{
                    objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
                }
            }
        }) { error in
            objActivity.stopActivity()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
}


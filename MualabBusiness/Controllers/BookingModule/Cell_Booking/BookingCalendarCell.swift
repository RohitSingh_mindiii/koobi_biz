//
//  BookingCalendarCell.swift
//  MualabCustomer
//
//  Created by Mac on 14/02/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

class BookingCalendarCell: UITableViewCell {
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgUserProfile: UIImageView!
    @IBOutlet weak var lblServiceName: UILabel!
    @IBOutlet weak var lblArtistName: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblBookingStatus: UILabel!
    @IBOutlet weak var lblDateAndTime: UILabel!
    @IBOutlet weak var lblOnlyDate: UILabel!

    @IBOutlet weak var btnViewMore: UIButton!
    @IBOutlet weak var btnAccept: UIButton!
    @IBOutlet weak var btnReject: UIButton!

    @IBOutlet weak var stackExpande: UIStackView!
    @IBOutlet weak var imgViewMoreDrop: UIImageView!
    @IBOutlet weak var lblBookingType: UILabel!
    @IBOutlet weak var lblServiceType: UILabel!
    @IBOutlet weak var lblTotalPrice: UILabel!
    @IBOutlet weak var lblPaymentStatus: UILabel!

    @IBOutlet weak var btnStartService: UIButton!
    @IBOutlet weak var btnEndServices: UIButton!

    var indexPath:IndexPath!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func cellLoadInBG(objAllBooking:Booking,fromBookingRequest:Bool){
        let objUser = objAllBooking.arrUserDetail
        let objBooking = objAllBooking.arrBookingInfo
        
        self.imgUserProfile.image = #imageLiteral(resourceName: "cellBackground")
        self.lblTime.text = objAllBooking.creationTime
        self.lblDate.text = objAllBooking.creationDate
        
        if objBooking.count > 0{
            let objBookingInfo = objBooking[0]
            self.lblServiceName.text = objBookingInfo.artistServiceName
            
            let formatter  = DateFormatter()
            formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
            formatter.dateFormat = "yyyy-MM-dd"
            let date = formatter.date(from: objBookingInfo.bookingDate)
            formatter.dateFormat = "dd/MM/yyyy"
            let strDate = formatter.string(from: date!)
            if fromBookingRequest == true{
            self.lblDateAndTime .text = strDate+", "+objBookingInfo.startTime.lowercased()+" - "+objBookingInfo.endTime.lowercased()
            }else{
            self.lblDateAndTime .text = objBookingInfo.startTime.lowercased()+" - "+objBookingInfo.endTime.lowercased()
            }
            if fromBookingRequest == false{
            self.lblOnlyDate .text = strDate
            }
            
            self.lblArtistName.text =  objBookingInfo.staffName

            if fromBookingRequest == true{
                self.btnViewMore.isHidden = true
            }else{
            self.btnViewMore.isHidden = false
            }
            var arrStaffName = [String]()
            
            for objName in objBooking{
                var add = true
                for aa in arrStaffName{
                    if aa == objName.staffName{
                        add = false
                    }
                }
                if add == true{
                    arrStaffName.append(objName.staffName)
                }
            }
            
            if arrStaffName.count  > 0 {
                self.lblArtistName.text = arrStaffName.joined(separator: ", ")
                
                if arrStaffName.count  > 2{
                    self.btnViewMore.isHidden = false
                    
                    if arrStaffName.count > 2{
                        var arr =  [String]()
                        if fromBookingRequest == true{
                        for i in 0...1{
                            arr.append(arrStaffName[i])
                        }
                        self.lblArtistName.text = arr.joined(separator: ", ")
                        }else{
                            self.lblArtistName.text = arr.joined(separator: ", ")
                        }
                    }else{
                        if arrStaffName.count > 0{
                         self.lblArtistName.text = arrStaffName[0]
                        }
                    }
                }
            }
        }
      
        
        if objAllBooking.customerType == "walking"{
            if objAllBooking.arrClientInfo.count > 0 {
            self.lblName.text = objAllBooking.arrClientInfo[0].firstName+" "+objAllBooking.arrClientInfo[0].lastName
            }
            self.imgUserProfile.image = #imageLiteral(resourceName: "cellBackground")
        }else{
            if objUser.count >= 1 {
                let objUserInfo = objUser[0]
                let url = URL(string: objUserInfo.profileImage)
                if  url != nil {
                    //self.imgUserProfile.af_setImage(withURL: url!)
                    self.imgUserProfile.sd_setImage(with: url!, placeholderImage: #imageLiteral(resourceName: "cellBackground"))
                }else{
                    self.imgUserProfile.image = #imageLiteral(resourceName: "cellBackground")
                }
                self.lblName.text = objUserInfo.userName
            }
        }
        
        if fromBookingRequest == true{
        self.btnAccept.isHidden = true
        self.btnReject.isHidden = true
        }
        if objAllBooking.bookStatus == "1"{
            self.lblBookingStatus.text = "Confirmed"
            self.lblBookingStatus.textColor =  #colorLiteral(red: 0, green: 0.7333333333, blue: 0.1529411765, alpha: 1)
        }else if objAllBooking.bookStatus == "2" {
            self.lblBookingStatus.text = "Cancelled"
            self.lblBookingStatus.textColor =  #colorLiteral(red: 0.9607843137, green: 0.01568627451, blue: 0.01568627451, alpha: 1)
        }else if objAllBooking.bookStatus == "3" {
            self.lblBookingStatus.text = "Completed"
            self.lblBookingStatus.textColor =  #colorLiteral(red: 0, green: 0.7333333333, blue: 0.1529411765, alpha: 1)
        }else if objAllBooking.bookStatus == "0"{
            self.lblBookingStatus.text = "Pending"
            self.lblBookingStatus.textColor =  #colorLiteral(red: 0.9200255673, green: 0.4889633489, blue: 0.08423942367, alpha: 1)
        }else if objAllBooking.bookStatus == "5"{
            self.lblBookingStatus.text = "In progress"
            self.lblBookingStatus.textColor =  #colorLiteral(red: 0, green: 0.7333333333, blue: 0.1529411765, alpha: 1)
        }
    }
    
}
class BookingCalendarStaffListCell: UITableViewCell {
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgUserProfile: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}



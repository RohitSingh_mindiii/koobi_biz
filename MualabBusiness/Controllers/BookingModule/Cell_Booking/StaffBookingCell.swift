//
//  StaffBookingCell.swift
//  MualabBusiness
//
//  Created by mac on 19/03/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

class StaffBookingCell: UICollectionViewCell {
    @IBOutlet weak var imgUserImage: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var btnService: UIButton!
    
}

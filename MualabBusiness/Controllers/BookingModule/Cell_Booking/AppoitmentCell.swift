//
//  AppoitmentCell.swift
//  MualabBusiness
//
//  Created by mac on 24/03/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//
import UIKit


class AppoitmentCell: UITableViewCell  {
    
    @IBOutlet weak var CellBG: UIView!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblServiceName: UILabel!
    @IBOutlet weak var btnChangeStaff: UIButton!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    
    var arrCellDetail = [BookingInfo]()
    var isOpen = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        CellBG.layer.shadowOffset = CGSize(width: -1, height: 1)
        CellBG.layer.shadowOpacity = 0.1
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}

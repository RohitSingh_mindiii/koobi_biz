//
//  BookingPendingCell.swift
//  MualabBusiness
//
//  Created by Mac on 16/03/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

protocol BookingPendingCellDelegate {
    func btnAcceptTapped(at index:IndexPath)
    func btnRejectTapped(at index:IndexPath)
    func btnCounterTapped(at index:IndexPath)
}

class BookingPendingCell: UITableViewCell {

    @IBOutlet weak var topPendingName: NSLayoutConstraint!
    
    @IBOutlet weak var heightPendingCellBottumView: NSLayoutConstraint!
    @IBOutlet weak var viewBottumePendingCell: UIView!
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgUserProfile: UIImageView!
    @IBOutlet weak var lblServiceName: UILabel!
    @IBOutlet weak var lblArtistName: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblDateAndTime: UILabel!

    //@IBOutlet weak var lblBookingStatus: UILabel!
    
    var delegate:BookingPendingCellDelegate!
    var indexPath:IndexPath!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
     }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    @IBAction func btnAcceptAction(_ sender: Any) {
        self.delegate?.btnAcceptTapped(at: indexPath)
    }
    @IBAction func btnRejectAction(_ sender: Any) {
        self.delegate?.btnRejectTapped(at: indexPath)
    }
    @IBAction func btnCounterAction(_ sender: Any) {
        self.delegate?.btnCounterTapped(at: indexPath)
    }
}

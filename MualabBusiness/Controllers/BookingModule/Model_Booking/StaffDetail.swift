

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class StaffDetail {
    public var staffId = ""
    public var satffName = ""
    public var staffImage = ""
    public var job = ""

    init(dict: [String : Any]) {
        staffId = String(dict["staffId"] as? Int ?? 0)
        if let status = dict["staffId"] as? String{
            staffId = status
        }
        satffName = dict["staffName"] as? String ?? ""
        staffImage = dict["staffImage"] as? String ?? ""
        job = dict["job"] as? String ?? ""
        
    }
}


public class ServiceStaffDetail {
    public var _id = ""
    public var satffName = ""
    public var staffImage = ""
    public var completionTime = ""
    public var bookingType = ""
    public var inCallPrice = ""
    public var outCallPrice = ""
    public var staffServicePrimaryID = 0

    

    init(dict: [String : Any]) {
        _id = String(dict["staffId"] as? Int ?? 0)
        if let status = dict["staffId"] as? String{
            _id = status
        }
        satffName = dict["staffName"] as? String ?? ""
        staffImage = dict["staffImage"] as? String ?? ""
        bookingType = dict["bookingType"] as? String ?? ""
        
        completionTime = dict["completionTime"] as? String ?? ""
        inCallPrice = dict["inCallPrice"] as? String ?? ""
        if let status = dict["inCallPrice"] as? Int{
            inCallPrice = String(status)
        }
        outCallPrice = dict["outCallPrice"] as? String ?? ""
        if let status = dict["outCallPrice"] as? Int{
            outCallPrice = String(status)
        }
        if let status = dict["staffServicePrimaryID"] as? Int{
            staffServicePrimaryID = status
        }
    }
}



public class ClientInfo {
    public var _id = ""
    public var firstName = ""
    public var lastName = ""
    public var phone = ""
    
    init(dict: [String : Any]) {
        _id = String(dict["_id"] as? Int ?? 0)
        if let status = dict["_id"] as? String{
            _id = status
        }
        firstName = dict["firstName"] as? String ?? ""
        lastName = dict["lastName"] as? String ?? ""
        phone = String(dict["phone"] as? Int ?? 0)
        if let status = dict["phone"] as? String{
            phone = status
        }
    }
}


public class CompanyInfo {
    public var _id = ""
    public var address = ""
    public var artistId = ""
    public var businessId = ""
    public var businessName = ""
    public var profileImage = ""
    public var userName = ""
    public var holiday = ""
    public var jobTitle = ""
    public var mediaAccess = ""
    public var serviceType = ""
    public var salary = ""
    public var message = ""

    public var arrBusinessType = [String]()
    var staffHours : [AddStaffHours] = [AddStaffHours]()
    var staffService : [AddStaffStaffService] = [AddStaffStaffService]()

    init(dict: [String : Any]) {
        
        if let _id1 = dict["_id"] as? String{
            _id = _id1
        }else if let _id1 = dict["_id"] as? Int{
            _id = String(_id1)
        }
       
        if let _id1 = dict["message"] as? String{
            message = _id1
        }else if let _id1 = dict["message"] as? Int{
            message = String(_id1)
        }
        
        if let _id1 = dict["salaries"] as? String{
            salary = _id1
        }else if let _id1 = dict["salaries"] as? Int{
            salary = String(_id1)
        }
        if let artistId1 = dict["artistId"] as? String{
            artistId = artistId1
        }else if let artistId1 = dict["artistId"] as? Int{
            artistId = String(artistId1)
        }
        
        if let businessId1 = dict["businessId"] as? String{
            businessId = businessId1
        }else if let businessId1 = dict["businessId"] as? Int{
            businessId = String(businessId1)
        }
        
        if let holiday1 = dict["holiday"] as? String{
            holiday = holiday1
        }else if let holiday1 = dict["holiday"] as? Int{
            holiday = String(holiday1)
        }
        
        userName = dict["userName"] as? String ?? ""
        businessName = dict["businessName"] as? String ?? ""
        profileImage = dict["profileImage"] as? String ?? ""
        address = dict["address"] as? String ?? ""
        
        jobTitle = dict["job"] as? String ?? ""
        mediaAccess = dict["mediaAccess"] as? String ?? ""
        
        if let arr = dict["businessType"] as? [[String : Any]]{
            for dict in arr{
                let objSubService = dict["serviceName"] as? String ?? ""
                self.arrBusinessType.append(objSubService)
            }
        }
        
        if let arr = dict["staffHours"] as? [[String : Any]]{
            for dict in arr{
                let objSubService = AddStaffHours.init(dict: dict)
                self.staffHours.append(objSubService)
            }
        }
        if let arr = dict["staffService"] as? [[String : Any]]{
            for dict in arr{
                let objSubService = AddStaffStaffService.init(dict: dict)
                self.staffService.append(objSubService)
            }
        }
        var arrBookingType = [String]()
            for obj in self.staffService{
                var add = true
                for obj1 in arrBookingType{
                    if obj1 == obj.serviceType{
                        add = false
                    }
                }
                if add == true{
                    arrBookingType.append(obj.serviceType)
                }
            }
        serviceType = "Both"

        if arrBookingType.count > 0 {
        if arrBookingType.count > 1{
            serviceType = "Both"
        }else {
            serviceType = arrBookingType[0]
        }
        }
        print("serviceType = ",serviceType)
        
    }
}

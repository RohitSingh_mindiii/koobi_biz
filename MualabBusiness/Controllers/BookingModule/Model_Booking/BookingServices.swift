//
//  BookingServices.swift
//  MualabCustomer
//
//  Created by Mac on 09/03/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import Foundation

class BookingServices {
    var bookingId : Int = 0
    var serviceId : Int = 0
    var subServiceId : Int = 0
    var subSubServiceId : Int = 0
    
    var serviceName : String = ""
    var subServiceName : String = ""
    var subSubServiceName : String = ""
    
    var serviceType : String = "" //inCall = 1, outCall = 2
    var bookingDateForServer : String = ""
    var startTime : String = ""
    var endTime : String = ""
    
    var price : Double = 0
    var completionTime : String = ""
    var isOutcall : Bool = false
    var bookingDate : String = ""
    var bookingTime : String = ""
    var isBooked : Bool = false
    var isFromEdit : Bool = false

     var objSubService: SubServiceAddStaff = SubServiceAddStaff(dict: ["":""])
   
    var companyId : String = ""
    var companyImage : String = ""
    var companyName : String = ""
    var bookingType : String = ""
    var customerType : String = ""
    var discountPrice : String = ""
    var location : String = ""
    var paymentStatus : String = ""
    var paymentType : String = ""
    var timeCount : String = ""
    var totalPrice : String = ""
    var bookStatus : String = ""
    var artistName : String = ""
    var artistImage : String = ""
    var userName : String = ""
    var userImage : String = ""
    var userId : String = ""

 
    var mergedBookingDate : Date?
    var strPrice : String = ""
    var isAddedToArray : Bool = false
    var staffName : String = ""
    var staffImage : String = ""
    var addressForBooking : String = ""
    var staffId : Int = 0
    var bookStaffId : Int = 0
     var objSubSubService: SubSubService = SubSubService(dict: ["":""])
}



class SubSubService {
    var artistId : Int = 0
    var serviceId : Int = 0
    var serviceName : String = ""
    
    var subServiceId : Int = 0
    var subServiceName : String = ""
    var subServiceDescription : String = ""
    
    var subSubServiceId : Int = 0
    var subSubServiceName : String = ""
    var isStaff : Int = 0
    var isInCallStaff : Int = 0
    var isOutCallStaff : Int = 0
    
    var inCallPrice : Double = 0
    var outCallPrice : Double = 0
    var completionTime : String = ""
    var isSelected = false
    
    ////Optional Properties
    var staffIdForEdit : Int = 0
    var strDateForEdit : String = ""
    var strSlotForEdit : String = ""
    ////
    
    init(dict : [String : Any]){
        serviceId = dict["serviceId"] as? Int ?? 0
        subServiceId = dict["subServiceId"] as? Int ?? 0
        subSubServiceId = dict["_id"] as? Int ?? 0
        subSubServiceName = dict["title"] as? String ?? ""
        inCallPrice = dict["inCallPrice"] as? Double ?? 0
        outCallPrice = dict["outCallPrice"] as? Double ?? 0
        completionTime = dict["completionTime"] as? String ?? ""
        subServiceDescription = dict["description"] as? String ?? ""
        isStaff = dict["isStaff"] as? Int ?? 0
        isInCallStaff = dict["incallStaff"] as? Int ?? 0
        isOutCallStaff = dict["outcallStaff"] as? Int ?? 0
    }
}
/*
 http://koobi.co.uk:3000/api/bookArtist
 
 data['serviceId'] = req.body.serviceId;
 data['subServiceId'] = req.body.subServiceId;
 data['artistServiceId'] = req.body.artistServiceId;
 
 data['artistId'] = req.body.artistId;
 data['staff'] = req.body.staff; // artistId
 
 data['serviceType'] = req.body.serviceType; // inCall = 1, outCall = 2
 data['bookingDate'] = req.body.bookingDate;//2018-03-14
 data['startTime'] = req.body.startTime; 10:00 AM
 data['endTime'] = req.body.endTime;     10:50 AM // add service time + prepration
 data['userId'] = req.body.userId;

*/

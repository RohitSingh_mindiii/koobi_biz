

import Foundation

public class UserDetail {
   
    public var _id = ""
    public var userName = ""
    public var profileImage = ""
    public var ratingCount = ""
    public var countryCode = ""
    public var contactNo = ""

    init(dict: [String : Any]) {
        _id = String(dict["_id"] as? Int ?? 0)
        if let status = dict["_id"] as? String{
            _id = status
        }
        
        userName = dict["user_name"] as? String ?? ""
        profileImage = dict["profile_image"] as? String ?? ""
        countryCode = dict["country_code"] as? String ?? ""
        ratingCount = String(dict["rating_count"] as? Int ?? 0)
        contactNo = String(dict["contact_no"] as? String ?? "0000000000")
    }
}

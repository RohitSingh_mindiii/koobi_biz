import Foundation

public class Booking : NSObject {
    
    var strStaff = ""
    
    var strValue = ""
    public var _id = ""
    public var artistId = ""
    public var userId = ""

    public var cellIndexPath:Int = 0
    public var bookingDate = ""
    public var bookingTime = ""
    public var bookStatus = ""
    public var paymentType = ""
    public var transactionId = ""
    public var paymentStatus = ""
    public var totalPrice = ""
    public var location = ""
    
    public var allSubServiceName = ""
    public var allServiceId = ""
    public var allSubServiceId = ""
    public var allArtictServiceId = ""
    
    public var isFinsh = ""
    public var creationDate = ""
    public var creationTime = ""
    public var bookingType = ""
    public var latitude = ""
    public var longitude = ""
    public var customerType = ""
    public var reviewByArtist = ""
    public var artistRating = "0"
    public var adminAmount = "0"
    public var adminCommision = "0"

    public var arrClientInfo : [ClientInfo] =  [ClientInfo]()

    public var arrUserDetail :[UserDetail] =  [UserDetail]()
    public var arrBookingInfo : [BookingInfo] =  [BookingInfo]()
    public var arrStaffInfo : [StaffDetail] =  [StaffDetail]()

    
    init(dict: [String : Any]) {
       
        _id = String(dict["_id"] as? Int ?? 0)
        if let _id1 = dict["_id"] as? String{
            _id = _id1
        }
        
        userId = String(dict["userId"] as? Int ?? 0)
        if let _id1 = dict["userId"] as? String{
            userId = _id1
        }
        
        artistId = String(dict["artistId"] as? Int ?? 0)
        if let artistId1 = dict["artistId"] as? String{
            artistId = artistId1
        }
        
        artistRating = String(dict["artistRating"] as? Int ?? 0)
        if let _id1 = dict["artistRating"] as? String{
            artistRating = _id1
        }
        
        latitude = String(dict["latitude"] as? Int ?? 0)
        if let artistId1 = dict["latitude"] as? String{
            latitude = artistId1
        }
        
        longitude = String(dict["longitude"] as? Int ?? 0)
        if let artistId1 = dict["longitude"] as? String{
            longitude = artistId1
        }
        bookingDate = dict["bookingDate"] as? String ?? ""
        bookingTime = dict["bookingTime"] as? String ?? ""
        reviewByArtist = dict["reviewByArtist"] as? String ?? ""
        location = dict["location"] as? String ?? ""
        customerType = dict["customerType"] as? String ?? ""
        
        adminAmount = dict["adminAmount"] as? String ?? ""
        if let adminCom = dict["adminCommision"] as? String{
            adminCommision = adminCom
        }else if let adminCom = dict["adminCommision"] as? Int{
            adminCommision = String(adminCom)
        }

        
        if let status = dict["bookStatus"] as? Int{
            bookStatus = String(status)
        }else if let status = dict["bookStatus"] as? String{
            bookStatus = status
        }
        if let status = dict["bookingType"] as? Int{
            bookingType = String(status)
        }else if let status = dict["bookingType"] as? String{
            bookingType = status
        }
        
        if let status = dict["creationDate"] as? Int {
            self.creationDate = String(status)
        }else if let status = dict["creationDate"] as? String {
            self.creationDate = status
        }
        if let status = dict["isFinsh"] as? Int {
            self.isFinsh = String(status)
        }else if let status = dict["isFinsh"] as? String {
            self.isFinsh = status
        }
        if let status = dict["creationTime"] as? Int {
            self.creationTime = String(status)
        }else if let status = dict["creationTime"] as? String {
            self.creationTime = status
        }
        if let prise = dict["totalPrice"] as? Int{
            totalPrice = String(prise)
        }else if let prise1 = dict["totalPrice"] as? String{
                totalPrice = prise1
        }
        
        
        if let temp = dict["paymentType"] as? Int{
            self.paymentType = String(temp)
        }else if let temp = dict["paymentType"] as? String{
            self.paymentType = temp
        }
        
        if let temp = dict["transjectionId"] as? Int{
            self.transactionId = String(temp)
        }else if let temp = dict["transjectionId"] as? String{
            self.transactionId = temp
        }
        
        paymentStatus = String(dict["paymentStatus"] as? Int ?? 0)
        if let paymentStatus1 = dict["paymentStatus"] as? String{
            paymentStatus = paymentStatus1
        }
    
        if let arr = dict["bookingInfo"] as? [[String : Any]]{
            for dict in arr{
                let obj = BookingInfo.init(dict: dict)
                    self.arrBookingInfo.append(obj)
            }
        }

        
        if let arr = dict["client"] as? [[String : Any]]{
            for dict in arr{
                let obj = ClientInfo.init(dict: dict)
                self.arrClientInfo.append(obj)
            }
        }

        
        if let arr1 = dict["userDetail"] as? [[String : Any]]{
            for dict in arr1{
                let obj1 = UserDetail.init(dict: dict)
                self.arrUserDetail.append(obj1)
            }
        }
 
        if self.arrBookingInfo.count > 0 {
           
            var arrSubServiceName:[String] = [String]()
            var arrServiceId:[String] = [String]()
            var arrSubServiceId:[String] = [String]()
            var arrArtictServiceId:[String] = [String]()
            
            for obj in arrBookingInfo{
                
                allSubServiceName = obj.artistServiceName
                allServiceId = obj.serviceId
                allSubServiceId = obj.subServiceId
                allArtictServiceId = obj.artistServiceId
                
                arrSubServiceName.append(allSubServiceName)
                arrServiceId.append(allServiceId)
                arrSubServiceId.append(allSubServiceId)
                arrArtictServiceId.append(allArtictServiceId)

            }
            arrArtictServiceId = Array(Set(arrArtictServiceId))
            arrSubServiceId = Array(Set(arrSubServiceId))
            arrServiceId = Array(Set(arrServiceId))
            
            if arrSubServiceName.count > 0{
                if arrSubServiceName.count >= 2{
                    let arrTemp = arrSubServiceName[0...arrSubServiceName.count-1]
                    allSubServiceName = arrTemp.joined(separator: ", ")
                }else{
                    allSubServiceName = arrSubServiceName[0]
                }
            }
            
            if arrServiceId.count > 0{
                if arrServiceId.count >= 2{
                    let arrTemp = arrServiceId[0...arrServiceId.count-1]
                    allServiceId = arrTemp.joined(separator: ", ")
                }else{
                    allServiceId = arrServiceId[0]
                }
            }
            
            if arrSubServiceId.count > 0{
                if arrSubServiceId.count >= 2{
                    let arrTemp = arrSubServiceId[0...arrSubServiceId.count-1]
                    allSubServiceId = arrTemp.joined(separator: ", ")
                }else{
                    allSubServiceId = arrSubServiceId.joined(separator: ", ")
                }
            }
            
            if arrArtictServiceId.count > 0{
                if arrArtictServiceId.count >= 2{
                    let arrTemp = arrArtictServiceId[0...arrArtictServiceId.count-1]
                    allArtictServiceId = arrTemp.joined(separator: ", ")
                }else{
                    allArtictServiceId = arrArtictServiceId.joined(separator: ", ")
                }
            }
        }
    }
    
    public func joined(separator: String) -> String {
        return strValue
    }
  
}

public class ModelReportReason : NSObject {
    
 
    public var _id = ""
    public var __v = ""
    public var title = ""
    public var type = ""
    
    
    init(dict: [String : Any]) {
        _id = String(dict["_id"] as? Int ?? 0)
        if let _id1 = dict["_id"] as? String{
            _id = _id1
        }
        
        __v = String(dict["__v"] as? Int ?? 0)
        if let _id1 = dict["__v"] as? String{
            __v = _id1
        }
        title = dict["title"] as? String ?? ""
        type = dict["type"] as? String ?? ""
    }
}

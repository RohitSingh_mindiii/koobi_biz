

import Foundation

public class BookingInfo:NSObject {
	
    public var _id : String = ""
    public var Name : String = ""

	public var bookingPrice : String = ""
	public var serviceId : String = ""
	public var subServiceId : String = ""
	public var artistServiceId : String = ""
	public var bookingDate : String = ""
	public var startTime : String = ""
	public var endTime : String = ""
	public var staffId : String = ""
	public var staffName : String = ""
	public var staffImage : String = ""
	public var artistServiceName : String = ""
    public var companyName : String = ""
    public var status : String = ""

    public var trackingLatitude = "22.705256498329575"
    public var trackingLongitude = "75.90910885331525"
    var arrReportList : [ModelReportList] =  [ModelReportList]()

    
   init(dict: [String : Any]){
    
        _id = String(dict["_id"] as? Int ?? 0)
        if let status = dict["_id"] as? String{
            _id = status
        }
    
    trackingLatitude = String(dict["trackingLatitude"] as? Int ?? 0)
    if let status = dict["trackingLatitude"] as? String{
        trackingLatitude = status
    }
    
    trackingLongitude = String(dict["trackingLongitude"] as? Int ?? 0)
    if let status = dict["trackingLongitude"] as? String{
        trackingLongitude = status
    }
    
        bookingPrice = String(dict["bookingPrice"] as? Int ?? 0)
        if let status = dict["bookingPrice"] as? String{
            bookingPrice = status
        }
    
        serviceId = String(dict["serviceId"] as? Int ?? 0)
        if let status = dict["serviceId"] as? String{
            serviceId = status
        }
        subServiceId = String(dict["subServiceId"] as? Int ?? 0)
        if let status = dict["subServiceId"] as? String{
            subServiceId = status
        }
        artistServiceId = String(dict["artistServiceId"] as? Int ?? 0)
        if let status = dict["artistServiceId"] as? String{
            artistServiceId = status
        }
    
        staffId = String(dict["staffId"] as? Int ?? 0)
        if let status = dict["staffId"] as? String{
            staffId = status
        }
    
        status = String(dict["status"] as? Int ?? 0)
        if let status1 = dict["status"] as? String{
            status = status1
        }
        staffName = dict["staffName"] as? String ?? ""
        staffImage = dict["staffImage"] as? String ?? ""
        artistServiceName = dict["artistServiceName"] as? String ?? ""
        bookingDate = dict["bookingDate"] as? String ?? ""
        startTime = dict["startTime"] as? String ?? ""
        //endTime = dict["endTime"] as? String ?? ""
        endTime = dict["endTimeBiz"] as? String ?? ""
        companyName = dict["companyName"] as? String ?? ""
    
    
    if let arr = dict["bookingReport"] as? [[String : Any]]{
        for dict in arr{
            let obj = ModelReportList.init(dict: dict)
            self.arrReportList.append(obj)
        }
    }
    
    
    }
}

//
//  ModelReportList.swift
//  MualabBusiness
//
//  Created by Mindiii on 2/19/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit

class ModelReportList: NSObject {
    
        var _id : String = ""
        var bookingId : String = ""
        var bookingInfoId :String = ""
        var businessId : String = ""
        var crd : String = ""
        var descriptions : String = ""
        var reportByUser : String = ""
        var adminReason : String = ""
        var reportForUser : String = ""
        var title : String = ""
        var status : String = "0"
    
         init(dict : [String : Any]){
           
            _id = String(dict["_id"] as? Int ?? 0)
            if let id = dict["_id"] as? String{
                _id = id
            }
            status = String(dict["status"] as? Int ?? 0)
            if let id = dict["status"] as? String{
                status = id
            }
            bookingId = String(dict["bookingId"] as? Int ?? 0)
            if let id = dict["bookingId"] as? String{
                bookingId = id
            }
            
            bookingInfoId = String(dict["bookingInfoId"] as? Int ?? 0)
            if let id = dict["bookingInfoId"] as? String{
                bookingInfoId = id
            }
            
            businessId = String(dict["businessId"] as? Int ?? 0)
            if let id = dict["businessId"] as? String{
                businessId = id
            }
            
            reportByUser = String(dict["reportByUser"] as? Int ?? 0)
            if let id = dict["reportByUser"] as? String{
                reportByUser = id
            }
            
            reportForUser = String(dict["reportForUser"] as? Int ?? 0)
            if let id = dict["reportForUser"] as? String{
                reportForUser = id
            }
            
                descriptions = dict["description"] as? String ?? ""
                crd = dict["crd"] as? String ?? ""
                adminReason = dict["adminReason"] as? String ?? ""
                title = dict["title"] as? String ?? ""
                crd = objAppShareData.crtToDateString(crd: crd)
        }
}

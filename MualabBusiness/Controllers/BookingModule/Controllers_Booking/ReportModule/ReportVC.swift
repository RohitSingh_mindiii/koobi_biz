//
//  ReportVC.swift
//  MualabCustomer
//
//  Created by Mindiii on 8/22/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit


class ReportVC: UIViewController,UITextFieldDelegate,UITextViewDelegate,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var txtSelectReason: UITextField!
    @IBOutlet weak var txtViewDescription: UITextView!
    
    @IBOutlet weak var txtReportDate: UITextField!
    @IBOutlet weak var txtReportStatus: UITextField!
    
    @IBOutlet weak var viewReportDate: UIView!
    @IBOutlet weak var viewReportStatus: UIView!
    @IBOutlet weak var viewNoteStatus: UIView!
    @IBOutlet weak var viewBtnSubmit: UIView!
    @IBOutlet weak var lblHint: UILabel!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var imgDropdownReason: UIImageView!

    @IBOutlet weak var lblNoteDetail: UILabel!
    @IBOutlet weak var lblHeader: UILabel!
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var viewBottumTable: UIView!
    @IBOutlet weak var HeightViewBottumTable: NSLayoutConstraint!


    
    var reportType = 0

 
    fileprivate var strMyId = ""
    var objBookingInfo = BookingInfo(dict: ["":""])
    var objBooking = Booking(dict: ["":""])
    var arrReportReasonList = [ModelReportReason]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblView.delegate = self
        self.tblView.dataSource = self
        self.viewBottumTable.isHidden = true
        if  let dict = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] {
            self.strMyId = dict[UserDefaults.keys.id] as? String ?? ""
        }
        self.txtSelectReason.delegate = self
        self.txtViewDescription.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.viewBottumTable.isHidden = true
           let dict = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] ?? ["":""]
            self.strMyId = dict[UserDefaults.keys.id] as? String ?? ""
            self.manageUI()
        if reportType != 1{
            self.callWebserviceForReasonList()
        }
    }
    
    func manageUI(){
        if reportType == 0{
            self.lblHeader.text = "Report"
            self.lblHint.isHidden = false
            self.imgDropdownReason.isHidden = false
            self.viewReportDate.isHidden = true
            self.viewReportStatus.isHidden = true
            self.viewNoteStatus.isHidden = true
            self.viewBtnSubmit.isHidden = false
            self.txtViewDescription.isUserInteractionEnabled = true
            self.btnSubmit.setTitle("Submit", for: .normal)
            self.txtViewDescription.text = ""
            self.txtSelectReason.text = ""
        }else if reportType == 1{
            self.lblHeader.text = "Report Details"
            self.lblHint.isHidden = true
            self.imgDropdownReason.isHidden = true
            self.viewReportDate.isHidden = false
            self.viewReportStatus.isHidden = false
            self.viewNoteStatus.isHidden = true
            self.viewBtnSubmit.isHidden = true
            self.txtViewDescription.isUserInteractionEnabled = false
            self.dataParsing()
        }else if reportType == 2{
            self.lblHeader.text = "Report Details"
            self.lblHint.isHidden = true
            self.imgDropdownReason.isHidden = true
            self.viewReportDate.isHidden = false
            self.viewReportStatus.isHidden = false
            self.viewNoteStatus.isHidden = false
            self.viewBtnSubmit.isHidden = false
            self.txtViewDescription.isUserInteractionEnabled = false
            self.btnSubmit.setTitle("Resubmit", for: .normal)
            self.dataParsing()
        }
    }
    
    func dataParsing(){
        if objBookingInfo.arrReportList.count > 0{
            self.txtSelectReason.text = objBookingInfo.arrReportList.last?.title
            self.txtViewDescription.text = objBookingInfo.arrReportList.last?.descriptions
            self.txtReportDate.text = objBookingInfo.arrReportList.last?.crd
            if objBookingInfo.arrReportList.last?.status == "1"{
                self.txtReportStatus.text = "Pending"
                self.txtReportStatus.textColor = #colorLiteral(red: 0.9200255673, green: 0.4889633489, blue: 0.08423942367, alpha: 1)
            }else{
                self.txtReportStatus.text = "Resolved"
                self.txtReportStatus.textColor = #colorLiteral(red: 0, green: 0.7333333333, blue: 0.1529411765, alpha: 1)
            }
            self.lblNoteDetail.text = objBookingInfo.arrReportList.last?.adminReason
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
 
    internal func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        var edit = true
        var newLength = 0
        var searchString: String? = nil
        if textView == self.txtViewDescription {
            if (text.count) != 0
            { searchString = self.txtViewDescription.text! + (text).uppercased()
                newLength = (self.txtViewDescription.text?.count)! + text.count - range.length
            }
            else {
            }
            if newLength <= 150{  edit = true
            }else{   edit = false  }
            return edit
        }
        return edit
    }
    
    
    internal func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var edit = true
        var newLength = 0
        var searchString: String? = nil
        if textField == self.txtSelectReason {
            if (string.count ) != 0
            { searchString = self.txtSelectReason.text! + (string).uppercased()
                newLength = (self.txtSelectReason.text?.count)! + string.count - range.length
            }
            else { searchString = (self.txtSelectReason.text as NSString?)?.substring(to: (self.txtSelectReason.text?.count)! - 1).uppercased()
            }
            if newLength <= 50{  edit = true
            }else{   edit = false  }
            return edit
        }
        return edit
    }
    
    internal func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.txtSelectReason{
            txtViewDescription.becomeFirstResponder()
        }
        return true
    }
    
    

    @IBAction func btnSubmitReportAction(_ sender: UIButton) {
        self.view.endEditing(true)
        if reportType == 2{
            reportType = 0
            self.manageUI()
        }else{
        self.txtSelectReason.text = self.txtSelectReason.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        self.txtViewDescription.text = self.txtViewDescription.text.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if self.txtSelectReason.text?.count == 0{
             objAppShareData.showAlert(withMessage: "Please select report reason", type: alertType.bannerDark,on: self)
        }else if self.txtViewDescription.text?.count == 0{
             objAppShareData.showAlert(withMessage: "Please enter report description", type: alertType.bannerDark,on: self)
        }else{
            self.reportAction()
        }
        }
    }
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.view.endEditing(true)
self.navigationController?.popViewController(animated: true)    }
    
    
    func reportAction(){
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        let myString = formatter.string(from: Date())
        let staffId = objBookingInfo.staffId
        
          let dict = ["bookingInfoId":objBookingInfo._id,
                            "description":self.txtViewDescription.text ?? "",
                            "bookingId":objBooking._id,
                            "staffId":staffId,
                            "businessId":objBooking.artistId,
                            "serviceName":objBookingInfo.artistServiceName,
                            "reportByUser":self.strMyId,
                            "reportForUser":objBooking.userId,
                            "serviceId":objBookingInfo.artistServiceId,
                            "reportDate":myString,
                            "title":self.txtSelectReason.text ?? ""] as [String : Any]
        
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
        }
            objActivity.startActivityIndicator()
            objServiceManager.requestPost(strURL: WebURL.bookingReport, params: dict  , success: { response in
                objServiceManager.StopIndicator()
                let keyExists = response["responseCode"] != nil
                if  keyExists {
                    sessionExpireAlertVC(controller: self)
                }else{
                    let strStatus =  response["status"] as? String ?? ""
                   // let strMsg = response["message"] as? String ?? kErrorMessage
                    if strStatus == k_success{
                        self.navigationController?.popViewController(animated: true)
                        objAppShareData.showAlert(withMessage: "Report Submitted successfully", type: alertType.bannerDark, on: self)
                    }else{
                        let msg = response["message"] as? String ?? ""
                        objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                    }
                }
            }){ error in
                objServiceManager.StopIndicator()
                objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
            }
        }
    }



//MARK: - button extension
extension ReportVC{
    @IBAction func btnDropReasonAction(_ sender: UIButton) {
        if reportType == 0{
        let objGetData = arrReportReasonList.map { $0.title }
            self.viewBottumTable.isHidden = false
            self.HeightViewBottumTable.constant = CGFloat(self.arrReportReasonList.count*45)

            self.tblView.reloadData()
      }
    }
}
//MARK: - button extension
extension ReportVC{
    func callWebserviceForReasonList(){
        if !objServiceManager.isNetworkAvailable(){
            return
        }
        self.arrReportReasonList.removeAll()
        let params =  ["type":"booking"]
        objServiceManager.requestGet(strURL:WebURL.reportReason, params: params as [String : AnyObject], success: { response in
            let  status = response["status"] as? String ?? ""
            print(response)
            if status == "success"{
                if let arr = response["data"] as? [[String:Any]]{
                    for obj in arr{
                        let obj = ModelReportReason.init(dict:obj)
                        self.arrReportReasonList.append(obj)
                    }
                   }
             }else{
                let msg = response["message"] as? String ?? ""
                objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
            }
        }) { error in
                      objActivity.stopActivity()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }}

//MARK: - table view method extension
extension ReportVC{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return arrReportReasonList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "CellBottumTableList"
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? CellBottumTableList{
                let obj = self.arrReportReasonList[indexPath.row]
                cell.lblTitle.text = obj.title
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            self.viewBottumTable.isHidden = true
             let item = arrReportReasonList[indexPath.row]
            self.txtSelectReason.text = item.title
    }

    @IBAction func btnHiddenBottumTable(_ sender: UIButton) {
        self.viewBottumTable.isHidden = true
    }
}

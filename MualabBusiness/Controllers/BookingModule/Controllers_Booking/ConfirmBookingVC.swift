//
//  ArtistServicesVC.swift
//  MualabBusiness
//
//  Created by mac on 29/05/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import DropDown
import CoreLocation
import Firebase
import FirebaseAuth
import HCSStarRatingView


class ConfirmBookingVC : UIViewController,UITableViewDelegate,UITableViewDataSource,CLLocationManagerDelegate,UITextViewDelegate{
    
    @IBOutlet weak var constraintTableHeight: NSLayoutConstraint!
    @IBOutlet weak var lblHeaderOutCallInCall: UILabel!
    @IBOutlet weak var imgCustomer: UIImageView!
    @IBOutlet weak var lblOutCallAddress: UILabel!
    @IBOutlet weak var lblCustomerName: UILabel!
    @IBOutlet weak var lblCustomerContact: UILabel!
    @IBOutlet weak var lblBookingType: UILabel!
    @IBOutlet weak var tblBookedServices: UITableView!
    @IBOutlet weak var lblTotalPrice: UILabel!
    @IBOutlet weak var lblAdminCommission: UILabel!
    @IBOutlet weak var lblRedTotalPrice: UILabel!
    @IBOutlet weak var lblBookingStatus: UILabel!
    @IBOutlet weak var lblPaymentStatus: UILabel!
    @IBOutlet weak var lblPaymentCompleteStatus: UILabel!
    @IBOutlet weak var viewPaymentStatus: UIView!

    @IBOutlet weak var viewRedTotalPrice: UIView!
    @IBOutlet weak var viewChatOption: UIView!
    @IBOutlet weak var viewCallOption: UIView!

    
    //review rating view Outlet
    @IBOutlet weak var viewRatingPopup: UIView!
    @IBOutlet weak var imgCustomerRatingPopup: UIImageView!
//    @IBOutlet weak var lblNameRatingPopup: UILabel!
   @IBOutlet weak var lblDetailRatingPopup: UILabel!
    @IBOutlet weak var viewRatingAssignRatingPopup: HCSStarRatingView!
    @IBOutlet weak var btnAddCommentRatingPopup: UIView!
    @IBOutlet weak var viewTxtAddCommentRatingPopup: UIView!
    @IBOutlet weak var txtAddCommentRatingPopup: SZTextView!
    
    @IBOutlet weak var viewVoucherCode: UIView!
    @IBOutlet weak var txtVoucherCode: UITextField!
    @IBOutlet weak var lblVoucherText: UILabel!

    @IBOutlet weak var viewAccept: UIView!
    @IBOutlet weak var viewReject: UIView!
    @IBOutlet weak var viewAddress: UIView!
    @IBOutlet weak var viewReviewStar: UIView!
    @IBOutlet weak var btnCompleteService: UIButton!
    
    @IBOutlet weak var viewTablePaymentStatus: UIView!
    @IBOutlet weak var btnManagePaidUnpad: UIButton!

     var dictVoucher = [:] as! [String:Any]
     var totalPrice = 0.0
     var discountPrice = 0.0
     var objArtistDetails = ArtistDetails(dict: ["":""])
     var objBookingServices = BookingServices()
     var arrBookedService = [BookingInfo]()
    var arrBookedMainService = [Booking]()

     var isOutCallSelectedConfirm = false
     var strMyAddress = ""
    var strBookingId = ""
    var artistId = ""
    var myId = ""
    var fromAppointment = false
    var fromPaymentVC = false

    override func viewDidLoad() {
        super.viewDidLoad()
        self.btnManagePaidUnpad.isHidden = true
        self.viewVoucherCode.isHidden = true
        self.viewPaymentStatus.isHidden = true
        self.btnCompleteService.isHidden = true
        self.viewChatOption.isHidden = true
        self.viewCallOption.isHidden = true
        self.viewReject.isHidden = true
        self.viewAccept.isHidden = true
        self.viewReviewStar.isHidden = true
        self.viewRatingPopup.isHidden = true
        self.viewTablePaymentStatus.isHidden = true
        self.viewTxtAddCommentRatingPopup.isHidden = true
        self.txtAddCommentRatingPopup.delegate = self
        
       //self.imgCustomer.layer.cornerRadius = 23
        self.imgCustomer.layer.masksToBounds = true
        let userInfo = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]  ?? ["":""]
        self.myId = userInfo[UserDefaults.keys.id] as? String ?? ""
        let adminId = UserDefaults.standard.string(forKey: UserDefaults.keys.newAdminId) ?? ""
        if adminId == self.myId || adminId == ""{
           // self.viewChatOption.isHidden = false
            self.viewCallOption.isHidden = false
        }else{
            self.artistId = adminId
            self.viewChatOption.isHidden = true
            self.viewCallOption.isHidden = true
        }
        self.addAccesorryToKeyBoard()
        self.lblBookingType.text = ""
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
   
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let userInfo = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]  ?? ["":""]
        self.viewAccept.isHidden = true
        self.viewReject.isHidden = true
        self.viewReviewStar.isHidden = true
        self.artistId = userInfo[UserDefaults.keys.id] as? String ?? ""
        self.myId = userInfo[UserDefaults.keys.id] as? String ?? ""
        let adminId = UserDefaults.standard.string(forKey: UserDefaults.keys.newAdminId) ?? ""
        if adminId == self.artistId || adminId == ""{
            self.viewCallOption.isHidden = false
          //self.viewChatOption.isHidden = false
        }else{
            self.artistId = adminId
            self.viewCallOption.isHidden = true
            self.viewChatOption.isHidden = true
        }
        self.lblBookingType.text = ""
        self.callWebserviceForGetBookedServices(fromCompleteService: false)
        objAppShareData.callWebserviceForFindOnTheWayUser()
        self.tabBarController?.tabBar.isHidden = true
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        print(" self.viewHeight  height ",viewHeightGloble)
        print("self.view.frame height ",self.tabBarController?.view.frame.height)
        
        if viewHeightGloble != Int(self.view.frame.height) {
            self.tabBarController?.tabBar.isHidden = true
            self.tabBarController?.view.frame = CGRect(x:0, y:0, width:self.view.frame.size.width, height:self.view.frame.size.height+(self.tabBarController?.tabBar.frame.size.height)!*2)
        }
    }
}

//MARK: - button extension
extension ConfirmBookingVC {
    @IBAction func btnReportAction(_ sender: UIButton) {
        objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
    }
    @IBAction func btnBackAction(_ sender: UIButton) {
        if fromAppointment == true{
            objAppShareData.objAppdelegate.gotoTabBar(withAnitmation: false)
        }else{
        self.navigationController?.popViewController(animated: true)
        }
    }
    @IBAction func btnCheckPaymentStatus(_ sender: Any) {
        self.viewTablePaymentStatus.isHidden = false
    }
    @IBAction func btnHiddenCheckPaymentStatus(_ sender: Any) {
        self.viewTablePaymentStatus.isHidden = true
    }
    @IBAction func btnPaidPaymentStatus(_ sender: Any) {
        self.viewTablePaymentStatus.isHidden = true
         let objBooking = arrBookedMainService[0]
        
        var notDone = 0
        for objNew in objBooking.arrBookingInfo{
            if objNew.status != "3"{
                notDone = notDone+1
            }
        }
        
        if objBooking.artistId == self.myId &&  (objBooking.arrBookingInfo.count == 1 ||   notDone == 0) {
            self.completeBooking(strType: "complete",fromCompleteService:true)
            self.btnManagePaidUnpad.isHidden = true
        }else{
            self.completeBooking(strType:"paid",fromCompleteService:false)
        }
    }
    
    @IBAction func btnUnpaidPaymentStatus(_ sender: Any) {
        self.viewTablePaymentStatus.isHidden = true
    }
    
    @IBAction func btnSubmitRatting(_ sender: Any) {
        self.view.endEditing(true)
        if self.arrBookedService.count == 0 || self.arrBookedMainService.count ==  0 {
            return
        }
        let objMainBooking = self.arrBookedMainService[0]
        let objUser = objMainBooking.arrUserDetail[0]
        self.txtAddCommentRatingPopup.text = self.txtAddCommentRatingPopup.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        if self.viewRatingAssignRatingPopup.value == 0 {
            objAppShareData.showAlert(withMessage: "Please give rating", type: alertType.bannerDark, on: self)
//        }else if self.txtAddCommentRatingPopup.text == "" {
//            objAppShareData.showAlert(withMessage: "Please give review", type: alertType.bannerDark, on: self)
        }else{
            let date = objAppShareData.dateFormatInFormYou(forAPI: Date())
            let  parameters = [
                "artistId" : self.artistId,
                "userId" : objMainBooking.userId,//objUser._id,
                "bookingId":objMainBooking._id,
                "reviewByUser": "",
                "ratingDate":date,
                "reviewByArtist":self.txtAddCommentRatingPopup.text ?? "",
                "rating": "\(self.viewRatingAssignRatingPopup.value)"
            ]
            callWebserviceForGet_AcceptRejectCount(dict: parameters, webURL: WebURL.bookingReviewRating, fromCompleteService: false)
        }
    }
    @IBAction func btnCancleRatting(_ sender: Any) {
        self.view.endEditing(true)
        self.viewRatingPopup.isHidden = true
    }
    @IBAction func btnAddComment(_ sender: Any) {
        self.viewTxtAddCommentRatingPopup.isHidden = !self.viewTxtAddCommentRatingPopup.isHidden
    }
    
    @IBAction func btnChatOption(_ sender: UIButton) {
        objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
    }
    
    @IBAction func btnCallOption(_ sender: UIButton) {
        if self.arrBookedMainService.count > 0{
            if arrBookedMainService[0].arrUserDetail.count > 0{
                    let obj = arrBookedMainService[0].arrUserDetail[0]
                
                if obj.contactNo == ""{
                    objAppShareData.showAlert(withMessage: "Contact number not available!", type: alertType.bannerDark,on: self)
                    return
                }
                    if let url = URL(string: "tel://\(obj.countryCode+obj.contactNo)"), UIApplication.shared.canOpenURL(url) {
                        if #available(iOS 10, *) {
                            UIApplication.shared.open(url)
                        } else {
                        UIApplication.shared.openURL(url)
                        }
                    }
            }else if arrBookedMainService[0].arrClientInfo.count > 0{
                let obj = arrBookedMainService[0].arrClientInfo[0]
                let userInfo = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]  ?? ["":""]
                
                let countryCode = userInfo[UserDefaults.keys.countryCode] as? String ?? ""

                if let url = URL(string: "tel://\(countryCode+obj.phone)"), UIApplication.shared.canOpenURL(url) {
                    if #available(iOS 10, *) {
                        UIApplication.shared.open(url)
                    } else {
                        UIApplication.shared.openURL(url)
                    }
                }
            }
        }
    }
}

//MARK: - Custom methods Extension
fileprivate extension ConfirmBookingVC{

    func manageView(){
        self.manageOutCallInCallView()
     }
    
    func manageOutCallInCallView(){
       
        if self.isOutCallSelectedConfirm{
            self.lblHeaderOutCallInCall.text = "Out Call"
            self.lblOutCallAddress.text = self.strMyAddress
        }else{
            self.lblHeaderOutCallInCall.text = "In Call"
        }
        if self.arrBookedMainService.count > 0{
            self.lblOutCallAddress.text = self.arrBookedMainService[0].location
            if self.arrBookedMainService[0].bookingType == "1"{
                self.lblHeaderOutCallInCall.text = "In Call"
            }else if self.arrBookedMainService[0].bookingType == "2"{
                self.lblHeaderOutCallInCall.text = "Out Call"
            }else{
                self.lblHeaderOutCallInCall.text = "Both"
            }
            let objAllBooking = self.arrBookedMainService[0]
            self.viewAccept.isHidden = true
            self.viewReject.isHidden = true
            
            let userInfo = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]  ?? ["":""]
            
            self.myId = userInfo[UserDefaults.keys.id] as? String ?? ""
            self.btnCompleteService.isHidden = true
            self.viewReviewStar.isHidden = true
            if objAllBooking.bookStatus == "3" && objAllBooking.customerType != "walking" && objAllBooking.reviewByArtist == "" && objAllBooking.artistRating == "0"{
                if self.myId == self.artistId || self.artistId == ""   {
                    self.viewReviewStar.isHidden = true
                }}
            if objAllBooking.bookStatus == "5"{
                if self.myId == self.artistId || self.artistId == ""   {
                     var btnShow = true
                    for obj in objAllBooking.arrBookingInfo{
                        if obj.status != "3" {
                            btnShow = false
                        }
                    }
                    if btnShow == true{
                      //  self.btnCompleteService.isHidden = false
                        if objAllBooking.paymentType == "1"{
                          self.btnCompleteService.isHidden = false
                            self.btnManagePaidUnpad.isHidden = true
                        }else{
                            self.lblPaymentStatus.text = "Cash"
                            if objAllBooking.paymentStatus == "1"{
                                self.btnCompleteService.isHidden = false
                                self.btnManagePaidUnpad.isHidden = true
                            }else{
                                self.btnCompleteService.isHidden = true
                                self.btnManagePaidUnpad.isHidden = false
                            }
                        }
                        
                    }
                }
            }
            
            
            if objAllBooking.bookStatus == "0" || objAllBooking.bookStatus == "1" {
            if self.myId == self.artistId || self.artistId == ""   {
                if self.artistId == objAllBooking.artistId{
                    self.viewAccept.isHidden = false
                    if fromPaymentVC == false{
                    self.viewReject.isHidden = false
                    }
                    if objAllBooking.bookStatus == "1"{
                        self.viewAccept.isHidden = true
                    }
                }else{
                    var btnShow = true
                    for obj in objAllBooking.arrBookingInfo{
                        if obj.staffId != myId {
                            btnShow = false
                        }
                    }
                    if btnShow == true{
                        self.viewAccept.isHidden = false
                        if fromPaymentVC == false{
                            self.viewReject.isHidden = false
                        }
                        if objAllBooking.bookStatus == "1"{self.viewAccept.isHidden = true}
                    }
                }
            }else{
                var btnShow = true
                for obj in objAllBooking.arrBookingInfo{
                    if obj.staffId != myId {
                        btnShow = false
                    }
                }
                if btnShow == true{
                    self.viewAccept.isHidden = false
                    if fromPaymentVC == false{
                        self.viewReject.isHidden = false
                    }
                    if objAllBooking.bookStatus == "1"{self.viewAccept.isHidden = true}
                }
            }
        }
            
           if objAllBooking.bookStatus == "0"{
                self.lblBookingStatus.text = "Pending"
                self.lblBookingStatus.textColor =  #colorLiteral(red: 0.9200255673, green: 0.4889633489, blue: 0.08423942367, alpha: 1)
            } else if objAllBooking.bookStatus == "1"{
                self.lblBookingStatus.text = "Confirmed"
                self.lblBookingStatus.textColor =  #colorLiteral(red: 0, green: 0.7333333333, blue: 0.1529411765, alpha: 1)
                //self.viewReject.isHidden = false
            }else if objAllBooking.bookStatus == "2" {
                self.lblBookingStatus.text = "Cancelled"
                self.lblBookingStatus.textColor =  #colorLiteral(red: 0.9607843137, green: 0.01568627451, blue: 0.01568627451, alpha: 1)
            }else if objAllBooking.bookStatus == "3" {
                self.lblBookingStatus.text = "Completed"
                self.lblBookingStatus.textColor =  #colorLiteral(red: 0, green: 0.7333333333, blue: 0.1529411765, alpha: 1)
            }else if objAllBooking.bookStatus == "5"{
                self.lblBookingStatus.text = "In progress"
                self.lblBookingStatus.textColor =  #colorLiteral(red: 0, green: 0.7333333333, blue: 0.1529411765, alpha: 1)
            }
           if objAllBooking.paymentStatus == "0"{
            self.lblPaymentCompleteStatus.text = "Pending"
            self.lblPaymentCompleteStatus.textColor =  #colorLiteral(red: 0.9200255673, green: 0.4889633489, blue: 0.08423942367, alpha: 1)
            } else  if objAllBooking.paymentStatus == "1"{
                self.lblPaymentCompleteStatus.text = "Completed"
                self.lblPaymentCompleteStatus.textColor =  #colorLiteral(red: 0, green: 0.7333333333, blue: 0.1529411765, alpha: 1)
            }else  if objAllBooking.paymentStatus == "2"{
                self.lblPaymentCompleteStatus.text = "Pending"
                self.lblPaymentCompleteStatus.textColor =  #colorLiteral(red: 0.9200255673, green: 0.4889633489, blue: 0.08423942367, alpha: 1)
            }else  if objAllBooking.paymentStatus == "3"{
                self.lblPaymentCompleteStatus.text = "Refund"
                self.lblPaymentCompleteStatus.textColor =  #colorLiteral(red: 0.9200255673, green: 0.4889633489, blue: 0.08423942367, alpha: 1)
            }
            if objAllBooking.paymentType == "1"{
                self.lblPaymentStatus.text = "Card"
                self.viewPaymentStatus.isHidden = false
            }else{
                self.lblPaymentStatus.text = "Cash"
                self.viewPaymentStatus.isHidden = true
            }
        }
    }
    func calculateTotalPrice(){
        var totalPrice = 0.0
        for obj in self.arrBookedService{
            totalPrice = totalPrice + Double(obj.bookingPrice)!
        }
        self.totalPrice = totalPrice
        /////
        let doubleStr = String(format: "%.2f", Double(self.totalPrice))
        ////
        //self.lblTotalPrice.text = "£" + String(totalPrice)
        self.lblTotalPrice.text = "£" + String(doubleStr)
        //self.lblRedTotalPrice.text = "£" + String(format: "%.2f",(totalPrice+Double(self.arrBookedMainService[0].adminAmount as? String ?? "0.0")! as? Double ?? 0.0))
        self.lblRedTotalPrice.text = "£" + String(format: "%.2f",totalPrice)
    }
}

//MARK: - Webservices Extension
fileprivate extension ConfirmBookingVC{
    
    func manageVoucherView(dict:[String:Any]){
        self.dictVoucher = dict
        var discountType = 0
        if let discount = dict["discountType"] as? Int{
            discountType = discount
        }else if let discount = dict["discountType"] as? String{
            discountType = Int(discount)!
        }
        var amount = 0.0
        if let amnt = dict["amount"] as? Double{
            amount = amnt
        }else if let amnt = dict["amount"] as? Int{
            amount = Double(amnt)
        }else if let amnt = dict["amount"] as? Float{
            amount = Double(amnt)
        }else if let amnt = dict["amount"] as? String{
            amount = Double(amnt)!
        }
        
        self.lblVoucherText.text = dict["voucherCode"] as? String ?? ""
 
        if discountType == 2{
            let newPrice = self.totalPrice - (self.totalPrice*amount/100)
            if newPrice>0{
                self.discountPrice = newPrice
                let commission = Double(self.arrBookedMainService[0].adminAmount ) ?? 0.0
                //let b = String(format: "%.2f", commission)
                //self.lblTotalPrice.text = "£" + String(format: "%.2f", newPrice+commission)//String(newPrice)
                self.lblTotalPrice.text = "£" + String(format: "%.2f", newPrice)
                /////
                //let doubleStr = String(format: "%.2f", Double(newPrice+commission))
                let doubleStr = String(format: "%.2f", Double(newPrice))
                self.lblTotalPrice.text = "£" + doubleStr
                ////
            }else{
                self.discountPrice = 0.0
                self.lblTotalPrice.text = "£" + "0"
            }
            self.txtVoucherCode.text = "-"+String(format: "%.2f", amount)+"%"
        }else{
            let newPrice = self.totalPrice-amount
            if newPrice>0{
                self.discountPrice = newPrice
                let commission = Double(self.arrBookedMainService[0].adminAmount ) ?? 0.0
                //let b = String(format: "%.2f", commission)
                //self.lblTotalPrice.text = "£" + String(format: "%.2f", newPrice+commission)
                self.lblTotalPrice.text = "£" + String(format: "%.2f", newPrice)
                /////
                //let doubleStr = String(format: "%.2f", Double(newPrice+commission))
                let doubleStr = String(format: "%.2f", Double(newPrice))
                self.lblTotalPrice.text = "£" + doubleStr
                ////
            }else{
                self.discountPrice = 0.0
                self.lblTotalPrice.text = "£" + "0"
            }
            self.txtVoucherCode.text = "-£"+String(format: "%.2f", amount)
        }
        self.txtVoucherCode.isUserInteractionEnabled = false
        self.viewRedTotalPrice.isHidden = false
        self.txtVoucherCode.textColor = #colorLiteral(red: 0, green: 0.7333333333, blue: 0.1529411765, alpha: 1)
    }
    
    func callWebserviceForGetBookedServices(fromCompleteService:Bool){
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        objWebserviceManager.StartIndicator()
        let dicParam = [
        "bookingId":self.strBookingId
            ] as [String : Any]
        
        objServiceManager.requestPostForJson(strURL: WebURL.bookingDetail, params: dicParam, success: { response in
            print(response)
            self.arrBookedService.removeAll()
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else {
                let strSucessStatus = response["status"] as? String ?? ""
                if  strSucessStatus == "success"{
                    print(response)
                    self.saveData(dicts:response)
                    if forReview == true{
                        objWebserviceManager.StopIndicator()
                        forReview = false
                        self.popUpRatting()
                    }
                     if fromCompleteService == true{
                        objWebserviceManager.StopIndicator()
                        self.popUpRatting()
                        let objMain = self.arrBookedMainService[0]
                        if objMain.paymentType == "1" && objMain.paymentStatus == "1"{
                        let param = ["id":objMain._id]
                        objAppShareData.callWebserviceForAnyAPIWithoutResponceCheck(param: param, api:WebURL.artistPayment)
                        }
                    }
                }else{
                    objWebserviceManager.StopIndicator()
                    let msg = response["message"] as? String ?? ""
                    objAppShareData.showAlert(withMessage: msg , type: alertType.bannerDark,on: self)
                }}
        }) { error in
            objWebserviceManager.StopIndicator()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
    func saveData(dicts:[String:Any]){
        self.arrBookedMainService.removeAll()
        self.arrBookedService.removeAll()
        if let strBookingt = dicts["data"] as? [String : Any]{
                let objBooking = Booking.init(dict: strBookingt)
                self.arrBookedMainService.append(objBooking)
            if arrBookedMainService.count > 0{
                self.arrBookedService = self.arrBookedMainService[0].arrBookingInfo
                if self.arrBookedMainService[0].adminAmount != ""{
                    let commission = Double(self.arrBookedMainService[0].adminAmount ) ?? 0.0
                    let b = String(format: "%.2f", commission)
                    self.lblAdminCommission.text = "£"+b+"  ("+self.arrBookedMainService[0].adminCommision+"%)"
                }else{
                    if self.arrBookedMainService[0].adminAmount != ""{
                        let commission = Double(self.arrBookedMainService[0].adminAmount ) ?? 0.0
                        let b = String(format: "%.2f", commission)
                        self.lblAdminCommission.text = "£"+b+"  ("+self.arrBookedMainService[0].adminCommision+"%)"
                    }else{
                        self.lblAdminCommission.text = "£"+self.arrBookedMainService[0].adminAmount+"  ("+self.arrBookedMainService[0].adminCommision+"%)"
                    }
                }
            }
            
            if objBooking.customerType == "walking"{
                if objBooking.arrClientInfo.count > 0 {
                    self.lblCustomerName.text = objBooking.arrClientInfo[0].firstName+" "+objBooking.arrClientInfo[0].lastName
                    self.lblCustomerContact.text = objBooking.arrClientInfo[0].phone
                }
                self.imgCustomer.image = #imageLiteral(resourceName: "cellBackground")
                self.lblBookingType.text = "Walk-in"//objBooking.customerType.capitalized
                self.lblCustomerContact.isHidden = false
                
            }else{
                self.lblBookingType.text = ""
                if objBooking.arrUserDetail.count > 0{
                    let objUserInfo = objBooking.arrUserDetail[0]
                    self.lblCustomerName.text = objUserInfo.userName
                    let url = URL(string: objUserInfo.profileImage)
                    if  url != nil {
                        //self.imgCustomer.af_setImage(withURL: url!)
                        self.imgCustomer.sd_setImage(with: url!, placeholderImage: #imageLiteral(resourceName: "cellBackground"))
                    }else{
                        self.imgCustomer.image = #imageLiteral(resourceName: "cellBackground")
                        self.lblCustomerContact.text = objUserInfo.contactNo
                    }
                    self.lblCustomerContact.isHidden = true
                }
            }
            
            self.calculateTotalPrice()
            self.viewRedTotalPrice.isHidden = true
            if let voucherCode = strBookingt["voucher"] as? [String:Any]{
                if voucherCode.count > 0{
                    self.viewVoucherCode.isHidden = false
                    self.manageVoucherView(dict: voucherCode)
                }else{
                    self.lblTotalPrice.text = "£" + String(format: "%.2f",(Double(self.arrBookedMainService[0].totalPrice as? String ?? "0.0") ?? 0.0+Double(self.arrBookedMainService[0].adminAmount as? String ?? "0.0")! as? Double ?? 0.0))

                    self.viewVoucherCode.isHidden = true
                }
            }
        }
        self.tblBookedServices.reloadData()
        self.manageTableViewHeight()
        self.manageView()
    }
    
    func manageTableViewHeight(){
        if self.arrBookedMainService.count > 0{
        if self.arrBookedMainService[0].bookStatus == "1" || self.arrBookedMainService[0].bookStatus == "5"{
            self.constraintTableHeight.constant = CGFloat(self.arrBookedService.count*285)
            for obj in self.arrBookedService{
                if obj.status != "0"{
                    self.constraintTableHeight.constant = CGFloat(self.arrBookedService.count*350)
                }
            }
        }else{
            self.constraintTableHeight.constant = CGFloat(self.arrBookedService.count*285)
        }
        }
        //self.constraintTableHeight.constant = CGFloat(1*195)
        self.view.layoutIfNeeded()
    }
}

//MARK: - UITableview delegate
extension ConfirmBookingVC {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrBookedService.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let  cell = tableView.dequeueReusableCell(withIdentifier: "SubServiceCell", for: indexPath) as! SubServiceCell
        cell.indexPath = indexPath
        let objBookedService = self.arrBookedService[indexPath.row]
        let objMain = arrBookedMainService[0]
        
        cell.btnOnTheWay.isHidden = true
        cell.btnStartService.isHidden = true
        cell.btnEndServices.isHidden = true
        cell.btnTrack.isHidden = true
        
        if objBookedService.staffImage != "" {
            if let url = URL(string: objBookedService.staffImage){
                //cell.imgProfie.af_setImage(withURL: url, placeholderImage:#imageLiteral(resourceName: "cellBackground"))
                cell.imgProfie.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "cellBackground"))
            }
        }else{
            cell.imgProfie.image = #imageLiteral(resourceName: "cellBackground")
        }
        cell.lblName.text = objBookedService.artistServiceName
        cell.lblArtistName.text = objBookedService.staffName
        cell.lblPrice.text = objBookedService.bookingPrice
        /////
        let doubleStr = String(format: "%.2f", Double(cell.lblPrice.text!)!)
        cell.lblPrice.text = doubleStr
        ////
        cell.lblPrice.text = "£" + cell.lblPrice.text!
        
        let formatter  = DateFormatter()
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        formatter.dateFormat = "yyyy-MM-dd"
        let date = formatter.date(from: objBookedService.bookingDate)
        formatter.dateFormat = "dd/MM/yyyy"
        let strDate = formatter.string(from: date!)
        
        cell.lblDuration.text = strDate + ", " + objBookedService.startTime + " - " + objBookedService.endTime
        if self.arrBookedMainService[0].bookStatus == "1" || self.arrBookedMainService[0].bookStatus == "5"{
            cell.viewStatus.isHidden = true
            for obj in self.arrBookedService{
                if obj.status != "0"{
                    cell.viewStatus.isHidden = false
                }
            }
        }else{
            cell.viewStatus.isHidden = true
        }
        
        let strStartTime =  objBookedService.startTime
        let dates = objAppShareData.getTimeFromTime(strDate: objBookedService.bookingDate+" "+strStartTime)
        let formatters  = DateFormatter()
        formatters.dateFormat = "yyyy-MM-dd hh:mm a"
        formatters.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        formatters.timeZone = NSTimeZone(abbreviation: "UTC") as TimeZone?
        let dataDate = formatters.date(from: dates)!
        let currentDate = objAppShareData.matchesTwoDate(dateA: dataDate, type: "pending")
        
        
        cell.lblStatusService.textColor = #colorLiteral(red: 0.9200255673, green: 0.4889633489, blue: 0.08423942367, alpha: 1)
        cell.lblStatusService.text = "Pending"
        if objMain.bookStatus == "1" || objMain.bookStatus == "3" || objMain.bookStatus == "5"{
            if objBookedService.status == "0"{
                cell.lblStatusService.textColor = #colorLiteral(red: 0, green: 0.7333333333, blue: 0.1529411765, alpha: 1)
                cell.lblStatusService.text = "Confirmed"
            }else if objBookedService.status == "1"{
                cell.lblStatusService.textColor = #colorLiteral(red: 0, green: 0.7333333333, blue: 0.1529411765, alpha: 1)
                cell.lblStatusService.text = "On the way"
            }else if objBookedService.status == "2"{
                cell.lblStatusService.textColor = #colorLiteral(red: 0, green: 0.851996243, blue: 0.8281483054, alpha: 1)
                cell.lblStatusService.text = "On going"
            }else if objBookedService.status == "3" && objMain.bookStatus == "5"{
                cell.lblStatusService.textColor = #colorLiteral(red: 0, green: 0.7333333333, blue: 0.1529411765, alpha: 1)
                cell.lblStatusService.text = "Service end"
            }else if objBookedService.status == "4"{
                    cell.lblStatusService.textColor = #colorLiteral(red: 0, green: 0.7333333333, blue: 0.1529411765, alpha: 1)
                    cell.lblStatusService.text = "Reached at customer location"
            }else{
                cell.lblStatusService.textColor = #colorLiteral(red: 0, green: 0.7333333333, blue: 0.1529411765, alpha: 1)
                cell.lblStatusService.text = "Service end"
            }
        }
        
        
        if (objMain.bookStatus == "1" || objMain.bookStatus == "5" ) && (objMain.paymentType == "1" && objMain.paymentStatus == "1") || (objMain.paymentType == "2" && (objMain.bookStatus == "1" || objMain.bookStatus == "5")){
            
            if objBookedService.status == "0" && objBookedService.staffId == self.myId && objMain.bookingType == "2"{
                cell.btnOnTheWay.isHidden = self.manageTrackButton()
                cell.btnTrack.isHidden = true
            }
            
            if objBookedService.status != "2" && objBookedService.status != "3" && currentDate == true && objBookedService.staffId == self.myId {
                ///// New change
                if self.arrBookedMainService[0].bookingType == "2"{
                    if objBookedService.status == "0" {
                        cell.btnOnTheWay.isHidden = false
                        cell.btnStartService.isHidden = true
                        cell.btnTrack.isHidden = true
                    }else{
                        cell.btnOnTheWay.isHidden = true
                        cell.btnStartService.isHidden = false
                        cell.btnTrack.isHidden = false
                    }
                }else{
                    cell.btnOnTheWay.isHidden = true
                    cell.btnStartService.isHidden = false
                    cell.btnTrack.isHidden = false
                }
                ////
                //cell.btnOnTheWay.isHidden = true
                //cell.btnStartService.isHidden = false
                //cell.btnTrack.isHidden = false
            }else if objBookedService.status == "2" && (objMain.artistId == self.myId || self.myId == objBookedService.staffId) {
                cell.btnEndServices.isHidden = false
                cell.btnTrack.isHidden = false
            }else  if objBookedService.status == "3"  {
                cell.btnTrack.isHidden = true
            }else if objBookedService.status == "4" && (objMain.artistId == self.myId || self.myId == objBookedService.staffId) {
                 cell.btnTrack.isHidden = false
            }else  if objMain.bookStatus == "5" && objBookedService.status == "1" && objBookedService.staffId == self.myId {
                cell.btnTrack.isHidden = false
            }
            
             if objBookedService.status != "2" && objBookedService.status != "3" && currentDate == true && (objMain.artistId == self.myId || self.myId != objBookedService.staffId) && objMain.artistId == self.myId {
                //// New change
                if self.arrBookedMainService[0].bookingType == "2"{
                    if objBookedService.status == "0" {
                        cell.btnOnTheWay.isHidden = false
                        cell.btnStartService.isHidden = true
                        cell.btnTrack.isHidden = true
                    }else{
                        cell.btnOnTheWay.isHidden = true
                        cell.btnStartService.isHidden = false
                        cell.btnTrack.isHidden = false
                    }
                }else{
                    cell.btnOnTheWay.isHidden = true
                    cell.btnStartService.isHidden = false
                    cell.btnTrack.isHidden = false
                }
                ////
                //cell.btnOnTheWay.isHidden = true
                //cell.btnStartService.isHidden = false
                //cell.btnTrack.isHidden = false
            }
           
        }
        
        if self.arrBookedMainService[0].bookingType == "1"{
            cell.btnTrack.isHidden = true
            cell.btnOnTheWay.isHidden = true
        }else if self.arrBookedMainService[0].bookingType == "2"{
            if !currentDate{
               //cell.btnOnTheWay.isHidden = true
            }
        }
        
//        if objMain.bookStatus != "0" && self.myId == objMain.artistId && objMain.customerType != "walking"{
//            cell.btnReport.isHidden = false
//        }else if objMain.bookStatus != "0" && self.myId == objBookedService.staffId && objMain.customerType != "walking"{
//            cell.btnReport.isHidden = false
//        }else{
//            cell.btnReport.isHidden = true
//        }
        
        if objMain.customerType != "walking" && objMain.bookStatus != "0" && self.myId == objBookedService.staffId  && currentDate == true{
            cell.btnReport.isHidden = false
        }else{
            cell.btnReport.isHidden = true
        }
        
        
        cell.btnProfile.tag = indexPath.row
        cell.btnProfile.superview?.tag = indexPath.section
        cell.btnProfile.addTarget(self, action: #selector(btnGoToProfile(_:)), for: .touchUpInside)
        
        cell.btnOnTheWay.tag = indexPath.row
        cell.btnOnTheWay.addTarget(self, action:#selector(btnOnTheWayTapped(sender:)) , for: .touchUpInside)
        cell.btnStartService.tag = indexPath.row
        cell.btnStartService.addTarget(self, action:#selector(btnStartServiecTapped(sender:)) , for: .touchUpInside)
        cell.btnEndServices.tag = indexPath.row
        cell.btnEndServices.addTarget(self, action:#selector(btnEndServiceTapped(sender:)) , for: .touchUpInside)
        cell.btnTrack.tag = indexPath.row
        cell.btnTrack.addTarget(self, action:#selector(btnTrackTapped(sender:)) , for: .touchUpInside)
        cell.btnReport.tag = indexPath.row
        cell.btnReport.addTarget(self, action:#selector(btnReportTapped(sender:)) , for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    
    func manageTrackButton()->Bool{
        var hiddenOnTheWay = false
        let objBooking = self.arrBookedMainService[0]
                if objBooking.bookStatus == "1" || objBooking.bookStatus == "3" || objBooking.bookStatus == "5"{
                    for obj in objBooking.arrBookingInfo{
                        if (obj.status == "1"  || obj.status == "2")  && self.artistId == obj.staffId{
                            hiddenOnTheWay =  true
                        }
                    }
                }
        return hiddenOnTheWay
    }
    
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        var numberOfChars:Int = 0
        let newText = (txtAddCommentRatingPopup.text as NSString).replacingCharacters(in: range, with: text)
        numberOfChars = newText.count
        if numberOfChars >= 150 && text != ""{
            return false
        }else{
        }
        return numberOfChars < 150
    }
    
    
    @objc  func btnOnTheWayTapped(sender: UIButton!){
        
        if self.arrBookedService.count == 0 || self.arrBookedMainService.count ==  0 {
            return
        }
        
        let objBooking = arrBookedMainService[0]
        let objService = arrBookedService[sender.tag]
        
        let paramCustomer = ["CustomerLat":objBooking.latitude,
                                                "CustomerLong":objBooking.longitude,
                                                "CustomerId":objBooking.userId,
                                                "CustomerBookingInfoId":objService._id,
                                                "CustomerBookingId":objBooking._id]
        
        UserDefaults.standard.set(paramCustomer, forKey: UserDefaults.keys.ParamCustomerTracking)
        
        var userType = ""
        if self.artistId == "" || self.artistId == self.myId{
        }else{ userType = "staff"}
        
let param = [  "userId":objBooking.userId,//userId,
                          "bookingId":objBooking._id,
                          "id":objService._id,
                          "serviceName":objService.artistServiceName,
                          "paymentType":objBooking.paymentType,
                          "price":objService.bookingPrice,
                          "userType":userType,
                          "artistId":self.myId,
                          "customerType":objBooking.customerType,
                          "type":"on the way"]
        
            callWebserviceForGet_AcceptRejectCount(dict: param, webURL: WebURL.bookingupdate,fromCompleteService:false)

    }
    
    
    @objc  func btnTrackTapped(sender: UIButton!){
        let objBooking = arrBookedMainService[0]
        let objService = arrBookedService[sender.tag]
    
            if  objAppShareData.onTracking == false {
                objAppShareData.UpdateLocationOnDatabase()
            }
            objAppShareData.onTracking = true
            let param = [  "userId":objBooking.userId,//userId,
                           "bookingId":objBooking._id,
                           "id":objService._id,
                           "serviceName":objService.artistServiceName,
                           "paymentType":objBooking.paymentType,
                           "price":objService.bookingPrice,
                           "type":"on the way"]
            let sb = UIStoryboard(name:"Booking",bundle:Bundle.main)
            
            let objChooseType = sb.instantiateViewController(withIdentifier:"TrackMapVC") as! TrackMapVC
            objChooseType.hidesBottomBarWhenPushed = true
            objChooseType.objBookedMainService = objBooking
            objChooseType.objBookedService = objService
            self.navigationController?.pushViewController(objChooseType, animated: true)
    }
    
    
    @objc  func btnReportTapped(sender: UIButton!){
        let objBooking = self.arrBookedMainService[0]
        let objService = arrBookedService[sender.tag]
        if objService.arrReportList.count > 0{
            let sb = UIStoryboard(name:"Booking",bundle:Bundle.main)
            let objChooseType = sb.instantiateViewController(withIdentifier:"ReportVC") as! ReportVC
            objChooseType.hidesBottomBarWhenPushed = true
            objChooseType.objBookingInfo = objService
            objChooseType.objBooking = objBooking
            if objService.arrReportList.last?.status == "1"{
                objChooseType.reportType = 1
            }else{
                objChooseType.reportType = 2
            }
            self.navigationController?.pushViewController(objChooseType, animated: true)
        }else{
            let sb = UIStoryboard(name:"Booking",bundle:Bundle.main)
            let objChooseType = sb.instantiateViewController(withIdentifier:"ReportVC") as! ReportVC
            objChooseType.hidesBottomBarWhenPushed = true
            objChooseType.objBookingInfo = objService
            objChooseType.objBooking = objBooking
            objChooseType.reportType = 0
            self.navigationController?.pushViewController(objChooseType, animated: true)
        }
    }
    
    @objc  func btnStartServiecTapped(sender: UIButton!){
        if self.arrBookedService.count == 0 || self.arrBookedMainService.count ==  0 {
            return
        }
        let objBooking = arrBookedMainService[0]
        let objService = arrBookedService[sender.tag]
  
            
            let param = [  "userId":objBooking.userId,//userId,//
                "bookingId":objBooking._id,//
                "id":objService._id,
                "serviceName":objService.artistServiceName,
                "paymentType":objBooking.paymentType,
                "price":objService.bookingPrice,
                "artistId":self.myId,
                "customerType":objBooking.customerType,
                "type":"start" ]
               callWebserviceForGet_AcceptRejectCount(dict: param, webURL: WebURL.bookingupdate,fromCompleteService:false)
    }
    
    
    @objc  func btnEndServiceTapped(sender: UIButton!){
        if self.arrBookedService.count == 0 || self.arrBookedMainService.count ==  0 {
            return
        }
        let objBooking = arrBookedMainService[0]
        let objService = arrBookedService[sender.tag]
        
        var notDone = 0
        for objNew in objBooking.arrBookingInfo{
            if objNew.status != "3"{
                notDone = notDone+1
            }
        }
        
        
        if objBooking.artistId == self.myId &&  (objBooking.arrBookingInfo.count == 1  ||   notDone == 1) && objBooking.paymentType == "1" {
            print("Last Service of Booking and self owner")
            let param = [  "userId":objBooking.userId,//userId,
                "bookingId":objBooking._id,
                "id":objService._id,
                "serviceName":objService.artistServiceName,
                "paymentType":objBooking.paymentType,
                "price":objService.bookingPrice,
                "artistId":self.myId,
                "customerType":objBooking.customerType,
                "type":"complete" ]
            
            self.completeBooking(strType: "complete",fromCompleteService:true)

            //callWebserviceForGet_AcceptRejectCount(dict: param, webURL: WebURL.bookingupdate,fromCompleteService:true)
        }else{
            let param = [  "userId":objBooking.userId,//userId,
                "bookingId":objBooking._id,
                "id":objService._id,
                "serviceName":objService.artistServiceName,
                "paymentType":objBooking.paymentType,
                "price":objService.bookingPrice,
                "artistId":self.myId,
                "customerType":objBooking.customerType,
                "type":"end" ]
               callWebserviceForGet_AcceptRejectCount(dict: param, webURL: WebURL.bookingupdate,fromCompleteService:false)
        }
    }
}


//MARK: - BookingPendingCellDelegate btn Action
extension ConfirmBookingVC {
    @IBAction func btnCompleteServiceAction(_ sender: UIButton) {
        self.completeBooking(strType: "complete",fromCompleteService:true)
    }
    
    func completeBooking(strType:String,fromCompleteService:Bool){
        if self.arrBookedService.count == 0 || self.arrBookedMainService.count ==  0 {
            return
        }
        let objBooking = self.arrBookedMainService[0]
        let bookingId = objBooking._id
        let serviceId = objBooking.allServiceId
        let subServiceId = objBooking.allSubServiceId
        let artistServiceId = objBooking.allArtictServiceId
      
       
        
            let param = [ "artistId":self.artistId,
                          "userId":objBooking.userId,//userId,
                          "bookingId":bookingId,
                          "serviceId":serviceId,
                          "subserviceId":subServiceId,
                          "artistServiceId":artistServiceId,
                          "type":strType,
                          "customerType":objBooking.customerType,
                          "paymentType":objBooking.paymentType
            ]
            callWebserviceForGet_AcceptRejectCount(dict: param, webURL: WebURL.bookingAction,fromCompleteService:fromCompleteService)
     
    }
    
    @IBAction func btnSratRatting(_ sender: UIButton) {
        self.popUpRatting()
    }
    @IBAction func btnAcceptAction(_ sender: UIButton) {
        if self.arrBookedService.count == 0 || self.arrBookedMainService.count ==  0 {
            return
        }
        let objBooking = self.arrBookedMainService[0]
        let bookingId = objBooking._id
        let serviceId = objBooking.allServiceId
        let subServiceId = objBooking.allSubServiceId
        let artistServiceId = objBooking.allArtictServiceId
        
      
            let param = [ "artistId":self.artistId,
                          "userId":objBooking.userId,//userId,
                          "bookingId":bookingId,
                          "serviceId":serviceId,
                          "subserviceId":subServiceId,
                          "artistServiceId":artistServiceId,
                          "customerType":objBooking.customerType,
                          "paymentType":objBooking.paymentType,
                          "type":"accept"
            ]
            callWebserviceForGet_AcceptRejectCount(dict: param, webURL: WebURL.bookingAction,fromCompleteService:false)
    }
    
    @IBAction func btnRejectAction(_ sender: UIButton) {
        if self.arrBookedService.count == 0 || self.arrBookedMainService.count ==  0 {
            return
        }
        let obj1 = arrBookedMainService[0]
        let objBooking = self.arrBookedMainService[0]
//        if obj1.arrUserDetail.count > 0{
//        let  obj2 = obj1.arrUserDetail
//        let objUserInfo = obj2[0]
//        let userId = objUserInfo._id
        
        let BookingId = obj1._id
        let serviceId = obj1.allServiceId
        let subServiceId = obj1.allSubServiceId
        let artistServiceId = obj1.allArtictServiceId
        
        let param = ["artistId":self.artistId,
                     "userId":objBooking.userId,//userId,
                     "bookingId":BookingId,
                     "serviceId":serviceId,
                     "subserviceId":subServiceId,
                     "artistServiceId":artistServiceId,
                     "customerType":objBooking.customerType,
                     "paymentType":objBooking.paymentType,
                     "type":"reject"]
        callWebserviceForGet_AcceptRejectCount(dict: param, webURL: WebURL.bookingAction,fromCompleteService:false)
 //   }
}
    
    
    
    func callWebserviceForGet_AcceptRejectCount(dict: [String : Any], webURL:String,fromCompleteService:Bool){
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        if self.arrBookedService.count == 0 || self.arrBookedMainService.count ==  0 {
            return
        }
        objActivity.startActivityIndicator()
        objServiceManager.requestPost(strURL: webURL, params: dict  , success: { response in
            objServiceManager.StopIndicator()
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else{
                let strStatus =  response["status"] as? String ?? ""
                let strMsg = response["message"] as? String ?? kErrorMessage
                self.viewRatingPopup.isHidden = true
                if strStatus == k_success{
                    objAppShareData.showAlert(withMessage: strMsg, type: alertType.bannerDark, on: self)
                    self.callWebserviceForGetBookedServices(fromCompleteService: fromCompleteService)
                }else{
                    self.tblBookedServices.reloadData()
                    let msg = response["message"] as! String
                    objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                }
            }
        }){ error in
            objServiceManager.StopIndicator()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
}


extension ConfirmBookingVC{
func popUpRatting(){
    let userInfo = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]  ?? ["":""]

    let userName = userInfo[UserDefaults.keys.userName] as? String ?? ""
    self.viewRatingAssignRatingPopup.value = 0.0
    self.txtAddCommentRatingPopup.text = ""
    
    if self.arrBookedMainService.count > 0{
    let objMainBooking = self.arrBookedMainService[0]
        if objMainBooking.arrUserDetail.count > 0{
            if objMainBooking.customerType == "walking"{}else{
            self.viewRatingPopup.isHidden = false
        }
            
            let objUser = objMainBooking.arrUserDetail[0]
          //  self.lblNameRatingPopup.text = userInfo[UserDefaults.keys.userName] as? String ?? ""
            let url = URL(string:  userInfo[UserDefaults.keys.profileImage] as? String ?? "" )
            if  url != nil {
                //self.imgCustomerRatingPopup.af_setImage(withURL: url!)
                self.imgCustomerRatingPopup.sd_setImage(with: url!, placeholderImage: #imageLiteral(resourceName: "cellBackground"))
            }else{
                self.imgCustomerRatingPopup.image = #imageLiteral(resourceName: "cellBackground")
            }
            
            let a = "Please review how satisfied you are with"
            let b = "@"+objUser.userName
            let c = ""// 's service."
    
    
    let StrName = NSMutableAttributedString(string: a + " ", attributes: [NSAttributedString.Key.font:UIFont(name: "Nunito-Regular", size: 17.0)!])
    
    StrName.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.lightGray, range: NSRange(location:0,length:StrName.length))
    
    let StrName1 = NSMutableAttributedString(string: b + " ", attributes: [NSAttributedString.Key.font:UIFont(name: "Nunito-SemiBold", size: 17.0)!])
    
    StrName1.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSRange(location:0,length:StrName1.length))
    
    
    let StrName2 = NSMutableAttributedString(string: c + " ", attributes: [NSAttributedString.Key.font:UIFont(name: "Nunito-Regular", size: 17.0)!])
    
    StrName2.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.lightGray, range: NSRange(location:0,length:StrName2.length))
    
    
    let combination = NSMutableAttributedString()
    
    combination.append(StrName)
    combination.append(StrName1)
    combination.append(StrName2)
    self.lblDetailRatingPopup.attributedText = combination
        }}}}



//MARK: - Custome method extension
fileprivate extension ConfirmBookingVC {
    func addAccesorryToKeyBoard(){
        let keyboardDoneButtonView = UIToolbar()
        keyboardDoneButtonView.sizeToFit()
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style:.plain, target: self, action: #selector(self.resignKeyBoard))
        doneButton.tintColor = appColor
        
        keyboardDoneButtonView.items = [flexBarButton, doneButton]
        
        txtAddCommentRatingPopup.inputAccessoryView = keyboardDoneButtonView
    }
    @objc func resignKeyBoard(){
        txtAddCommentRatingPopup.endEditing(true)
        self.view.endEditing(true)
    }
}

//MARK: - profile redirection
extension ConfirmBookingVC{
    
    @objc func btnGoToProfile(_ sender: UIButton)
    {
        let objData = arrBookedService[sender.tag].staffName
      //  let a = objData.arrUserDetail[0]
        let dicParam =  ["userName":objData]
        btnProfileListAction(dicParam: dicParam, userName: objData)
    }
    
    @IBAction func btnCustomerProfile(_ sender: UIButton) {
        if arrBookedMainService.count > 0{
           if arrBookedMainService[0].arrUserDetail.count > 0 && arrBookedMainService[0].customerType != "walking"{
                  let a = arrBookedMainService[0].arrUserDetail[0].userName
                    let dicParam =  ["userName":a]
                    btnProfileListAction(dicParam: dicParam, userName: a)
            }
        }
    }
    
    @IBAction func btnProfileAction(_ sender: UIButton) {
        let objData = arrBookedService[sender.tag].staffName
        //  let a = objData.arrUserDetail[0]
        let dicParam =  ["userName":objData]
        btnProfileListAction(dicParam: dicParam, userName: objData)
    }
    func gotoProfileVC (){
        self.view.endEditing(true)
        let sb: UIStoryboard = UIStoryboard(name: "ArtistProfile", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"SWRevealViewController") as? SWRevealViewController{
            objVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(objVC, animated: true)
        }
    }
    
    func recall() {
    }
    
    func btnProfileListAction(dicParam:[String:Any],userName:String){
        //let section = 0
        //let row = (sender as AnyObject).tag
        //let indexPath = IndexPath(row: row!, section: section)
        //let objUser = arrLikeUsersList[indexPath.row]
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        
        objServiceManager.requestPost(strURL: WebURL.profileByUserName, params: dicParam, success: { response in
            if response["status"] as? String ?? "" == "success"{
                var strId = ""
                var strType = ""
                if let dictUser = response["userDetail"] as? [String : Any]{
                    let myId = dictUser["_id"] as? Int ?? 0
                    strId = String(myId)
                    strType = dictUser["userType"] as? String ?? ""
                }
                let dic = [
                    "tabType" : "people",
                    "tagId": strId,
                    "userType":strType,
                    "title": userName ?? ""
                    ] as [String : Any]
                self.openProfileForSelectedTagPopoverWithInfo(dict: dic)
            }
        }) { error in
        }
    }
    
    func openProfileForSelectedTagPopoverWithInfo(dict : [AnyHashable: Any]){
        
        var dictTemp : [AnyHashable : Any]?
        
        dictTemp = dict
        
        if let dict1 = dict as? [String:[String :Any]] {
            if let dict2 = dict1.first?.value {
                dictTemp = dict2
            }
        }
        
        guard let dictFinal = dictTemp as? [String : Any] else { return }
        
        var strUserType: String?
        var tagId : Int?
        
        if let userType = dictFinal["userType"] as? String{
            strUserType = userType
            
            if let idTag = dictFinal["tagId"] as? Int{
                tagId = idTag
            }else{
                if let idTag = dictFinal["tagId"] as? String{
                    tagId = Int(idTag)
                }
            }
            
            var myId = ""
            if let dicUser = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] {
                myId = dicUser["_id"] as? String ?? ""
            }
            if myId == String(tagId ?? 0) ?? "" {
                //isNavigate = true
                objAppShareData.isOtherSelectedForProfile = false
                self.gotoProfileVC()
                return
            }
            
            if let strUserType = strUserType, let tagId =  tagId {
                
                objAppShareData.selectedOtherIdForProfile  = String(tagId)
                objAppShareData.isOtherSelectedForProfile = true
                objAppShareData.selectedOtherTypeForProfile = strUserType  //"artist" or "user"
                //isNavigate = true
                self.gotoProfileVC()
            }
        }
    }
}

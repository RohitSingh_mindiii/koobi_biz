

import UIKit
import FSCalendar
import DropDown
import HCSStarRatingView
import Firebase

class BookingCalendarVC: UIViewController,UITableViewDelegate,UITableViewDataSource,FSCalendarDataSource, FSCalendarDelegate, UIGestureRecognizerDelegate,UITextFieldDelegate,FSCalendarDelegateAppearance {
    
    var fromStaffSelectId = ""
    var fromStaffSelectName = ""
    var fromDetailBooking = false
    var expandCellId = "00"
    private var arrCompanyList  = [CompanyInfo]()
    var somedays : Array = [Int]()

    //notification count
    @IBOutlet weak var viewNotificationCount: UIView!
    @IBOutlet weak var lblNotificationCount: UILabel!
    var ref: DatabaseReference!

    
    //review rating view Outlet
    @IBOutlet weak var viewRatingPopup: UIView!
    @IBOutlet weak var imgCustomerRatingPopup: UIImageView!
    //    @IBOutlet weak var lblNameRatingPopup: UILabel!
    @IBOutlet weak var lblDetailRatingPopup: UILabel!
    @IBOutlet weak var viewRatingAssignRatingPopup: HCSStarRatingView!
    @IBOutlet weak var btnAddCommentRatingPopup: UIView!
    @IBOutlet weak var viewTxtAddCommentRatingPopup: UIView!
    @IBOutlet weak var txtAddCommentRatingPopup: SZTextView!
    
    
    
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var viewPandingAlert: UIView!
    @IBOutlet weak var btnHiddenSearch: UIButton!
    @IBOutlet weak var btnHiddenSearch2: UIButton!
    
    @IBOutlet weak var lblTableHeader: UILabel!
    @IBOutlet weak var viewAddService: UIView!
    @IBOutlet weak var viewStaffList: UIView!
    @IBOutlet weak var viewIncallOutcallList: UIView!
    fileprivate var _refHandle: DatabaseHandle?
    @IBOutlet weak var viewAlertErrorMsg: UIView!
    @IBOutlet weak var lblCompanyBusinessName: UILabel!
    @IBOutlet weak var lblBusinessName: UILabel!
    @IBOutlet weak var imgDropdownCompanyList: UIImageView!

    @IBOutlet weak var tblStaffList: UITableView!
    @IBOutlet weak var viewStaffFilter: UIView!
    @IBOutlet weak var imgFilter: UIImageView!

    var arrStaffList = [UserListModel]()
    var strSelecrStaffId = ""
    var strSearchText = ""
    
    @IBOutlet weak var viewShedowCalendor: UIView!
    @IBOutlet weak var lblNoDataFound: UIView!
    @IBOutlet weak var lblPandingCount: UILabel!

    let dropDown = DropDown()
    
    @IBOutlet weak var lblBookingType: UILabel!
    @IBOutlet weak var lblBookingStatus: UILabel!

//table outlat
    @IBOutlet weak var tblBookingList: UITableView!
   // @IBOutlet weak var heightTableView: NSLayoutConstraint!
    
//@IBOutlet weak var tblBookingList2: UITableView!
    //amit
    @IBOutlet weak var lblSelectedDete: UILabel!
    @IBOutlet weak var imgVwDropDown: UIImageView!
    @IBOutlet weak var HeightViewBottumTable: NSLayoutConstraint!

    
    
  //  for complete booking
    @IBOutlet weak var viewTablePaymentStatus: UIView!
    
    @IBOutlet weak var viewLocationAlert: UIView!

    
    
//amit
//Calendar
    @IBOutlet weak var calendar: FSCalendar!
    @IBOutlet weak var calendarHeightConstraint: NSLayoutConstraint!
  //  @IBOutlet weak var heightTableBookingConstraint: NSLayoutConstraint!
    
    fileprivate var arrBookingStatusToday = ["Pending Completion","All Bookings","Cancelled Bookings","Completed Bookings"]
    fileprivate var arrBookingStatusPast = ["Pending Completion","All Bookings","Cancelled Bookings","Completed Bookings"]
    fileprivate var arrBookingStatusFuture = ["Pending Completion","All Bookings","Cancelled Bookings"]
    
    fileprivate var arrBookingStatus =  ["Pending Completion","All Bookings","Cancelled Bookings","Completed Bookings"]
    private  let businessNameType:String? = UserDefaults.standard.string(forKey:UserDefaults.keys.businessNameType)
    var artistId =  ""
    var myId =  ""

    private var arrAllBooking = [Booking]()
    private var dictSelectObject = Booking(dict: ["":""])

    private var arrBookingInfo = [ModelBookingTimeSlotTodayPending]()
    private var dateSelected : Date = Date()
    private var newDate : Date = Date()
    
     private var strLatitude = ""
     private var strLongitude = ""
    private var isLocEnable = false
    private var reloadTableFor = "staff"

    ////
    @IBOutlet weak var viewChatbadgeCount: UIView!
    @IBOutlet weak var lblChatbadgeCount: UILabel!
    ////

    //@IBOutlet weak var heightTableBookingConstraint2: NSLayoutConstraint!
    // @IBOutlet weak var dataHeightConstraint: NSLayoutConstraint!
    
    fileprivate lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd"
        return formatter
    }()
    
    fileprivate lazy var scopeGesture: UIPanGestureRecognizer = {
        [unowned self] in
        let panGesture = UIPanGestureRecognizer(target: self.calendar, action: #selector(self.calendar.handleScopeGesture(_:)))
        panGesture.delegate = self
        panGesture.minimumNumberOfTouches = 1
        panGesture.maximumNumberOfTouches = 2
        return panGesture
        }()
    deinit {
     }
}

//MARK: - view Life cycle
extension BookingCalendarVC{
    
    //MARK: - SystemMethod
    override func viewDidLoad() {
        super.viewDidLoad()
        ref = Database.database().reference()
         self.viewLocationAlert.isHidden = true
        self.isLocEnable = locationAuthorization()
        self.viewNotificationCount.isHidden = true
        self.getNotificationCount()
        
        NotificationCenter.default.addObserver(self, selector: #selector(willEnterForground), name: UIApplication.willEnterForegroundNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(WillResignActive), name: UIApplication.willResignActiveNotification, object: nil)
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "refreshChatBadgeCount"), object: nil, queue: nil) { [weak self](Notification) in
            self?.refreshChatBadgeCount(Notification)
        }
        
        self.viewRatingPopup.isHidden = true
        self.viewTablePaymentStatus.isHidden = true
        
        self.imgDropdownCompanyList.isHidden = true
        if viewHeightGloble == 0{
            viewHeightGloble = Int(self.view.frame.height)
        }
        objAppShareData.fromDetailPage = false

        self.strSearchText = ""
        self.txtSearch.text = ""
        self.lblPandingCount.text = "0"
        self.lblPandingCount.isHidden = true
        self.viewSearch.isHidden = true
        self.btnHiddenSearch.isHidden = true
        self.btnHiddenSearch2.isHidden = true
        
        let userInfo = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as?  [String:Any]  ??  ["":""]
        
        self.myId = userInfo[UserDefaults.keys.userId] as? String ?? ""
        self.artistId = userInfo[UserDefaults.keys.userId] as? String ?? ""
        let businessTypes = userInfo[UserDefaults.myDetail.businessType] as? String ?? ""
        
        if businessTypes != "business"{
            let adminId = UserDefaults.standard.string(forKey: UserDefaults.keys.newAdminId) ?? ""
            if adminId == self.artistId || adminId == ""{
            }else{
                self.artistId = adminId
            }
        }
        configureCalendar(SelectDate: Date())
        self.setuoDropDownAppearance()
        self.lblBookingStatus.text = self.arrBookingStatus[0]
        self.configureView()
        self.lblBookingStatus.text = "All Bookings"
        self.addGesturesToView()
        let strDate = dateFormatter.string(from: Date())
        let a = self.getDayFromSelectedDate(strDate: strDate)
        self.clickTodayDateForChangeWeakStartDate(fromDidload: true)
        //Rohit
       // self.GetChatHistoryFromFirebase()
 
    }
    
    
    @objc func willEnterForground(_ notification: Notification) {
        self.isLocEnable = locationAuthorization()
        if !self.isLocEnable{
            self.viewLocationAlert.isHidden = false
        }else{
             self.viewLocationAlert.isHidden = true
        }
    }
    
    @objc func WillResignActive(_ notification: Notification) {
        
        objAppShareData.objAppdelegate.locationManager.stopUpdatingLocation()
        objAppShareData.objAppdelegate.locationManager.pausesLocationUpdatesAutomatically = true
        objAppShareData.objAppdelegate.locationManager.allowsBackgroundLocationUpdates = false
    }
    @objc func refreshChatBadgeCount(_ objNotify: Notification){
        if objChatShareData.chatBadgeCount == 0{
            self.viewChatbadgeCount.isHidden = true
        }else{
            self.lblChatbadgeCount.text = String(objChatShareData.chatBadgeCount)
            self.viewChatbadgeCount.isHidden = false
        }
    }
    func addGesturesToView() -> Void {
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.topToBottomSWIPE))
        swipeRight.direction = .down
        self.viewStaffList.addGestureRecognizer(swipeRight)
    }
    
    @objc func topToBottomSWIPE() ->Void {
        self.view.endEditing(true)
        self.viewStaffList.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    @IBAction func btnSetLocAlways(_ sender: UIButton){
        let alert = UIAlertController(title: "Koobi Biz", message: "''Koobi Biz'' requires Location Services to work.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", tableName: nil, comment: ""), style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: NSLocalizedString("Settings", tableName: nil, comment: ""), style: .default, handler: { _ in
            let url = URL(string: UIApplication.openSettingsURLString)!
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
             ////
               if objAppShareData.isFromNotification{
                   if objAppShareData.notificationType == "chat" {
                       let sb: UIStoryboard = UIStoryboard(name: "Chat", bundle: Bundle.main)
                       if let objVC = sb.instantiateViewController(withIdentifier:"ChatHistoryVC") as? ChatHistoryVC{
                           objVC.hidesBottomBarWhenPushed = true
                           navigationController?.pushViewController(objVC, animated: false)
                        return
                       }
                   }
               }
               ////
               
        self.viewRatingPopup.isHidden = true
        self.viewTablePaymentStatus.isHidden = true
        self.somedays.removeAll()
        //configureCalendar(SelectDate: newDate)
        self.isLocEnable = locationAuthorization()
        self.callWebserviceForGetServices()
        
        objLocationManager.getCurrentLocation()

        self.view.endEditing(true)
        self.imgDropdownCompanyList.isHidden = true
        expandCellId = "00"
        self.arrAllBooking.removeAll()
        self.indicator.stopAnimating()
        self.setuoDropDownAppearance()
        if self.fromDetailBooking == true{
            self.fromDetailBooking = false
        }else{
            self.lblPandingCount.isHidden = true
            self.strSearchText = ""
            self.txtSearch.text = ""
            self.viewSearch.isHidden = true
          //  self.clickTodayDate()
        }
        
        
        let userInfo = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]  ?? ["":""]
       
        
        let bookingSetting = userInfo["bookingSetting"] as? String ?? ""
        if bookingSetting == "0"{
            self.viewPandingAlert.isHidden = true
            arrBookingStatusToday = ["Pending Completion","All Bookings","Cancelled Bookings","Completed Bookings"]
            arrBookingStatusPast = ["Pending Completion","All Bookings","Cancelled Bookings","Completed Bookings"]
            arrBookingStatusFuture = ["Pending Completion","All Bookings","Cancelled Bookings"]
        }else{
            self.viewPandingAlert.isHidden = false
             arrBookingStatusToday = ["Pending Completion","All Bookings","Booking Request","Cancelled Bookings","Completed Bookings"]
            arrBookingStatusPast = ["Pending Completion","All Bookings","Cancelled Bookings","Completed Bookings"]
            arrBookingStatusFuture = ["Pending Completion","All Bookings","Booking Request","Cancelled Bookings"]
        }
        
        
        if objAppShareData.isFromNotification {
            self.manageNotification()
        }
        if objAppShareData.fromAddPost == true{
            objAppShareData.fromAddPost = false
            tabBarController?.selectedIndex = 4
        }
        if let id = userInfo[UserDefaults.keys.id] as? String {
            self.artistId = id
            self.myId = id
        }else if let id = userInfo[UserDefaults.keys.id] as? Int{
            self.artistId = String(id)
            self.myId = String(id)
        }
        let params1 = ["artistId":self.myId,"bookingType":"walking"]
        callWebserviceForDeleteUnCompletedBooking(param: params1)
        
        let businessTypes = userInfo[UserDefaults.myDetail.businessType] as? String ?? ""
        if businessTypes != "business"{
            let adminId = UserDefaults.standard.string(forKey: UserDefaults.keys.newAdminId) ?? ""
            if adminId == self.artistId || adminId == ""{
            }else{
                self.artistId = adminId
            }
        }
        
        let bookingType = UserDefaults.standard.string(forKey: UserDefaults.keys.mainServiceType) ?? ""
        self.manageHiddenView(bookingType: bookingType)
        
        if objAppShareData.fromDetailPage == true{
            objAppShareData.fromDetailPage = false
            configureCalendar(SelectDate: newDate)
            self.lblBookingType.text = objAppShareData.bookingType
            self.getlatestSlotDataFor(dateSelected: newDate, activity: false)
        }else{
            configureCalendar(SelectDate: Date())
            self.lblBookingType.text = "All Types"
            objAppShareData.bookingType = "All Types"
            self.clickTodayDate(activity: false, fromDidload: false)
         //   self.getlatestSlotDataFor(dateSelected: Date())
        }
        let param = ["artistId":self.artistId,"search": ""]
        self.callWebserviceForGet_artistStaff(dict: param)
        self.arrAllBooking.removeAll()
        self.tblBookingList.reloadData(); self.tblBookingList.contentSize.height = self.tblBookingList.contentSize.height+60
        self.adjustTableBookingHeight()
        self.viewStaffList.isHidden = true

        objAppShareData.callWebserviceForFindOnTheWayUser()
        
        if let dict = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]{
            var artistId = dict[UserDefaults.keys.userId] as? String ?? ""
            
            if let id = dict[UserDefaults.keys.userId] as? String{
                artistId = id
            }else if let id  = dict[UserDefaults.keys.userId] as? Int{
                artistId = String(id)
            }
            let dict2 = ["artistId":artistId]
            callWebserviceForCompanyList(dict: dict2)
        }
        
        self.view.endEditing(true)
        if  viewHeightGloble < Int(self.tabBarController?.view.frame.height ?? CGFloat(viewHeightGloble)) || self.tabBarController?.tabBar.isHidden == true{
            self.tabBarController?.tabBar.isHidden = false
            let b = Int(self.view.frame.width)
            let a = CGRect(x:0,y:0,width:b,height:viewHeightGloble)
            self.tabBarController?.view.frame = a//CGRect(x:0, y:0, width:self.view.frame.size.width, height:viewHeightGloble)
        }
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        adjustTableBookingHeight()
    }
    
    func manageNotification(){
        
        if objAppShareData.notificationType == "booking" {
            self.gotoBookingDetailModule()
            return
        }else if objAppShareData.notificationType == "feed" {
        
            let sb: UIStoryboard = UIStoryboard(name: "ArtistProfile", bundle: Bundle.main)
            if let objVC = sb.instantiateViewController(withIdentifier:"FeedDetailVC") as? FeedDetailVC {
                if let userInfo = objAppShareData.notificationInfoDict {
                        if let notiId  = userInfo["notifyId"] as? Int{
                            objVC.feedId =  String(notiId)
                            objVC.headerTitle = "Post"
                        }else  if let notiId  = userInfo["notifyId"] as? String{
                            objVC.feedId =  notiId
                            objVC.headerTitle = "Post"
                    }
                }
                objAppShareData.isFromNotification = false
                objVC.hidesBottomBarWhenPushed = true
                navigationController?.pushViewController(objVC, animated: false)
            }
            return
        }else if  objAppShareData.notificationType == "profile" {
            
            let sb: UIStoryboard = UIStoryboard(name: "ArtistProfile", bundle: Bundle.main)
            if let objVC = sb.instantiateViewController(withIdentifier:"SWRevealViewController") as? SWRevealViewController{
                objVC.hidesBottomBarWhenPushed = true
                 objAppShareData.isFromNotification = false
                navigationController?.pushViewController(objVC, animated: false)
            }
    
            
//            let sb: UIStoryboard = UIStoryboard(name: "ArtistProfile", bundle: Bundle.main)
//            if let objVC = sb.instantiateViewController(withIdentifier:"ArtistProfileVC") as? ArtistProfileVC {
//                if let userInfo = objAppShareData.notificationInfoDict {
//                }
//                objAppShareData.isFromNotification = false
//                objVC.hidesBottomBarWhenPushed = true
//                navigationController?.pushViewController(objVC, animated: false)
        //    }
            return
        }else if objAppShareData.notificationType == "certificate"{
            let sb: UIStoryboard = UIStoryboard(name: "Certificate", bundle: Bundle.main)
            if let objVC = sb.instantiateViewController(withIdentifier:"CertificateLIstVC") as? CertificateLIstVC {
                objAppShareData.isFromNotification = false
                objVC.hidesBottomBarWhenPushed = true
                navigationController?.pushViewController(objVC, animated: false)
            }
            
        }else if objAppShareData.notificationType == "adminCommission"{
            let sb = UIStoryboard(name:"BusinessAdmin",bundle:Bundle.main)
                if let objVC = sb.instantiateViewController(withIdentifier:"ReminderListVC") as? ReminderListVC{
                if let userInfo = objAppShareData.notificationInfoDict {
                    if let notiId  = userInfo["notifyId"] as? Int{
                        objVC.strUserId =  String(notiId)
                     }else  if let notiId  = userInfo["notifyId"] as? String{
                        objVC.strUserId =  notiId
                    }}
                objVC.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(objVC, animated: true)
            }
  
        }else if objAppShareData.notificationType == "invitation"{
            let sb: UIStoryboard = UIStoryboard(name: "Invitation", bundle: Bundle.main)
            if let objVC = sb.instantiateViewController(withIdentifier:"InvitationListVC") as? InvitationListVC {
                objAppShareData.isFromNotification = false
                objVC.hidesBottomBarWhenPushed = true
                navigationController?.pushViewController(objVC, animated: false)
            }
        }else if objAppShareData.notificationType == "invitationAccept"{
            let sb: UIStoryboard = UIStoryboard(name: "StaffList", bundle: Bundle.main)
            if let objVC = sb.instantiateViewController(withIdentifier:"UserListAddStaffVC") as? UserListAddStaffVC {
                objAppShareData.isFromNotification = false
                objVC.hidesBottomBarWhenPushed = true
                navigationController?.pushViewController(objVC, animated: false)
            }
        }else{
            objAppShareData.isFromNotification = false
        }
        
    }
    
}





// MARK: - UITextfield Delegate
extension BookingCalendarVC{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        
        let text = textField.text! as NSString
        
        if (text.length == 1)  && (string == "") {
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
            self.strSearchText = ""
            self.btnHiddenSearch.isHidden = false
            getArtistDataWith(andSearchText: "")
        }else{
            var substring: String = textField.text!
            substring = (substring as NSString).replacingCharacters(in: range, with: string)
            substring = substring.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            let a = substring.count
            if a > 20 && substring != ""{
                return false
            }else{
                searchAutocompleteEntries(withSubstring: substring)
            }
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
        self.strSearchText = ""
        getArtistDataWith( andSearchText: "")
        return true
    }
    
    // MARK: - searching operation
    func searchAutocompleteEntries(withSubstring substring: String) {
        if substring != "" {
            self.strSearchText = substring
            // to limit network activity, reload half a second after last key press.
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
            self.perform(#selector(self.reload), with: nil, afterDelay: 0.5)
        }
    }
    
    @objc func reload() {
        self.getArtistDataWith( andSearchText: strSearchText)
    }
    
    func getArtistDataWith(andSearchText: String) {
        self.getlatestSlotDataFor(dateSelected: self.newDate, activity: false)
    }
}

//MARK: - userDefine Method extension
extension BookingCalendarVC{
    
    func manageHiddenView(bookingType:String){
        let userInfo = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]  ?? ["":""]
        let businessType = userInfo[UserDefaults.keys.businessType] as? String ?? ""
        if businessType == "business"{
            self.viewStaffFilter.isHidden = false
        }else{
            self.viewStaffFilter.isHidden = true
        }
        
        
        if self.artistId == myId || self.artistId == ""{
                        if bookingType == "3"{
                            self.viewIncallOutcallList.isHidden = false
                        }else{
                            self.viewIncallOutcallList.isHidden = true
                            }
        }else{
                    let bookTyp = UserDefaults.standard.string(forKey:UserDefaults.keys.newAdminBookingType) ?? "Both"
                            if bookTyp == "Both" || bookTyp == ""{
                                self.viewIncallOutcallList.isHidden = false
                            }else{
                                self.viewIncallOutcallList.isHidden = true
                            }
        }
        
        
    }
    
    func configureView(){
        self.tblBookingList.delegate = self
        self.tblBookingList.dataSource = self
        self.tblStaffList.delegate = self
        self.tblStaffList.dataSource = self
        self.lblNoDataFound.isHidden = true
        self.viewStaffList.isHidden = true
        self.viewAddService.isHidden = true
        self.viewAlertErrorMsg.isHidden = true
    }
    
    func setuoDropDownAppearance()  {
        let appearance = DropDown.appearance()
        appearance.cellHeight = 40
        appearance.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        appearance.selectionBackgroundColor = UIColor(red: 0.6494, green: 0.8155, blue: 1.0, alpha: 0.2)
        appearance.separatorColor = UIColor.black// #colorLiteral(red: 0.8392156863, green: 0.8392156863, blue: 0.8392156863, alpha: 1)
        appearance.cornerRadius = 5
        appearance.shadowColor = UIColor.black //UIColor(white: 0.6, alpha: 1)
        appearance.shadowOpacity = 1
        appearance.shadowRadius = 1
        appearance.animationduration = 0.25
        appearance.textColor = .black
        appearance.textFont = UIFont (name: "Nunito-Regular", size: 14) ?? .systemFont(ofSize: 14)
    }
    
    func tableMaximumHeight(){
//             if arrAllBooking.count >= 2 {
//                self.heightTableView.constant = CGFloat(self.arrAllBooking.count*111)
//            }else if arrAllBooking.count == 0{
//                self.heightTableView.constant = 111
//            }else{
//                self.heightTableView.constant = 111
//            }
    }
    
   func NumberOfRowsInSection(){

    let newArray = self.arrAllBooking
    self.arrAllBooking.removeAll()
    for obj in newArray{
        var add = true
        for objSecond in self.arrAllBooking{
            if objSecond._id == obj._id{
                add = false
            }
        }
        if add{
            self.arrAllBooking.append(obj)
        }
    }
    
         if arrAllBooking.count == 0{
            self.lblNoDataFound.isHidden = false
        }else{
            self.lblNoDataFound.isHidden = true
        }
 }
    
    func addStaffSelfIndex(){
        
        let userData = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as! [String:Any]
        var image = ""
        image = userData["profileImage"] as? String ?? ""
        let dict1 = ["staffId":"0","staffName":"My booking","staffImage":image]
        let obj = StaffDetail(dict: dict1)
 //   objAppShareData.objStaffListBookingVC.arrStaffData.append(obj)
    }
}

// MARK: - table view Delegate and Datasource methods extension
extension BookingCalendarVC {
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        if tableView == tblStaffList{
            if reloadTableFor == "staff"{
                self.lblTableHeader.text = "Staff List"
                self.HeightViewBottumTable.constant = CGFloat(self.arrStaffList.count*50)
                return self.arrStaffList.count
            }else if reloadTableFor == "bookingType"{
                self.lblTableHeader.text = "Booking Types"
                self.HeightViewBottumTable.constant = CGFloat(135)
                return 3
            }else if reloadTableFor == "bookingStatus"{
                self.lblTableHeader.text = "Booking Status"
                self.HeightViewBottumTable.constant = CGFloat(self.arrBookingStatus.count*45)
                return self.arrBookingStatus.count
            }else  if reloadTableFor == "company"{
                self.lblTableHeader.text = "Business Types"
                self.HeightViewBottumTable.constant = CGFloat(self.arrCompanyList.count*45)
                return self.arrCompanyList.count
            }else{
                return 0
            }
        }else{
        tableMaximumHeight()
        self.NumberOfRowsInSection()
              return self.arrAllBooking.count
        }
     }
    
    
//cell for at index
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
          //  let objAllBooking = arrAllBooking[indexPath.row]
        if tableView == tblStaffList{
            if reloadTableFor == "staff"{
 
            let cellIdentifier = "BookingCalendarStaffListCell"
            if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? BookingCalendarStaffListCell{
                
                if self.arrStaffList.count > 0{
                let objStaffList = self.arrStaffList[indexPath.row]
                cell.lblName.text = objStaffList.staffName
                let url = URL(string: objStaffList.staffImage ?? "")
                if  url != nil {
                    //cell.imgUserProfile.af_setImage(withURL: url!)
                    cell.imgUserProfile.sd_setImage(with: url!, placeholderImage: #imageLiteral(resourceName: "cellBackground"))
                }else{
                    cell.imgUserProfile.image = #imageLiteral(resourceName: "cellBackground")
                }
                if indexPath.row == 0{
                    cell.imgUserProfile.image = #imageLiteral(resourceName: "group_ico")
                }
                if self.strSelecrStaffId == String(objStaffList.staffId ?? 0) {
                    cell.lblName.textColor = appColor
                }else{
                    cell.lblName.textColor = UIColor.black
                }
            }
                return cell
            }
            }else if reloadTableFor == "bookingType"{
                let cellIdentifier = "CellBottumTableList"
                if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? CellBottumTableList{
                    let arrBookingType = ["All Types","Incall","Outcall"]
                    let obj = arrBookingType[indexPath.row]
                    cell.lblTitle.text = obj
                    return cell
                }
            }else if reloadTableFor == "bookingStatus"{
                let cellIdentifier = "CellBottumTableList"
                if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? CellBottumTableList{
                    let arrBookingStatus = self.arrBookingStatus
                    let obj = arrBookingStatus[indexPath.row]
                    cell.lblTitle.text = obj
                    return cell
                }
            }else if reloadTableFor == "company"{
                let cellIdentifier = "BookingCalendarStaffListCell"
                if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? BookingCalendarStaffListCell{
                    if self.arrCompanyList.count > 0{
                        let objStaffList = self.arrCompanyList[indexPath.row]
                        cell.lblName.text = objStaffList.businessName
                        let url = URL(string: objStaffList.profileImage ?? "")
                        if  url != nil {
                            //cell.imgUserProfile.af_setImage(withURL: url!)
                            cell.imgUserProfile.sd_setImage(with: url!, placeholderImage: #imageLiteral(resourceName: "cellBackground"))
                        }else{
                            cell.imgUserProfile.image = #imageLiteral(resourceName: "cellBackground")
                        }
                    }
                    return cell
                }
            }
            return UITableViewCell()
        }else{
            if self.arrAllBooking.count >= indexPath.row+1{
            let cellIdentifier = "BookingCalendarCell"
            if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? BookingCalendarCell{
                
                let objAllBooking = self.arrAllBooking[indexPath.row]
 
                cell.cellLoadInBG(objAllBooking:objAllBooking, fromBookingRequest: false)
                if self.lblBookingStatus.text == "All Bookings" || self.lblBookingStatus.text == "Pending Completion"{
                    cell.lblBookingStatus.isHidden = false
                }else{
                    cell.lblBookingStatus.isHidden = true
                }
                
                cell.btnViewMore.tag = indexPath.row
                cell.btnViewMore.addTarget(self, action:#selector(btnViewMoreTapped(sender:)) , for: .touchUpInside)
                cell.imgViewMoreDrop.image = #imageLiteral(resourceName: "down_arrow_icon")
                cell.btnViewMore.setTitle("View More", for: .normal)
               
                
                cell.lblServiceType.text = "(Out Call)"
                if objAllBooking.bookingType == "1"{
                    cell.lblServiceType.text = "(In Call)"
                }
                if objAllBooking.customerType == "walking"{
                cell.lblServiceType.text = "(Walk-in)"
                }
                
                cell.lblPaymentStatus.isHidden = false
                cell.lblBookingType.textAlignment = .right
                
                
                
                
                if expandCellId == objAllBooking._id{
                    cell.btnViewMore.setTitle("View Less", for: .normal)
                    cell.stackExpande.isHidden = false
                    cell.imgViewMoreDrop.image = #imageLiteral(resourceName: "up_arrow_ico")
                   // cell.lblServiceType.text = "(Out Call)"
                    cell.lblTotalPrice.text = "£"+objAllBooking.totalPrice
                    ////
                    let doubleStr = String(format: "%.2f", ceil(Double(objAllBooking.totalPrice)!))
                    cell.lblTotalPrice.text = "£"+doubleStr
                    ////
                    cell.lblTotalPrice.text = "£" + String(format: "%.2f",(Double(objAllBooking.totalPrice as? String ?? "0.0")! as? Double ?? 0.0))
//                    if objAllBooking.bookingType == "1"{
//                        cell.lblServiceType.text = "(In Call)"
//                    }
                    if objAllBooking.paymentType == "1"{
                        cell.lblBookingType.text =  "Card"
                        cell.lblBookingType.textAlignment = .right
                        cell.lblPaymentStatus.isHidden = false
                        
                        if objAllBooking.paymentStatus == "0"{
                            cell.lblPaymentStatus.text = " (Unpaid)"
                            cell.lblPaymentStatus.textColor = UIColor.orange
                        }else if  objAllBooking.paymentStatus == "1"{
                            cell.lblPaymentStatus.text = " (Completed)"
                            cell.lblPaymentStatus.textColor = #colorLiteral(red: 0, green: 0.7333333333, blue: 0.1529411765, alpha: 1)
                        }else if objAllBooking.paymentStatus == "2"{
                            cell.lblPaymentStatus.text = " (Pending)"
                            cell.lblPaymentStatus.textColor = UIColor.orange
                        }else if objAllBooking.paymentStatus == "3"{
                            cell.lblPaymentStatus.text = " (Refund)"
                            cell.lblPaymentStatus.textColor = #colorLiteral(red: 0.9200255673, green: 0.4889633489, blue: 0.08423942367, alpha: 1)
                        }
                    }else{
                        cell.lblBookingType.textAlignment = .right
                        cell.lblPaymentStatus.isHidden = false
                        cell.lblBookingType.text = "Cash"
                     if objAllBooking.paymentStatus == "0"{
                        cell.lblPaymentStatus.text = " (Unpaid)"
                        cell.lblPaymentStatus.textColor = UIColor.orange
                    }else if  objAllBooking.paymentStatus == "1"{
                            cell.lblPaymentStatus.text = " (Paid)"
                            cell.lblPaymentStatus.textColor = #colorLiteral(red: 0, green: 0.7333333333, blue: 0.1529411765, alpha: 1)
                        }else if objAllBooking.paymentStatus == "2"{
                            cell.lblPaymentStatus.text = " (Unpaid)"
                            cell.lblPaymentStatus.textColor = UIColor.orange
                        }else if objAllBooking.paymentStatus == "3"{
                            cell.lblPaymentStatus.text = " (Refund)"
                            cell.lblPaymentStatus.textColor = #colorLiteral(red: 0.9200255673, green: 0.4889633489, blue: 0.08423942367, alpha: 1)
                        }
                   }
                }else{
                    cell.stackExpande.isHidden = true
                }
    
                cell.lblDate.isHidden = false
                cell.lblTime.isHidden = false
                
                cell.btnStartService.isHidden = true
                cell.btnEndServices.isHidden = true
                
                cell.btnStartService.tag = indexPath.row
                cell.btnStartService.addTarget(self, action:#selector(btnStartServiecTapped(sender:)) , for: .touchUpInside)
                
                cell.btnEndServices.tag = indexPath.row
                cell.btnEndServices.addTarget(self, action:#selector(btnEndServiecTapped(sender:)) , for: .touchUpInside)
                
  if (objAllBooking.bookStatus == "1" || objAllBooking.bookStatus == "5" ) && (objAllBooking.paymentType == "1" && objAllBooking.paymentStatus == "1") || (objAllBooking.paymentType == "2" && (objAllBooking.bookStatus == "1" || objAllBooking.bookStatus == "5")){
    
    for objBookedService in objAllBooking.arrBookingInfo{

                let strStartTime =  objBookedService.startTime
                let dates = objAppShareData.getTimeFromTime(strDate: objBookedService.bookingDate+" "+strStartTime)
                let formatters  = DateFormatter()
                formatters.dateFormat = "yyyy-MM-dd hh:mm a"
                formatters.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
                formatters.timeZone = NSTimeZone(abbreviation: "UTC") as TimeZone?
                let dataDate = formatters.date(from: dates)!
                let currentDate = objAppShareData.matchesTwoDate(dateA: dataDate, type: "pending")
        
                    if objBookedService.status != "2" && objBookedService.status != "3" && currentDate == true && objBookedService.staffId == self.myId {
                        ///// New change
                        if objAllBooking.bookingType == "2" {
                            if objBookedService.status == "1"{
                            cell.btnStartService.isHidden = false
                            }
                        }else{
                           cell.btnStartService.isHidden = false
                        }
                        ////
                        //cell.btnStartService.isHidden = false
                        break
                    }
        
                    if objBookedService.status != "2" && objBookedService.status != "3" && currentDate == true && (objAllBooking.artistId == self.myId || self.myId != objBookedService.staffId) && objAllBooking.artistId == self.myId {
                        ///// New change
                        if objAllBooking.bookingType == "2" {
                            if objBookedService.status == "1"{
                                cell.btnStartService.isHidden = false
                            }
                        }else{
                            cell.btnStartService.isHidden = false
                        }
                        ////
                        //cell.btnStartService.isHidden = false
                        break
                    }
        
                    if objBookedService.status == "2" && objBookedService.status != "3" && currentDate == true && objBookedService.staffId == self.myId {
                        cell.btnEndServices.isHidden = false
                        break
                    }
        
                    if objBookedService.status == "2" && objBookedService.status != "3" && currentDate == true && (objAllBooking.artistId == self.myId || self.myId != objBookedService.staffId) && objAllBooking.artistId == self.myId {
                        cell.btnEndServices.isHidden = false
                        break
                    }
        
            }
    
 }
                return cell
            }else{
                return UITableViewCell()
            }
            }else{
                return UITableViewCell()
            }
    }
        }
     
  
    
   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
    if tableView == tblStaffList{
        self.showTabbar()

    if reloadTableFor == "bookingType"{
            let arrBookingType = ["All Types","Incall","Outcall"]
            let obj = arrBookingType[indexPath.row]
            self.lblBookingType.text = obj
            objAppShareData.bookingType = obj
        self.getlatestSlotDataFor(dateSelected: self.newDate, activity: true)
    }else if reloadTableFor == "bookingStatus"{
            let arrBookingStatus = self.arrBookingStatus
            let obj = arrBookingStatus[indexPath.row]
            self.lblBookingStatus.text = obj
        self.getlatestSlotDataFor(dateSelected: self.newDate, activity: true)
    }else  if reloadTableFor == "staff"{
            let obj = self.arrStaffList[indexPath.row]
            self.strSelecrStaffId = String(obj.staffId ?? 0)
        self.getlatestSlotDataFor(dateSelected: newDate, activity: true)
    }else if reloadTableFor == "company"{
            
            if  viewHeightGloble < Int(self.tabBarController?.view.frame.height ?? CGFloat(viewHeightGloble)) || self.tabBarController?.tabBar.isHidden == true{
                self.tabBarController?.tabBar.isHidden = false
                let b = Int(self.view.frame.width)
                let a = CGRect(x:0,y:0,width:b,height:viewHeightGloble)
                self.tabBarController?.view.frame = a//CGRect(x:0, y:0, width:self.view.frame.size.width, height:viewHeightGloble)
            }
            let objCompany = self.arrCompanyList[indexPath.row]
            if let dict = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfoDic) as? [String:Any]{
                self.view.endEditing(true)
                self.lblBusinessName.text = objCompany.businessName
                UserDefaults.standard.set(objCompany.businessId, forKey: UserDefaults.keys.newAdminId)
                UserDefaults.standard.set(objCompany.serviceType, forKey: UserDefaults.keys.newAdminBookingType)
                self.viewWillAppear(false)
            }
        }
        self.viewStaffList.isHidden = true
    }else{
        let obj = self.arrAllBooking[indexPath.row]
         dictSelectObject = obj
//      self.viewTablePaymentStatus.isHidden = false
//        self.hiddenTabbar()

            let sb = UIStoryboard(name:"Booking",bundle:Bundle.main)
            let objChooseType = sb.instantiateViewController(withIdentifier:"ConfirmBookingVC") as! ConfirmBookingVC
            objChooseType.hidesBottomBarWhenPushed = true
            objChooseType.strBookingId =  obj._id
            self.fromDetailBooking = true
            objAppShareData.fromDetailPage = true
        objWebserviceManager.StartIndicator()
            self.navigationController?.pushViewController(objChooseType, animated: false)
        }
    }

    
    
    
    
    
    @objc  func btnStartServiecTapped(sender: UIButton!){
       let objBooking =  self.arrAllBooking[sender.tag]
         var objService = objBooking.arrBookingInfo[0]
        
        for newObj in objBooking.arrBookingInfo{
            let strStartTime =  newObj.startTime
            let dates = objAppShareData.getTimeFromTime(strDate: newObj.bookingDate+" "+strStartTime)
            let formatters  = DateFormatter()
            formatters.dateFormat = "yyyy-MM-dd hh:mm a"
            formatters.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
            formatters.timeZone = NSTimeZone(abbreviation: "UTC") as TimeZone?
            let dataDate = formatters.date(from: dates)!
            let currentDate = objAppShareData.matchesTwoDate(dateA: dataDate, type: "pending")
            
            if newObj.status != "2" && newObj.status != "3" && currentDate == true && newObj.staffId == self.myId {
              objService = newObj
                break
            }
        }
        
        let param = [  "userId":objBooking.userId,//userId,//
            "bookingId":objBooking._id,//
            "id":objService._id,
            "serviceName":objService.artistServiceName,
            "paymentType":objBooking.paymentType,
            "price":objService.bookingPrice,
            "artistId":self.myId,
            "customerType":objBooking.customerType,
            "type":"start" ]
        callWebserviceForGet_AcceptRejectCount(dict: param, webURL: WebURL.bookingupdate,fromCompleteService:false,forPayment:false)
    }
   
    
    @objc  func btnEndServiecTapped(sender: UIButton!){
        
        let objBooking =  self.arrAllBooking[sender.tag]
        dictSelectObject = objBooking
         var objService = objBooking.arrBookingInfo[0]
   
        for newObj in objBooking.arrBookingInfo{
            let strStartTime =  newObj.startTime
            let dates = objAppShareData.getTimeFromTime(strDate: newObj.bookingDate+" "+strStartTime)
            let formatters  = DateFormatter()
            formatters.dateFormat = "yyyy-MM-dd hh:mm a"
            formatters.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
            formatters.timeZone = NSTimeZone(abbreviation: "UTC") as TimeZone?
            let dataDate = formatters.date(from: dates)!
            let currentDate = objAppShareData.matchesTwoDate(dateA: dataDate, type: "pending")
            
            if newObj.status == "2" && newObj.status != "3" && currentDate == true && newObj.staffId == self.myId {
                objService = newObj
                break
            }
        }
        
        
    /*    let param = [  "userId":objBooking.userId,//userId,//
            "bookingId":objBooking._id,//
            "id":objService._id,
            "serviceName":objService.artistServiceName,
            "paymentType":objBooking.paymentType,
            "price":objService.bookingPrice,
            "artistId":self.myId,
            "customerType":objBooking.customerType,
            "type":"end" ]
        
        callWebserviceForGet_AcceptRejectCount(dict: param, webURL: WebURL.bookingupdate,fromCompleteService:false)
        
  */
        
        
        
  
        var notDone = 0
        for objNew in objBooking.arrBookingInfo{
            if objNew.status != "3"{
                notDone = notDone+1
            }
        }
        
        
        if objBooking.artistId == self.myId &&  (objBooking.arrBookingInfo.count == 1  ||   notDone == 1) && objBooking.paymentType == "1" {
            
            print("Last Service of Booking and self owner")
            let param = [  "userId":objBooking.userId,//userId,
                "bookingId":objBooking._id,
                "id":objService._id,
                "serviceName":objService.artistServiceName,
                "paymentType":objBooking.paymentType,
                "price":objService.bookingPrice,
                "artistId":self.myId,
                "customerType":objBooking.customerType,
                "type":"complete" ]
            if objBooking.customerType == "walking"{
                self.completeBooking(strType: "complete",fromCompleteService:true)
            }else{
                self.completeBooking(strType: "complete",fromCompleteService:true)
            }
            //callWebserviceForGet_AcceptRejectCount(dict: param, webURL: WebURL.bookingupdate,fromCompleteService:true)
        }else if objBooking.artistId == self.myId &&  (objBooking.arrBookingInfo.count == 1  ||   notDone == 1) && (objBooking.paymentType == "2" || objBooking.paymentType == "3"){
            
            let param = [  "userId":objBooking.userId,//userId,
                "bookingId":objBooking._id,
                "id":objService._id,
                "serviceName":objService.artistServiceName,
                "paymentType":objBooking.paymentType,
                "price":objService.bookingPrice,
                "artistId":self.myId,
                "customerType":objBooking.customerType,
                "type":"end"]
            callWebserviceForGet_AcceptRejectCount(dict: param, webURL: WebURL.bookingupdate,fromCompleteService:false,forPayment:true)
            
        }else{
            let param = [  "userId":objBooking.userId,//userId,
                "bookingId":objBooking._id,
                "id":objService._id,
                "serviceName":objService.artistServiceName,
                "paymentType":objBooking.paymentType,
                "price":objService.bookingPrice,
                "artistId":self.myId,
                "customerType":objBooking.customerType,
                "type":"end" ]
            
            callWebserviceForGet_AcceptRejectCount(dict: param, webURL: WebURL.bookingupdate,fromCompleteService:false,forPayment:false)
        }
    }
    
    func callWebserviceForGet_AcceptRejectCount(dict: [String : Any], webURL:String,fromCompleteService:Bool,forPayment:Bool){
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        
        objActivity.startActivityIndicator()
        objServiceManager.requestPost(strURL: webURL, params: dict  , success: { response in
            objServiceManager.StopIndicator()
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else{
                let strStatus =  response["status"] as? String ?? ""
                let strMsg = response["message"] as? String ?? kErrorMessage
                 if strStatus == k_success{
                   if  self.viewRatingPopup.isHidden == false || self.viewTablePaymentStatus.isHidden == false{
                        self.showTabbar()
                        self.viewTablePaymentStatus.isHidden = true
                        self.viewRatingPopup.isHidden = true
                    }

                  //  self.viewWillAppear(false)
                    if forPayment == true{
                        self.viewTablePaymentStatus.isHidden = false
                        objAppShareData.showAlert(withMessage: strMsg, type: alertType.bannerDark, on: self)
                        self.hiddenTabbar()
                        return
                    }else{
                            if fromCompleteService == true{

                                objWebserviceManager.StopIndicator()
                                if  self.dictSelectObject.customerType != "walking"{
                                objAppShareData.showAlert(withMessage: strMsg, type: alertType.bannerDark, on: self)
                                self.popUpRatting()
                                }
                                self.getlatestSlotDataFor(dateSelected: self.newDate, activity: true)

                                let objMain = self.dictSelectObject
                                if objMain.paymentType == "1" && objMain.paymentStatus == "1"{
                                    let param = ["id":objMain._id]
                                    objAppShareData.callWebserviceForAnyAPIWithoutResponceCheck(param: param, api:WebURL.artistPayment)
                                }
                            }else{
                                objAppShareData.showAlert(withMessage: strMsg, type: alertType.bannerDark, on: self)
                                self.getlatestSlotDataFor(dateSelected: self.newDate, activity: false)
                            }
                    }
                 }else{
                     let msg = response["message"] as! String
                    objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                }
            }
        }){ error in
            objServiceManager.StopIndicator()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }

}

//MARK: - BookingPendingCellDelegate btn Action
extension BookingCalendarVC {
    
    @IBAction func btnChatAction(_ sender: UIButton) {
        let sb: UIStoryboard = UIStoryboard(name: "Chat", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"ChatHistoryVC") as? ChatHistoryVC{
            objVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(objVC, animated: true)
        }
    }
    
    @objc  func btnAcceptTapped(sender: UIButton!){
        
        let objBooking = arrAllBooking[sender.tag]
        
        let bookingId = objBooking._id
        let serviceId = objBooking.allServiceId
        let subServiceId = objBooking.allSubServiceId
        let artistServiceId = objBooking.allArtictServiceId
        
        if objBooking.arrUserDetail.count > 0 {
            
            let objUserInfo = objBooking.arrUserDetail[0]
            let userId = objUserInfo._id
            
            let param = [ "artistId":self.artistId,
                          "userId":userId,
                          "bookingId":bookingId,
                          "serviceId":serviceId,
                          "subserviceId":subServiceId,
                          "artistServiceId":artistServiceId,
                          "type":"accept"
              ]
            callWebserviceForGet_AcceptRejectCount(dict: param, webURL: WebURL.bookingAction)
        }
    }
    
    @objc  func btnRejectTapped(sender: UIButton!){
        
        let obj1 = arrAllBooking[sender.tag]
        
        let  obj2 = obj1.arrUserDetail
        let objUserInfo = obj2[0]
        let userId = objUserInfo._id
        
        let BookingId = obj1._id
        let serviceId = obj1.allServiceId
        let subServiceId = obj1.allSubServiceId
        let artistServiceId = obj1.allArtictServiceId
       
        let param = ["artistId":self.artistId,
                     "userId":userId,
                     "bookingId":BookingId,
                     "serviceId":serviceId,
                     "subserviceId":subServiceId,
                     "artistServiceId":artistServiceId,
                     "type":"reject"]
        
        callWebserviceForGet_AcceptRejectCount(dict: param, webURL: WebURL.bookingAction)
    }
    
    @objc  func btnViewMoreTapped(sender: UIButton!){
        let obj = self.arrAllBooking[sender.tag]
        if expandCellId == obj._id{
            expandCellId = "00"
        }else{
            expandCellId = obj._id
        }
        self.tblBookingList.reloadData(); self.tblBookingList.contentSize.height = self.tblBookingList.contentSize.height+60
   }
}

//MARK: - Button extension
fileprivate extension BookingCalendarVC {
    
    
    @IBAction func btnNotificationVC(_ sender: UIButton) {
        self.view.endEditing(true)
        let sb: UIStoryboard = UIStoryboard(name: "Notification", bundle: Bundle.main)
 
        let objChooseType = sb.instantiateViewController(withIdentifier:"NotificationVC") as! NotificationVC
        objChooseType.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(objChooseType, animated: true)
    }
    
    @IBAction func btnSearchOptionFilter(_ sender: UIButton) {
        self.view.endEditing(true)
        self.viewSearch.isHidden = !self.viewSearch.isHidden

        if self.strSearchText != "" || self.txtSearch.text != ""{
            self.txtSearch.text = ""
            self.strSearchText = ""
            self.getlatestSlotDataFor(dateSelected: self.newDate, activity: false)
        }
        self.btnHiddenSearch.isHidden = self.viewSearch.isHidden
         self.btnHiddenSearch2.isHidden = self.viewSearch.isHidden
    }
    
    @IBAction func btnHiddenSearchFilter(_ sender: UIButton) {
        if self.txtSearch.text == ""  || self.strSearchText == ""{
            self.viewSearch.isHidden = true
        }
        self.btnHiddenSearch.isHidden = true
        self.btnHiddenSearch2.isHidden = true
    }
    
    @IBAction func btnNewAppoinmentAction(_ sender: UIButton) {
        self.view.endEditing(true)
        let sb = UIStoryboard(name:"BookingNew",bundle:Bundle.main)
        let objChooseType = sb.instantiateViewController(withIdentifier:"WakingCustomerListVC") as! WakingCustomerListVC
        objChooseType.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(objChooseType, animated: true)
    }
    
    @IBAction func btnAddServiceAction(_ sender: UIButton) {
        self.view.endEditing(true)
        tabBarController?.selectedIndex = 3
    }
    
    @IBAction func btnCompanyDropDown(_ sender: UIButton) {
        reloadTableFor = "company"
        if self.arrCompanyList.count > 1{
            self.viewStaffList.isHidden = false
            self.tblStaffList.reloadData()
            self.hiddenTabbar()
        }
        self.view.endEditing(true)
       // tabBarController?.selectedIndex = 0
    }
    
    @IBAction func btnFilterAction(_ sender: UIButton) {
        self.viewStaffList.isHidden = !self.viewStaffList.isHidden
        if self.txtSearch.text == ""  || self.strSearchText == ""{
            self.viewSearch.isHidden = true
        }
        reloadTableFor = "staff"
        self.tblStaffList.reloadData()
        self.btnHiddenSearch.isHidden = true
        self.hiddenTabbar()
    }
    
    func hiddenTabbar(){
        self.tabBarController?.tabBar.isHidden = true
        self.tabBarController?.view.frame = CGRect(x:0, y:0, width:self.view.frame.size.width, height:self.view.frame.size.height+(self.tabBarController?.tabBar.frame.size.height)!*2)
    }
    func showTabbar(){
      if self.tabBarController?.tabBar.isHidden == true{
            self.tabBarController?.tabBar.isHidden = false
            let b = Int(self.view.frame.width)
            let a = CGRect(x:0,y:0,width:b,height:viewHeightGloble)
            self.tabBarController?.view.frame = a//CGRect(x:0, y:0, width:self.view.frame.size.width, height:viewHeightGloble)
        }
    }
    
    @IBAction func btnHideStaffListAction(_ sender: UIButton) {
        self.viewStaffList.isHidden = true
        self.showTabbar()
    }
 
    @IBAction func btnPandingAction(_ sender: UIButton) {
        if self.txtSearch.text == ""  || self.strSearchText == ""{
            self.viewSearch.isHidden = true
        }
                self.btnHiddenSearch.isHidden = true
                let sb = UIStoryboard(name:"Booking",bundle:Bundle.main)
                let objChooseType = sb.instantiateViewController(withIdentifier:"BookingPandingListVC") as! BookingPandingListVC
                objChooseType.BookingType = self.lblBookingType.text ?? "All Types"
                objAppShareData.fromDetailPage = true
                objChooseType.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(objChooseType, animated: true)
    }
    
    
    @IBAction func btnBookingType(_ sender: UIButton) {
        self.view.endEditing(true)
      
        reloadTableFor = "bookingType"
        self.viewStaffList.isHidden = false
        self.tblStaffList.reloadData()
        self.hiddenTabbar()
    }
    
    @IBAction func btnBookingStatus(_ sender: UIButton) {
        self.view.endEditing(true)
        var arrLocal = self.arrBookingStatus
        reloadTableFor = "bookingStatus"
        self.hiddenTabbar()
        self.viewStaffList.isHidden = false
        self.tblStaffList.reloadData()
    }
}


// MARK:- FSCalendar delegate
extension BookingCalendarVC{
    
    func setCalenderScopeWeek(){
        
        self.calendar.setScope(.week, animated: true)
        UIView.animate(withDuration: 0.4, animations: {
            self.imgVwDropDown.transform = CGAffineTransform(rotationAngle: (0.0 * CGFloat(Double.pi)) / 180.0)
        })
    }
    
    func setCalenderScopeMonth(){
        
        self.calendar.setScope(.month, animated: true)
        UIView.animate(withDuration: 0.4, animations: {
            self.imgVwDropDown.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) / 180.0)
        })
    }
    
    
    
    @IBAction func toggleCalenderAction(sender: AnyObject) {
        
        if self.calendar.scope == .month {
            setCalenderScopeWeek()
        } else {
            setCalenderScopeMonth()
        }
    }
    
    @IBAction func btnPreviousAction(sender: AnyObject) {
        
        let _calendar = Calendar.current
        var dateComponents = DateComponents()
        
        if self.calendar.scope == .month{
            dateComponents.month = -1 // For prev button -1, For next button 1
        }else{
            dateComponents.weekOfMonth = -1 // For prev button -1, For next button 1
        }

        calendar.currentPage = _calendar.date(byAdding: dateComponents, to: calendar.currentPage)!
        self.calendar.setCurrentPage(calendar.currentPage, animated: true)
        self.animationLeft(viewAnimation: self.calendar)
    }
    
    @IBAction func btnNextAction(sender: AnyObject) {
        let _calendar = Calendar.current
        var dateComponents = DateComponents()
        if self.calendar.scope == .month{
            dateComponents.month = 1 // For prev button -1, For next button 1
        }else{
            dateComponents.weekOfMonth = 1 // For prev button -1, For next button 1
        }
        calendar.currentPage = _calendar.date(byAdding: dateComponents, to: calendar.currentPage)!
        self.calendar.setCurrentPage(calendar.currentPage, animated: true)
        self.animationReight(viewAnimation: self.calendar)
    }
    
    private func animationReight(viewAnimation: UIView) {
        UIView.animate(withDuration: 0.0, animations: {
            viewAnimation.frame.origin.x = +viewAnimation.frame.width
        }) { (_) in
            UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseIn], animations: {
                viewAnimation.frame.origin.x -= viewAnimation.frame.width
            })
        }
    }
    private func animationLeft(viewAnimation: UIView) {
        UIView.animate(withDuration: 0.3, animations: {
            viewAnimation.frame.origin.x = +viewAnimation.frame.width
        }) { (_) in
            UIView.animate(withDuration: 0.0, delay: 0, options: [.curveEaseIn], animations: {
                viewAnimation.frame.origin.x -= viewAnimation.frame.width
            })
        }
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, fillDefaultColorFor date: Date) -> UIColor? {
        let myCalendar = Calendar(identifier: .gregorian)
        let weekDay = myCalendar.component(.weekday, from: date)
        if !somedays.contains(weekDay)
        {
            return UIColor.init(red: 153.0/255.0, green: 153.0/255.0, blue: 153.0/255.0, alpha: 1.0)
        }
        else{
            return nil
        }
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, titleDefaultColorFor date: Date) -> UIColor? {
    
        let dateString : String = self.dateFormatter.string(from:date)
        let myCalendar = Calendar(identifier: .gregorian)
        let weekDay = myCalendar.component(.weekday, from: date)
        let day = myCalendar.component(.day, from: date)
        if !somedays.contains(weekDay)
        {
            return UIColor.white
        }
        else{
            return nil
        }
    }

    
    @IBAction func btnAlertSessionManageOkAction(sender: AnyObject) {
            self.viewAlertErrorMsg.isHidden = true
        UserDefaults.standard.set(self.myId, forKey: UserDefaults.keys.newAdminId)
        self.viewWillAppear(true)
    }
    
    @IBAction func btnTodayClicked(_ sender: UIButton) {
        self.clickTodayDateForChangeWeakStartDate(fromDidload: false)
        self.clickTodayDateForChangeWeakStartDate(fromDidload: false)
    }
    func clickTodayDateForChangeWeakStartDate(fromDidload:Bool){
        
        if !fromDidload{
            objActivity.startActivityIndicator()
        }
        if self.txtSearch.text == ""  || self.strSearchText == ""{
            self.viewSearch.isHidden = true
        }
        self.btnHiddenSearch.isHidden = true
        self.clickTodayDate(activity:false, fromDidload: fromDidload)

        if self.calendar.scope == .month {
            UIView.animate(withDuration: 0.4, animations: {
                //self.calendar.setCurrentPage(Date(), animated: false)
                self.setCalenderScopeWeek()
            })
            
        }
    }
    func clickTodayDate(activity:Bool,fromDidload:Bool){
        self.calendar.setCurrentPage(Date(), animated: false)
        self.calendar.select(Date())
        self.newDate = Date()
        self.lblBookingStatus.text = self.arrBookingStatusToday[0]
        self.arrBookingStatus = self.arrBookingStatusToday
     //   if !fromDidload{
        self.getlatestSlotDataFor(dateSelected: Date(), activity: activity)
     //   }
    }
      
    /**
     Tells the delegate the calendar is about to change the bounding rect.
     */
    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
       
        UIView.animate(withDuration: 0.6, animations: {
            DispatchQueue.main.async {
                self.calendarHeightConstraint.constant =  bounds.height
                self.view.layoutIfNeeded()
            }
        })
       
    }
    /**
     Tells the delegate a date in the calendar is selected by tapping.
     */
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        objActivity.startActivityIndicator()
        self.getlatestSlotDataFor(dateSelected: newDate, activity: false)
        if monthPosition == .next || monthPosition == .previous {
            calendar.setCurrentPage(date, animated: true)
        }
        let strDate1 = self.dateFormatter.string(from: date)
        let date1 = self.dateFormatter.date(from: strDate1)
        self.changeCalenderHeader(date: date1!)

        calendar.setCurrentPage(date, animated: true)
        if self.calendar.scope == .month {
            UIView.animate(withDuration: 0.4, animations: {
                //self.calendar.setCurrentPage(Date(), animated: false)
                self.setCalenderScopeWeek()
            })
        }
    }
    
    /**
     Tells the delegate the calendar is about to change the current page.
     */
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        self.changeCalenderHeader(date: calendar.currentPage)
    }
    
    /**
     Close past dates in FSCalendar
     */
    
    func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at     monthPosition: FSCalendarMonthPosition) -> Bool{
        let strDate1 = self.dateFormatter.string(from: date)
        let date1 = self.dateFormatter.date(from: strDate1)
        
        let strDate2 = self.dateFormatter.string(from: Date())
        let date2 = self.dateFormatter.date(from: strDate2)
        self.newDate = date1!

        if(date1! < date2!){
            self.lblBookingStatus.text = self.arrBookingStatusPast[0]
            self.arrBookingStatus = self.arrBookingStatusPast
        }else if (date1! > date2!){
            self.lblBookingStatus.text = self.arrBookingStatusFuture[0]
            self.arrBookingStatus = self.arrBookingStatusFuture
        }else{
            self.lblBookingStatus.text = self.arrBookingStatusToday[0]
            self.arrBookingStatus = self.arrBookingStatusToday
        }
//        if self.calendar.scope == .month {
//            UIView.animate(withDuration: 0.4, animations: {
//                self.calendar.setCurrentPage(self.newDate, animated: false)
//                self.changeCalenderHeader(date: date1!)
//                self.setCalenderScopeWeek()
//            })
//        }
        return true
    }
}

// MARK:- Notification redirection
extension BookingCalendarVC {
    
    func gotoBookingDetailModule(){
        self.view.endEditing(true)
        let sb: UIStoryboard = UIStoryboard(name: "Booking", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"ConfirmBookingVC") as? ConfirmBookingVC {
            if let userInfo = objAppShareData.notificationInfoDict {
                if let notiId  = userInfo["notifyId"] as? Int{
                    objVC.strBookingId =  String(notiId)
                }else{
                    if let notiId  = userInfo["notifyId"] as? String{
                        objVC.strBookingId =  notiId
                } }  }
            objAppShareData.isFromNotification = false
            objVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(objVC, animated: false)
        }
    }
}
extension BookingCalendarVC{
    func configureCalendar(SelectDate:Date){
        if UIDevice.current.model.hasPrefix("iPad") {
            self.calendarHeightConstraint.constant = 400
        }
        self.calendar.delegate = self
        self.calendar.dataSource = self
        self.txtSearch.delegate = self
        self.calendar.select(SelectDate)
        self.calendar.calendarHeaderView.isHidden = true
        self.changeCalenderHeader(date: SelectDate)
        self.view.addGestureRecognizer(self.scopeGesture)
        self.calendar.scope = .week
        // For UITest
        self.calendar.accessibilityIdentifier = "calendar"
    }
    
    func changeCalenderHeader(date : Date){
        let fmt = DateFormatter()
        fmt.dateFormat = "dd MMMM yyyy"
        let strCalenderHeader = fmt.string(from: date)
        let newDateForCheck = fmt.string(from: newDate)
        if strCalenderHeader == newDateForCheck{
            self.lblSelectedDete.text = strCalenderHeader
        }else{
            fmt.dateFormat = "MMMM yyyy"
            let strCalenderHeader2 = fmt.string(from: date)
             self.lblSelectedDete.text = strCalenderHeader2
        }
    }
}

//Notification
extension BookingCalendarVC{
    func adjustTableBookingHeight(){
        UIView.animate(withDuration: 0.4, animations: {
            DispatchQueue.main.async {
   //  self.heightTableBookingConstraint.constant = 300
                self.view.layoutIfNeeded()
            }
        })
    }
}

// MARK:- Custom Methods
extension BookingCalendarVC{
    
    func getlatestSlotDataFor(dateSelected : Date,activity:Bool){
        expandCellId = "00"
        let userInfo = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]  ?? ["":""]

        let formatter  = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy hh:mm a"
        var strCurrentDate = formatter.string(from: dateSelected)
        let strSelectedDay = self.getDayFromSelectedDate(strDate: strCurrentDate)
        formatter.dateFormat = "yyyy-MM-dd"
        strCurrentDate = formatter.string(from: dateSelected)
        let strSelectedDate = formatter.string(from: dateSelected)
        self.strLatitude = userInfo[UserDefaults.keys.latitude] as? String ?? ""
        self.strLongitude = userInfo[UserDefaults.keys.longitude] as? String ?? ""
        if strLatitude == "" &&  strLongitude == "" {
            strLatitude = objLocationManager.strlatitude ?? ""
            strLongitude = objLocationManager.strlongitude ?? ""
        }
        if strSelecrStaffId == "" || strSelecrStaffId == "0"{
            self.imgFilter.image = #imageLiteral(resourceName: "gray_filter_ico")
        }else{
            self.imgFilter.image = #imageLiteral(resourceName: "filter_ico")
        }
       
        
        var bType = "All Bookings"
        let arr1 = self.lblBookingStatus.text?.components(separatedBy: " ")
        if arr1?.count == 2{
            if arr1?[1] == "Bookings"{
                bType = (arr1?[0] ?? "All")+" Booking"
            }
        }
        
        print("bType = ",bType)
        if self.lblBookingStatus.text == "Pending Completion"{
            bType = "Pending Booking"
        }
        if self.lblBookingStatus.text == "Booking Request"{
            bType = "Booking Request"
        }
        
        var parameters : Dictionary = [:] as [String : Any]
        if self.businessNameType == "independent"{
            var serType = ""
            if self.lblBookingType.text == "Incall"{
                serType = "1"
            }else if self.lblBookingType.text == "Outcall"{
                serType = "2"
            }
            
            
          
            parameters  = [
                "type":bType,
                "search" : self.strSearchText,
                "date" : strSelectedDate,
                "latitude" : strLatitude,
                "longitude" : strLongitude,
                "staffId":self.strSelecrStaffId,
                "artistId":self.artistId,
                "userId":self.myId,
                "bookingType":serType]
        }else{
            var serType = ""
            if self.lblBookingType.text == "Incall"{
                serType = "1"
            }else if self.lblBookingType.text == "Outcall"{
                serType = "2"
            }
            parameters  = [
                "type":bType,
                "search" : self.strSearchText,
                "date" : strSelectedDate,
                "latitude" : strLatitude,
                "longitude" : strLongitude,
                "staffId":self.strSelecrStaffId,
                "artistId":self.artistId,
                "userId":self.myId,
                "bookingType":serType]
        }
        
        
        self.callWebserviceForGet_BookingList(dict: parameters,activity:activity)
    }
}

//MARK:- Webservice Call

extension BookingCalendarVC {
    func callWebserviceForGet_BookingList(dict: [String : Any],activity:Bool){
         self.arrAllBooking.removeAll()
        if !objServiceManager.isNetworkAvailable(){
            objWebserviceManager.StopIndicator()
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        if activity == true{
        self.indicator.startAnimating()
        }
        objServiceManager.requestPost(strURL: WebURL.artistBookingHistory, params: dict  , success: { response in
            
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                self.indicator.stopAnimating()
                objServiceManager.StopIndicator()
                sessionExpireAlertVC(controller: self)
            }else{
                print("Responce = ",response)
                let strStatus =  response["status"] as? String ?? ""
                if strStatus == k_success{
                   if let serviceCount = response["serviceCount"] as? Int{
                    
                    if self.artistId == self.myId || self.artistId == ""{
                        if serviceCount == 0 {
                            self.viewAddService.isHidden = false
                        }else{
                            self.viewAddService.isHidden = true
                        }
                    }else{
                        self.viewAddService.isHidden = true
                    }
                    }
                    if let dict = response["userData"] as? [String:Any]{
                    if let bookingType = dict["serviceType"] as? String{
                        self.manageHiddenView(bookingType: bookingType)
                    }else if let bookingType = dict["serviceType"] as? Int{
                        self.manageHiddenView(bookingType: String(bookingType))
                        }
                    }
                    self.parseResponce(response:response)
                    
                }else{
                    self.indicator.stopAnimating()
                    objServiceManager.StopIndicator()
                    self.tblBookingList.reloadData(); self.tblBookingList.contentSize.height = self.tblBookingList.contentSize.height+60
                    let msg = response["message"] as? String ?? ""
                    if msg == "Company not exist" {
                        //  UserDefaults.standard.set(obj.businessId, forKey: UserDefaults.keys.newAdminId)
                        if self.artistId == self.myId || self.artistId == ""{
                        //    objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                        }else{
                            if let dict = response["userData"] as? [String:Any]{
                             let businessName = dict["businessName"] as? String ?? ""
                            self.changeString(userName: businessName)
                            self.viewAlertErrorMsg.isHidden = false
                            }
                        }
                    }else{
                        objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                    }
                }
            }
        }){ error in
            self.indicator.stopAnimating()
            objServiceManager.StopIndicator()
            //objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
    func callWebserviceForGet_AcceptRejectCount(dict: [String : Any], webURL:String){
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            objActivity.startActivityIndicator()
            objServiceManager.requestPost(strURL: webURL, params: dict  , success: { response in
                objServiceManager.StopIndicator()
                let keyExists = response["responseCode"] != nil
                if  keyExists {
                    sessionExpireAlertVC(controller: self)
                }else{
                    let strStatus =  response["status"] as? String ?? ""
                    let strMsg = response["message"] as? String ?? kErrorMessage
                    if strStatus == k_success{
                    objAppShareData.showAlert(withMessage: strMsg, type: alertType.bannerDark, on: self)
                        self.getlatestSlotDataFor(dateSelected: self.newDate, activity: false)
                     }else{
                        self.tblBookingList.reloadData(); self.tblBookingList.contentSize.height = self.tblBookingList.contentSize.height+60
                        let msg = response["message"] as? String ?? ""
                            objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                    }
                }
            }){ error in
                objServiceManager.StopIndicator()
                objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
            }
        }
    


    //data parsing
    func parseResponce(response:[String : Any]){
        self.lblPandingCount.text = "0"
       if let bookingRequestCount = response["bookingRequestCount"] as? Int{
        if bookingRequestCount > 0{
            if self.artistId != self.myId && self.artistId != ""{
                let userInfo = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]  ?? ["":""]
                let bookingSetting = userInfo["bookingSetting"] as? String ?? ""
                if bookingSetting == "1"{
                    self.PendingCountGetlatestSlotDataFor()
                }
            }else{
                self.lblPandingCount.isHidden = false
                self.lblPandingCount.text = String(bookingRequestCount)
            }
        }else{
            self.lblPandingCount.isHidden = true
        }
        }
        if let arr = response["data"] as? [[String:Any]]{
            if arr.count > 0{
                    for strBookingt in arr{
                        let objBooking = Booking.init(dict: strBookingt)
                                if objBooking.arrBookingInfo.count > 0 {
                                    self.arrAllBooking.append(objBooking)
                                }
                    }
            }
        }
        var arrPendingCount = [String]()
        for ob in self.arrAllBooking{
            if ob.bookStatus == "0"{
                arrPendingCount.append("")
            }
        }
        self.indicator.stopAnimating()
        objServiceManager.StopIndicator()
        self.tblBookingList.reloadData(); self.tblBookingList.contentSize.height = self.tblBookingList.contentSize.height+60
        
        if self.arrAllBooking.count > 0{
            self.tblBookingList.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
        }
    }
    
    func callWebserviceForDeleteUnCompletedBooking(param:[String:Any]){
        if !objServiceManager.isNetworkAvailable(){
            return
        }
        objServiceManager.requestPost(strURL:WebURL.deleteClientBookService, params: param, success: { response in
            objAppShareData.stopTimerForHoldBookings()
            objAppShareData.objAppdelegate.clearData()
        }) { error in
        }
    }
}

extension BookingCalendarVC {
    
    func getTimeFromDate(strDate:String)-> String{
        let formatter  = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy hh:mm a"
        guard let todayDate = formatter.date(from: strDate) else { return "" }
        formatter.dateFormat = "hh:mm a"
        let strTime = formatter.string(from: todayDate)
        return strTime
    }
    
    func getDayFromSelectedDate(strDate:String)-> String{
        
        guard let weekDay = getDayOfWeek(strDate)else { return "" }
        switch weekDay {
        case 1:
            self.calendar.firstWeekday = 6+2

            return "6"//"Sun"
        case 2:
            self.calendar.firstWeekday = 0+2

            return "0"//Mon"
        case 3:
            self.calendar.firstWeekday = 1+2

            return "1"//Tue"
        case 4:
            self.calendar.firstWeekday = 2+2

            return "2"//"Wed"
        case 5:
            self.calendar.firstWeekday = 3+2

            return "3"//"Thu"
        case 6:
            self.calendar.firstWeekday = 4+2

            return "4"//"Fri"
        case 7:
            self.calendar.firstWeekday = 5+2

            return "5"//"Sat"
        default:
            self.calendar.firstWeekday = 0

             return "Day"
        }
        return ""
    }
    
    func getDayOfWeek(_ today:String) -> Int? {
        let formatter  = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy hh:mm a"// "yyyy-MM-dd"
        guard let todayDate = formatter.date(from: today) else { return nil }
        let myCalendar = Calendar(identifier: .gregorian)
        let weekDay = myCalendar.component(.weekday, from: todayDate)
        return weekDay
    }
    
    func matchesTwoDate(dateA:Date, type:String) -> Bool{
        let currentDate = Date()
        let CurrentTimeZone = NSTimeZone(abbreviation: "GMT")
        let SystemTimeZone = NSTimeZone.system as NSTimeZone
        let currentGMTOffset: Int? = CurrentTimeZone?.secondsFromGMT(for: currentDate)
        let SystemGMTOffset: Int = SystemTimeZone.secondsFromGMT(for: currentDate)
        let interval = TimeInterval((SystemGMTOffset - currentGMTOffset!))
        let TodayDate = Date(timeInterval: interval, since: currentDate)
        let dateB:Date = TodayDate
        var addIndex = false
        
        switch dateA.compare(dateB) {
            
        case .orderedAscending:
            addIndex = true
            print(type+"Date A is later than date B")
        case .orderedDescending:
            addIndex = false
            print(type+"Date A is earlier than date B")
        case .orderedSame:
            addIndex = false
            print(type+"The two dates are the same")
        }
        return addIndex
    }
    
    func getTimeFromTime(strDate:String)-> String{
        let formatter  = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd hh:mm a"
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        let todayDate = formatter.date(from: strDate)
        let formatedDate: String = formatter.string(from: todayDate!)
        return formatedDate
    }
 }

//MARK:- Webservice Call
extension BookingCalendarVC {
    
    func callWebserviceForGet_artistStaff(dict: [String : Any]){
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        self.arrStaffList.removeAll()
        objServiceManager.requestPost(strURL: WebURL.artistStaff, params: dict  , success: { response in
            objServiceManager.StopIndicator()
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else{
                let strStatus =  response["status"] as? String ?? ""
                if strStatus == k_success{
                    self.parseResponceStaffList(response:response)
                }else{

                }
            }
        }){ error in
            if self.arrStaffList.count > 0{
                self.lblNoDataFound.isHidden = true
            }else{
                self.lblNoDataFound.isHidden = false
            }
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
    func parseResponceStaffList(response:[String : Any]){
        self.arrStaffList.removeAll()
        let userInfo = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]  ?? ["":""]

        let objMydata = UserListModel(dict: ["job":"","status":"1","staffId":"","staffName":"All Staff","staffImage":"","_id":"","businessId":""])
        self.arrStaffList.append(objMydata)
        let myName = userInfo[UserDefaults.keys.userName] ?? ""
        let myImage = userInfo[UserDefaults.keys.profileImage] ?? ""
        let myId = userInfo[UserDefaults.keys.id] ?? ""
        let Id = userInfo[UserDefaults.keys.userId] ?? ""
        print(myId)
        print(Id)

        let objMydata1 = UserListModel(dict: ["job":"","status":"1","staffId":myId,"staffName":myName,"staffImage":myImage,"_id":"","businessId":""])
        self.arrStaffList.append(objMydata1)

        if let arr = response["staffList"] as? [[String:Any]]{
            if arr.count > 0{
                for dictArtistData in arr {
                    let objArtistList = UserListModel.init(dict: dictArtistData)
                    if objArtistList.status == "1"{
                        self.arrStaffList.append(objArtistList)
                    }
                }
            }
        }
    }
    
    
    
    
    func changeString(userName:String){
        let colorBlack = UIColor.black
        
        let a = "You are no longer available in "
        let b = userName
        let c = " being as a staff "
        
        
        let StrName = NSMutableAttributedString(string: a + " ", attributes: [NSAttributedString.Key.font:UIFont(name: "Nunito-Regular", size: 17.0)!])
        
        StrName.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.lightGray, range: NSRange(location:0,length:StrName.length))
        
        let StrName1 = NSMutableAttributedString(string: b + " ", attributes: [NSAttributedString.Key.font:UIFont(name: "Nunito-SemiBold", size: 17.0)!])
        
        StrName1.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSRange(location:0,length:StrName1.length))
        
        
        let StrName2 = NSMutableAttributedString(string: c + " ", attributes: [NSAttributedString.Key.font:UIFont(name: "Nunito-Regular", size: 17.0)!])
        
        StrName2.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.lightGray, range: NSRange(location:0,length:StrName2.length))
        
        
        let combination = NSMutableAttributedString()
        
        combination.append(StrName)
        combination.append(StrName1)
        combination.append(StrName2)
        self.lblCompanyBusinessName.attributedText = combination
    }
}


extension BookingCalendarVC{
func PendingCountGetlatestSlotDataFor(){
    let userInfo = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]  ?? ["":""]
    
    self.strLatitude = userInfo[UserDefaults.keys.latitude] as? String ?? ""
    self.strLongitude = userInfo[UserDefaults.keys.longitude] as? String ?? ""
    if strLatitude == "" &&  strLongitude == "" {
        strLatitude = objLocationManager.strlatitude ?? ""
        strLongitude = objLocationManager.strlongitude ?? ""
    }
    
    var parameters : Dictionary = [:] as [String : Any]
    if self.businessNameType == "independent"{
        var serType = ""
        if self.lblBookingType.text == "Incall"{
            serType = "1"
        }else if self.lblBookingType.text == "Outcall"{
            serType = "2"
        }
        parameters  = [
            "type":"Booking Request",
            "date" : "",
            "search":self.strSearchText,
            "latitude" : strLatitude,
            "longitude" : strLongitude,
            "staffId":self.strSelecrStaffId,
            "artistId":self.artistId,
            "bookingType":serType]
    }else{
        var serType = ""
        if self.lblBookingType.text == "Incall"{
            serType = "1"
        }else if self.lblBookingType.text == "Outcall"{
            serType = "2"
        }
        parameters  = [
            "type":"Booking Request",
            "date" : "",
            "search":self.strSearchText,
            "latitude" : strLatitude,
            "longitude" : strLongitude,
            "staffId":self.strSelecrStaffId,
            "artistId":self.artistId,
            "bookingType":serType]
    }
    self.callWebserviceForGet_BookingRequestList(dict: parameters)
}

    
    func callWebserviceForGet_BookingRequestList(dict: [String : Any]){
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        objServiceManager.requestPost(strURL: WebURL.artistBookingHistory, params: dict  , success: { response in
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else{
                let strStatus =  response["status"] as? String ?? ""
                if strStatus == k_success{
                    if let arr = response["data"] as? [[String:Any]]{
                        if arr.count > 0{
                            self.lblPandingCount.isHidden = false
                        }else{
                            self.lblPandingCount.isHidden = true
                        }
                    }
                    }else{
                }
            }
        }){ error in
        }
    }
}

//MARK:- Webservice Call
extension BookingCalendarVC {
    
    func callWebserviceForCompanyList(dict: [String : Any]){
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        
        objActivity.startActivityIndicator()
        objServiceManager.requestPost(strURL: WebURL.companyInfo, params: dict  , success: { response in
        objServiceManager.StopIndicator()
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else{
                let strStatus =  response["status"] as? String ?? ""
                if strStatus == k_success{
                    self.parseResponce1(response:response)
                }else{
                    let msg = response["message"] as! String
                    objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                }
            }
        }){ error in
            objServiceManager.StopIndicator()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
}

    func parseResponce1(response:[String : Any]){
        self.arrCompanyList.removeAll()
        let dict = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] ?? ["":""]
        var userName = dict[UserDefaults.keys.userName] as? String ?? ""
        //var name = ""
        var img = ""
        //name = decoded["businessName"] as? String ?? ""
        img = dict[UserDefaults.myDetail.profileImage] as? String ?? ""
        if userName == ""{
            userName = "Business Admin"
        }
        let objMydata = CompanyInfo(dict: ["businessName":userName,"profileImage":img,"businessId":dict[UserDefaults.keys.userId] ?? ""])
        self.arrCompanyList.append(objMydata)
        
        if let arr = response["businessList"] as? [[String:Any]]{
            if arr.count > 0{
                for dictArtistData in arr {
                    let objArtistList = CompanyInfo.init(dict: dictArtistData)
                    var statusObj = "0"
                    if let status = dictArtistData["status"] as? String{
                        statusObj = status
                    }else if let status = dictArtistData["status"] as? Int{
                        statusObj = String(status)
                    }
                    if statusObj == "1"{
                        self.arrCompanyList.append(objArtistList)
                    }
                }
            }
        }
        let id = UserDefaults.standard.string(forKey: UserDefaults.keys.newAdminId)
        for obj in self.arrCompanyList{
            if obj.businessId == id{
                self.lblBusinessName.text = obj.businessName
            }
        }
        if self.arrCompanyList.count  == 1{
            self.imgDropdownCompanyList.isHidden  = true
        }else{
            self.imgDropdownCompanyList.isHidden = false
        }
//        self.setOldData()
}
}


//MARK: - check working hource
extension BookingCalendarVC{
    func callWebserviceForGetServices(){

    if !objServiceManager.isNetworkAvailable(){
    objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
    return
    }
    objActivity.startActivityIndicator()
    var strUserId = ""
    if  let userInfo = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] {
    if let id = userInfo["_id"] as? String {
    strUserId = id
    }
    }
    let dicParam = [
    "artistId":strUserId,//objAppShareData.selectedOtherIdForProfile
    "userId":myId,
    "bookingType":"walking"
    ] as [String : Any]
    
    objServiceManager.requestPostForJson(strURL: WebURL.artistService, params: dicParam, success: { response in
    print(response)
    objActivity.stopActivity()
    let keyExists = response["responseCode"] != nil
    if  keyExists {
    sessionExpireAlertVC(controller: self)
    }else {
    let strSucessStatus = response["status"] as? String ?? ""
    if  strSucessStatus == "success"{
    objActivity.stopActivity()
    self.saveData(dict:response)
    }else{
    let msg = response["message"] as? String ?? ""
    objAppShareData.showAlert(withMessage: msg , type: alertType.bannerDark,on: self)
    }}
    }) { error in
    objActivity.stopActivity()
    objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
    }
    }
    
    func saveData(dict:[String:Any]){
        
        if let dict = dict["artistDetail"] as? [String : Any]{
            if (dict["busineshours"] as? [[String : Any]]) != nil{
                let strDate = dateFormatter.string(from: Date())
                let strDay = self.getDayFromSelectedDate(strDate: strDate)
                var dayOld = Int(strDay)
                var strOpeningTime = ""
                let arrHours = dict["busineshours"] as? [[String : Any]]
                
                var staffHours = [AddStaffHours]()
                if let arr = dict["busineshours"] as? [[String : Any]]{
                    for dict in arr{
                        let objSubService = AddStaffHours.init(dict: dict)
                        staffHours.append(objSubService)
                    }
                }
                
                for dict in arrHours!{
                    let day = dict["day"] as? Int
                    let strStartTime = dict["startTime"] as? String
                    let strEndTime = dict["endTime"] as? String
                    //dayOld = 5
                    if dayOld == day{
                        if strOpeningTime.count != 0{
                            strOpeningTime = strOpeningTime + " & " + strStartTime! + " - " + strEndTime!
                        }else{
                            strOpeningTime = strStartTime! + " - " + strEndTime!
                        }
                    }
                    var newDay = day! + 2
                    if newDay == 8{
                        newDay = 1
                    }
                    self.somedays.append(newDay)
                }
                self.calendar.reloadData()
            }
        }
    }
}



//MARK: - complete booking Option
extension BookingCalendarVC{
    
    @IBAction func btnHiddenCheckPaymentStatus(_ sender: Any) {
        self.viewTablePaymentStatus.isHidden = true
        self.getlatestSlotDataFor(dateSelected: self.newDate, activity: false)
        self.showTabbar()
    }
    
    @IBAction func btnPaidPaymentStatus(_ sender: Any) {
        self.viewTablePaymentStatus.isHidden = true
        self.showTabbar()
        let objBooking = self.dictSelectObject
        var notDone = 0
        for objNew in objBooking.arrBookingInfo{
            if objNew.status != "3"{
                notDone = notDone+1
            }
        }
        if objBooking.artistId == self.myId &&  (objBooking.arrBookingInfo.count == 1 ||   notDone == 0) {
            self.completeBooking(strType: "complete",fromCompleteService:true)
        }else{
            self.completeBooking(strType:"paid",fromCompleteService:false)
        }
    }
    @IBAction func btnUnpaidPaymentStatus(_ sender: Any) {
        self.viewTablePaymentStatus.isHidden = true
        self.showTabbar()
        self.getlatestSlotDataFor(dateSelected: newDate, activity: true)
    }
    @IBAction func btnCancleRatting(_ sender: Any) {
        self.view.endEditing(true)
        self.viewRatingPopup.isHidden = true
        self.showTabbar()
        self.getlatestSlotDataFor(dateSelected: newDate, activity: true)
    }
    @IBAction func btnAddComment(_ sender: Any) {
        self.viewTxtAddCommentRatingPopup.isHidden = !self.viewTxtAddCommentRatingPopup.isHidden
    }
    
    @IBAction func btnSubmitRatting(_ sender: Any) {
        self.view.endEditing(true)
       
        let objMainBooking = self.dictSelectObject
        
        let objUser = objMainBooking.arrUserDetail[0]
        self.txtAddCommentRatingPopup.text = self.txtAddCommentRatingPopup.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        if self.viewRatingAssignRatingPopup.value == 0 {
            objAppShareData.showAlert(withMessage: "Please give rating", type: alertType.bannerDark, on: self)
//        }else if self.txtAddCommentRatingPopup.text == "" {
//            objAppShareData.showAlert(withMessage: "Please give review", type: alertType.bannerDark, on: self)
        }else{
            let date = objAppShareData.dateFormatInFormYou(forAPI: Date())
            let  parameters = [
                "artistId" : self.artistId,
                "userId" : objMainBooking.userId,//objUser._id,
                "bookingId":objMainBooking._id,
                "reviewByUser": "",
                "ratingDate":date,
                "reviewByArtist":self.txtAddCommentRatingPopup.text ?? "",
                "rating": "\(self.viewRatingAssignRatingPopup.value)"
            ]
            callWebserviceForGet_AcceptRejectCount(dict: parameters, webURL: WebURL.bookingReviewRating, fromCompleteService: false,forPayment:false)
        }
    }
    func completeBooking(strType:String,fromCompleteService:Bool){
       
        let objBooking = self.dictSelectObject
        
        let bookingId = objBooking._id
        let serviceId = objBooking.allServiceId
        let subServiceId = objBooking.allSubServiceId
        let artistServiceId = objBooking.allArtictServiceId
        
        let param = [ "artistId":self.artistId,
                      "userId":objBooking.userId,//userId,
                    "bookingId":bookingId,
                    "serviceId":serviceId,
                    "subserviceId":subServiceId,
                    "artistServiceId":artistServiceId,
                    "type":strType,
                    "customerType":objBooking.customerType,
                    "paymentType":objBooking.paymentType
        ]
        callWebserviceForGet_AcceptRejectCount(dict: param, webURL: WebURL.bookingAction,fromCompleteService:fromCompleteService,forPayment:false)
        
    }
    
    
    func popUpRatting(){
        let userInfo = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]  ?? ["":""]
        
        let userName = userInfo[UserDefaults.keys.userName] as? String ?? ""
        self.viewRatingAssignRatingPopup.value = 0.0
        self.txtAddCommentRatingPopup.text = ""
        
        if self.arrAllBooking.count > 0{
            let objMainBooking = self.dictSelectObject
            if objMainBooking.arrUserDetail.count > 0 {
                if objMainBooking.customerType == "walking"{}else{
                    self.viewRatingPopup.isHidden = false
                    self.hiddenTabbar()

                }
                
                let objUser = objMainBooking.arrUserDetail[0]
                //  self.lblNameRatingPopup.text = userInfo[UserDefaults.keys.userName] as? String ?? ""
                let url = URL(string:  userInfo[UserDefaults.keys.profileImage] as? String ?? "" )
//                if  url != nil {
//                    self.imgCustomerRatingPopup.af_setImage(withURL: url!)
//                }else{
//                    self.imgCustomerRatingPopup.image = #imageLiteral(resourceName: "cellBackground")
//                }
                
                let a = "Please review how satisfied you are with"
                let b = "@"+objUser.userName
                let c = ""// 's service."
                
                
                let StrName = NSMutableAttributedString(string: a + " ", attributes: [NSAttributedString.Key.font:UIFont(name: "Nunito-Regular", size: 17.0)!])
                
                StrName.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.lightGray, range: NSRange(location:0,length:StrName.length))
                
                let StrName1 = NSMutableAttributedString(string: b + " ", attributes: [NSAttributedString.Key.font:UIFont(name: "Nunito-SemiBold", size: 17.0)!])
                
                StrName1.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSRange(location:0,length:StrName1.length))
                
                
                let StrName2 = NSMutableAttributedString(string: c + " ", attributes: [NSAttributedString.Key.font:UIFont(name: "Nunito-Regular", size: 17.0)!])
                
                StrName2.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.lightGray, range: NSRange(location:0,length:StrName2.length))
                
                
                let combination = NSMutableAttributedString()
                
                combination.append(StrName)
                combination.append(StrName1)
                combination.append(StrName2)
                self.lblDetailRatingPopup.attributedText = combination
            }}}
}


//MARK: - firebase method
extension BookingCalendarVC{
    func getNotificationCount(){
        
        var strMyId = ""
        if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.businessInfoDic){
            if let userId = dict["_id"] as? String {
                strMyId = userId
            }
            ref.child("businessBookingBadgeCount").child(strMyId).observe(.value, with: { [weak self] (snapshot) in
                guard let strongSelf = self else {
                    return
                }
                let dict = snapshot.value as? [String:Any]
                print(dict)
                var totalCount = 0
                if let count = dict?["socialCount"] as? Int {
                    totalCount = count+totalCount
                }else if let count = dict?["socialCount"] as? String {
                    totalCount = (Int(count) ?? 0)+totalCount
                }
                if let count = dict?["bookingCount"] as? Int {
                    totalCount = count+totalCount
                }else if let count = dict?["bookingCount"] as? String {
                    totalCount = (Int(count) ?? 0)+totalCount
                }
                if totalCount == 0{
                    self?.viewNotificationCount.isHidden = true
                    self?.lblNotificationCount.text =  "0"
                }else{
                    self?.viewNotificationCount.isHidden = false
                    self?.lblNotificationCount.text =  String(totalCount)
                    if  totalCount >= 100{
                        self?.lblNotificationCount.text = "99+"
                    }
                }
            })
        }
    }
    
    
    func GetChatHistoryFromFirebase() {
        _refHandle = self.ref.child("chat_history").child(myId).observe(.value, with: { [weak self] (snapshot) -> Void in
            guard let strongSelf = self else {
                return
            }
            objChatShareData.chatBadgeCount = 0
            print(snapshot)
            
            if let arr = snapshot.value as? [Any]{
                for dicttt in arr {
                    if let dict = dicttt as? NSDictionary{
                        print("\n\ndicttt = \(dict)")
                        
                        let objChatList = ChatHistoryData()
                        
                        if let unreadMessage = dict["unreadMessage"] as? String {
                            var a = unreadMessage
                            if a == "" {
                                a = "0"
                            }
                            objChatList.strUnreadMessage = Int(a)!
                            objChatShareData.chatBadgeCount = objChatShareData.chatBadgeCount + Int(a)!
                        }else if let unreadMessage = dict["unreadMessage"] as? Int{
                            objChatList.strUnreadMessage = unreadMessage
                            objChatShareData.chatBadgeCount = objChatShareData.chatBadgeCount + unreadMessage
                        }
                    }
                }
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshChatBadgeCount"), object: nil)
            }else if let dict = snapshot.value as? NSDictionary {
                strongSelf.parseDataChatHistory(dictHistory: dict)
            }else{
                objChatShareData.chatBadgeCount = 0
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshChatBadgeCount"), object: nil)
            }
        })
    }
    
    func parseDataChatHistory(dictHistory:NSDictionary){
        for (key, element) in dictHistory {
            print("key = \(key)")
            print("element = \(element)")
            if let dict = element as? [String:Any]{
                let objChatList = ChatHistoryData()
                if let unreadMessage = dict["unreadMessage"] as? String {
                    var a = unreadMessage
                    if a == "" {
                        a = "0"
                    }
                    objChatShareData.chatBadgeCount = objChatShareData.chatBadgeCount + Int(a)!
                    objChatList.strUnreadMessage = Int(a)!
                }else if let unreadMessage = dict["unreadMessage"] as? Int{
                    objChatList.strUnreadMessage = unreadMessage
                    objChatShareData.chatBadgeCount = objChatShareData.chatBadgeCount + unreadMessage
                    
                }
            }
        }
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshChatBadgeCount"), object: nil)
    }
}

//
//  BookingPandingListVC.swift
//  MualabBusiness
//
//  Created by Mindiii on 1/16/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import UIKit
import FSCalendar
import DropDown

class BookingPandingListVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate  {
        var fromStaffSelectId = ""
        var fromStaffSelectName = ""
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var btnHiddenSearch: UIButton!

    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var viewSearchFilter: UIView!
    @IBOutlet weak var lblTableHeader: UILabel!

        @IBOutlet weak var viewStaffList: UIView!
        @IBOutlet weak var viewIncallOutcallList: UIView!
        @IBOutlet weak var tblStaffList: UITableView!
        @IBOutlet weak var viewStaffFilter: UIView!
        @IBOutlet weak var viewAllFilter: UIView!
    @IBOutlet weak var HeightViewBottumTable: NSLayoutConstraint!

        fileprivate var forBookingType = false
    
        var arrStaffList = [UserListModel]()
        var strSelecrStaffId = ""

        var myId = ""
        var strSearchText = ""
        @IBOutlet weak var lblNoDataFound: UIView!
    
        let dropDown = DropDown()
        @IBOutlet weak var lblBookingType: UILabel!
        //@IBOutlet weak var lblStaffName: UILabel!
    @IBOutlet weak var imgFilter: UIImageView!

        //table outlat
        @IBOutlet weak var tblBookingList: UITableView!
        @IBOutlet weak var heightTableView: NSLayoutConstraint!
        @IBOutlet weak var heightTableBookingConstraint: NSLayoutConstraint!

        private  let businessNameType:String? = UserDefaults.standard.string(forKey:UserDefaults.keys.businessNameType)
        var artistId =  ""
        
        private var arrAllBooking = [Booking]()
        
        private var arrBookingInfo = [ModelBookingTimeSlotTodayPending]()
        private var dateSelected : Date = Date()
        private var newDate : Date = Date()
        
        private var strLatitude = ""
        private var strLongitude = ""
        var BookingType = "All Types"

        var fromDetails = false
        //@IBOutlet weak var heightTableBookingConstraint2: NSLayoutConstraint!
    
}
    //MARK: - view Life cycle
    extension BookingPandingListVC{
        
        //MARK: - SystemMethod
        override func viewDidLoad() {
            super.viewDidLoad()
            fromDetails = false
            self.setuoDropDownAppearance()
            self.viewSearchFilter.isHidden = true
            self.btnHiddenSearch.isHidden = true
            self.lblBookingType.text = self.BookingType
            self.configureView()
            self.addGesturesToView()
        }
        func addGesturesToView() -> Void {
            let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.topToBottomSWIPE))
            swipeRight.direction = .down
            self.viewStaffList.addGestureRecognizer(swipeRight)
        }
        
        @objc func topToBottomSWIPE() ->Void {
            self.view.endEditing(true)
            self.viewStaffList.isHidden = true
        }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
        }
        
        override var preferredStatusBarStyle: UIStatusBarStyle {
            return .default
        }
        
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
            self.view.endEditing(true)
            self.indicator.stopAnimating()
           let userInfo = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]  ?? ["":""]
            self.artistId = userInfo[UserDefaults.keys.id] as? String ?? ""
            self.myId = userInfo[UserDefaults.keys.id] as? String ?? ""
            let businessType = userInfo[UserDefaults.myDetail.businessType] as? String ?? ""
            if businessType != "business"{
                    let adminId = UserDefaults.standard.string(forKey: UserDefaults.keys.newAdminId) ?? ""
                if adminId == self.artistId || adminId == ""{
                }else{
                    self.artistId = adminId
                }
                }
            
            let bookingType = UserDefaults.standard.string(forKey: UserDefaults.keys.mainServiceType) ?? ""

            
            self.getlatestSlotDataFor()
            self.manageHiddenView(bookingType: bookingType)
            let param = ["artistId":self.artistId,"search": ""]
            self.callWebserviceForGet_artistStaff(dict: param)
            self.arrAllBooking.removeAll()
            self.tblBookingList.reloadData()
            self.adjustTableBookingHeight()
            self.viewStaffList.isHidden = true
            if fromDetails == true && self.viewSearchFilter.isHidden == false  && self.strSearchText != ""{
                self.viewSearchFilter.isHidden = false
            }else{
                self.viewSearchFilter.isHidden = true
            }
            
            objAppShareData.callWebserviceForFindOnTheWayUser()
        }
        
        override func viewDidAppear(_ animated: Bool) {
            super.viewDidAppear(animated)
            adjustTableBookingHeight()
        }
    }



// MARK: - UITextfield Delegate
extension BookingPandingListVC{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        
        let text = textField.text! as NSString
        
        if (text.length == 1)  && (string == "") {
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
            self.strSearchText = ""
            self.btnHiddenSearch.isHidden = false
            getArtistDataWith(andSearchText: "")
        }else{
            var substring: String = textField.text!
            substring = (substring as NSString).replacingCharacters(in: range, with: string)
            substring = substring.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            let a = substring.count
            if a > 20 && substring != ""{
                return false
            }else{
                searchAutocompleteEntries(withSubstring: substring)
            }
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
        self.strSearchText = ""
        self.btnHiddenSearch.isHidden = false
        getArtistDataWith( andSearchText: "")
        return true
    }
    
    // MARK: - searching operation
    func searchAutocompleteEntries(withSubstring substring: String) {
        if substring != "" {
            self.strSearchText = substring
            // to limit network activity, reload half a second after last key press.
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
            self.perform(#selector(self.reload), with: nil, afterDelay: 0.5)
        }
    }
    
    @objc func reload() {
        self.getArtistDataWith( andSearchText: strSearchText)
    }
    
    func getArtistDataWith(andSearchText: String) {
        self.getlatestSlotDataFor()
    }
}


    //MARK: - userDefine Method extension
    extension BookingPandingListVC{
        func manageHiddenView(bookingType:String){
            let userInfo = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]  ?? ["":""]
            
            let businessType = userInfo[UserDefaults.keys.businessType] as? String ?? ""
            if businessType == "business"{
                self.viewStaffFilter.isHidden = false
            }else{
                self.viewStaffFilter.isHidden = true
            }
            
            
            if bookingType == "3" || bookingType == "0"  || bookingType == ""{
                self.viewIncallOutcallList.isHidden = false
            }else{
                self.viewIncallOutcallList.isHidden = true
            }
            
            if self.artistId == myId || self.artistId == ""{
                if bookingType == "3"{
                    self.viewIncallOutcallList.isHidden = false
                }else{
                    self.viewIncallOutcallList.isHidden = true
                }}else{
                let bookTyp = UserDefaults.standard.string(forKey:UserDefaults.keys.newAdminBookingType) ?? "Both"
                if bookTyp == "Both" || bookTyp == ""{
                    self.viewIncallOutcallList.isHidden = false
                }else{
                    self.viewIncallOutcallList.isHidden = true
                }
            }
            
//            if self.viewStaffFilter.isHidden == true && self.viewIncallOutcallList.isHidden == true{
//                self.viewAllFilter.isHidden = true
//            }else{
//                self.viewAllFilter.isHidden = false
//            }
        }
        func configureView(){
            self.tblBookingList.delegate = self
            self.tblBookingList.dataSource = self
            self.tblStaffList.delegate = self
            self.tblStaffList.dataSource = self
            self.lblNoDataFound.isHidden = true
            self.viewStaffList.isHidden = true
            self.txtSearch.delegate = self
            let userInfo = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]  ?? ["":""]
            self.myId = userInfo[UserDefaults.keys.id] as? String ?? ""
        }
        func setuoDropDownAppearance()  {
            let appearance = DropDown.appearance()
            appearance.cellHeight = 40
            appearance.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            appearance.selectionBackgroundColor = UIColor(red: 0.6494, green: 0.8155, blue: 1.0, alpha: 0.2)
            appearance.separatorColor = UIColor.black// #colorLiteral(red: 0.8392156863, green: 0.8392156863, blue: 0.8392156863, alpha: 1)
            appearance.cornerRadius = 5
            appearance.shadowColor = UIColor.black //UIColor(white: 0.6, alpha: 1)
            appearance.shadowOpacity = 1
            appearance.shadowRadius = 1
            appearance.animationduration = 0.25
            appearance.textColor = .black
            appearance.textFont = UIFont (name: "Nunito-Regular", size: 14) ?? .systemFont(ofSize: 14)

        }
        
        func tableMaximumHeight(){
            if arrAllBooking.count >= 2 {
                self.heightTableView.constant = CGFloat(self.arrAllBooking.count*135)
            }else if arrAllBooking.count == 0{
                self.heightTableView.constant = 135
            }else{
                self.heightTableView.constant = 135
            }
            self.heightTableView.constant = self.viewStaffList.frame.height*0.8
        }
        
        func NumberOfRowsInSection(){
            if arrAllBooking.count == 0{
                self.lblNoDataFound.isHidden = false
            }else{
                self.lblNoDataFound.isHidden = true
            }
        }
        
        
        func addStaffSelfIndex(){
            
            let userData = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as! [String:Any]
            var image = ""
            image = userData["profileImage"] as? String ?? ""
            let dict1 = ["staffId":"0","staffName":"My booking","staffImage":image]
            let obj = StaffDetail(dict: dict1)
       //     objAppShareData.objStaffListBookingVC.arrStaffData.append(obj)
        }
    }
    
    // MARK: - table view Delegate and Datasource methods extension
    extension BookingPandingListVC {
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            
            if tableView == tblStaffList{
                if forBookingType{
                    self.HeightViewBottumTable.constant = CGFloat(135)
                    return 3
                }else{
                    self.HeightViewBottumTable.constant = CGFloat(self.arrStaffList.count*50)
                    return self.arrStaffList.count
                }
            }else{
                tableMaximumHeight()
                self.NumberOfRowsInSection()
                return self.arrAllBooking.count
            }
        }
        
        //cell for at index
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
            //  let objAllBooking = arrAllBooking[indexPath.row]
            if tableView == tblStaffList{
                if forBookingType == true{
                    let cellIdentifier = "CellBottumTableList"
                    if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? CellBottumTableList{
                                let arrBookingType = ["All Types","Incall","Outcall"]
                                let obj = arrBookingType[indexPath.row]
                                cell.lblTitle.text = obj
                        return cell
                    }
                    return UITableViewCell()
                }else{
                let cellIdentifier = "BookingCalendarStaffListCell"
                if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? BookingCalendarStaffListCell{
                    let objStaffList = arrStaffList[indexPath.row]
                    cell.lblName.text = objStaffList.staffName
                    let url = URL(string: objStaffList.staffImage ?? "")
                    if  url != nil {
                        //cell.imgUserProfile.af_setImage(withURL: url!)
                        cell.imgUserProfile.sd_setImage(with: url!, placeholderImage: #imageLiteral(resourceName: "cellBackground"))
                    }else{
                        cell.imgUserProfile.image = #imageLiteral(resourceName: "cellBackground")
                    }
                    if indexPath.row == 0 {
                        cell.imgUserProfile.image = #imageLiteral(resourceName: "group_ico")
                    }
                    if self.strSelecrStaffId == String(objStaffList.staffId ?? 0) {
                        cell.lblName.textColor = appColor
                    }else{
                        cell.lblName.textColor = UIColor.black
                    }
                    return cell
                }
            }
                return UITableViewCell()
            }else{
                let cellIdentifier = "BookingCalendarCell"
                if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? BookingCalendarCell{
                    if self.arrAllBooking.count == 0{
                        return UITableViewCell()
                    }else{
                   let objAllBooking = self.arrAllBooking[indexPath.row]
                        cell.cellLoadInBG(objAllBooking:objAllBooking, fromBookingRequest: true)
                    cell.lblBookingStatus.isHidden = true
                    cell.btnAccept.isHidden = true
                    cell.btnReject.isHidden = true
                    let userInfo = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]  ?? ["":""]

                    let myId = userInfo[UserDefaults.keys.id] as? String ?? ""
                    if myId == self.artistId || self.artistId == ""   {
                    if self.artistId == objAllBooking.artistId{
                        cell.btnAccept.isHidden = false
                        cell.btnReject.isHidden = false
                    }else{
                        var btnShow = true
                        for obj in objAllBooking.arrBookingInfo{
                            if obj.staffId != myId {
                                btnShow = false
                            }
                        }
                        if btnShow == true{
                            cell.btnAccept.isHidden = false
                            cell.btnReject.isHidden = false
                        }
                    }
                    }else{
                        var btnShow = true
                        for obj in objAllBooking.arrBookingInfo{
                            if obj.staffId != myId {
                                btnShow = false
                            }
                        }
                        if btnShow == true{
                            cell.btnAccept.isHidden = false
                            cell.btnReject.isHidden = false
                        }
                    }
                    if objAllBooking.customerType == "walking"{
                        if objAllBooking.arrClientInfo.count > 0 {
                            cell.lblArtistName.text = objAllBooking.arrClientInfo[0].firstName+" "+objAllBooking.arrClientInfo[0].lastName
                        }
                        cell.imgUserProfile.image = #imageLiteral(resourceName: "cellBackground")
                    }
                    cell.btnAccept.tag = indexPath.row
                    cell.btnAccept.addTarget(self, action:#selector(btnAcceptTapped(sender:)) , for: .touchUpInside)
                    cell.btnReject.tag = indexPath.row
                    cell.btnReject.addTarget(self, action:#selector(btnRejectTapped(sender:)) , for: .touchUpInside)
                    cell.btnViewMore.tag = indexPath.row
                    cell.btnViewMore.addTarget(self, action:#selector(btnViewMoreTapped(sender:)) , for: .touchUpInside)
                    return cell
                }
                }else{
                    return UITableViewCell()
                }
            }
        }
        
        
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
            if tableView == tblStaffList{
                if forBookingType == true{
                    self.viewStaffList.isHidden = true
                    let arrBookingType = ["All Types","Incall","Outcall"]
                    let  item = arrBookingType[indexPath.row]
                    self.lblBookingType.text = item
                    objAppShareData.bookingType = item
                    self.getlatestSlotDataFor()
                }else{
                let obj = self.arrStaffList[indexPath.row]
                self.strSelecrStaffId = String(obj.staffId ?? 0)
                self.getlatestSlotDataFor()
                self.viewStaffList.isHidden = true
                }
            }else{
                if self.arrAllBooking.count == 0{
                }else{
                let obj = self.arrAllBooking[indexPath.row]
                    let sb = UIStoryboard(name:"Booking",bundle:Bundle.main)
                    let objChooseType = sb.instantiateViewController(withIdentifier:"ConfirmBookingVC") as! ConfirmBookingVC
                    objChooseType.hidesBottomBarWhenPushed = true
                    objChooseType.strBookingId =  obj._id
                    self.fromDetails = true
                    self.navigationController?.pushViewController(objChooseType, animated: true)
                }}
        }
    }
    
    //MARK: - BookingPendingCellDelegate btn Action
    extension BookingPandingListVC {
        
      @objc  func btnAcceptTapped(sender: UIButton!){
//        objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
//        return
        
            let objBooking = arrAllBooking[sender.tag]
            let bookingId = objBooking._id
            let serviceId = objBooking.allServiceId
            let subServiceId = objBooking.allSubServiceId
            let artistServiceId = objBooking.allArtictServiceId
            
            if objBooking.arrUserDetail.count > 0 {
                
                let objUserInfo = objBooking.arrUserDetail[0]
                let userId = objUserInfo._id
                
                let param = [ "artistId":self.artistId,
                              "userId":userId,
                              "bookingId":bookingId,
                              "serviceId":serviceId,
                              "subserviceId":subServiceId,
                              "artistServiceId":artistServiceId,
                              "customerType":objBooking.customerType,
                              "paymentType":objBooking.paymentType,
                              "type":"accept"
                ]
                callWebserviceForGet_AcceptRejectCount(dict: param, webURL: WebURL.bookingAction)
            }
        }
        
        @objc  func btnRejectTapped(sender: UIButton!){
            let obj1 = arrAllBooking[sender.tag]
            let objBooking = arrAllBooking[sender.tag]

//            objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
//            return
            let  obj2 = obj1.arrUserDetail
            let objUserInfo = obj2[0]
            let userId = objUserInfo._id
            
            let BookingId = obj1._id
            let serviceId = obj1.allServiceId
            let subServiceId = obj1.allSubServiceId
            let artistServiceId = obj1.allArtictServiceId
            
            let param = ["artistId":self.artistId,
                                     "userId":userId,
                                     "bookingId":BookingId,
                                     "serviceId":serviceId,
                                     "subserviceId":subServiceId,
                                     "artistServiceId":artistServiceId,
                                     "customerType":objBooking.customerType,
                                     "paymentType":objBooking.paymentType,
                                     "type":"reject"]
            callWebserviceForGet_AcceptRejectCount(dict: param, webURL: WebURL.bookingAction)
        }
        
        @objc  func btnViewMoreTapped(sender: UIButton!){
            let obj = self.arrAllBooking[sender.tag]
            let sb = UIStoryboard(name:"Booking",bundle:Bundle.main)
            let objChooseType = sb.instantiateViewController(withIdentifier:"ConfirmBookingVC") as! ConfirmBookingVC
            objChooseType.hidesBottomBarWhenPushed = true
            objChooseType.strBookingId =  obj._id
            self.navigationController?.pushViewController(objChooseType, animated: true)
        }
    }
    
    //MARK: - Button extension
    fileprivate extension BookingPandingListVC {
        
        @IBAction func btnNewAppoinmentAction(_ sender: UIButton) {
            self.view.endEditing(true)
            objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
        }
        @IBAction func btnBackAction(_ sender: UIButton) {
            self.view.endEditing(true)
            self.navigationController?.popViewController(animated: true)
        }
        @IBAction func btnFilterAction(_ sender: UIButton) {
            forBookingType = false
            self.tblStaffList.reloadData()
            self.lblTableHeader.text = "Staff List"
            if self.arrStaffList.count == 0{
                objAppShareData.showAlert(withMessage: "No staff added", type: alertType.bannerDark, on: self)
            }else{
            self.viewStaffList.isHidden = !self.viewStaffList.isHidden
            }
        }
        
        @IBAction func btnHideStaffListAction(_ sender: UIButton) {
            self.viewStaffList.isHidden = true
        }
        @IBAction func btnSearchOptionFilter(_ sender: UIButton) {
            self.view.endEditing(true)
            self.viewSearchFilter.isHidden = !self.viewSearchFilter.isHidden
            
            if self.strSearchText != "" || self.txtSearch.text != ""{
                self.txtSearch.text = ""
                self.strSearchText = ""
               self.getlatestSlotDataFor()
            }
            self.btnHiddenSearch.isHidden = self.viewSearchFilter.isHidden
        }
        
        @IBAction func btnHiddenSearchFilter(_ sender: UIButton) {
            if self.txtSearch.text == "" || self.strSearchText == ""  {
                self.viewSearchFilter.isHidden = true
            }
            self.btnHiddenSearch.isHidden = true
        }
        
        @IBAction func btnAllStaffListAction(_ sender: UIButton) {
            self.viewStaffList.isHidden = true
            self.strSelecrStaffId = ""
        }
        
        @IBAction func btnBookingType(_ sender: UIButton) {
            self.view.endEditing(true)
            let arrBookingType = ["All Types","Incall","Outcall"]
            forBookingType = true
            self.lblTableHeader.text = "Booking Types"
            self.viewStaffList.isHidden = false
            self.tblStaffList.reloadData()
            
        }
    }

    //Notification
    extension BookingPandingListVC{
        func adjustTableBookingHeight(){
            UIView.animate(withDuration: 0.4, animations: {
                DispatchQueue.main.async {
                    self.heightTableBookingConstraint.constant = 300
                    self.view.layoutIfNeeded()
                }
            })
        }
    }
    
    // MARK:- Custom Methods
    extension BookingPandingListVC{
        
        func getlatestSlotDataFor(){
            let userInfo = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]  ?? ["":""]

            self.strLatitude = userInfo[UserDefaults.keys.latitude] as? String ?? ""
            self.strLongitude = userInfo[UserDefaults.keys.longitude] as? String ?? ""
            if strLatitude == "" &&  strLongitude == "" {
                strLatitude = objLocationManager.strlatitude ?? ""
                strLongitude = objLocationManager.strlongitude ?? ""
            }
            if strSelecrStaffId == "" || strSelecrStaffId == "0"{
                self.imgFilter.image = #imageLiteral(resourceName: "gray_filter_ico")
            }else{
                self.imgFilter.image = #imageLiteral(resourceName: "filter_ico")
            }
            var parameters : Dictionary = [:] as [String : Any]
            if self.businessNameType == "independent"{
                var serType = ""
                if self.lblBookingType.text == "Incall"{
                    serType = "1"
                }else if self.lblBookingType.text == "Outcall"{
                    serType = "2"
                }
                parameters  = [
                    "type":"Booking Request",
                    "date" : "",
                    "search":self.strSearchText,
                    "latitude" : strLatitude,
                    "longitude" : strLongitude,
                    "staffId":self.strSelecrStaffId,
                    "artistId":self.artistId,
                    "bookingType":serType]
            }else{
                var serType = ""
                if self.lblBookingType.text == "Incall"{
                    serType = "1"
                }else if self.lblBookingType.text == "Outcall"{
                    serType = "2"
                }
                parameters  = [
                    "type":"Booking Request",
                    "date" : "",
                    "search":self.strSearchText,
                    "latitude" : strLatitude,
                    "longitude" : strLongitude,
                    "staffId":self.strSelecrStaffId,
                    "artistId":self.artistId,
                    "bookingType":serType]
            }
            self.callWebserviceForGet_BookingList(dict: parameters)
        }
    }
    
    //MARK:- Webservice Call
    
    extension BookingPandingListVC {
        func callWebserviceForGet_BookingList(dict: [String : Any]){
            self.arrAllBooking.removeAll()
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            self.indicator.startAnimating()
            objServiceManager.requestPost(strURL: WebURL.artistBookingHistory, params: dict  , success: { response in
                
                let keyExists = response["responseCode"] != nil
                if  keyExists {
                    objWebserviceManager.StopIndicator()
                    objServiceManager.StopIndicator()
                    self.indicator.stopAnimating()
                    sessionExpireAlertVC(controller: self)
                }else{
                    print("Responce = ",response)
                    let strStatus =  response["status"] as? String ?? ""
                    if strStatus == k_success{
                        self.indicator.stopAnimating()
                         self.parseResponce(response:response)
                    }else{
                        self.indicator.stopAnimating()
                        self.tblBookingList.reloadData()
                        objWebserviceManager.StopIndicator()
                    }
                }
            }){ error in
                self.indicator.stopAnimating()
                objServiceManager.StopIndicator()
                objWebserviceManager.StopIndicator()
                //objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
            }
        }
        
        func callWebserviceForGet_AcceptRejectCount(dict: [String : Any], webURL:String){
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            objActivity.startActivityIndicator()
            objServiceManager.requestPost(strURL: webURL, params: dict  , success: { response in
                objServiceManager.StopIndicator()
                let keyExists = response["responseCode"] != nil
                if  keyExists {
                    sessionExpireAlertVC(controller: self)
                }else{
                    let strStatus =  response["status"] as? String ?? ""
                    let strMsg = response["message"] as? String ?? kErrorMessage
                    if strStatus == k_success{
                        objAppShareData.showAlert(withMessage: strMsg, type: alertType.bannerDark, on: self)
                        self.getlatestSlotDataFor()
                        
                        if let dict = response["userData"] as? [String:Any]{
                            if let bookingType = dict["serviceType"] as? String{
                                self.manageHiddenView(bookingType: bookingType)
                            }else if let bookingType = dict["serviceType"] as? Int{
                                self.manageHiddenView(bookingType: String(bookingType))
                            }
                        }
                    }else{
                        self.tblBookingList.reloadData()
                        let msg = response["message"] as! String
                        objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                    }
                }
            }){ error in
                objServiceManager.StopIndicator()
                objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
            }
        }
        
        //data parsing
        func parseResponce(response:[String : Any]){
            if let arr = response["data"] as? [[String:Any]]{
                if arr.count > 0{
                    for strBookingt in arr{
                        let objBooking = Booking.init(dict: strBookingt)
                        if objBooking.arrBookingInfo.count > 0 {
                            if objBooking.bookStatus == "0"{
                            self.arrAllBooking.append(objBooking)
                            }
                        }
                    }
                }
            }
            var arrPendingCount = [String]()
            for ob in self.arrAllBooking{
                if ob.bookStatus == "0"{
                    arrPendingCount.append("")
                }
            }
            self.tblBookingList.reloadData()
        }
    }

    
    //MARK:- Webservice Call
    extension BookingPandingListVC {
        func callWebserviceForGet_artistStaff(dict: [String : Any]){
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            self.arrStaffList.removeAll()
            objServiceManager.requestPost(strURL: WebURL.artistStaff, params: dict  , success: { response in
                objServiceManager.StopIndicator()
                let keyExists = response["responseCode"] != nil
                if  keyExists {
                    sessionExpireAlertVC(controller: self)
                }else{
                    let strStatus =  response["status"] as? String ?? ""
                    if strStatus == k_success{
                        self.parseResponceStaffList(response:response)
                    }else{
                        self.tblStaffList.reloadData()
                    }
                }
            }){ error in
                self.tblStaffList.reloadData()
                if self.arrStaffList.count > 0{
                    self.lblNoDataFound.isHidden = true
                }else{
                    self.lblNoDataFound.isHidden = false
                }
                objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
            }
        }
        
        func parseResponceStaffList(response:[String : Any]){
            self.arrStaffList.removeAll()
            let userInfo = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]  ?? ["":""]

            let objMydata = UserListModel(dict: ["job":"","status":"1","staffId":0,"staffName":"All Staff","staffImage":"","_id":"","businessId":""])
            self.arrStaffList.append(objMydata)
            let myName = userInfo[UserDefaults.keys.userName] ?? ""
            let myImage = userInfo[UserDefaults.keys.profileImage] ?? ""
            let myId = userInfo[UserDefaults.keys.id] ?? ""

            let objMydata1 = UserListModel(dict: ["job":"","status":"1","staffId":(myId),"staffName":myName,"staffImage":myImage,"_id":"","businessId":""])
            self.arrStaffList.append(objMydata1)
            if let arr = response["staffList"] as? [[String:Any]]{
                if arr.count > 0{
                    for dictArtistData in arr {
                        let objArtistList = UserListModel.init(dict: dictArtistData)
                        if objArtistList.status == "1"{
                            self.arrStaffList.append(objArtistList)
                        }
                    }
                }
            }
            
            self.tblStaffList.reloadData()
        }
}

//
//  TrackMapVC.swift
//  MualabBusiness
//
//  Created by Mindiii on 1/24/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import Alamofire
import GoogleMaps
import GooglePlaces
import INTULocationManager
import AlamofireImage
import CoreLocation
import HCSStarRatingView

class TrackMapVC: UIViewController,GMSMapViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,CLLocationManagerDelegate {
    
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var collectionService: UICollectionView!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var imgUserOnSlider: UIImageView!
    @IBOutlet weak var lblStaffAddress: UILabel!
    @IBOutlet weak var lblCustomerAddress: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDateAndTime: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var imgDropDownDetail: UIImageView!
    @IBOutlet weak var imgCompleted: UIImageView!
    @IBOutlet weak var viewMaximumValueImage: UIView!
    @IBOutlet weak var viewRating: HCSStarRatingView!

    
    @IBOutlet weak var bottumDetailView: UIView!
    @IBOutlet weak var radiusSlider:customSlider2!

    @IBOutlet weak var btnStart: UIButton!
    @IBOutlet weak var btnEnd: UIButton!

    var myId = ""
    var artistId = ""
    var objBookedMainService = Booking(dict: [:])
    var objBookedService = BookingInfo(dict: [:])
    var locationManager = CLLocationManager()
    var maximumValue = 0.0
    

    
    @IBOutlet weak var viewMap: GMSMapView!
//
       fileprivate var arrMarker:[GMSMarker] = []
//    fileprivate var arrAllJobList = [ModelClientJobList]()
    var strLatitude = ""
    var strLongitude = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewMaximumValueImage.isHidden = false

        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
        self.configureView()
        objAppShareData.onTrackView = true
        self.collectionService.delegate = self
        self.collectionService.dataSource = self
        self.collectionService.reloadData()
        viewMap.mapType = GMSMapViewType.normal
        self.radiusSlider.setThumbImage(UIImage.init(named: "grayGubbara_ico"),for:.highlighted)
        self.radiusSlider.minimumTrackTintColor = appColor
        self.radiusSlider.value = 0.0
        let thumbImage = addUserImage(inImage: UIImage.init(named: "grayGubbara_ico")!, atPoint: CGPoint.init(x:9,y:6))
        
        let img = addUserImageSecond(inImage: thumbImage, atPoint:  CGPoint.init(x:12,y:8))
        self.radiusSlider.setThumbImage(img, for:.normal)
        self.radiusSlider.setThumbImage(img, for:.highlighted)

      //  self.lblCustomerAddress.text = objBookedMainService.location
        PlaceAPIWork()
        self.viewMap.isMyLocationEnabled = true
        
        
        
        
        
        
        
      if  let userInfo = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.businessInfoDic) {
        if let imgUrl = userInfo["profileImage"] as? String {
            if imgUrl != "" {
                if let url = URL(string: imgUrl){
                 //   self.imgUserOnSlider.af_setImage(withURL: url, placeholderImage:#imageLiteral(resourceName: "cellBackground"))
                }
            }else{
              //  self.imgUserOnSlider.image = #imageLiteral(resourceName: "cellBackground")
            }
        }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        let userInfo = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]  ?? ["":""]
        self.artistId = userInfo[UserDefaults.keys.id] as? String ?? ""
        self.myId = userInfo[UserDefaults.keys.id] as? String ?? ""
        if let id = userInfo[UserDefaults.keys.id] as? Int{
            self.artistId = String(id)
            self.myId = String(id)
        }
        let adminId = UserDefaults.standard.string(forKey: UserDefaults.keys.newAdminId) ?? ""
        if adminId == self.artistId || adminId == ""{
        }else{
            self.artistId = adminId
        }
        
        self.bottumDetailView.isHidden = true
            self.imgDropDownDetail.image = #imageLiteral(resourceName: "upper_ico")
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
        let latitude = location?.coordinate.latitude ?? 0.0
        let longitude = location?.coordinate.longitude ?? 0.0
        objAppShareData.objModelBusinessSetup.trackingLatitude = String(latitude)
        objAppShareData.objModelBusinessSetup.trackingLongitude = String(longitude)
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation((location ?? nil)!) { (placemarks, error) in
            if (error != nil){
                print("error in reverseGeocode")
            }
            if placemarks != nil{
            let placemark = placemarks! as [CLPlacemark]
            if placemark.count>0{
                let placemark = placemarks![0]
                print("locality = ",placemark.locality ?? "")
                print("administrativeArea = ",placemark.administrativeArea ?? "")
                print("country = ",placemark.country ?? "")
                print("name = ",placemark.name ?? "")
                print("subLocality = ",placemark.subLocality ?? "")
                print("subAdministrativeArea = ",placemark.subAdministrativeArea ?? "")
                let address = String(placemark.subLocality ?? "")+" "+String(placemark.name ?? "")
                let address2 = String(placemark.locality ?? "")+" "+String(placemark.administrativeArea ?? "")+" "+String(placemark.country ?? "")
                self.lblStaffAddress.text  = address+address2
                }}
            }
     }
    
    func configureView(){
        let objUser = objBookedMainService.arrUserDetail[0]
        if objUser.profileImage != "" {
            if let url = URL(string: objUser.profileImage){
                //self.imgUser.af_setImage(withURL: url, placeholderImage:#imageLiteral(resourceName: "cellBackground"))
                self.imgUser.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "cellBackground"))
                //self.imgUserOnSlider.af_setImage(withURL: url, placeholderImage:#imageLiteral(resourceName: "cellBackground"))
                self.imgUserOnSlider.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "cellBackground"))

            }
        }else{
            self.imgUser.image = #imageLiteral(resourceName: "cellBackground")
        }
        self.imgCompleted.isHidden = true
            if objBookedService.status == "0"{
                self.lblStatus.textColor = #colorLiteral(red: 0, green: 0.7333333333, blue: 0.1529411765, alpha: 1)
                self.lblStatus.text = "Confirmed"
            }else if objBookedService.status == "1"{
                self.lblStatus.textColor = #colorLiteral(red: 0, green: 0.7333333333, blue: 0.1529411765, alpha: 1)
                self.lblStatus.text = "On the way"
            }else if objBookedService.status == "2"{
                self.lblStatus.textColor = #colorLiteral(red: 0, green: 0.851996243, blue: 0.8281483054, alpha: 1)
                self.lblStatus.text = "On going"
            }else if objBookedService.status == "3" && objBookedMainService.bookStatus == "5"{
                self.lblStatus.textColor = #colorLiteral(red: 0, green: 0.7333333333, blue: 0.1529411765, alpha: 1)
                self.lblStatus.text = "Completed"
                self.imgCompleted.isHidden = false
            }else if objBookedService.status == "4"{
                self.lblStatus.textColor = #colorLiteral(red: 0, green: 0.7333333333, blue: 0.1529411765, alpha: 1)
                self.lblStatus.text = "Reached"
            }else{
                self.lblStatus.textColor = #colorLiteral(red: 0, green: 0.7333333333, blue: 0.1529411765, alpha: 1)
                self.lblStatus.text = "Service end"
        }
       
        
        self.lblName.text = objBookedMainService.arrUserDetail[0].userName
        let formatter  = DateFormatter()
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        formatter.dateFormat = "yyyy-MM-dd"
        let date = formatter.date(from: objBookedService.bookingDate)
        formatter.dateFormat = "dd/MM/yyyy"
        let strDate = formatter.string(from: date!)
        self.lblDateAndTime.text = strDate + ", " + objBookedService.startTime + " - " + objBookedService.endTime
        
        self.btnStart.isHidden = true
        self.btnEnd.isHidden = true
        
        let strStartTime =  objBookedService.startTime
        let dates = objAppShareData.getTimeFromTime(strDate: objBookedService.bookingDate+" "+strStartTime)
        let formatters  = DateFormatter()
        formatters.dateFormat = "yyyy-MM-dd hh:mm a"
        formatters.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        formatters.timeZone = NSTimeZone(abbreviation: "UTC") as TimeZone?
        let dataDate = formatters.date(from: dates)!
        let currentDate = objAppShareData.matchesTwoDate(dateA: dataDate, type: "pending")
        
        if (objBookedMainService.bookStatus == "1" || objBookedMainService.bookStatus == "5" ) && (objBookedMainService.paymentType == "1" && objBookedMainService.paymentStatus == "1") || (objBookedMainService.paymentType == "2" && (objBookedMainService.bookStatus == "1" || objBookedMainService.bookStatus == "5")){
            if objBookedService.status != "2" && objBookedService.status != "3" && currentDate == true{
                self.btnStart.isHidden = false
            }else if objBookedService.status == "2"{
                self.btnEnd.isHidden = false
            }
        }
        
        let review = objBookedMainService.arrUserDetail[0].ratingCount
        self.viewRating.value = CGFloat(Float(review) ?? 0.0)
    }
}

//MARK : - Button method extension
extension TrackMapVC{
    
    @IBAction func btnCurrentLocation(_ sender: UIButton) {
        let dict = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.businessInfoDic)
        let mylat = (objAppShareData.objModelBusinessSetup.trackingLatitude as NSString).doubleValue
        let mylong = (objAppShareData.objModelBusinessSetup.trackingLongitude as NSString).doubleValue
        let camera = GMSCameraPosition.camera(withLatitude: mylat, longitude: mylong, zoom: 12)
        self.viewMap.camera = camera
    }
    
    @IBAction func btnUpDownDetail(_ sender: UIButton) {
        self.bottumDetailView.isHidden = !self.bottumDetailView.isHidden
        self.imgDropDownDetail.image = #imageLiteral(resourceName: "DropGray")
        if bottumDetailView.isHidden == true{
            self.imgDropDownDetail.image = #imageLiteral(resourceName: "upper_ico")
        }
    }
    
    @IBAction func btnStartl(_ sender: UIButton) {
            let objBooking = objBookedMainService
            let objService = objBookedService
            if objBooking.arrUserDetail.count > 0 {
                let objUserInfo = objBooking.arrUserDetail[0]
                let userId = objUserInfo._id
                
                let param = [  "userId":objBooking.userId,//userId,//
                    "bookingId":objBooking._id,//
                    "id":objService._id,
                    "serviceName":objService.artistServiceName,
                    "paymentType":objBooking.paymentType,
                    "price":objService.bookingPrice,
                    "artistId":self.myId,
                    "customerType":objBooking.customerType,
                    "type":"start" ]
                callWebserviceForGet_AcceptRejectCount(dict: param, webURL: WebURL.bookingupdate, fromStart: true)
            }
        
    }
    func callWebserviceForGet_AcceptRejectCount(dict: [String : Any], webURL:String,fromStart:Bool){
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            objActivity.startActivityIndicator()
            objServiceManager.requestPost(strURL: webURL, params: dict  , success: { response in
                objServiceManager.StopIndicator()
                let keyExists = response["responseCode"] != nil
                if  keyExists {
                    sessionExpireAlertVC(controller: self)
                }else{
                    let strStatus =  response["status"] as? String ?? ""
                    let strMsg = response["message"] as? String ?? kErrorMessage
                    if strStatus == k_success{
                        self.callWebserviceForGetBookedServices()
                        objAppShareData.showAlert(withMessage: strMsg, type: alertType.bannerDark, on: self)
                    }else{
                        let msg = response["message"] as! String
                        objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                    }
                }
            }){ error in
                objServiceManager.StopIndicator()
                objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
            }
        }
        
      
    
    @IBAction func btnEnd(_ sender: UIButton) {
        let objBooking = objBookedMainService
        let objService = objBookedService
            if objBooking.arrUserDetail.count > 0 {
                let objUserInfo = objBooking.arrUserDetail[0]
                let userId = objUserInfo._id
                
                let param = [  "userId":objBooking.userId,//userId,
                               "bookingId":objBooking._id,
                               "id":objService._id,
                               "serviceName":objService.artistServiceName,
                               "paymentType":objBooking.paymentType,
                               "price":objService.bookingPrice,
                               "artistId":self.myId,
                               "customerType":objBooking.customerType,
                               "type":"end" ]
                callWebserviceForGet_AcceptRejectCount(dict: param, webURL: WebURL.bookingupdate, fromStart: false)
        }
    }
    
    @IBAction func btnCallOption(_ sender: UIButton) {
        let obj = objBookedMainService.arrUserDetail[0]
        if let url = URL(string: "tel://\(obj.countryCode+obj.contactNo)"), UIApplication.shared.canOpenURL(url) {
            if obj.contactNo == ""{
                objAppShareData.showAlert(withMessage: "Contact number not available!", type: alertType.bannerDark,on: self)
                return
            }
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    //    objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
    }
    
    @IBAction func btnMapType(_ sender: UIButton) {
        if viewMap.mapType == GMSMapViewType.normal{
            viewMap.mapType = GMSMapViewType.satellite
        }else {
            viewMap.mapType = GMSMapViewType.normal
        }
        
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        objAppShareData.onTrackView = false
        self.navigationController?.popViewController(animated: true)
    }
    
}
//MARK:- CURRENT LOCATION API
extension TrackMapVC{
    func locationAccess(){
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self as GMSAutocompleteViewControllerDelegate
        autocompleteController.modalPresentationStyle = .fullScreen
        present(autocompleteController, animated: true, completion: nil)
    }

    func PlaceAPIWork(){
        let locationManager = INTULocationManager.sharedInstance()
        
        locationManager.requestLocation(withDesiredAccuracy: .city,timeout: 10.0,delayUntilAuthorized: true) { (currentLocation, achievedAccuracy, status) in
            
            if (status == INTULocationStatus.success) {
                objWebserviceManager.StopIndicator()
                let lcc = CLLocation.init(latitude: (currentLocation?.coordinate.latitude)!, longitude: (currentLocation?.coordinate.longitude)!)
                self.strLatitude = String(describing: (currentLocation?.coordinate.latitude)!)
                self.strLongitude = String(describing: (currentLocation?.coordinate.longitude)!)
                let latTemp = self.strLatitude
                let longTemp = self.strLongitude
                let mylat = (latTemp as NSString).doubleValue
                let mylong = (longTemp as NSString).doubleValue
                self.setDataOnMapWith(lat: mylat, long: mylong)
                _ = self.getAddressFromLocation(location: lcc)
                self.map_Google()
            }else if (status == INTULocationStatus.timedOut) {
             }else {
            }
        }
       // objWebserviceManager.StartIndicator()
    }
    
    func setDataOnMapWith(lat:Double, long:Double) -> Void
    {
        let strLatitude1 = "22.7051"
        let strLongitude1 = "75.9091"
        
        let mylat = (strLatitude1 as NSString).doubleValue
        let mylong = (strLongitude1 as NSString).doubleValue
        //let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: long, zoom: 12)
        let camera = GMSCameraPosition.camera(withLatitude: mylat, longitude: mylong, zoom: 12)
        
        self.viewMap.camera = camera
        let position = CLLocationCoordinate2D(latitude: lat, longitude: long)
        
        let marker = GMSMarker(position: position)
        marker.icon = UIImage(named: "ico_map_pin_f")
        
        ///////////////
        let DynamicView=UIView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        DynamicView.backgroundColor=UIColor.clear
        //Creating Marker Pin imageview for Custom Marker
        var imageViewForPinMarker : UIImageView
        imageViewForPinMarker  = UIImageView(frame:CGRect(x: 0, y: 0, width: 40, height: 40));
        imageViewForPinMarker.image = UIImage(named:"ico_map_pin_f")
        
        //Creating User Profile imageview
        var imageViewForUserProfile : UIImageView
        
        imageViewForUserProfile  = UIImageView(frame:CGRect(x: 11.5, y: 5.5, width: 17, height: 17))
        imageViewForUserProfile.layer.cornerRadius = imageViewForUserProfile.frame.size.height/2
        imageViewForUserProfile.layer.masksToBounds = true
        
        
        //Adding userprofile imageview inside Marker Pin Imageview
        imageViewForPinMarker.addSubview(imageViewForUserProfile)
        //Adding Marker Pin Imageview isdie view for Custom Marker
        DynamicView.addSubview(imageViewForPinMarker)
        //Converting dynamic uiview to get the image/marker icon.
        UIGraphicsBeginImageContextWithOptions(DynamicView.frame.size, false, UIScreen.main.scale)
        DynamicView.layer.render(in: UIGraphicsGetCurrentContext()!)
        let imageConverted: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        marker.icon = imageConverted
        arrMarker.append(marker)
        marker.map = self.viewMap
        /////////////////////
    }
}

//MARK: - PLACE API
extension TrackMapVC: GMSAutocompleteViewControllerDelegate {
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        self.strLatitude = String(place.coordinate.latitude)
        self.strLongitude = String(place.coordinate.longitude)
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
    }
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    func getAddressFromLocation(location:CLLocation) ->String{
        var addressString = String()
        CLGeocoder().reverseGeocodeLocation(location,completionHandler: {(placemarks, error) -> Void in
            //locationManager.stopUpdatingLocation()
            if error != nil {
                return
            }
            if (placemarks?.count)! > 0 {
                let pm = placemarks?.last
                if let formattedAddress = pm?.addressDictionary?["FormattedAddressLines"] as? [String] {
                    addressString = formattedAddress.joined(separator: ", ")
                }}
            else {
            }
        })
        return addressString
    }
}
extension TrackMapVC{
func map_Google(){
    viewMap.clear()
    viewMap.setMinZoom(8, maxZoom: viewMap.maxZoom)
    arrMarker.removeAll()
    
      let userInfo = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]  ?? ["":""]
    
    for i in 0...1  {
        let objModel  = objBookedService
        let objUser  = objBookedMainService.arrUserDetail[0]

        var position = CLLocationCoordinate2D()

        if i == 0{
            position.latitude = Double(objBookedMainService.latitude) ?? 0
            position.longitude = Double(objBookedMainService.longitude) ?? 0
        }
        if i == 1{
            position.latitude = Double(objAppShareData.objModelBusinessSetup.trackingLatitude ) ?? 0.0
            position.longitude = Double(objAppShareData.objModelBusinessSetup.trackingLongitude) ?? 0.0
            if objBookedService.status == "3" && objBookedMainService.bookStatus == "5"{
                position.latitude = Double(objBookedMainService.latitude ) ?? 0.0
                position.longitude = Double(objBookedMainService.latitude) ?? 0.0
            }
        }
        
        let marker = GMSMarker(position: position)
        marker.accessibilityLabel = objBookedMainService.location
        marker.accessibilityValue = "\(i)"
        marker.groundAnchor = CGPoint(x: 0.50, y: 0.0)
        
        if i == 0{
            CATransaction.begin()
            CATransaction.setAnimationDuration(0.5)
            marker.position = CLLocationCoordinate2D(latitude: Double(objBookedMainService.latitude) ?? 0, longitude: Double(objBookedMainService.longitude) ?? 0)
            CATransaction.commit()
        }
        if i == 1{
            position.latitude = Double(objAppShareData.objModelBusinessSetup.trackingLatitude ) ?? 0.0
            position.longitude = Double(objAppShareData.objModelBusinessSetup.trackingLongitude) ?? 0.0
            CATransaction.begin()
            CATransaction.setAnimationDuration(0.5)
            marker.position = CLLocationCoordinate2D(latitude: Double(objAppShareData.objModelBusinessSetup.trackingLatitude) ?? 0, longitude: Double(objAppShareData.objModelBusinessSetup.trackingLongitude) ?? 0)
            CATransaction.commit()
        }
        
        
        ///////////////
        
        
        
        
        let DynamicView=UIView(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
        DynamicView.backgroundColor=UIColor.clear
        var imageViewForPinMarker : UIImageView
        imageViewForPinMarker  = UIImageView(frame:CGRect(x: 0, y: 0, width: 50, height: 60));
        

        
        
        //Creating User Profile imageview
        var imageViewForUserProfile : UIImageView
        imageViewForUserProfile  = UIImageView(frame:CGRect(x: 10, y: 9, width: 30, height: 30
        ))
        imageViewForUserProfile.layer.cornerRadius = imageViewForUserProfile.frame.size.width/2
        imageViewForUserProfile.layer.masksToBounds = true
        
        if i == 0{
            imageViewForPinMarker.image = #imageLiteral(resourceName: "blue_map_ico")
            if objUser.profileImage == ""
            {  imageViewForUserProfile.image = #imageLiteral(resourceName: "cellBackground")  }else
            {  let url = URL(string:objUser.profileImage)
                //imageViewForUserProfile.af_setImage(withURL: url!)
                imageViewForUserProfile.sd_setImage(with: url!, placeholderImage: #imageLiteral(resourceName: "cellBackground"))
            }
            marker.accessibilityLabel = objBookedMainService.location
        }
        
        if i == 1{
             imageViewForPinMarker.image = #imageLiteral(resourceName: "gray_ico")
            if objModel.staffImage == ""
            {  imageViewForUserProfile.image = #imageLiteral(resourceName: "cellBackground")  }else
            {  let url = URL(string:objModel.staffImage)
                //imageViewForUserProfile.af_setImage(withURL: url!)
                imageViewForUserProfile.sd_setImage(with: url!, placeholderImage: #imageLiteral(resourceName: "cellBackground"))
            }
            marker.accessibilityLabel =  objAppShareData.objModelBusinessSetup.trackingAddress
        }
    
       
        
        let coordinate₀ = CLLocation(latitude: Double(objBookedMainService.latitude)  ?? 0.0, longitude: Double(objBookedMainService.longitude) ?? 0.0)
        let coordinate₁ = CLLocation(latitude: Double(objAppShareData.objModelBusinessSetup.trackingLatitude) ?? 0.0, longitude:  Double(objAppShareData.objModelBusinessSetup.trackingLongitude) ?? 0.0)
        let distanceInMeters = coordinate₀.distance(from: coordinate₁)
        if distanceInMeters <= 10 ||  (objBookedService.status == "3" && objBookedMainService.bookStatus == "5"){
       
            if i == 1{
            imageViewForPinMarker.image = #imageLiteral(resourceName: "map_meet_users_ico")
            imageViewForUserProfile.image = nil
            }else{
                imageViewForPinMarker.image = nil
                imageViewForUserProfile.image = nil
            }
        }
        
        
            //Adding userprofile imageview inside Marker Pin Imageview
            imageViewForPinMarker.addSubview(imageViewForUserProfile)
            //Adding Marker Pin Imageview isdie view for Custom Marker
            DynamicView.addSubview(imageViewForPinMarker)
        
            //Converting dynamic uiview to get the image/marker icon.
            UIGraphicsBeginImageContextWithOptions(DynamicView.frame.size, false, UIScreen.main.scale)
            DynamicView.layer.render(in: UIGraphicsGetCurrentContext()!)
            let imageConverted: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
            UIGraphicsEndImageContext()
            marker.icon = imageConverted
            arrMarker.append(marker)
            marker.map = self.viewMap
        if i == 0{
            marker.title = objUser.userName
            marker.snippet = self.lblStatus.text// objAppShareData.objModelBusinessSetup.trackingAddress
        }
        if i == 1{
            marker.title = objModel.Name
            marker.snippet = objBookedMainService.location
        }
        if distanceInMeters <= 10{
            marker.snippet = "Reached at customer location"//objBookedMainService.location
        }
        if objBookedService.status == "3" && objBookedMainService.bookStatus == "5"{
            marker.snippet = "Completed"
        }

        marker.layer.borderWidth = 1
        marker.layer.borderColor =  UIColor.darkGray.cgColor
        marker.infoWindowAnchor = CGPoint(x: 0.5, y: 0.5)
    }

    //end of for loop
    
    let startPoint = CLLocation(latitude: Double(self.strLatitude ?? "0.0")  ?? 0.0, longitude: Double(self.strLongitude ?? "0.0") ?? 0.0)
    let coordinate₀ = CLLocation(latitude: Double(objBookedMainService.latitude)  ?? 0.0, longitude: Double(objBookedMainService.longitude) ?? 0.0)
    let coordinate₁ = CLLocation(latitude: Double(objAppShareData.objModelBusinessSetup.trackingLatitude) ?? 0.0, longitude:  Double(objAppShareData.objModelBusinessSetup.trackingLongitude) ?? 0.0)
    
    let distanceFromCurrentPointMeters = coordinate₀.distance(from: coordinate₁)
    var distanceFromStartPointMeters = startPoint.distance(from: coordinate₁)
    
//    if(distanceFromCurrentPointMeters <= 1609.344)
//    {
//     self.lblDistance.text = String(format: "%.2f", distanceFromCurrentPointMeters)+" Meter"
//    }
//    else   {
        let a = distanceFromCurrentPointMeters/1609.344
        self.lblDistance.text = String(format: "%.2f", a)+" Miles"
//    }

    self.radiusSlider.minimumValue = 0.0
    self.radiusSlider.maximumValue = Float(distanceFromStartPointMeters)
    if distanceFromCurrentPointMeters >= distanceFromStartPointMeters{
        distanceFromStartPointMeters = distanceFromCurrentPointMeters
        self.radiusSlider.maximumValue = Float(distanceFromCurrentPointMeters)
        self.strLatitude = objAppShareData.objModelBusinessSetup.trackingLatitude
        self.strLongitude = objAppShareData.objModelBusinessSetup.trackingLongitude
        if maximumValue >= distanceFromCurrentPointMeters {
        }else{
            maximumValue = distanceFromCurrentPointMeters
        }
    }else{
        if maximumValue >= distanceFromStartPointMeters {
        }else{
            maximumValue = distanceFromStartPointMeters
        }
    }
    self.radiusSlider.maximumValue =  Float(maximumValue)
    let review = objBookedMainService.arrUserDetail[0].ratingCount
    self.viewRating.value = CGFloat(Float(review) ?? 0.0)
        
    print("distance = ",Float(distanceFromStartPointMeters)-Float(distanceFromCurrentPointMeters))
    print("disLat = ",objAppShareData.objModelBusinessSetup.trackingLatitude)
    print("disLong = ",objAppShareData.objModelBusinessSetup.trackingLongitude)

    self.radiusSlider.value = Float(maximumValue-distanceFromCurrentPointMeters)

    if distanceFromCurrentPointMeters <= 10 || (objBookedService.status == "3" && objBookedMainService.bookStatus == "5"){
        self.viewMaximumValueImage.isHidden = true
        self.radiusSlider.setThumbImage(#imageLiteral(resourceName: "meet_users_ico"), for:.normal)
        self.radiusSlider.value = Float(maximumValue)
    }else{
        self.radiusSlider.setThumbImage(UIImage.init(named: "grayGubbara_ico"),for:.normal)
        let thumbImage = addUserImage(inImage: UIImage.init(named: "grayGubbara_ico")!, atPoint: CGPoint.init(x:9,y:6))
        let img = addUserImageSecond(inImage: thumbImage, atPoint:  CGPoint.init(x:12,y:8))
        self.radiusSlider.setThumbImage(img, for:.normal)
        self.viewMaximumValueImage.isHidden = false
     }
     self.lblCustomerAddress.text =  objBookedMainService.location
    if    objAppShareData.onTrackView == true{
        DispatchQueue.main.asyncAfter(deadline: .now() + 8) {
            self.map_Google()
        }
    }
}
}

//MARK:- Collection view Delegate Methods
extension TrackMapVC:UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1//objBookedMainService.arrBookingInfo.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellIdentifier = "SubsubServicesCell"
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath as IndexPath) as! StaffBookingCell
        let obj = objBookedService//objBookedMainService.arrBookingInfo[indexPath.row].artistServiceName
        cell.lblUserName.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        cell.lblUserName.text = obj.artistServiceName
        cell.lblUserName.layer.cornerRadius = 10
        cell.lblUserName.layer.masksToBounds = true
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        var cellWidth:CGFloat = 0
        var cellHeight:CGFloat = 26
        cellWidth = CGFloat(self.collectionService.frame.size.width-2)
        let a = objBookedMainService.arrBookingInfo[indexPath.row].artistServiceName
        var sizeOfString = CGSize()
        if let font = UIFont(name: "Nunito-Regular", size: 13)
        {
            let finalDate = a
            let fontAttributes = [NSAttributedString.Key.font: font] // it says name, but a UIFont works
            sizeOfString = (finalDate as NSString).size(withAttributes: fontAttributes)
        }
        cellWidth = sizeOfString.width+20
        cellHeight = 26
        return CGSize(width: cellWidth, height: cellHeight)
    }
    
    func addUserImage(inImage image: UIImage, atPoint point: CGPoint) -> UIImage {
        let scale = UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(image.size, false, scale)
        image.draw(in: CGRect(origin: CGPoint.zero, size: image.size))
        var smallImage = #imageLiteral(resourceName: "cellBackground")
        let img = UIImageView()
        if objBookedService.staffImage != "" {
            if let url = URL(string: objBookedService.staffImage){
                //img.af_setImage(withURL: url, placeholderImage:#imageLiteral(resourceName: "cellBackground"))
                img.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "cellBackground"))
            }
        }
        img.layer.cornerRadius = 11
        img.layer.masksToBounds = true
        smallImage = img.image ?? #imageLiteral(resourceName: "cellBackground")
        smallImage.draw(in: CGRect(x: 17, y: 22, width: 30, height: 30))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        self.radiusSlider.minimumTrackTintColor = #colorLiteral(red: 0, green: 0.851996243, blue: 0.8281483054, alpha: 1)
        UIGraphicsEndImageContext()
        return newImage!
    }
    
    func addUserImageSecond(inImage image: UIImage, atPoint point: CGPoint) -> UIImage {
        let scale = UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(image.size, false, scale)
        image.draw(in: CGRect(origin: CGPoint.zero, size: image.size))
        let smallImage = #imageLiteral(resourceName: "gary_circle_img2")
        smallImage.draw(in: CGRect(x: 11, y: 15, width: 43, height: 43))

        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
}

//MARK: - GET BOOKINGDETAIL
extension TrackMapVC{

    func callWebserviceForGetBookedServices(){
            if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
            }
            objActivity.startActivityIndicator()
            let dicParam = [
            "bookingId":objBookedMainService._id
            ] as [String : Any]
    
            objServiceManager.requestPostForJson(strURL: WebURL.bookingDetail, params: dicParam, success: { response in
            print(response)
            objActivity.stopActivity()
            let keyExists = response["responseCode"] != nil
            if  keyExists {
            sessionExpireAlertVC(controller: self)
            }else {
            let strSucessStatus = response["status"] as? String ?? ""
            if  strSucessStatus == "success"{
            objActivity.stopActivity()
            print(response)
            self.saveData(dicts:response)
            }else{
            let msg = response["message"] as? String ?? ""
            objAppShareData.showAlert(withMessage: msg , type: alertType.bannerDark,on: self)
            }}
            }) { error in
            objActivity.stopActivity()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
            }
    }
    
    func saveData(dicts:[String:Any]){
        
        if let strBookingt = dicts["data"] as? [String : Any]{
            let objBooking = Booking.init(dict: strBookingt)
            objBookedMainService = objBooking
            for obj in objBookedMainService.arrBookingInfo{
                if obj._id == objBookedService._id{
                    objBookedService = obj
                    self.configureView()
                    break
                }
            }
        }
        self.configureView()
    }
}

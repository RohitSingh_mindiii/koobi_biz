//
//  ReviewListVC.swift
//  MualabBusiness
//
//  Created by Mindiii on 2/5/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit

class ReviewListVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var tblReviewList:UITableView!
    @IBOutlet weak var lblNoDataFound:UIView!
    fileprivate var arrReviewList = [ModelReview]()
    var userId = ""
    var userType = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblReviewList.delegate = self
        self.tblReviewList.dataSource = self
        self.tblReviewList.reloadData()
        self.tblReviewList.estimatedRowHeight = 112
        self.callWebserviceForReviewList()
    
    }
   
}
//MARK:-  button method
extension ReviewListVC {
    @IBAction func btnBack(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK:- tableview delegate datasource method
extension ReviewListVC {
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if arrReviewList.count == 0{
            self.lblNoDataFound.isHidden = false
        }else{
            self.lblNoDataFound.isHidden = true
        }
        return arrReviewList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        if let cell  = tableView.dequeueReusableCell(withIdentifier: "CellReviewList")! as? CellReviewList {
            let objArtistDetails = arrReviewList[indexPath.row]
            cell.imgProfile.image = UIImage(named: "cellBackground")
            cell.lblReviewDescription.text  =  objArtistDetails.reviewByUser
            
            let a = objArtistDetails.artistRatingCrd.components(separatedBy: "-")
            if a.count > 2{
                cell.lblTime.text = a[2]+"/"+a[1]+"/"+a[0]
            }
            
            cell.btnProfile.tag = indexPath.row
            cell.btnProfile.superview?.tag = indexPath.section
            cell.btnProfile.addTarget(self, action: #selector(btnGoToProfile(_:)), for: .touchUpInside)

            
            cell.lblCustomerName.text  = objArtistDetails.arrUserDetail[0].userName
            cell.viewRating.value = CGFloat(Int(objArtistDetails.userRating) ?? 0)
            if objArtistDetails.arrUserDetail[0].profileImage != "" {
                if let url = URL(string: objArtistDetails.arrUserDetail[0].profileImage){
                    //cell.imgProfile.af_setImage(withURL: url, placeholderImage:#imageLiteral(resourceName: "cellBackground"))
                    cell.imgProfile.sd_setImage(with: url, placeholderImage:#imageLiteral(resourceName: "cellBackground"))
                }
            }
            return cell
        }else{
            return UITableViewCell()
        }
    }
    
    
   
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}
//MARK:- Webservice Call
extension ReviewListVC {
    func callWebserviceForReviewList(){
        self.arrReviewList.removeAll()
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        
        objActivity.startActivityIndicator()
        let dicParam = [
            "userId":self.userId,
            "userType":self.userType
            ] as [String : Any]
        objServiceManager.requestGet(strURL: WebURL.getRatingReview, params: dicParam as [String : AnyObject] , success: { response in
            objServiceManager.StopIndicator()
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else{
                let strStatus =  response["status"] as? String ?? ""
                if strStatus == k_success{
                    self.parseResponce(response:response)
                    self.tblReviewList.reloadData()
                }else{
                    self.tblReviewList.reloadData()
                    let msg = response["message"] as! String
                    objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                }
            }
        }){ error in
            self.tblReviewList.reloadData()
            objServiceManager.StopIndicator()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
    func parseResponce(response:[String : Any]){
        self.arrReviewList.removeAll()
        print(response)
        if let arr = response["data"] as? [[String:Any]]{
            if arr.count > 0{
                for dictArtistData in arr {
                    let objArtistList = ModelReview.init(dict: dictArtistData)
                        self.arrReviewList.append(objArtistList)
                }
            }
        }
        self.tblReviewList.reloadData()
    }
}


//MARK: - profile redirection
extension ReviewListVC{
    
    @objc func btnGoToProfile(_ sender: UIButton)
    {
        let objData = arrReviewList[sender.tag]
        let a = objData.arrUserDetail[0]
        let dicParam =  ["userName":a.userName]
        btnProfileListAction(dicParam: dicParam, userName: a.userName)
    }
    
    func gotoProfileVC (){
        self.view.endEditing(true)
        let sb: UIStoryboard = UIStoryboard(name: "ArtistProfile", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"SWRevealViewController") as? SWRevealViewController{
            objVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(objVC, animated: true)
        }
    }
    
    func recall() {
    }
    
    func btnProfileListAction(dicParam:[String:Any],userName:String){
        //let section = 0
        //let row = (sender as AnyObject).tag
        //let indexPath = IndexPath(row: row!, section: section)
        //let objUser = arrLikeUsersList[indexPath.row]
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        
        objServiceManager.requestPost(strURL: WebURL.profileByUserName, params: dicParam, success: { response in
            if response["status"] as? String ?? "" == "success"{
                var strId = ""
                var strType = ""
                if let dictUser = response["userDetail"] as? [String : Any]{
                    let myId = dictUser["_id"] as? Int ?? 0
                    strId = String(myId)
                    strType = dictUser["userType"] as? String ?? ""
                }
                let dic = [
                    "tabType" : "people",
                    "tagId": strId,
                    "userType":strType,
                    "title": userName ?? ""
                    ] as [String : Any]
                self.openProfileForSelectedTagPopoverWithInfo(dict: dic)
            }
        }) { error in
        }
    }
    
    func openProfileForSelectedTagPopoverWithInfo(dict : [AnyHashable: Any]){
        
        var dictTemp : [AnyHashable : Any]?
        
        dictTemp = dict
        
        if let dict1 = dict as? [String:[String :Any]] {
            if let dict2 = dict1.first?.value {
                dictTemp = dict2
            }
        }
        
        guard let dictFinal = dictTemp as? [String : Any] else { return }
        
        var strUserType: String?
        var tagId : Int?
        
        if let userType = dictFinal["userType"] as? String{
            strUserType = userType
            
            if let idTag = dictFinal["tagId"] as? Int{
                tagId = idTag
            }else{
                if let idTag = dictFinal["tagId"] as? String{
                    tagId = Int(idTag)
                }
            }
            
            var myId = ""
            if let dicUser = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] {
                myId = dicUser["_id"] as? String ?? ""
            }
            if myId == String(tagId ?? 0) ?? "" {
                //isNavigate = true
                objAppShareData.isOtherSelectedForProfile = false
                self.gotoProfileVC()
                return
            }
            
            if let strUserType = strUserType, let tagId =  tagId {
                
                objAppShareData.selectedOtherIdForProfile  = String(tagId)
                objAppShareData.isOtherSelectedForProfile = true
                objAppShareData.selectedOtherTypeForProfile = strUserType  //"artist" or "user"
                //isNavigate = true
                self.gotoProfileVC()
            }
        }
    }
}

//
//  StaffCompleteBookingVC.swift
//  MualabBusiness
//
//  Created by Mindiii on 3/28/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit

class StaffCompleteBookingVC:  UIViewController, UITableViewDelegate, UITableViewDataSource {
        
        @IBOutlet weak var viewPastBooking: UIView!
        @IBOutlet weak var tblPastBooking: UITableView!
        @IBOutlet weak var activity: UIActivityIndicatorView!
        @IBOutlet weak var lblNoRecord: UILabel!
    
         var selectedIndexPath : IndexPath?
        var arrPastBookingData : [PastFutureBookingData] = []
        var arrPastBookingDataAll : [PastFutureBookingData] = []
    
    fileprivate var strUserId : String = ""
     var strStaffId : String = ""

        
        override func viewDidLoad() {
            super.viewDidLoad()
            configureView()
        }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
        }
        
        override var preferredStatusBarStyle: UIStatusBarStyle {
            return .default
        }
        
        override func viewDidAppear(_ animated: Bool) {
            super.viewDidAppear(true)
            self.tabBarController?.view.frame =  CGRect(x:0,y:0,width:Int(self.view.frame.width),height:viewHeightGloble)
        }
        
        override func viewWillAppear(_ animated: Bool) {
            self.activity.stopAnimating()
            
            self.view.endEditing(true)
            selectedIndexPath = nil
            self.getBookingDataWith(0)
        }
        
  
    }
    
    //MARK: - custome method extension
    extension StaffCompleteBookingVC {
        
        func configureView(){
            
            if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]{
                if let userId = dict["_id"] as? String {
                    self.strUserId =  userId
                }
            }else{
                let userInfo = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] ?? ["":""]
                if let id = userInfo["_id"] as? String {
                    strUserId = id
                }
            }
            
            
            self.tblPastBooking.delegate = self
            self.tblPastBooking.dataSource = self
            
            self.tblPastBooking.rowHeight = UITableView.automaticDimension;
            self.tblPastBooking.estimatedRowHeight = 77.0;

            self.lblNoRecord.isHidden = true
            
            self.viewPastBooking.isHidden = false
        }
    

        @IBAction func btnBackAction(_ sender: Any){
            objAppShareData.clearNotificationData()
            navigationController?.popViewController(animated: true)
        }

        
    }
    
    // MARK: - table view Delegate and Datasource
    extension StaffCompleteBookingVC  {
        
        // MARK: - Tableview delegate methods
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
                return arrPastBookingData.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
        {
                let obj  : PastFutureBookingData = arrPastBookingData[indexPath.row]
                var objUser = UserDetail(dict: [:])
                
                if obj.arrUserDetail.count > 0{
                    objUser = obj.arrUserDetail[0]
                }
                
                    let cellIdentifier = "CellSmallPastFutureReview"
                    if let cell : CellSmallPastFutureReview = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)! as? CellSmallPastFutureReview {
                        
                        cell.indexPath = indexPath
                        obj.indexPath = indexPath
                         // cell.vwRating.value = CGFloat(obj.rating)
                        cell.setDataInCollection(obj: obj)
                        cell.lblDate.text = self.getFormattedBookingDateToShowFrom(strDate: obj.bookingDate)//obj.bookingDate
                        cell.lblDate.text = cell.lblDate.text! + ", " + obj.bookingTime
                        cell.lblAmount.text = "£" + obj.totalPrice
                        
                        if obj.customerType == "walking"{
                            if obj.arrClientInfo.count > 0 {
                                cell.lblName.text = obj.arrClientInfo[0].firstName+" "+obj.arrClientInfo[0].lastName
                            }
                            cell.imgVwProfile.image = #imageLiteral(resourceName: "cellBackground")
                            cell.lblWalkingStatus.isHidden = false
                        }else{
                            cell.lblWalkingStatus.isHidden = true
                            cell.imgVwProfile.image = #imageLiteral(resourceName: "cellBackground")
                            cell.lblName.text = obj.arrUserDetail[0].userName
                            let strImg = obj.arrUserDetail[0].profileImage
                            if strImg != "" {
                                if let url = URL(string: strImg){
                                    //cell.imgVwProfile.af_setImage(withURL: url, placeholderImage: UIImage(named: "cellBackground"))
                                    cell.imgVwProfile.sd_setImage(with: url, placeholderImage: UIImage(named: "cellBackground"))
                                }
                            }
                         }
                      
                          return cell
                    }else{
                        return UITableViewCell()
                    }
                    
               
            
        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
            //return
            var objPastFutureBookingData  : PastFutureBookingData = PastFutureBookingData(dict: ["":""])
                objPastFutureBookingData = arrPastBookingData[indexPath.row]
          
            self.gotoAppoitmentBookingVC(obj: objPastFutureBookingData)
        }
        
    }

    
    //MARK: - custome method extension
    extension StaffCompleteBookingVC {
        
        func getBookingDataWith(_ page: Int) {
            let parameters = ["type": "Past","staffId":strStaffId]
            self.webServiceCall_getBookingData(parameters: parameters)
        }
        
        
        func webServiceCall_getBookingData(parameters : [String:Any]){
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            self.lblNoRecord.isHidden = true
            objWebserviceManager.requestPost(strURL:  WebURL.userBookingHistory, params: parameters as [String : AnyObject] , success: { response in
                print(response)
                //  objWebserviceManager.StopIndicator()
                self.activity.stopAnimating()
 
                self.arrPastBookingDataAll.removeAll()
                let keyExists = response["responseCode"] != nil
                if  keyExists {
                    sessionExpireAlertVC(controller: self)
                }else{
                    let strStatus =  response["status"] as? String ?? ""
                    
                    if strStatus == k_success{
                            if let arr = response["data"] as? [[String : Any]]{
                                for dict in arr{
                                    let obj = PastFutureBookingData.init(dict: dict)
                                    self.arrPastBookingDataAll.append(obj)
                                }
                            }
                        self.arrPastBookingData = self.arrPastBookingDataAll
                    }else{
                        if strStatus == "fail"{
                        }else{
                            if let msg = response["message"] as? String{
                                objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                            }
                        }
                    }
                }
                
                    self.tblPastBooking.reloadData()
                    if self.arrPastBookingData.count == 0 {
                        self.lblNoRecord.isHidden = false
                    }else{
                        self.lblNoRecord.isHidden = true
                    }
               
                
            }) { (error) in
                self.activity.stopAnimating()
  
                    self.tblPastBooking.reloadData()
                    if self.arrPastBookingData.count == 0 {
                        self.lblNoRecord.isHidden = false
                    }else{
                        self.lblNoRecord.isHidden = true
                    }
                
                objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
            }
        }
        
        func callWebserviceFor_addCommentAndRating(parameters : [String:Any] , obj : PastFutureBookingData ){
            
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            
            self.activity.startAnimating()
            
            objWebserviceManager.requestPost(strURL: WebURL.bookingReviewRating, params: parameters  , success: { response in
                
                self.activity.stopAnimating()
                
                let keyExists = response["responseCode"] != nil
                if  keyExists {
                    sessionExpireAlertVC(controller: self)
                }else{
                    
                    let strStatus =  response["status"] as? String ?? ""
                    let strMsg = response["message"] as? String ?? kErrorMessage
                    
                    if strStatus == k_success{
                        obj.reviewStatus = 1
                        obj.isReviewAvailable = true
                        obj.reviewByUser = parameters["reviewByArtist"] as? String ?? ""
                        if let rating = parameters["rating"] as? String {
                            obj.artistRating = Int(Double(rating) ?? 0)
                        }
                        objAppShareData.showAlert(withMessage: strMsg, type: alertType.bannerDark,on: self)
                    }else{
                        objAppShareData.showAlert(withMessage: strMsg, type: alertType.bannerDark,on: self)
                    }
                }
                
            }){ error in
                
                self.activity.stopAnimating()
                objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
            }
        }
    }
    
    //MARK: - custome method extension
    extension StaffCompleteBookingVC {
        
        func getFormattedBookingDateToShowFrom(strDate : String) -> String{
            
            if strDate == ""{
                return ""
            }
            
            let formatter  = DateFormatter()
            formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
            formatter.dateFormat = "yyyy-MM-dd"
            if let date = formatter.date(from: strDate){
                formatter.dateFormat = "dd/MM/yyyy" //"dd MMMM yyyy"
                
                let strDate = formatter.string(from: date)
                return strDate
            }
            return ""
        }

        func gotoAppoitmentBookingVC(obj : PastFutureBookingData) {
            
            self.view.endEditing(true)
            let sb = UIStoryboard(name:"Booking",bundle:Bundle.main)
            let objChooseType = sb.instantiateViewController(withIdentifier:"ConfirmBookingVC") as! ConfirmBookingVC
            objChooseType.hidesBottomBarWhenPushed = true
            objChooseType.strBookingId =  String(obj._id)
            self.navigationController?.pushViewController(objChooseType, animated: true)
         }
 }



//
//  userSearchVC.swift
//  MualabBusiness
//
//  Created by mac on 06/04/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

class userSearchVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UIScrollViewDelegate {
    
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var txtSearchName: UITextField!
    @IBOutlet weak var tblUserSearch: UITableView!
    var refrease = false
    @IBOutlet weak var lblNoDataFound: UILabel!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    
    fileprivate var pageNo: Int = 0
    fileprivate var totalCount = 0
    private var arrArtistList = [ArtistList]()
    fileprivate var strSearchText = ""
    let pageSize = 10 // that's up to you, really
    let preloadMargin = 5 // or whatever number that makes sense with your page size
    var lastLoadedPage = 0
    
    //MARK: - refreshControl
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(self.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = appColor
        return refreshControl
    }()
    
    //MARK: - system method
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewConfigure()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.lblNoDataFound.isHidden = true
        self.txtSearchName.text = ""
        loadDataWithPageCount(page: 0, strSearchText: self.txtSearchName.text ?? "")
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        loadDataWithPageCount(page: 0, strSearchText: self.txtSearchName.text ?? "")
    }
}

//MARK: - Button extension

extension userSearchVC{
    
    @IBAction func btnSearchUser(_ sender: UIButton) {
        self.view.endEditing(true)
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK: - TextField delegate method extension
extension userSearchVC{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        txtSearchName.resignFirstResponder()
        self.view.endEditing(true)
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        
        self.loadDataWithPageCount(page: 0, strSearchText: "")
        return true
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        self.lblNoDataFound.isHidden = true
        let text = textField.text! as NSString
        if (text.length == 1)  && (string == "") {
            self.loadDataWithPageCount(page: 0, strSearchText: "")
        }
        
        var substring: String = textField.text!
        substring = (substring as NSString).replacingCharacters(in: range, with: string)
        substring = substring.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        searchAutocompleteEntries(withSubstring: substring)
        
        return true
    }
    
    func searchAutocompleteEntries(withSubstring substring: String) {
        if !(substring == "") {
            self.loadDataWithPageCount(page: 0, strSearchText: substring)
        }
    }
}

//MARK: - Tableview delegate method extension
extension userSearchVC{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrArtistList.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        if self.arrArtistList.count < self.totalCount {
            let nextPage: Int = Int(indexPath.item / pageSize) + 1
            let preloadIndex = nextPage * pageSize - preloadMargin
            // trigger the preload when you reach a certain point AND prevent multiple loads and updates
            if (indexPath.item >= preloadIndex && lastLoadedPage < nextPage) {
                loadDataWithPageCount(page: nextPage, strSearchText: self.strSearchText)
            }
        }
        
        let cellIdentifier = "CellUserListAddStaff"
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? CellUserListAddStaff {
            let objData = arrArtistList[indexPath.row]
            cell.lblUserName.text = objData.userName
            cell.imgUserProfile.image = #imageLiteral(resourceName: "cellBackground")
            if objData.profileImage !=  "" {
                if let url = URL(string: objData.profileImage){
                    //cell.imgUserProfile.af_setImage(withURL: url, placeholderImage: UIImage(named: "cellBackground"))
                    cell.imgUserProfile.sd_setImage(with: url, placeholderImage: UIImage(named: "cellBackground"))
                }
            }
            return cell
        } else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let objArtistList = arrArtistList[indexPath.row]
        objAppShareData.objModelHoldAddStaff.artistId = String(objArtistList._id )
        let userInfo = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] ?? ["":""]
        objAppShareData.objModelHoldAddStaff.businessId = String(describing: userInfo[UserDefaults.keys.id] ?? "")
        let name = objArtistList.userName
        objModelStaffDetail.name = name
        self.onDidSelect()
        let sb = UIStoryboard(name:"StaffList",bundle:Bundle.main)

        objAppShareData.objAddStaffDoneSubSubServices.arrDoneTimeSloartJson = ""
        objAppShareData.objAddStaffDoneSubSubServices.arrDoneServiceJson = ""
        objAppShareData.arrAddedStaffServices.removeAll()
            if let objVC = sb.instantiateViewController(withIdentifier:"AddStaffFormVC") as? AddStaffFormVC{
                    objVC.objArtistList = objArtistList
                    objVC.strHeader = "Add Staff"
                     objVC.hidesBottomBarWhenPushed = true
                    self.navigationController?.pushViewController(objVC, animated: true)
                }
     }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.view.endEditing(true)
    }
    
    func onDidSelect(){
        objAppShareData.objAddStaffDoneSubSubServices.arrDoneTimeSloartJson = ""
        objAppShareData.objModelHoldAddStaff.arrParseData.removeAll()
        objAppShareData.objBookSerSelectionVC.arrSelectedServicess.removeAll()
        objAppShareData.objModelEditTimeSloat.arrFinalTimes.removeAll()
        objAppShareData.editStaff = true
        objAppShareData.fromEditStaff = false
        objAppShareData.editStaffTimeSloat = false
    }
}

//MARK: - Custome method extension
extension userSearchVC{
    
    func  viewConfigure(){
        self.tblUserSearch.delegate = self
        self.tblUserSearch.dataSource = self
        self.txtSearchName.delegate = self
        tblUserSearch.addSubview(self.refreshControl)
        self.viewSearch.layer.borderWidth  = 1
        self.viewSearch.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
    }
    
    func loadDataWithPageCount(page: Int, strSearchText : String = "") {
        self.strSearchText = strSearchText.lowercased()
        lastLoadedPage = page
        self.pageNo = page
        self.txtSearchName.text = self.txtSearchName.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let strName = strSearchText
        var businessId = ""
        if let dictUser = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]{
            print("dictUser = ",dictUser)
            if let buId = dictUser[UserDefaults.keys.userId] as? String{
                businessId = String(buId)
            } else if let buId = dictUser[UserDefaults.keys.userId] as? Int{
                businessId = String(buId)
            }
         }
        
        let dicParam = ["search": strName,
                         "page": self.pageNo,
                         "limit": pageSize,
                         "artistId":businessId] as [String : Any]

        self.callWebserviceForGet_allArtist(dict: dicParam)
    }
}

//MARK:- Webservice Call
extension userSearchVC {
    
    func callWebserviceForGet_allArtist(dict: [String: Any]){
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        if  !self.refreshControl.isRefreshing && self.pageNo == 0 {
            self.activity.startAnimating()
        }
        
        self.lblNoDataFound.isHidden = true
        objWebserviceManager.requestPostForJson(strURL: WebURL.allArtist, params: dict , success: { response in
            self.refreshControl.endRefreshing()
            self.activity.stopAnimating()
            objActivity.stopActivity()
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else {
            if self.pageNo == 0 {
                self.arrArtistList.removeAll()
                self.tblUserSearch.reloadData()
            }
            
            let strSucessStatus = response["status"] as? String
            if strSucessStatus == k_success{
                 if let totalCount = response["totalCount"] as? Int{
                    self.totalCount = totalCount
                }
                self.parseResponce(response:response)
             }else{
                if strSucessStatus == "fail"{
                    
                }else{
                    let msg = response["message"] as? String ?? ""
                    objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                }
                self.activity.stopAnimating()
                if self.arrArtistList.count > 0{
                    self.lblNoDataFound.isHidden = true
                }else{
                    self.lblNoDataFound.isHidden = false
                }
            }
            
            self.tblUserSearch.reloadData()
            if self.arrArtistList.count==0{
                self.lblNoDataFound.isHidden = false
            }else{
                self.lblNoDataFound.isHidden = true
            }
            }
        }) { (error) in
            
            self.refreshControl.endRefreshing()
            self.activity.stopAnimating()
            
            self.tblUserSearch.reloadData()
            if self.arrArtistList.count==0{
                self.lblNoDataFound.isHidden = false
            }else{
                self.lblNoDataFound.isHidden = true
            }
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
    func parseResponce(response:[String : Any]){
        
        if let arr = response["artistList"] as? [[String:Any]]{
            if arr.count > 0{
                for ArtistData in arr{
                    let objArtistList = ArtistList.init(dict: ArtistData)
                    arrArtistList.append(objArtistList)
                }
            }
        }
        self.activity.stopAnimating()
        if self.arrArtistList.count > 0{
            self.lblNoDataFound.isHidden = true
        }else{
            self.lblNoDataFound.isHidden = false
        }
    }
}

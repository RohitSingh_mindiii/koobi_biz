//
//  UserListAddStaffVC.swift
//  MualabBusiness
//
//  Created by mac on 06/04/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit


class UserListAddStaffVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {
    
    @IBOutlet weak var lblNoDataFound: UILabel!
    @IBOutlet weak var tblUserList: UITableView!
    @IBOutlet weak var txtSearchName: UITextField!
    @IBOutlet weak var activity: UIActivityIndicatorView!

    //delete view outlate
    @IBOutlet weak var viewDeletePopup: UIView!

    fileprivate var arrStaffInfo = [AddStaffInfo]()
    fileprivate var arrUserList = [UserListModel]()
    fileprivate var IntGoVC = 0
    fileprivate var objArtistList = ArtistList(dict:[:])
    fileprivate var strStaffId = 0
    fileprivate var strArtistId = ""
    private  let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
      fileprivate var strSearchText = ""
    
    //MARK: - refreshControl
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(self.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = appColor
        return refreshControl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
            IntGoVC = 0
        self.lblNoDataFound.isHidden = true
         self.getArtistStaffList()
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        self.getArtistStaffList()
    }
    
}

//MARK: - Tableview delegate method extension
extension UserListAddStaffVC{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          return self.arrUserList.count
        }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "CellUserListAddStaff"
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? CellUserListAddStaff {
            if self.arrUserList.count > 0 {
           
            let objData = arrUserList[indexPath.row]
            cell.lblTitle.text = objData.job
            cell.lblUserName.text = objData.staffName
            cell.imgUserProfile.image = #imageLiteral(resourceName: "cellBackground")
            
            if let strUrl = objData.staffImage {
                if let url = URL(string: strUrl){
                    //cell.imgUserProfile.af_setImage(withURL: url, placeholderImage: UIImage(named: "cellBackground"))
                    cell.imgUserProfile.sd_setImage(with: url, placeholderImage: UIImage(named: "cellBackground"))
                }
            }
                if objData.status == "0"{
                    cell.viewRequest.isHidden = false
                }else{
                    cell.viewRequest.isHidden = true
                }
            return cell
            }else{
                return UITableViewCell()
            }
           
        }else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if  IntGoVC != 0 {
            return
        }
        let selectObj = arrUserList[indexPath.row]
        
     if selectObj.status == "1"{
        IntGoVC = 1
        objModelStaffDetail.name = arrUserList[indexPath.row].staffName!
        objModelStaffDetail.image = arrUserList[indexPath.row].staffImage!
        
        objAppShareData.objModelHoldAddStaff.artistId = String(arrUserList[indexPath.row].staffId ?? 0)
        objAppShareData.objModelHoldAddStaff._id = String(arrUserList[indexPath.row]._id ?? 0)
        objAppShareData.objModelHoldAddStaff.staffId = String(arrUserList[indexPath.row].staffId ?? 0)
 
      
        let dictNextVC = ["userName":selectObj.staffName ?? "",
                          "profileImage":selectObj.staffImage ?? "",
                          "_id":selectObj.staffId ?? ""] as [String : Any]
        
        objArtistList =  ArtistList(dict:dictNextVC )
         self.onDidSelect()
        var businessId = ""
        if let dictUser = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]{
            print("dictUser = ",dictUser)
            
            if let id = dictUser[UserDefaults.keys.userId] as? String{
                businessId = id
            }else if let id = dictUser[UserDefaults.keys.userId] as? Int{
                businessId = String(id)
            }
        }
        
        let StaffId = arrUserList[indexPath.row].staffId
        let params = [
            "artistId":StaffId ?? 0,
            "businessId":businessId
            ] as [String : AnyObject]
         self.callWebserviceForGetStaffInformation(param: params)
       }
    }
    
    func onDidSelect(){
        if  let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? User{
            objAppShareData.objModelHoldAddStaff.businessId = String(describing: userInfo.userId)
            objAppShareData.objModelEditTimeSloat.arrFinalTimes.removeAll()
            objAppShareData.objAddStaffDoneSubSubServices.arrDoneTimeSloartJson = ""
            objAppShareData.objModelHoldAddStaff.arrParseData.removeAll()
            objAppShareData.objBookSerSelectionVC.arrSelectedServicess.removeAll()
            objAppShareData.fromEditStaff = true
            objAppShareData.editStaff = true
            objAppShareData.editStaffTimeSloat = true
            objAppShareData.addTimeSloat = true
        }else {
            let decodedN = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decodedN) as! [String:Any]
            if let id = userInfo["_id"] as? Int {
               objAppShareData.objModelHoldAddStaff.businessId = String(describing: id)
               objAppShareData.objModelEditTimeSloat.arrFinalTimes.removeAll()
               objAppShareData.objAddStaffDoneSubSubServices.arrDoneTimeSloartJson = ""
               objAppShareData.objModelHoldAddStaff.arrParseData.removeAll()
               objAppShareData.objBookSerSelectionVC.arrSelectedServicess.removeAll()
               objAppShareData.fromEditStaff = true
               objAppShareData.editStaff = true
               objAppShareData.editStaffTimeSloat = true
               objAppShareData.addTimeSloat = true
            }else if let id = userInfo["_id"] as? String {
                objAppShareData.objModelHoldAddStaff.businessId = id
                objAppShareData.objModelEditTimeSloat.arrFinalTimes.removeAll()
                objAppShareData.objAddStaffDoneSubSubServices.arrDoneTimeSloartJson = ""
                objAppShareData.objModelHoldAddStaff.arrParseData.removeAll()
                objAppShareData.objBookSerSelectionVC.arrSelectedServicess.removeAll()
                objAppShareData.fromEditStaff = true
                objAppShareData.editStaff = true
                objAppShareData.editStaffTimeSloat = true
                objAppShareData.addTimeSloat = true
            }
        }
    }
    
    @available(iOS 11.0, *)
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
    {
        let objReminder = self.arrUserList[indexPath.row]

        let Delete = UIContextualAction(style: .normal, title:  "", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            let dictUser = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] ?? ["":""]
            print("dictUser = ",dictUser)
            let busId = dictUser[UserDefaults.keys.userId] as? String ?? ""
            
                self.strArtistId = busId
                self.strStaffId = objReminder.staffId!
                self.showDeletePopup()
                success(true)
            })
        
        Delete.image = #imageLiteral(resourceName: "delete")
        Delete.backgroundColor =  #colorLiteral(red: 0.9137254902, green: 0.2117647059, blue: 0.2274509804, alpha: 1)
        
        let cancelBTN = UIContextualAction(style: .normal, title:  "", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
 
            let dictUser = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] ?? ["":""]
                print("dictUser = ",dictUser)
            let busId = dictUser[UserDefaults.keys.userId] as? String ?? ""
            self.strArtistId = String(describing: busId)
            self.strStaffId = objReminder.staffId!
            self.showDeletePopup()
            success(true)
        })
        cancelBTN.image = #imageLiteral(resourceName: "cross")
        cancelBTN.backgroundColor =  #colorLiteral(red: 0.9137254902, green: 0.2117647059, blue: 0.2274509804, alpha: 1)
        
        if objReminder.status == "0"{
            return UISwipeActionsConfiguration(actions: [cancelBTN])
        }else{
            return UISwipeActionsConfiguration(actions: [Delete])
        }
            return UISwipeActionsConfiguration(actions: [Delete])
    }
}

//MARK: - Custome method extension
extension UserListAddStaffVC{
    
    func  configureView() {
        self.txtSearchName.delegate = self

        self.tblUserList.delegate = self
        self.tblUserList.dataSource = self
        self.tblUserList.addSubview(self.refreshControl)
        self.viewDeletePopup.isHidden = true
    }
    
    
    
    func showDeletePopup(){
        self.viewDeletePopup.isHidden = false
    }
}

//MARK: - Custome method extension
extension UserListAddStaffVC{
    
    func getArtistStaffList(){
        var businessId = ""
        if let dictUser = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]{
            print("dictUser = ",dictUser)
            if let busId = dictUser[UserDefaults.keys.userId] as? String{
                businessId = busId
            }else if let busId = dictUser[UserDefaults.keys.userId] as? Int{
               businessId = String(busId)
            }
        }
        let param = ["artistId":businessId, "search":self.txtSearchName.text ?? ""]
        self.callWebserviceForGet_artistStaff(dict: param)
    }
}

//MARK: - Button extension
extension UserListAddStaffVC{
    @IBAction func btnNoDelete(_ sender: UIButton) {
    self.viewDeletePopup.isHidden = true
        self.tblUserList.reloadData()
    }
    @IBAction func btnYesDelete(_ sender: UIButton) {
        let param = ["businessId":self.strArtistId,"staffId":self.strStaffId] as [String : Any]
        self.callWebserviceForDeleteStaff(param: param as [String : AnyObject])
        
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnAddNewStaff(_ sender: UIButton) {
        
        let sb = UIStoryboard(name:"StaffList",bundle:Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"userSearchVC") as? userSearchVC{
            objVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(objVC, animated: true)
        }
    }
}

//MARK:- Webservice Call
extension UserListAddStaffVC {
    
    func callWebserviceForGet_artistStaff(dict: [String : Any]){
        self.lblNoDataFound.isHidden = true
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        self.arrUserList.removeAll()
        if  !self.refreshControl.isRefreshing {
            self.activity.startAnimating()
        }
        
        objServiceManager.requestPost(strURL: WebURL.artistStaff, params: dict  , success: { response in
            
            objServiceManager.StopIndicator()
            self.refreshControl.endRefreshing()

            let keyExists = response["responseCode"] != nil
            if  keyExists {
                self.activity.stopAnimating()

                sessionExpireAlertVC(controller: self)
            }else{
                
                let strStatus =  response["status"] as? String ?? ""
                if strStatus == k_success{
                    self.parseResponce(response:response)
                    self.activity.stopAnimating()
                 }else{
                    self.tblUserList.reloadData()
                    self.activity.stopAnimating()
                    if self.arrUserList.count != 0 {
                        self.lblNoDataFound.isHidden = true
                    }else{
                        self.lblNoDataFound.isHidden = false
                    }
                  }
            }
        }){ error in
            self.refreshControl.endRefreshing()
            self.tblUserList.reloadData()
            self.activity.stopAnimating()
            if self.arrUserList.count > 0{
                self.lblNoDataFound.isHidden = true
            }else{
                self.lblNoDataFound.isHidden = false
            }
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
    func parseResponce(response:[String : Any]){
        self.arrUserList.removeAll()
        if let arr = response["staffList"] as? [[String:Any]]{
            if arr.count > 0{
                for dictArtistData in arr {
                    let objArtistList = UserListModel.init(dict: dictArtistData)
                    self.arrUserList.append(objArtistList)
                }
            }
        }
        self.tblUserList.reloadData()
        self.activity.stopAnimating()
        if self.arrUserList.count > 0{
            self.lblNoDataFound.isHidden = true
        }else{
            self.lblNoDataFound.isHidden = false
        }
    }
}

//MARK: - TextField delegate method extension
extension UserListAddStaffVC{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        txtSearchName.resignFirstResponder()
        self.view.endEditing(true)
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        self.loadDataWithPageCount(page: 0, strSearchText: "")
        return true
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        
        let text = textField.text! as NSString
        if (text.length == 1)  && (string == "") {
            self.loadDataWithPageCount(page: 0, strSearchText: "")
        }
        
        var substring: String = textField.text!
        substring = (substring as NSString).replacingCharacters(in: range, with: string)
        substring = substring.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        searchAutocompleteEntries(withSubstring: substring)
        
        return true
    }
    
    func searchAutocompleteEntries(withSubstring substring: String) {
        if !(substring == "") {
            self.loadDataWithPageCount(page: 0, strSearchText: substring)
        }
    }
    func loadDataWithPageCount(page: Int, strSearchText : String = "") {
        self.strSearchText = strSearchText.lowercased()
    
        self.txtSearchName.text = self.txtSearchName.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let strName = strSearchText
        
        var businessId = ""
        if let dictUser = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]{
            print("dictUser = ",dictUser)
            businessId = dictUser[UserDefaults.myDetail.id] as? String ?? ""
        }
        
        let dicParam = [ "search": strName,
                         "artistId":objAppShareData.strArtistID,
                         "businessId":businessId
            ] as [String : Any]
 
        self.callWebserviceForGet_artistStaff(dict: dicParam as [String : Any])
    }
}
fileprivate extension UserListAddStaffVC{
    
    func callWebserviceForGetStaffInformation(param:[String:AnyObject]){
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
         objServiceManager.requestPostForJson(strURL: WebURL.staffInformation, params:param, success: { response in
             objActivity.stopActivity()
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else {
            
            if  response["status"] as! String == "success"{
                print(response)
                objActivity.stopActivity()
                self.saveData(dict:response)
            }else{
                let msg = response["message"] as? String ?? ""
                objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
            }
            }
        }) { error in
            objActivity.stopActivity()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
   
    //save data
    func saveData(dict:[String:Any]){
        objAppShareData.arrAddedStaffServicesOriginal.removeAll()
        objAppShareData.arrAddedStaffServices.removeAll()

        self.arrStaffInfo.removeAll()
        objAppShareData.objModelHoldAddStaff.arrParseData.removeAll()
        if let dict = dict["staffDetails"] as? [[String : Any]]{
            for dicService in dict{
                let obj = AddStaffInfo.init(dict: dicService)
                self.arrStaffInfo.append(obj)
            }
            
//////////////////////////////////////////////////////////////////////////////////////////////////////

            let sloatArray = self.arrStaffInfo[0].staffHours
            if sloatArray.count > 0{
                for dicSloats in sloatArray{

                    var strSrtartTime = ""
                    var strEndTime = ""
                    var day = ""
                    
                    strSrtartTime = dicSloats.startTime
                    strEndTime = dicSloats.endTime
                    day = String(dicSloats.day)
                    day = getDayFromSelectedDate(strDate: day)
 
                    if objAppShareData.objModelEditTimeSloat.arrFinalTimes.count > 0 {
                        var addtrue = true
                        for dayChack in objAppShareData.objModelEditTimeSloat.arrFinalTimes{
                            if day == dayChack.strDay{
                                addtrue = false
                               let objSloats = timeSloat.init(startTime: strSrtartTime, endTime: strEndTime)
                                dayChack.arrTimeSloats.append(objSloats!)
                            }
                        }
                         if addtrue == true{
                           let objSloats = timeSloat.init(startTime: strSrtartTime, endTime: strEndTime)
                            let objTimaSloat = openingTimes(open: true, day: day, times: [objSloats!])
                            objAppShareData.objModelEditTimeSloat.arrFinalTimes.append(objTimaSloat!)
                        }
                      }else{
                        let objSloats = timeSloat.init(startTime: strSrtartTime, endTime: strEndTime)
                        let objTimaSloat = openingTimes(open: true, day: day, times: [objSloats!])
                        objAppShareData.objModelEditTimeSloat.arrFinalTimes.append(objTimaSloat!)
                    }
                }
            }
            objAppShareData.objAddStaffDoneSubSubServices.arrDoneTimeSloartJson = jsonFromArray(arrBusinessHours: objAppShareData.objModelEditTimeSloat.arrFinalTimes)
            
//////////////////////////////////////////////////////////////////////////////////////////////////////
            objAppShareData.fromStaffSelectCell = true
            let obj = arrStaffInfo[0]
            objAppShareData.objUsersAddServiceVC.arrStaffInfo.removeAll()
            objAppShareData.objUsersAddServiceVC.arrStaffInfo = [obj]
            let arr = obj.staffService
            objAppShareData.objBookSerSelectionVC.arrSelectedServicess = arr
            
            if objAppShareData.objBookSerSelectionVC.arrSelectedServicess.count > 0 {
                objAppShareData.arrAddedStaffServices.removeAll()
                objAppShareData.arrAddedStaffServicesOriginal.removeAll()
                
            for newDic in objAppShareData.objBookSerSelectionVC.arrSelectedServicess{
                let newArtistServiceId = String(newDic.artistServiceId)
                let newArtistId = String(newDic.artistId)
                let newBusinessId = String(newDic.businessId)
                let newCompletionTime = String(newDic.completionTime)
                let newInCallPrice = newDic.inCallPrice
                let newOutCallPrice = newDic.outCallPrice
                let newServiceId = String(newDic.serviceId)
                let newSubserviceId = String(newDic.subServiceId)
                var mainPrice = newDic.inCallPrice
                var bukType = ""
                if float_t(newDic.inCallPrice) == 0{
                    bukType = "Outcall"
                    mainPrice = newDic.outCallPrice
                }else if float_t(newDic.outCallPrice) == 0{
                    bukType = "Incall"
                }else{
                    bukType = "Both"
                }
                let a = ["_id":String(newDic.artistServiceId),
                         "mainBookingId":String(newDic.serviceId),
                         "mainCategoryId":String(newDic.subServiceId),
                         "serviceName":"",
                         "price":mainPrice,
                         "incallPrice":newDic.inCallPrice,
                         "outcallPrice":newDic.outCallPrice,
                         "bookingType":bukType,
                         "describe":"",
                         "time":newDic.completionTime] as [String : Any]
                let objModelServicesList = ModelServicesList(dict: a)
                objAppShareData.arrAddedStaffServices.append(objModelServicesList)
                objAppShareData.arrAddedStaffServicesOriginal.append(objModelServicesList)
                
                
                let objModel = ModelHoldAddStaffId(title: "", artistId: newArtistId, artistServiceId: newArtistServiceId, businessId: newBusinessId, completionTime: newCompletionTime, inCallPrice: String(newInCallPrice), serviceId: newServiceId, outCallPrice: String(newOutCallPrice), subserviceId: newSubserviceId)
                if objAppShareData.objModelHoldAddStaff.arrParseData.count > 0{
                    for dic in objAppShareData.objModelHoldAddStaff.arrParseData{
                        if newArtistServiceId == dic.artistServiceId{
                        }else{
                          objAppShareData.objModelHoldAddStaff.arrParseData.append(objModel!)
                        }
                    }
                }else{
                objAppShareData.objModelHoldAddStaff.arrParseData.append(objModel!)
                }
            }
        }
            let sb = UIStoryboard(name:"StaffList",bundle:Bundle.main)
            if let objVC = sb.instantiateViewController(withIdentifier:"AddStaffFormVC") as? AddStaffFormVC{
                 objVC.objArtistList = objArtistList
                objVC.strHeader = "My Staff"
                objVC.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(objVC, animated: true)
            }
        }
    }
    
    
    //MARK: - Make String for API
    func jsonFromArray(arrBusinessHours:[openingTimes]) ->String{
        var arr = [[String:Any]]()
        var dataString = ""
        for (_,openings) in arrBusinessHours.enumerated(){
            for time in openings.arrTimeSloats{
                let das = getDayNoFromSelectedDay(strDate: openings.strDay)
                let dic = ["day":das,
                           "startTime":time.strStartTime,
                           "endTime":time.strEndTime] as [String : Any]
                //"status":Int(truncating:NSNumber.init(value: openings.isOpen))
                arr.append(dic)
            }
        }
        
        if let objectData = try? JSONSerialization.data(withJSONObject: arr, options: JSONSerialization.WritingOptions(rawValue: 0)) {
            let objectString = String(data: objectData,encoding: .utf8)
            dataString = objectString!
            return dataString
        }
        return dataString
    }
}

//MARK: - delete staff api
fileprivate extension UserListAddStaffVC{
    func callWebserviceForDeleteStaff(param:[String:AnyObject]){
        self.viewDeletePopup.isHidden = true
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        objActivity.startActivityIndicator()
        objServiceManager.requestPostForJson(strURL: WebURL.deleteStaff, params:param, success: { response in
            // self.arrServices.removeAll()
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                objActivity.stopActivity()
                sessionExpireAlertVC(controller: self)
            }else {
                if  response["status"] as! String == "success"{
                    print(response)
                    self.getArtistStaffList()
                }else{
                    objActivity.stopActivity()
                    let msg = response["message"] as? String ?? ""
                    objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                }
            }
        }) { error in
            objActivity.stopActivity()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
}
extension UserListAddStaffVC{
    func getDayFromSelectedDate(strDate:String)-> String{
        var Day = "Day"
        if strDate == "6"{
            Day = "Sunday"//"Sun"
        }else if strDate == "0"{
            Day = "Monday"//Mon"
        }else if strDate == "1"{
            Day = "Tuesday"//Tue"
        }else if strDate == "2"{
            Day = "Wednesday"//"Wed"
        }else if strDate == "3"{
            Day = "Thursday"//"Thu"
        }else if strDate == "4"{
            Day = "Friday"//"Fri"
        }else if strDate == "5"{
            Day = "Saturday"//"Sat"
        }else{
            Day = "Day"
        }
        return Day
    }
    func getDayNoFromSelectedDay(strDate:String)-> Int{
        var Day = 0
        
        if strDate == "Sunday"{
            Day = 6//"Sun"
        }else if strDate == "Monday"{
            Day = 0//Mon"
        }else if strDate == "Tuesday"{
            Day = 1//Tue"
        }else if strDate == "Wednesday"{
            Day = 2//"Wed"
        }else if strDate == "Thursday"{
            Day = 3//"Thu"
        }else if strDate == "Friday"{
            Day = 4//"Fri"
        }else if strDate == "Saturday"{
            Day = 5//"Sat"
        }else{
            Day = 0
        }
        return Day
    }

    func getDayOfWeek(_ today:String) -> Int? {
        
        // returns an integer from 1 - 7, with 1 being Sunday and 7 being Saturday
        let formatter  = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy hh:mm a"// "yyyy-MM-dd"
        guard let todayDate = formatter.date(from: today) else { return nil }
        let myCalendar = Calendar(identifier: .gregorian)
        let weekDay = myCalendar.component(.weekday, from: todayDate)
        return weekDay
    }
}

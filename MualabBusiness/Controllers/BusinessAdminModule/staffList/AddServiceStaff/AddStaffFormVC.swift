//
//  AddStaffFormVC.swift
//  MualabBusiness
//
//  Created by Mac on 11/12/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import DropDown
import CoreData
import Foundation

fileprivate enum day:Int {
    case Monday
    case Tuesday
    case Wednesday
    case Thursday
    case Friday
    case Saturday
    case Sunday
    
    var string: String {
        return String(describing: self)
    }
}
class AddStaffFormVC: UIViewController,UITextViewDelegate,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource{
    //uper labels view
    var objArtistList = ArtistList(dict: [:])
     fileprivate var arrOpeningTimes = [openingTimes]()

        @IBOutlet weak var lblHollydayAllocation:UITextField!
        @IBOutlet weak var txtSalary:UITextField!
        @IBOutlet weak var txtMessage:UITextView!
        @IBOutlet weak var lblSocialMediaAccess:UILabel!
        @IBOutlet weak var lblHeader:UILabel!
        @IBOutlet weak var lblJobTitle:UILabel!
        @IBOutlet weak var lblUserName:UILabel!
        @IBOutlet weak var imgUser:UIImageView!
        @IBOutlet weak var lblJobType:UILabel!
        @IBOutlet weak var viewCompleteBooking:UIView!
    @IBOutlet weak var lblTableHeader:UILabel!

    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var viewBottumTable: UIView!
    
        let dropDown = DropDown()
        var strHeader = "Add Staff"
        let arrJobTitle = ["Beginner","Moderate","Expert"]
    let arrSocialMediaType = ["Admin","Editor","Moderator"]
    fileprivate var forJobTitle = false
    var arrJobType = [String]()

        override func viewDidLoad() {
            super.viewDidLoad()
            self.viewBottumTable.isHidden = true
            self.tblView.delegate = self
            self.tblView.dataSource = self
            self.viewCompleteBooking.isHidden = true
            self.txtMessage.delegate = self
            self.txtSalary.delegate = self
            self.lblHeader.text = self.strHeader
            self.addAccesorryToKeyBoard()
           
            if  let dict = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]{
                let  businessType = dict[UserDefaults.keys.serviceType] as? String ?? ""
                if businessType == "1"{
                    self.lblJobType.text = "Incall"
                }else  if businessType == "2"{
                    self.lblJobType.text = "Outcall"
                }else  if businessType == "3"{
                    self.lblJobType.text = "All Types"
                }else{
                    self.lblJobType.text = "All Types"
                }
            }
            
            self.addGesturesToView()
    }
    func addGesturesToView() -> Void {
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.topToBottomSWIPE))
        swipeRight.direction = .down
        self.viewBottumTable.addGestureRecognizer(swipeRight)
    }
    
    @objc func topToBottomSWIPE() ->Void {
        self.view.endEditing(true)
        self.viewBottumTable.isHidden = true
    }
        
        override func viewWillAppear(_ animated: Bool) {
          self.timeSloats()
            self.setuoDropDownAppearance()
            self.imgUser.image = #imageLiteral(resourceName: "cellBackground")
            let strUrl = objArtistList.profileImage
                 if let url = URL(string: strUrl){
                    //self.imgUser.af_setImage(withURL: url, placeholderImage: UIImage(named: "cellBackground"))
                    self.imgUser.sd_setImage(with: url, placeholderImage: UIImage(named: "cellBackground"))
                }
            self.lblUserName.text = objArtistList.userName
            dataParsing()
     }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
}
    //MARK: - custome method extension
    extension AddStaffFormVC{
    func dataParsing(){
        
        if objAppShareData.fromStaffSelectCell == true && objAppShareData.objUsersAddServiceVC.arrStaffInfo.count > 0 {
            self.lblJobType.text = "All Types"
            if objAppShareData.objUsersAddServiceVC.arrStaffInfo[0].bookingType == "1"{
                self.lblJobType.text = "Incall"
            }else if objAppShareData.objUsersAddServiceVC.arrStaffInfo[0].bookingType == "2"{
                self.lblJobType.text = "Outcall"
            }
            objAppShareData.fromStaffSelectCell = false
            self.viewCompleteBooking.isHidden = false
            self.lblSocialMediaAccess.text = objAppShareData.objUsersAddServiceVC.arrStaffInfo[0].mediaAccess
            self.lblJobTitle.text = objAppShareData.objUsersAddServiceVC.arrStaffInfo[0].job
            self.lblHollydayAllocation.text = objAppShareData.objUsersAddServiceVC.arrStaffInfo[0].holiday
            self.lblHollydayAllocation.text = String(objAppShareData.objUsersAddServiceVC.arrStaffInfo[0].totalHoliday)
            self.txtSalary.text = String(objAppShareData.objUsersAddServiceVC.arrStaffInfo[0].salary)
            self.txtMessage.text = String(objAppShareData.objUsersAddServiceVC.arrStaffInfo[0].message)

            self.lblUserName.text = objModelStaffDetail.name
            if objModelStaffDetail.image != "" {
                if let url = URL(string: objModelStaffDetail.image){
                    //self.imgUser.af_setImage(withURL: url, placeholderImage: UIImage(named: "cellBackground"))
                    self.imgUser.sd_setImage(with: url, placeholderImage: UIImage(named: "cellBackground"))
                }
                
            }
        }}
        
        
        func checkSalary()->Bool{
            var bool = true
            self.txtSalary.text = self.txtSalary.text?.trimmingCharacters(in: .whitespacesAndNewlines)

            if self.txtSalary.text != "" && self.txtSalary.text != nil {
                let value = Double(self.txtSalary.text ?? "00.00") ?? 00.00
                if value != 0{
                    bool = false
                }
            }
            return bool
        }        
}



//MARK: - Button Extension
fileprivate extension AddStaffFormVC{
    @IBAction func btnJobTypeChange(_ sender: UIButton) {
        if  let dict = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]{
           let  businessType = dict[UserDefaults.keys.businessType] as? String ?? ""
            if businessType == "1"{
                arrJobType = ["Incall"]
            }else  if businessType == "2"{
                arrJobType = ["Outcall"]
            } else{
                arrJobType = ["All Types","Incall","Outcall"]
                forJobTitle = false
                self.viewBottumTable.isHidden = false
                self.tblView.reloadData()
                self.lblTableHeader.text = "Booking Types"
            }

        }
    }
    
    
    func alertForWarning(setText:String){
        let alert = UIAlertController(title: "Alert", message: "Are you sure? if you change job type already added services will be deleted", preferredStyle: .alert)
        let ok = UIAlertAction(title: "Yes", style: .default, handler: { action in
            objAppShareData.arrAddedStaffServices.removeAll()
            self.lblJobType.text = setText
        })
        alert.addAction(ok)
        let cancel = UIAlertAction(title: "No", style: .default, handler: { action in
        })
        alert.addAction(cancel)
        DispatchQueue.main.async(execute: {
            self.present(alert, animated: true)
        })
    }
    
    
    
        @IBAction func btnContinews(_ sender: UIButton) {
            self.view.endEditing(true)
            
            self.lblJobTitle.text = self.lblJobTitle.text?.trimmingCharacters(in: .whitespacesAndNewlines)
            self.lblSocialMediaAccess.text = self.lblSocialMediaAccess.text?.trimmingCharacters(in: .whitespacesAndNewlines)
            self.lblHollydayAllocation.text = self.lblHollydayAllocation.text?.trimmingCharacters(in: .whitespacesAndNewlines)
            self.txtSalary.text = self.txtSalary.text?.trimmingCharacters(in: .whitespacesAndNewlines)
            self.txtMessage.text = self.txtMessage.text?.trimmingCharacters(in: .whitespacesAndNewlines)

            if self.lblJobTitle.text == "" || self.lblJobTitle.text == "Select Job Title"{
                objAppShareData.showAlert(withMessage: "Select job title", type: alertType.bannerDark, on: self)
            }else if objAppShareData.arrAddedStaffServices.count == 0{
                objAppShareData.showAlert(withMessage: "Select at least one service for staff", type: alertType.bannerDark, on: self)
//            }else if self.lblSocialMediaAccess.text == "" || self.lblSocialMediaAccess.text == "Social Media Access"{
//                objAppShareData.showAlert(withMessage: "Select social media access", type: alertType.bannerDark, on: self)
            }else  if self.txtSalary.text?.count == 0{
                    objAppShareData.showAlert(withMessage: "Please enter salary", type: alertType.bannerDark, on: self)
            }else if self.checkSalary()  == true{
                objAppShareData.showAlert(withMessage: "Please enter valid salary", type: alertType.bannerDark, on: self)
            }else{
//            else if self.txtMessage.text?.count == 0{
//                objAppShareData.showAlert(withMessage: "Please enter message", type: alertType.bannerDark, on: self)
//            }
          
                
                
                
                if objAppShareData.fromEditStaff == true{
                    if self.lblHollydayAllocation.text != "" && self.lblHollydayAllocation.text != nil{
                        if  objAppShareData.objUsersAddServiceVC.arrStaffInfo[0].alreadyGivenHoliday > Int(self.lblHollydayAllocation.text ?? "0") ?? 0 {
                            objAppShareData.showAlert(withMessage: "Please enter more than "+String(objAppShareData.objUsersAddServiceVC.arrStaffInfo[0].alreadyGivenHoliday)+" holiday", type: alertType.bannerDark, on: self)
                            return

                        }

                    }
                }
                var arrData = [String]()
                
                for objMain in objAppShareData.arrAddedStaffServices{
                       arrData.append(objMain.id)
                 }
                arrData = Array(Set(arrData))
                var strArrParseDataIds = ""
                if let objectData = try? JSONSerialization.data(withJSONObject: arrData, options: JSONSerialization.WritingOptions(rawValue: 0)) {
                    objAppShareData.objModelHoldAddStaff.strArrParseDataIds = String(data: objectData, encoding: .utf8)!
                    strArrParseDataIds = String(data: objectData, encoding: .utf8)!
                }


                //let strArrParseDataIds = objAppShareData.objModelHoldAddStaff.strArrParseDataIds
                
                var holiday = ""
                var staffType = ""
                var editId = ""

                if self.lblHollydayAllocation.text == "Holiday Allocation" || self.lblHollydayAllocation.text == ""{
                    holiday = ""
                }else{
                    holiday = self.lblHollydayAllocation.text!
                }
                if objAppShareData.fromEditStaff == true {
                    staffType = "edit"
                    editId = objAppShareData.objModelHoldAddStaff._id
                } else {
                    staffType = ""
                    editId = ""
                }
                var businessId = ""
                if  let dict = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]{
                    businessId = dict[UserDefaults.keys.userId] as? String ?? ""
                    
                   if let buId = dict[UserDefaults.keys.userId] as? String{
                    businessId = String(buId)
                    } else if let buId = dict[UserDefaults.keys.userId] as? Int{
                      businessId = String(buId)
                    }

                }
                
                var bookingType = "3"
                if self.lblJobType.text == "Incall"{
                    bookingType = "1"
                }else if self.lblJobType.text == "Outcall"{
                    bookingType = "2"
                }
                
                
                let salary = String(format: "%.2f", Double(self.txtSalary.text ?? "0.00") ?? 0.00)
                 let param = ["artistId":String(objArtistList._id),
                             "businessId":businessId,
                             "job":self.lblJobTitle.text!,
                             "mediaAccess":self.lblSocialMediaAccess.text ?? "",
                             "staffService":strArrParseDataIds,
                             "staffHours":objAppShareData.addStaffTimeSloat,
                             "type":staffType,
                             "holiday":holiday,
                             "salaries":salary,
                             "bookingType":bookingType,
                             "message":self.txtMessage.text ?? "",
                             "editId":editId,"serviceType":"3"]
                
                print("param = ",param)
                self.callWebServicesForAddStaff(dict: param)

            }
 

         }
        
        
        @IBAction func btnBack(_ sender: UIButton) {
            objAppShareData.manageNavigation = false
            self.navigationController?.popViewController(animated: true)
          }
    
        @IBAction func btnJobTitle(_ sender: UIButton) {
            self.view.endEditing(true)
            
            let objGetData = self.arrJobTitle.map { $0 }
            guard objGetData.isEmpty == false else {
                objAppShareData.showAlert(withMessage: "No business type found", type: alertType.bannerDark,on: self)
                return
            }
                self.viewBottumTable.isHidden = false
                forJobTitle = true
                self.lblTableHeader.text = "Job Title"
                self.tblView.reloadData()
        }
        
        
    
        @IBAction func btnSocialMediaAccess(_ sender: UIButton) {
            
            self.view.endEditing(true)
            let objGetData = self.arrSocialMediaType.map { $0}
            guard objGetData.isEmpty == false else {
                objAppShareData.showAlert(withMessage: "No category type available", type: alertType.bannerDark,on: self)
                return
            }
            
            dropDown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
            dropDown.anchorView = sender // UIView or UIBarButtonItem
            dropDown.direction = .bottom
            dropDown.dataSource = objGetData
            dropDown.width = self.view.frame.size.width - 50
            dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                print("Selected item: \(item) at index: \(index)")
                self.lblSocialMediaAccess.text = item
                let index_2 = index
            }
            dropDown.show()
        }
    func setuoDropDownAppearance()  {
        let appearance = DropDown.appearance()
        appearance.cellHeight = 40
        appearance.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        appearance.selectionBackgroundColor = UIColor(red: 0.6494, green: 0.8155, blue: 1.0, alpha: 0.2)
        appearance.separatorColor = UIColor.black// #colorLiteral(red: 0.8392156863, green: 0.8392156863, blue: 0.8392156863, alpha: 1)
        appearance.cornerRadius = 5
        appearance.shadowColor = UIColor.black //UIColor(white: 0.6, alpha: 1)
        appearance.shadowOpacity = 1
        appearance.shadowRadius = 1
        appearance.animationduration = 0.25
        appearance.textColor = .black
        appearance.textFont = UIFont (name: "Nunito-Regular", size: 17) ?? .systemFont(ofSize: 14)
        
    }
    @IBAction func btnWorkingHours(_ sender:Any){
        let sb = UIStoryboard(name:"StaffList",bundle:Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"StaffWorkingHoursVC") as? StaffWorkingHoursVC{
            self.navigationController?.pushViewController(objVC, animated: true)
        }
    }
    @IBAction func btnCompleteBooking(_ sender:Any){
        let sb = UIStoryboard(name:"StaffList",bundle:Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"StaffCompleteBookingVC") as? StaffCompleteBookingVC{
            objVC.strStaffId = String(objArtistList._id)
            self.navigationController?.pushViewController(objVC, animated: true)
        }
    }
    
        @IBAction func btnServices(_ sender:Any){
            let sb = UIStoryboard(name:"StaffList",bundle:Bundle.main)
            if let objVC = sb.instantiateViewController(withIdentifier:"ServiceListAddStaffVC") as? ServiceListAddStaffVC{
                objVC.objArtistList = objArtistList
                objVC.bookingType = self.lblJobType.text ?? "All Types"
                 self.navigationController?.pushViewController(objVC, animated: true)
            }
            
        }
}
        
        //MARK Get TimeSloat
 fileprivate extension AddStaffFormVC{
    
            func timeSloats(){
                //           var arrOpeningTimes = [openingTimes]()
                arrOpeningTimes.removeAll()
                let arrDays = ["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"]
                for day in arrDays{
                    let objTimeSloat = timeSloat.init(startTime:"10:00 AM",endTime:"07:00 PM")
                    let arr = [objTimeSloat] as! [timeSloat]
                    var objOpenings:openingTimes?
                    objOpenings = openingTimes.init(open: true, day: day, times: arr)
                    if objOpenings?.arrTimeSloats.count != 0{
                        arrOpeningTimes.append(objOpenings!)
                    }
                }
                callWebserviceForGetBusinessProfile()
    }
        
    func callWebserviceForGetBusinessProfile(){
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        
        objActivity.startActivityIndicator()
        
        objServiceManager.requestGetForJson(strURL: WebURL.getBusinessProfile, params: [:], success: { response in
            if  response["status"] as! String == "success"{
                print(response)
                let rs = response["artistRecord"] as! NSArray
                let dic = rs[0] as! [String:Any]
                self.saveData(dic)
            }else{
                let msg = response["message"] as? String ?? ""
                
                objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
            }
        }) { error in
            
            // objAppShareData.showDeafultAlert(withTitle: message.error.title, message: message.error.wrong, on: self)
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
        
        
        
    }
    
    func callWebServicesForAddStaff(dict:[String:Any]){
        
        objWebserviceManager.StartIndicator()
        
        objWebserviceManager.requestPostForJson(strURL: WebURL.addStaff, params: dict , success: { response in
            objWebserviceManager.StopIndicator()
            objActivity.stopActivity()
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
                
            }else {
                
                let strSucessStatus = response["status"] as? String
                if strSucessStatus == k_success{
                    objAppShareData.objModelHoldAddStaff.arrParseData.removeAll()
                    objAppShareData.objBookSerSelectionVC.arrSelectedServicess.removeAll()
                    objAppShareData.objAddStaffDoneSubSubServices.arrDoneTimeSloartJson = ""
                    objAppShareData.objModelEditTimeSloat.arrFinalTimes.removeAll()
                    
                    objAppShareData.showAlert(withMessage: response["message"] as? String ?? "", type: alertType.bannerDark,on: self)
                 
                    var dictUse = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfoDic) as? [String:Any] ?? ["":""]
                    dictUse[UserDefaults.myDetail.businessType] = "business"
                    
//                    UserDefaults.standard.set(dictUse, forKey: UserDefaults.keys.userInfoDic)
//                    UserDefaults.standard.set(dictUse, forKey: UserDefaults.keys.businessInfoDic)
                    
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                     self.navigationController?.popViewController(animated: true)
                    }
                  }else{
                    if strSucessStatus == "fail"{
                        
                    }else{
                        let msg = response["message"] as? String ?? ""
                        objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                    }
                }
            }
        }) { (error) in
            objWebserviceManager.StartIndicator()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    func saveData(_ dicResponse:[String:Any]){
        print(dicResponse)
        let arr = dicResponse["businessHour"] as! [[String:Any]]
        let bankStatus = dicResponse["bankStatus"] as! Bool
        UserDefaults.standard.set(bankStatus, forKey: UserDefaults.keys.bankStatus)
        UserDefaults.standard.synchronize()
        if arr.count>0{
            self.removeTimeObjectsArray()
            for dic in arr{
                if let d = dic["day"] as? Int{
                    for (index,openings) in self.arrOpeningTimes.enumerated(){
                        if d == index{
                            openings.isOpen = true
                            openings.strDay = (day(rawValue:index)?.string)!
                            let objTimeSloat = timeSloat.init(startTime:dic["startTime"] as! String,endTime:dic["endTime"] as! String)
                            openings.arrTimeSloats.append(objTimeSloat!)
                        }
                    }
                }
            }
            
            if self.arrOpeningTimes.count > 0{
                for dic in self.arrOpeningTimes{
                    let selectIndex = self.arrOpeningTimes.index(of: dic)
                    if dic.arrTimeSloats.count == 0{
                        self.arrOpeningTimes.remove(at: selectIndex!)
                    }
                }
            }
            objAppShareData.objModelEditTimeSloat.arrValidTimes = self.arrOpeningTimes
            if objAppShareData.objAddStaffDoneSubSubServices.arrDoneTimeSloartJson == ""{
                objAppShareData.addStaffTimeSloat = self.jsonFromArray(arrBusinessHours: self.arrOpeningTimes)
            }else{
                objAppShareData.addStaffTimeSloat = objAppShareData.objAddStaffDoneSubSubServices.arrDoneTimeSloartJson
            }
        }
    }
 
    func jsonFromArray(arrBusinessHours:[openingTimes]) ->String{
        var arr = [[String:Any]]()
        var dataString = ""
        for (index,openings) in arrBusinessHours.enumerated(){
            var dic = [:] as [String:Any]
            for time in openings.arrTimeSloats{
                let das = getDayFromSelectedDate(strDate: openings.strDay)
                dic = ["day":das,
                       "startTime":time.strStartTime,
                       "endTime":time.strEndTime]
                //"status":Int(truncating:NSNumber.init(value: openings.isOpen))
                if openings.arrTimeSloats.count > 1{
                    arr.append(dic)
                }
            }
            if openings.arrTimeSloats.count == 1{
                arr.append(dic)
            }
        }
        
        if let objectData = try? JSONSerialization.data(withJSONObject: arr, options: JSONSerialization.WritingOptions(rawValue: 0)) {
            let objectString = String(data: objectData,encoding: .utf8)
            dataString = objectString!
            return dataString
        }
        return dataString
    }
    
    func removeTimeObjectsArray(){
        for obj in self.arrOpeningTimes{
            obj.arrTimeSloats.removeAll()
            obj.isOpen = false
        }
    }
    
    func getDayFromSelectedDate(strDate:String)-> Int{
        var Day = 0
         if strDate == "Sunday"{
            Day = 6//"Sun"
        }else if strDate == "Monday"{
            Day = 0//Mon"
        }else if strDate == "Tuesday"{
            Day = 1//Tue"
        }else if strDate == "Wednesday"{
            Day = 2//"Wed"
        }else if strDate == "Thursday"{
            Day = 3//"Thu"
        }else if strDate == "Friday"{
            Day = 4//"Fri"
        }else if strDate == "Saturday"{
            Day = 5//"Sat"
        }else{
            Day = 0
        }
        return Day
    }
        func alert(str:String){
            objAppShareData.showAlert(withMessage: str, type: alertType.bannerDark,on: self)
         }
    
    
    
    func addAccesorryToKeyBoard(){
        let keyboardDoneButtonView = UIToolbar()
        keyboardDoneButtonView.sizeToFit()
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style:.plain, target: self, action: #selector(self.resignKeyBoard))
        doneButton.tintColor = appColor
        
        keyboardDoneButtonView.items = [flexBarButton, doneButton]
        lblHollydayAllocation.inputAccessoryView = keyboardDoneButtonView
        txtMessage.inputAccessoryView = keyboardDoneButtonView
        txtSalary.inputAccessoryView = keyboardDoneButtonView
    }
    @objc func resignKeyBoard(){
        lblHollydayAllocation.endEditing(true)
        txtMessage.endEditing(true)
        txtSalary.endEditing(true)
        self.view.endEditing(true)
    }
}

//MARK: - textfield Extension
extension AddStaffFormVC{
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        var numberOfChars:Int = 0
        let newText = (txtMessage.text as NSString).replacingCharacters(in: range, with: text)
        numberOfChars = newText.count
        
        if numberOfChars >= 200 && text != ""{
            return false
        }else{
            
        }
        
        return numberOfChars < 200
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        
        let text = textField.text! as NSString
        if (text.length == 1)  && (string == "") {
        }else{
            var substring: String = textField.text!
            substring = (substring as NSString).replacingCharacters(in: range, with: string)
            substring = substring.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            let a = substring.count
             if textField == self.txtSalary  {
                if a > 7 && string == ""{
                    return true
                }else if a > 7 && substring != ""{
                    return false
                }else{
                    let isNumber = CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: string))
                    let withDecimal = (
                        string == NumberFormatter().decimalSeparator &&
                            textField.text?.contains(string) == false
                    )
                    let maxLength = 7
                    let currentString: NSString = textField.text! as NSString
                    let newString: NSString =
                        currentString.replacingCharacters(in: range, with: string) as NSString
                    return newString.length <= maxLength && isNumber || withDecimal
                }
            }else{
                return true
            }
            return true
        }
        return true
    }
}

//MARK: - table view method extension
extension AddStaffFormVC{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
 
        if forJobTitle == true {
            return arrJobTitle.count
        }else{
            return self.arrJobType.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "CellBottumTableList"
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? CellBottumTableList{
            if forJobTitle == true{
                let obj = self.arrJobTitle[indexPath.row]
                cell.lblTitle.text = obj
            }else{
                let obj = self.arrJobType[indexPath.row]
                cell.lblTitle.text = obj
            }
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.viewBottumTable.isHidden = true
        if forJobTitle == true{
            self.lblJobTitle.text = arrJobTitle[indexPath.row]
        }else{
            let item = arrJobType[indexPath.row]
            if objAppShareData.arrAddedStaffServices.count > 0{
                self.alertForWarning(setText: item)
            }else{
                self.lblJobType.text = item
            }
        }
    }
    
    @IBAction func btnHiddenBottumTable(_ sender: UIButton) {
        self.viewBottumTable.isHidden = true
        
    }
}

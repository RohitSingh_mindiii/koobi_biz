//
//  AddStaffServiceVC.swift
//  MualabBusiness
//
//  Created by Mac on 12/12/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import DropDown


var arrModelFinelSubCategory = [ModelFinelSubCategory]()
class AddStaffServiceVC: UIViewController {
 
        var viewType = ""
         @IBOutlet weak var lblServiceName:UILabel!
 
        var businessId = ""
        var fromCategory = false
        
        let dropDown = DropDown()
    
        ///core data variable up
    
        override func viewDidLoad() {
            super.viewDidLoad()
        }
        
        override func viewWillAppear(_ animated: Bool) {
 
 
            let businessType = UserDefaults.standard.string(forKey: UserDefaults.keys.mainServiceType)
            var strType = ""
            if businessType == "1"{
                strType = "Incall"
            }else if businessType == "2"{
                strType = "Outcall"
            }else{
                strType = "Both"
            }
            if strType == "Incall"{
            }else if strType == "Outcall"{
            }else{
                strType = "Both"
            }
            
            
            let obj = objAppShareData.objModelFinalSubCategory
            self.lblServiceName.text = obj.param[obj.serviceName] ?? ""
          }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
        
    
        @IBAction func btnContinews(_ sender: UIButton) {
            self.view.endEditing(true)
            self.addArrayData()
        }

    
        @IBAction func btnBack(_ sender: UIButton) {
            objAppShareData.manageNavigation = false
            self.removeDataFromField()
        }
    }



    
    //MARK: - Button Extension
    fileprivate extension AddStaffServiceVC{
        @IBAction func btnBookingType(_ sender:Any){
            self.gotoNextVC(strType: "Booking")
        }
        
        @IBAction func btnTime(_ sender:Any){
            self.gotoNextVC(strType: "Time")
        }
        @IBAction func btnPrice(_ sender:Any){
            self.gotoNextVC(strType: "Price")
        }
        
        
        func gotoNextVC(strType:String){
            
                let sb = UIStoryboard(name:"StaffList",bundle:Bundle.main)
                let objChooseType = sb.instantiateViewController(withIdentifier:"AddEditStaffServiceVC") as! AddEditStaffServiceVC
                objChooseType.viewType = strType
                self.navigationController?.pushViewController(objChooseType, animated: true)
         }
        
        func addArrayData(){

            let obj = objAppShareData.objModelFinalSubCategory
            let abc = obj.param[obj.bookingType] ?? ""
            if abc == "Incall"{
                obj.param[obj.outCallPrice] =   "0.00"
            }else if abc == "Outcall"{
                obj.param[obj.incallPrice] = "0.00"
            }
            
            
            let businessName = obj.param[obj.bookingName] ?? ""
            let catName = obj.param[obj.categoryName] ?? ""
            let servName = obj.param[obj.serviceName] ?? ""
            let busId = obj.param[obj.strBusinessId] ?? ""
            let catId = obj.param[obj.strCategoryId] ?? ""
            let bukType = obj.param[obj.bookingType] ?? ""
            var icPrice = obj.param[obj.incallPrice] ?? "0.00"
            var ocPrice = obj.param[obj.outCallPrice] ?? "0.00"
            let desc = obj.param[obj.descriptions] ?? ""
            let time = obj.param[obj.completionTime] ?? ""
            let lastServiceId = obj.param[obj.strSubSubServiceId] ?? ""

            var mainPrice = icPrice
            if bukType == "Outcall"{
                mainPrice = ocPrice
            }
            
            let icPriceDouble = Double(icPrice) ?? 0.0
            let ocPriceDouble = Double(ocPrice) ?? 0.0
            
//            if businessName == ""{//
//                self.alert(str: "Please select business type")
//            }else if catName == "" || catName == "No category available"{
//                self.alert(str: "Please select category")
//            }else
                if servName == ""{
                self.alert(str: "Please enter service name")
            }else if bukType == ""{
                self.alert(str: "Please select booking type")
            }else if bukType == "Both" && ((icPrice == "0.00" || ocPrice == "0.00") ||  (icPrice == "0.0" || ocPrice == "0.0")){
                self.alert(str: "Please enter price")
            }else if bukType == "Both" && (icPrice == "" || ocPrice == ""){
                self.alert(str: "Please enter price")
            }else if bukType == "Both" && (icPrice == ""  || ocPrice == ""  || ocPrice == "0.00" || icPrice == "0.0"  || ocPrice == "0.0"){
                self.alert(str: "Please enter price")
            }else if bukType == "Both" && icPrice == "0.00"  && ocPrice != ""{
                self.alert(str: "Please enter price")
                    
            }else if bukType == "Both" && (icPriceDouble < 5.0 || ocPriceDouble < 5.0){
                self.alert(str: "Please enter a amount that is no lesser than £5")
            
            }else if bukType == "Incall" && (icPrice == "0.00" || icPrice == "" || icPrice == "0.0" ){
                self.alert(str: "Please enter incall price")
            }else if bukType == "Incall" && (icPriceDouble < 5.0){
                self.alert(str: "Please enter a amount that is no lesser than £5")
            
            }else if bukType == "Outcall" && ( ocPrice == "0.00" || ocPrice == "" || ocPrice == "0.0"){
                self.alert(str: "Please enter outcall price")
            }else if bukType == "Outcall" && (ocPriceDouble < 5.0){
                self.alert(str: "Please enter a amount that is no lesser than £5")
            
            }else if desc == ""{
                self.alert(str: "Please enter description")
            }else if time == ""{
                self.alert(str: "Please select completion time")
            }else{
                
                if icPrice != ""{
                    let aPrice  = float_t(icPrice) ?? 0.00
                    icPrice = String(aPrice)
                }
                if ocPrice != ""{
                    let aPrice  = float_t(ocPrice) ?? 0.00
                    ocPrice = String(aPrice)
                }
                
                let a = ["_id":lastServiceId,
                          "mainBookingId":busId,
                          "mainCategoryId":catId,
                          "serviceName":servName,
                          "price":mainPrice,
                          "incallPrice":icPrice,
                          "outcallPrice":ocPrice,
                          "bookingType":bukType,
                          "describe":desc,
                          "time":time]
                var added = false
                let objModelServicesList = ModelServicesList(dict: a)
                for obj in objAppShareData.arrAddedStaffServices{
                   if obj.id == lastServiceId{
                    added = true
                        obj.price = mainPrice
                        obj.incallPrice = icPrice
                        obj.outcallPrice = ocPrice
                        obj.bookingType = bukType
                        obj.time = time
                     }
                }
                if added == false{
                objAppShareData.arrAddedStaffServices.append(objModelServicesList)
                }
                    ///////////////core data method work up
                backVC()
            }
            
        }
        func backVC(){
            objAppShareData.manageNavigation = false
            self.alert(str:"Service Added Successfully.")
            self.navigationController?.popViewController(animated: true)
        }
        
        
        func alert(str:String){
            objAppShareData.showAlert(withMessage: str, type: alertType.bannerDark,on: self)
        }
    }



    //MARK: - gate data in globle array from local data base
    extension AddStaffServiceVC{
        
 
        
        func removeDataFromField(){
             self.navigationController?.popViewController(animated: true)
        }
        func GotoServiceVC(){
            objAppShareData.objModelFinalSubCategory.param = ["":""]
            let sb = UIStoryboard(name:"SignUpProcess",bundle:Bundle.main)
            objAppShareData.manageNavigation = false
            
            let objChooseType = sb.instantiateViewController(withIdentifier:"ServicesListVC") as! ServicesListVC
            self.navigationController?.pushViewController(objChooseType, animated: true)
        }
}



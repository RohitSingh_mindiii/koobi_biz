//
//  ServiceListAddStaffVC.swift
//  MualabBusiness
//
//  Created by Mac on 11/12/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import DropDown
import CoreData
class ServiceListAddStaffVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
        var objArtistList = ArtistList(dict: [:])
    var bookingType = "All Types"

        var edition = false
        @IBOutlet weak var tblBusinessLIst:UITableView!
        @IBOutlet weak var lblNoDataFound:UILabel!
        var managedContext:NSManagedObjectContext?
        var arrBusinessTypeList = [ModelBusinessTypeList]()
        var navigate = true
    var hitAPI = false
        override func viewDidLoad() {
            super.viewDidLoad()
            
            guard let appDelegate =
                UIApplication.shared.delegate as? AppDelegate else {
                    return
            }
            managedContext =
                appDelegate.persistentContainer.viewContext
            
            self.tblBusinessLIst.delegate = self
            self.tblBusinessLIst.dataSource = self
            self.tblBusinessLIst.reloadData()
            
            NotificationCenter.default.addObserver(self, selector: #selector(self.reloadDataForDeleteCell), name: NSNotification.Name(rawValue: "Delete"), object: nil)
            
            NotificationCenter.default.addObserver(self, selector: #selector(self.reloadDataForEditCell), name: NSNotification.Name(rawValue: "Edit"), object: nil)
            
            NotificationCenter.default.addObserver(self, selector: #selector(self.reloadDataForDetailCell), name: NSNotification.Name(rawValue: "Detail"), object: nil)
        }
    override func viewWillAppear(_ animated: Bool) {
        navigate = true
        let businessType = UserDefaults.standard.string(forKey: UserDefaults.keys.mainServiceType)
        //self.fetchParcelObject()
        var strType = ""
        if businessType == "1"{
            strType = "Incall"
        }else if businessType == "2"{
            strType = "Outcall"
        }else{
            strType = "Both"
            //self.viewBookingType.isHidden = true
        }
        objAppShareData.objModelFinalSubCategory.param[objAppShareData.objModelFinalSubCategory.bookingType] = strType
        self.callWebserviceForUpdateData()
        //self.fetchParcelObject()
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
extension ServiceListAddStaffVC{
        @objc func reloadDataForDeleteCell(notification: Notification){
            edition = true
             self.deleteCell(objModel: objAppShareData.objModelServicesList)
        }
        @objc func reloadDataForDetailCell(notification: Notification){

             if objAppShareData.manageNavigation == false{
                objAppShareData.manageNavigation = true
                if navigate == false{
                    return
                }
                let o = objAppShareData.objModelServicesList
                let a = objAppShareData.objModelFinalSubCategory
               // a.param = [:]
                a.param[a.serviceName] = o.serviceName
                a.param[a.strBusinessId] = o.mainBookingId
                a.param[a.strCategoryId] = o.mainCategoryId
      
                if bookingType == "Incall"{
                    a.param[a.bookingType] = "Incall"//o.bookingType
                    a.param[a.incallPrice] = o.price
                    a.param[a.outCallPrice] = "0.00"//o.outcallPrice
                    a.param[a.bookingTypeOld] = o.bookingTypeOld
                } else if bookingType == "Outcall"{
                    a.param[a.bookingType] = "Outcall"//o.bookingType
                    a.param[a.incallPrice] = "0.00" //o.price
                    a.param[a.outCallPrice] = o.outcallPrice
                    a.param[a.bookingTypeOld] = o.bookingTypeOld
                }else if bookingType == "All Types"{
                    a.param[a.bookingType] = o.bookingType
                    a.param[a.incallPrice] = o.price
                    a.param[a.outCallPrice] = o.outcallPrice
                    a.param[a.bookingTypeOld] = o.bookingTypeOld
                }else{
                    a.param[a.bookingType] = o.bookingType
                    a.param[a.incallPrice] = o.price
                    a.param[a.outCallPrice] = o.outcallPrice
                    a.param[a.bookingTypeOld] = o.bookingTypeOld
                }
                
//                a.param[a.bookingType] = o.bookingType
//                a.param[a.incallPrice] = o.price
//                a.param[a.outCallPrice] = o.outcallPrice
//                a.param[a.bookingTypeOld] = o.bookingTypeOld

                a.param[a.descriptions] = o.describe
                a.param[a.completionTime] = o.time
                a.param[a.strSubSubServiceId] = o.id
                let sb = UIStoryboard(name:"StaffList",bundle:Bundle.main)
                let objChooseType = sb.instantiateViewController(withIdentifier:"AddStaffServiceVC") as! AddStaffServiceVC
               // objChooseType.objModelClass = objAppShareData.objModelServicesList
                objChooseType.viewType = "NO"
                if navigate == true{
                    navigate = false
                    
                    if self.bookingType == o.bookingTypeOld ||  self.bookingType == "All Types" || o.bookingTypeOld == "Both"{
                        edition = true
                        self.navigationController?.pushViewController(objChooseType, animated: true)
                    }else{
                        navigate = true
                        objAppShareData.showAlert(withMessage: "Please add only"+bookingType+" service", type: alertType.bannerDark,on: self)
                        objAppShareData.manageNavigation = false
                    }
                }
            }
        }
        @objc func reloadDataForEditCell(notification: Notification){
            edition = true
            self.EditDetailCell(objModel: objAppShareData.objModelServicesList, strViewType: "NO")
        }
    
    }
    
    //MARK:- extension for tableview delegate
    extension ServiceListAddStaffVC {
        func numberOfSections(in tableView: UITableView) -> Int
        {
            if self.arrBusinessTypeList.count > 0{
                self.lblNoDataFound.isHidden = true
            }else{
                self.lblNoDataFound.isHidden = false
            }
            return self.arrBusinessTypeList.count
        }
        
        //    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int){
        //        view.tintColor = UIColor.white
        //        let header = view as! UITableViewHeaderFooterView
        //        header.textLabel?.textColor = UIColor.black
        //
        //    }
 
        func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
            let headerView = UIView()
            headerView.backgroundColor = UIColor.white
            
            let headerLabel = UILabel(frame: CGRect(x: 20, y: 2, width:
                tableView.bounds.size.width, height: tableView.bounds.size.height))
            headerLabel.font = UIFont(name: "Nunito-Medium", size: 17)
            headerLabel.textColor = UIColor.black
            headerLabel.text = self.tableView(self.tblBusinessLIst, titleForHeaderInSection: section)
            headerLabel.sizeToFit()
            headerView.addSubview(headerLabel)
            
            return headerView
        }
        func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
            return 30
        }
        func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
        {
            return self.arrBusinessTypeList[section].businessName.capitalized
         }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            let arrDevelopers = self.arrBusinessTypeList[section].arrBusinessList
            return arrDevelopers.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
            if let cell = tableView.dequeueReusableCell(withIdentifier: "CellAddStaffServies", for: indexPath) as? CellAddStaffServies{
                let obj  = arrBusinessTypeList[indexPath.section].arrBusinessList[indexPath.row]
                cell.lblCategoryName.text = obj.categoryName
                cell.arrService = obj.arrServices
                print("height = ",CGFloat(30+(obj.arrServices.count*45)))
                cell.setNeedsLayout()
                
            cell.arrBusinessCategory.append(arrBusinessTypeList[indexPath.section].businessName)
                cell.arrBusinessCategory.append(obj.categoryName)
                cell.updateCell = true
                cell.tblServiceList.reloadData()
                return cell
            }else{
                return UITableViewCell()
            }
        }
    }
    
    
    //MARK:- extension for tableview delegate
    extension ServiceListAddStaffVC {
        
        @IBAction func btnContinew(_ sender:Any){
            self.hitAPI = true
            navigate = true
            if objAppShareData.arrAddedStaffServices.count == 0{
                objAppShareData.showAlert(withMessage: "Please add at least one service", type: alertType.bannerDark,on: self)
            }else{
            self.SendDataOnServer()
            }
        }
        
        @IBAction func btnBack(_ sender:Any){
            navigate = true
            if  edition == true{
                 let alertController = UIAlertController(title: "Alert", message: "Are you sure want to discard changes?", preferredStyle: .alert)
                let OKAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default){
                    UIAlertAction in
                    objAppShareData.arrAddedStaffServices = objAppShareData.arrAddedStaffServicesOriginal
                    objAppShareData.objModelFinalSubCategory.param = [:]
                      self.navigationController?.popViewController(animated: true)
                }
                let CancelAction = UIAlertAction(title: "No", style: UIAlertAction.Style.default){
                    UIAlertAction in
                }
                alertController.addAction(OKAction)
                alertController.addAction(CancelAction)
                self.present(alertController, animated: true, completion: nil)
            }else{
                self.navigationController?.popViewController(animated: true)
        }
    }
  }
    
    extension ServiceListAddStaffVC{
        
        func EditDetailCell(objModel:ModelServicesList,strViewType:String){
            if objAppShareData.manageNavigation == false{
                objAppShareData.manageNavigation = true
 
                let o = objModel
                let a = objAppShareData.objModelFinalSubCategory
               
                a.param[a.serviceName] = o.serviceName
                a.param[a.strBusinessId] = o.mainBookingId
                a.param[a.strCategoryId] = o.mainCategoryId
                a.param[a.bookingType] = o.bookingType
                a.param[a.incallPrice] = o.price
                a.param[a.outCallPrice] = o.outcallPrice
                a.param[a.descriptions] = o.describe
                a.param[a.completionTime] = o.time
                a.param[a.strSubSubServiceId] = o.id
                a.param[a.bookingTypeOld] = o.bookingTypeOld

                
                let sb = UIStoryboard(name:"StaffList",bundle:Bundle.main)
                let objChooseType = sb.instantiateViewController(withIdentifier:"AddStaffServiceVC") as! AddStaffServiceVC
               // objChooseType.objModelClass = objModel
                objChooseType.viewType = strViewType
                //objChooseType.serviceName = objModel.serviceName
                if navigate == true{
                    navigate = false
                    self.navigationController?.pushViewController(objChooseType, animated: true)
                }
            }
        }
        
        func deleteCell(objModel:ModelServicesList){
            var index = -1
            var delete = ""
            for objMainArray in objAppShareData.arrAddedStaffServices{
               index = index+1
                if (objMainArray.id == objModel.id){
                    delete = String(index)
                }
            }
            
            if delete != ""{
                objAppShareData.arrAddedStaffServices.remove(at: Int(delete)!)
                //self.tblBusinessLIst.reloadData()
                self.callWebserviceForUpdateData()
            }
         }
        
        func SendDataOnServer(){
            self.sendArray()
        }
        func sendArray(){
            var serviceJosnArray = [[String: Any]]()
            var serviceJosn = [String: Any]()
            for obj in objAppShareData.arrAddedStaffServices{
                let objSub = obj
                var a = ""
                a = objSub.mainBookingId
                var priceIC = objSub.incallPrice
                if objSub.incallPrice != ""{
                    let b = float_t(objSub.incallPrice) ?? 0.00
                    priceIC = String(b)
                }else{
                    priceIC = "0.00"
                }
                var priceOC = objSub.outcallPrice
                if objSub.outcallPrice != ""{
                    let b = float_t(objSub.outcallPrice) ?? 0.00
                    priceOC = String(b)
                }else{
                    priceOC = "0.00"
                }
                
var mainServiceName = objAppShareData.objModelFinalSubCategory.param[objAppShareData.objModelFinalSubCategory.bookingName] ?? ""
var mainCategoryName = objAppShareData.objModelFinalSubCategory.param[objAppShareData.objModelFinalSubCategory.categoryName] ?? ""
                for objArrBusin in self.arrBusinessTypeList{
                    if objArrBusin.businessId == a{
                      mainServiceName = objArrBusin.businessName
                    }
                    
                }
                for objArrBusin in self.arrBusinessTypeList{
                    for bName in objArrBusin.arrBusinessList{
                        if bName.categoryId == objSub.mainCategoryId{
                            mainCategoryName = bName.categoryName
                            print("catId , CatName = ",objSub.mainCategoryId,objSub.mainCategoryId)
                            break
                        }
                    }
                }
                serviceJosn["serviceId"] = a
                serviceJosn["artistServiceId"] = objSub.id
                serviceJosn["subserviceId"] = objSub.mainCategoryId
                serviceJosn["title"] = objSub.serviceName
                serviceJosn["description"] = objSub.describe
                serviceJosn["inCallPrice"] = priceIC
                serviceJosn["outCallPrice"] = priceOC
                serviceJosn["completionTime"] = objSub.time
                serviceJosn["serviceName"] = mainServiceName
                serviceJosn["subServiceName"] = mainCategoryName
                serviceJosnArray.append(serviceJosn)
            }
            
            var objectString = ""
            if let objectData = try? JSONSerialization.data(withJSONObject: serviceJosnArray, options: JSONSerialization.WritingOptions(rawValue: 0)) {
                objectString = String(data: objectData, encoding: .utf8)!
            }
            var staffType = ""
            var editId = ""
            if objAppShareData.fromEditStaff == true {
                staffType = "edit"
                editId = objAppShareData.objModelHoldAddStaff._id
            } else {
                staffType = ""
                editId = ""
            }
            var businessId = ""
            if let dictUser = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]{
                print("dictUser = ",dictUser)
                businessId = dictUser[UserDefaults.myDetail.id] as? String ?? ""
                if let id = dictUser[UserDefaults.keys.userId] as? String{
                    businessId = id
                }else if let id  = dictUser[UserDefaults.keys.userId] as? Int{
                    businessId = String(id)
                }
            }
            let dict = ["artistService":objectString]
            print(dict)
            objAppShareData.objAddStaffDoneSubSubServices.arrDoneServiceJson = objectString

                  let param = ["type":staffType,
                               "artistId":String(objArtistList._id),
                               "businessId":businessId,
                              "staffService":objAppShareData.objAddStaffDoneSubSubServices.arrDoneServiceJson] as [String : Any]
            CallAPIAddService(param: param)
            //for back vc
            objAppShareData.objAddStaffDoneSubSubServices.arrDoneServiceJson = objectString
             objAppShareData.objModelFinalSubCategory.param = [:]
          }
        
  }
    
    //MARK: - call api for get service list from backecd server and set on local server
    
    extension ServiceListAddStaffVC{
         func callWebserviceForUpdateData(){
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            self.arrBusinessTypeList.removeAll()
            self.tblBusinessLIst.reloadData()
            
            objActivity.startActivityIndicator()
            arrMain.removeAll()
            var businessId = ""
            if let dict = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]{
                
                if let id = dict[UserDefaults.keys.userId] as? String{
                    businessId = id
                }else if let id  = dict[UserDefaults.keys.userId] as? Int{
                    businessId = String(id)
                }
                
             }
             objServiceManager.requestPostForJson(strURL: WebURL.artistService, params: ["artistId":businessId], success: { response in
                print(response)
                
                let status = response["status"] as? String ?? ""
                if status == "success"{
                    
                    
                    if let arrArtistServices = response["artistServices"] as? [[String:Any]]{
                        for dict1 in arrArtistServices {
                            var MainServiceId = ""
                            if let id = dict1["serviceId"] as? Int { MainServiceId = String(id)
                            }else if let id = dict1["serviceId"] as? String{ MainServiceId = id
                            }
                            let businessName = dict1["serviceName"] as? String ?? ""
                            var arrServices = [[String:Any]]()
                            var arrayCate = [[String:Any]]()
                            
                            if let arrSer = dict1["subServies"] as? [[String:Any]]{
                                for dict11 in arrSer{
                                    var arrayServices = [[String:Any]]()
                                    
                                    var catId = dict11["subServiceId"] as? Int ?? 0

                                    if let arrArtistServices = dict11["artistservices"] as? [[String:Any]]{
                                        for dict111 in arrArtistServices{
                                            
                                            
                                            
                                            var outCallPrice = "0.0"
                                            if let id = dict111["outCallPrice"] as? String {
                                                outCallPrice = String(id)
                                                print("Value = Int = ",outCallPrice)
                                            }else if let id = dict111["outCallPrice"] as? double_t{
                                                outCallPrice = String(id)
                                                print("Value = String = ",outCallPrice)
                                            }else if let id = dict111["outCallPrice"] as? float_t{
                                                outCallPrice = String(id)
                                                print("Value = float_t = ",outCallPrice)
                                            }else if let id = dict111["outCallPrice"] as? Int{
                                                outCallPrice = String(id)
                                                print("Value = double_t = ",outCallPrice)
                                            }else if let id = dict111["outCallPrice"] as? Double{
                                                outCallPrice = String(id)
                                                print("Value = Double = ",outCallPrice)
                                                
                                            }
                                            
                                            
                                            
                                            
                                            var inCallPrice = "0.0"
                                            if let id = dict111["inCallPrice"] as? String{
                                                inCallPrice = String(id)
                                                print("Value = String = ",inCallPrice)
                                            }else if let id = dict111["inCallPrice"] as? double_t {
                                                inCallPrice = String(id)
                                                print("double_t = Int = ",inCallPrice)
                                            }else if let id = dict111["inCallPrice"] as? float_t{
                                                inCallPrice = String(id)
                                                print("Value = float_t = ",inCallPrice)
                                            }else if let id = dict111["inCallPrice"] as? Int{
                                                inCallPrice = String(id)
                                                print("Value = double_t = ",inCallPrice)
                                            }else if let id = dict111["inCallPrice"] as? Double{
                                                inCallPrice = String(id)
                                                print("Value = Double = ",inCallPrice)
                                                
                                            }
                                            
                                            var idService = ""
                                            if let id = dict111["_id"] as? Int { idService = String(id)
                                            }else if let id = dict111["_id"] as? String{ idService = id
                                            }
                                            let bukTyp = dict111["bookingType"] as? String ?? "0.00"
                                         
                                            var price = inCallPrice
                                            if bukTyp == "Outcall"{
                                                price = outCallPrice
                                            }
                                            
                                            if self.bookingType == "Incall"{
                                                price = inCallPrice
                                            }else if self.bookingType == "Outcall"{
                                                price = outCallPrice
                                            }
                                            
                                            
                                            var dictA = ["price":price,
                                                         "incallPrice":inCallPrice,
                                                         "outcallPrice":outCallPrice,
                                                         "_id":idService,
                                                         "mainBookingId":MainServiceId,
                                                         "mainCategoryId":String(catId),
                                                         "serviceName":dict111["title"] as? String ?? "",
                                                         "bookingType":dict111["bookingType"] as? String ?? "",
                                                         "bookingTypeOld":dict111["bookingType"] as? String ?? "",
                                                         "describe":dict111["description"] as? String ?? "",
                                                         "time":dict111["completionTime"] as? String ?? ""] as [String : Any]

                                            let lastServiceName = dict111["title"] as? String ?? ""
                                            for objMainArray in objAppShareData.arrAddedStaffServices{
                                                
                                                objMainArray.bookingTypeOld = dict111["bookingType"] as? String ?? ""

                                if objMainArray.id == idService {
                                     dictA["price"] = objMainArray.price
                                    dictA["incallPrice"] = objMainArray.incallPrice
                                    dictA["outcallPrice"] = objMainArray.outcallPrice
                                    dictA["time"] = objMainArray.time
                                    dictA["bookingType"] = objMainArray.bookingType
                                                }
                                            }
                                if self.bookingType == "All Types" || self.bookingType == (dict111["bookingType"] as? String ?? "")  || (dict111["bookingType"] as? String ?? "") == "Both" ||  "All Types" == (dict111["bookingType"] as? String ?? "") {
                                            arrayServices.append(dictA)
                                            arrServices.append(dictA)
                                            }
                                        }
                                    }
                                    let catName = dict11["subServiceName"] as? String ?? ""
                                    let c = dict1["serviceName"] ?? ""
                                    let dict1 = ["categoryId":String(catId),
                                                 "categoryName":catName,
                                                 "businessName":businessName,
                                                 "arrServices":arrayServices] as [String : Any]
                                    if arrayServices.count > 0{
                                        arrayCate.append(dict1)
                                    }
                                }
                            }
                            let dict2 = ["businessName":businessName,
                                         "businessId":MainServiceId,
                                         "arrServices":arrServices,
                                         "arrCategory":arrayCate] as [String : Any]
                            
                            let objModelBusinessTypeList = ModelBusinessTypeList(dict: dict2)
                            if objModelBusinessTypeList.arrBusinessList.count > 0{
                                self.arrBusinessTypeList.append(objModelBusinessTypeList)
                            }
                        }
                        if arrMain.count > 0{
                            self.tblBusinessLIst.reloadData()
                            //self.parseExpensesList()
                        }
                    }
                    self.tblBusinessLIst.reloadData()
                }else{
                    self.tblBusinessLIst.reloadData()
                }
            }) { error in
                objActivity.stopActivity()
                self.tblBusinessLIst.reloadData()
                
            }
        }
    }

/////////////////

//MARK: - call api
extension ServiceListAddStaffVC{
    func CallAPIAddService(param:[String:Any]){
     
        objServiceManager.StartIndicator()
        objWebserviceManager.requestPostForJson(strURL: WebURL.addStaffService, params: param , success: { response in
            
            objServiceManager.StopIndicator()
            let strSucessStatus = response["status"] as? String
            if strSucessStatus == k_success{
               // self.saveData(dict:response)
                objAppShareData.editStaff = false
                   self.navigationController?.popViewController(animated: true)
                objAppShareData.arrAddedStaffServicesOriginal = objAppShareData.arrAddedStaffServices
            }else{
                objServiceManager.StopIndicator()
                if strSucessStatus == "fail"{
                    
                }else{
                    let msg = response["message"] as? String ?? ""
                    objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                }
            }
        }) { (error) in
            if self.hitAPI == true{
                self.hitAPI = false
                self.sendArray()
            }else{
                objServiceManager.StopIndicator()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
            }
        }
    }
}

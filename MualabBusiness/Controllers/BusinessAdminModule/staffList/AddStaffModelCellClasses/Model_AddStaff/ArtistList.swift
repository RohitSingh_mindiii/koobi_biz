 

import Foundation
 
public class ArtistList {
	public var userName : String = ""
	public var profileImage : String = ""
	public var _id : Int = 0

	init(dict: [String : Any]) {
 		userName = dict ["userName"] as? String ?? ""
		profileImage = dict ["profileImage"] as? String ?? ""
		_id = dict ["_id"] as? Int ?? 0
	}
 }
 
class UserListModel {
     var staffName : String?
     var staffImage : String?
     var _id : Int?
     var job : String?
     var staffId : Int?
    var status : String = ""
    var alreadyGivenHoliday : Int = 0
    var totalHoliday : Int = 0

       
    
    init(dict: [String : Any]) {
        staffName = dict ["staffName"] as? String ?? ""
        
        if let sta = dict ["status"] as? String{
            self.status = sta
        }else if let sta = dict ["status"] as? Int{
            self.status = String(sta)
        }
        if let sta = dict ["staffId"] as? String{
            if sta != ""{
                staffId = Int(sta)
            }
        }else if let sta = dict ["staffId"] as? Int{
            self.staffId = sta
        }
        staffImage = dict ["staffImage"] as? String ?? ""
        job = dict ["job"] as? String ?? ""        
        _id = dict ["_id"] as? Int

        if let day = dict["holiday"] as? String{
            if day != ""{
                totalHoliday = Int(day )!
            }
        }else if let day = dict["holiday"] as? Int{
            totalHoliday = day
        }
        
        
        if let arrHolidayRemaining = dict["holidayGiven"] as? [[String:Any]]{
            for objHolidayRemaining in arrHolidayRemaining{
                if let day = objHolidayRemaining["holidayGiven"] as? String{
                    if day != ""{
                        alreadyGivenHoliday = alreadyGivenHoliday+Int(day )!
                    }
                }else if let day = objHolidayRemaining["holidayGiven"] as? Int{
                        alreadyGivenHoliday = alreadyGivenHoliday+day
                 }
            }
        }
        
        
    }
 }

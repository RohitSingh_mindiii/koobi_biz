//
//  ModelAddStaffInfo.swift
//  MualabBusiness
//
//  Created by mac on 13/04/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import Foundation

class AddStaffInfo {
    
    var _Id : Int = 0
    var businessId : Int = 0
    var holiday:String = ""
    var job:String = ""
    var salary:String = ""
    var message:String = ""
    var mediaAccess:String = ""
    var alreadyGivenHoliday : Int = 0
    var totalHoliday : Int = 0
    var staffHours : [AddStaffHours] = [AddStaffHours]()
    var staffService : [AddStaffStaffService] = [AddStaffStaffService]()
    var bookingType = ""
    
    init(dict: [String : Any]){
        
        _Id = dict["_id"] as? Int ?? 0
        bookingType = String(dict["bookingType"] as? Int ?? 0)
        businessId = dict["businessId"] as? Int ?? 0
        salary = String(dict["salaries"] as? Int ?? 0)
        holiday = dict["holiday"] as? String ?? ""
        job = dict["job"] as? String ?? ""
        mediaAccess = dict["mediaAccess"] as? String ?? ""
        message = dict["message"] as? String ?? ""
        
        if let bookingTypes = dict["bookingType"] as? String {
            bookingType = bookingTypes
        }
        
        if let salarys = dict["salaries"] as? String {
            salary = salarys
        }else if let salarys = dict["salaries"] as? Double {
            salary = String(salarys)
        }else if let salarys = dict["salaries"] as? Float {
            salary =  String(salarys)
        }
        
        if let arr = dict["staffHours"] as? [[String : Any]]{
            for dict in arr{
                let objSubService = AddStaffHours.init(dict: dict)
                self.staffHours.append(objSubService)
            }
        }
        
        
        if let arr = dict["staffService"] as? [[String : Any]]{
            for dict in arr{
                let objSubService = AddStaffStaffService.init(dict: dict)
                self.staffService.append(objSubService)
            }
        }
        
        
        if let day = dict["holiday"] as? String{
            if day != ""{
                totalHoliday = Int(day )!
            }
        }else if let day = dict["holiday"] as? Int{
            totalHoliday = day
        }
        
        
        if let arrHolidayRemaining = dict["holidayGiven"] as? [[String:Any]]{
            for objHolidayRemaining in arrHolidayRemaining{
                if let day = objHolidayRemaining["holidayGiven"] as? String{
                    if day != ""{
                        alreadyGivenHoliday = alreadyGivenHoliday+Int(day )!
                    }
                }else if let day = objHolidayRemaining["holidayGiven"] as? Int{
                    alreadyGivenHoliday = alreadyGivenHoliday+day
                }
            }
        }
        
    }
}


class AddStaffHours {
    var day : Int = 0
    var endTime:String = ""
    var startTime:String = ""
    init(dict: [String : Any]){
        day = dict["day"] as? Int ?? 0
        if let days = dict["day"] as? String{
            day = Int(days)!  }
        if let days = dict["day"] as? Int{
            day = days }
        endTime = dict["endTime"] as? String ?? ""
        startTime = dict["startTime"] as? String ?? ""
    }
}



class AddStaffStaffService:NSObject {
    
    var _Id:String = ""
    var artistId:String = ""
    var artistServiceId:String = ""
    var businessId:String = ""
    var completionTime : String = ""
    var inCallPrice : String = ""
    var outCallPrice : String = ""
    var serviceId:String = ""
    var subServiceId : String = ""
 

    var __v = "0"
//    var _id  = 0
//    var artistId = 0
//    var artistServiceId = 0
//    var businessId = 0
//    var completionTime = "0"
    var crd = "0"
    var deleteStatus = "0"
//    var inCallPrice = "0"
//    var outCallPrice = "0"
//    var serviceId = 0
    var staffId = "0"
    var status = "0"
//   var subserviceId = 0
    var title = ""
    var upd = "0"
    var serviceType = ""

    init(dict: [String : Any]){

        if let _Id1 = dict["_id"] as? Int{
            _Id = String(_Id1)
        }else if let _Id1 = dict["_id"] as? String{
            _Id = _Id1
        }

        if let subserviceId1 = dict["subserviceId"] as? Int{
            subServiceId = String(subserviceId1)
        }else if let subserviceId1 = dict["subserviceId"] as? String{
            subServiceId = subserviceId1
        }
        
        if let subserviceId1 = dict["artistServiceId"] as? Int{
            artistServiceId = String(subserviceId1)
        }else if let subserviceId1 = dict["artistServiceId"] as? String{
            artistServiceId = subserviceId1
        }
        
        
        if let artistId1 = dict["artistId"] as? Int{
            artistId = String(artistId1)
        }else if let artistId1 = dict["artistId"] as? String{
            artistId = artistId1
        }
        
        if let artistServiceId1 = dict["artistServiceId"] as? Int{
            artistServiceId = String(artistServiceId1)
        }else if let artistServiceId1 = dict["artistServiceId"] as? String{
            artistServiceId = artistServiceId1
        }
        

        
        if let businessId1 = dict["businessId"] as? Int{
            businessId = String(businessId1)
        }else if let businessId1 = dict["businessId"] as? String{
            businessId = businessId1
        }

        
        if let inCallPrice1 = dict["inCallPrice"] as? Int{
            inCallPrice = String(inCallPrice1)
        }else if let inCallPrice1 = dict["inCallPrice"] as? String{
            inCallPrice = inCallPrice1
        }
        
        
        if let outCallPrice1 = dict["outCallPrice"] as? Int{
            outCallPrice = String(outCallPrice1)
        }else if let outCallPrice1 = dict["outCallPrice"] as? String{
            outCallPrice = outCallPrice1
        }

        if let completionTime1 = dict["completionTime"] as? Int{
            completionTime = String(completionTime1)
        }else if let completionTime1 = dict["completionTime"] as? String{
            completionTime = completionTime1
        }

        if let serviceId1 = dict["serviceId"] as? Int{
            serviceId = String(serviceId1)
        }else if let serviceId1 = dict["serviceId"] as? String{
            serviceId = serviceId1
        }
        
        if let subserviceId1 = dict["subserviceId"] as? Int{
            subServiceId = String(subserviceId1)
        }else if let subserviceId1 = dict["subserviceId"] as? String{
            subServiceId = subserviceId1
        }
        
        if let __v1 = dict["__v"] as? Int{
            __v = String(__v1)
        }else if let __v1 = dict["__v"] as? String{
            __v = __v1
        }

        if let completionTime1 = dict["completionTime"] as? Int{
            completionTime = String(completionTime1)
        }else if let completionTime1 = dict["completionTime"] as? String{
            completionTime = completionTime1
        }
        
        if let inCallPrice1 = dict["inCallPrice"] as? Int{
            inCallPrice = String(inCallPrice1)
        }else if let inCallPrice1 = dict["inCallPrice"] as? String{
            inCallPrice = inCallPrice1
        }
        
        outCallPrice = dict["outCallPrice"] as? String ?? ""
        
        if let outCallPrice1 = dict["outCallPrice"] as? Int{
            outCallPrice = String(outCallPrice1)
        }else if let outCallPrice1 = dict["outCallPrice"] as? String{
            outCallPrice = outCallPrice1
        }

        if let staffId1 = dict["staffId"] as? Int{
            staffId = String(staffId1)
        }else if let staffId1 = dict["staffId"] as? String{
            staffId = staffId1
        }

        if let status1 = dict["status"] as? Int{
            status = String(status1)
        }else if let status1 = dict["status"] as? String{
            status = status1
        }

        if let title1 = dict["title"] as? Int{
            title = String(title1)
        }else if let title1 = dict["title"] as? String{
            title = title1
        }
        
        if let title1 = dict["bookingType"] as? Int{
            serviceType = String(title1)
        }else if let title1 = dict["bookingType"] as? String{
            serviceType = title1
        }
    }
}

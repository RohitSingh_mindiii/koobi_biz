//
//  SubService.swift
//  MualabCustomer
//
//  Created by Mac on 27/02/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import Foundation

class AddStaffSubServices:NSObject {
 
    var serviceId : Int = 0
    var Id : Int = 0
    var serviceName : String = ""
    var subServiceId : Int = 0
    var subServiceName : String = ""
    
    var arrSubSubService : [AddStaffSubSubServices] = [AddStaffSubSubServices]()
   
    init(dict : [String : Any]){
        
        Id = dict["_id"] as? Int ?? 0
        serviceId = dict["serviceId"] as? Int ?? 0
        subServiceId = dict["subServiceId"] as? Int ?? 0
        subServiceName = dict["subServiceName"] as? String ?? ""
        
        if let arr = dict["artistservices"] as? [[String : Any]]{
            for dict in arr{
                let obj = AddStaffSubSubServices.init(dict: dict)
                self.arrSubSubService.append(obj)
            }
        }
                
    }
}


//
//  Service.swift
//  Mualab
//
//  Created by Amit on 10/27/17.
//  Copyright © 2017 MINDIII. All rights reserved.
//

import Foundation

class AddStaffService:NSObject{
    
	var serviceId : Int = 0
    var serviceName : String = ""
    var arrSubServices : [AddStaffSubServices] = [AddStaffSubServices]()
	
    init(dict: [String : Any]){
        
        if let serviceId = dict["serviceId"] as? Int{
            self.serviceId = serviceId
        }
        if let serviceName = dict["serviceName"] as? String{
            self.serviceName = serviceName
        }
        
        if let arr = dict["subServies"] as? [[String : Any]]{
            for dict in arr{
                let objSubService = AddStaffSubServices.init(dict: dict)
                self.arrSubServices.append(objSubService)
            }
        }
    }
}
 

//
//  SubSubService.swift
//  MualabCustomer
//
//  Created by Mac on 27/02/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import Foundation

class AddStaffSubSubServices:NSObject {
    
    var subSubServiceId : Int = 0
    var inCallPrice : Double = 0
    var outCallPrice : Double = 0
    var subSubServiceName : String = ""
    var completionTime : String = ""
    
    init(dict : [String : Any]){
        subSubServiceId = dict["_id"] as? Int ?? 0
        subSubServiceName = dict["title"] as? String ?? ""
        inCallPrice = dict["inCallPrice"] as? Double ?? 0
        outCallPrice = dict["outCallPrice"] as? Double ?? 0
        completionTime = dict["completionTime"] as? String ?? ""
    }
}


class AddStaffDoneSubSubServices {
   var arrDoneSubService = [AddStaffDoneSubSubServicesData]()
    var arrDoneTimeSloartJson = ""
    var arrDoneServiceJson = ""

}

class AddStaffDoneSubSubServicesData:NSObject {
    var _Id:String = ""
    var artistId:String = ""
    var artistServiceId:String = ""
    var businessId:String = ""
    var completionTime : String = ""
    var inCallPrice : String = ""
    var outCallPrice : String = ""
    var serviceId:String = ""
    var subServiceId : String = ""

    
    init(dict : [String : Any]){
        _Id = dict["_id"] as? String ?? ""
        subServiceId = dict["subserviceId"] as? String ?? ""
        artistId = dict["artistId"] as? String ?? ""
        artistServiceId = dict["artistServiceId"] as? String ?? ""
        businessId = dict["businessId"] as? String ?? ""
        inCallPrice = dict["inCallPrice"] as? String ?? ""
        outCallPrice = dict["outCallPrice"] as? String ?? ""
        completionTime = dict["completionTime"] as? String ?? ""
        serviceId = dict["serviceId"] as? String ?? ""
        subServiceId = dict["subserviceId"] as? String ?? ""
    }
}

class AddStaffTimeSloat:NSObject {
    var day:String = ""
    var startTime:String = ""
    var endTime:String = ""

    var arrAddStaffTimeSloat = [AddStaffTimeSloat]()
}


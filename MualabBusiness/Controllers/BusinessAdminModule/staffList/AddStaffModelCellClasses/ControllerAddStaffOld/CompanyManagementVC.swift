//
//  CompanyManagementVC.swift
//  MualabBusiness
//
//  Created by mac on 14/05/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

class CompanyManagementVC: UIViewController,UITableViewDataSource,UITableViewDelegate{

    @IBOutlet weak var lblNodataFound: UILabel!
    @IBOutlet weak var tblCompanyList: UITableView!
    
    //variable declaration
    private var artistId = ""
    private var arrCompanyList  = [CompanyInfo]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblCompanyList.delegate = self
        self.tblCompanyList.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.APIMethod()
    }
 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
//MARK: - custome method extension
fileprivate extension CompanyManagementVC{
    func APIMethod(){
        let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfo) as! Data
        let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! User
        self.artistId = String(describing: userInfo.userId)
        let dict = ["artistId":self.artistId]
        self.callWebserviceForCompanyList(dict: dict)
    }
}

//MARK: - custome method extension
fileprivate extension CompanyManagementVC{
    @IBAction func btnBackAction(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK: - Tableview delegate method extension
extension CompanyManagementVC{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.arrCompanyList.count > 0{
            self.lblNodataFound.isHidden = true
        }else{
            self.lblNodataFound.isHidden = false
        }
        return arrCompanyList.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cellIdentifier = "CellCompanyList"
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? CellCompanyList{
            let objArr = self.arrCompanyList[indexPath.row]
            
            let url = URL(string: objArr.profileImage)
            if  url != nil {
                cell.imgCompany.af_setImage(withURL: url!)
            }else{
                cell.imgCompany.image = #imageLiteral(resourceName: "cellBackground")
            }
            cell.lblCompanyName.text = objArr.businessName
            
            return cell
        }else{
            return UITableViewCell()
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //let objArr = self.arrCompanyList[indexPath.row]

    }
}
//MARK:- Webservice Call
fileprivate extension CompanyManagementVC {
    func callWebserviceForCompanyList(dict: [String : Any]){
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        objActivity.startActivityIndicator()
        objServiceManager.requestPost(strURL: WebURL.companyInfo, params: dict  , success: { response in
            
            objServiceManager.StopIndicator()
            
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else{
                
                let strStatus =  response["status"] as? String ?? ""
                if strStatus == k_success{
                    self.parseResponce(response:response)
                }else{
                    let msg = response["message"] as! String
                    objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                }
            }
        }){ error in
            self.tblCompanyList.reloadData()
            objServiceManager.StopIndicator()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
    func parseResponce(response:[String : Any]){
        self.arrCompanyList.removeAll()
        if let arr = response["businessList"] as? [[String:Any]]{
            if arr.count > 0{
                for dictArtistData in arr {
                    let objArtistList = CompanyInfo.init(dict: dictArtistData)
                    self.arrCompanyList.append(objArtistList)
                }
            }
            self.tblCompanyList.reloadData()
        }
    }
}

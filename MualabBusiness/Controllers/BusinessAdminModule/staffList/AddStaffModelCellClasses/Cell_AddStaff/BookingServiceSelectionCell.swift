//
//  BookingServiceSelectionCell.swift
//  MualabCustomer
//
//  Created by Mac on 08/02/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

class BookingServiceSelectionCell: UITableViewCell {
    
    @IBOutlet weak var viewCellBG: UIView!
    @IBOutlet weak var viewSelectedCellBG: UIView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblInCallPrice: UILabel!
    @IBOutlet weak var lblCompletionTime: UILabel!
    @IBOutlet weak var lblOutCallPrice: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}


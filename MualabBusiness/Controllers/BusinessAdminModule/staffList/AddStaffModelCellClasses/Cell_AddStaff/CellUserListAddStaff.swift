//
//  CellUserListAddStaff.swift
//  MualabBusiness
//
//  Created by mac on 06/04/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

class CellUserListAddStaff: UITableViewCell {
    
    @IBOutlet weak var viewRequest: UIView!
    @IBOutlet weak var imgUserProfile: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}

class CellCertificateList: UITableViewCell {
    
    @IBOutlet weak var imgCertificate: UIImageView!
    @IBOutlet weak var imgCertificateStatus: UIImageView!

    @IBOutlet weak var lblCertiName: UILabel!
    @IBOutlet weak var lblCertiDes: UILabel!
    
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnDelete: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}


class CellVoucherList: UITableViewCell {
 
    @IBOutlet weak var lblVouchewCode: UILabel!
    @IBOutlet weak var lblDiscount: UILabel!
    @IBOutlet weak var lblExpiryDate: UILabel!
    @IBOutlet weak var btnShear: UIButton!
 
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}

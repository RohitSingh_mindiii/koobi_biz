//  InvitationListVC.swift
//  MualabBusiness
//  Created by Mac on 20/12/18.
//  Copyright © 2018 Mindiii. All rights reserved.

import UIKit
import Firebase

class InvitationListVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tblInvitationList:UITableView!
    @IBOutlet weak var lblNoDataFound:UILabel!
    var ref: DatabaseReference!

    private var arrCompanyList  = [CompanyInfo]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblNoDataFound.isHidden = true
        self.tblInvitationList.delegate = self
        self.tblInvitationList.dataSource = self
        self.manageInvitationCount()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func manageInvitationCount(){
        if let dict = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]{
            var artId = ""
            if let artistId = dict[UserDefaults.keys.userId] as? String{
                artId = artistId
            }else if let artistId = dict[UserDefaults.keys.userId] as? Int{
                artId = String(artistId)
            }
        self.ref = Database.database().reference()
         self.ref.child("socialBookingBadgeCount").child(artId).updateChildValues(["businessInvitationCount":0])
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        if let dict = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]{
          var artId = ""
            if let artistId = dict[UserDefaults.keys.userId] as? String{
                artId = artistId
           }else if let artistId = dict[UserDefaults.keys.userId] as? Int{
                artId = String(artistId)
            }
            print("dict = ",dict)
            let dict = ["artistId":artId]
            self.callWebserviceForCompanyList(dict: dict)
         }
    }
}

//MARK:-  button method
extension InvitationListVC {
    @IBAction func btnBack(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK:- tableview delegate datasource method
extension InvitationListVC {
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if arrCompanyList.count == 0{
            self.lblNoDataFound.isHidden = false
        }else{
            self.lblNoDataFound.isHidden = true
        }
        return arrCompanyList.count
    }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
          if let cell  = tableView.dequeueReusableCell(withIdentifier: "CellInvitationList")! as? CellInvitationList {
          var objArtistDetails = arrCompanyList[indexPath.row]
                cell.imgVwProfile.image = UIImage(named: "cellBackground")
           // cell.lblName.text  = objArtistDetails.userName
            cell.lblName.text  = objArtistDetails.businessName
                cell.setDataInCollectionData(obj: objArtistDetails.arrBusinessType)
                if objArtistDetails.profileImage != "" {
                    if let url = URL(string: objArtistDetails.profileImage){
                        //cell.imgVwProfile.af_setImage(withURL: url, placeholderImage:#imageLiteral(resourceName: "cellBackground"))
                        cell.imgVwProfile.sd_setImage(with: url, placeholderImage: UIImage(named: "cellBackground"))
                    }
                }
            return cell
        }else{
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var objArtistDetails = arrCompanyList[indexPath.row]
        let sb = UIStoryboard(name:"Invitation",bundle:Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"InvitationDetailVC") as? InvitationDetailVC{
            objVC.objCompanyList = objArtistDetails
            self.navigationController?.pushViewController(objVC, animated: true)
        }
        // self.gotoProfileVC()
    }
}


//MARK:- Webservice Call
extension InvitationListVC {
    
    func callWebserviceForCompanyList(dict: [String : Any]){
    self.arrCompanyList.removeAll()
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }

        objActivity.startActivityIndicator()
        objServiceManager.requestPost(strURL: WebURL.companyInfo, params: dict  , success: { response in
            objServiceManager.StopIndicator()
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else{
                
                let strStatus =  response["status"] as? String ?? ""
                if strStatus == k_success{
                    self.parseResponce(response:response)
                    self.tblInvitationList.reloadData()

                }else{
                    self.tblInvitationList.reloadData()
                    let msg = response["message"] as! String
                    objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                }
            }
        }){ error in
            self.tblInvitationList.reloadData()
            objServiceManager.StopIndicator()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
    func parseResponce(response:[String : Any]){
        self.arrCompanyList.removeAll()
        
        if let arr = response["businessList"] as? [[String:Any]]{
            if arr.count > 0{
                for dictArtistData in arr {
                    let objArtistList = CompanyInfo.init(dict: dictArtistData)
                    var statusObj = "0"
                    if let status = dictArtistData["status"] as? String{
                        statusObj = status
                    }else if let status = dictArtistData["status"] as? Int{
                        statusObj = String(status)
                    }
                    if statusObj == "0"{
                        self.arrCompanyList.append(objArtistList)
                    }
                }
            }
        }
        self.tblInvitationList.reloadData()
    }
}

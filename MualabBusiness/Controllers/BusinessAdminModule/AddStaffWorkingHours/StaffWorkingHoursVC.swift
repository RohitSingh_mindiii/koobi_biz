//
//  StaffWorkingHoursVC.swift
//  MualabBusiness
//
//  Created by Mac on 17/04/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

fileprivate enum day:Int {
    case Monday
    case Tuesday
    case Wednesday
    case Thursday
    case Friday
    case Saturday
    case Sunday
    
    var string: String {
        return String(describing: self)
    }
}

class StaffWorkingHoursVC: UIViewController {
    
    @IBOutlet weak var startTimePicker : UIDatePicker!
    @IBOutlet weak var endTimePicker : UIDatePicker!
    
    @IBOutlet weak var timePickerBottem : NSLayoutConstraint!
    
    @IBOutlet weak var tblBusinessHours : UITableView!
    
    @IBOutlet weak var lblTimes : UILabel!
    @IBOutlet weak var lblDay : UILabel!
    
    @IBOutlet weak var btnNext : UIButton!
    
    
    fileprivate var arrOpeningTimes = [openingTimes]()
   
    fileprivate var fromNextButton = false

    
    fileprivate var tblTag = Int()
    fileprivate var timeTag = Int()
}

//MARK: - View hirarchy
extension StaffWorkingHoursVC{
    override func viewDidLoad() {
        super.viewDidLoad()
        if objAppShareData.editStaffTimeSloat == true{
             dataParsingMethod()
        }else{
        makeDeafultsDay()
            callWebserviceForGetBusinessProfile()
        }
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //makeDeafultsDay()
        customTimePickers()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.configureView()
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
}
//MARK: -Local Methods
fileprivate extension StaffWorkingHoursVC{
    func configureView() {
        self.timePickerBottem.constant = -self.view.frame.size.height/1.5
        self.btnNext.layer.cornerRadius = self.btnNext.frame.size.height/2
    }
    
    func customTimePickers()  {
        startTimePicker.date = self.setDateFrom("10:00 AM")
        endTimePicker.minimumDate = startTimePicker.date.addingTimeInterval(1800.0)
        endTimePicker.date = self.setDateFrom("07:00 PM")
        startTimePicker.addTarget(self, action: #selector(self.dateChanged(_:)), for: .valueChanged)
        endTimePicker.addTarget(self, action: #selector(self.dateChanged(_:)), for: .valueChanged)
    }
    
    //UIDatePicker methods
    @objc func dateChanged(_ sender: UIDatePicker) {
        endTimePicker.minimumDate = startTimePicker.date.addingTimeInterval(1800.0)
        self.lblTimes.text = "From: \(self.timeFormat(forAPI: self.startTimePicker.date)) To: \(self.timeFormat(forAPI: self.endTimePicker.date))"
    }
    
    func makeDeafultsDay() {
        self.arrOpeningTimes.removeAll()

        //objAppShareData.objModelEditTimeSloat.arrFinalTimes1.removeAll()
        let arrDays = ["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"]
        for day in arrDays{
            let objTimeSloat = timeSloat.init(startTime:"10:00 AM",endTime:"07:00 PM")
            let arr = [objTimeSloat] as! [timeSloat]
            var objOpenings:openingTimes?
            objOpenings = openingTimes.init(open: true, day: day, times: arr)
            if objOpenings?.arrTimeSloats.count != 0{
            self.arrOpeningTimes.append(objOpenings!)
            //objAppShareData.objModelEditTimeSloat.arrFinalTimes1.append(objOpenings!)
            }
         }
        
        self.tblBusinessHours.reloadData()
    }
    
    func validateTimeSloat(startTime:Date,endTime:Date) -> Bool {
        
        let sTimePicker = self.timeFormat(forAPI: self.startTimePicker.date)
        let eTimePicker = self.timeFormat(forAPI: self.endTimePicker.date)
        
        let sTimeForCheck = self.setDateFrom(sTimePicker)
        let eTimeForCheck = self.setDateFrom(eTimePicker)
        
        if sTimeForCheck > startTime && eTimeForCheck < endTime{
            return false
        }else if sTimeForCheck >= startTime && sTimeForCheck <= endTime{
            return false
        }else if eTimeForCheck >= startTime && eTimeForCheck <= endTime {
            return false
        }else if sTimeForCheck < startTime && eTimeForCheck > endTime{
            return false
        }
        return true
        
    }
    //MARK: - validate time for final submission
    
    func validateTimeSloatFinal(startTime:Date,endTime:Date,checkStartTime:Date,checkEndTime:Date) -> Bool {
        
        let sTimePicker = self.timeFormat(forAPI: checkStartTime)
        let eTimePicker = self.timeFormat(forAPI:checkEndTime)
        
        let sTimeForCheck = self.setDateFrom(sTimePicker)
        let eTimeForCheck = self.setDateFrom(eTimePicker)
        
        if sTimeForCheck > startTime && eTimeForCheck < endTime{
            return false
        }else if sTimeForCheck >= startTime && sTimeForCheck <= endTime{
            return false
        }else if eTimeForCheck >= startTime && eTimeForCheck <= endTime {
            return false
        }else if sTimeForCheck < startTime && eTimeForCheck > endTime{
            return false
        }
        return true
    }
    
    //MARK: - Open Time Picker
    
    func openTimePicker(tableTag:Int,timeArrayIndex:Int) {
        self.tblTag = tableTag
        self.timeTag = timeArrayIndex
       
        var startTime = ""
        var endTime = ""
      // let objArr = self.arrOpeningTimes[self.tblTag]
        let objArrLocal = objAppShareData.objModelEditTimeSloat.arrValidTimes[self.tblTag]
        

        if self.timeTag == 0{
            startTime = objArrLocal.arrTimeSloats [0].strStartTime
            endTime = objArrLocal.arrTimeSloats [0].strEndTime
        }else{
            if objArrLocal.arrTimeSloats.count > 1{
                startTime = objArrLocal.arrTimeSloats [1].strStartTime
                endTime = objArrLocal.arrTimeSloats [1].strEndTime
            }else{
            startTime = objArrLocal.arrTimeSloats [0].strStartTime
            endTime = objArrLocal.arrTimeSloats [0].strEndTime
            }
        }
        
        self.timePickerBottem.constant = 0;
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
        
        let objOpenings = objAppShareData.objModelEditTimeSloat.arrValidTimes[self.tblTag]
        self.lblDay.text = objOpenings.strDay
       // let times = objOpenings.arrTimeSloats [self.timeTag]
      
        self.lblTimes.text = "From: \(startTime) To: \(endTime)"
        
                self.startTimePicker.date = self.setDateFrom(startTime)
                self.endTimePicker.date = self.setDateFrom(endTime)
        
                self.startTimePicker.minimumDate = self.setDateFrom(startTime)
                self.endTimePicker.minimumDate = self.setDateFrom(endTime)
        
                self.startTimePicker.maximumDate = self.setDateFrom(endTime)
                self.endTimePicker.maximumDate = self.setDateFrom(endTime)
    }
    
    //MARK: - Remove time sloat
    func deleteTimeSloat(tableTag:Int,timeArrayIndex:Int) {
        self.btnCancelTimeSelection(0)
        let objOpening = self.arrOpeningTimes[tableTag]
        objOpening.arrTimeSloats.remove(at: timeArrayIndex)
        self.tblBusinessHours.reloadData()
    }
    
    func gotoCallOptionVC(animated:Bool){
        let sb = UIStoryboard(name:"SignUpProcess",bundle:Bundle.main)
        let objChooseType = sb.instantiateViewController(withIdentifier:"OutcallOptionVC") as! OutcallOptionVC
        self.navigationController?.pushViewController(objChooseType, animated:animated)
    }
    //MARK: - Make String for API
    func jsonFromArray(arrBusinessHours:[openingTimes]) ->String{
        var arr = [[String:Any]]()
        var dataString = ""
        for (/*index*/_,openings) in arrBusinessHours.enumerated(){
            for time in openings.arrTimeSloats{
                let das = getDayNoFromSelectedDay(strDate: openings.strDay)
                let dic = ["day":das,
                           "startTime":time.strStartTime,
                           "endTime":time.strEndTime] as [String : Any]
                //"status":Int(truncating:NSNumber.init(value: openings.isOpen))
                arr.append(dic)
            }
        }
        
        if let objectData = try? JSONSerialization.data(withJSONObject: arr, options: JSONSerialization.WritingOptions(rawValue: 0)) {
            let objectString = String(data: objectData,encoding: .utf8)
            dataString = objectString!
            return dataString
        }
        return dataString
    }
    
 
    func removeTimeObjectsArray(){
        for obj in self.arrOpeningTimes{
            obj.arrTimeSloats.removeAll()
            obj.isOpen = false
        }
    }
}
//MARK : - Save Methods
fileprivate extension StaffWorkingHoursVC{
    
    func saveData(_ dicResponse:[String:Any]){
        print(dicResponse)
        let arr = dicResponse["businessHour"] as! [[String:Any]]
        let bankStatus = dicResponse["bankStatus"] as! Bool
        UserDefaults.standard.set(bankStatus, forKey: UserDefaults.keys.bankStatus)
        UserDefaults.standard.synchronize()
        if arr.count>0{
            self.removeTimeObjectsArray()
            for dic in arr{
                if let d = dic["day"] as? Int{
                    for (index,openings) in self.arrOpeningTimes.enumerated(){
                        if d == index{
                            openings.isOpen = true
                            openings.strDay = (day(rawValue:index)?.string)!
                            let objTimeSloat = timeSloat.init(startTime:dic["startTime"] as! String,endTime:dic["endTime"] as! String)
                            openings.arrTimeSloats.append(objTimeSloat!)
                        }
                    }
                }
            }
            if self.arrOpeningTimes.count > 0{
                for dic in self.arrOpeningTimes{
                    let selectIndex = self.arrOpeningTimes.index(of: dic)
                    if dic.arrTimeSloats.count == 0{
                        self.arrOpeningTimes.remove(at: selectIndex!)
                    }
                }
            }
            self.tblBusinessHours.reloadData()
        }
    }
}

//MARK : - IBAction
fileprivate extension StaffWorkingHoursVC{
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        fromNextButton = false
        btnNextMethod()
        
        if objAppShareData.objAddStaffDoneSubSubServices.arrDoneTimeSloartJson == objAppShareData.addStaffTimeSloat {
            self.navigationController?.popViewController(animated: true)
        }else {
          
            let alertController = UIAlertController(title: "Alert", message: "Are you sure want to discard changes?", preferredStyle: .alert)
            let OKAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default){
                UIAlertAction in
                objAppShareData.objAddStaffDoneSubSubServices.arrDoneTimeSloartJson = ""
                objAppShareData.objModelEditTimeSloat.arrFinalTimes.removeAll()
                objAppShareData.editStaffTimeSloat = false

              self.navigationController?.popViewController(animated: true)
            }
            let CancelAction = UIAlertAction(title: "No", style: UIAlertAction.Style.default){
                UIAlertAction in
            }
            alertController.addAction(OKAction)
            alertController.addAction(CancelAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    @IBAction func btnDone( _ sender:Any){
        let objOpenings = self.arrOpeningTimes[self.tblTag]
        let objTimeSloat = objOpenings.arrTimeSloats[self.timeTag]
        
  
        if self.timeTag == 1{
        for (index, times) in (objOpenings.arrTimeSloats.enumerated()){
            if index != self.timeTag{
                if !validateTimeSloat(startTime: self.setDateFrom(times.strStartTime), endTime: self.setDateFrom(times.strEndTime)){
                    print("Invalid Date")
                    objAppShareData.showAlert(withMessage: "Time slot in the same day cannot intersect.", type: alertType.bannerDark, on: self)
                    return
                }
            }
            }}

        objTimeSloat.strStartTime = self.timeFormat(forAPI: self.startTimePicker.date)
        objTimeSloat.strEndTime = self.timeFormat(forAPI: self.endTimePicker.date)

        //Update table
        let section = 0
        let row = self.tblTag
        let indexPath = IndexPath(row: row, section: section)
        let cell = tblBusinessHours.cellForRow(at: indexPath) as! BusinessHoursTableCell
        cell.tblTime?.reloadData()
        
        self.btnCancelTimeSelection(0)
    }
    @IBAction func btnCancelTimeSelection(_ sender:Any){
        self.timePickerBottem.constant = -self.view.frame.size.height/1.5;
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func btnIsSelecteDay(_ sender:UIButton){
        let section = 0
        let row = sender.tag
        let indexPath = IndexPath(row: row, section: section)
        let cell = tblBusinessHours.cellForRow(at:indexPath) as! BusinessHoursTableCell
        
        let objOpening = self.arrOpeningTimes[row]
        
        if objOpening.isOpen{
            objOpening.isOpen = false
            cell.btnIsOpen?.isSelected = false
            objOpening.arrTimeSloats.removeAll()
        }else{
            cell.btnIsOpen?.isSelected = true
            objOpening.isOpen = true
            let objTimeSloat = timeSloat.init(startTime: "10:00 AM",endTime: "07:00 PM")
            objOpening.arrTimeSloats.append(objTimeSloat!)
        }
        self.btnCancelTimeSelection(0)
        self.tblBusinessHours.reloadData()
        //cell.tblTime?.reloadData()
    }
    
    @IBAction func btnAddTimeSloat(_ sender:UIButton){
        let row = sender.tag
        let objOpening = self.arrOpeningTimes[row]
        //let objFinalOpening = objAppShareData.objModelEditTimeSloat.arrFinalTimes1[row]
        let objValid = objAppShareData.objModelEditTimeSloat.arrValidTimes[row]

        if objOpening.arrTimeSloats.count == 1{
            var startTime = ""
            var endTime = ""
            if objValid.arrTimeSloats.count > 1 {
                let times1 = objValid.arrTimeSloats[1]
                startTime = times1.strStartTime
                endTime = times1.strEndTime
            }else{
                let times1 = objValid.arrTimeSloats[0]
                startTime = times1.strStartTime
                endTime = times1.strEndTime
            }
            let objTimeSloat = timeSloat.init(startTime:startTime,endTime:endTime)
            objOpening.arrTimeSloats.append(objTimeSloat!)
            self.tblBusinessHours.reloadData()
        }else{
            objAppShareData.showAlert(withMessage: "Max time slot reached", type: alertType.bannerDark, on: self)
        }
    }
    
    @IBAction func btnLogout(_ sender:UIButton){
        appDelegate.logout()
    }
    
    @IBAction func btnNext(_ sender:UIButton){
        self.view.endEditing(true)
        fromNextButton = true
        
             btnNextMethod()
     }
    
    func btnNextMethod(){
        var valueAvalable = false
        for objOpen in self.arrOpeningTimes{
            let objTimeSloat = objOpen.arrTimeSloats
            if objTimeSloat.count > 0{
                valueAvalable = true
            }
            if objTimeSloat.count > 1{
                let time1 = objTimeSloat[0]
                let time2 = objTimeSloat[1]
                if !validateTimeSloatFinal(startTime: self.setDateFrom(time1.strStartTime),endTime: self.setDateFrom(time1.strEndTime), checkStartTime:self.setDateFrom(time2.strStartTime),checkEndTime:self.setDateFrom(time2.strEndTime)){
                    print("Invalid Date")
                    objAppShareData.showAlert(withMessage: "Time slot in the same day cannot intersect.", type: alertType.bannerDark, on: self)
                    return
                }
            }
        }
        
        
        for objOpen in self.arrOpeningTimes{
            let objTimeSloat = objOpen.arrTimeSloats
            if objTimeSloat.count > 1{
                let time1 = objTimeSloat[0]
                let time2 = objTimeSloat[1]
                
                if time1.strEndTime == time2.strStartTime{
                    objAppShareData.showAlert(withMessage: "Time slot in the same day cannot intersect.", type: alertType.bannerDark, on: self)
                    return
                }else if time1.strStartTime == time1.strEndTime{
                    objAppShareData.showAlert(withMessage: "Time slot in the same time cannot intersect.", type: alertType.bannerDark, on: self)
                    return
                }else if time2.strStartTime == time2.strEndTime{
                    objAppShareData.showAlert(withMessage: "Time slot in the same time cannot intersect.", type: alertType.bannerDark, on: self)
                    return
                }
            }else if objTimeSloat.count == 1{
                let time1 = objTimeSloat[0]
                if time1.strStartTime == time1.strEndTime{
                    objAppShareData.showAlert(withMessage: "Time slot in the same time cannot intersect.", type: alertType.bannerDark, on: self)
                    return
                }
            }
        }
       // self.callWebserviceForAddBusinessHours()
        
        objAppShareData.objAddStaffDoneSubSubServices.arrDoneTimeSloartJson = self.jsonFromArray(arrBusinessHours: self.arrOpeningTimes)
        if self.arrOpeningTimes.count > 0{
            objAppShareData.objModelEditTimeSloat.arrStoreTimes = self.arrOpeningTimes
        }
        
        let dicParam = ["businessHour":self.jsonFromArray(arrBusinessHours:self.arrOpeningTimes)]
        print("param = ", dicParam)
        if fromNextButton == true{
            objAppShareData.editStaffTimeSloat = true
            objAppShareData.editStaff = false
            objAppShareData.addTimeSloat = false
            if valueAvalable == false{
                objAppShareData.showAlert(withMessage: "Select at least one day operation hours", type: alertType.bannerDark, on: self)
            }else{
            popToVC()
            }
        }
    }
    
    func popToVC(){
         self.navigationController?.popViewController(animated: true)
    }
}

//Webservices
fileprivate extension StaffWorkingHoursVC{
    
    func callWebserviceForAddBusinessHours() {
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        objActivity.startActivityIndicator()
        let dicParam = ["businessHour":self.jsonFromArray(arrBusinessHours: self.arrOpeningTimes)]
        
        objServiceManager.requestPost(strURL: WebURL.addBusinessHour, params: dicParam, success: { response in
            print(response)
            if response["status"] as! String == "success"{
                let dic = response["massage"] as! [[String:Any]]
                UserDefaults.standard.set(dic, forKey: UserDefaults.keys.businessHour)
                UserDefaults.standard.synchronize()
                self.gotoCallOptionVC(animated: true)
            }else{
                let msg = response["message"] as? String ?? ""
                objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
            }
        }) { error in
            print(error)
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
    func callWebserviceForGetBusinessProfile(){
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        
        objActivity.startActivityIndicator()
        objServiceManager.requestGetForJson(strURL: WebURL.getBusinessProfile, params: [:], success: { response in
            if  response["status"] as! String == "success"{
                print(response)
                let rs = response["artistRecord"] as! NSArray
                let dic = rs[0] as! [String:Any]
                self.saveData(dic)
            }else{
                let msg = response["message"] as? String ?? ""
                objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
            }
        }) { error in
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
}
//MARK: - UITableViewDelegate and UITableViewDataSource
extension StaffWorkingHoursVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOpeningTimes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblBusinessHours.dequeueReusableCell(withIdentifier: "BusinessHoursTableCell") as! BusinessHoursTableCell
        objAppShareData.imageShow = false

        let objOpenings = self.arrOpeningTimes[indexPath.row]
        let index = indexPath.row
        if index == self.arrOpeningTimes.count-1 || index == self.arrOpeningTimes.count{
            objAppShareData.imageShow = true
        }
        cell.lblDay?.text = objOpenings.strDay
        cell.btnIsOpen?.isSelected = objOpenings.isOpen
        cell.arrTime = objOpenings.arrTimeSloats
        cell.delegate = self
        if objOpenings.isOpen{
            cell.btnAdd?.isHidden = false
            cell.lblNotWorking?.isHidden = true
        }else{
            cell.btnAdd?.isHidden = true
            cell.lblNotWorking?.isHidden = false
        }
        //
        cell.tblTime?.tag = indexPath.row
        cell.btnIsOpen?.tag = indexPath.row
        cell.btnAdd?.tag = indexPath.row
        //
        cell.tblTimeHeight?.constant = CGFloat((objOpenings.arrTimeSloats.count * 26)+26)+8
        
        if objOpenings.arrTimeSloats.count == 0 || objOpenings.arrTimeSloats.count == 1{
            cell.tblTimeHeight?.constant = 35
        }
        if objOpenings.arrTimeSloats.count == 2 {
            cell.tblTimeHeight?.constant = 62
        }
        if objOpenings.arrTimeSloats.count == 0{
            cell.lblDay?.textColor = UIColor.lightGray
        }else{
            cell.lblDay?.textColor = UIColor.black
        }
        cell.tblTime?.reloadData()
        return cell
    }
}

//MARK: - BusinessHoursTableCellDelegate

extension StaffWorkingHoursVC : BusinessHoursTableCellDelegate{

    func removeTimeSloat(withSenderTable tableView: UITableView, timeArrayIndex: Int) {
        self.deleteTimeSloat(tableTag: tableView.tag, timeArrayIndex: timeArrayIndex)
    }
    
    func selectedTimeSloat(withSenderTable tableView:UITableView, timeArrayIndex:Int){
        openTimePicker(tableTag: tableView.tag, timeArrayIndex: timeArrayIndex)
    }
}


//MARK: - Date formates

fileprivate extension StaffWorkingHoursVC {
    
    //MARK: - Date time format
    func timeFormat(forAPI date: Date) -> String {
        let formatter = DateFormatter()
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        formatter.dateFormat = "hh:mm a"
        let formattedTime: String = formatter.string(from: date)
        return formattedTime.uppercased()
    }
    
    // Set deafult Times from string
    func setDateFrom(_ time: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        dateFormatter.dateFormat = "hh:mm a"
        let date = dateFormatter.date(from: time)
        return date!
    }
}

//MARK: - Date ParsingMethod
fileprivate extension StaffWorkingHoursVC {
    func dataParsingMethod(){
        self.arrOpeningTimes.removeAll()
        if objAppShareData.addTimeSloat == true {
        if objAppShareData.objModelEditTimeSloat.arrFinalTimes.count > 0{
            let arrFinam = objAppShareData.objModelEditTimeSloat.arrFinalTimes
            
            for newObj in arrFinam{
                let isOpen = newObj.isOpen
                let strDay = newObj.strDay
                let arrTimeSloats = newObj.arrTimeSloats

                let obj = openingTimes(open: isOpen, day: strDay, times: arrTimeSloats)
                self.arrOpeningTimes.append(obj!)
        }
    }
          
            
            if objAppShareData.objModelEditTimeSloat.arrValidTimes.count > 0{
                for objValid in objAppShareData.objModelEditTimeSloat.arrValidTimes{
                    var index = 0
                    let selectIndex = objAppShareData.objModelEditTimeSloat.arrValidTimes.index(of: objValid)
                    index = selectIndex!
                   let dayValid = objValid.strDay
                    var addIndex = false
                    for objLocal in self.arrOpeningTimes{
                        if objLocal.strDay == dayValid{
                            addIndex = true
                        }
                    }
                    if addIndex == false{
                        let obj = openingTimes(open: false, day: dayValid, times: [])
                        self.arrOpeningTimes.insert(obj!, at: index)
                    }
                }
            }
            

        }else if objAppShareData.objModelEditTimeSloat.arrStoreTimes.count > 0{
            let arrFinam = objAppShareData.objModelEditTimeSloat.arrStoreTimes
            
            for newObj in arrFinam{
                let isOpen = newObj.isOpen
                let strDay = newObj.strDay
                let arrTimeSloats = newObj.arrTimeSloats
                
                let obj = openingTimes(open: isOpen, day: strDay, times: arrTimeSloats)
                self.arrOpeningTimes.append(obj!)
            }
        }
}
    
    func getDayNoFromSelectedDay(strDate:String)-> Int{
        var Day = 0
        
        if strDate == "Sunday"{
            Day = 6//"Sun"
        }else if strDate == "Monday"{
            Day = 0//Mon"
        }else if strDate == "Tuesday"{
            Day = 1//Tue"
        }else if strDate == "Wednesday"{
            Day = 2//"Wed"
        }else if strDate == "Thursday"{
            Day = 3//"Thu"
        }else if strDate == "Friday"{
            Day = 4//"Fri"
        }else if strDate == "Saturday"{
            Day = 5//"Sat"
        }else{
            Day = 0
        }
        return Day
    }
}


//
//  CertificateLIstVC.swift
//  MualabBusiness
//
//  Created by Mac on 26/12/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

class CertificateLIstVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPickerViewDelegate,UITextFieldDelegate,UITextViewDelegate {
   

        
        fileprivate var fromCamera = false
        fileprivate var artistId = ""
        var artistType = ""
        var addCertificate = true
        var newArtistId = ""
    
    fileprivate var certificateImage:UIImage = #imageLiteral(resourceName: "gallery_placeholder")
    fileprivate var certificateID = ""

        @IBOutlet weak var viewImage: UIView!
        @IBOutlet weak var imgCertificate: UIImageView!
    @IBOutlet weak var btnAddCertificate: UIButton!
    @IBOutlet weak var lblHeaderAddQualification: UILabel!

        @IBOutlet weak var tblCertificate: UITableView!
        @IBOutlet weak var lblNoDataFound: UIView!
        @IBOutlet weak var txtTitle: UITextField!
        @IBOutlet weak var txtDescrib: UITextView!

        var imagePicker = UIImagePickerController()
        fileprivate var arrCertificate = [CertificateModel]()
        
        
        override func viewDidLoad() {
            super.viewDidLoad()
            DelegateCalling()
            self.addAccesorryToKeyBoard()
            self.preferredStatusBarStyle
        }
        
        override var preferredStatusBarStyle: UIStatusBarStyle {
            return .default
        }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
        
        override func viewWillAppear(_ animated: Bool) {
            
            let decoded1 = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] ?? ["":""]
            let userId = decoded1[UserDefaults.keys.userId] as? String ?? ""
            self.artistId = userId
            self.artistType = decoded1["userType"] as? String ?? ""
            
            if fromCamera == true{
                fromCamera = false
            }else{
                APICertificatelist()
            }
        }
        
        
        func APICertificatelist(){
            let decoded1 = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] ?? ["":""]
            let userId = decoded1[UserDefaults.keys.userId] as? String ?? ""
            self.artistId = userId
            self.artistType = decoded1["userType"] as? String ?? ""
            let type = decoded1["userType"] as? String ?? ""
            callWebserviceForCertificateList(dict: ["artistId":self.artistId,"type":type])
        }
    }

//MARK: - textfield Extension
extension CertificateLIstVC{
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
       
        var numberOfChars:Int = 0
        let newText = (txtDescrib.text as NSString).replacingCharacters(in: range, with: text)
        numberOfChars = newText.count
        
        if numberOfChars >= 200 && text != ""{
            return false
        }else{
            
        }
        
        return numberOfChars < 200
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        let text = textField.text! as NSString
        if  addCertificate == false && textField == txtTitle{
            return false
        }
        if (text.length == 1)  && (string == "") {
        }else{
            var substring: String = textField.text!
            substring = (substring as NSString).replacingCharacters(in: range, with: string)
            substring = substring.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            let a = substring.count
                if a > 50 && substring != ""{
                    return false
                }
            return true
            
        }
        return true
    }
    
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.txtTitle{
            txtDescrib.becomeFirstResponder()
        }else if textField == self.txtDescrib{
            txtDescrib.resignFirstResponder()
        }
        return true
    }
}
    
    //MARK: - custome method
    extension CertificateLIstVC{
        func DelegateCalling(){
            imagePicker.delegate = self
            self.viewImage.isHidden = true
            self.tblCertificate.delegate = self
            self.tblCertificate.dataSource = self
            self.txtDescrib.delegate = self
            self.txtTitle.delegate = self

//            self.imgCertificate.layer.cornerRadius = 10
//            self.imgCertificate.layer.masksToBounds = true
        }
        
        func indexMethod(){
            if self.arrCertificate.count == 0 {
                self.lblNoDataFound.isHidden = false
            }else{
                self.lblNoDataFound.isHidden = true
            }
        }
    }
//MARK: - tableview method
extension CertificateLIstVC{
func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//    if arrCertificate.count == 0{
//        self.btnAddCertificate.setTitle("Add", for: .normal)
//        self.lblNoDataFound.isHidden = false
//    }else{
//        self.btnAddCertificate.setTitle("Add More", for: .normal)
//        self.lblNoDataFound.isHidden = true
//    }
    self.indexMethod()
    return arrCertificate.count
}

func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    if let cell = tableView.dequeueReusableCell(withIdentifier: "CellCertificateList", for: indexPath) as? CellCertificateList {
        
        let objStaff = arrCertificate[indexPath.row]
        
        cell.imgCertificate.image = #imageLiteral(resourceName: "gallery_placeholder")
        cell.lblCertiName.text = objStaff.title
        cell.lblCertiDes.text = objStaff.descriptions
        
        if objStaff.status == "0" {
            cell.imgCertificateStatus.isHidden = true
        } else {
            cell.imgCertificateStatus.isHidden = false
        }
        let url = URL(string: objStaff.certificateImage)
        if url != nil{
            //cell.imgCertificate.af_setImage(withURL: url!)
            cell.imgCertificate.sd_setImage(with: url!, placeholderImage:nil)
        }else{
            cell.imgCertificate.image =  #imageLiteral(resourceName: "gallery_placeholder")
        }
        cell.btnDelete.isHidden = true
        cell.btnEdit.isHidden = true
        cell.btnDelete.tag = indexPath.row
        cell.btnDelete.addTarget(self, action:#selector(deleteCertificate(sender:)) , for: .touchUpInside)
        cell.btnEdit.tag = indexPath.row
        cell.btnEdit.addTarget(self, action:#selector(editCertificate(sender:)) , for: .touchUpInside)
        
        return cell
    }else{
        return UITableViewCell()
    }
}
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // let isNavigate = true
        let objFeed = arrCertificate[indexPath.row]
        let sb = UIStoryboard(name: "Add", bundle: Bundle.main)
        let objShowImage = sb.instantiateViewController(withIdentifier:"showFullimagesVC") as! showFullimagesVC
        // objShowImage.lblHeaderName.text = "Certificate"
        let obj = feedData(dict: ["feedPost":objFeed.certificateImage,"videoThumb":objFeed.certificateImage])
        var arr = [feedData]()
        arr.append(obj!)
        objShowImage.strHeaderName = "Qualification"
        objShowImage.arrFeedImages = arr
        self.fromCamera = true
        objShowImage.modalPresentationStyle = .fullScreen
        present(objShowImage, animated: true)// { _ in }
    }
    
    
    
    @available(iOS 11.0, *)
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?{
        
        let Delete = UIContextualAction(style: .normal, title:  "", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            success(true)
            let objMyEventsData  = self.arrCertificate[indexPath.row]
            let dict = ["artistId":objMyEventsData.artistId,"certificateId":objMyEventsData._id]
            self.DeleteView(dict1: dict)
        })
        
        Delete.image = #imageLiteral(resourceName: "delete")
        Delete.backgroundColor =  #colorLiteral(red: 1, green: 0.1490196078, blue: 0, alpha: 1)
        
        let edit = UIContextualAction(style: .normal, title:  "", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            success(true)
             self.addCertificate = false
            let objMyEventsData  = self.arrCertificate[indexPath.row]
            let url = URL(string: objMyEventsData.certificateImage)
            if url != nil{
                //self.imgCertificate.af_setImage(withURL: url!)
                self.imgCertificate.sd_setImage(with: url!, placeholderImage:nil)
            }else{
                self.imgCertificate.image =  #imageLiteral(resourceName: "gallery_placeholder")
            }
            self.certificateImage = self.imgCertificate.image!
            self.certificateID = objMyEventsData._id
            self.txtTitle.text = objMyEventsData.title
            self.txtDescrib.text = objMyEventsData.descriptions
            self.viewImage.isHidden = false
            self.lblHeaderAddQualification.text = "Edit Qualification"
            
            //self.webServiceForEditFolder(folderId:  String(obj._id), folderName:  obj.folderName)
        })
        
        edit.image = #imageLiteral(resourceName: "edit")
        edit.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        
        return UISwipeActionsConfiguration(actions: [Delete,edit])
    }
}
    
    //MARK:- Webservice Call
    extension CertificateLIstVC {
        
        func callWebserviceForCertificateList(dict: [String : Any]){
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            objActivity.startActivityIndicator()
            objServiceManager.requestPost(strURL: WebURL.getAllCertificate, params: dict  , success: { response in
                self.arrCertificate.removeAll()
                objServiceManager.StopIndicator()
                let keyExists = response["responseCode"] != nil
                if  keyExists {
                    sessionExpireAlertVC(controller: self)
                }else{
                    let strStatus =  response["status"] as? String ?? ""
                    if strStatus == k_success{
                        self.parseResponce(response:response)
                    }else{
                        self.tblCertificate.reloadData()
                        // let msg = response["message"] as! String
                        //objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                    }
                }
            }){ error in
                self.tblCertificate.reloadData()
                objServiceManager.StopIndicator()
                objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
            }
        }
        
        func parseResponce(response:[String : Any]){
            if let arr = response["allCertificate"] as? [[String:Any]]{
                if arr.count > 0{
                    for dictArtistData in arr {
                        let objArtistList = CertificateModel.init(dict: dictArtistData)
                        self.arrCertificate.append(objArtistList!)
                    }
                }
                self.tblCertificate.reloadData()
            }
        }
        
        
        func callWebserviceForImageAdd(data:Data,dict:[String:Any],url:String,keys:String){
            
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            
            objActivity.startActivityIndicator()
            objServiceManager.uploadMultipartData(strURL: url, params:dict as [String : AnyObject], imageData:data, fileName: "file.jpg", key: keys, mimeType: "image/*", success: { response in
                
                let keyExists = response["responseCode"] != nil
                if  keyExists {
                    sessionExpireAlertVC(controller: self)
                }else{
                    let strStatus =  response["status"] as? String ?? ""
                    if strStatus == k_success{
                        self.viewImage.isHidden = true
                        let msg = response["message"] as? String ?? ""
                        objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                        self.APICertificatelist()
                    }else{
                        objWebserviceManager.StopIndicator()
                        let msg = response["message"] as! String
                        objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                    }
                }
            }){ error in
                self.tblCertificate.reloadData()
                objServiceManager.StopIndicator()
                objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
            }
        }
        
        func callWebserviceForDeleteCertificate(dict: [String : Any]){
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            objActivity.startActivityIndicator()
            objServiceManager.requestPost(strURL: WebURL.deleteCertificate, params: dict  , success: { response in
                objServiceManager.StopIndicator()
                let keyExists = response["responseCode"] != nil
                if  keyExists {
                    sessionExpireAlertVC(controller: self)
                }else{
                    let strStatus =  response["status"] as? String ?? ""
                    if strStatus == k_success{
                        self.APICertificatelist()
                    }else{
                        let msg = response["message"] as! String
                        objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                    }
                }
            }){ error in
                self.tblCertificate.reloadData()
                objServiceManager.StopIndicator()
                objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
            }
        } }
    
    //MARK: - button extension
    extension CertificateLIstVC{
        
        @IBAction func btnBackAction(_ sender: UIButton) {
            self.navigationController?.popViewController(animated: true)
        }
        
        @objc func deleteCertificate(sender: UIButton!){
            self.view.endEditing(true)
            let objMyEventsData  = self.arrCertificate[sender.tag]
            let dict = ["artistId":objMyEventsData.artistId,"certificateId":objMyEventsData._id]
            DeleteView(dict1: dict)
        }
        @objc func editCertificate(sender: UIButton!){
            self.view.endEditing(true)
            addCertificate = false
            let objMyEventsData  = self.arrCertificate[sender.tag]
            let url = URL(string: objMyEventsData.certificateImage)
            if url != nil{
                //self.imgCertificate.af_setImage(withURL: url!)
                self.imgCertificate.sd_setImage(with: url!, placeholderImage:nil)
            }else{
                self.imgCertificate.image =  #imageLiteral(resourceName: "gallery_placeholder")
            }
            certificateImage = self.imgCertificate.image!
            certificateID = objMyEventsData._id
            self.txtTitle.text = objMyEventsData.title
            self.txtDescrib.text = objMyEventsData.descriptions
            self.viewImage.isHidden = false
            self.lblHeaderAddQualification.text = "Edit Qualification"
        }
        func DeleteView(dict1: [String : Any]){
            let alertController = UIAlertController(title: "Alert", message: "Are you sure want to delete qualification?", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default) {
                UIAlertAction in
                self.callWebserviceForDeleteCertificate(dict: dict1)
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default) {
                UIAlertAction in
            }
            cancelAction.setValue(#colorLiteral(red: 1, green: 0.1490196078, blue: 0, alpha: 1), forKey: "titleTextColor")
            okAction.setValue(#colorLiteral(red: 0, green: 0.851996243, blue: 0.8281483054, alpha: 1), forKey: "titleTextColor")
            
            alertController.addAction(cancelAction)
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
            
        }
        @IBAction func btnAddCertificateAction(_ sender: UIButton) {
            self.lblHeaderAddQualification.text = "Add Qualification"
            self.view.endEditing(true)
            addCertificate = true
            self.viewImage.isHidden = false
            self.txtTitle.text = ""
            self.txtDescrib.text = ""
            certificateID = ""
            self.certificateImage = #imageLiteral(resourceName: "gallery_placeholder")
            self.imgCertificate.image = nil
        }
        @IBAction func btnCameraImageAction(_ sender: UIButton) {
            self.view .endEditing(true)
            
            let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
            let cameraAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default)
            {
                UIAlertAction in
                self.fromCamera = true
                self.openCamera()
            }
            let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default)
            {
                UIAlertAction in
                self.fromCamera = true
                self.openGallary()
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
            {
                UIAlertAction in
            }
            alert.addAction(cameraAction)
            alert.addAction(gallaryAction)
            alert.addAction(cancelAction)
            self.present(alert, animated: true, completion: nil)
        }
        @IBAction func btnCancleCertificate(_ sender: UIButton) {
            self.view.endEditing(true)
            self.viewImage.isHidden = true
            self.imgCertificate.image = nil
        }
        
        @IBAction func btnDoneCertificate(_ sender: UIButton) {
            self.view.endEditing(true)
            txtTitle.text = txtTitle.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            txtDescrib.text = txtDescrib.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
           
            if imgCertificate.image == nil{
                objAppShareData.showAlert(withMessage: "Please select qualification image", type: alertType.bannerDark,on: self)
            }else if txtTitle.text?.count == 0{
                objAppShareData.showAlert(withMessage: "Please enter qualification title", type: alertType.bannerDark,on: self)
            }else if txtDescrib.text?.count == 0{
                objAppShareData.showAlert(withMessage: "Please enter qualification description", type: alertType.bannerDark,on: self)
            }else{
                
                
                if certificateID == ""{
                    let img = self.imgCertificate.image?.fixedOrientation()
                    let data1 =  objCustom.compressImage(image: img!) as Data?
                    let param = ["artistId":artistId,"title":self.txtTitle.text ?? "","description":self.txtDescrib.text ?? ""]
                    callWebserviceForImageAdd(data: data1!, dict: param, url: WebURL.addArtistCertificate, keys: "certificateImage")
                }else{
                     if certificateImage == self.imgCertificate.image!{
                        let param = ["artistId":artistId,"title":self.txtTitle.text ?? "","description":self.txtDescrib.text ?? "","id":certificateID]
                        callWebserviceForImageAdd(data: Data(), dict: param, url: WebURL.editArtistCertificate, keys: "NO")
                    }else{
                        let img = self.imgCertificate.image?.fixedOrientation()
                        let data1 =  objCustom.compressImageCertificate(image: img!) as Data?
                        let param = ["artistId":artistId,"title":self.txtTitle.text ?? "","description":self.txtDescrib.text ?? "","id":certificateID]
                        callWebserviceForImageAdd(data: data1!, dict: param, url: WebURL.editArtistCertificate, keys: "certificateImage")
                    }
                }
                

                
            }
            
        }
    }
    //MARK: - UIImagePicker extension Methods
    extension CertificateLIstVC{
        internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
            if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
                imgCertificate.image = image
                self.viewImage.isHidden = false
                
            }
            else if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
                imgCertificate.image = image
                self.viewImage.isHidden = false
            }
            else{ print("Something went wrong") }
            self.dismiss(animated: true, completion: nil)
        }
        
        private func imagePickerControllerDidCancel(picker: UIImagePickerController)
        { dismiss(animated: true, completion: nil)
            print("picker cancel.") }
        
        func openCamera() //to Access Camera
        {  if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.modalPresentationStyle = .fullScreen
            self.present(imagePicker, animated: true, completion: nil) }
        else { /*  openGallary() */ }
        }
        func openGallary() //to Access Gallery
        {    imagePicker.allowsEditing = false
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            imagePicker.modalPresentationStyle = .fullScreen
            self.present(imagePicker, animated: true, completion: nil)
            
        }
        
        
        
        
        func addAccesorryToKeyBoard(){
            let keyboardDoneButtonView = UIToolbar()
            keyboardDoneButtonView.sizeToFit()
            let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
            let doneButton = UIBarButtonItem(title: "Done", style:.plain, target: self, action: #selector(self.resignKeyBoard))
            doneButton.tintColor = appColor
            
            keyboardDoneButtonView.items = [flexBarButton, doneButton]
            txtDescrib.inputAccessoryView = keyboardDoneButtonView
        }
        @objc func resignKeyBoard(){
            txtDescrib.endEditing(true)
            self.view.endEditing(true)
        }
}


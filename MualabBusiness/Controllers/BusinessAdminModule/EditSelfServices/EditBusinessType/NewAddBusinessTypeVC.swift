//
//  NewAddBusinessTypeVC.swift
//  MualabBusiness
//
//  Created by Mac on 03/01/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit

class NewAddBusinessTypeVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
   
        var artistId =  ""
        fileprivate var arrBusinessType = [ModelAddBusinessType]()
        var callOption: Int = 0
        
        @IBOutlet weak var tblBusinessLIst:UITableView!
        @IBOutlet weak var lblNoDataFound:UIView!
        var cellSelectId = ""

        var fromBusinessAdminVC = false
        
       var screenNo = 0
        
        override func viewDidLoad() {
            super.viewDidLoad()
            if fromBusinessAdminVC == true{
                self.fromBusinessAdminVC = false
                let sb = UIStoryboard(name:"BusinessAdmin",bundle:Bundle.main)
                let objChooseType = sb.instantiateViewController(withIdentifier:"NewServiceListVC") as! NewServiceListVC
                self.navigationController?.pushViewController(objChooseType, animated: false)
            }
            self.manageVCRedirection()

            self.tblBusinessLIst.delegate = self
            self.tblBusinessLIst.dataSource = self
            self.cellSelectId = ""
            self.tblBusinessLIst.reloadData()
           
            // Do any additional setup after loading the view.
        }
        override func viewWillAppear(_ animated: Bool) {
            let userInfo = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]  ?? ["":""]
            self.artistId = userInfo[UserDefaults.keys.id] as? String ?? ""
            self.cellSelectId = ""
            self.callWebserviceForGetServices()
            
        }
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
    
    func manageVCRedirection(){
        if screenNo == 1{
            let sb = UIStoryboard(name:"BusinessAdmin",bundle:Bundle.main)
            let objChooseType = sb.instantiateViewController(withIdentifier:"NewSearchAddBusinessTypeVC") as! NewSearchAddBusinessTypeVC
            self.screenNo = 0
            self.navigationController?.pushViewController(objChooseType, animated: false)
        }else if screenNo == 4{
            let sb = UIStoryboard(name:"BusinessAdmin",bundle:Bundle.main)
            let objChooseType = sb.instantiateViewController(withIdentifier:"NewSearchCategoryTypeVC") as! NewSearchCategoryTypeVC
            objChooseType.cellSelectId = self.cellSelectId
            self.screenNo = 0
            self.navigationController?.pushViewController(objChooseType, animated: false)
            }
        }
    }
    
    //MARK: - Webservices
    extension NewAddBusinessTypeVC{
        internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            if arrBusinessType.count > 0{
                self.lblNoDataFound.isHidden = true
            }else{
                self.lblNoDataFound.isHidden = false
            }
            return arrBusinessType.count
        }
        
        internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "BusinessTypeTableCell") as! BusinessTypeTableCell
            let objSer = arrBusinessType[indexPath.row]
            cell.lblServiceName.text = objSer.serviceName.capitalized
            if objSer.status == "0"{
                cell.viewStatusManage.isHidden = false
            }else{
                cell.viewStatusManage.isHidden = true
                
            }
            if self.cellSelectId == objSer.serviceId{
                cell.imgCheck.isHidden = true
                cell.lblServiceName.textColor = appColor
            }else{
                cell.imgCheck.isHidden = true
                cell.lblServiceName.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            }
            
            cell.btnChackUnchack.tag = indexPath.row
            cell.btnChackUnchack.addTarget(self, action:#selector(btnChackUnchackAction(sender:)) , for: .touchUpInside)
            return cell
        }
        
        internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){

        }
        
        @available(iOS 11.0, *)
        func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
        {
            let Delete = UIContextualAction(style: .normal, title:  "", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
                let objSer = self.arrBusinessType[indexPath.row]
                let businessId = objSer.serviceId
                self.deleteCell(mainBookingId: businessId)
                success(true)
            })
            Delete.image = #imageLiteral(resourceName: "delete")
            Delete.backgroundColor =  #colorLiteral(red: 0.9137254902, green: 0.2117647059, blue: 0.2274509804, alpha: 1)
            return UISwipeActionsConfiguration(actions: [Delete])
        }
        
        
        @objc func btnChackUnchackAction(sender: UIButton!){
            self.view.endEditing(true)
            let objMyEventsData = self.arrBusinessType[sender.tag]
            
            let indexPath = IndexPath(row:(sender as AnyObject).tag, section: (sender as AnyObject).superview!!.tag)
            let cell = (tblBusinessLIst?.cellForRow(at: indexPath) as? BusinessTypeTableCell)!
            
            if self.cellSelectId == objMyEventsData.serviceId{
                self.cellSelectId = ""
            }else{
                self.cellSelectId = objMyEventsData.serviceId
            }
            self.tblBusinessLIst.reloadData()            
            objAppShareData.objModelFinalSubCategory.param[objAppShareData.objModelFinalSubCategory.bookingName] = objMyEventsData.serviceName
            
            objAppShareData.objModelFinalSubCategory.param[objAppShareData.objModelFinalSubCategory.strBusinessId] = objMyEventsData.serviceId
            if screenNo == 5{
                let sb = UIStoryboard(name:"BusinessAdmin",bundle:Bundle.main)
                let objChooseType = sb.instantiateViewController(withIdentifier:"NewSearchCategoryTypeVC") as! NewSearchCategoryTypeVC
                objChooseType.idBusiness = self.cellSelectId
                self.navigationController?.pushViewController(objChooseType, animated: true)
            }else{
                let sb = UIStoryboard(name:"BusinessAdmin",bundle:Bundle.main)
                let objChooseType = sb.instantiateViewController(withIdentifier:"NewAddCategoryTypeVC") as! NewAddCategoryTypeVC
                objChooseType.businessId = self.arrBusinessType[indexPath.row].serviceId//self.cellSelectId
                objChooseType.screenNo = 0
                self.navigationController?.pushViewController(objChooseType, animated: true)
            }
          
        }
        
       
    }
    //MARK: - Webservices
    fileprivate extension NewAddBusinessTypeVC{
        
        @IBAction func btnBack(_ sender:Any){
//            let sb = UIStoryboard(name:"BusinessAdmin",bundle:Bundle.main)
//            let objChooseType = sb.instantiateViewController(withIdentifier:"NewServiceListVC") as! NewServiceListVC
//            self.navigationController?.pushViewController(objChooseType, animated: false)
            self.navigationController?.popViewController(animated: true)
        }
        
        @IBAction func btnAddBusinessType(_ sender:Any){
            let sb = UIStoryboard(name:"BusinessAdmin",bundle:Bundle.main)
            let objChooseType = sb.instantiateViewController(withIdentifier:"NewSearchAddBusinessTypeVC") as! NewSearchAddBusinessTypeVC
            self.navigationController?.pushViewController(objChooseType, animated: true)
        }
        
        @IBAction func btnAddCategoryType(_ sender:Any){
            self.addCategory()
        }
        
        func addCategory(){
            if self.arrBusinessType.count == 0{
                objAppShareData.showAlert(withMessage: "Please add business type", type: alertType.bannerDark,on: self)
            }else if self.cellSelectId == ""{
                objAppShareData.showAlert(withMessage: "Please select any business type", type: alertType.bannerDark,on: self)
            }else{
                let sb = UIStoryboard(name:"BusinessAdmin",bundle:Bundle.main)
                let objChooseType = sb.instantiateViewController(withIdentifier:"NewSearchCategoryTypeVC") as! NewSearchCategoryTypeVC
                objChooseType.idBusiness = self.cellSelectId
                self.navigationController?.pushViewController(objChooseType, animated: true)
            }
        }
        
        @IBAction func btnContinew(_ sender:Any){
            if self.arrBusinessType.count == 0{
                objAppShareData.showAlert(withMessage: "Please add business type", type: alertType.bannerDark,on: self)
            }else if self.cellSelectId == ""{
                objAppShareData.showAlert(withMessage: "Please select any business type", type: alertType.bannerDark,on: self)
            }else{
                let sb = UIStoryboard(name:"BusinessAdmin",bundle:Bundle.main)
                let objChooseType = sb.instantiateViewController(withIdentifier:"NewAddCategoryTypeVC") as! NewAddCategoryTypeVC
                objChooseType.businessId = self.cellSelectId
                self.navigationController?.pushViewController(objChooseType, animated: true)
            }
        }
    }
    //MARK: - Webservices
    fileprivate extension NewAddBusinessTypeVC{
        func callWebserviceForGetServices(){
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            objActivity.startActivityIndicator()
            self.arrBusinessType.removeAll()
            objServiceManager.requestGet(strURL: WebURL.myBusinessType, params: nil, success: { response in
                if  response["status"] as? String == "success"{
                    //self.saveData(dict:response)
                    if let data = response["data"] as? [[String:Any]]{
                        for obj in data{
                            let myObj = ModelAddBusinessType.init(dict: obj)
                            self.arrBusinessType.append(myObj)
                        }
                    }
                    self.tblBusinessLIst.reloadData()
                }else{
                    self.tblBusinessLIst.reloadData()
                }
            }) { error in
                self.tblBusinessLIst.reloadData()
                // objAppShareData.showDeafultAlert(withTitle: message.error.title, message: message.error.wrong, on: self)
                objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
            }
        }
        
        func deleteCell(mainBookingId:String){
            self.checkBackEndResponce(Ids:mainBookingId)
        }
        
        func checkBackEndResponce(Ids:String){
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            objActivity.startActivityIndicator()
            let dict = ["id":Ids,"artistId":self.artistId,"type":"businessType"]
            objServiceManager.requestPost(strURL: WebURL.bookingCheck, params: dict as [String : AnyObject], success: { response in
                if  response["status"] as? String == "success"{
                    let message = response["message"] as? String ?? ""
                if message == "Booking not exist"{
                    self.callWebserviceDeleteServices(Ids:Ids)
                }else{
                    objAppShareData.showAlert(withMessage:  "You can't delete this business type, currently it is used by customer for booking.", type: alertType.bannerDark,on: self)
                    }
                }else{
                    objAppShareData.showAlert(withMessage:  response["message"] as? String ?? "", type: alertType.bannerDark,on: self)
                }
            }) { error in
                self.tblBusinessLIst.reloadData()
                objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
            }
        }
        
        func callWebserviceDeleteServices(Ids:String){
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            objActivity.startActivityIndicator()
            let dict = ["id":Ids]
            
            objServiceManager.requestPost(strURL: WebURL.deleteBussinessType, params: dict as [String : AnyObject], success: { response in
                
                self.arrBusinessType.removeAll()
                
                if  response["status"] as? String == "success"{
                    self.callWebserviceForGetServices()
                }else{
                    self.tblBusinessLIst.reloadData()
                }
            }) { error in
                self.tblBusinessLIst.reloadData()
                objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
            }
        }
}

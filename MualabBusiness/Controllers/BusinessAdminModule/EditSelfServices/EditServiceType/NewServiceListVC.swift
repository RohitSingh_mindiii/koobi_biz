//
//  NewServiceListVC.swift
//  MualabBusiness
//
//  Created by Mac on 03/01/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import DropDown

class NewServiceListVC:  UIViewController,UITableViewDelegate,UITableViewDataSource {
        @IBOutlet weak var tblBusinessLIst:UITableView!
        @IBOutlet weak var lblNoDataFound:UIView!
    fileprivate var arrBusinessType = [ModelAddBusinessType]()
    var businessTypeCount = 0

    
    @IBOutlet weak var btnIncall:UIButton!
    @IBOutlet weak var btnOutcall:UIButton!
    @IBOutlet weak var viewBtnIncallBottum:UIView!
    @IBOutlet weak var viewBtnOutcallBottum:UIView!
    @IBOutlet weak var viewIncallSet:UIView!
    @IBOutlet weak var viewOutcallSet:UIView!
    
    @IBOutlet weak var viewButtonSet:UIView!

    @IBOutlet weak var viewBtnInOut: UIView!
    
    
    var arrBusinessTypeListTable = [ModelBusinessTypeList]()
    var arrIncallList = [ModelBusinessTypeList]()
    var arrOutcallList = [ModelBusinessTypeList]()
    
        var arrBusinessTypeList = [ModelBusinessTypeList]()
        var navigate = true
        var serType = "I"
        var artistId = ""
    
        override func viewDidLoad() {
            super.viewDidLoad()
            self.addGesturesToView()

            self.viewButtonSet.isHidden = true
           serType = "I"
            self.tblBusinessLIst.delegate = self
            self.tblBusinessLIst.dataSource = self
            /*self.tblBusinessLIst.reloadData(); self.tblBusinessLIst.contentSize.height = self.tblBusinessLIst.contentSize.height+60*/
            self.tblReloadDataForIncallOutcall()
            
            NotificationCenter.default.addObserver(self, selector: #selector(self.reloadDataForDeleteCell), name: NSNotification.Name(rawValue: "Delete"), object: nil)
            
            NotificationCenter.default.addObserver(self, selector: #selector(self.reloadDataForEditCell), name: NSNotification.Name(rawValue: "Edit"), object: nil)
            
            NotificationCenter.default.addObserver(self, selector: #selector(self.reloadDataForDetailCell), name: NSNotification.Name(rawValue: "Detail"), object: nil)
        }
        
        
        @objc func reloadDataForDeleteCell(notification: Notification){
            self.checkBackEndResponce(Ids: objAppShareData.objModelServicesList.id, forDelete: true)
        }
        @objc func reloadDataForDetailCell(notification: Notification){
            if objAppShareData.manageNavigation == false{
                objAppShareData.manageNavigation = true
                
                if navigate == false{
                    return
                }
                let o = objAppShareData.objModelServicesList
                let a = objAppShareData.objModelFinalSubCategory
                a.param = [:]
                a.param[a.serviceName] = o.serviceName
                a.param[a.strBusinessId] = o.mainBookingId
                a.param[a.strCategoryId] = o.mainCategoryId
                a.param[a.bookingType] = o.bookingType
                a.param[a.incallPrice] = o.price
                a.param[a.outCallPrice] = o.outcallPrice
                a.param[a.descriptions] = o.describe
                a.param[a.completionTime] = o.time
                
                let sb = UIStoryboard(name:"BusinessAdmin",bundle:Bundle.main)
                let objChooseType = sb.instantiateViewController(withIdentifier:"NewEditServiceVC") as! NewEditServiceVC
                objChooseType.objModelClass = objAppShareData.objModelServicesList
                objChooseType.viewType = "YES"
                if navigate == true{
                    navigate = false
                    self.navigationController?.pushViewController(objChooseType, animated: true)
                }
            }
        }
        @objc func reloadDataForEditCell(notification: Notification){
            self.checkBackEndResponce(Ids: objAppShareData.objModelServicesList.id, forDelete: false)
            
        }
        override func viewWillAppear(_ animated: Bool) {
            navigate = true
            let businessType = UserDefaults.standard.string(forKey: UserDefaults.keys.mainServiceType)
            var strType = ""
            self.viewIncallSet.isHidden = false
            self.viewOutcallSet.isHidden = false
            if businessType == "1"{
                strType = "Incall"
                self.serType = "I"
                self.viewOutcallSet.isHidden = true
                self.viewIncallSet.isHidden = false
            }else if businessType == "2"{
                strType = "Outcall"
                self.serType = "O"
                self.viewIncallSet.isHidden = true
                self.viewOutcallSet.isHidden = false
            }else{
                if  self.serType == ""{
                self.serType = "I"
                }
                strType = "Both"

                //self.viewBookingType.isHidden = true
            }
            objAppShareData.objModelFinalSubCategory.param[objAppShareData.objModelFinalSubCategory.bookingType] = strType
            
            //.get(String(self.callOption),forKey: UserDefaults.keys.mainServiceType)
            self.callWebserviceForUpdateData()
            let userInfo = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]  ?? ["":""]
            self.artistId = userInfo[UserDefaults.keys.id] as? String ?? ""
          
            self.callWebserviceForGetBusinessType()
        }
        
        
        
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
    }
    
    //MARK:- extension for tableview delegate
    extension NewServiceListVC {
        func numberOfSections(in tableView: UITableView) -> Int
        {
            if self.arrBusinessTypeListTable.count > 0{
                self.lblNoDataFound.isHidden = true
            }else{
                self.lblNoDataFound.isHidden = false
            }
            return self.arrBusinessTypeListTable.count
        }
        

        
        func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
            let headerView = UIView()
            headerView.backgroundColor = UIColor.white
            
            let headerLabel = UILabel(frame: CGRect(x: 20, y: 2, width:
                tableView.bounds.size.width, height: tableView.bounds.size.height))
            headerLabel.font = UIFont(name: "Nunito-Medium", size: 17)
            headerLabel.textColor = UIColor.black
            headerLabel.text = self.tableView(self.tblBusinessLIst, titleForHeaderInSection: section)
            headerLabel.sizeToFit()
            headerView.addSubview(headerLabel)
            
            return headerView
        }
        func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
            return 30
        }
        func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
        {
            return self.arrBusinessTypeListTable[section].businessName.capitalized
            
        }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            let arrDevelopers = self.arrBusinessTypeListTable[section].arrBusinessList
            return arrDevelopers.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
            if let cell = tableView.dequeueReusableCell(withIdentifier: "BusinessListTypeTableCell", for: indexPath) as? BusinessListTypeTableCell{
           
                let obj  = arrBusinessTypeListTable[indexPath.section].arrBusinessList[indexPath.row]
          
                cell.lblCategoryName.text = obj.categoryName
                cell.arrService = obj.arrServices
                print("height = ",CGFloat(30+(obj.arrServices.count*45)))
                cell.setNeedsLayout()
                cell.arrBusinessCategory.append(arrBusinessTypeListTable[indexPath.section].businessName)
                cell.arrBusinessCategory.append(obj.categoryName)
                cell.serType = serType
                cell.tblServiceList.reloadData()
                return cell
            }else{
                return UITableViewCell()
            }
        }
    }
    
    
    //MARK:- extension for tableview delegate
    extension NewServiceListVC {
        
        @IBAction func btnContinew(_ sender:Any){
            let controllers = self.navigationController?.viewControllers
            var back = true
            for vc in controllers! {
                if vc is BusinessAdminLiastVC {
                    back = false
                    _ = self.navigationController?.popToViewController(vc as! BusinessAdminLiastVC, animated: true)
                }
            }
            if back == true{
                self.navigationController?.popViewController(animated: true)
            }
        }
        
        @IBAction func btnBack(_ sender:Any){
            let controllers = self.navigationController?.viewControllers
            var back = true
                for vc in controllers! {
                    if vc is BusinessAdminLiastVC {
                        back = false
                        _ = self.navigationController?.popToViewController(vc as! BusinessAdminLiastVC, animated: true)
                    }
                }
            if back == true{
                self.navigationController?.popViewController(animated: true)
            }
        }
        
        @IBAction func btnAddCategoryType(_ sender:Any){
            self.viewButtonSet.isHidden = true

         //   self.popToViewContruller()
            let sb = UIStoryboard(name:"BusinessAdmin",bundle:Bundle.main)
            if self.arrBusinessType.count == 0{
                let objChooseType = sb.instantiateViewController(withIdentifier:"NewAddBusinessTypeVC") as! NewAddBusinessTypeVC
                objChooseType.screenNo = 4
                self.navigationController?.pushViewController(objChooseType, animated: false)
            }else if self.arrBusinessType.count  == 1{
                let objChooseType = sb.instantiateViewController(withIdentifier:"NewSearchCategoryTypeVC") as! NewSearchCategoryTypeVC
                objChooseType.idBusiness = self.arrBusinessType[0].serviceId
                self.navigationController?.pushViewController(objChooseType, animated: false)
            }else{
                let objChooseType = sb.instantiateViewController(withIdentifier:"NewAddBusinessTypeVC") as! NewAddBusinessTypeVC
                objChooseType.screenNo = 5
                self.navigationController?.pushViewController(objChooseType, animated: false)
            }
            
        }
        
        
        @IBAction func btnAddBusinessType(_ sender:Any){
            self.viewButtonSet.isHidden = true
            //   self.popToViewContruller()
        if self.arrBusinessType.count  == 0{
            let sb = UIStoryboard(name:"BusinessAdmin",bundle:Bundle.main)
            let objChooseType = sb.instantiateViewController(withIdentifier:"NewAddBusinessTypeVC") as! NewAddBusinessTypeVC
            objChooseType.screenNo = 1
            self.navigationController?.pushViewController(objChooseType, animated: false)
        }else{
            let sb = UIStoryboard(name:"BusinessAdmin",bundle:Bundle.main)
            let objChooseType = sb.instantiateViewController(withIdentifier:"NewAddBusinessTypeVC") as! NewAddBusinessTypeVC
            objChooseType.screenNo = 0
            self.navigationController?.pushViewController(objChooseType, animated: false)
            }
        }
        
        @IBAction func btnAddServiceType(_ sender:Any){
            self.viewButtonSet.isHidden = true

            //    objAppShareData.showAlert(withMessage: "Under development", type: alertType.bannerDark,on: self)
            //    return
        objAppShareData.objModelFinalSubCategory.param = [:]
            let businessType = UserDefaults.standard.string(forKey: UserDefaults.keys.mainServiceType)
            var strType = ""
            if businessType == "1"{
                strType = "Incall"
            }else if businessType == "2"{
                strType = "Outcall"
            }else{
                strType = "Both"
            }
            if strType == "Incall"{
            }else if strType == "Outcall"{
            }else{
                strType = "Both"
            }
            objAppShareData.objModelFinalSubCategory.param[objAppShareData.objModelFinalSubCategory.bookingType] =
            strType
            let sb = UIStoryboard(name:"BusinessAdmin",bundle:Bundle.main)
            let objChooseType = sb.instantiateViewController(withIdentifier:"NewAddServiceVC") as! NewAddServiceVC
            objChooseType.fromCategory = false
            self.navigationController?.pushViewController(objChooseType, animated: true)
        }}
    
    
    extension NewServiceListVC{
        
        func EditDetailCell(objModel:ModelServicesList,strViewType:String){
            if objAppShareData.manageNavigation == false{
                objAppShareData.manageNavigation = true
                let obj = objModel
                let a = objAppShareData.objModelFinalSubCategory
                a.param = [:]
                a.param[a.serviceName] = obj.serviceName
                a.param[a.strBusinessId] = obj.mainBookingId
                a.param[a.strCategoryId] = obj.mainCategoryId
                a.param[a.bookingType] = obj.bookingType
                a.param[a.incallPrice] = obj.price
                a.param[a.outCallPrice] = obj.outcallPrice
                a.param[a.descriptions] = obj.describe
                a.param[a.completionTime] = obj.time
                a.param[a.strSubSubServiceId] = obj.id
                let sb = UIStoryboard(name:"BusinessAdmin",bundle:Bundle.main)
                let objChooseType = sb.instantiateViewController(withIdentifier:"NewEditServiceVC") as! NewEditServiceVC
                objChooseType.objModelClass = objModel
                objChooseType.viewType = strViewType
                objChooseType.serviceName = objModel.serviceName
                if navigate == true{
                    navigate = false
                    self.navigationController?.pushViewController(objChooseType, animated: true)
                }
            }
        }
        
        
        func deleteCell(objModel:ModelServicesList){
            let param = ["id":objModel.id]
            
            if objModel.bookingType == "Both"{
                var InPrice = objModel.incallPrice
                var OutPrice = objModel.outcallPrice
                if serType == "I"{
                    InPrice = "0.0"
                }
                if serType == "O"{
                    OutPrice = "0.0"
                }
                let catID = objModel.mainCategoryId
                
            let mainDict = ["serviceId":objModel.mainBookingId,
                            "subserviceId":catID,
                            "title":objModel.serviceName,
                            "description":objModel.describe,
                            "inCallPrice":InPrice,
                            "outCallPrice":OutPrice,
                            "completionTime":objModel.time,
                            "id":objModel.id] as [String : Any]
            self.callWebserviceAddService(dicParam: mainDict)
            }else{
               callWebserviceDeleteService(dicParam: param)
            }
            
        }
        
        
        func sendArray(arrSend:[ModelServicesList]){
            var serviceJosnArray = [[String: Any]]()
            var serviceJosn = [String: Any]()
            for obj in arrSend{
                let objSub = obj
                
                var a = ""
                a = objSub.mainBookingId
                
                var priceIC = objSub.incallPrice
                if objSub.incallPrice != ""{
                    let b = float_t(objSub.incallPrice) ?? 0.0
                    priceIC = String(b)
                }else{
                    priceIC = "0.0"
                }
                var priceOC = objSub.outcallPrice
                if objSub.outcallPrice != ""{
                    let b = float_t(objSub.outcallPrice) ?? 0.0
                    priceOC = String(b)
                }else{
                    priceOC = "0.0"
                }
                
                serviceJosn["serviceId"] = a
                serviceJosn["subserviceId"] = objSub.mainCategoryId
                serviceJosn["title"] = objSub.serviceName
                serviceJosn["description"] = objSub.describe
                serviceJosn["inCallPrice"] = priceIC
                serviceJosn["outCallPrice"] = priceOC
                serviceJosn["completionTime"] = objSub.time
                serviceJosnArray.append(serviceJosn)
            }
            
            var objectString = ""
            if let objectData = try? JSONSerialization.data(withJSONObject: serviceJosnArray, options: JSONSerialization.WritingOptions(rawValue: 0)) {
                objectString = String(data: objectData, encoding: .utf8)!
            }
            
            let dict = ["artistService":objectString]
            print(dict)
            self.callWebserviceForUpdateRange(dicParam: dict)
        }
        
        func alert(str:String){
            //objAppShareData.showAlert(withMessage: str, type: alertType.bannerDark,on: self)
            if objAppShareData.editSelfServices == true{
                let controllers = self.navigationController?.viewControllers
                for vc in controllers! {
                    if vc is  BusinessAdminLiastVC{
                        // navigationCheck = true
                        _ = self.navigationController?.popToViewController(vc as! BusinessAdminLiastVC, animated: true)
                    }
                }
            }
        }
        func callWebserviceForUpdateRange(dicParam:[String:Any]){
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            objActivity.startActivityIndicator()
            
            objServiceManager.requestPost(strURL: WebURL.addArtistService, params: dicParam, success: { response in
                let  status = response["status"] as? String ?? ""
                if status == "success"{
                    if let arr = response["artistServices"] as? [[String:Any]]{
                        
                        self.alert(str:(response["message"] as? String ?? ""))
                        //                    }else{
                        //                        objAppShareData.showAlert(withMessage: "Plese add service", type: alertType.bannerDark,on: self)
                        //                    }
                    }
                    
                }else{
                    objAppShareData.showAlert(withMessage: (response["message"] as? String ?? ""), type: alertType.bannerDark,on: self)
                }
            }) { error in
                objActivity.stopActivity()
                objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
            }
        }
    }
    
    //MARK: - Navigation method
    extension NewServiceListVC{
        func popToViewContruller(){
            let controllers = self.navigationController?.viewControllers
            for vc in controllers! {
                if vc is NewAddBusinessTypeVC {
                    _ = self.navigationController?.popToViewController(vc as! NewAddBusinessTypeVC, animated: true)
                }
            }
        }
    }
    
    
    //MARK: - call api for get service list from backecd server and set on local server
    
    extension NewServiceListVC{
        
        func callWebserviceForUpdateData(){
            
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            self.arrBusinessTypeList.removeAll()
            let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] ?? ["":""]
let userId = decoded[UserDefaults.keys.userId] as? String ?? ""
            objActivity.startActivityIndicator()
            arrMain.removeAll()
            objServiceManager.requestPostForJson(strURL: WebURL.artistService, params: ["artistId":userId], success: { response in
                print(response)
                
                let status = response["status"] as? String ?? ""
                if status == "success"{
                    
                    
                    if let arrArtistServices = response["artistServices"] as? [[String:Any]]{
                        for dict1 in arrArtistServices {
                            var ids = ""
                            if let id = dict1["serviceId"] as? Int {
                                ids = String(id)
                            }else if let id = dict1["serviceId"] as? String{
                                ids = id
                            }
                            let businessName = dict1["serviceName"] as? String ?? ""
                            
                            var arrServices = [[String:Any]]()
                            var arrayCate = [[String:Any]]()
                            
                            if let arrSer = dict1["subServies"] as? [[String:Any]]{
                                for dict11 in arrSer{
                                    var arrayServices = [[String:Any]]()
                                    var catId = ""
                                    if let cid = dict11["subServiceId"] as? String{
                                       catId = cid
                                    }else if let cid = dict11["subServiceId"] as? Int{
                                        catId = String(cid)
                                    }
                                    
                                    if let arrArtistServices = dict11["artistservices"] as? [[String:Any]]{
                                        for dict111 in arrArtistServices{
                                            
                                            var outCallPrice = "0.0"
                                            if let id = dict111["outCallPrice"] as? String {
                                                outCallPrice = String(id)
                                                print("Value = Int = ",outCallPrice)
                                            }else if let id = dict111["outCallPrice"] as? double_t{
                                                outCallPrice = String(id)
                                                print("Value = String = ",outCallPrice)
                                            }else if let id = dict111["outCallPrice"] as? float_t{
                                                outCallPrice = String(id)
                                                print("Value = float_t = ",outCallPrice)
                                            }else if let id = dict111["outCallPrice"] as? Int{
                                                outCallPrice = String(id)
                                                print("Value = double_t = ",outCallPrice)
                                            }else if let id = dict111["outCallPrice"] as? Double{
                                                outCallPrice = String(id)
                                                print("Value = Double = ",outCallPrice)

                                            }
                                            
                                            
                                            
                                            
                                            var inCallPrice = "0.0"
                                            if let id = dict111["inCallPrice"] as? String{
                                                    inCallPrice = String(id)
                                                print("Value = String = ",inCallPrice)
                                            }else if let id = dict111["inCallPrice"] as? double_t {
                                                    inCallPrice = String(id)
                                                    print("double_t = Int = ",inCallPrice)
                                            }else if let id = dict111["inCallPrice"] as? float_t{
                                                inCallPrice = String(id)
                                                print("Value = float_t = ",inCallPrice)
                                            }else if let id = dict111["inCallPrice"] as? Int{
                                                inCallPrice = String(id)
                                                print("Value = double_t = ",inCallPrice)
                                            }else if let id = dict111["inCallPrice"] as? Double{
                                                inCallPrice = String(id)
                                                print("Value = Double = ",inCallPrice)

                                            }
                                            
                                            var idService = ""
                                            if let id = dict111["_id"] as? Int { idService = String(id)
                                            }else if let id = dict111["_id"] as? String{ idService = id
                                            }
                                            
                                            
                                            let dictA = ["price":inCallPrice,
                                                         "incallPrice":inCallPrice,
                                                         "outcallPrice":outCallPrice,
                                                         "_id":idService,
                                                         "mainBookingId":ids,
                                                         "mainCategoryId":catId,
                                                         "serviceName":dict111["title"] as? String ?? "",
                                                         "bookingType":dict111["bookingType"] as? String ?? "",
                                                         "describe":dict111["description"] as? String ?? "",
                                                         "time":dict111["completionTime"] as? String ?? ""] as [String : Any]
                                            // let obj = ModelServicesList.init(dict: dict)
                                            arrayServices.append(dictA)
                                            arrServices.append(dictA)
                                        }
                                    }
                                    let catName = dict11["subServiceName"] as? String ?? ""
                                    let c = dict1["serviceName"] ?? ""
                                    let dict1 = ["categoryId":catId,
                                                 "categoryName":catName,
                                                 "businessName":businessName,
                                                 "arrServices":arrayServices] as [String : Any]
                                    if arrayServices.count > 0{
                                        arrayCate.append(dict1)
                                    }
                                }
                            }
                            let dict2 = ["businessName":businessName,
                                         "businessId":ids,
                                         "arrServices":arrServices,
                                         "arrCategory":arrayCate] as [String : Any]
                            
                            let objModelBusinessTypeList = ModelBusinessTypeList(dict: dict2)
                            
                            if objModelBusinessTypeList.arrBusinessList.count > 0{
                                self.arrBusinessTypeList.append(objModelBusinessTypeList)
                            }
                        }
                        if arrMain.count > 0{
                             self.tblReloadDataForIncallOutcall()
                        }
                    }
                      self.tblReloadDataForIncallOutcall()
                }else{
                   self.tblReloadDataForIncallOutcall()
                }
            }) { error in
                objActivity.stopActivity()
                /*self.tblBusinessLIst.reloadData(); self.tblBusinessLIst.contentSize.height = self.tblBusinessLIst.contentSize.height+60*/ self.tblReloadDataForIncallOutcall()
            }
        }
        
        func checkBackEndResponce(Ids:String,forDelete:Bool){
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            objActivity.startActivityIndicator()
            let dict = ["id":Ids,"artistId":self.artistId,"type":"service"]
            objServiceManager.requestPost(strURL: WebURL.bookingCheck, params: dict as [String : AnyObject], success: { response in
                if  response["status"] as? String == "success"{
                    let message = response["message"] as? String ?? ""
                    if message == "Booking not exist"{
                        if forDelete == true{
                            self.deleteCell(objModel: objAppShareData.objModelServicesList)
                        }else{
                            self.EditDetailCell(objModel: objAppShareData.objModelServicesList, strViewType: "NO")
                        }
                    }else{
                        if forDelete == true{
                            objAppShareData.showAlert(withMessage:  "You can't delete this service, currently  it is used by customer for booking.", type: alertType.bannerDark,on: self)
                        }else{
                            objAppShareData.showAlert(withMessage:  "You can't edit this service, currently  it is used by customer for booking.", type: alertType.bannerDark,on: self)
                        }
                    }
                }else{
                    objAppShareData.showAlert(withMessage:  response["message"] as? String ?? "", type: alertType.bannerDark,on: self)
                }
            }) { error in
                self.tblBusinessLIst.reloadData(); self.tblBusinessLIst.contentSize.height = self.tblBusinessLIst.contentSize.height+60
                
                objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
            }
        }
        
        
        
        
        
        func callWebserviceDeleteService(dicParam:[String:Any]){
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            objActivity.startActivityIndicator()
            
            objServiceManager.requestPost(strURL: WebURL.deleteService, params: dicParam, success: { response in
                let  status = response["status"] as? String ?? ""
                var goBack = false
                if status == "success"{
                   self.callWebserviceForUpdateData()
                }else{
                    objAppShareData.showAlert(withMessage: (response["message"] as? String ?? ""), type: alertType.bannerDark,on: self)
                }
            }) { error in
                objActivity.stopActivity()
                objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
            }
        }
    }


extension NewServiceListVC{
    
    
    //////////////////////////////////////////////////manage parsing acording to incall outcall
    
    func tblReloadDataForIncallOutcall(){
        self.arrIncallList.removeAll()
        self.arrOutcallList.removeAll()
        self.arrBusinessTypeListTable.removeAll()
        for obj in self.arrBusinessTypeList{
            var arrInCat = [[String:Any]]()
            var arrOutCat = [[String:Any]]()
            let businessName = obj.businessName
            let ids = obj.businessId
            var arrServices = [[String:Any]]()
            
            for obj1 in obj.arrBusinessList{
                var arrInServices = [[String:Any]]()
                var arrOutServices = [[String:Any]]()
                let catId = obj1.categoryId
                let catName = obj1.categoryName
                for obj2 in obj1.arrServices{
                    let dict = [
                        "mainBookingId":obj.businessId,
                        "mainCategoryId":obj1.categoryId,
                        "_id":obj2.id,
                        "serviceName":obj2.serviceName,
                        "price":obj2.price,
                        "incallPrice":obj2.incallPrice,
                        "outcallPrice":obj2.outcallPrice,
                        "bookingType":obj2.bookingType,
                        "describe":obj2.describe,
                        "time":obj2.time]
                    if obj2.bookingType == "Incall" || obj2.bookingType == "Both"{
                        arrInServices.append(dict)
                    }
                    if obj2.bookingType == "Outcall" || obj2.bookingType == "Both"{
                        arrOutServices.append(dict)
                    }
                    
                    arrServices.append(dict)
                }
                
                
                var dict1 = ["categoryId":catId,
                             "categoryName":catName,
                             "businessName":businessName,
                             "arrServices":arrOutServices] as [String : Any]
                
                if arrInServices.count > 0 {
                    dict1["arrServices"] = arrInServices
                    arrInCat.append(dict1)
                }
                
                if arrOutServices.count > 0 {
                    dict1["arrServices"] = arrOutServices
                    arrOutCat.append(dict1)
                }
            }
            
            var dict2 = ["businessName":businessName,
                         "businessId":ids,
                         "arrServices":arrServices,
                         "arrCategory":arrOutCat] as [String : Any]
            
            if arrInCat.count > 0 {
                
                dict2["arrCategory"] = arrInCat
                let objSubSerInc = ModelBusinessTypeList(dict: dict2)
                arrIncallList.append(objSubSerInc)
                
            }
            if arrOutCat.count > 0 {
                dict2["arrCategory"] = arrOutCat
                let objSubSerOut = ModelBusinessTypeList(dict: dict2)
                arrOutcallList.append(objSubSerOut)
                
            }
        }
        print("arrIncallList = ",self.arrIncallList.count)
        print("arrOutcallList = ",self.arrOutcallList.count)
        
        
        if self.serType == "I"{
            arrBusinessTypeListTable = self.arrIncallList
        }else if serType == "O" {
            arrBusinessTypeListTable = self.arrOutcallList
        }else{
            arrBusinessTypeListTable = arrBusinessTypeList
        }
        if self.serType == "I" {
            self.btnIncall.setTitleColor(#colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1), for: .normal)
            self.btnOutcall.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            self.viewBtnIncallBottum.backgroundColor = #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1)
            self.viewBtnOutcallBottum.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        }else{
            self.btnOutcall.setTitleColor(#colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1), for: .normal)
            self.btnIncall.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
             self.viewBtnIncallBottum.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
            self.viewBtnOutcallBottum.backgroundColor = #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1)
        }
        let dict = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] ?? ["":""]
        let serviceType = dict[UserDefaults.keys.serviceType] as? String ?? ""
      
        if serviceType == "0" || serviceType == "3"{
        }else if serviceType == "1"{
            self.arrOutcallList.removeAll()
        }else if serviceType == "2"{
            self.arrIncallList.removeAll()
        }
        
        if serType == "I"{
            arrBusinessTypeListTable = self.arrIncallList
        }else if serType == "O" {
            arrBusinessTypeListTable = self.arrOutcallList
        }else{
            arrBusinessTypeListTable = arrBusinessTypeList
        }
        self.tblBusinessLIst.reloadData(); self.tblBusinessLIst.contentSize.height = self.tblBusinessLIst.contentSize.height+60
     }
    
    
    @IBAction func btnIncall(_ sender:Any){
        serType = "I"
        if serType == "I"{
            arrBusinessTypeListTable = self.arrIncallList
        }else if serType == "O" {
            arrBusinessTypeListTable = self.arrOutcallList
        }else{
            arrBusinessTypeListTable = arrBusinessTypeList
        }
        if self.serType == "I" {
            self.btnIncall.setTitleColor(#colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1), for: .normal)
            self.btnOutcall.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            self.viewBtnIncallBottum.backgroundColor = #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1)
            self.viewBtnOutcallBottum.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        }else{
            self.btnOutcall.setTitleColor(#colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1), for: .normal)
            self.btnIncall.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
             self.viewBtnIncallBottum.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
            self.viewBtnOutcallBottum.backgroundColor = #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1)
        }
        /*self.tblBusinessLIst.reloadData(); self.tblBusinessLIst.contentSize.height = self.tblBusinessLIst.contentSize.height+60*/ self.tblReloadDataForIncallOutcall()
    }
    
    @IBAction func btnOutcall(_ sender:Any){
        serType = "O"
        if serType == "I"{
            arrBusinessTypeListTable = self.arrIncallList
        }else if serType == "O" {
            arrBusinessTypeListTable = self.arrOutcallList
        }else{
            arrBusinessTypeListTable = arrBusinessTypeList
        }
        if self.serType == "I" {
            self.btnIncall.setTitleColor(#colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1), for: .normal)
            self.btnOutcall.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            self.viewBtnIncallBottum.backgroundColor = #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1)
            self.viewBtnOutcallBottum.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        }else{
            self.btnOutcall.setTitleColor(#colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1), for: .normal)
            self.btnIncall.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
             self.viewBtnIncallBottum.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
            self.viewBtnOutcallBottum.backgroundColor = #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1)
        }
        /*self.tblBusinessLIst.reloadData(); self.tblBusinessLIst.contentSize.height = self.tblBusinessLIst.contentSize.height+60*/ self.tblReloadDataForIncallOutcall()
    }
    
    
    
    
    func callWebserviceAddService(dicParam:[String:Any]){
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        objActivity.startActivityIndicator()
        
        objServiceManager.requestPost(strURL: WebURL.addService, params: dicParam, success: { response in
            let  status = response["status"] as? String ?? ""
            if status == "success"{
                self.callWebserviceForUpdateData()
            }else{
                objAppShareData.showAlert(withMessage: (response["message"] as? String ?? ""), type: alertType.bannerDark,on: self)
            }
        }) { error in
            objActivity.stopActivity()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
}


//MARK: - Webservices
fileprivate extension NewServiceListVC{
    func addGesturesToView() -> Void {
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.topToBottomSWIPE))
        swipeRight.direction = .down
        self.viewButtonSet.addGestureRecognizer(swipeRight)
    }
    
    @objc func topToBottomSWIPE() ->Void {
        self.view.endEditing(true)
        self.viewButtonSet.isHidden = true
    }
    @IBAction func btnPlus(_ sender:Any){
        self.viewButtonSet.isHidden = false
    }
    
    @IBAction func btnHiddenViewButtonSet(_ sender:Any){
        self.viewButtonSet.isHidden = true
    }
    
    func callWebserviceForGetBusinessType(){
        arrBusinessType.removeAll()
        objActivity.startActivityIndicator()
        if !objServiceManager.isNetworkAvailable(){
            objActivity.stopActivity()
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        objServiceManager.requestGet(strURL: WebURL.myBusinessType, params: nil, success: { response in
            if  response["status"] as? String == "success"{
                
                if let data = response["data"] as? [[String:Any]]{
                    self.businessTypeCount = data.count
                    for dict in data{
                    let obj = ModelAddBusinessType.init(dict: dict)
                        self.arrBusinessType.append(obj)
                    }
                }
                objActivity.stopActivity()
            }else{
                objActivity.stopActivity()
            }
        }) { error in
            objActivity.stopActivity()
        }
}
}

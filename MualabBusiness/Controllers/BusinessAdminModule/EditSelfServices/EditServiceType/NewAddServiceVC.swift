//
//  NewAddServiceVC.swift
//  MualabBusiness
//
//  Created by Mac on 03/01/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//


import UIKit
import DropDown

class NewAddServiceVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
        //uper labels view
        @IBOutlet weak var viewBookingType:UIView!
        @IBOutlet weak var lblBusinessName:UILabel!
        @IBOutlet weak var lblCategoryName:UILabel!
    
    @IBOutlet weak var lblDescription:UILabel!
    @IBOutlet weak var lblServiceName:UITextField!
    @IBOutlet weak var lblPrice:UITextField!
    @IBOutlet weak var lblTime:UITextField!
    @IBOutlet weak var lblBookingType:UITextField!

    @IBOutlet weak var btnBusinessType:UIButton!
    @IBOutlet weak var btnCategoryType:UIButton!
    @IBOutlet weak var btnServiceName:UIButton!
    @IBOutlet weak var btnBookingType:UIButton!
    @IBOutlet weak var btnPrice:UIButton!
    @IBOutlet weak var btnDescription:UIButton!
    @IBOutlet weak var btnTime:UIButton!

    @IBOutlet weak var tblCompany: UITableView!
    @IBOutlet weak var viewBottumTable: UIView!
    @IBOutlet weak var lblTableHeader: UILabel!
    @IBOutlet weak var HeightViewBottumTable: NSLayoutConstraint!

    
    @IBOutlet weak var imgDropdownBusinessType: UIImageView!
    @IBOutlet weak var imgDropdownCategoryType: UIImageView!

    
        fileprivate var arrBusinessType = [ModelAddBusinessType]()
        fileprivate var arrBookingType = ["Both","Incall","Outcall"]
        fileprivate var arrBusinessTypeCategorye = [ModelAddBusinessType]()
        var businessId = ""
        var fromCategory = false
        var forBusinessType = true
        var forBookingType = true
        let dropDown = DropDown()
        
    
        override func viewDidLoad() {
            super.viewDidLoad()
            self.imgDropdownBusinessType.isHidden = false
            self.imgDropdownCategoryType.isHidden = false
            self.viewConfigure()
            self.tblCompany.delegate = self
            self.tblCompany.dataSource = self
            self.viewBottumTable.isHidden = true
            self.lblBusinessName.text = objAppShareData.objModelFinalSubCategory.param[objAppShareData.objModelFinalSubCategory.bookingName]
            self.lblCategoryName.text = objAppShareData.objModelFinalSubCategory.param[objAppShareData.objModelFinalSubCategory.categoryName]
            self.addGesturesToView()
    }
    func addGesturesToView() -> Void {
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.topToBottomSWIPE))
        swipeRight.direction = .down
        self.viewBottumTable.addGestureRecognizer(swipeRight)
    }
    
    @objc func topToBottomSWIPE() ->Void {
        self.view.endEditing(true)
        self.viewBottumTable.isHidden = true
    }
        
        override func viewWillAppear(_ animated: Bool) {
            
            self.viewBookingType.isHidden = true
            
            let businessType = UserDefaults.standard.string(forKey: UserDefaults.keys.mainServiceType)
            var strType = ""
            if businessType == "1"{
                strType = "Incall"
            }else if businessType == "2"{
                strType = "Outcall"
            }else{
                strType = "Both"
            }
            if strType == "Incall"{
            }else if strType == "Outcall"{
            }else{
                strType = "Both"
                self.viewBookingType.isHidden = false
            }
            self.parseDataInOutlet()
        }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
        
        func viewConfigure(){
            self.callWebserviceForGetBusinessType()
        }
  
    func parseDataInOutlet(){
        let obj = objAppShareData.objModelFinalSubCategory
        self.lblTime.text = obj.param[obj.completionTime]
        self.lblServiceName.text = obj.param[obj.serviceName]
        self.lblDescription.text = obj.param[obj.descriptions]
        self.lblBookingType.text = obj.param[obj.bookingType]
        let incallPrice = obj.param[obj.incallPrice] ?? ""
        let outCallPrice = obj.param[obj.outCallPrice] ?? ""
        if obj.param[obj.bookingType] == "Outcall"{
            self.lblPrice.text = "£" + outCallPrice
        }else if obj.param[obj.bookingType] == "Incall"{
            self.lblPrice.text = "£" + incallPrice
        }else if obj.param[obj.bookingType] == "Both"{
                if incallPrice.count > 0{
                    self.lblPrice.text = "Incall - £" + incallPrice
                    self.lblPrice.text = self.lblPrice.text! + ", "
                    self.lblPrice.text = self.lblPrice.text! + "Outcall - £" + outCallPrice
                }
        }else{
            self.lblPrice.text = "£ 0.00"
        }
            
        ////
//        let doubleStrNN = String(format: "%.2f", ceil(Double(self.lblPrice.text ?? "") ?? 00.00))
//        if doubleStrNN == "00.00"{
//            self.lblPrice.text = ""
//        }else{
//            self.lblPrice.text = doubleStrNN
//        }
        
        ////
        if  obj.param[obj.completionTime] != ""{
            let data =  obj.param[obj.completionTime] ?? "00:00"
            let a = data.components(separatedBy: ":")
            if a.count  > 1{
                if a[0] == "00"{
                    self.lblTime.text = "Completion Time : "+a[1]+" min"
                }else{
                    self.lblTime.text = "Completion Time : "+a[0]+" hr "+a[1]+" min"
                }
            }
        }
        self.buttonHiddenShowManage()
    }
}
extension NewAddServiceVC {
    func buttonHiddenShowManage(){
        let obj = objAppShareData.objModelFinalSubCategory
        self.btnBusinessType = self.buttonWhiteColor(btn: btnBusinessType)
        self.btnCategoryType = self.buttonWhiteColor(btn: btnCategoryType)
        self.btnServiceName = self.buttonWhiteColor(btn: btnServiceName)
        self.btnBookingType = self.buttonWhiteColor(btn: btnBookingType)
        self.btnPrice = self.buttonWhiteColor(btn: btnPrice)
        self.btnDescription = self.buttonWhiteColor(btn: btnDescription)
        self.btnTime = self.buttonWhiteColor(btn: btnTime)
      
        if obj.param[obj.bookingName] != "" && obj.param[obj.bookingName]  != nil {
            self.btnBusinessType = self.buttonClearColor(btn: btnBusinessType)
        }
        if obj.param[obj.categoryName] != "" && obj.param[obj.categoryName]  != nil {
            self.btnCategoryType = self.buttonClearColor(btn: btnCategoryType)
        }
        if obj.param[obj.serviceName] != "" && obj.param[obj.serviceName]  != nil {
            self.btnServiceName = self.buttonClearColor(btn: btnServiceName)
        }
        if obj.param[obj.bookingType] != "" && obj.param[obj.bookingType]  != nil {
            self.btnBookingType = self.buttonClearColor(btn: btnBookingType)
        }
        if self.lblPrice.text == "" || self.lblPrice.text == nil{
        }else{
            self.btnPrice = self.buttonClearColor(btn: btnPrice)
        }
        if obj.param[obj.descriptions] != "" && obj.param[obj.descriptions]  != nil {
            self.btnDescription = self.buttonClearColor(btn: btnDescription)
        }
        if obj.param[obj.completionTime] != "00:00" && obj.param[obj.completionTime]  != nil {
            self.btnTime = self.buttonClearColor(btn: btnTime)
        }
       
    }
    
    func buttonClearColor(btn:UIButton) -> UIButton{
        btn.backgroundColor = UIColor.clear
        btn.setTitleColor(UIColor.clear, for: .normal)
        return btn
    }
    func buttonWhiteColor(btn:UIButton)  -> UIButton{
            btn.backgroundColor = UIColor.white
            btn.setTitleColor(UIColor.black, for: .normal)
            return btn
    }
        

    }
    
    //MARK: - popup and dropdown button
    fileprivate extension NewAddServiceVC{
        func setuoDropDownAppearance()  {
            let appearance = DropDown.appearance()
            appearance.cellHeight = 40
            appearance.backgroundColor = UIColor(white: 1, alpha: 1)
            appearance.selectionBackgroundColor = UIColor(red: 0.6494, green: 0.8155, blue: 1.0, alpha: 0.2)
            appearance.separatorColor = UIColor(white: 0.7, alpha: 0.8)
            appearance.cornerRadius = 10
            appearance.shadowColor = UIColor(white: 0.6, alpha: 1)
            appearance.shadowOpacity = 0.9
            appearance.shadowRadius = 25
            appearance.animationduration = 0.25
            appearance.textColor = .darkGray
        }
        @IBAction func btnBusiness(_ sender: UIButton) {
            if self.arrBusinessType.count == 1 && self.lblBusinessName.text != "" && self.lblBusinessName.text != nil{
                return
            }
            
            self.view.endEditing(true)
            let objGetData = self.arrBusinessType.map { $0.serviceName }
            guard objGetData.isEmpty == false else {
                objAppShareData.showAlert(withMessage: "No business type found", type: alertType.bannerDark,on: self)
                return
            }
            forBookingType = false
            forBusinessType = true
            self.viewBottumTable.isHidden = false
            self.lblTableHeader.text = "Business Types"
            self.HeightViewBottumTable.constant = CGFloat(self.arrBusinessType.count*50)
            self.tblCompany.reloadData()
        }
        @IBAction func btnBack(_ sender: UIButton) {
            objAppShareData.manageNavigation = false
            self.removeDataFromField()
        }
        
        @IBAction func btnContinews(_ sender: UIButton) {
            self.view.endEditing(true)
            self.addArrayData()
        }
        
        @IBAction func btnCategory(_ sender: UIButton) {
            if self.arrBusinessTypeCategorye.count == 1 && self.lblCategoryName.text != "" && self.lblCategoryName.text != nil && self.lblCategoryName.text != "No category type available"{
                return
            }
            if self.lblBusinessName.text == ""{
                objAppShareData.showAlert(withMessage: "first select business type", type: alertType.bannerDark,on: self)
                return
            }
            self.view.endEditing(true)
            let objGetData = self.arrBusinessTypeCategorye.map { $0.subServiceName }
            guard objGetData.isEmpty == false else {
                objAppShareData.showAlert(withMessage: "No category type available", type: alertType.bannerDark,on: self)
                return
            }
            forBookingType = false
            forBusinessType = false
            self.viewBottumTable.isHidden = false
            self.lblTableHeader.text = "Category Types"
            self.HeightViewBottumTable.constant = CGFloat(self.arrBusinessTypeCategorye.count*50)
            self.tblCompany.reloadData()
        }
        
    }
    
    //MARK: - Webservices
    fileprivate extension NewAddServiceVC{
        func callWebserviceForGetBusinessType(){
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            objActivity.startActivityIndicator()
            self.arrBusinessType.removeAll()
            
            objServiceManager.requestGet(strURL: WebURL.myBusinessType, params: nil, success: { response in
                
                
                if  response["status"] as? String == "success"{
                    //self.saveData(dict:response)
                    if let data = response["data"] as? [[String:Any]]{
                        for obj in data{
                            let myObj = ModelAddBusinessType.init(dict: obj)
                            if myObj.status == "1"{
                                self.arrBusinessType.append(myObj)
                            }
                        }
                        if self.arrBusinessType.count == 1{
                            let obj = self.arrBusinessType[0]
                            self.lblBusinessName.text = obj.serviceName
                            self.lblCategoryName.text = ""
                            objAppShareData.objModelFinalSubCategory.param[objAppShareData.objModelFinalSubCategory.bookingName] = self.lblBusinessName.text ?? ""
                            objAppShareData.objModelFinalSubCategory.param[objAppShareData.objModelFinalSubCategory.categoryName] = ""
                            self.buttonHiddenShowManage()
                            self.businessId = obj.serviceId
                            objAppShareData.objModelFinalSubCategory.param[objAppShareData.objModelFinalSubCategory.strBusinessId] = obj.serviceId
                            self.imgDropdownBusinessType.isHidden = true
                            self.callWebserviceForGetServices(strId:obj.serviceId, fromviewwillappear: true)
                        }else{
                            self.imgDropdownBusinessType.isHidden = false
                        }
                        
                        
                    }
                    //self.tblBusinessLIst.reloadData()
                }else{
                    // self.tblBusinessLIst.reloadData()
                    
                }
            }) { error in
                // self.tblBusinessLIst.reloadData()
                
                // objAppShareData.showDeafultAlert(withTitle: message.error.title, message: message.error.wrong, on: self)
                objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
            }
        }
        
        func callWebserviceForGetServices(strId:String,fromviewwillappear:Bool){
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            objActivity.startActivityIndicator()
            let dict = ["categoryId":strId]
            objServiceManager.requestGet(strURL: WebURL.mysubCategoryBusinessId, params: dict as [String : AnyObject], success: { response in
                self.arrBusinessTypeCategorye.removeAll()
                
                if  response["status"] as? String == "success"{
                    //self.saveData(dict:response)
                    if let data = response["data"] as? [[String:Any]]{
                        for obj in data{
                            let myObj = ModelAddBusinessType.init(dict: obj)
                            if myObj.status == "1"{
                                self.arrBusinessTypeCategorye.append(myObj)
                            }
                        }
                        
                        
                        
                        if self.arrBusinessTypeCategorye.count == 1 && fromviewwillappear == true{
                            let obj = self.arrBusinessTypeCategorye[0]
                            self.lblCategoryName.text = obj.subServiceName
                            objAppShareData.objModelFinalSubCategory.param[objAppShareData.objModelFinalSubCategory.bookingName] = self.lblBusinessName.text
                            objAppShareData.objModelFinalSubCategory.param[objAppShareData.objModelFinalSubCategory.categoryName] = self.lblCategoryName.text
                            self.buttonHiddenShowManage()
                            objAppShareData.objModelFinalSubCategory.param[objAppShareData.objModelFinalSubCategory.strCategoryId] = obj.subServiceId
                            self.imgDropdownCategoryType.isHidden = true
                        }else{
                            self.imgDropdownCategoryType.isHidden = false
                        }
                        
                        
                        
                        if self.arrBusinessTypeCategorye.count == 0{
                            self.lblCategoryName.text = "No category available"
                        }
                    }
                    //self.tblBusinessLIst.reloadData()
                }else{
                    self.lblCategoryName.text = "No category available"
                    //self.tblBusinessLIst.reloadData()
                    
                }
            }) { error in
                // self.arrServiceType.reloadData()
                // objAppShareData.showDeafultAlert(withTitle: message.error.title, message: message.error.wrong, on: self)
                objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
                
            }
        }
    }
    
    //MARK: - Button Extension
    fileprivate extension NewAddServiceVC{
        
        @IBAction func btnBookingName(_ sender:Any){
            self.gotoNextVC(strType: "Name")
        }
        
        @IBAction func btnBookingType(_ sender:Any){
            //self.gotoNextVC(strType: "Booking")
            self.view.endEditing(true)
            //let objGetData = self.arrBusinessType.map { $0.serviceName }
            //guard objGetData.isEmpty == false else {
//                objAppShareData.showAlert(withMessage: "No business type found", type: alertType.bannerDark,on: self)
                //return
            //}
            forBookingType = true
            forBusinessType = false
            self.viewBottumTable.isHidden = false
            self.lblTableHeader.text = "Booking Types"
            self.HeightViewBottumTable.constant = CGFloat(3*50)
            self.tblCompany.reloadData()
        }
        
        @IBAction func btnPrice(_ sender:Any){
            self.gotoNextVC(strType: "Price")
        }
        
        @IBAction func btnDescription(_ sender:Any){
            self.gotoNextVC(strType: "Description")
        }
        
        @IBAction func btnTime(_ sender:Any){
           // self.gotoNextVC(strType: "Time")
            if self.lblBusinessName.text == "" || self.lblBusinessName.text == nil {
                objAppShareData.showAlert(withMessage: "Please select business type", type: alertType.bannerDark,on: self)
            }else if  self.lblCategoryName.text == "" || self.lblCategoryName.text == "No category available" || self.lblCategoryName.text == nil {
                objAppShareData.showAlert(withMessage: "Please select category type", type: alertType.bannerDark,on: self)
            }else{                
                let sb = UIStoryboard(name:"SignUpProcess",bundle:Bundle.main)
                let objChooseType = sb.instantiateViewController(withIdentifier:"AddServiceTimeVC") as! AddServiceTimeVC
                //objChooseType.viewType = strType
                self.navigationController?.pushViewController(objChooseType, animated: true)
            }
        }
        
        func gotoNextVC(strType:String){
            
            if self.lblBusinessName.text == "" {
                objAppShareData.showAlert(withMessage: "Please select business type", type: alertType.bannerDark,on: self)
            }else if  self.lblCategoryName.text == "" || self.lblCategoryName.text == "No category available"{
                objAppShareData.showAlert(withMessage: "Please select category type", type: alertType.bannerDark,on: self)
            }else{
                let sb = UIStoryboard(name:"BusinessAdmin",bundle:Bundle.main)
                let objChooseType = sb.instantiateViewController(withIdentifier:"NewAddServiceFieldVC") as! NewAddServiceFieldVC
                objChooseType.viewType = strType
                self.navigationController?.pushViewController(objChooseType, animated: true)
            }
        }
        
        func addArrayData(){
            
            let obj = objAppShareData.objModelFinalSubCategory
            
            
            obj.param[obj.bookingName] = self.lblBusinessName.text ?? ""
            let businessName = obj.param[obj.bookingName] ?? ""
            
            obj.param[obj.categoryName] = self.lblCategoryName.text ?? ""
            let catName = obj.param[obj.categoryName] ?? ""
            
            let servName = obj.param[obj.serviceName] ?? ""
            let busId = obj.param[obj.strBusinessId] ?? ""
            let catId = obj.param[obj.strCategoryId] ?? ""
            let bukType = obj.param[obj.bookingType] ?? ""
            var icPrice = obj.param[obj.incallPrice] ?? "0.0"
            var ocPrice = obj.param[obj.outCallPrice] ?? "0.0"
            let desc = obj.param[obj.descriptions] ?? ""
            let time = obj.param[obj.completionTime] ?? ""
            
            if icPrice == ""{
                icPrice = "0.0"
            }
            if ocPrice == ""{
                ocPrice = "0.0"
            }
            
            var mainPrice = icPrice
            if bukType == "Outcall"{
                mainPrice = ocPrice
            }
            
            let icPriceDouble = Double(icPrice) ?? 0.0
            let ocPriceDouble = Double(ocPrice) ?? 0.0
            
            if businessName == ""{
                self.alert(str: "Please select business type")
            }else if catName == "" || catName == "No category available"{
                self.alert(str: "Please select category")
            }else if servName == ""{
                self.alert(str: "Please enter service name")
            }else if bukType == ""{
                self.alert(str: "Please select booking type")
            }else if bukType == "Both" && ((icPrice == "0.00" || ocPrice == "0.00") ||  (icPrice == "0.0" || ocPrice == "0.0")){
                self.alert(str: "Please enter price")
            }else if bukType == "Both" && (icPrice == "" || ocPrice == ""){
                self.alert(str: "Please enter price")
            }else if bukType == "Both" && (icPrice == ""  || ocPrice == ""  || ocPrice == "0.00" || icPrice == "0.0"  || ocPrice == "0.0"){
                self.alert(str: "Please enter price")
            }else if bukType == "Both" && icPrice == "0.00"  && ocPrice != ""{
                self.alert(str: "Please enter price")
            
            }else if bukType == "Both" && (icPriceDouble < 5.0 || ocPriceDouble < 5.0){
                self.alert(str: "Please enter a amount that is no lesser than £5")
           
            }else if bukType == "Incall" && (icPrice == "0.00" || icPrice == "" || icPrice == "0.0" ){
                self.alert(str: "Please enter incall price")
            }else if bukType == "Incall" && (icPriceDouble < 5.0){
                self.alert(str: "Please enter a amount that is no lesser than £5")
            
            }else if bukType == "Outcall" && ( ocPrice == "0.00" || ocPrice == "" || ocPrice == "0.0"){
                self.alert(str: "Please enter outcall price")
            }else if bukType == "Outcall" && (ocPriceDouble < 5.0){
                self.alert(str: "Please enter a amount that is no lesser than £5")
            
            }else if desc == ""{
                self.alert(str: "Please enter description")
            }else if time == ""{
                self.alert(str: "Please select completion time")
            }else{
                
                if icPrice != ""{
                    let aPrice  = float_t(icPrice) ?? 0.0
                    icPrice = String(aPrice)
                }
                if ocPrice != ""{
                    let aPrice  = float_t(ocPrice) ?? 0.0
                    ocPrice = String(aPrice)
                }
                
                let a = [["_id":catId,
                          "mainBookingId":busId,
                          "mainCategoryId":catId,
                          "serviceName":servName,
                          "price":mainPrice,
                          "incallPrice":icPrice,
                          "outcallPrice":ocPrice,
                          "bookingType":bukType,
                          "describe":desc,
                          "time":time]]
                let mainDict = [ "serviceId":busId,
                "subserviceId":catId,
                "title":servName,
                "description":desc,
                "inCallPrice":icPrice,
                "outCallPrice":ocPrice,
                "completionTime":time,
                "id":""]
                callWebserviceAddService(dicParam: mainDict)
                
            }
        }
        
        func callWebserviceAddService(dicParam:[String:Any]){
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            objActivity.startActivityIndicator()
            
            objServiceManager.requestPost(strURL: WebURL.addService, params: dicParam, success: { response in
                let  status = response["status"] as? String ?? ""
                var goBack = false
                if status == "success"{
                    let controllers = self.navigationController?.viewControllers
                    for vc in controllers! {
                        if vc is  NewServiceListVC{
                            // navigationCheck = true
                            goBack = true
                            _ = self.navigationController?.popToViewController(vc as! NewServiceListVC, animated: true)
                        }
                    }
                    if goBack == false {
                        self.GotoServiceVC()
                    }
                 }else{
                    objAppShareData.showAlert(withMessage: "The service name already exist", type: alertType.bannerDark,on: self)
                }
            }) { error in
                objActivity.stopActivity()
                objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
            }
        }
        func alert(str:String){
            objAppShareData.showAlert(withMessage: str, type: alertType.bannerDark,on: self)
        }
    }
    
     
    //MARK: - gate data in globle array from local data base
    extension NewAddServiceVC{
        
        
        
        func removeDataFromField(){
            //objAppShareData.objModelFinalSubCategory.param = ["":""]
            self.navigationController?.popViewController(animated: true)
        }
        func GotoServiceVC(){
            objAppShareData.objModelFinalSubCategory.param = ["":""]
            let sb = UIStoryboard(name:"BusinessAdmin",bundle:Bundle.main)
            objAppShareData.manageNavigation = false
            
            let objChooseType = sb.instantiateViewController(withIdentifier:"NewServiceListVC") as! NewServiceListVC
            self.navigationController?.pushViewController(objChooseType, animated: true)
        }
}

//MARK: - table view method extension
extension NewAddServiceVC{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if forBusinessType == true{
            return self.arrBusinessType.count
        }else if forBookingType == true{
            return self.arrBookingType.count
        }else{
            return self.arrBusinessTypeCategorye.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "CellBottumTableList"
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? CellBottumTableList{
            if forBusinessType == true{
                let obj = self.arrBusinessType[indexPath.row]
                cell.lblTitle.text = obj.serviceName
            }else if forBookingType == true{
                cell.lblTitle.text = self.arrBookingType[indexPath.row]
            }else{
                let obj = self.arrBusinessTypeCategorye[indexPath.row]
                   cell.lblTitle.text = obj.subServiceName
            }
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
          self.viewBottumTable.isHidden = true
        
        if forBusinessType == true{
                        let obj = self.arrBusinessType[indexPath.row]
                        self.lblBusinessName.text = obj.serviceName
                        self.lblCategoryName.text = ""
                        objAppShareData.objModelFinalSubCategory.param[objAppShareData.objModelFinalSubCategory.bookingName] = self.lblBusinessName.text ?? ""
                        objAppShareData.objModelFinalSubCategory.param[objAppShareData.objModelFinalSubCategory.categoryName] = ""
                        self.buttonHiddenShowManage()
                        self.businessId = obj.serviceId
                        objAppShareData.objModelFinalSubCategory.param[objAppShareData.objModelFinalSubCategory.strBusinessId] = obj.serviceId
            self.callWebserviceForGetServices(strId:obj.serviceId, fromviewwillappear: false)
        }else if forBookingType == true{
            self.viewBottumTable.isHidden = true
            self.lblBookingType.text = self.arrBookingType[indexPath.row]
            let a = objAppShareData.objModelFinalSubCategory
            a.param[a.outCallPrice]  =  ""
            a.param[a.incallPrice] = ""
            objAppShareData.objModelFinalSubCategory.param[objAppShareData.objModelFinalSubCategory.bookingType] = self.lblBookingType.text ?? ""
        }else{
                        let obj = self.arrBusinessTypeCategorye[indexPath.row]
                        self.lblCategoryName.text = obj.subServiceName
                        objAppShareData.objModelFinalSubCategory.param[objAppShareData.objModelFinalSubCategory.bookingName] = self.lblBusinessName.text
                        objAppShareData.objModelFinalSubCategory.param[objAppShareData.objModelFinalSubCategory.categoryName] = self.lblCategoryName.text
                        self.buttonHiddenShowManage()
                        objAppShareData.objModelFinalSubCategory.param[objAppShareData.objModelFinalSubCategory.strCategoryId] = obj.subServiceId
        }
        
    }
        

    
  
    
    @IBAction func btnHiddenBottumTable(_ sender: UIButton) {
        self.viewBottumTable.isHidden = true
       
    }
}

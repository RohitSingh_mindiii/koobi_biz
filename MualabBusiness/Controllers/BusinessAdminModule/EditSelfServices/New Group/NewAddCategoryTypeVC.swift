//
//  NewAddCategoryTypeVC.swift
//  MualabBusiness
//
//  Created by Mac on 03/01/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit

class NewAddCategoryTypeVC:  UIViewController,UITableViewDelegate,UITableViewDataSource {
    
        fileprivate var arrBusinessType = [ModelAddBusinessType]()
        var cellSelectId = ""
        var businessId = ""
        var artistId = ""
        var screenNo = 0
    
        @IBOutlet weak var tblBusinessLIst:UITableView!
        @IBOutlet weak var lblNoDataFound:UIView!
    

    override func viewDidLoad() {
            super.viewDidLoad()
            self.cellSelectId = ""
           
            self.tblBusinessLIst.delegate = self
            self.tblBusinessLIst.dataSource = self
            self.tblBusinessLIst.reloadData()
            self.manageVCRedirection()
            // Do any additional setup after loading the view.
        }
        
        override func viewWillAppear(_ animated: Bool) {
            self.cellSelectId = ""
            let userInfo = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]  ?? ["":""]
            self.artistId = userInfo[UserDefaults.keys.id] as? String ?? ""
            self.callWebserviceForGetServices()
        }
    
    func manageVCRedirection(){
        if screenNo == 4{
            screenNo = 0
            let sb = UIStoryboard(name:"BusinessAdmin",bundle:Bundle.main)
            let objChooseType = sb.instantiateViewController(withIdentifier:"NewSearchCategoryTypeVC") as! NewSearchCategoryTypeVC
            objChooseType.idBusiness = self.businessId
            self.navigationController?.pushViewController(objChooseType, animated: false)
        }
        
    }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
        
        
    }
    //MARK: - Webservices
    fileprivate extension NewAddCategoryTypeVC{
        internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            if arrBusinessType.count > 0{
                self.lblNoDataFound.isHidden = true
            }else{
                self.lblNoDataFound.isHidden = false
            }
            return arrBusinessType.count
        }
        
        internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "BusinessTypeTableCell") as! BusinessTypeTableCell
            let objSer = arrBusinessType[indexPath.row]
            cell.lblServiceName.text = objSer.subServiceName.capitalized
            if objSer.status == "0"{
                cell.viewStatusManage.isHidden = false
            }else{
                cell.viewStatusManage.isHidden = true
            }
            
            if self.cellSelectId == objSer.id{
                cell.imgCheck.isHidden = true
                cell.lblServiceName.textColor = appColor
                
            }else{
                cell.imgCheck.isHidden = true
                cell.lblServiceName.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                
            }
            
            cell.btnChackUnchack.tag = indexPath.row
            cell.btnChackUnchack.addTarget(self, action:#selector(btnChackUnchackAction(sender:)) , for: .touchUpInside)
            
            return cell
        }
        
        @objc func btnChackUnchackAction(sender: UIButton!){
            self.view.endEditing(true)
            let objMyEventsData = self.arrBusinessType[sender.tag]
            
            let indexPath = IndexPath(row:(sender as AnyObject).tag, section: (sender as AnyObject).superview!!.tag)
            let cell = (tblBusinessLIst?.cellForRow(at: indexPath) as? BusinessTypeTableCell)!
            if objMyEventsData.status != "0"{
                if self.cellSelectId == objMyEventsData.id{
                    self.cellSelectId = ""
                }else{
                    self.cellSelectId = objMyEventsData.id
                }
                self.tblBusinessLIst.reloadData()
            }else{
                objAppShareData.showAlert(withMessage: "Your category request is pending", type: alertType.bannerDark,on: self)
            }
            objAppShareData.objModelFinalSubCategory.param[objAppShareData.objModelFinalSubCategory.categoryName] = objMyEventsData.subServiceName
            objAppShareData.objModelFinalSubCategory.param[objAppShareData.objModelFinalSubCategory.strCategoryId] = objMyEventsData.subServiceId
                self.continueWork()
        }
        internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
            //        let sb = UIStoryboard(name:"SignUpProcess",bundle:Bundle.main)
            //        let objChooseType = sb.instantiateViewController(withIdentifier:"CategoriesVC") as! CategoriesVC
            //objChooseType.selectedIndex = indexPath.row
            //objChooseType.callOption = self.callOption
            // self.navigationController?.pushViewController(objChooseType, animated: true)
        }
        @available(iOS 11.0, *)
        
        internal func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
        {
            let Delete = UIContextualAction(style: .normal, title:  "", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
                let objSer = self.arrBusinessType[indexPath.row]
                let businessIds = objSer.subServiceId
                self.deleteCell(mainBookingId: self.businessId, categoryId: businessIds)
                success(true)
            })
            Delete.image = #imageLiteral(resourceName: "delete")
            Delete.backgroundColor =  #colorLiteral(red: 0.9137254902, green: 0.2117647059, blue: 0.2274509804, alpha: 1)
            return UISwipeActionsConfiguration(actions: [Delete])
        }
        
    }
    
    //MARK: - Webservices
    fileprivate extension NewAddCategoryTypeVC{
        @IBAction func btnContinew(_ sender:Any){
            self.continueWork()
        }
        func continueWork(){
            if self.cellSelectId == ""{
                objAppShareData.showAlert(withMessage: "Please select any category", type: alertType.bannerDark,on: self)
            }else{
                let sb = UIStoryboard(name:"BusinessAdmin",bundle:Bundle.main)
                let objChooseType = sb.instantiateViewController(withIdentifier:"NewServiceListVC") as! NewServiceListVC
                self.navigationController?.pushViewController(objChooseType, animated: true)
            }
        }
        
        @IBAction func btnBack(_ sender:Any){
            self.popToViewContruller()
        }
        
         
        
        @IBAction func btnAddBusinessType(_ sender:Any){
            let sb = UIStoryboard(name:"BusinessAdmin",bundle:Bundle.main)
            let objChooseType = sb.instantiateViewController(withIdentifier:"NewSearchCategoryTypeVC") as! NewSearchCategoryTypeVC
            objChooseType.idBusiness = self.businessId
            self.navigationController?.pushViewController(objChooseType, animated: true)
        }
        
        @IBAction func btnAddCategoryType(_ sender:Any){
            let businessType = UserDefaults.standard.string(forKey: UserDefaults.keys.mainServiceType)
            let newObj = objAppShareData.objModelFinalSubCategory
            
            let businessName = newObj.param[newObj.bookingName]
            let CategoryName = newObj.param[newObj.categoryName]
            let businessId = newObj.param[newObj.strBusinessId]
            let CategoryId = newObj.param[newObj.strCategoryId]
            let a = objAppShareData.objModelFinalSubCategory
            a.param = ["":""]
            var strType = ""
            if businessType == "1"{
                strType = "Incall"
            }else if businessType == "2"{
                strType = "Outcall"
            }else{
                strType = "Both"
                //self.viewBookingType.isHidden = true
            }
            a.param[a.bookingType] = strType
            a.param[a.bookingName] = businessName
            a.param[a.categoryName] = CategoryName
            a.param[a.strBusinessId] = businessId
            a.param[a.strCategoryId] = CategoryId

            
            if self.cellSelectId == ""{
                objAppShareData.showAlert(withMessage: "Please select any category", type: alertType.bannerDark,on: self)
            }else{
                let sb = UIStoryboard(name:"BusinessAdmin",bundle:Bundle.main)
                let objChooseType = sb.instantiateViewController(withIdentifier:"NewAddServiceVC") as! NewAddServiceVC
                objChooseType.fromCategory = true
                self.navigationController?.pushViewController(objChooseType, animated: true)
            }
        }
    }
    
    //MARK: - Webservices
    fileprivate extension NewAddCategoryTypeVC{
        func callWebserviceForGetServices(){
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            objActivity.startActivityIndicator()
            let dict = ["categoryId":self.businessId]
            objServiceManager.requestGet(strURL: WebURL.mysubCategoryBusinessId, params: dict as [String : AnyObject], success: { response in
                self.arrBusinessType.removeAll()

                if  response["status"] as? String == "success"{
                    //self.saveData(dict:response)
                    if let data = response["data"] as? [[String:Any]]{
                        for obj in data{
                            let myObj = ModelAddBusinessType.init(dict: obj)
                            self.arrBusinessType.append(myObj)
                        }
                    }
                    self.tblBusinessLIst.reloadData()
                }else{
                    self.tblBusinessLIst.reloadData()
                    
                }
            }) { error in
                self.tblBusinessLIst.reloadData()
                // objAppShareData.showDeafultAlert(withTitle: message.error.title, message: message.error.wrong, on: self)
                objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
            }
        }
    }
    //MARK: - Navigation method
    extension NewAddCategoryTypeVC{
        func popToViewContruller(){
            let controllers = self.navigationController?.viewControllers
            for vc in controllers! {
                if vc is NewAddBusinessTypeVC {
                    _ = self.navigationController?.popToViewController(vc as! NewAddBusinessTypeVC, animated: true)
                }
            }
        }
        
        
        
        func deleteCell(mainBookingId:String,categoryId:String){
             checkBackEndResponce(Ids:categoryId)
        }
        func checkBackEndResponce(Ids:String){
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            objActivity.startActivityIndicator()
            let dict = ["id":Ids,"artistId":self.artistId,"type":"category"]
            objServiceManager.requestPost(strURL: WebURL.bookingCheck, params: dict as [String : AnyObject], success: { response in
                if  response["status"] as? String == "success"{
                    let message = response["message"] as? String ?? ""
                    if message == "Booking not exist"{
                        self.callWebserviceDeleteServices(Ids:Ids)
                    }else{
                        objAppShareData.showAlert(withMessage:  "You can't delete this category, currently it is used by customer for booking.", type: alertType.bannerDark,on: self)
                    }
                }else{
                    objAppShareData.showAlert(withMessage:  response["message"] as? String ?? "", type: alertType.bannerDark,on: self)
                }
            }) { error in
                self.tblBusinessLIst.reloadData()
                objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
            }
        }
        
        
        
        
        func callWebserviceDeleteServices(Ids:String){
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            objActivity.startActivityIndicator()
            let dict = ["id":Ids]
            objServiceManager.requestPost(strURL: WebURL.deleteCategory, params: dict as [String : AnyObject], success: { response in
                self.arrBusinessType.removeAll()
                
                if  response["status"] as? String == "success"{
                    self.callWebserviceForGetServices()
                }else{
                    self.tblBusinessLIst.reloadData()
                }
            }) { error in
                self.tblBusinessLIst.reloadData()
                objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
            }
        }
}


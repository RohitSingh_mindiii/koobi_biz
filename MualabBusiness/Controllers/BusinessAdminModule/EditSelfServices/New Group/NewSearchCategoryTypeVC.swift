//
//  NewSearchCategoryTypeVC.swift
//  MualabBusiness
//
//  Created by Mac on 03/01/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit

class NewSearchCategoryTypeVC:  UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {
        @IBOutlet weak var indicate: UIActivityIndicatorView!
        
        fileprivate var arrBusinessType = [ModelAddBusinessType]()
        var idBusiness = ""
        var cellSelectId = ""
        fileprivate var strText = ""
        @IBOutlet weak var tblBusinessLIst:UITableView!
        @IBOutlet weak var ViewNoDataFound:UIView!
        @IBOutlet weak var txtSearchName:UITextField!
        @IBOutlet weak var lblNoDataFound:UIView!
        @IBOutlet weak var btnContinew:UIButton!
        
        override func viewDidLoad() {
            super.viewDidLoad()
            self.indicate.stopAnimating()
            
            self.txtSearchName.delegate = self
            self.tblBusinessLIst.delegate = self
            self.tblBusinessLIst.dataSource = self
            self.cellSelectId = ""
            self.tblBusinessLIst.reloadData()
            
            
            // Do any additional setup after loading the view.
        }
        
        override func viewWillAppear(_ animated: Bool) {
            self.cellSelectId = ""
            self.indicate.stopAnimating()
             
            self.callWebserviceForGetServices(param: ["search":self.strText,"categoryId":self.idBusiness])
        }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
    }
    extension NewSearchCategoryTypeVC:UIScrollViewDelegate{
        func scrollViewDidScroll(_ scrollView: UIScrollView){
            self.view.endEditing(true)
        }
    }
    //MARK: - Webservices
    fileprivate extension NewSearchCategoryTypeVC{
        
        
        @IBAction func btnBack(_ sender:Any){
            self.backToLast()
        }
        
        func backToLast(){
            self.navigationController?.popViewController(animated: true)
        }
        func backToNewVC(){
//            let sb = UIStoryboard(name:"BusinessAdmin",bundle:Bundle.main)
//            let objChooseType = sb.instantiateViewController(withIdentifier:"NewAddCategoryTypeVC") as! NewAddCategoryTypeVC
//            objChooseType.businessId = self.idBusiness
            self.navigationController?.popViewController(animated: true)
        }
        @IBAction func btnContinew(_ sender:Any){
        self.continueWork()
        }
        func continueWork(){
            var name = ""
            if self.ViewNoDataFound.isHidden == false{
                if self.txtSearchName.text == "" {
                    objAppShareData.showAlert(withMessage: "Please enter any category", type: alertType.bannerDark,on: self)
                }else{
                    name = self.txtSearchName.text ?? ""
                }
            }else{
                
                if self.cellSelectId == "" && self.txtSearchName.text != "" {
                    name = self.txtSearchName.text ?? ""
                }else if self.cellSelectId != ""{
                    for obj in self.arrBusinessType{
                        if self.cellSelectId == obj.id{
                            name = obj.title
                        }
                    }
                }else if self.txtSearchName.text != ""  {
                    name = self.txtSearchName.text ?? ""
                }else{
                    objAppShareData.showAlert(withMessage: "Please select any category", type: alertType.bannerDark,on: self)
                }
            }
            if name != ""{
                self.callWebserviceForAddBusiness(param: ["title":name,"categoryId":self.idBusiness])
            }
            
        }
        @IBAction func btnAdd(_ sender:Any){
            
            self.txtSearchName.text = txtSearchName.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            if (self.txtSearchName.text?.count)! > 0 {
                let str = self.txtSearchName.text ?? ""
                self.txtSearchName.text = ""
                self.callWebserviceForAddBusiness(param: ["title":str,"categoryId":self.idBusiness])
            }
        }
    }
    
    // MARK: - UITextfield Delegate
    extension NewSearchCategoryTypeVC{
        
        func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
            
            let text = textField.text! as NSString
            
            if (text.length == 1)  && (string == "") {
                NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
                self.strText = ""
                getArtistDataWith(andSearchText: "")
            }else{
                var substring: String = textField.text!
                substring = (substring as NSString).replacingCharacters(in: range, with: string)
                substring = substring.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                let a = substring.count
                if a > 50 && substring != ""{
                    return false
                }else{
                    searchAutocompleteEntries(withSubstring: substring)
                }
            }
            return true
        }
        
        func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            textField.resignFirstResponder()
            return true
        }
        
        func textFieldShouldClear(_ textField: UITextField) -> Bool {
            self.view.endEditing(true)
            
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
            self.strText = ""
            getArtistDataWith( andSearchText: "")
            return true
        }
        
        // MARK: - searching operation
        func searchAutocompleteEntries(withSubstring substring: String) {
            if substring != "" {
                self.strText = substring
                // to limit network activity, reload half a second after last key press.
                NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
                self.perform(#selector(self.reload), with: nil, afterDelay: 0.5)
            }
        }
        
        @objc func reload() {
            self.getArtistDataWith( andSearchText: strText)
        }
        
        func getArtistDataWith(andSearchText: String) {
            self.callWebserviceForGetServices(param: ["search": andSearchText.lowercased() ,"categoryId":self.idBusiness])
        }
    }
    
    //MARK: - Webservices
    extension NewSearchCategoryTypeVC{
        internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            // DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(100)) {
            self.btnContinew.isHidden = false
            
            if self.arrBusinessType.count > 0{
                self.ViewNoDataFound.isHidden = true
                self.lblNoDataFound.isHidden = true
                
            }else if self.strText == ""{
                self.ViewNoDataFound.isHidden = true
                self.lblNoDataFound.isHidden = false
                self.btnContinew.isHidden = true
                
            }else if self.strText != ""{
                self.ViewNoDataFound.isHidden = false
                self.lblNoDataFound.isHidden = true
                self.btnContinew.isHidden = true
            }else{
                self.ViewNoDataFound.isHidden = true
                self.lblNoDataFound.isHidden = false
            }
            
            // }
            return arrBusinessType.count
        }
        
        internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "BusinessTypeTableCell") as! BusinessTypeTableCell
            let objSer = arrBusinessType[indexPath.row]
            cell.lblServiceName.text = objSer.title
            
            if self.cellSelectId == objSer.id{
                cell.imgCheck.isHidden = false
                cell.lblServiceName.textColor = appColor
            }else{
                cell.imgCheck.isHidden = true
                cell.lblServiceName.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            }
            
            cell.btnChackUnchack.tag = indexPath.row
            cell.btnChackUnchack.addTarget(self, action:#selector(btnChackUnchackAction(sender:)) , for: .touchUpInside)
            
            return cell
        }
        
        @objc func btnChackUnchackAction(sender: UIButton!){
            self.view.endEditing(true)
            let objMyEventsData = self.arrBusinessType[sender.tag]
            
            let indexPath = IndexPath(row:(sender as AnyObject).tag, section: (sender as AnyObject).superview!!.tag)
            let cell = (tblBusinessLIst?.cellForRow(at: indexPath) as? BusinessTypeTableCell)!
            
            if self.cellSelectId == objMyEventsData.id{
                self.cellSelectId = ""
            }else{
                self.cellSelectId = objMyEventsData.id
            }
            self.tblBusinessLIst.reloadData()
            self.continueWork()

        }
        
        internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
            //        let sb = UIStoryboard(name:"SignUpProcess",bundle:Bundle.main)
            //        let objChooseType = sb.instantiateViewController(withIdentifier:"CategoriesVC") as! CategoriesVC
            //objChooseType.selectedIndex = indexPath.row
            //objChooseType.callOption = self.callOption
            // self.navigationController?.pushViewController(objChooseType, animated: true)
        }
    }
    //MARK: - Webservices
    fileprivate extension NewSearchCategoryTypeVC{
        func callWebserviceForGetServices(param:[String:Any]){
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            //objActivity.startActivityIndicator()
            self.indicate.startAnimating()
            
            
            let dict = ["categoryId":self.idBusiness,"search":self.strText]
            objServiceManager.requestGet(strURL: WebURL.allSubcategorySearchBusinessId, params: dict as [String : AnyObject], success: { response in
                self.arrBusinessType.removeAll()
                if  response["status"] as? String == "success"{
                    //self.saveData(dict:response)
                    if let data = response["data"] as? [[String:Any]]{
                        for obj in data{
                            let myObj = ModelAddBusinessType.init(dict: obj)
                            self.arrBusinessType.append(myObj)
                        }
                    }
                    self.tblBusinessLIst.reloadData()
                    self.indicate.stopAnimating()
                    
                }else{
                    self.tblBusinessLIst.reloadData()
                    self.indicate.stopAnimating()
                    
                }
            }) { error in
                self.tblBusinessLIst.reloadData()
                self.indicate.stopAnimating()
                
                // objAppShareData.showDeafultAlert(withTitle: message.error.title, message: message.error.wrong, on: self)
                objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
            }
        }
        
        
        func callWebserviceForAddBusiness(param:[String:Any]){
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            //   objActivity.startActivityIndicator()
            self.indicate.startAnimating()
            
            objServiceManager.requestPost(strURL: WebURL.addSubCategory, params: param as [String : AnyObject], success: { response in
                // self.arrBusinessType.removeAll()
                self.indicate.stopAnimating()
                
                if  response["status"] as? String == "success"{
                    let msg = response["message"] as? String ?? ""
                    
                    self.backToNewVC()
                    objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                }else{
                    let msg = response["message"] as? String ?? ""
                    
                    objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                    self.callWebserviceForGetServices(param:[:])
                    self.tblBusinessLIst.reloadData()
                }
            }) { error in
                self.tblBusinessLIst.reloadData()
                // objAppShareData.showDeafultAlert(withTitle: message.error.title, message: message.error.wrong, on: self)
                objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
            }
        }
}



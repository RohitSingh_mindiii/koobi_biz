//
//  CellHolidayList.swift
//  MualabBusiness
//
//  Created by Mindiii on 2/19/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit

class CellHolidayList: UITableViewCell {
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblArtistName: UILabel!
    @IBOutlet weak var lblHolidayDay: UILabel!
    @IBOutlet weak var lblHolidayFromToDate: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblHolidayAlreadyGivenDay: UILabel!
    @IBOutlet weak var btnProfile: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
class CellBottumTableList: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgCompany: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if isSelected == true{
            self.lblTitle.textColor = appColor
        }else{
            self.lblTitle.textColor = UIColor.black
        }
    }
}

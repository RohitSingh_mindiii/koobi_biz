//
//  ModelHoliday.swift
//  MualabBusiness
//
//  Created by Mindiii on 2/19/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit

class ModelHoliday: NSObject {
    var __v = ""
    var _id = ""
    var businessId = ""
    var crd = ""
    var endDate = ""
    var holiday = ""
    var holidayGiven = ""
    var reason = ""
    var staffId = ""
    var startDate = ""
    var status = ""
    var upd = ""
    var AlreadyGivenHollyday = 0
    var arrStaffInfo = [UserDetail]()
    
    init(dict : [String : Any]){
        
        _id = String(dict["_id"] as? Int ?? 0)
        if let id = dict["_id"] as? String{
            _id = id
        }
        
        __v = String(dict["__v"] as? Int ?? 0)
        if let id = dict["__v"] as? String{
            __v = id
        }
        
        businessId = String(dict["businessId"] as? Int ?? 0)
        if let id = dict["businessId"] as? String{
            businessId = id
        }
        
        if let id = dict["endDate"] as? String{
            endDate = id
        }
        
        holiday = String(dict["holiday"] as? Int ?? 0)
        if let id = dict["holiday"] as? String{
            holiday = id
        }
        
         holidayGiven = String(dict["holidayGiven"] as? Int ?? 0)
        if let id = dict["holidayGiven"] as? String{
            holidayGiven = id
        }
 
         if let id = dict["reason"] as? String{
            reason = id
        }
        
 
        staffId = String(dict["staffId"] as? Int ?? 0)
        if let id = dict["staffId"] as? String{
            staffId = id
        }
 
         if let id = dict["startDate"] as? String{
            startDate = id
        }
 
        status = String(dict["status"] as? Int ?? 0)
        if let id = dict["status"] as? String{
            status = id
        }
 
        if let id = dict["upd"] as? String{
            upd = id
        }
        if let data = dict["staffInfo"] as? [[String:Any]]{
            for dict2 in data{
                let obj = UserDetail.init(dict: dict2)
                self.arrStaffInfo.append(obj)
            }
        }
        
        if let arrHoliday = dict["holidayGiven"] as? [[String:Any]]{
            for objNew in arrHoliday{
                if let pbj = objNew["holidayGiven"] as? Int{
                    AlreadyGivenHollyday = AlreadyGivenHollyday+pbj
                }
            }
        }
        
        
        crd = dict["crd"] as? String ?? ""
        crd = objAppShareData.crtToDateString(crd: crd)
    }
    
}

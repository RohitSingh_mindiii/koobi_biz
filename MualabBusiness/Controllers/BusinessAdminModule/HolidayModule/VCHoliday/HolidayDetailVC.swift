//
//  HolidayDetailVC.swift
//  MualabBusiness
//
//  Created by Mindiii on 2/16/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit

class HolidayDetailVC: UIViewController {
    
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var lblDay:UILabel!
    @IBOutlet weak var lblDate:UILabel!
    @IBOutlet weak var lblReason:UILabel!
    @IBOutlet weak var imgUser:UIImageView!
    
    var objHolidayDetail =  ModelHoliday(dict: ["":""])
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if self.objHolidayDetail.arrStaffInfo.count > 0{
            let objStaff =  self.objHolidayDetail.arrStaffInfo[0]
            let url = URL(string: objStaff.profileImage )
            if  url != nil {
                //self.imgUser.af_setImage(withURL: url!)
                self.imgUser.sd_setImage(with: url!, placeholderImage: UIImage(named: "cellBackground"))
            }else{
                self.imgUser.image = #imageLiteral(resourceName: "cellBackground")
            }
            self.lblName.text  = objStaff.userName.capitalized
        }
        
        
        let startDate = objAppShareData.currentToRequiredDate(date: objHolidayDetail.startDate, current: "yyyy-MM-dd", required: "dd/MM/yyyy")
        let endDare = objAppShareData.currentToRequiredDate(date: objHolidayDetail.endDate, current: "yyyy-MM-dd", required: "dd/MM/yyyy")
 
        self.lblReason.text = objHolidayDetail.reason
        self.lblDate.text = "From - "+startDate+"   To - "+endDare
        self.lblDay.text = "Holidays Given - "+objHolidayDetail.holidayGiven

    }
    
    @IBAction func btnBackAction(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
}

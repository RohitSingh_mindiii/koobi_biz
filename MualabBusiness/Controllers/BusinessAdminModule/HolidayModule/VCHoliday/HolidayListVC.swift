//
//  HolidayListVC.swift
//  MualabBusiness
//
//  Created by Mindiii on 2/19/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import DropDown

class HolidayListVC: UIViewController ,UITableViewDelegate,UITableViewDataSource{
    @IBOutlet weak var tblHolidayList:UITableView!
    @IBOutlet weak var lblNoDataFound:UILabel!
    @IBOutlet weak var lblStaffName:UILabel!

    @IBOutlet weak var viewStaffList: UIView!
    @IBOutlet weak var tblStaffList: UITableView!
    @IBOutlet weak var HeightViewBottumTable: NSLayoutConstraint!

    var strSelecrStaffId = ""
    
    
    var arrHolidayList = [ModelHoliday]()
    var  arrStaffList = [UserListModel]()
    var dropDown = DropDown()
    var MyId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblHolidayList.delegate = self
        self.tblHolidayList.dataSource = self
        self.tblStaffList.delegate = self
        self.tblStaffList.dataSource = self
        self.lblStaffName.text = "All Staff"
        self.strSelecrStaffId = ""
        self.addGesturesToView()
    }
    func addGesturesToView() -> Void {
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.topToBottomSWIPE))
        swipeRight.direction = .down
        self.viewStaffList.addGestureRecognizer(swipeRight)
    }
    
    @objc func topToBottomSWIPE() ->Void {
        self.view.endEditing(true)
        self.viewStaffList.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let userInfo = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]  ?? ["":""]
        self.MyId = userInfo[UserDefaults.keys.id] as? String ?? ""
        self.viewStaffList.isHidden = true

        let param = ["myId":MyId,"staffId":self.strSelecrStaffId]
        self.callWebserviceHolidayList(dict: param)
        
        let param2 = ["artistId":MyId,"search":""]
        self.callWebserviceForGet_artistStaff(dict: param2)
    }
}


//MARK:- tableview delegate datasource method
extension HolidayListVC {
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if tableView == tblStaffList{
            return self.arrStaffList.count
        }else{
        if arrHolidayList.count == 0{
            self.lblNoDataFound.isHidden = false
        }else{
            self.lblNoDataFound.isHidden = true
        }
        return arrHolidayList.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        if tableView == tblStaffList{
            let cellIdentifier = "BookingCalendarStaffListCell"
            if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? BookingCalendarStaffListCell{
                if self.arrStaffList.count > 0{
                    let objStaffList = self.arrStaffList[indexPath.row]
                    cell.lblName.text = objStaffList.staffName?.capitalized
                    let url = URL(string: objStaffList.staffImage ?? "")
                    if  url != nil {
                        //cell.imgUserProfile.af_setImage(withURL: url!)
                        cell.imgUserProfile.sd_setImage(with: url!, placeholderImage: UIImage(named: "cellBackground"))
                    }else{
                        cell.imgUserProfile.image = #imageLiteral(resourceName: "cellBackground")
                    }
                    if indexPath.row == 0{
                        cell.imgUserProfile.image = #imageLiteral(resourceName: "group_ico")
                    }
                    if self.strSelecrStaffId == String(objStaffList.staffId ?? 0) {
                        cell.lblName.textColor = appColor
                    }else{
                        cell.lblName.textColor = UIColor.black
                    }
                }
                return cell
            }
            return UITableViewCell()
        }else{
        
        
        if let cell  = tableView.dequeueReusableCell(withIdentifier: "CellHolidayList")! as? CellHolidayList {
            let objArtistDetails = arrHolidayList[indexPath.row]
            cell.lblArtistName.text  = "---"//objArtistDetails.crd
            cell.lblDate.text  = objArtistDetails.crd
            cell.lblHolidayDay.text  = "Number of Days - "+objArtistDetails.holidayGiven
            
            
            let startDate = objAppShareData.currentToRequiredDate(date: objArtistDetails.startDate, current: "yyyy-MM-dd", required: "dd/MM/yyyy")
            let endDare = objAppShareData.currentToRequiredDate(date: objArtistDetails.endDate, current: "yyyy-MM-dd", required: "dd/MM/yyyy")
            cell.lblHolidayFromToDate.text  = startDate + " To " + endDare
            cell.lblHolidayAlreadyGivenDay.text  =  "Holidays Already Given - "+objArtistDetails.holidayGiven+"/"+objArtistDetails.holiday
        
            if objArtistDetails.arrStaffInfo.count > 0{
                let objStaff = objArtistDetails.arrStaffInfo[0]
                let url = URL(string: objStaff.profileImage )
            if  url != nil {
                //cell.imgProfile.af_setImage(withURL: url!)
                cell.imgProfile.sd_setImage(with: url!, placeholderImage: UIImage(named: "cellBackground"))
            }else{
                cell.imgProfile.image = #imageLiteral(resourceName: "cellBackground")
                }
                cell.lblArtistName.text  = objStaff.userName.capitalized
            }
            cell.btnProfile.tag = indexPath.row
            cell.btnProfile.superview?.tag = indexPath.section
            cell.btnProfile.addTarget(self, action: #selector(btnGoToProfile(_:)), for: .touchUpInside)
            
            return cell
        }else{
            return UITableViewCell()
            }}
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tblStaffList{
            let obj = self.arrStaffList[indexPath.row]
            self.strSelecrStaffId = String(obj.staffId ?? 0)
            self.lblStaffName.text = obj.staffName ?? ""
            self.viewStaffList.isHidden = true

            if obj.staffName == "All Staff"{
                let param = ["myId":self.MyId,"staffId":""]
                self.callWebserviceHolidayList(dict: param)            }else{
            let param = ["myId":self.MyId,"staffId":self.strSelecrStaffId]
            self.callWebserviceHolidayList(dict: param)
            }
        }else{
        let objArtistDetails = arrHolidayList[indexPath.row]
        let sb = UIStoryboard(name:"Holiday",bundle:Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"HolidayDetailVC") as? HolidayDetailVC{
             objVC.objHolidayDetail = objArtistDetails
            self.navigationController?.pushViewController(objVC, animated: true)
        }
     }
    }
    
}

//MARK: - goto profile acrion
extension HolidayListVC{
    @objc func btnGoToProfile(_ sender: UIButton)
    {
        if arrHolidayList[sender.tag].arrStaffInfo.count > 0 {
            let objData = arrHolidayList[sender.tag].arrStaffInfo[0].userName
            let dicParam =  ["userName":objData]
            btnProfileListAction(dicParam: dicParam, userName: objData)
        }
    }
    
    func btnProfileListAction(dicParam:[String:Any],userName:String){
        //let section = 0
        //let row = (sender as AnyObject).tag
        //let indexPath = IndexPath(row: row!, section: section)
        //let objUser = arrLikeUsersList[indexPath.row]
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        
        objServiceManager.requestPost(strURL: WebURL.profileByUserName, params: dicParam, success: { response in
            if response["status"] as? String ?? "" == "success"{
                var strId = ""
                var strType = ""
                if let dictUser = response["userDetail"] as? [String : Any]{
                    let myId = dictUser["_id"] as? Int ?? 0
                    strId = String(myId)
                    strType = dictUser["userType"] as? String ?? ""
                }
                let dic = [
                    "tabType" : "people",
                    "tagId": strId,
                    "userType":strType,
                    "title": userName ?? ""
                    ] as [String : Any]
                self.openProfileForSelectedTagPopoverWithInfo(dict: dic)
            }
        }) { error in
        }
    }
    
    func openProfileForSelectedTagPopoverWithInfo(dict : [AnyHashable: Any]){
        
        var dictTemp : [AnyHashable : Any]?
        
        dictTemp = dict
        
        if let dict1 = dict as? [String:[String :Any]] {
            if let dict2 = dict1.first?.value {
                dictTemp = dict2
            }
        }
        
        guard let dictFinal = dictTemp as? [String : Any] else { return }
        
        var strUserType: String?
        var tagId : Int?
        
        if let userType = dictFinal["userType"] as? String{
            strUserType = userType
            
            if let idTag = dictFinal["tagId"] as? Int{
                tagId = idTag
            }else{
                if let idTag = dictFinal["tagId"] as? String{
                    tagId = Int(idTag)
                }
            }
            
            var myId = ""
            if let dicUser = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] {
                myId = dicUser["_id"] as? String ?? ""
            }
            if myId == String(tagId ?? 0) ?? "" {
                //isNavigate = true
                objAppShareData.isOtherSelectedForProfile = false
                self.gotoProfileVC()
                return
            }
            
            if let strUserType = strUserType, let tagId =  tagId {
                objAppShareData.selectedOtherIdForProfile  = String(tagId)
                objAppShareData.isOtherSelectedForProfile = true
                objAppShareData.selectedOtherTypeForProfile = strUserType  //"artist" or "user"
                //isNavigate = true
                self.gotoProfileVC()
            }
        }
    }
    func gotoProfileVC (){
        self.view.endEditing(true)
        let sb: UIStoryboard = UIStoryboard(name: "ArtistProfile", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"SWRevealViewController") as? SWRevealViewController{
            objVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(objVC, animated: true)
        }
    }
}
    //MARK: - Button  method
    extension HolidayListVC{
    @IBAction func btnBackAction(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
        @IBAction func btnHideStaffListAction(_ sender: UIButton) {
            self.viewStaffList.isHidden = true
        }
    @IBAction func btnAddHolidayAction(_ sender:UIButton){
        let sb = UIStoryboard(name:"Holiday",bundle:Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"AddHolidayVC") as? AddHolidayVC{
            objVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(objVC, animated: true)
        }
    }
   
    @IBAction func btnSelectStaff(_ sender: UIButton) {
        self.view.endEditing(true)
        let objGetData = self.arrStaffList.map { $0.staffName?.capitalized ?? "" }
        guard objGetData.isEmpty == false else {
            objAppShareData.showAlert(withMessage: "No staff available", type: alertType.bannerDark,on: self)
            return
        }
        self.HeightViewBottumTable.constant = CGFloat(self.arrStaffList.count*50)
        self.tblStaffList.reloadData()
        if self.arrStaffList.count == 0{
            objAppShareData.showAlert(withMessage: "No staff added", type: alertType.bannerDark, on: self)
        }else{
            self.viewStaffList.isHidden = !self.viewStaffList.isHidden
        }
        
//        let objGetData = self.arrStaffList.map { $0.staffName?.capitalized ?? "" }
//        guard objGetData.isEmpty == false else {
//            objAppShareData.showAlert(withMessage: "No staff available", type: alertType.bannerDark,on: self)
//            return
//        }
//        dropDown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
//        dropDown.anchorView = sender // UIView or UIBarButtonItem
//        dropDown.direction = .bottom
//        dropDown.dataSource = objGetData
//        dropDown.width = self.view.frame.size.width/2
//        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
//            print("Selected item: \(item) at index: \(index)")
//            self.lblStaffName.text = item.capitalized
//            var id = 0
//            for obj in self.arrStaffList{
//                if self.lblStaffName.text == "All Staff" || self.lblStaffName.text == "All Staff" || self.lblStaffName.text == nil {
//                    let param = ["myId":self.MyId,"staffId":""]
//                }else if self.lblStaffName.text?.capitalized == obj.staffName?.capitalized{
//                    id = obj.staffId ?? 0
//                }
//            }
//                if id == 0{
//                    let param = ["myId":self.MyId,"staffId":""]
//                    self.callWebserviceHolidayList(dict: param)
//                }else{
//                            let param = ["myId":self.MyId,"staffId":String(id)]
//                            self.callWebserviceHolidayList(dict: param)
//                }
//        }
//        dropDown.show()
    }
}


extension HolidayListVC{
func callWebserviceHolidayList(dict: [String : Any]){
    if !objServiceManager.isNetworkAvailable(){
        objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
        return
    }
    self.arrHolidayList.removeAll()
    objActivity.startActivityIndicator()
    objServiceManager.requestPost(strURL: WebURL.holidayList, params: dict  , success: { response in
        objServiceManager.StopIndicator()
        let keyExists = response["responseCode"] != nil
        if  keyExists {
            sessionExpireAlertVC(controller: self)
        }else{
            let strStatus =  response["status"] as? String ?? ""
            if strStatus == k_success{
                if let data = response["data"] as? [[String:Any]]{
                    for dict in data{
                        let objModel = ModelHoliday.init(dict: dict)
                        self.arrHolidayList.append(objModel)
                    }
                }
                self.tblHolidayList.reloadData()
            }else{
                self.tblHolidayList.reloadData()
                let msg = response["message"] as? String ?? ""
             //   objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
            }
        }
    }){ error in
        objServiceManager.StopIndicator()
        self.tblHolidayList.reloadData()
        objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
    }
}
}

//MARK:- Webservice Call
extension HolidayListVC {
    
    func callWebserviceForGet_artistStaff(dict: [String : Any]){
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        
        self.arrStaffList.removeAll()
        self.tblHolidayList.reloadData()
        objWebserviceManager.StartIndicator()
        objServiceManager.requestPost(strURL: WebURL.artistStaff, params: dict  , success: { response in
            objServiceManager.StopIndicator()
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else{
                let strStatus =  response["status"] as? String ?? ""
                if strStatus == k_success{
                    self.parseResponce(response:response)
                }else{
                }
            }
        }){ error in
            objServiceManager.StopIndicator()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
    func parseResponce(response:[String : Any]){
        self.arrStaffList.removeAll()
        if let arr = response["staffList"] as? [[String:Any]]{
            if arr.count > 0{
                let myDict = ["staffName":"All Staff","_id":"","businessId":"","staffImage":"","job":"","staffId":"","status":"","alreadyGivenHoliday":"","totalHoliday":""]
                let obj  = UserListModel(dict: myDict)
                self.arrStaffList.append(obj)

                for dictArtistData in arr {
                    let objArtistList = UserListModel.init(dict: dictArtistData)
                    if  objArtistList.status == "1"{
                    self.arrStaffList.append(objArtistList)
                    }
                }
            }
        }
    }
}

//
//  AddHolidayVC.swift
//  MualabBusiness
//
//  Created by Mindiii on 2/19/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
import DropDown

class AddHolidayVC: UIViewController,UITextFieldDelegate,UITextViewDelegate,UITableViewDelegate,UITableViewDataSource {
        
        @IBOutlet weak var txtStaffName: UITextField!
        @IBOutlet weak var lblHolidayGiven: UITextField!
        @IBOutlet weak var txtDescription: UITextView!
        
        @IBOutlet weak var lblStartDate: UILabel!
        @IBOutlet weak var lblEndDate: UILabel!
        @IBOutlet weak var btnContinue: UIButton!
    
        @IBOutlet weak var viewPicker:UIView!
        @IBOutlet weak var DOBPicker : UIDatePicker!
    
    @IBOutlet weak var viewStaffList: UIView!
    @IBOutlet weak var tblStaffList: UITableView!
    @IBOutlet weak var HeightViewBottumTable: NSLayoutConstraint!

        var  arrStaffList = [UserListModel]()
        var startDate = true
        let dropDown = DropDown()
        
        var strStartDate = ""
        var strEndDate = ""
        var MyId = ""

        var voucherTypeEdit = false
        var objData = VoucherModel(dict: ["":""])
    
        override func viewDidLoad() {
            super.viewDidLoad()
            self.viewPicker.isHidden = true
            self.strStartDate = ""
            self.strEndDate = ""
            self.txtDescription.delegate = self
            self.txtStaffName.delegate = self
            self.tblStaffList.delegate = self
            self.tblStaffList.dataSource = self
            self.viewStaffList.isHidden = true
            self.addAccesorryToKeyBoard()
            // Do any additional setup after loading the view.
            self.addGesturesToView()
        }
    func addGesturesToView() -> Void {
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.topToBottomSWIPE))
        swipeRight.direction = .down
        self.viewStaffList.addGestureRecognizer(swipeRight)
    }
    
    @objc func topToBottomSWIPE() ->Void {
        self.view.endEditing(true)
        self.viewStaffList.isHidden = true
    }
        override func viewWillAppear(_ animated: Bool) {
            self.viewStaffList.isHidden = true

            strStartDate = ""
            strEndDate = ""
            self.DOBPicker.minimumDate = Date()
            let userInfo = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]  ?? ["":""]
             self.MyId = userInfo[UserDefaults.keys.id] as? String ?? ""
            let param = ["artistId":MyId,"search":""]
                self.callWebserviceForGet_artistStaff(dict: param)
                self.btnContinue.setTitle("Save & Continue", for: .normal)
            
        }
        
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
        
        
    }
    
    //MARK: - button extension
    extension AddHolidayVC{
        func textFieldShouldReturn(_ textField: UITextField) -> Bool{
             if textField == self.txtDescription{
                txtDescription.resignFirstResponder()
            }
            return true
        }
 
        
        func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
            var numberOfChars:Int = 0
            let newText = (txtDescription.text as NSString).replacingCharacters(in: range, with: text)
            numberOfChars = newText.count
            if numberOfChars >= 150 && text != ""{
                return false
            }else{
            }
            return numberOfChars < 150
        }
    }
    //MARK: - Custome method extension
    fileprivate extension AddHolidayVC {
        func addAccesorryToKeyBoard(){
            let keyboardDoneButtonView = UIToolbar()
            keyboardDoneButtonView.sizeToFit()
            let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
            let doneButton = UIBarButtonItem(title: "Done", style:.plain, target: self, action: #selector(self.resignKeyBoard))
            doneButton.tintColor = appColor
            keyboardDoneButtonView.items = [flexBarButton, doneButton]
            txtDescription.inputAccessoryView = keyboardDoneButtonView
        }
        @objc func resignKeyBoard(){
            txtDescription.endEditing(true)
            self.view.endEditing(true)
        }
        
    }
    //MARK: - popup and dropdown button
    fileprivate extension AddHolidayVC{
            
        @IBAction func btnSelectStaff(_ sender: UIButton) {
            self.view.endEditing(true)
            let objGetData = self.arrStaffList.map { $0.staffName?.capitalized ?? "" }
            guard objGetData.isEmpty == false else {
                objAppShareData.showAlert(withMessage: "No staff available", type: alertType.bannerDark,on: self)
                return
            }
            self.HeightViewBottumTable.constant = CGFloat(self.arrStaffList.count*50)
            self.viewStaffList.isHidden = false
            self.tblStaffList.reloadData()
        
        }
        
        @IBAction func btnValidityStartDate(_ sender: UIButton) {
            self.view.endEditing(true)
            
            if self.txtStaffName.text == "" ||  self.txtStaffName.text == nil{
                objAppShareData.showAlert(withMessage: "Please select staff", type: alertType.banner, on: self)
           return
            }
            
            var reaminingHoliday = 0
            for obj in self.arrStaffList{
                if obj.staffName?.capitalized == self.txtStaffName.text?.capitalized{
                    reaminingHoliday = obj.totalHoliday-obj.alreadyGivenHoliday
                }
            }
            
            if reaminingHoliday == 0 {
                objAppShareData.showAlert(withMessage: "No holiday available!", type: alertType.banner, on: self)
                return
            }
            
            if sender.tag == 0 {
                self.viewPicker.isHidden = false
                self.DOBPicker.minimumDate = Date()
                self.DOBPicker.maximumDate  = Calendar.current.date(byAdding: .year, value: 10, to: Date())

                self.startDate = true
                if self.lblStartDate.text != "" && self.lblStartDate.text != "From Date"{
                    let a = self.lblStartDate.text?.components(separatedBy: "/")
                    if a?.count == 3 {
                        self.DOBPicker.date = objAppShareData.setDateFromString1(time:self.lblStartDate.text!)
                    }
                }
            }else if sender.tag == 1 {
                if self.lblStartDate.text == "" || self.lblStartDate.text == "From Date" {
                    objAppShareData.showAlert(withMessage: "Please select From date", type: alertType.banner, on: self)
                }else{
                    self.viewPicker.isHidden = false
                    self.startDate = false
                    self.DOBPicker.minimumDate = objAppShareData.setDateFromString1(time:self.lblStartDate.text!)
                    let dates = Calendar.current.date(byAdding: .year, value: 1, to:Date())
                 //   self.DOBPicker.maximumDate  = Calendar.current.date(byAdding: .day, value: reaminingHoliday, to: self.DOBPicker.minimumDate ?? dates!)
                    // datePicker.date = minDate!
                    if self.lblEndDate.text != "" && self.lblEndDate.text != "To Date"{
                        let a = self.lblEndDate.text?.components(separatedBy: "/")
                        if a?.count == 3 {
                            self.DOBPicker.date = objAppShareData.setDateFromString1(time:self.lblEndDate.text!)
                        }
                    }
                }
            }
        }
        
        func checkDifferenceToDate () -> Int{
            let date1 = objAppShareData.setDateFromString1(time:self.lblStartDate.text!)
            let date2 = objAppShareData.setDateFromString1(time:self.lblEndDate.text!)
            var timeDays = 0
            var secondsBetween1: TimeInterval? = nil
            secondsBetween1 = date2.timeIntervalSince(date1)
            let secondsBetween = Int(secondsBetween1!)
            let numberOfDays: Int = secondsBetween / 86400
            if numberOfDays > 0 {
                timeDays = numberOfDays
            }
            return timeDays
        }
        
        
        @IBAction func btnDoneCancleDOBSelection(_ sender:UIButton){
            self.view.endEditing(true)
            if sender.tag == 1 {
                if startDate == true{
                    self.strStartDate = objAppShareData.dateFormatToShow1(forAPI: self.DOBPicker.date)
                    if self.lblStartDate.text == objAppShareData.dateFormatToShowDace(forAPI: self.DOBPicker.date){
                    }else{
                        self.lblEndDate.text = "To Date"
                    }
                    self.lblStartDate.text = objAppShareData.dateFormatToShowDace(forAPI: self.DOBPicker.date)
                }else{
                    self.strEndDate = objAppShareData.dateFormatToShow1(forAPI: self.DOBPicker.date)
                    self.lblEndDate.text = objAppShareData.dateFormatToShowDace(forAPI: self.DOBPicker.date)
                }
            }
            self.viewPicker.isHidden = true
        }
        
        
        @IBAction func btnContinew(_ sender:UIButton){
            self.view.endEditing(true)
            txtStaffName.text = txtStaffName.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            txtDescription.text = txtDescription.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            if self.txtStaffName.text?.count == 0{
                objAppShareData.shakeViewField(self.txtStaffName)
            }else if self.lblStartDate.text == "From Date"{
                objAppShareData.showAlert(withMessage: "Please select From date", type: alertType.banner, on: self)
            }else if self.lblEndDate.text == "To Date"{
                objAppShareData.showAlert(withMessage:  "Please select To date", type: alertType.banner, on: self)
            }else if self.txtDescription.text?.count == 0{
                objAppShareData.shakeViewField(self.txtDescription)
            }else{
                let userInfo = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]  ?? ["":""]
               let myId = userInfo[UserDefaults.keys.id] as? String ?? ""
               var staffId = ""
                var _Id = ""
                for obj in arrStaffList{
                    if obj.staffName?.capitalized == self.txtStaffName.text?.capitalized{
                       staffId = String(obj.staffId ?? 0)
                        _Id = String(obj._id ?? 0)
                    }
                }
                
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
                let myCurrentDateString = formatter.string(from: Date())
                
                let holidayGiven =  self.checkDifferenceToDate()+1
                
                var reaminingHoliday = 0
                for obj in self.arrStaffList{
                    if obj.staffName?.capitalized == self.txtStaffName.text?.capitalized{
                        reaminingHoliday = obj.totalHoliday-obj.alreadyGivenHoliday
                    }
                }

                if holidayGiven > reaminingHoliday{
                    objAppShareData.showAlert(withMessage: "Please select valid holidays", type: alertType.banner, on: self)
                    return
                }
                if holidayGiven == 0{
                    objAppShareData.showAlert(withMessage: "Please select at least one holidays", type: alertType.banner, on: self)
                    return
                }
                
                let param =  ["staffId":staffId,
                              "staffMainId":_Id,
                    "holidayGiven":String(holidayGiven),
                    "startDate":self.strStartDate,// ('yyyy-mm-dd')
                    "endDate":self.strEndDate,//  ('yyyy-mm-dd')
                    "reason":self.txtDescription.text ?? "",
                    "myId":myId,
                    "crdDate":myCurrentDateString] as [String : Any]
               self.callWebserviceAddHoliday(dict: param)
            }
        }
        @IBAction func btnBackAction(_ sender:UIButton){
            self.navigationController?.popViewController(animated: true)
        }
        
        
        func callWebserviceAddHoliday(dict: [String : Any]){
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            objActivity.startActivityIndicator()
            objServiceManager.requestPost(strURL: WebURL.addHoliday, params: dict  , success: { response in
                objServiceManager.StopIndicator()
                let keyExists = response["responseCode"] != nil
                if  keyExists {
                    sessionExpireAlertVC(controller: self)
                }else{
                    let strStatus =  response["status"] as? String ?? ""
                    if strStatus == k_success{
                        objAppShareData.showAlert(withMessage: "Holiday added successfully", type: alertType.bannerDark,on: self)
                        self.navigationController?.popViewController(animated: true)
                    }else{
                        let msg = response["message"] as? String ?? ""
                        objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                    }
                }
            }){ error in
                objServiceManager.StopIndicator()
                objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
            }
        }
}



//MARK:- Webservice Call
extension AddHolidayVC {
    
    func callWebserviceForGet_artistStaff(dict: [String : Any]){
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        self.arrStaffList.removeAll()
        objWebserviceManager.StartIndicator()
        objServiceManager.requestPost(strURL: WebURL.artistStaff, params: dict  , success: { response in
            objServiceManager.StopIndicator()
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else{
                let strStatus =  response["status"] as? String ?? ""
                if strStatus == k_success{
                    self.parseResponce(response:response)
                }else{
                }
            }
        }){ error in
            objServiceManager.StopIndicator()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
    func parseResponce(response:[String : Any]){
        self.arrStaffList.removeAll()
        if let arr = response["staffList"] as? [[String:Any]]{
            if arr.count > 0{
                for dictArtistData in arr {
                    let objArtistList = UserListModel.init(dict: dictArtistData)
                    if  objArtistList.status == "1"{
                    self.arrStaffList.append(objArtistList)
                    }
                }
            }
        }
    }
}

//MARK:- tableview delegate datasource method
extension AddHolidayVC {
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
             return self.arrStaffList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
            let cellIdentifier = "BookingCalendarStaffListCell"
            if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? BookingCalendarStaffListCell{
                if self.arrStaffList.count > 0{
                    let objStaffList = self.arrStaffList[indexPath.row]
                    cell.lblName.text = objStaffList.staffName?.capitalized
                    let url = URL(string: objStaffList.staffImage ?? "")
                    if  url != nil {
                        //cell.imgUserProfile.af_setImage(withURL: url!)
                        cell.imgUserProfile.sd_setImage(with: url!, placeholderImage: UIImage(named: "cellBackground"))
                    }else{
                        cell.imgUserProfile.image = #imageLiteral(resourceName: "cellBackground")
                    }
                    if indexPath.row == 0{
                        cell.imgUserProfile.image = #imageLiteral(resourceName: "group_ico")
                    }
//                    if self.strSelecrStaffId == String(objStaffList.staffId ?? 0) {
//                        cell.lblName.textColor = appColor
//                    }else{
//                        cell.lblName.textColor = UIColor.black
//                    }
                }
                return cell
            }
            return UITableViewCell()
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
             let obj = self.arrStaffList[indexPath.row]
        if self.txtStaffName.text != obj.staffName{
            self.txtDescription.text = ""
            self.lblEndDate.text = "To Date"
            self.lblHolidayGiven.text = ""
            self.lblStartDate.text = "From Date"
        }
        self.txtStaffName.text = obj.staffName?.capitalized
        self.viewStaffList.isHidden = true

        for obj in self.arrStaffList{
            if obj.staffName?.capitalized == self.txtStaffName.text?.capitalized{
                self.lblHolidayGiven.text = String(obj.alreadyGivenHoliday)+"/"+String(obj.totalHoliday)
            }
        }
        
    }
    
    @IBAction func btnHideStaffListAction(_ sender: UIButton) {
        self.viewStaffList.isHidden = true
    }


}

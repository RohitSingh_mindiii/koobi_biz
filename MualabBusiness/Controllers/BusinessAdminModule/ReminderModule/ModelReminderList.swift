//
//  ModelReminderList.swift
//  MualabBusiness
//
//  Created by Mindiii on 5/29/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit

class ModelReminderList: NSObject {
    var artistId = ""
    var crd = ""
    var notifincationType = ""
    var upd = ""
    var id = ""
    var date = ""
    var message = ""
    var currentDate = ""
    var notifincationId = 0

    
    
    
    
      init(dict: [String : Any]) {
        self.crd = dict["crd"] as? String ?? ""
        message = dict["message"] as? String ?? ""
        upd = dict["upd"] as? String ?? ""
        date = objAppShareData.crtToDateString(crd: crd)
        id = objCustom.typeManage(strKey: "_id", dict: dict)
        artistId = objCustom.typeManage(strKey: "artistId", dict: dict)
        currentDate = dict["currentDate"] as? String ?? ""
        let arr = currentDate.components(separatedBy: "-")
        if arr.count == 3{
           currentDate = arr[2] + "/" + arr[1] + "/" + arr[0]
        }
        //currentDate = objAppShareData.crtToDateString(crd: currentDate)
        notifincationType = objCustom.typeManage(strKey: "notifincationType", dict: dict)
        if let nId = dict["notifincationType"] as? Int{
            notifincationId = nId
        }
    }
    
    
   
}


//  ReminderListVC.swift
//  MualabBusiness
//
//  Created by Mindiii on 5/27/19.
//  Copyright © 2019 Mindiii. All rights reserved.

import UIKit
import Firebase

class ReminderListVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tblReminder: UITableView!
    @IBOutlet weak var lblTotalPatAmmount: UILabel!
    @IBOutlet weak var lblPreviousAmmount: UILabel!
    @IBOutlet weak var lblCurrentAmmount: UILabel!
    @IBOutlet weak var viewStackPrevious: UIView!

    @IBOutlet weak var lblNoDataFound: UIView!
    @IBOutlet weak var lblMonthCommission: UILabel!
    @IBOutlet weak var btnPay: UIButton!

    private var arrReminderList  = [ModelReminderList]()
    var totalPrice = "0.0"
    var strUserId = ""
    var ref: DatabaseReference!

    
    fileprivate var previewsEndDate = "2019-04-30";
    fileprivate var previewsStartDate = "2019-04-01";
    fileprivate var endDate = "2019-05-31";
    fileprivate var startDate = "2019-05-01";
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewStackPrevious.isHidden = true

        self.btnPay.backgroundColor = #colorLiteral(red: 0, green: 0.851996243, blue: 0.8281483054, alpha: 0.5)
        self.lblNoDataFound.isHidden = false
        self.tblReminder.delegate = self
        self.tblReminder.dataSource = self
        self.tblReminder.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
            self.manageInvitationCount()
            self.monthName(date: Date())
            self.callWebserviceForReminderList()
            self.getStartEndDate()
        
    }
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnPayAction(_ sender: UIButton) {
        if self.lblTotalPatAmmount.text == "£0" || self.lblTotalPatAmmount.text == "0" || self.lblTotalPatAmmount.text == "" || self.lblTotalPatAmmount.text == "£0.0" || self.lblTotalPatAmmount.text == "£0.00"{
            return
        }else{
        let sb = UIStoryboard(name:"PaymentModule",bundle:Bundle.main)
        let objChooseType = sb.instantiateViewController(withIdentifier:"MakePaymentVC") as! MakePaymentVC
        objChooseType.strBookingId = "0"//.arrBookedMainService[0]._id
        objChooseType.strbidAmount = totalPrice
        self.navigationController?.pushViewController(objChooseType, animated: true)
    }
    }
}


//MARK: - table view method extension
extension ReminderListVC{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.arrReminderList.count == 0{
            self.lblNoDataFound.isHidden = false
        }else{
            self.lblNoDataFound.isHidden = true
        }
        return arrReminderList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "CellReminderList"
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? CellReminderList{
            let obj = self.arrReminderList[indexPath.row]
            
            if obj.notifincationType == "1"{
                cell.imgIcon.image = #imageLiteral(resourceName: "reminder_1")
                cell.lblReminderDetail.text = obj.message
                //cell.lblDate.text = obj.date
                cell.lblDate.text = obj.currentDate
            }else if obj.notifincationType == "2"{
                cell.imgIcon.image = #imageLiteral(resourceName: "reminder_2")
                cell.lblReminderDetail.text = obj.message
                //cell.lblDate.text = obj.date
                cell.lblDate.text = obj.currentDate
            }else{
                cell.imgIcon.image = #imageLiteral(resourceName: "reminder_3")
                cell.lblReminderDetail.text = obj.message
                //cell.lblDate.text = obj.date
                cell.lblDate.text = obj.currentDate
            }
            return cell
        }
        
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}


//MARK:- Webservice Call
extension ReminderListVC {
    
    func callWebserviceForPaymentData(dict: [String : Any]){
        self.arrReminderList.removeAll()
       
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        
        objActivity.startActivityIndicator()
        
        objServiceManager.requestPost(strURL: WebURL.bookingAdminCommsion, params: ["":""]  , success: { response in
        objServiceManager.StopIndicator()
          
            let keyExists = response["responseCode"] != nil
                if  keyExists {
                    sessionExpireAlertVC(controller: self)
                }else{
                    let strStatus =  response["status"] as? String ?? ""
                    if strStatus == k_success{
                        self.parseResponce(response:response)
                    }else{
                        self.tblReminder.reloadData()
                        let msg = response["message"] as! String
                        objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                    }
                }
        }){ error in
            self.tblReminder.reloadData()
          
            objServiceManager.StopIndicator()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
    func parseResponce(response:[String : Any]){
        var previous = "0"
        var current = "0"
        
        if let data = response["data"] as? String{
            self.lblCurrentAmmount.text =  "£"+data
            current = data
        }else if  let data = response["data"] as? Int{
            self.lblCurrentAmmount.text =  "£"+String(format: "%.2f",  double_t(data))
           current = String(data)
        }else if  let data = response["data"] as? float_t{
            self.lblCurrentAmmount.text =  "£"+String(format: "%.2f",  double_t(data))
            current = String(data)
        }else if  let data = response["data"] as? double_t{
            self.lblCurrentAmmount.text =  "£"+String(format: "%.2f",  double_t(data))
            current = String(data)
        }
        
        
  

        previewsEndDate = response["previewsEndDate"] as? String ?? ""
        previewsStartDate =  response["previewsStartDate"] as? String ?? ""
        endDate =  response["endDate"] as? String ?? ""
        startDate = response["startDate"] as? String ?? ""
  
        if let data = response["previousData"] as? String{
            self.lblPreviousAmmount.text =  "£"+data
            previous = data
        }else if  let data = response["previousData"] as? Int{
            self.lblPreviousAmmount.text =  "£"+String(format: "%.2f",  double_t(data))
            previous = String(data)
        }else if  let data = response["previousData"] as? float_t{
            self.lblPreviousAmmount.text =  "£"+String(format: "%.2f",  double_t(data))
            previous = String(data)
        }else if  let data = response["previousData"] as? double_t{
            self.lblPreviousAmmount.text =  "£"+String(format: "%.2f", data)
            previous = String(data)
        }
        
        totalPrice = String((float_t(previous) ?? 0.0)+(float_t(current) ?? 0.0))
        self.lblTotalPatAmmount.text = "£"+String(format: "%.2f", double_t(totalPrice) ?? 0.0)
 
    if  (float_t(previous) ?? 0.0) == 0{
            self.selectedDay(strDate: previewsStartDate)
        }else{
            self.selectedDay(strDate: startDate)
        }
        
        if self.lblTotalPatAmmount.text == "£0" || self.lblTotalPatAmmount.text == "0" || self.lblTotalPatAmmount.text == "" || self.lblTotalPatAmmount.text == "£0.0" || self.lblTotalPatAmmount.text == "£0.00"{
            self.btnPay.backgroundColor = #colorLiteral(red: 0, green: 0.851996243, blue: 0.8281483054, alpha: 0.5)
        }else{
            self.btnPay.backgroundColor = appColor
        }
        if (float_t(previous) ?? 0.0) == 0{
            self.viewStackPrevious.isHidden = true
        }else{
            self.viewStackPrevious.isHidden = true
            self.lblTotalPatAmmount.text = self.lblPreviousAmmount.text
        }
       // self.lblTotalPatAmmount.text =  "£20.00"
    }
    
    func monthName(date:Date){
        let now = date
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "LLLL"
        self.lblMonthCommission.text = "Commission of "+dateFormatter.string(from: now)
    }
    
    func selectedDay(strDate:String){
        let isoDate = strDate+"T10:44:00+0000"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
        let date = dateFormatter.date(from:isoDate)!
        //monthName(date: date)
    }
    
    func manageInvitationCount(){
        if let dict = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]{
            var artId = ""
            if let artistId = dict[UserDefaults.keys.userId] as? String{
                artId = artistId
            }else if let artistId = dict[UserDefaults.keys.userId] as? Int{
                artId = String(artistId)
            }
            self.ref = Database.database().reference()
            self.ref.child("socialBookingBadgeCount").child(artId).updateChildValues(["commissionCount":0])
        }
    }
    func getStartEndDate(){
    
//            let thisMonthCurrentDate = Date() ?? Date()
//            let thisMonthStartDate = startOfMonth(startDate: Date())  ?? Date()
//            let priviousEndDate  = Calendar.current.date(byAdding: .day, value: -1, to: thisMonthStartDate )  ?? Date()
//            let priviousStartDate  = startOfMonth(startDate: priviousEndDate ?? Date())  ?? Date()
//
//        let preStartDate = objAppShareData.dateFormatToShow1(forAPI: priviousStartDate )
//        let preEndDate = objAppShareData.dateFormatToShow1(forAPI: priviousEndDate ?? Date())
//        let CurrentDate = objAppShareData.dateFormatToShow1(forAPI: thisMonthCurrentDate )
//        let StartDate = objAppShareData.dateFormatToShow1(forAPI: thisMonthStartDate )
//
        let dict = ["startDate":startDate, "endDate":endDate,   "previewsStartDate":previewsStartDate, "previewsEndDate":previewsEndDate]
       //let dict = ["startDate":thisMonthStartDate, "endDate":thisMonthCurrentDate,   "previewsStartDate":priviousStartDate, "previewsEndDate":priviousEndDate]

      self.callWebserviceForPaymentData(dict: dict)
     }
    
    func getInSepreatedForm(date:String) -> String{
       let arr =  date.components(separatedBy: " ")
        return arr[0]
    }

    func startOfMonth(startDate:Date) -> Date {
        return Calendar.current.date(from: Calendar.current.dateComponents([.year, .month], from: startDate))!
    }
    
    func endOfMonth(startDate:Date) -> Date {
        return Calendar.current.date(byAdding: DateComponents(month: 1, day: -1), to: startDate)!
    }
}

//MARK: - button extension
extension ReminderListVC{
    func callWebserviceForReminderList(){
        if !objServiceManager.isNetworkAvailable(){
            return
        }
        self.arrReminderList.removeAll()
        objActivity.startActivityIndicator()
        objServiceManager.requestGet(strURL:WebURL.adminReminder, params:nil, success: { response in
            objActivity.stopActivity()
            let  status = response["status"] as? String ?? ""
            print(response)
            if status == "success"{
                if let arr = response["data"] as? [[String:Any]] {
                    for obj in arr{
                        let newObj = ModelReminderList.init(dict: obj)
                              self.arrReminderList.append(newObj)
                     }
                    if self.arrReminderList.count > 0{
                        let arr = self.arrReminderList.sorted(by: { $0.notifincationId < $1.notifincationId })
                        self.arrReminderList = arr
                    }
                    self.tblReminder.reloadData()
                }
            }else{
                self.tblReminder.reloadData()
                let msg = response["message"] as? String ?? ""
                objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
            }
        }) { error in
            objActivity.stopActivity()
             objAppShareData.showDeafultAlert(withTitle: message.error.title, message: message.error.wrong, on: self)
             self.tblReminder.reloadData()
            objActivity.stopActivity()
        }
}
}

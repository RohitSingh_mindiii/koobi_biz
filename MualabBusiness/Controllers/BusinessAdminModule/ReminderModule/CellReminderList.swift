//
//  CellReminderList.swift
//  MualabBusiness
//
//  Created by Mindiii on 5/27/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit

class CellReminderList: UITableViewCell {
    @IBOutlet weak var lblReminderDetail: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var imgIcon: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//
//  CompanyBusinessInfoVC.swift
//  MualabBusiness
//
//  Created by Mac on 25/12/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import CoreTelephony

fileprivate class countryCodeDataEdit: NSObject {
    
    var countryName : String = ""
    var dialCode : String = ""
    var countryCode : String = ""
    init(countryName:String?, dialCode:String? , countryCode :String?) {
        self.countryName = countryName ?? ""
        self.dialCode = dialCode ?? ""
        self.countryCode = countryCode ?? ""
    }
}

class CompanyBusinessInfoVC: UIViewController,UITextFieldDelegate{

        @IBOutlet weak var lblBookingSetting:UILabel!
        @IBOutlet weak var lblPaymentType:UILabel!
        @IBOutlet weak var lblBookinkType:UILabel!
        @IBOutlet weak var lblRadiusOfArea:UILabel!
        @IBOutlet weak var lblBusinessAddress:UILabel!
        @IBOutlet weak var lblLocationAddress:UILabel!
        @IBOutlet weak var txtBusinessEmail:UITextField!
        @IBOutlet weak var lblCompletionTime:UILabel!
        @IBOutlet weak var lblCompletionTimeOutCall:UILabel!

    @IBOutlet weak var img1:UIImageView!
    @IBOutlet weak var img2:UIImageView!
    @IBOutlet weak var img3:UIImageView!
    @IBOutlet weak var img4:UIImageView!
    @IBOutlet weak var img5:UIImageView!

        @IBOutlet weak var lblDynamicAddressTytle:UILabel!
        @IBOutlet weak var txtBusinessName:UITextField!
        @IBOutlet weak var txtPhoneNumber : UITextField!
    
    @IBOutlet weak var viewRadiurOfArea : UIView!
    @IBOutlet weak var viewReturnLocationAdd : UIView!
    @IBOutlet weak var viewBusinessAddress : UIView!
    @IBOutlet weak var viewSupper : UIView!
    @IBOutlet weak var viewBookingSetting : UIView!
    @IBOutlet weak var viewPaymentOption : UIView!

        @IBOutlet weak var btnSaveAndContinue:UIButton!
        @IBOutlet weak var btnBookingSetting:UIButton!

    @IBOutlet weak var btnCountryCode : UIButton!
    @IBOutlet weak var countryCodeView : UIView!
    @IBOutlet weak var searchBar : UISearchBar!
    @IBOutlet weak var tblCountryCode : UITableView!
    @IBOutlet weak var bottomConstraint1 : NSLayoutConstraint!
    @IBOutlet weak var btnCountryWidth : NSLayoutConstraint!
    @IBOutlet weak var lblNoResults : UILabel!

    
    fileprivate var arrayCountryCode : [countryCodeDataEdit] = []
    fileprivate var arrToShow : [countryCodeDataEdit] = []
    fileprivate var countryCode: String = ""
    
    let paramOld = ["":""]
        var strUserId: String = ""
        fileprivate var strLocalCCode: String = ""
        
        override func viewDidLoad() {
            super.viewDidLoad()
            self.countryCodeView.alpha = 0.0;
            self.searchBar.delegate = self
            self.tblCountryCode.delegate = self
            self.tblCountryCode.dataSource = self
            objAppShareData.fromAddressVC = false
            objAppShareData.paramAddress = ["":""]
            self.txtPhoneNumber.delegate = self
            self.txtBusinessEmail.delegate = self
            self.txtBusinessName.delegate = self
            let dict = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] ?? ["":""]
            objAppShareData.paramAddress = dict as? [String : String] ?? ["":""]
            let id = dict[UserDefaults.keys.userId] as? String ?? ""
            if id  == self.strUserId{
                self.btnCountryWidth.constant = 0
                self.parseBusinessSignUpDetail(dictOfInfo: dict)
                let businessType = UserDefaults.standard.string(forKey: UserDefaults.keys.mainServiceType) ?? ""
                objAppShareData.objModelBusinessSetup.serviceType = businessType
                
                if objAppShareData.objModelBusinessSetup.bookingSetting ==  "0"{
                     self.lblBookingSetting.text = "Automatic"
                } else if objAppShareData.objModelBusinessSetup.bookingSetting ==  "1"{
                     self.lblBookingSetting.text = "Manual"
                }
                
//                if objAppShareData.objModelBusinessSetup.paymentType == "1"{
//                    self.lblPaymentType.text = "Cash"
//                }else{
//                    self.lblPaymentType.text = "Card"
//                    objAppShareData.objModelBusinessSetup.paymentType = "0"
//                }
                
            }else{
                self.btnCountryWidth.constant = 0
            }
            
            customSearchBar()
            getCountryCode()
            observeKeyboard()
            
            objAppShareData.objAppdelegate.parseBusinessSignUpDetail(dictOfInfo: dict)
            self.addAccesorryToKeyBoard()
            
            let countryCodeTap = UITapGestureRecognizer(target: self, action: #selector(handleCountryCodeTap(gestureRecognizer:)))
            btnCountryCode.addGestureRecognizer(countryCodeTap)
            // Do any additional setup after loading the view.
        }
    
    //Custom UISearchBar
    func customSearchBar() {
        searchBar.backgroundImage = UIImage()
        searchBar.searchBarStyle = .minimal;
        let textFieldInsideSearchBar = searchBar.value(forKey: "searchField") as? UITextField
        textFieldInsideSearchBar?.font = UIFont.init(name: "Nunito-Light", size: 17.0)
        textFieldInsideSearchBar?.textColor = UIColor.white
        
        let textFieldInsideSearchBarLabel = textFieldInsideSearchBar!.value(forKey: "placeholderLabel") as? UILabel
        textFieldInsideSearchBarLabel?.textColor = UIColor.white
        
        let clearButton = textFieldInsideSearchBar?.value(forKey: "_clearButton") as? UIButton
        let templateImage = UIImage.init(named:"clearButton")
        clearButton?.setImage(templateImage,for:.normal)
        
        let glassIconView = textFieldInsideSearchBar?.leftView as? UIImageView
        glassIconView?.image = glassIconView?.image?.withRenderingMode(.alwaysTemplate)
        glassIconView?.tintColor = UIColor.white
    }
    @objc func handleCountryCodeTap(gestureRecognizer: UIGestureRecognizer) {
        self.countryCodeView.isHidden = false
        // searchBar.becomeFirstResponder()
        self.view.endEditing(true)
        UIView.animate(withDuration: 0.5) {
            self.countryCodeView.alpha = 1.0
        }
    }
        func parseDataFromLocal(){
            self.txtBusinessEmail.text = objAppShareData.objModelBusinessSetup.businessEmail
            self.txtBusinessName.text = objAppShareData.objModelBusinessSetup.businessName
            self.txtPhoneNumber.text = objAppShareData.objModelBusinessSetup.businessContactNo
            self.btnCountryCode.setTitle(objAppShareData.objModelBusinessSetup.countryCode, for: .normal)
            self.txtPhoneNumber.text = objAppShareData.objModelBusinessSetup.countryCode+" - "+objAppShareData.objModelBusinessSetup.businessContactNo
            let dict = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] ?? ["":""]
            
            let id = dict[UserDefaults.keys.userId] as? String ?? ""
            if id  != self.strUserId{
                self.txtPhoneNumber.text = objAppShareData.objModelBusinessSetup.countryCode+" - "+objAppShareData.objModelBusinessSetup.businessContactNo

            }
            objAppShareData.objModelBusinessSetup.address2 = objAppShareData.objModelBusinessSetup.address
            self.lblBusinessAddress.text = objAppShareData.objModelBusinessSetup.address2
            self.lblLocationAddress.text = objAppShareData.objModelBusinessSetup.address2
            self.lblRadiusOfArea.text = objAppShareData.objModelBusinessSetup.radius+" Miles"
            if self.lblRadiusOfArea.text == " Miles"{
                self.lblRadiusOfArea.text = ""
            }
          
        }
        
        override func viewWillAppear(_ animated: Bool) {
            
            if objAppShareData.objModelBusinessSetup.outCallpreprationTime == "HH:MM"{
                objAppShareData.objModelBusinessSetup.outCallpreprationTime = "00:00"
            }
            if objAppShareData.objModelBusinessSetup.inCallpreprationTime == "HH:MM"{
                objAppShareData.objModelBusinessSetup.inCallpreprationTime = "00:00"
            }
            self.btnSaveAndContinue.isHidden = true
            let dict = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] ?? ["":""]
            let id = dict[UserDefaults.keys.userId] as? String ?? ""
            self.img1.isHidden = true
            self.img2.isHidden = true
            self.img3.isHidden = true
            self.img4.isHidden = true
            self.img5.isHidden = true
            self.viewSupper.isHidden = false
            
            if objAppShareData.objModelBusinessSetup.bookingSetting ==  "0"{
                self.lblBookingSetting.text = "Automatic"
            } else if objAppShareData.objModelBusinessSetup.bookingSetting ==  "1"{
                self.lblBookingSetting.text = "Manual"
            }
            
            if objAppShareData.objModelBusinessSetup.paymentType == "1"{
                self.lblPaymentType.text = "Card"
            }else if objAppShareData.objModelBusinessSetup.paymentType == "2"{
                
                self.lblPaymentType.text = "Cash"
                
                objAppShareData.objModelBusinessSetup.paymentType = "2"
            }else if objAppShareData.objModelBusinessSetup.paymentType == "3"{
                self.lblPaymentType.text = "Both"
            }else{
//            objAppShareData.objModelBusinessSetup.paymentType = "1"
//                self.lblPaymentType.text = "Card"
            objAppShareData.objModelBusinessSetup.paymentType = "2  "
                self.lblPaymentType.text = "Cash"
            }
            
            self.viewBookingSetting.isHidden = true
            self.viewPaymentOption.isHidden = true
            if id == strUserId{
                self.viewBookingSetting.isHidden = false
                self.viewPaymentOption.isHidden = false
                self.viewSupper.isHidden = true
                self.img1.isHidden = false
                self.img2.isHidden = false
                self.img3.isHidden = false
                self.img4.isHidden = false
                self.img5.isHidden = false
                self.btnSaveAndContinue.isHidden = false
               
                if objAppShareData.fromAddressVC == false{
                    self.lblBusinessAddress.text = objAppShareData.objModelBusinessSetup.address
                }else{
                    if objAppShareData.paramAddress["address2"] ?? "" != ""{
                        self.lblBusinessAddress.text = objAppShareData.paramAddress["address2"] ?? ""
                        self.lblLocationAddress.text = objAppShareData.paramAddress["address2"] ?? ""
                    }
                }
                
                self.lblRadiusOfArea.text = objAppShareData.objModelBusinessSetup.radius+" Miles"
                if self.lblRadiusOfArea.text == " Miles"{
                    self.lblRadiusOfArea.text = ""
                }
                self.configureView(businessType: objAppShareData.objModelBusinessSetup.serviceType)
            }else{
                self.callWebserviceForGetCompanyInfo()
            }
            self.serviceBreakTime()
            self.buttonHiddenShowManage()
        }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
    
    
    @IBAction func btnBookingSetting(_ sender:Any){
        self.view.endEditing(true)
        let sb = UIStoryboard(name:"SignUpProcess",bundle:Bundle.main)
        let objChooseType = sb.instantiateViewController(withIdentifier:"BookingManageVC") as! BookingManageVC
        self.navigationController?.pushViewController(objChooseType, animated:true)
    }
    
    @IBAction func btnPaymentType(_ sender:Any){
        self.view.endEditing(true)
        let sb = UIStoryboard(name:"SignUpProcess",bundle:Bundle.main)
        let objChooseType = sb.instantiateViewController(withIdentifier:"PaymentOptionVC") as! PaymentOptionVC
        objChooseType.fromSignUp = false
        self.navigationController?.pushViewController(objChooseType, animated:true)
    }
}


//MARK: - UISearchBar Delegate
extension CompanyBusinessInfoVC:UISearchBarDelegate{
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String){
        searchAutocompleteEntries(withSubstring: searchText)
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
        btnCloseCountryView(0)
    }
    func searchAutocompleteEntries(withSubstring substring: String) {
        let searchString = substring.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let new = searchString.lowercased(with: Locale(identifier: "en"))
        if searchString.count > 0 {
            arrToShow = arrayCountryCode.filter({ $0.countryName.contains(searchString.uppercased()) || $0.countryName.contains(searchString.lowercased()) || $0.countryName.contains(searchString) || $0.dialCode.contains(searchString) || $0.countryCode.contains(searchString.uppercased()) || $0.countryName.contains(new) || $0.countryName.contains(new.capitalized);
            })
        }else {
            arrToShow = arrayCountryCode
        }
        self.arrToShow = self.arrToShow.sorted(by: { $0.countryName < $1.countryName })
        self.tblCountryCode.reloadData()
        if arrToShow.count>0 {
            self.lblNoResults.isHidden = true
        }else{
            self.lblNoResults.isHidden = false
        }
    }
    //Get CountryCoads
    func getCountryCode() {
        countryCode = "+1"
        let carrier = CTTelephonyNetworkInfo().subscriberCellularProvider
        
        if (carrier?.isoCountryCode?.uppercased()) != nil{
            strLocalCCode = (carrier?.isoCountryCode?.uppercased())!
        }
        do {
            if let file = Bundle.main.url(forResource: "countrycode", withExtension: "txt") {
                let data = try Data(contentsOf: file)
                let dictData = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                if let object = dictData as? [String : Any] {
                    // json is a dictionary
                    let arr = object["country"] as! [AnyObject]
                    for dicCountry in arr{
                        let objCountry = countryCodeDataEdit.init(countryName: dicCountry["name"] as? String, dialCode: dicCountry["dial_code"] as? String , countryCode: dicCountry["code"] as? String)
                        self.arrayCountryCode.append(objCountry)
                        if (dicCountry["code"] as? String ?? "" == strLocalCCode) {
                            countryCode = (dicCountry["dial_code"] as? String)!
                            self.btnCountryCode.setTitle(dicCountry["dial_code"] as? String, for: .normal) 
                        }
                    }
                    
                    self.arrToShow = self.arrayCountryCode.sorted(by: { $0.countryName < $1.countryName })
                    self.tblCountryCode.reloadData()
                } else if let object = dictData as? [Any] {
                    // json is an array
                } else {
                    //print("JSON is invalid")
                }
            } else {
                //print("no file")
            }
        } catch {
            //print(error.localizedDescription)
        }
    }
    
    // MARK: - keyboard methods
    func observeKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        let info = notification.userInfo
        let kbFrame = info?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
        let animationDuration = (info?[UIResponder.keyboardAnimationDurationUserInfoKey] ?? 0.0) as? TimeInterval
        let keyboardFrame: CGRect? = kbFrame?.cgRectValue
        let height: CGFloat? = keyboardFrame?.size.height
        bottomConstraint1.constant = height!
        UIView.animate(withDuration: animationDuration ?? 0.0, animations: {() -> Void in
            self.view.layoutIfNeeded()
        })
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        let info = notification.userInfo
        let animationDuration = (info?[UIResponder.keyboardAnimationDurationUserInfoKey] ?? 0.0) as! TimeInterval
        bottomConstraint1.constant = 8
        UIView.animate(withDuration: animationDuration , animations: {() -> Void in
            self.view.layoutIfNeeded()
        })
    }
}
//MARK: - UITableView Delegate and Datasource
extension CompanyBusinessInfoVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrToShow.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:"CountryCodeCell") as! CountryCodeCell
        let objCountry = self.arrToShow[indexPath.row]
        cell.lblCountry?.text = objCountry.countryName
        cell.lblCode?.text = objCountry.dialCode
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        let objCountry = self.arrToShow[indexPath.row]
        self.btnCountryCode.setTitle(objCountry.dialCode, for: .normal)
        self.countryCode = objCountry.dialCode
        
        if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.userInfo){
            let pn = dict[UserDefaults.keys.contactNo] as? String ?? ""
            let cc = dict[UserDefaults.keys.countryCode] as? String ?? ""
//            if self.txtPhoneNumber.text == pn && cc == self.lblCountryCode.text {
//                self.imgPhoneVerifi.isHidden = true
//                self.btnContactValidation.isHidden = true
//                objAppShareData.otpVerify = true
//            }else{
//                self.imgPhoneVerifi.isHidden = false
//                self.btnContactValidation.isHidden = false
//                objAppShareData.otpVerify = false
//            }
        }
        btnCloseCountryView(0)
    }
    
    @IBAction func btnCloseCountryView(_ sender:Any){
        UIView.animate(withDuration: 0.5) {
            self.countryCodeView.alpha = 0.0
            DispatchQueue.main.asyncAfter(deadline:.now() + 0.5) {
                self.countryCodeView.isHidden = true
            }
            self.view.endEditing(true)
        }
    }
}
    //MARK: - Custome method extension
    extension CompanyBusinessInfoVC{
        func configureView(businessType:String){
            if businessType == "1"{
                self.viewBusinessAddress.isHidden = false
                self.viewRadiurOfArea.isHidden = true
                self.viewReturnLocationAdd.isHidden = true
                self.lblBookinkType.text = "In Call"

            }else if businessType == "2"{
                self.lblDynamicAddressTytle.text = "Business  Address / Returning Location"

                self.viewBusinessAddress.isHidden = true
                self.viewRadiurOfArea.isHidden = false
                self.viewReturnLocationAdd.isHidden = false
                self.lblBookinkType.text = "Out Call"
            }else if businessType == "3" || businessType == "0"{
                self.lblDynamicAddressTytle.text = "Business  Address / Returning Location"

                self.viewBusinessAddress.isHidden = true
                self.viewRadiurOfArea.isHidden = false
                self.viewReturnLocationAdd.isHidden = false
                self.lblBookinkType.text = "In Call / Out Call"
            }else{
                self.viewBusinessAddress.isHidden = true
                self.viewRadiurOfArea.isHidden = true
                self.viewReturnLocationAdd.isHidden = true
            }
        }
            func serviceBreakTime() {
                
                self.lblCompletionTimeOutCall.isHidden = true
                self.lblCompletionTime.isHidden = true
                if objAppShareData.objModelBusinessSetup.serviceType == "1"{
                    self.lblCompletionTime.isHidden = false
                    let arr1 = objAppShareData.objModelBusinessSetup.inCallpreprationTime.components(separatedBy: ":")
                    if arr1.count > 1{
                        if arr1[0] == "00"{
                            self.lblCompletionTime.text = "Incall    : "+arr1[1]+" Minutes"
                        }else{
                            if arr1[1] == "00"{
                                self.lblCompletionTime.text = "Incall    : "+arr1[0]+" Hours "
                            }else{
                                self.lblCompletionTime.text = "Incall    : "+arr1[0]+" Hours "+arr1[1]+" Minutes"
                            }
                        }
                    }
                    //                self.lblCompletionTime.text = "Incall  : "+objAppShareData.objModelBusinessSetup.inCallpreprationTime
                }else if objAppShareData.objModelBusinessSetup.serviceType == "2"{
                    self.lblCompletionTimeOutCall.isHidden = false
                    
                    self.lblCompletionTimeOutCall.text = "Outcall : "+objAppShareData.objModelBusinessSetup.outCallpreprationTime
                    let arr1 = objAppShareData.objModelBusinessSetup.outCallpreprationTime.components(separatedBy: ":")
                    if arr1.count > 1{
                        if arr1[0] == "00"{
                            self.lblCompletionTimeOutCall.text = "Outcall : "+arr1[1]+" Minutes"
                        }else{
                            if arr1[1] == "00"{
                                self.lblCompletionTimeOutCall.text = "Outcall : "+arr1[0]+" Hours "
                            }else{
                                self.lblCompletionTimeOutCall.text = "Outcall : "+arr1[0]+" Hours "+arr1[1]+" Minutes"
                            }
                        }
                    }
                }else{
                    self.lblCompletionTimeOutCall.isHidden = false
                    self.lblCompletionTime.isHidden = false
                    var a =  "Incall    : "+objAppShareData.objModelBusinessSetup.inCallpreprationTime
                    var b = "Outcall : "+objAppShareData.objModelBusinessSetup.outCallpreprationTime
                    
                    let arr1 = objAppShareData.objModelBusinessSetup.inCallpreprationTime.components(separatedBy: ":")
                    if arr1.count > 1{
                        if arr1[0] == "00"{
                            a = "Incall    : "+arr1[1]+" Minutes"
                        }else{
                            if arr1[1] == "00"{
                                a = "Incall    : "+arr1[0]+" Hours "
                            }else{
                                a = "Incall    : "+arr1[0]+" Hours "+arr1[1]+" Minutes"
                            }
                        }
                    }
                    let arr2 = objAppShareData.objModelBusinessSetup.outCallpreprationTime.components(separatedBy: ":")
                    if arr2.count > 1{
                        if arr2[0] == "00"{
                            b = "Outcall : "+arr2[1]+" Minutes"
                        }else{
                            if arr2[1] == "00"{
                                b = "Outcall : "+arr2[0]+" Hours "
                            }else{
                                b = "Outcall : "+arr2[0]+" Hours "+arr2[1]+" Minutes"
                            }
                        }
                        
                        self.lblCompletionTime.text = a
                        self.lblCompletionTimeOutCall.text = b
                        
                    }
                
            }
                if self.lblCompletionTime.text == "Incall    : 00 Minutes" && self.lblCompletionTimeOutCall.text == "Outcall : 00 Minutes"{
                    self.lblCompletionTime.isHidden = true
                    self.lblCompletionTimeOutCall.isHidden = true
                }
        }
        
        
        
        func addAccesorryToKeyBoard(){
            let keyboardDoneButtonView = UIToolbar()
            keyboardDoneButtonView.sizeToFit()
            let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
            let doneButton = UIBarButtonItem(title: "Done", style:.plain, target: self, action: #selector(self.resignKeyBoard))
            doneButton.tintColor = appColor
            
            keyboardDoneButtonView.items = [flexBarButton, doneButton]
            
            txtPhoneNumber.inputAccessoryView = keyboardDoneButtonView
        }
        @objc func resignKeyBoard(){
            txtPhoneNumber.endEditing(true)
            self.view.endEditing(true)
        }
        
        func addressVC(strFromAddress:String){
            let sb = UIStoryboard(name:"SignUpProcess",bundle:Bundle.main)
            let objChooseType = sb.instantiateViewController(withIdentifier:"OutcallOptionVC") as! OutcallOptionVC
            objChooseType.fromAddress = strFromAddress
            objChooseType.address = self.lblBusinessAddress.text ?? ""
            self.navigationController?.pushViewController(objChooseType, animated:true)
        }
    }
    
    //MARK: - textfield method extension
    extension CompanyBusinessInfoVC{
        func textFieldShouldReturn(_ textField: UITextField) -> Bool{
            if textField == self.txtBusinessName{
                txtBusinessEmail.becomeFirstResponder()
            }else if textField == self.txtBusinessEmail{
                txtPhoneNumber.becomeFirstResponder()
            }else if textField == self.txtPhoneNumber{
                txtPhoneNumber.resignFirstResponder()
            }
            return true
        }
    }
    
    
    //MARK: - button extension
    extension CompanyBusinessInfoVC{
        @IBAction func btnBack(_ sender:Any){
            self.view.endEditing(true)
            let dict = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] ?? ["":""]
            objAppShareData.objAppdelegate.parseBusinessSignUpDetail(dictOfInfo: dict)
            self.navigationController?.popViewController(animated: true)
         }
        
        @IBAction func btnBookingType(_ sender:Any){
            self.view.endEditing(true)
            
            let sb = UIStoryboard(name:"SignUpProcess",bundle:Bundle.main)
            let objChooseType = sb.instantiateViewController(withIdentifier:"BusinessTimeSelectionVC") as! BusinessTimeSelectionVC
            self.navigationController?.pushViewController(objChooseType, animated: true)
        }
        
        @IBAction func btnContinew(_ sender:Any){
            self.view.endEditing(true)
            
            //self.GotoVerifyPhoneVC()
            // return
            var invalid = false
            var strMessage = ""
            txtBusinessEmail.text = txtBusinessEmail.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            txtBusinessName.text = txtBusinessName.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            
            txtPhoneNumber.text = txtPhoneNumber.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            var businessType =  objAppShareData.objModelBusinessSetup.serviceType
            
            if businessType == "" || businessType == "0"{
                objAppShareData.objModelBusinessSetup.serviceType = "3"
                businessType = "3"
            }
             if self.txtBusinessName.text?.count == 0{
                objAppShareData.shakeTextField(self.txtBusinessName)
                invalid = true
            }else if businessType == "" || businessType == "0"{
                invalid = true
                objAppShareData.showAlert(withMessage: "Please select booking type", type: alertType.banner, on: self)
            }else if self.txtPhoneNumber.text?.count == 0{
                objAppShareData.shakeTextField(self.txtPhoneNumber)
                invalid = true
            }else if self.txtBusinessEmail.text?.count == 0{
                objAppShareData.shakeTextField(self.txtBusinessEmail)
                invalid = true
            }else if !(objValidationManager.isValidateEmail(strEmail: self.txtBusinessEmail.text!)){
                strMessage = message.validation.email
                invalid = true
            }else if businessType == "1" && objAppShareData.objModelBusinessSetup.address2 == ""{
                invalid = true
                objAppShareData.showAlert(withMessage: "Please enter business address", type: alertType.banner, on: self)
            }else if businessType == "2" && objAppShareData.objModelBusinessSetup.address2 == ""{
                invalid = true
                objAppShareData.showAlert(withMessage: "Please enter return location address", type: alertType.banner, on: self)
            }else if businessType == "2" &&  objAppShareData.objModelBusinessSetup.radius == "0" {
                invalid = true
                objAppShareData.showAlert(withMessage: "Please select radius of area coverage", type: alertType.banner, on: self)
            }else if businessType == "3" && objAppShareData.objModelBusinessSetup.address2 == ""{
                invalid = true
                objAppShareData.showAlert(withMessage: "Please enter business address", type: alertType.banner, on: self)
                
            }else if businessType == "3" && (objAppShareData.objModelBusinessSetup.radius == "0" || objAppShareData.objModelBusinessSetup.radius == ""  || objAppShareData.objModelBusinessSetup.radius == " "){
                invalid = true
                objAppShareData.showAlert(withMessage: "Please select radius of area coverage", type: alertType.banner, on: self)
                
            }
//                else if objAppShareData.objModelBusinessSetup.businessHours != "1"{
//                invalid = true
//                objAppShareData.showAlert(withMessage: "Please select business operation hours", type: alertType.banner, on: self)
//            }
//             else if businessType == "1" && objAppShareData.objModelBusinessSetup.inCallpreprationTime == "00:00"{
//                invalid = true
//                objAppShareData.showAlert(withMessage: "Please select break between appointments time", type: alertType.banner, on: self)
//            }else if businessType == "2" && (objAppShareData.objModelBusinessSetup.outCallpreprationTime == "00:00" || objAppShareData.objModelBusinessSetup.outCallpreprationTime == "HH:MM"){
//                invalid = true
//                objAppShareData.showAlert(withMessage: "Please select break between appointments time", type: alertType.banner, on: self)
//            }else if businessType == "3" && (objAppShareData.objModelBusinessSetup.inCallpreprationTime == "00:00" || objAppShareData.objModelBusinessSetup.inCallpreprationTime == "HH:MM"){
//                invalid = true
//                objAppShareData.showAlert(withMessage: "Please select break between appointments time", type: alertType.banner, on: self)
//            }else if businessType == "3" && (objAppShareData.objModelBusinessSetup.outCallpreprationTime == "00:00" || objAppShareData.objModelBusinessSetup.outCallpreprationTime == "HH:MM"){
//                invalid = true
//                objAppShareData.showAlert(withMessage: "Please select break between appointments time", type: alertType.banner, on: self)
//            }
            
            
            if invalid && strMessage.count>0 {
                objAppShareData.showAlert(withMessage: strMessage, type: alertType.banner, on: self)
            }else if invalid == false{
                self.view.endEditing(true)
                objAppShareData.objModelBusinessSetup.countryCode = self.btnCountryCode.currentTitle ?? "+91"
                let businessContactNo = objAppShareData.objModelBusinessSetup.businessContactNo
                let inCallpreprationTime = objAppShareData.objModelBusinessSetup.inCallpreprationTime
                let outCallpreprationTime = objAppShareData.objModelBusinessSetup.outCallpreprationTime
                let businessEmail = objAppShareData.objModelBusinessSetup.businessEmail
                let businessName = objAppShareData.objModelBusinessSetup.businessName
                let countryCode = objAppShareData.objModelBusinessSetup.countryCode

                if objAppShareData.fromAddressVC == true && (objAppShareData.paramAddress[UserDefaults.keys.address2] ?? "" != "") {
                    objAppShareData.objModelBusinessSetup.location = objAppShareData.paramAddress["name"] ?? ""
                    objAppShareData.objModelBusinessSetup.address2 = objAppShareData.paramAddress[UserDefaults.keys.address2]   ?? ""
                    objAppShareData.objModelBusinessSetup.country = objAppShareData.paramAddress[UserDefaults.keys.country]  ?? ""
                    objAppShareData.objModelBusinessSetup.state = objAppShareData.paramAddress[UserDefaults.keys.state]   ?? ""
                    objAppShareData.objModelBusinessSetup.city = objAppShareData.paramAddress[UserDefaults.keys.city]   ?? ""
                   objAppShareData.objModelBusinessSetup.address = objAppShareData.paramAddress[UserDefaults.keys.address2]   ?? ""
                    objAppShareData.objModelBusinessSetup.latitude = objAppShareData.paramAddress[UserDefaults.keys.latitude]   ?? ""
                   objAppShareData.objModelBusinessSetup.longitude = objAppShareData.paramAddress[UserDefaults.keys.longitude]   ?? ""
                }
                let location = objAppShareData.objModelBusinessSetup.location
                let address2 = objAppShareData.objModelBusinessSetup.address2
                let country = objAppShareData.objModelBusinessSetup.country
                let state = objAppShareData.objModelBusinessSetup.state
                let city = objAppShareData.objModelBusinessSetup.city
                let radius = objAppShareData.objModelBusinessSetup.radius
                let address = objAppShareData.objModelBusinessSetup.address
                let latitude = objAppShareData.objModelBusinessSetup.latitude
                let longitude = objAppShareData.objModelBusinessSetup.longitude
                
                let incallTimeHours = objAppShareData.objModelBusinessSetup.incallTimeHours
                let incallTimeMinute = objAppShareData.objModelBusinessSetup.incallTimeMinute
                let outcallTimeHours = objAppShareData.objModelBusinessSetup.outcallTimeHours
                let outcallTimeMinute = objAppShareData.objModelBusinessSetup.outcallTimeMinute
                let serviceType = objAppShareData.objModelBusinessSetup.serviceType
                let bookingSetting = objAppShareData.objModelBusinessSetup.bookingSetting
                let param = ["businessContactNo":objAppShareData.objModelBusinessSetup.businessContactNo,
                             "inCallpreprationTime":inCallpreprationTime,
                             "outCallpreprationTime":outCallpreprationTime,
                             "businessEmail":self.txtBusinessEmail.text ?? "",
                             "businessName":self.txtBusinessName.text ?? "",
                             "location":location,
                             "address2":address2,
                             "country":country,
                             "state":state,
                             "city":city,
                             "radius":radius,
                             "address":address,
                             "latitude":latitude,
                             "longitude":longitude,
                             "serviceType":serviceType,
                             "isDocument":"3",
                             "incallTimeHours":incallTimeHours,
                             "incallTimeMinute":incallTimeMinute,
                             "outcallTimeHours":outcallTimeHours,
                             "outcallTimeMinute":outcallTimeMinute]
                
                
                let paramSend = ["businessContactNo":objAppShareData.objModelBusinessSetup.businessContactNo,
                                 "countryCode":countryCode,
                                 "inCallpreprationTime":inCallpreprationTime,
                                 "outCallpreprationTime":outCallpreprationTime,
                                 "businessEmail":self.txtBusinessEmail.text ?? "",
                                 "businessName":self.txtBusinessName.text ?? "",
                                 "serviceType":serviceType,
                                 "isDocument":"3",
                                 "location":"aap ke hisab se dekh lo",
                                 "address2":address2,
                                 "country":country,
                                 "state":state,
                                 "city":city,
                                 "radius":radius,
                                 "address":address,
                                 "latitude":latitude,
                                 "payOption":objAppShareData.objModelBusinessSetup.paymentType,
                                 "longitude":longitude,
                    "bookingSetting":bookingSetting]

                callWebserviceForRegistration(dict: paramSend)
            }
        }
        
        func callWebserviceForRegistration(dict:[String:Any]){
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            objActivity.startActivityIndicator()
            objServiceManager.requestPost(strURL: WebURL.updateRecord, params: dict, success:{ response in
                if response["status"] as? String == "success"{
                    if let dictUserDetail = response["user"] as? [String:Any]{
                        let message = response["message"] as? String ?? ""
                        //objAppShareData.showAlert(withMessage: message, type: alertType.banner, on: self)
                        objAppShareData.objAppdelegate.parseBusinessSignUpDetail(dictOfInfo: dictUserDetail)
                        self.gotoNextVC()
                    }else if let dictUserDetail = response["users"] as? [String:Any]{
                        let message = response["message"] as? String ?? ""
                        //objAppShareData.showAlert(withMessage: message, type: alertType.banner, on: self)
                        objAppShareData.objAppdelegate.parseBusinessSignUpDetail(dictOfInfo: dictUserDetail)
                        self.gotoNextVC()
                    }
                }else{
                    let msg = response["message"] as! String
                    objAppShareData.showAlert(withMessage: msg, type: alertType.banner, on: self)
                }
            }) { error in
                objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
            }
        }
        
        func callWebserviceForGetCompanyInfo(){
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            
            let dict = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] ?? ["":""]

            let id = dict[UserDefaults.keys.userId] as? String ?? ""
            
            let param = ["userId":self.strUserId,"loginUserId":id]
            
            objActivity.startActivityIndicator()
            objServiceManager.requestPost(strURL: WebURL.getProfile, params: param, success:{ response in
                if response["status"] as? String == "success"{
                    if let dictUserDetail = response["userDetail"] as? [[String:Any]]{
                        let message = response["message"] as? String ?? ""
                        self.parseBusinessSignUpDetail(dictOfInfo: dictUserDetail[0])
                        self.serviceBreakTime()
                        //objAppShareData.showAlert(withMessage: message, type: alertType.banner, on: self)
                    }else if let dictUserDetail = response["userDetail"] as? [String:Any]{
                        let message = response["message"] as? String ?? ""
                   self.serviceBreakTime()
                    }
                }else{
                    let msg = response["message"] as! String
                    objAppShareData.showAlert(withMessage: msg, type: alertType.banner, on: self)
                }
            }) { error in
                objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
            }
        }
        
        
        func gotoNextVC(){
            self.navigationController?.popViewController(animated: true)
        }
        
        @IBAction func btnBusinessHours(_ sender:Any){
            self.view.endEditing(true)
            
            let sb = UIStoryboard(name:"SignUpProcess",bundle:Bundle.main)
            let objChooseType = sb.instantiateViewController(withIdentifier:"BusinessHoursVC") as! BusinessHoursVC
            self.navigationController?.pushViewController(objChooseType, animated:true)
        }
        
        @IBAction func btnBusinessAddress(_ sender:Any){
            self.view.endEditing(true)
            
            let  businessType =  objAppShareData.objModelBusinessSetup.serviceType
            if businessType == ""{
                objAppShareData.showAlert(withMessage: "Please select booking type", type: alertType.banner, on: self)
            }else{
                let sb = UIStoryboard(name:"Main",bundle:Bundle.main)
                let objVC = sb.instantiateViewController(withIdentifier:"AddAddressVC") as! AddAddressVC
                objVC.isOutcallOption = true
                objVC.fromRadiusVC = false
                self.navigationController?.pushViewController(objVC, animated: true)
                //self.addressVC(strFromAddress: "0")
            }
        }
        
        @IBAction func btnRadiusOfAreaCoverage(_ sender:Any){
            self.view.endEditing(true)
            
            let  businessType =  objAppShareData.objModelBusinessSetup.serviceType
            if businessType == ""{
                objAppShareData.showAlert(withMessage: "Please select booking type", type: alertType.banner, on: self)
            }else if self.lblBusinessAddress.text == ""{
                objAppShareData.showAlert(withMessage: "Please enter business address", type: alertType.banner, on: self)
            }else{
                self.addressVC(strFromAddress: "2")
            }
        }
        
        @IBAction func btnReturnLocationAddress(_ sender:Any){
            self.view.endEditing(true)
            
            let  businessType = objAppShareData.objModelBusinessSetup.serviceType
            if businessType == ""{
                objAppShareData.showAlert(withMessage: "Please select booking type", type: alertType.banner, on: self)
            }else{
                
                let sb = UIStoryboard(name:"Main",bundle:Bundle.main)
                let objVC = sb.instantiateViewController(withIdentifier:"AddAddressVC") as! AddAddressVC
                objVC.isOutcallOption = true
                objVC.fromRadiusVC = false
                self.navigationController?.pushViewController(objVC, animated: true)
                // self.addressVC(strFromAddress: "1")
            }
        }
        
        @IBAction func btnBusinessBreakAppoinments(_ sender:Any){
            self.view.endEditing(true)
            
            let businessType = objAppShareData.objModelBusinessSetup.serviceType
            
            if businessType == "1" ||  businessType == "2" ||  businessType == "3"{
                let sb = UIStoryboard(name:"SignUpProcess",bundle:Bundle.main)
                let objChooseType = sb.instantiateViewController(withIdentifier:"SetTimeVC") as! SetTimeVC
                let dict = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] ?? ["":""]
                let id = dict[UserDefaults.keys.userId] as? String ?? ""
                if id  == strUserId{
                    objChooseType.isEditingView = true
                }else{
                    objChooseType.isEditingView = false
                }
                self.navigationController?.pushViewController(objChooseType, animated:true)
            }else{
                objAppShareData.showAlert(withMessage: "Please select booking type", type: alertType.banner, on: self)
                
            }
        }
}

fileprivate extension CompanyBusinessInfoVC{
    
    func parseBusinessSignUpDetail(dictOfInfo:[String:Any]){
        print("dictOfInfo = ",dictOfInfo)
        var  v = ""
        if let id = dictOfInfo["__v"] as? String{
            v = id
        }else if let id = dictOfInfo["__v"] as? Int{
            v = String(id)
        }
        
        var id1 = ""
        if let ids = dictOfInfo["_id"] as? String{
            id1 = ids
        }else if let ids = dictOfInfo["_id"] as? Int{
            id1 = String(ids)
        }else if let ids = dictOfInfo["_Id"] as? String{
            id1 = String(ids)
        }else if let ids = dictOfInfo["_Id"] as? Int{
            id1 = String(ids)
        }
        
        var bankStatus = ""
        if let id = dictOfInfo["bankStatus"] as? String{
            bankStatus = id
        }else if let id = dictOfInfo["bankStatus"] as? Int{
            bankStatus = String(id)
        }
        
        var certificateCount = ""
        if let id = dictOfInfo["certificateCount"] as? String{
            certificateCount = id
        }else if let id = dictOfInfo["certificateCount"] as? Int{
            certificateCount = String(id)
        }
        
        var chatId = ""
        if let id = dictOfInfo["chatId"] as? String{
            chatId = id
        }else if let id = dictOfInfo["chatId"] as? Int{
            chatId = String(id)
        }
        
        var deviceType = ""
        if let id = dictOfInfo["deviceType"] as? String{
            deviceType = id
        }else if let id = dictOfInfo["deviceType"] as? Int{
            deviceType = String(id)
        }
        
        var followersCount = ""
        if let id = dictOfInfo["followersCount"] as? String{
            followersCount = id
        }else if let id = dictOfInfo["followersCount"] as? Int{
            followersCount = String(id)
        }
        
        var followingCount = ""
        if let id = dictOfInfo["followingCount"] as? String{
            followingCount = id
        }else if let id = dictOfInfo["followingCount"] as? Int{
            followingCount = String(id)
        }
        
        var isDocument = ""
        if let id = dictOfInfo["isDocument"] as? String{
            isDocument = id
        }else if let id = dictOfInfo["isDocument"] as? Int{
            isDocument = String(id)
        }
 
        
        
        var radius = "0"
        if let id = dictOfInfo["radius"] as? String{
            radius = id
        }else if let id = dictOfInfo["radius"] as? Int{
            radius = String(id)
        }
        
        var serviceType = ""
        if let id = dictOfInfo["serviceType"] as? String{
            serviceType = id
        }else if let id = dictOfInfo["serviceType"] as? Int{
            serviceType = String(id)
        }
        
        var bookingSetting = ""
        if let id = dictOfInfo["bookingSetting"] as? String{
            bookingSetting = id
        }else if let id = dictOfInfo["bookingSetting"] as? Int{
            bookingSetting = String(id)
        }
        
        var businessHours = ""
        if let id = dictOfInfo["businesshours"] as? String{
            businessHours = id
        }else if let id = dictOfInfo["businesshours"] as? Int{
            businessHours = String(id)
        }

        var address = ""
        address = dictOfInfo["address"] as? String ?? ""
        
        var address2 = ""
        address2 = dictOfInfo["address2"] as? String ?? ""
        
        
    
        var businessName = "";
        businessName = dictOfInfo["businessName"] as? String ?? ""
        
//        var businessType = ""
//        businessType = dictOfInfo["businessType"] as? String ?? ""
        
//        var businesspostalCode = ""
//        businesspostalCode = dictOfInfo["businesspostalCode"] as? String ?? ""
        
        var city = ""
        city = dictOfInfo["city"] as? String ?? ""
        
//        var contactNo = ""
//        contactNo = dictOfInfo["contactNo"] as? String ?? ""
        
        var country = ""
        country = dictOfInfo["country"] as? String ?? ""
        
        var countryCode = ""
        countryCode = dictOfInfo["countryCode"] as? String ?? ""
        
 
 
        var inCallpreprationTime = "00:00"
        inCallpreprationTime = dictOfInfo["inCallpreprationTime"] as? String ?? "00:00"
 
        var state = ""
        state = dictOfInfo["state"] as? String ?? ""
 
        
        var outCallpreprationTime = "00:00"
        outCallpreprationTime = dictOfInfo["outCallpreprationTime"] as? String ?? "00:00"
        
        var longitude = ""
        longitude = dictOfInfo["longitude"] as? String ?? ""
        
        var latitude = ""
        latitude = dictOfInfo["latitude"] as? String ?? ""
        
        var businessEmail = ""
        businessEmail = dictOfInfo["businessEmail"] as? String ?? ""
        if businessEmail == ""{
            businessEmail = dictOfInfo["email"] as? String ?? ""
        }
        
        var businessContactNo = ""
        businessContactNo = dictOfInfo["businessContactNo"] as? String ?? ""
        if businessContactNo == ""{
            businessContactNo = dictOfInfo["contactNo"] as? String ?? ""
        }
        
        var location = ""
        location = dictOfInfo["location"] as? String ?? ""
 
        var incallTimeHours = "00"
        var incallTimeMinute = "00"
        
        if inCallpreprationTime == ""{
            inCallpreprationTime = "00:00"
        }
        let a = inCallpreprationTime.components(separatedBy: ":")
        incallTimeHours = "\(a[0])"
        incallTimeMinute =  "\(a[1])"
        
        var outcallTimeHours = "00"
        var outcallTimeMinute = "00"
        
        if outCallpreprationTime == ""{
            outCallpreprationTime = "00:00"
        }
        let b = outCallpreprationTime.components(separatedBy: ":")
        outcallTimeHours =  "\(b[0])"
        outcallTimeMinute = "\(b[1])"
        
        if serviceType == "" || serviceType == "0"{
            serviceType = "3"
        }

        UserDefaults.standard.setValue(serviceType, forKey: UserDefaults.keys.mainServiceType)

        objAppShareData.objModelBusinessSetup.serviceType = serviceType
        objAppShareData.objModelBusinessSetup.businessContactNo = businessContactNo
        objAppShareData.objModelBusinessSetup.inCallpreprationTime = inCallpreprationTime
        objAppShareData.objModelBusinessSetup.outCallpreprationTime = outCallpreprationTime
        objAppShareData.objModelBusinessSetup.businessEmail = businessEmail
        objAppShareData.objModelBusinessSetup.businessName = businessName
        objAppShareData.objModelBusinessSetup.countryCode = countryCode
        objAppShareData.objModelBusinessSetup.location = location
        if address2.count > 0{
          objAppShareData.objModelBusinessSetup.address2 = address2
        }else{
          objAppShareData.objModelBusinessSetup.address2 = address
        }
        objAppShareData.objModelBusinessSetup.country = country
        objAppShareData.objModelBusinessSetup.state = state
        objAppShareData.objModelBusinessSetup.city = city
        objAppShareData.objModelBusinessSetup.radius = radius
        objAppShareData.objModelBusinessSetup.address = address
        objAppShareData.objModelBusinessSetup.latitude = latitude
        objAppShareData.objModelBusinessSetup.longitude = longitude
        objAppShareData.objModelBusinessSetup.incallTimeHours = incallTimeHours
        objAppShareData.objModelBusinessSetup.incallTimeMinute = incallTimeMinute
        objAppShareData.objModelBusinessSetup.outcallTimeHours = outcallTimeHours
        objAppShareData.objModelBusinessSetup.outcallTimeMinute = outcallTimeMinute
        objAppShareData.objModelBusinessSetup.businessHours = businessHours
        objAppShareData.objModelBusinessSetup.bookingSetting = bookingSetting
        
        self.configureView(businessType: serviceType)
        self.parseDataFromLocal()
    }
}


extension CompanyBusinessInfoVC {
    func buttonHiddenShowManage(){
        let obj = objAppShareData.objModelBusinessSetup
        self.btnBookingSetting = self.buttonClearColor(btn: btnBookingSetting)
        
        
        if objAppShareData.objModelBusinessSetup.bookingSetting == "0"{
            self.lblBookingSetting.text = "Automatic"
        }else if objAppShareData.objModelBusinessSetup.bookingSetting == "1"{
            self.lblBookingSetting.text = "Manual"
        }else{
            self.lblBookingSetting.text = ""
            self.buttonWhiteColor(btn: btnBookingSetting)
        }
        
    }
    
    func buttonClearColor(btn:UIButton) -> UIButton{
        btn.backgroundColor = UIColor.clear
        btn.setTitleColor(UIColor.clear, for: .normal)
        return btn
    }
    func buttonWhiteColor(btn:UIButton)  -> UIButton{
        btn.backgroundColor = UIColor.white
        btn.setTitleColor(UIColor.black, for: .normal)
        return btn
    }
    
}



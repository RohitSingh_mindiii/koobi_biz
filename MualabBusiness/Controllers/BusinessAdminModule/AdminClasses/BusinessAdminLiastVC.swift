//
//  BusinessAdminLiastVC.swift
//  MualabBusiness
//
//  Created by Mac on 11/12/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import HCSStarRatingView
import DropDown
import CoreData
import Firebase

var viewHeightGloble = 0
class BusinessAdminLiastVC: UIViewController,SWRevealViewControllerDelegate,UITableViewDelegate,UITableViewDataSource {
    
    ////
       @IBOutlet weak var viewChatbadgeCount: UIView!
       @IBOutlet weak var lblChatbadgeCount: UILabel!
    ////
    
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var viewBlackLayer: UIView!
    
    //notification count
    @IBOutlet weak var viewNotificationCount: UIView!
    @IBOutlet weak var lblNotificationCount: UILabel!

    @IBOutlet weak var viewRating: HCSStarRatingView!
    @IBOutlet weak var imgHeaderDropDown: UIImageView!
  //  @IBOutlet weak var imgProfileHeaderOption: UIImageView!
    var managedContext:NSManagedObjectContext?
    @IBOutlet weak var lblBusinessName: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!

    @IBOutlet weak var stack1: UIStackView!
    @IBOutlet weak var stack2: UIStackView!
    @IBOutlet weak var stack3: UIStackView!
    @IBOutlet weak var stack4: UIView!
    
    @IBOutlet weak var viewWalkingListIcon: UIStackView!
    @IBOutlet weak var viewWalkingListIconRight: UIView!

    @IBOutlet weak var viewRatingStarOption: UIView!
    var ref: DatabaseReference!

    @IBOutlet weak var stackButtonStaffBusType: UIStackView!

    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var imgCertificate: UIImageView!
    
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblServiceType: UILabel!
    @IBOutlet weak var lblRattingCount: UILabel!
    
    @IBOutlet weak var btnStaff: UIButton!
    @IBOutlet weak var btnBusiness: UIButton!

    @IBOutlet weak var viewButtonStaff: UIView!
    @IBOutlet weak var viewButtonBusiness: UIView!
    
    @IBOutlet weak var viewBottumLine1: UIView!
    @IBOutlet weak var viewBottumLine2: UIView!
    private var arrCompanyList  = [CompanyInfo]()

    
    @IBOutlet weak var tblCompany: UITableView!
    @IBOutlet weak var viewBottumTable: UIView!
    @IBOutlet weak var HeightViewBottumTable: NSLayoutConstraint!
    
    @IBOutlet weak var viewInvitationBadgeCount: UIView!
    @IBOutlet weak var lblInvitationBadgeCount: UILabel!

    @IBOutlet weak var viewWalkingReminderBadgeCount: UIView!
    @IBOutlet weak var lblWalkingReminderBadgeCount: UILabel!
    @IBOutlet weak var viewWalkingReminder: UIView!
    @IBOutlet weak var viewClassOption: UIView!

    
    let dropDown = DropDown()
    fileprivate var companyId = ""
    fileprivate var staffAction = false
    fileprivate var strServiceCount = "0"
    fileprivate var isInvitation = 0
    fileprivate var isAdminComission = 0
    //fileprivate var arrBusinessType = [ModelAddBusinessType]()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewWalkingListIconRight.isHidden = false
        self.viewWalkingListIconRight.isHidden = true
        ref = Database.database().reference()
        self.badgeCount()
        self.addGesturesToView()
        self.tblCompany.delegate = self
        self.tblCompany.dataSource = self
        self.viewNotificationCount.isHidden = true
        self.viewWalkingReminderBadgeCount.isHidden = true
        self.viewWalkingReminder.isHidden = true
        self.viewClassOption.isHidden = true
        
        self.imgHeaderDropDown.isHidden = true
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        self.viewButtonStaff.isHidden = false
        self.viewButtonBusiness.isHidden = false
        self.viewBlackLayer.isHidden = true
        self.viewBottumTable.isHidden = true
        
        if revealViewController() != nil {
            revealViewController().setFrontViewPosition(FrontViewPosition.left, animated: true)
        }
        
        
        if revealViewController() != nil {
            revealViewController().rightViewRevealWidth = 265 //self.view.frame.size.width * 0.7
            menuButton.addTarget(revealViewController, action: #selector(SWRevealViewController.rightRevealToggle(_:)), for: .touchUpInside)
            self.revealViewController().delegate = self
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
         NotificationCenter.default.addObserver(self, selector: #selector(self.hideShowBlurViewForSlideMenu), name: NSNotification.Name(rawValue: "hideShowBlurView"), object: nil)
        
        managedContext = appDelegate.persistentContainer.viewContext
        self.stackButtonStaffBusType.isHidden = false
        self.selectBusinessAction()
        self.setuoDropDownAppearance()
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "refreshChatBadgeCount"), object: nil, queue: nil) { [weak self](Notification) in
            self?.refreshChatBadgeCount(Notification)
        }
    }

    @objc func hideShowBlurViewForSlideMenu(_ objNotify: Notification) {
        self.viewBottumTable.isHidden = true

        print(objNotify)
        self.view.endEditing(true)
        let dict = objNotify.userInfo as! [String:Any]
        let position = dict["xPosition"] as! String
        if position == "0"{
            self.viewBlackLayer.isHidden = true
            if viewHeightGloble == Int(self.view.frame.height) {
            self.tabBarController?.tabBar.isHidden = false
             self.tabBarController?.view.frame = CGRect(x:0, y:0, width:self.view.frame.size.width, height:self.view.frame.size.height)
            }
        }else if position == "1"{
            self.viewBlackLayer.isHidden = false
            if viewHeightGloble != Int(self.view.frame.height) {
            self.tabBarController?.tabBar.isHidden = true
            self.tabBarController?.view.frame = CGRect(x:0, y:0, width:self.view.frame.size.width, height:self.view.frame.size.height+(self.tabBarController?.tabBar.frame.size.height)!*2)
            }
        }
    }
    // UIGestureRecognizerDelegate method

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.viewWalkingListIconRight.isHidden = false
        self.viewWalkingListIconRight.isHidden = true
        self.scrollView.contentOffset.y = 0.0

        self.callWebserviceForUpdateMyInfo()
        self.viewBlackLayer.isHidden = true
        self.stack4.isHidden = true; self.viewWalkingListIcon.isHidden = true
        self.viewBottumTable.isHidden = true
        if revealViewController() != nil {
            revealViewController().setFrontViewPosition(FrontViewPosition.left, animated: true)
        }
        if DoneEditProfile == true{
            DoneEditProfile = false
            objAppShareData.showAlert(withMessage: "Profile updated successfully.", type: alertType.banner, on: self)
        }
        
        objAppShareData.fromDetailPage = false
        self.removePropertyData()
        objAppShareData.editSelfServices = false
        self.imgProfile.image = #imageLiteral(resourceName: "cellBackground")
    
        
        //self.imgProfileHeaderOption.image = #imageLiteral(resourceName: "cellBackground")
        
        let dict = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfoDic) as? [String:Any] ?? ["":""]
            let businessType = dict[UserDefaults.myDetail.businessType] as? String ?? ""
            var userName = dict[UserDefaults.myDetail.userName] as? String ?? ""
        if userName == ""{
            userName = "Business Admin"
        }
        if self.lblBusinessName.text == "Business Admin"  || self.lblBusinessName.text == userName {
         //   self.stackButtonStaffBusType.isHidden = false
            self.manageStaffButton()
            self.btnBusiness.setTitle("My Business", for: .normal)
        }else{
          //  self.stackButtonStaffBusType.isHidden = true
            self.viewButtonStaff.isHidden = true
            self.btnBusiness.setTitle("Business", for: .normal)

        }
        if self.lblBusinessName.text == "Business Admin"{
            self.lblBusinessName.text  = userName
        }
      if let dict = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]{
        var artistId = dict[UserDefaults.keys.userId] as? String ?? ""

        if let id = dict[UserDefaults.keys.userId] as? String{
            artistId = id
        }else if let id  = dict[UserDefaults.keys.userId] as? Int{
            artistId = String(id)
        }
        let dict2 = ["artistId":artistId]

        self.callWebserviceForMyInfo()
        self.callWebserviceForCompanyList(dict: dict2)

        if let dict = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfoDic) as? [String:Any]{
            let businessType = dict[UserDefaults.myDetail.businessType] as? String ?? ""
            if businessType != "business"{
            if self.isInvitation != 0{
                self.stack4.isHidden = false; self.viewWalkingListIcon.isHidden = false
                self.viewWalkingListIcon.isHidden = true
            }
            }
        }else{
            self.imgHeaderDropDown.isHidden = true
        }
        
        self.lblUserName.text = dict[UserDefaults.myDetail.userName] as? String ?? ""
        self.lblRattingCount.text = "("+(dict[UserDefaults.myDetail.reviewCount] as? String  ?? "")+")"
        let a = dict[UserDefaults.myDetail.ratingCount] as? String ?? "0"
        guard let n = NumberFormatter().number(from: a) else { return }
        self.viewRating.value = CGFloat(truncating: n)
 
        let serviceType =  dict[UserDefaults.myDetail.serviceType] as? String ?? ""

        if serviceType == "1"{
            self.lblServiceType.text = "In Call"
        }else if serviceType == "2"{
            self.lblServiceType.text = "Out Call"
        }else{ self.lblServiceType.text = "Both"}
        let img = dict[UserDefaults.myDetail.profileImage] as? String ?? ""
        if img !=  "" {
            if let url = URL(string: img){
                //self.imgProfile.af_setImage(withURL: url, placeholderImage: UIImage(named: "cellBackground"))
                self.imgProfile.sd_setImage(with: url, placeholderImage: UIImage(named: "cellBackground"))
//    self.imgProfileHeaderOption.af_setImage(withURL: url, placeholderImage: UIImage(named: "cellBackground"))
            }
        }
        }
        self.setOldData()
        if self.staffAction == true{
            self.selectStaffAction(fromViewwillappear: true)
        }else{
            self.selectBusinessAction()
        }
    //    self.manageStaffButton()
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshChatBadgeCount"), object: nil)

  }
 
    override func viewDidAppear(_ animated: Bool) {
        self.viewBottumTable.isHidden = true
        if let dict = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfoDic) as? [String:Any]{
            let businessType = dict[UserDefaults.myDetail.businessType] as? String ?? ""
            if businessType != "business"{
                if self.isInvitation != 0{
                    self.stack4.isHidden = false; self.viewWalkingListIcon.isHidden = false
                    self.viewWalkingListIcon.isHidden = true
                }
            }
        }
        if let dict = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]{
        self.lblRattingCount.text = "("+(dict[UserDefaults.myDetail.reviewCount] as? String  ?? "")+")"
        let a = dict[UserDefaults.myDetail.ratingCount] as? String ?? "0"
        guard let n = NumberFormatter().number(from: a) else { return }
        self.viewRating.value = CGFloat(truncating: n)
        }
        print(" self.viewHeight  height ",viewHeightGloble)
        print("self.view.frame height ",self.tabBarController?.view.frame.height)

        if  viewHeightGloble < Int(self.tabBarController?.view.frame.height ?? CGFloat(viewHeightGloble)) || self.tabBarController?.tabBar.isHidden == true{
            self.tabBarController?.tabBar.isHidden = false
            let b = Int(self.view.frame.width)
            let a = CGRect(x:0,y:0,width:b,height:viewHeightGloble)
            self.tabBarController?.view.frame = a//CGRect(x:0, y:0, width:self.view.frame.size.width, height:viewHeightGloble)
        }
    }
    
    func hideRevealViewController(){
        if revealViewController() != nil {
            revealViewController().setFrontViewPosition(FrontViewPosition.left, animated: true)
        }
    }
    
    @IBAction func btnManu(_ sender: UIButton) {
        self.view.endEditing(true)
        self.tabBarController?.tabBar.isHidden = true
        self.tabBarController?.view.frame = CGRect(x:0, y:0, width:self.view.frame.size.width, height:self.view.frame.size.height+(self.tabBarController?.tabBar.frame.size.height)!*2)
        self.viewBlackLayer.isHidden = false
    }
    
    
}

//MARK: - table view method extension
extension BusinessAdminLiastVC{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrCompanyList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "CellBottumTableList"
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? CellBottumTableList{
            let obj = self.arrCompanyList[indexPath.row]
            cell.lblTitle.text = obj.businessName//.capitalized
            let url = URL(string: obj.profileImage)
            if url != nil{
                //cell.imgCompany.af_setImage(withURL: url!)
                cell.imgCompany.sd_setImage(with: url!, placeholderImage: UIImage(named: "cellBackground"))
            }else{
                cell.imgCompany.image =  #imageLiteral(resourceName: "ShareImageOnSocial")
            }
            return cell
        }
        return UITableViewCell()
        }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if  viewHeightGloble < Int(self.tabBarController?.view.frame.height ?? CGFloat(viewHeightGloble)) || self.tabBarController?.tabBar.isHidden == true{
            self.tabBarController?.tabBar.isHidden = false
            let b = Int(self.view.frame.width)
            let a = CGRect(x:0,y:0,width:b,height:viewHeightGloble)
            self.tabBarController?.view.frame = a//CGRect(x:0, y:0, width:self.view.frame.size.width, height:viewHeightGloble)
        }
        let objCompany = self.arrCompanyList[indexPath.row]
        self.viewBottumTable.isHidden = true
        if let dict = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfoDic) as? [String:Any]{
            let businessName = dict[UserDefaults.myDetail.businessName] as? String ?? ""
            let userName = dict[UserDefaults.myDetail.userName] as? String ?? ""
                   self.view.endEditing(true)
                     self.lblBusinessName.text = objCompany.businessName
                    
                    self.staffAction = false
                    self.selectBusinessAction()
                    
                    if self.lblBusinessName.text == "Business Admin"  ||  self.lblBusinessName.text == businessName ||  self.lblBusinessName.text == userName{
                        //  self.stackButtonStaffBusType.isHidden = false
                        self.stack2.isHidden = false;
                        if self.isAdminComission != 0 {
                            viewWalkingReminder.isHidden = false
                        }
                        self.viewWalkingListIconRight.isHidden = false
                        self.viewWalkingListIconRight.isHidden = true
                        if self.isInvitation != 0{
                            if let dict = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfoDic) as? [String:Any]{
                                let businessType = dict[UserDefaults.myDetail.businessType] as? String ?? ""
                                if businessType != "business"{
                                    self.stack4.isHidden = false; self.viewWalkingListIcon.isHidden = false
                                    self.viewWalkingListIcon.isHidden = true
                                }}
                        }
                        self.manageStaffButton()
                        self.btnBusiness.setTitle("My Business", for: .normal)
                    }else{
                        self.btnBusiness.setTitle("Business", for: .normal)
                        //  self.stackButtonStaffBusType.isHidden = true
                        self.stack2.isHidden = true;self.viewWalkingListIcon.isHidden = true;self.viewWalkingListIconRight.isHidden = true; viewWalkingReminder.isHidden = true
                        self.stack4.isHidden = true; self.viewWalkingListIcon.isHidden = true
                        self.viewButtonStaff.isHidden = true
                    }
                                self.companyId =  objCompany.businessId
                                UserDefaults.standard.set(objCompany.businessId, forKey: UserDefaults.keys.newAdminId)
                                UserDefaults.standard.set(objCompany.serviceType, forKey: UserDefaults.keys.newAdminBookingType)
                                self.manageStaffButton()

            }
        }
}

    


//MARK: - Custome method extension
extension BusinessAdminLiastVC{
    
    func selectBusinessAction(){
        if stack3.isHidden == false{
            self.stack1.isHidden = false
            self.stack2.isHidden = false;
            if self.isAdminComission != 0{
                viewWalkingReminder.isHidden = false
            }
            self.stack3.isHidden = true
            self.viewRatingStarOption.isHidden = false
            if self.isAdminComission != 0{
               self.viewWalkingReminder.isHidden = false
            }
            self.viewClassOption.isHidden = true
            self.viewBottumLine1.backgroundColor = appColor
            self.viewBottumLine2.backgroundColor = UIColor.white
            self.btnStaff.setTitleColor(UIColor.lightGray, for: .normal)
            self.btnBusiness.setTitleColor(appColor, for: .normal)
            if let dict = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfoDic) as? [String:Any]{
                let businessType = dict[UserDefaults.myDetail.businessType] as? String ?? ""
                if businessType != "business"{
                    
                    if self.isInvitation != 0{
                        self.stack4.isHidden = false; self.viewWalkingListIcon.isHidden = false
                        self.viewWalkingListIcon.isHidden = true
                    }
                }
                if businessType == "independent" && self.arrCompanyList.count == 1{
                    if self.isInvitation != 0{
                        self.stack4.isHidden = false; self.viewWalkingListIcon.isHidden = false
                        self.viewWalkingListIcon.isHidden = true
                    }
                }
            }
        }
    }
    
    func manageStaffButton(){
        if let dict = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfoDic) as? [String:Any]{
             let businessType = dict[UserDefaults.myDetail.businessType] as? String ?? ""
            var businessName = dict[UserDefaults.myDetail.businessName] as? String ?? ""
           
            //   if (businessType == "independent" && self.arrCompanyList.count == 1) || businessType == "business"{
            self.viewWalkingListIcon.isHidden = false
            self.viewWalkingListIcon.isHidden = true
            self.viewWalkingListIconRight.isHidden = false
            self.viewWalkingListIconRight.isHidden = true
            if self.isAdminComission != 0{
               self.viewWalkingReminder.isHidden = false
            }
            if self.viewInvitationBadgeCount.isHidden == false{
                self.viewWalkingListIcon.isHidden = false
                self.viewWalkingListIcon.isHidden = true
            }else{
                self.viewWalkingListIcon.isHidden = true
            }
            
            if businessName == "" || self.strServiceCount == "0"{
                self.viewButtonStaff.isHidden = true
                self.viewWalkingListIcon.isHidden = true
                self.viewWalkingListIconRight.isHidden = true
                self.viewWalkingReminder.isHidden = true
            }else{
                self.viewButtonStaff.isHidden = false
                
                let adminId = UserDefaults.standard.string(forKey: UserDefaults.keys.newAdminId) ?? ""
                let myId = dict[UserDefaults.keys.id]  as? String ?? ""
                
                if adminId == myId || adminId == ""{
                    self.viewButtonStaff.isHidden = false
                    
                }else{
                    self.viewWalkingListIcon.isHidden = true
                    self.viewWalkingListIconRight.isHidden = true
                    self.viewWalkingReminder.isHidden = true

                    self.viewButtonStaff.isHidden = true
                }
            }
        }
        if self.staffAction == true{
            self.viewWalkingListIcon.isHidden = true
            self.viewWalkingListIconRight.isHidden = true
            self.viewWalkingReminder.isHidden = true
        }
    }
    
    
    func selectStaffAction(fromViewwillappear:Bool){
        if let dict = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfoDic) as? [String:Any]{
            
            let businessType = dict[UserDefaults.myDetail.businessType] as? String ?? ""
            
            var  userName = dict[UserDefaults.myDetail.userName] as? String ?? ""
            if userName == ""{
                userName = "Business Admin"
            }
         //   if (businessType == "independent" && self.arrCompanyList.count == 1) || businessType == "business"{
            if (self.lblBusinessName.text == "Business Admin" ||  self.lblBusinessName.text == userName ) && (self.strServiceCount != "0" && self.strServiceCount != "" ){
            self.stack1.isHidden = true
            self.stack2.isHidden = true;self.viewWalkingListIcon.isHidden = true;self.viewWalkingListIconRight.isHidden = true; viewWalkingReminder.isHidden = true
            self.stack3.isHidden = false
            self.stack4.isHidden = true;
            self.viewWalkingListIcon.isHidden = true
            self.viewWalkingListIcon.isHidden = true
            self.viewBottumLine1.backgroundColor = UIColor.white
            self.viewBottumLine2.backgroundColor = appColor
            self.btnStaff.setTitleColor(appColor, for: .normal)
            self.btnBusiness.setTitleColor(UIColor.lightGray, for: .normal)
                self.viewRatingStarOption.isHidden = true
                self.viewWalkingReminder.isHidden = true
                self.viewClassOption.isHidden = true
            }else{
                if fromViewwillappear == false{
                objAppShareData.showAlert(withMessage:  "Please update business profile before adding staff", type: alertType.bannerDark, on: self)
                }
            }
    }
        self.viewWalkingListIconRight.isHidden = true

    }
    func setuoDropDownAppearance(){
        let appearance = DropDown.appearance()
        appearance.cellHeight = 40
        appearance.backgroundColor = UIColor.white
        appearance.selectionBackgroundColor = UIColor.white
        appearance.separatorColor = #colorLiteral(red: 0.7202966371, green: 0.7202966371, blue: 0.7202966371, alpha: 1)
        appearance.cornerRadius = 5
        appearance.shadowColor = #colorLiteral(red: 0.7202966371, green: 0.7202966371, blue: 0.7202966371, alpha: 1)
        appearance.shadowOpacity = 0.9
        appearance.shadowRadius = 2
        appearance.animationduration = 0.25
        appearance.textColor = .darkGray

    }

    
    @IBAction func btnProfileImageZoom(_ sender:Any){
        let sb = UIStoryboard(name: "Add", bundle: Bundle.main)
        if let objShowImage = sb.instantiateViewController(withIdentifier:"showImagesVC") as? showImagesVC{
            var arr = [Any]()
            arr.append(self.imgProfile.image)
            objShowImage.isTypeIsUrl = false
            objShowImage.arrFeedImages = arr
            objShowImage.modalPresentationStyle = .fullScreen
            present(objShowImage, animated: true)
        }
    }
}

//MARK: - button extension
extension BusinessAdminLiastVC{
    
    @objc func refreshChatBadgeCount(_ objNotify: Notification){
        if objChatShareData.chatBadgeCount == 0{
            self.viewChatbadgeCount.isHidden = true
        }else{
            self.lblChatbadgeCount.text = String(objChatShareData.chatBadgeCount)
            self.viewChatbadgeCount.isHidden = false
        }
    }
    @IBAction func btnChatAction(_ sender: UIButton) {
        let sb: UIStoryboard = UIStoryboard(name: "Chat", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"ChatHistoryVC") as? ChatHistoryVC{
            objVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(objVC, animated: true)
        }
    }
    
    @IBAction func btnWalkingCustomerList(_ sender: UIButton) {
        self.view.endEditing(true)
        let sbNew: UIStoryboard = UIStoryboard(name: "BookingNew", bundle: Bundle.main)
        if let objVC = sbNew.instantiateViewController(withIdentifier:"WalkingListVC") as? WalkingListVC{
            objVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(objVC, animated: true)
        }
    }
    @IBAction func btnProfileOption(_ sender: UIButton) {
        self.view.endEditing(true)
        let sb: UIStoryboard = UIStoryboard(name: "Notification", bundle: Bundle.main)
//        if let objVC = sb.instantiateViewController(withIdentifier:"SWRevealViewController") as? SWRevealViewController{
//            objVC.hidesBottomBarWhenPushed = true
//            let dictnnn : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.businessInfoDic) ?? ["":""]
//                    objAppShareData.selectedOtherIdForProfile  = dictnnn[UserDefaults.keys.id] as? String ?? ""
//                    objAppShareData.isOtherSelectedForProfile = false
//                    objAppShareData.selectedOtherTypeForProfile = dictnnn[UserDefaults.keys.userType] as? String ?? ""
//            navigationController?.pushViewController(objVC, animated: true)
//        }
        let objChooseType = sb.instantiateViewController(withIdentifier:"NotificationVC") as! NotificationVC
       // if userId == self.companyId || self.companyId == ""{
       //     objChooseType.userId = userId
      //  }else{
          //  objChooseType.userId = self.companyId
      //  }
      //  objChooseType.userType = userType
        objChooseType.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(objChooseType, animated: true)
    }
    
    
    
    
    @IBAction func btnReviewAction(_ sender: UIButton) {
        let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] ??  ["":""]
        
        var userId = "0"
        var userType = ""
        //        if self.isOtherSelectedForProfile {
        //            userId = self.selectedOtherIdForProfile
        //        }else{
        //            userId = myId
        //        }
        userId = decoded[UserDefaults.keys.id] as? String ??  ""
        userType = decoded[UserDefaults.keys.userType] as? String ??  ""
        print(userType)
        self.view.endEditing(true)
        let sb = UIStoryboard(name:"Review",bundle:Bundle.main)
        let objChooseType = sb.instantiateViewController(withIdentifier:"ReviewListVC") as! ReviewListVC
        if userId == self.companyId || self.companyId == ""{
            objChooseType.userId = userId
        }else{
            objChooseType.userId = self.companyId
        }
        objChooseType.userType = userType
        objChooseType.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(objChooseType, animated: true)
    }
    
    @IBAction func btnPaymentSetup(_ sender: UIButton) {
        let sb = UIStoryboard(name:"SignUpProcess",bundle:Bundle.main)
        let objChooseType = sb.instantiateViewController(withIdentifier:"AddBankDetailVC") as! AddBankDetailVC
        objAppShareData.editSelfServices = true
        objChooseType.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(objChooseType, animated: true)
    }
    
    @IBAction func btnSelectBusinessAdminHeader(_ sender: UIButton) {
        if let dict = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfoDic) as? [String:Any]{
            let businessType = dict[UserDefaults.myDetail.businessType] as? String ?? ""
            let businessName = dict[UserDefaults.myDetail.businessName] as? String ?? ""
            let userName = dict[UserDefaults.myDetail.userName] as? String ?? ""

        if self.arrCompanyList.count > 1{
            if self.arrCompanyList.count > 1 {
                dropDown.show()
                self.HeightViewBottumTable.constant = CGFloat(self.arrCompanyList.count*50)
                self.viewBottumTable.isHidden = false
                self.tblCompany.reloadData()
                self.tabBarController?.tabBar.isHidden = true
                self.tabBarController?.view.frame = CGRect(x:0, y:0, width:self.view.frame.size.width, height:self.view.frame.size.height+(self.tabBarController?.tabBar.frame.size.height)!*2)
            }else{
                //dropDown.show()
            }
            }
        }
    }
    
    
    func setOldData(){
        if self.staffAction == true{
            self.viewWalkingListIcon.isHidden = true
            self.viewWalkingListIconRight.isHidden = true
            self.viewWalkingReminder.isHidden = true
        }
        if let dict = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfoDic) as? [String:Any]{
            let businessType = dict[UserDefaults.myDetail.businessType] as? String ?? ""
            var userName = dict[UserDefaults.myDetail.userName] as? String ?? ""
            var businessName = dict[UserDefaults.myDetail.businessName] as? String ?? ""
            if businessType != "business"{
                let adminId = UserDefaults.standard.string(forKey: UserDefaults.keys.newAdminId) ?? ""
                let myId = dict[UserDefaults.keys.id]  as? String ?? ""
                if adminId == myId || adminId == ""{
                    if userName == ""{
                        self.lblBusinessName.text = "Business Admin"
                    }else{
                        self.lblBusinessName.text = userName
                    }
                    self.companyId =  myId
                    UserDefaults.standard.set(myId, forKey: UserDefaults.keys.newAdminId)
                }else{
                    self.companyId =  adminId
                        for obj in self.arrCompanyList{
                            if self.companyId == obj.businessId{
                                self.lblBusinessName.text = obj.businessName
                            }
                        }
                    UserDefaults.standard.set(adminId, forKey: UserDefaults.keys.newAdminId)
            }
                if userName == ""{
                    userName = "Business Admin"
                }
    if self.lblBusinessName.text == "Business Admin" || self.lblBusinessName.text == userName  || self.lblBusinessName.text == businessName {
         //   self.stackButtonStaffBusType.isHidden = false
        if let dict = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfoDic) as? [String:Any]{
            let businessType = dict[UserDefaults.myDetail.businessType] as? String ?? ""
            if businessType != "business"{
            self.stack2.isHidden = false;
                if self.isAdminComission != 0 {
                    viewWalkingReminder.isHidden = false
                }
        if self.isInvitation != 0{
            self.stack4.isHidden = false; self.viewWalkingListIcon.isHidden = false
                self.viewWalkingListIcon.isHidden = true
                }}}
        self.manageStaffButton()
    }else{
         //   self.stackButtonStaffBusType.isHidden = true
            self.stack2.isHidden = true;self.viewWalkingListIcon.isHidden = true;self.viewWalkingListIconRight.isHidden = true; viewWalkingReminder.isHidden = true
            self.stack4.isHidden = true; self.viewWalkingListIcon.isHidden = true
        self.viewButtonStaff.isHidden = true
    }
        }
    }
        if self.staffAction == true{
            self.selectStaffAction(fromViewwillappear: true)
            self.viewWalkingListIcon.isHidden = true
            self.viewWalkingListIconRight.isHidden = true
            self.viewWalkingReminder.isHidden = true
        }else{
            self.selectBusinessAction()
        }
    }
    
    
    @IBAction func btnBusinessSelection(_ sender: UIButton) {
        self.staffAction = false
        self.viewWalkingListIconRight.isHidden = false
        self.viewWalkingListIconRight.isHidden = true
        self.selectBusinessAction()
    }
    
    @IBAction func btnStaffSelection(_ sender: UIButton) {
        self.staffAction = true
        self.selectStaffAction(fromViewwillappear: false)
        }
    @IBAction func btnHiddenBottumTable(_ sender: UIButton) {
         self.viewBottumTable.isHidden = true
        if  viewHeightGloble < Int(self.tabBarController?.view.frame.height ?? CGFloat(viewHeightGloble)) || self.tabBarController?.tabBar.isHidden == true{
            self.tabBarController?.tabBar.isHidden = false
            let b = Int(self.view.frame.width)
            let a = CGRect(x:0,y:0,width:b,height:viewHeightGloble)
            self.tabBarController?.view.frame = a//CGRect(x:0, y:0, width:self.view.frame.size.width, height:viewHeightGloble)
        }
    }
    
    @IBAction func btnBusinessInfo(_ sender: UIButton) {
        let sb = UIStoryboard(name:"BusinessAdmin",bundle:Bundle.main)
        
        for objArr in self.arrCompanyList{
            if objArr.businessName == self.lblBusinessName.text{
                self.companyId = objArr.businessId
            }
        }
        let dict = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] ?? ["":""]
 
        let myId = dict[UserDefaults.keys.userId]as? String ?? ""
//        if self.companyId == myId || self.companyId == "" {
//            objAppShareData.showAlert(withMessage: "Under development", type: alertType.bannerDark, on: self)
//        }else{
            if let objVC = sb.instantiateViewController(withIdentifier:"CompanyBusinessInfoVC") as? CompanyBusinessInfoVC{
                objVC.strUserId = self.companyId
                objVC.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(objVC, animated: true)
            //}
        }

    }
 
    @IBAction func btnWalkinReminderAction(_ sender: UIButton) {
        let sb = UIStoryboard(name:"BusinessAdmin",bundle:Bundle.main)
        for objArr in self.arrCompanyList{    if objArr.businessName == self.lblBusinessName.text{
                self.companyId = objArr.businessId   }
        }
        let dict = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] ?? ["":""]
        let myId = dict[UserDefaults.keys.userId]as? String ?? ""
        if let objVC = sb.instantiateViewController(withIdentifier:"ReminderListVC") as? ReminderListVC{
            objVC.strUserId = self.companyId
            objVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(objVC, animated: true)
        }
    }
    
    @IBAction func btnServices(_ sender: UIButton) {
        let dict = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] ?? ["":""]
        var staffId = ""
        for objArr in self.arrCompanyList{
            if objArr.businessName == self.lblBusinessName.text{
                self.companyId = objArr.businessId
                staffId = objArr._id
            }
        }
        let myId = dict[UserDefaults.keys.userId]as? String ?? ""
        if self.companyId == myId || self.companyId == "" {
           
            let name = dict[UserDefaults.keys.businessName] as? String ?? ""
            let businessHour = UserDefaults.standard.string(forKey: UserDefaults.keys.businessHourCount) ?? "0"
            if name == ""{
                objAppShareData.showAlert(withMessage:  "Please update business info before adding services", type: alertType.bannerDark, on: self)
                return
            }else if businessHour == "0"{
                objAppShareData.showAlert(withMessage:  "Please Complete your business operation hours", type: alertType.bannerDark, on: self)
                return
            }
            let sb = UIStoryboard(name:"BusinessAdmin",bundle:Bundle.main)
            let objChooseType = sb.instantiateViewController(withIdentifier:"NewServiceListVC") as! NewServiceListVC
            objAppShareData.editSelfServices = true
            objChooseType.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(objChooseType, animated: false)
    }else{
        let sb = UIStoryboard(name:"BusinessAdmin",bundle:Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"ServiceListCompanyVC") as? ServiceListCompanyVC{
            objVC.businessId = self.companyId
            objVC.staffId = staffId
            objVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(objVC, animated: true)
        }
        }
    }
    
    @IBAction func btnInvitation(_ sender: UIButton) {
        let sb = UIStoryboard(name:"Invitation",bundle:Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"InvitationListVC") as? InvitationListVC{
            objVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(objVC, animated: true)
        }
    }
    
    @IBAction func btnMessage(_ sender: UIButton) {
        objAppShareData.showAlert(withMessage: "Under development", type: alertType.bannerDark, on: self)
    }
    
    @IBAction func btnWorkingHours(_ sender: UIButton) {
        objAppShareData.editSelfServices = true
       var arrNew =  self.arrCompanyList
       let dict = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] ?? ["":""]
      let businessName = dict[UserDefaults.keys.businessName] as? String ?? ""
        for objArr in self.arrCompanyList{
            if objArr.businessName == self.lblBusinessName.text{
                self.companyId = objArr.businessId
            }
        }
        
        let myId = dict[UserDefaults.keys.userId]as? String ?? ""
        if self.companyId != myId{
        for objarrNew in arrNew{
            if objarrNew.businessId == self.companyId{
        let sloatArray = objarrNew.staffHours
        if sloatArray.count > 0{
            for dicSloats in sloatArray{
                
                var strSrtartTime = ""
                var strEndTime = ""
                var day = ""
                
                strSrtartTime = dicSloats.startTime
                strEndTime = dicSloats.endTime
                day = String(dicSloats.day)
                day = objAppShareData.getDayFromSelectedDate(strDate: day)
                
                if objAppShareData.objModelEditTimeSloat.arrFinalTimes.count > 0 {
                    var addtrue = true
                    for dayChack in objAppShareData.objModelEditTimeSloat.arrFinalTimes{
                        if day == dayChack.strDay{
                            addtrue = false
                            let objSloats = timeSloat.init(startTime: strSrtartTime, endTime: strEndTime)
                            dayChack.arrTimeSloats.append(objSloats!)
                        }
                    }
                    if addtrue == true{
                        let objSloats = timeSloat.init(startTime: strSrtartTime, endTime: strEndTime)
                        let objTimaSloat = openingTimes(open: true, day: day, times: [objSloats!])
                        objAppShareData.objModelEditTimeSloat.arrFinalTimes.append(objTimaSloat!)
                    }
                }else{
                    let objSloats = timeSloat.init(startTime: strSrtartTime, endTime: strEndTime)
                    let objTimaSloat = openingTimes(open: true, day: day, times: [objSloats!])
                    objAppShareData.objModelEditTimeSloat.arrFinalTimes.append(objTimaSloat!)
                    
                }
            }
        }
                let sb = UIStoryboard(name:"BusinessAdmin",bundle:Bundle.main)
                if let objVC = sb.instantiateViewController(withIdentifier:"workingHoursCompanyVC") as? workingHoursCompanyVC{
                    objVC.hidesBottomBarWhenPushed = true
                    
                    self.navigationController?.pushViewController(objVC, animated: true)
                }
        }
            }
            
            
        }else{
            
            let businessHour = UserDefaults.standard.string(forKey: UserDefaults.keys.businessHourCount) ?? "0"

            if businessName != "" && businessHour != "0" {
                objAppShareData.editSelfServices  = true
                let sb = UIStoryboard(name:"SignUpProcess",bundle:Bundle.main)
                let objChooseType = sb.instantiateViewController(withIdentifier:"BusinessHoursVC") as! BusinessHoursVC
                objChooseType.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(objChooseType, animated:true)
         
            }else if  businessName == ""{
                
                objAppShareData.showAlert(withMessage: "Please update business info before adding operation hours", type: alertType.bannerDark, on: self)
                
            }else if  businessHour == "0"{
            
                objAppShareData.editSelfServices  = true
                let sb = UIStoryboard(name:"SignUpProcess",bundle:Bundle.main)
                let objChooseType = sb.instantiateViewController(withIdentifier:"BusinessHoursVC") as! BusinessHoursVC
                objChooseType.hidesBottomBarWhenPushed = true
               self.navigationController?.pushViewController(objChooseType, animated:true)
            }
        }
    }
    
    
    
    
    
    
    
    
    
    @IBAction func btnCertificate(_ sender: UIButton) {
        let sb = UIStoryboard(name:"Certificate",bundle:Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"CertificateLIstVC") as? CertificateLIstVC{
            objVC.hidesBottomBarWhenPushed = true
            
            self.navigationController?.pushViewController(objVC, animated: true)
        }
        
    }
    
    @IBAction func btnVoucherCode(_ sender: UIButton) {
        let sb = UIStoryboard(name:"Voucher",bundle:Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"VoucherListVC") as? VoucherListVC{
            objVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(objVC, animated: true)
         }
        
    }
    
    @IBAction func btnMyStaff(_ sender: UIButton) {
        let sb = UIStoryboard(name:"StaffList",bundle:Bundle.main)
         if let objVC = sb.instantiateViewController(withIdentifier:"UserListAddStaffVC") as? UserListAddStaffVC{
            objVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(objVC, animated: true)
        }

    }
    @IBAction func btnAddStaff(_ sender: UIButton) {
        let sb = UIStoryboard(name:"StaffList",bundle:Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"userSearchVC") as? userSearchVC{
            objVC.hidesBottomBarWhenPushed = true

            self.navigationController?.pushViewController(objVC, animated: true)
        }

    }
    @IBAction func btnHolidaPlanner(_ sender: UIButton) {
        let sb = UIStoryboard(name:"Holiday",bundle:Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"HolidayListVC") as? HolidayListVC{
            objVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(objVC, animated: true)
        }
        
    }
}


//MARK:- Webservice Call
extension BusinessAdminLiastVC {
    
    func callWebserviceForCompanyList(dict: [String : Any]){
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        
        objActivity.startActivityIndicator()
        objServiceManager.requestPost(strURL: WebURL.companyInfo, params: dict  , success: { response in
            
            objServiceManager.StopIndicator()
            
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else{
                
                let strStatus =  response["status"] as? String ?? ""
                if strStatus == k_success{
                    self.parseResponce(response:response)
                }else{
                    self.viewWalkingListIconRight.isHidden = false
                    self.viewWalkingListIconRight.isHidden = true
                    self.setOldData()
                    let msg = response["message"] as! String
                    objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                }
            }
            
        }){ error in
            objServiceManager.StopIndicator()
            self.setOldData()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
    func callWebserviceForMyInfo(){
        callWebserviceForMyInfo2()
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        objServiceManager.requestGet(strURL: WebURL.getBusinessProfile, params: nil, success: { response in
            let keyExists = response["responseCode"] != nil
            if  keyExists { sessionExpireAlertVC(controller: self)
            }else{
                let strStatus =  response["status"] as? String ?? ""
                print(response)
                if strStatus == k_success{
                    if let dictUserDetail = response["artistRecord"] as? [[String:Any]]{
                        let dictOfInfo =  dictUserDetail[0]
                        UserDefaults.standard.setValue("0", forKey: UserDefaults.keys.businessHourCount)
                        var workingHours = ""
                        if let arrWorkingHours = dictOfInfo["businessHour"] as? [[String:Any]]{
                            print("arrWorkingHours count = ",arrWorkingHours.count)
                            UserDefaults.standard.setValue(String(arrWorkingHours.count), forKey: UserDefaults.keys.businessHourCount)
                        }
                    }
                }else{
                } }}){ error in
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
    func callWebserviceForMyInfo2(){
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        let dict = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] ?? ["":""]
        let id = dict[UserDefaults.keys.userId] as? String ?? ""
        let param = ["userId":id,"loginUserId":id]
        
        objActivity.startActivityIndicator()
        objServiceManager.requestPost(strURL: WebURL.getProfile, params: param, success:{ response in
            if response["status"] as? String == "success"{
                if let dictUserDetail = response["userDetail"] as? [[String:Any]]{
                    let dictUser = dictUserDetail[0]
                    
                    ////
                    let isAdminComission = dictUser["isAdminComission"] as? Int ?? 0
                    self.isAdminComission = isAdminComission
                    if let isAdminComission = dictUser["isAdminComission"] as? String{
                        self.isAdminComission = Int(isAdminComission) ?? 0
                    }
                    if let isAdminComission = dictUser["isAdminComission"] as? Double{
                        self.isAdminComission = Int(isAdminComission) ?? 0
                    }
                    if let isAdminComission = dictUser["isAdminComission"] as? Float{
                        self.isAdminComission = Int(isAdminComission) ?? 0
                    }
                    if self.isAdminComission != 0 {
                       self.viewWalkingReminder.isHidden = false
                    }else{
                       self.viewWalkingReminder.isHidden = true
                    }
                    ////
                    
                    if  let IsInvitation = dictUser["isInvitation"] as? Int{
                        self.isInvitation = IsInvitation
                        if self.isInvitation != 0{
                            if let dict = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfoDic) as? [String:Any]{
                                let businessType = dict[UserDefaults.myDetail.businessType] as? String ?? ""
                                if businessType != "business"{
                                        self.stack4.isHidden = false; self.viewWalkingListIcon.isHidden = false
                                    self.viewWalkingListIcon.isHidden = true
                                }}
                        }else{
                            self.ref.child("socialBookingBadgeCount").child(id).updateChildValues(["businessInvitationCount":0])
                            self.stack4.isHidden = true; self.viewWalkingListIcon.isHidden = true
                        }
                    }
                  if  let isCertificate = dictUser["isCertificateVerify"] as? Int{
                    if isCertificate == 0 {
                        self.imgCertificate.isHidden = true
                    }else{
                        self.imgCertificate.isHidden = false
                    }
                    }
                    if  let serviceCount = dictUser["serviceCount"] as? Int{
                             self.strServiceCount = String(serviceCount)
                    }else if  let serviceCount = dictUser["serviceCount"] as? String{
                        self.strServiceCount = serviceCount
                    }
                    self.manageStaffButton()
                }
            }
        }) { error in
            print(error)
        }
    }
    func callWebserviceForUpdateMyInfo(){
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        let dict = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] ?? ["":""]
        let bio = dict[UserDefaults.keys.bio] as? String ?? ""
        let param = ["bio":bio]
        objServiceManager.requestPost(strURL: WebURL.updateRecord, params: param, success:{ response in
            if response["status"] as? String == "success"{
                if let dictUserDetail = response["user"] as? [String:Any]{
                    print(dictUserDetail)
                    objAppShareData.objAppdelegate.parseBusinessSignUpDetail(dictOfInfo: dictUserDetail)
                    if let dict = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]{
                        self.lblRattingCount.text = "("+(dict[UserDefaults.myDetail.reviewCount] as? String  ?? "")+")"
                        let a = dict[UserDefaults.myDetail.ratingCount] as? String ?? "0"
                        guard let n = NumberFormatter().number(from: a) else { return }
                        self.viewRating.value = CGFloat(truncating: n)
                    }
                }
            }
        }) { error in
        }
    }
    
    func parseResponce(response:[String : Any]){
        self.arrCompanyList.removeAll()
        
        let dict = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] ?? ["":""]
        var userName = dict[UserDefaults.keys.userName] as? String ?? ""
        //var name = ""
        var img = ""
        //name = decoded["businessName"] as? String ?? ""
        img = dict[UserDefaults.myDetail.profileImage] as? String ?? ""
        if userName == ""{
            userName = "Business Admin"
        }
        let objMydata = CompanyInfo(dict: ["businessName":userName,"profileImage":img,"businessId":dict[UserDefaults.keys.userId] ?? ""])
        self.arrCompanyList.append(objMydata)
        
        if let arr = response["businessList"] as? [[String:Any]]{
            if arr.count > 0{
                for dictArtistData in arr {
                    let objArtistList = CompanyInfo.init(dict: dictArtistData)
                    var statusObj = "0"
                    if let status = dictArtistData["status"] as? String{
                        statusObj = status
                    }else if let status = dictArtistData["status"] as? Int{
                        statusObj = String(status)
                    }
                    if statusObj == "1"{
                        self.arrCompanyList.append(objArtistList)
                    }
                 }
            }
        }
        if self.arrCompanyList.count == 1{
                self.imgHeaderDropDown.isHidden = true
        }else{
            self.imgHeaderDropDown.isHidden = false
        }
        let id = UserDefaults.standard.string(forKey: UserDefaults.keys.newAdminId)
        for obj in self.arrCompanyList{
            if obj.businessId == id{
                self.lblBusinessName.text = obj.businessName
            }
        }
        self.setViewOnDefalstSetId()
        self.setOldData()
    }
    
    
    
    func setViewOnDefalstSetId(){
        if let dict = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfoDic) as? [String:Any]{
            let businessName = dict[UserDefaults.myDetail.businessName] as? String ?? ""
            let userName = dict[UserDefaults.myDetail.userName] as? String ?? ""
            if self.lblBusinessName.text == "Business Admin"  ||  self.lblBusinessName.text == businessName ||  self.lblBusinessName.text == userName{
            //  self.stackButtonStaffBusType.isHidden = false
            self.stack2.isHidden = false;
                if self.isAdminComission != 0{
                    viewWalkingReminder.isHidden = false
                }
            if self.isInvitation != 0{
            if let dict = UserDefaults.standard.value(forKey: UserDefaults.keys.userInfoDic) as? [String:Any]{
            let businessType = dict[UserDefaults.myDetail.businessType] as? String ?? ""
            if businessType != "business"{
            self.stack4.isHidden = false; self.viewWalkingListIcon.isHidden = false
                self.viewWalkingListIcon.isHidden = true
            }}
            }
            self.manageStaffButton()
            self.btnBusiness.setTitle("My Business", for: .normal)
            }else{
            self.btnBusiness.setTitle("Business", for: .normal)
            //  self.stackButtonStaffBusType.isHidden = true
            self.stack2.isHidden = true;self.viewWalkingListIcon.isHidden = true;self.viewWalkingListIconRight.isHidden = true; viewWalkingReminder.isHidden = true
            self.stack4.isHidden = true; self.viewWalkingListIcon.isHidden = true
            self.viewButtonStaff.isHidden = true
            }
            
            self.manageStaffButton()
        }
}
    
    
    func removePropertyData(){
        let fetchRequest: NSFetchRequest<BusinessType> = BusinessType.fetchRequest()
        do {   let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest as! NSFetchRequest<NSFetchRequestResult>)
            try managedContext?.execute(deleteRequest)  } catch { }
    }
    
    
  
        func addGesturesToView() -> Void {
            let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.topToBottomSWIPE))
            swipeRight.direction = .down
            self.viewBottumTable.addGestureRecognizer(swipeRight)
        }
    
    @objc func topToBottomSWIPE() ->Void {
        self.view.endEditing(true)
        self.viewBottumTable.isHidden = true
        if  viewHeightGloble < Int(self.tabBarController?.view.frame.height ?? CGFloat(viewHeightGloble)) || self.tabBarController?.tabBar.isHidden == true{
            self.tabBarController?.tabBar.isHidden = false
            let b = Int(self.view.frame.width)
            let a = CGRect(x:0,y:0,width:b,height:viewHeightGloble)
            self.tabBarController?.view.frame = a//CGRect(x:0, y:0, width:self.view.frame.size.width, height:viewHeightGloble)
        }
    }
}

extension BusinessAdminLiastVC{
    func badgeCount(){
        self.viewInvitationBadgeCount.isHidden = true

        var strMyId = ""
        if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.businessInfoDic){
            if let userId = dict["_id"] as? String {
                strMyId = userId
            }
            if strMyId != ""{
                ref.child("socialBookingBadgeCount").child( UserDefaults.standard.string(forKey: UserDefaults.keys.userId) ?? strMyId).observe(.value, with: { (snapshot) in
                    self.viewInvitationBadgeCount.isHidden = true

                    let dict = snapshot.value as? [String:Any]
                    
                    var bagCount = "0"
                    if let count = dict?["businessInvitationCount"] as? Int {
                        if count >= 100{
                            bagCount = "99+"
                        }else{
                            bagCount = String(count)
                        }
                    }else if let count = dict?["businessInvitationCount"] as? String {
                        bagCount = count
                    }
                    if bagCount == "0"{
                        self.lblInvitationBadgeCount.text =  "0"
                        self.viewInvitationBadgeCount.isHidden = true
                    }else{
                        self.stack4.isHidden = self.viewRatingStarOption.isHidden
                        self.viewWalkingListIcon.isHidden = self.stack4.isHidden
                        self.viewInvitationBadgeCount.isHidden = false
                        self.lblInvitationBadgeCount.text =  bagCount
                    }
                    
                    var WalkinhbagCount = "0"
                    if let count = dict?["commissionCount"] as? Int {
                        if count >= 100{
                            WalkinhbagCount = "99+"
                        }else{
                            WalkinhbagCount = String(count)
                        }
                    }else if let count = dict?["commissionCount"] as? String {
                        WalkinhbagCount = count
                    }
                    if WalkinhbagCount == "0"{
                        self.lblWalkingReminderBadgeCount.text =  "0"
                        self.viewWalkingReminderBadgeCount.isHidden = true
                    }else{
                        self.viewWalkingReminderBadgeCount.isHidden = false
                        self.lblWalkingReminderBadgeCount.text =  WalkinhbagCount
                    }
                    
                    
                    
                })

                var strMyId = ""
                if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.businessInfoDic){
                    if let userId = dict["_id"] as? String {
                        strMyId = userId
                    }
                    ref.child("businessBookingBadgeCount").child(strMyId).observe(.value, with: { [weak self] (snapshot) in
                        guard let strongSelf = self else {
                            return
                        }
                        let dict = snapshot.value as? [String:Any]
                        print(dict)
                        var totalCount = 0
                        if let count = dict?["socialCount"] as? Int {
                            totalCount = count+totalCount
                        }else if let count = dict?["socialCount"] as? String {
                            totalCount = (Int(count) ?? 0)+totalCount
                        }
                        if let count = dict?["bookingCount"] as? Int {
                            totalCount = count+totalCount
                        }else if let count = dict?["bookingCount"] as? String {
                            totalCount = (Int(count) ?? 0)+totalCount
                        }
                        if totalCount == 0{
                            self?.viewNotificationCount.isHidden = true
                            self?.lblNotificationCount.text =  "0"
                        }else{
                            self?.viewNotificationCount.isHidden = false
                            self?.lblNotificationCount.text =  String(totalCount)
                            if  totalCount >= 100{
                                self?.lblNotificationCount.text = "99+"
                            }
                        }
                    })
                }
            }
        }
    }
}

//
//  VoucherListVC.swift
//  MualabBusiness
//
//  Created by Mac on 31/12/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import ContactsUI

class VoucherListVC: UIViewController,UITableViewDelegate,UITableViewDataSource,CNContactPickerDelegate {

    
    @IBOutlet weak var viewDeletePopup: UIView!
    @IBOutlet weak var lblNodatafound: UIView!
    
    @IBOutlet weak var tblVouchewList: UITableView!
    var fromWalkingBooking = false

    
    var arrVoucher = [VoucherModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.DelegateCalling()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.callWebserviceForVoucherList()
        self.tblVouchewList.reloadData()
        self.viewDeletePopup.isHidden = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func click_Contact(_ sender: Any) {
        let cnPicker = CNContactPickerViewController()
        cnPicker.delegate = self
        cnPicker.modalPresentationStyle = .fullScreen
        self.present(cnPicker, animated: true, completion: nil)
    }
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contacts: [CNContact]) {
        contacts.forEach { contact in
            for number in contact.phoneNumbers {
                let phoneNumber = number.value
                print("number is = \(phoneNumber)")
            }
        }
    }
    func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
        print("Cancel Contact Picker")
    }
}

//MARK: - custome method
extension VoucherListVC{
    func DelegateCalling(){
        self.viewDeletePopup.isHidden = true
        self.tblVouchewList.delegate = self
        self.tblVouchewList.dataSource = self
  }
}

//MARK: - custome method
extension VoucherListVC{
@IBAction func btnAddVouchew(_ sender: UIButton) {
    let sb = UIStoryboard(name:"Voucher",bundle:Bundle.main)
    let objChooseType = sb.instantiateViewController(withIdentifier:"AddVoucherVC") as! AddVoucherVC
    objChooseType.voucherTypeEdit = false
    self.navigationController?.pushViewController(objChooseType, animated: true)
    
    }
@IBAction func btnBackAction(_ sender: UIButton) {
 self.navigationController?.popViewController(animated: true)
}
    
}
extension VoucherListVC{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrVoucher.count == 0{
            self.lblNodatafound.isHidden = false
        }else{
            self.lblNodatafound.isHidden = true
        }
        
        return arrVoucher.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "CellVoucherList", for: indexPath) as? CellVoucherList {
            
            let objStaff = arrVoucher[indexPath.row]
            
            cell.lblVouchewCode.text = objStaff.voucherCode.uppercased()
            cell.lblExpiryDate.text = objStaff.endDate
            if objStaff.discountType == "1"{
                cell.lblDiscount.text = "£"+objStaff.amount
            }else{
                cell.lblDiscount.text = objStaff.amount+"%"
            }
            let a = objStaff.endDate.components(separatedBy: "-")
            if a.count > 2{
                cell.lblExpiryDate.text = a[0]+"/"+a[1]+"/"+a[2]
            }

            
            cell.btnShear.tag = indexPath.row
            cell.btnShear.addTarget(self, action:#selector(ShearVoucher(sender:)) , for: .touchUpInside)
           
            
            return cell
        }else{
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let objVoucher = self.arrVoucher[indexPath.row]
        if  fromWalkingBooking == true{
        objAppShareData.strVoucherCode = objVoucher.voucherCode
        self.navigationController?.popViewController(animated: true)
        }else{
            let sb = UIStoryboard(name:"Voucher",bundle:Bundle.main)
            let objChooseType = sb.instantiateViewController(withIdentifier:"VoucherDetailVC") as! VoucherDetailVC
            objChooseType.voucherTypeEdit = true
            objChooseType.objData = objVoucher
            self.navigationController?.pushViewController(objChooseType, animated: true)

        }
    }
    
    @available(iOS 11.0, *)
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
    {
        let Delete = UIContextualAction(style: .normal, title:  "", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            let objSer = self.arrVoucher[indexPath.row]
            let businessId = objSer._id
            self.deleteCell(voucherId: businessId)
            success(true)
        })
        Delete.image = #imageLiteral(resourceName: "delete")
        Delete.backgroundColor =  #colorLiteral(red: 0.9137254902, green: 0.2117647059, blue: 0.2274509804, alpha: 1)
        
        
        let Edit = UIContextualAction(style: .normal, title:  "", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            let objSer = self.arrVoucher[indexPath.row]
            self.editVoucherCode(objData:objSer)
            success(true)
        })
        Edit.image = #imageLiteral(resourceName: "edit")
        Edit.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        //return UISwipeActionsConfiguration(actions: [Delete,Edit])
        return UISwipeActionsConfiguration(actions: [Delete])
    }
    
    
   
    
    func deleteCell(voucherId:String){
        self.view.endEditing(true)
        let dict = ["id":voucherId]

        DeleteView(dict1: dict)
    }
    
    func editVoucherCode(objData:VoucherModel){
        let sb = UIStoryboard(name:"Voucher",bundle:Bundle.main)
        let objChooseType = sb.instantiateViewController(withIdentifier:"AddVoucherVC") as! AddVoucherVC
        objChooseType.voucherTypeEdit = true
        objChooseType.objData = objData
        self.navigationController?.pushViewController(objChooseType, animated: true)
    }
    
    @objc func ShearVoucher(sender: UIButton!){
         let objData = self.arrVoucher[sender.tag]
        let sb = UIStoryboard(name:"Voucher",bundle:Bundle.main)
        let objChooseType = sb.instantiateViewController(withIdentifier:"ShareVoucherCodeVC") as! ShareVoucherCodeVC
        objChooseType.objData = objData
        self.navigationController?.pushViewController(objChooseType, animated: true)
    }
    
    
    
    
    func DeleteView(dict1: [String : Any]){
        let alertController = UIAlertController(title: "Alert", message: "Are you sure want to delete voucher?", preferredStyle: .alert)
   
        
        let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.callWebserviceForDeleteVoucher(dict: dict1)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default) {
            UIAlertAction in
        }
        cancelAction.setValue(#colorLiteral(red: 1, green: 0.1490196078, blue: 0, alpha: 1), forKey: "titleTextColor")
        okAction.setValue(#colorLiteral(red: 0, green: 0.851996243, blue: 0.8281483054, alpha: 1), forKey: "titleTextColor")
        
        alertController.addAction(cancelAction)
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
   
}

extension VoucherListVC{

func callWebserviceForVoucherList(){
    if !objServiceManager.isNetworkAvailable(){
        objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
        return
    }
    objActivity.startActivityIndicator()
    objServiceManager.requestGet(strURL: WebURL.allvoucherList, params: nil  , success: { response in
        self.arrVoucher.removeAll()
        objServiceManager.StopIndicator()
        let keyExists = response["responseCode"] != nil
        if  keyExists {
            sessionExpireAlertVC(controller: self)
        }else{
            let strStatus =  response["status"] as? String ?? ""
            if strStatus == k_success{
                self.parseResponce(response:response)
            }else{
                self.tblVouchewList.reloadData()
                // let msg = response["message"] as! String
                //objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
            }
        }
    }){ error in
        self.tblVouchewList.reloadData()
        objServiceManager.StopIndicator()
        objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
    }
}
    
    
    func parseResponce(response:[String : Any]){
        if let arr = response["data"] as? [[String:Any]]{
            if arr.count > 0{
                for dictArtistData in arr {
                    let objArtistList = VoucherModel.init(dict: dictArtistData)
                    self.arrVoucher.append(objArtistList!)
                }
                self.tblVouchewList.reloadData()
            }
            self.tblVouchewList.reloadData()
        }
    }
    
    
    
    func callWebserviceForDeleteVoucher(dict: [String : Any]){
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        
        objActivity.startActivityIndicator()
        objServiceManager.requestPost(strURL: WebURL.deleteVoucher, params: dict  , success: { response in
            objServiceManager.StopIndicator()
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else{
                let strStatus =  response["status"] as? String ?? ""
                if strStatus == k_success{
                    self.callWebserviceForVoucherList()
                }else{
                    let msg = response["message"] as! String
                    objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                }
            }
        }){ error in
            self.tblVouchewList.reloadData()
            objServiceManager.StopIndicator()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
}

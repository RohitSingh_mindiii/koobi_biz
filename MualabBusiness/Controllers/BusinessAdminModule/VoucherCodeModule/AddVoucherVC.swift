//
//  AddVoucherVC.swift
//  MualabBusiness
//
//  Created by Mac on 31/12/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

class AddVoucherVC: UIViewController,UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var txtVoucherCode: UITextField!
    @IBOutlet weak var lblDiscountType: UILabel!
    @IBOutlet weak var txtDiscountAmmount: UITextField!
    
    @IBOutlet weak var lblStartDate: UILabel!
    @IBOutlet weak var lblEndDate: UILabel!
    @IBOutlet weak var lblPicketHeader: UILabel!
    @IBOutlet weak var btnContinue: UIButton!

    
    @IBOutlet weak var viewDateStartEnd: UIView!
    @IBOutlet weak var imgDropDown: UIImageView!

    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var viewBottumTable: UIView!
    
    @IBOutlet weak var viewPicker:UIView!
    @IBOutlet weak var DOBPicker : UIDatePicker!
    
    var startDate = true
    //let arrDiscountType = ["Percentage (%)","Amount (£)"]
    let arrDiscountType = ["Percentage (%)"]

    var strStartDate = ""
    var strEndDate = ""

    var voucherTypeEdit = false
    var objData = VoucherModel(dict: ["":""])
    override func viewDidLoad() {
         super.viewDidLoad()
        self.viewBottumTable.isHidden = true
        self.tblView.delegate = self
        self.tblView.dataSource = self
         self.viewPicker.isHidden = true
         self.strStartDate = ""
         self.strEndDate = ""
         self.txtDiscountAmmount.delegate = self
         self.txtVoucherCode.delegate = self
         self.addAccesorryToKeyBoard()
        
        self.viewDateStartEnd.isHidden = true
        if self.viewDateStartEnd.isHidden == true{
            self.imgDropDown.image = #imageLiteral(resourceName: "DropGray")
        }else{
            self.imgDropDown.image = #imageLiteral(resourceName: "UpGray")
        }
        // Do any additional setup after loading the view.
        self.addGesturesToView()
    }
    func addGesturesToView() -> Void {
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.topToBottomSWIPE))
        swipeRight.direction = .down
        self.viewBottumTable.addGestureRecognizer(swipeRight)
    }
    
    @objc func topToBottomSWIPE() ->Void {
        self.view.endEditing(true)
        self.viewBottumTable.isHidden = true
    }

    override func viewWillAppear(_ animated: Bool) {
         strStartDate = ""
         strEndDate = ""
        self.DOBPicker.minimumDate = Date()
        self.viewBottumTable.isHidden = true
        if self.lblDiscountType.text == "Percentage (%)" || self.lblDiscountType.text == "Amount (£)"{
            self.txtDiscountAmmount.isHidden = false
        }else{
            self.txtDiscountAmmount.isHidden = true
        }
        
        if self.voucherTypeEdit == true{
            self.txtVoucherCode.isUserInteractionEnabled = false
            self.txtDiscountAmmount.text = objData?.amount ?? ""
            self.lblStartDate.text = objData?.startDate ?? ""
            self.lblEndDate.text = objData?.endDate ?? ""
            self.txtVoucherCode.text = objData?.voucherCode ?? ""
 
            if objData?.discountType == "1" {
                self.lblDiscountType.text = "Amount (£)"
            }else{
                self.lblDiscountType.text = "Percentage (%)"
            }
            self.txtDiscountAmmount.isHidden = false
            
            let date1 = objAppShareData.setDateFromString1(time:self.lblStartDate.text!)
            self.strStartDate = objAppShareData.dateFormatToShow1(forAPI: date1)

            let date2 = objAppShareData.setDateFromString1(time:self.lblEndDate.text!)
            self.strEndDate = objAppShareData.dateFormatToShow1(forAPI: date2)

            self.btnContinue.setTitle("Edit & Continue", for: .normal)
            
        }else{
            self.btnContinue.setTitle("Save & Continue", for: .normal)

            self.txtVoucherCode.isUserInteractionEnabled = true

        }
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 

}

//MARK: - button extension
extension AddVoucherVC{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        if textField == self.txtVoucherCode{
            txtVoucherCode.resignFirstResponder()
        }else if textField == self.txtDiscountAmmount{
            txtDiscountAmmount.resignFirstResponder()
        }
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == self.txtVoucherCode{
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(100)) {
            self.caps()
            }}
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.txtVoucherCode{
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(100)) {
                self.caps()
            }}
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        
        let text = textField.text!.uppercased() as NSString
        self.txtVoucherCode.autocapitalizationType = .allCharacters
        if textField == txtVoucherCode{
        let characterset = NSCharacterSet(charactersIn: "@#abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
        if string.rangeOfCharacter(from: characterset.inverted) != nil {
            print("string contains special characters")
            return false
            }}
        
        if (text.length == 1)  && (string == "") {
            if textField == self.txtVoucherCode{
                DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(100)) {
                    self.caps()
                }}
        }else{
            if textField == self.txtVoucherCode{
                DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(100)) {
                    self.caps()
                }}
            var substring: String = textField.text!
            substring = (substring as NSString).replacingCharacters(in: range, with: string)
            substring = substring.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            let a = substring.count
            
            if textField == self.txtDiscountAmmount{
                var str = substring
                str = str.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)

            if self.lblDiscountType.text == "Percentage (%)" && str != "" && str != "." {
                let value = float_t(str) ?? 0.0
                if value > 100{
                    objAppShareData.showAlert(withMessage:  "Percentage value must be less then 100", type: alertType.banner, on: self)
                    return false

                }
                }
            }
            
              if textField == self.txtVoucherCode {
                
                if a > 7 && substring != ""{
                    return false
                }else{
                       return true
                }
              }else if textField == self.txtDiscountAmmount{
                
                if a > 7 && substring != ""{
                    return false
                }else{
                    let isNumber = CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: string))
                    let withDecimal = (
                        string == NumberFormatter().decimalSeparator &&
                            textField.text?.contains(string) == false
                    )
                    let maxLength = 7
                    let currentString: NSString = textField.text! as NSString
                    let newString: NSString =
                        currentString.replacingCharacters(in: range, with: string) as NSString
                    return newString.length <= maxLength && isNumber || withDecimal
                }
            }else{
                return true
            }
         }
        return true
    }
    
    
    func caps(){
        self.txtVoucherCode.text = txtVoucherCode.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)

        var a = self.txtVoucherCode.text
        let b = a?.uppercased()
        self.txtVoucherCode.text = b
        
        

    }
}
//MARK: - Custome method extension
fileprivate extension AddVoucherVC {
    func addAccesorryToKeyBoard(){
        let keyboardDoneButtonView = UIToolbar()
        keyboardDoneButtonView.sizeToFit()
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style:.plain, target: self, action: #selector(self.resignKeyBoard))
        doneButton.tintColor = appColor
        
        keyboardDoneButtonView.items = [flexBarButton, doneButton]
        
        txtDiscountAmmount.inputAccessoryView = keyboardDoneButtonView
    }
    @objc func resignKeyBoard(){
        txtDiscountAmmount.endEditing(true)
        self.view.endEditing(true)
    }
    
}
//MARK: - popup and dropdown button
fileprivate extension AddVoucherVC{
    
    @IBAction func btnSelectDiscountType(_ sender: UIButton) {

        self.view.endEditing(true)

        self.viewBottumTable.isHidden = false
        self.tblView.reloadData()
    }
    
    @IBAction func btnDateDropDown(_ sender: UIButton) {
        self.viewDateStartEnd.isHidden = !self.viewDateStartEnd.isHidden
        if self.viewDateStartEnd.isHidden == true{
            self.imgDropDown.image = #imageLiteral(resourceName: "DropGray")
        }else{
            self.imgDropDown.image = #imageLiteral(resourceName: "UpGray")
        }
    }
    @IBAction func btnValidityStartDate(_ sender: UIButton) {
        self.selectDateFromPicker(senderTag: sender.tag)
    }
    
    func selectDateFromPicker(senderTag:Int){
        self.view.endEditing(true)
        if senderTag == 0 {
            self.lblPicketHeader.text = "From Date"

                 self.viewPicker.isHidden = false
                  self.DOBPicker.minimumDate = Date()
            self.startDate = true
            if self.lblStartDate.text != "" && self.lblStartDate.text != "From Date"{
                let a = self.lblStartDate.text?.components(separatedBy: "/")
                if a?.count == 3 {
                    self.DOBPicker.date = objAppShareData.setDateFromString1(time:self.lblStartDate.text!)
                }
            }
        }else if senderTag == 1 {
            self.lblPicketHeader.text = "To Date"

           if self.lblStartDate.text == "" || self.lblStartDate.text == "From Date" {
            objAppShareData.showAlert(withMessage: "Please select start date", type: alertType.banner, on: self)
           }else{
            self.viewPicker.isHidden = false
            self.startDate = false
            self.DOBPicker.minimumDate = objAppShareData.setDateFromString1(time:self.lblStartDate.text!)
            if self.lblEndDate.text != "" && self.lblEndDate.text != "To Date"{
                let a = self.lblEndDate.text?.components(separatedBy: "/")
                if a?.count == 3 {
                    self.DOBPicker.date = objAppShareData.setDateFromString1(time:self.lblEndDate.text!)
                }
            }
        }
        }
    }
    
    
    
    
    @IBAction func btnDoneCancleDOBSelection(_ sender:UIButton){
        self.view.endEditing(true)
        if sender.tag == 1 {
            if startDate == true{
                self.strStartDate = objAppShareData.dateFormatToShow1(forAPI: self.DOBPicker.date)
                if self.lblStartDate.text == objAppShareData.dateFormatToShowDace(forAPI: self.DOBPicker.date){
                }else{ self.lblEndDate.text = "To Date" }
                self.lblStartDate.text = objAppShareData.dateFormatToShowDace(forAPI: self.DOBPicker.date)
             }else{
                self.strEndDate = objAppShareData.dateFormatToShow1(forAPI: self.DOBPicker.date)
                self.lblEndDate.text = objAppShareData.dateFormatToShowDace(forAPI: self.DOBPicker.date)
            }
            if self.lblEndDate.text == "To Date"{
                self.selectDateFromPicker(senderTag: 1)
            }else{
                self.viewPicker.isHidden = true
            }
        }else{
        self.viewPicker.isHidden = true
        }
    }
    
    @IBAction func btnContinew(_ sender:UIButton){
         txtVoucherCode.text = txtVoucherCode.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
         txtDiscountAmmount.text = txtDiscountAmmount.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let discount = Double(self.txtDiscountAmmount.text!) ?? 0.0
        if self.txtVoucherCode.text?.count == 0{
             objAppShareData.shakeViewField(self.txtVoucherCode)
        }else if self.lblDiscountType.text == "Discount Type"{
            objAppShareData.showAlert(withMessage: "Please select discount type", type: alertType.banner, on: self)
        }else if self.txtDiscountAmmount.text?.count == 0{
            objAppShareData.shakeViewField(self.txtDiscountAmmount)
        }else if discount > 25{
            objAppShareData.showAlert(withMessage: "Please enter a voucher discount that is no greater than 25%", type: alertType.banner, on: self)
        }else if self.lblStartDate.text == "From Date"{
            objAppShareData.showAlert(withMessage: "Please select voucher validity date", type: alertType.banner, on: self)
        }else if self.lblEndDate.text == "To Date"{
            objAppShareData.showAlert(withMessage:  "Please select end date", type: alertType.banner, on: self)

        }else{
            var discountType = ""
            
            if self.lblDiscountType.text == "Amount (£)"{
                discountType = "1"
            }else if self.lblDiscountType.text == "Percentage (%)"{
                discountType = "2"
//                let a = float_t(self.txtDiscountAmmount.text ?? "0.0") ?? 0.0
//                if a > 100{
//                    objAppShareData.showAlert(withMessage:  "Please select to date", type: alertType.banner, on: self)
//
//                }
                
            }
            var id = ""
            if self.voucherTypeEdit == true{
                id = objData?._id ?? ""
            }
            let dicts  = ["voucherCode":self.txtVoucherCode.text ?? "",
                        "discountType":discountType,
                        "amount":self.txtDiscountAmmount.text ?? "",
                        "startDate":strStartDate,
                        "endDate":strEndDate,
                        "id":id]
            self.callWebserviceAddVoucher(dict: dicts)
        }
        
        
        
    }
    @IBAction func btnBackAction(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    func callWebserviceAddVoucher(dict: [String : Any]){
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        objActivity.startActivityIndicator()
        objServiceManager.requestPost(strURL: WebURL.addVoucher, params: dict  , success: { response in
             objServiceManager.StopIndicator()
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else{
                let strStatus =  response["status"] as? String ?? ""
                if strStatus == k_success{
                    self.navigationController?.popViewController(animated: true)
                }else{
                      let msg = response["message"] as? String ?? ""
                    objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                }
            }
        }){ error in
             objServiceManager.StopIndicator()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
}

extension AddVoucherVC{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "CellBottumTableList"
        //let arrDiscountType = ["Percentage (%)","Amount (£)"]
        let arrDiscountType = ["Percentage (%)"]
        let obj = arrDiscountType[indexPath.row]
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? CellBottumTableList{
            cell.lblTitle.text = arrDiscountType[indexPath.row]
            return cell
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //let arrDiscountType = ["Percentage (%)","Amount (£)"]
        let arrDiscountType = ["Percentage (%)"]
        let obj = arrDiscountType[indexPath.row]
            self.lblDiscountType.text = obj
        if self.lblDiscountType.text == "Percentage (%)" || self.lblDiscountType.text == "Amount (£)"{
            self.txtDiscountAmmount.isHidden = false
        }else{
            self.txtDiscountAmmount.isHidden = true
        }
        self.viewBottumTable.isHidden = true
    }
    @IBAction func btnHiddenTable(_ sender:Any){
        self.viewBottumTable.isHidden = true
    }
}

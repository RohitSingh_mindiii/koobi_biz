//
//  VoucherDetailVC.swift
//  MualabBusiness
//
//  Created by Mindiii on 4/23/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit

class VoucherDetailVC: UIViewController {
    
    @IBOutlet weak var txtVoucherCode: UITextField!
    @IBOutlet weak var lblDiscountType: UILabel!
    @IBOutlet weak var txtDiscountAmmount: UITextField!
    @IBOutlet weak var lblStartDate: UILabel!
    @IBOutlet weak var lblEndDate: UILabel!
    @IBOutlet weak var viewDateStartEnd: UIView!

    var startDate = true
    let arrDiscountType = ["Percentage (%)","Amount (£)"]
 
    
    var voucherTypeEdit = false
    var objData = VoucherModel(dict: ["":""])
    override func viewDidLoad() {
        super.viewDidLoad()
        
         
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
            self.txtDiscountAmmount.text = objData?.amount ?? ""
            self.lblStartDate.text = "From - "+(objData?.startDate ?? "")
            self.lblEndDate.text = "To - "+(objData?.endDate ?? "")
            self.txtVoucherCode.text = objData?.voucherCode ?? ""
            
            if objData?.discountType == "1" {
                self.lblDiscountType.text = "Discount Type"//"Amount (£)"
                self.txtDiscountAmmount.text = "£"+(objData?.amount ?? "")

            }else{
                self.lblDiscountType.text = "Discount Type"//"Percentage (%)"
                self.txtDiscountAmmount.text = (objData?.amount ?? "")+"%"

            }
            self.txtDiscountAmmount.isHidden = false
           
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnBackAction(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
}




//  MyContactVC.swift
//  MualabBusiness
//  Created by Mindiii on 5/2/19.
//  Copyright © 2019 Mindiii. All rights reserved.

import UIKit
import ContactsUI
import MessageUI

class MyContactVC: UIViewController,UITableViewDelegate,UITableViewDataSource,CNContactPickerDelegate, MFMessageComposeViewControllerDelegate,UITextFieldDelegate  {
  
    @IBOutlet weak var tblContact: UITableView!
    @IBOutlet weak var txtSearch: UITextField!
    let contactStore = CNContactStore()
    var contacts = [CNContact]()
    var arrDictContact = [ModelAddContactList]()
    var arrFilteredContact = [ModelAddContactList]()
    var strSearchText = ""
    var shareText = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblContact.delegate = self
        self.tblContact.dataSource = self
        self.txtSearch.delegate = self
        self.contactData()
        // Do any additional setup after loading the view.
    }
    
    func contactData(){
        self.arrDictContact.removeAll()
        self.arrFilteredContact.removeAll()
        let keys = [ CNContactFormatter.descriptorForRequiredKeys(for: .fullName),  CNContactPhoneNumbersKey, CNContactEmailAddressesKey] as [Any]
        
        let request = CNContactFetchRequest(keysToFetch: keys as! [CNKeyDescriptor])
        do {
            try contactStore.enumerateContacts(with: request){
                (contact, stop) in
                // Array containing all unified contacts from everywhere
                self.contacts.append(contact)
                print(contact)
                
                for phoneNumber in contact.phoneNumbers {
                    
                    if let number = phoneNumber.value as? CNPhoneNumber, let label = phoneNumber.label {
                        let localizedLabel = CNLabeledValue<CNPhoneNumber>.localizedString(forLabel: label)
//                        print("givenName = \(contact.givenName) ")
//                        print("Name = \(contact.familyName) ")
//                        print("localizedLabel = \(localizedLabel) ")
//                        print("number= \(number.stringValue)")
//                        print("email: \(contact.emailAddresses)")
                        let name = "\(contact.givenName) "+"\(contact.familyName) "
                        let contact = "\(number.stringValue) "
                        let type = "\(localizedLabel) "
                        let dict = ["name": name, "contactNo":contact, "type":type]
                        let obj = ModelAddContactList.init(dict: dict)
                        self.arrDictContact.append(obj)
                        self.arrFilteredContact.append(obj)
                    }
                }
            }
            //   print(self.contacts)
            
            self.tblContact.reloadData()

            
        } catch {
            print("unable to fetch contacts")
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return arrFilteredContact.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "CellMyContact", for: indexPath) as? CellMyContact {
        
            let dict = arrFilteredContact[indexPath.row]
    
            cell.lblContact.text = dict.contactNo 
            cell.lblName.text = dict.name 
                cell.imgProfile.image = #imageLiteral(resourceName: "cellBackground")
//                if let image : UIImage = UIImage(data: (objContact.imageData ?? Data()) ){
//                    cell.imgProfile.image =  image
//                }
            
            return cell
        }
        return UITableViewCell()
      }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj = self.arrFilteredContact[indexPath.row]
        let no = obj.contactNo as? String ?? "9630173139"
        if (MFMessageComposeViewController.canSendText()) {
            let controller = MFMessageComposeViewController()
            controller.body = self.shareText
            controller.recipients = [no]
            controller.messageComposeDelegate = self
            controller.modalPresentationStyle = .fullScreen
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController!, didFinishWith result: MessageComposeResult) {
        //... handle sms screen actions
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
 
}



// MARK: - UITextfield Delegate
extension MyContactVC{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        
        let text = textField.text! as NSString
        
        if (text.length == 1)  && (string == "") {
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
            self.strSearchText = ""
 
            getArtistDataWith(andSearchText: "")
        }else{
            var substring: String = textField.text!
            substring = (substring as NSString).replacingCharacters(in: range, with: string)
            substring = substring.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            let a = substring.count
            if a > 20 && substring != ""{
                return false
            }else{
                 searchAutocompleteEntries(withSubstring: substring)
            }
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.view.endEditing(true)
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
        self.strSearchText = ""
        getArtistDataWith( andSearchText: "")
        return true
    }
    
    // MARK: - searching operation
    func searchAutocompleteEntries(withSubstring substring: String) {
        if substring != "" {
            self.strSearchText = substring
            // to limit network activity, reload half a second after last key press.
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
            self.perform(#selector(self.reload), with: nil, afterDelay: 0.5)
        }
    }
    
    @objc func reload() {
        self.getArtistDataWith( andSearchText: strSearchText)
    }
    
    func getArtistDataWith(andSearchText: String) {

        let filtered = arrDictContact.filter { $0.name.localizedCaseInsensitiveContains(andSearchText ?? "") }
        if andSearchText == ""{
            self.arrFilteredContact = arrDictContact
        }else{
        self.arrFilteredContact = filtered
        }
        self.tblContact.reloadData()
        if self.arrFilteredContact.count > 0{
            self.tblContact.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
        }
    }
}

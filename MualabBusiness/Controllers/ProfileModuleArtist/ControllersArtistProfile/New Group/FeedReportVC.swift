//
//  FeedReportVC.swift
//  MualabCustomer
//
//  Created by Mac on 23/10/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import Firebase
import DropDown

class FeedReportVC: UIViewController,UITextFieldDelegate,UITextViewDelegate,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var txtSelectReason: UITextField!
    @IBOutlet weak var txtViewDescription: UITextView!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var viewBottumTable: UIView!
    @IBOutlet weak var HeightViewBottumTable: NSLayoutConstraint!
    var strLink = ""
    var feedId = ""
    let dropDown = DropDown()
    @IBOutlet weak var lblLink: UILabel!
    var objModel = feeds(dict: [:])
    fileprivate var strMyChatId = ""
    fileprivate var arrReasons = [String]()

    fileprivate var ref: DatabaseReference!
    fileprivate var messages: [DataSnapshot]! = []
    fileprivate var _refHandle: DatabaseHandle?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewBottumTable.isHidden = true
        self.callWebserviceFor_getReportReasons()
         let dict = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]  ?? ["":""]
        self.tblView.delegate = self
        self.tblView.dataSource = self
        self.strMyChatId =  dict[UserDefaults.keys.userId] as? String ?? ""
        self.txtSelectReason.delegate = self
        self.txtViewDescription.delegate = self
        ref = Database.database().reference()
        setuoDropDownAppearance()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.lblLink.text = self.strLink
        let a = self.objModel?._id ?? 0
        self.lblLink.text = WebURL.BaseUrl+WebURL.feedDetails + "/" + String(a)
        let dict = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]  ?? ["":""]
        self.strMyChatId =  dict[UserDefaults.keys.userId] as? String ?? ""
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    internal func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        var edit = true
        var newLength = 0
        var searchString: String? = nil
        if textView == self.txtViewDescription {
            if (text.count) != 0
            { searchString = self.txtViewDescription.text! + (text).uppercased()
                newLength = (self.txtViewDescription.text?.count)! + text.count - range.length
            }
            else {
                //searchString = (self.txtViewDescription.text as NSString?)?.substring(to: (self.txtViewDescription.text?.count)! - 1).uppercased()
            }
            if newLength <= 500{  edit = true
            }else{   edit = false  }
            return edit
        }
        return edit
    }
    
    
    internal func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var edit = true
        var newLength = 0
        var searchString: String? = nil
        if textField == self.txtSelectReason {
            if (string.count ) != 0
            { searchString = self.txtSelectReason.text! + (string).uppercased()
                newLength = (self.txtSelectReason.text?.count)! + string.count - range.length
            }
            else { searchString = (self.txtSelectReason.text as NSString?)?.substring(to: (self.txtSelectReason.text?.count)! - 1).uppercased()
            }
            if newLength <= 50{  edit = true
            }else{   edit = false  }
            return edit
        }
        return edit
    }
    
    internal func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.txtSelectReason{
            txtViewDescription.becomeFirstResponder()
        }
        return true
    }
    
    
    @IBAction func btnSubmitReportAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.txtSelectReason.text = self.txtSelectReason.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        self.txtViewDescription.text = self.txtViewDescription.text.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if self.txtSelectReason.text?.count == 0{
            objAppShareData.showAlert(withMessage: "Please enter report reason", type: alertType.bannerDark,on: self)
        }else if self.txtViewDescription.text?.count == 0{
            objAppShareData.showAlert(withMessage: "Please enter report description", type: alertType.bannerDark,on: self)
        }else{
            self.reportAction()
        }
    }
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    func setuoDropDownAppearance()  {
        let appearance = DropDown.appearance()
        appearance.cellHeight = 45
        appearance.backgroundColor = UIColor(white: 1, alpha: 1)
        appearance.selectionBackgroundColor = UIColor(red: 0.6494, green: 0.8155, blue: 1.0, alpha: 0.2)
        appearance.separatorColor = UIColor(white: 0.7, alpha: 0.8)
        appearance.cornerRadius = 2
        appearance.shadowColor = UIColor(white: 0.6, alpha: 1)
        appearance.shadowOpacity = 0.9
        appearance.shadowRadius = 25
        appearance.animationduration = 0.25
        appearance.textColor = .darkGray
    }
    
    @IBAction func btnReasonAction(_ sender: UIButton) {
        
        self.view.endEditing(true)
        let objGetData = self.arrReasons.map { $0 }

        if self.arrReasons.count == 0{
           objAppShareData.showAlert(withMessage: "No reason found", type: alertType.bannerDark,on: self)
            return
        }
        
        self.viewBottumTable.isHidden = false
        self.HeightViewBottumTable.constant = CGFloat(self.arrReasons.count*45)
        self.tblView.reloadData()
    }
    
    func reportAction(){
        
            let time = ServerValue.timestamp()
        

        let dict = ["feedId":self.objModel?._id ?? "",
                    "feedOwnerId":self.objModel?.userId ?? "",
                    "link":self.lblLink.text,
                    "myId":self.strMyChatId ,//
                    "reason":self.txtSelectReason.text ?? "",//
                    "description":self.txtViewDescription.text ?? "",//
                    "timeStamp":time] as [String : Any]
            print(dict)
        
            let id = String(self.objModel?._id ?? 0)
        self.ref.child("feed_report").childByAutoId().setValue(dict)
        objAppShareData.showAlert(withMessage: "Report sent successfully", type: alertType.bannerDark,on: self)
            self.navigationController?.popViewController(animated: true)
        
    }
    
    // MARK: - Webservices
    func callWebserviceFor_getReportReasons(){
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        //objWebserviceManager.StartIndicator()
        var parameters = [String:Any]()
        objWebserviceManager.requestGetForJson(strURL: WebURL.reportReason, params: parameters , success: { response in
            self.arrReasons.removeAll()
            let strSucessStatus = response["status"] as? String
            let strMessage = response["message"] as? String
            if strSucessStatus == k_success{
                let arrReason = response["data"] as? [[String:Any]]
                for dict in arrReason!{
                    let strReason = dict["title"] as? String
                    self.arrReasons.append(strReason!)
                }
            }else{
                objAppShareData.showAlert(withMessage: strMessage!, type: alertType.bannerDark,on: self)
            }
        }) { (error) in
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
}


//MARK: - table view method extension
extension FeedReportVC{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrReasons.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "CellBottumTableList"
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? CellBottumTableList{
            let obj = self.arrReasons[indexPath.row]
            cell.lblTitle.text = obj
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.viewBottumTable.isHidden = true
        let item = arrReasons[indexPath.row]
        self.txtSelectReason.text = item
    }
    
    @IBAction func btnHiddenBottumTable(_ sender: UIButton) {
        self.viewBottumTable.isHidden = true
    }
}

//
//  AboutArtistVC.swift
//  MualabBusiness
//
//  Created by Mindiii on 3/14/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit

class AboutArtistVC: UIViewController,UITextViewDelegate {

    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var txtAboutUs: UITextView!
    @IBOutlet weak var viewMyOption: UIView!
    var strAboutUs = ""
    var staffId = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.txtAboutUs.delegate = self
        self.addAccesorryToKeyBoard()
        let dictnnn : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] ?? ["":""]
        let myId = dictnnn[UserDefaults.keys.userId] as? String ?? ""
        if staffId == myId || staffId == "0" {
            self.viewMyOption.isHidden = false
            self.lblHeader.text = "About Us"
        }else{
            self.viewMyOption.isHidden = true
            self.lblHeader.text = "About Us"

        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.viewConfigure()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

//MARK:- Custome Methods extension
extension AboutArtistVC{
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        var numberOfChars:Int = 0
        let newText = (txtAboutUs.text as NSString).replacingCharacters(in: range, with: text)
        numberOfChars = newText.count
        if numberOfChars >= 500 && text != ""{
            return false
        }else{
        }
        return numberOfChars < 500
    }
    
    @IBAction func btnBack(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSubmit(_ sender:Any){
        txtAboutUs.text = txtAboutUs.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        if self.txtAboutUs.text.count == 0 {
            objAppShareData.showAlert(withMessage: "Please enter about us", type: alertType.banner, on: self)
        }else{
            self.callWebserviceForUpdateLocation(dicParam: ["bio":self.txtAboutUs.text ?? ""])
        }
    }
    
    
    func callWebserviceForUpdateLocation(dicParam:[String:Any]){
        objServiceManager.StartIndicator()
        objServiceManager.requestPost(strURL: WebURL.updateRecord, params: dicParam, success:{ response in
            objServiceManager.StopIndicator()
            if response["status"] as? String == "success"{
                if let msg = response["message"] as? String{
                   // objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                    self.backToProfile()
                }
            }else{
                if let msg = response["message"] as? String{
                    objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                }
            }
        }) { error in
            objServiceManager.StopIndicator()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
    func backToProfile(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func viewConfigure(){
        self.lblDescription.text = self.strAboutUs
        self.txtAboutUs.text = self.strAboutUs

        //self.changeString(userName: objCompanyList.businessName)
    }
    
    
    func changeString(userName:String){
        let colorBlack = UIColor.black
        
        let a = "We invite you to join"
        let b = userName
        let c = "as a staff member Accept the invitation and get started login in biz app with the same social credential"
        
        
        let StrName = NSMutableAttributedString(string: a + " ", attributes: [NSAttributedString.Key.font:UIFont(name: "Nunito-Regular", size: 17.0)!])
        
        StrName.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.lightGray, range: NSRange(location:0,length:StrName.length))
        
        let StrName1 = NSMutableAttributedString(string: b + " ", attributes: [NSAttributedString.Key.font:UIFont(name: "Nunito-SemiBold", size: 17.0)!])
        
        StrName1.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSRange(location:0,length:StrName1.length))
        
        
        let StrName2 = NSMutableAttributedString(string: c + " ", attributes: [NSAttributedString.Key.font:UIFont(name: "Nunito-Regular", size: 17.0)!])
        
        StrName2.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.lightGray, range: NSRange(location:0,length:StrName2.length))
        
        
        let combination = NSMutableAttributedString()
        
        combination.append(StrName)
        combination.append(StrName1)
        combination.append(StrName2)
        self.lblDescription.attributedText = combination
    }
    
    
    
    func addAccesorryToKeyBoard(){
        let keyboardDoneButtonView = UIToolbar()
        keyboardDoneButtonView.sizeToFit()
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style:.plain, target: self, action: #selector(self.resignKeyBoard))
        doneButton.tintColor = appColor
        keyboardDoneButtonView.items = [flexBarButton, doneButton]
        txtAboutUs.inputAccessoryView = keyboardDoneButtonView
    }
    @objc func resignKeyBoard(){
        txtAboutUs.endEditing(true)
        self.view.endEditing(true)
    }
}


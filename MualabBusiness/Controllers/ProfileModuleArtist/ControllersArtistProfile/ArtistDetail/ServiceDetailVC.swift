//
//  ServiceDetailVC.swift
//  MualabBusiness
//
//  Created by Mindiii on 3/18/19.
//  Copyright © 2019 Mindiii. All rights reserved.

import UIKit

class ServiceDetailVC: UIViewController{
    
    
    var param:[String:Any] = [:]
        @IBOutlet weak var lblDescription:UILabel!
        @IBOutlet weak var txtOnlyPrice:UITextField!
    
        @IBOutlet weak var viewBusins:UIView!
        @IBOutlet weak var viewCategory:UIView!
        @IBOutlet weak var viewOnlyPrice: UIView!
        @IBOutlet weak var viewBookingType:UIView!
        @IBOutlet weak var viewPrice:UIView!
        @IBOutlet weak var viewPriceOutCall:UIView!
        
        @IBOutlet weak var txtBusinessName:UITextField!
        @IBOutlet weak var txtCategoryName:UITextField!
        @IBOutlet weak var txtServiceName:UITextField!
        @IBOutlet weak var txtBookingType:UITextField!
        @IBOutlet weak var txtPrice:UITextField!
        @IBOutlet weak var txtOutCall:UITextField!
        
         @IBOutlet weak var txtTime:UITextField!
    
    
        override func viewDidLoad() {
            super.viewDidLoad()
        }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
        }
    
        
        override func viewWillAppear(_ animated: Bool) {
            self.parseDataFromLocalDataBase()
        }

        @IBAction func btnBack(_ sender:Any){
            self.navigationController?.popViewController(animated: true)
        }
   
    
        func parseDataFromLocalDataBase(){
            var businessType = ""
            let inPrice = param["incallPrice"] as? String ?? ""
            let ocPrice = param["outcallPrice"] as? String ?? ""
            
            if inPrice == "" || inPrice == "0.0"{
                businessType = "2"
            }else if ocPrice == "" || ocPrice == "0.0"{
                businessType = "1"
            }else{
                businessType = "3"
            }
            
            var strType = ""
            if businessType == "1"{
                strType = "Incall"
            }else if businessType == "2"{
                strType = "Outcall"
            }else{
                strType = "Both"
            }
            if strType == "Incall"{
            }else if strType == "Outcall"{
            }else{
                strType = "Both"
                self.viewBookingType.isHidden = false
            }
 
            
            self.txtBookingType.text = strType
            self.txtServiceName.text = param["title"] as? String ?? ""
            self.lblDescription.text = param["description"] as? String ?? ""
            
            self.txtTime.text = param["completionTime"] as? String ?? ""
            
            self.txtBusinessName.text = param["businessTypeName"] as? String ?? ""
            self.txtCategoryName.text = param["categoryName"] as? String ?? ""
            self.txtOutCall.text = ocPrice
            self.txtPrice.text = inPrice
            self.txtOnlyPrice.text = inPrice
 
            ////
            let doubleStrNN = String(format: "%.2f", ceil(Double(self.txtOutCall.text!)!))
            let doubleStr = String(format: "%.2f", ceil(Double(self.txtPrice.text!)!))
            self.txtOutCall.text = doubleStrNN
            self.txtPrice.text = doubleStr
            self.txtOnlyPrice.text = doubleStr
            ////
           
            self.viewBookingType.isHidden = false
            self.viewPrice.isHidden = false
            
                if strType == "Both"{
                    self.viewPriceOutCall.isHidden = false
                    self.viewPrice.isHidden = false
                    self.txtOnlyPrice.text = inPrice
                    ////
                    let doubleStrNN = String(format: "%.2f", ceil(Double(self.txtOnlyPrice.text!)!))
                    self.txtOnlyPrice.text = doubleStrNN
                    ////
                    self.viewOnlyPrice.isHidden = true
                }else if strType == "Incall"{
                    self.viewPriceOutCall.isHidden = true
                    self.viewPrice.isHidden = false
                    self.txtOnlyPrice.text = inPrice
                    ////
                    let doubleStrNN = String(format: "%.2f", ceil(Double(self.txtOnlyPrice.text!)!))
                    self.txtOnlyPrice.text = doubleStrNN
                    ////
                    self.viewOnlyPrice.isHidden = true

                }else if strType == "Outcall"{
                    self.viewPrice.isHidden = true
                    self.viewPriceOutCall.isHidden = false
                    self.txtOnlyPrice.text = ocPrice
                    ////
                    let doubleStrNN = String(format: "%.2f", ceil(Double(self.txtOnlyPrice.text!)!))
                    self.txtOnlyPrice.text = doubleStrNN
                    ////
                    self.viewOnlyPrice.isHidden = true
                }
        }
}

//
//  ArtistCertificateVC.swift
//  MualabBusiness
//
//  Created by Mindiii on 3/14/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit

class ArtistCertificateVC:  UIViewController,UITableViewDelegate,UITableViewDataSource {
        

        @IBOutlet weak var tblCertificate: UITableView!
        @IBOutlet weak var lblNoDataFound: UIView!
        var artistId = ""
        
        fileprivate var arrCertificate = [CertificateModel]()
        
        
        override func viewDidLoad() {
            super.viewDidLoad()
            self.preferredStatusBarStyle
            self.DelegateCalling()
        }
        
        override var preferredStatusBarStyle: UIStatusBarStyle {
            return .default
        }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
        
        override func viewWillAppear(_ animated: Bool) {
            
            let decoded1 = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] ?? ["":""]
            let userId = decoded1[UserDefaults.keys.userId] as? String ?? ""
                APICertificatelist()
        }
        
        
        func APICertificatelist(){
            let decoded1 = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] ?? ["":""]
            let userId = decoded1[UserDefaults.keys.userId] as? String ?? ""
            if self.artistId == "" || self.artistId == "0"{
                self.artistId = userId
            }
            callWebserviceForCertificateList(dict: ["artistId":self.artistId,"type":"artist"])
        }
    }
 
    
    //MARK: - custome method
    extension ArtistCertificateVC{
        func DelegateCalling(){
            self.tblCertificate.delegate = self
            self.tblCertificate.dataSource = self
        }
        
        func indexMethod(){
            if self.arrCertificate.count == 0 {
                self.lblNoDataFound.isHidden = false
            }else{
                self.lblNoDataFound.isHidden = true
            }
        }
    }
    //MARK: - tableview method
    extension ArtistCertificateVC{
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            if arrCertificate.count == 0{
                self.lblNoDataFound.isHidden = false
            }else{
                self.lblNoDataFound.isHidden = true
            }
            
            return arrCertificate.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
            if let cell = tableView.dequeueReusableCell(withIdentifier: "CellCertificateList", for: indexPath) as? CellCertificateList {
                
                let objStaff = arrCertificate[indexPath.row]
                
                cell.imgCertificate.image = #imageLiteral(resourceName: "gallery_placeholder")
                cell.lblCertiName.text = objStaff.title
                cell.lblCertiDes.text = objStaff.descriptions
                
                if objStaff.status == "0" {
                    cell.imgCertificateStatus.isHidden = true
                } else {
                    cell.imgCertificateStatus.isHidden = false
                }
                let url = URL(string: objStaff.certificateImage)
                if url != nil{
                    //cell.imgCertificate.af_setImage(withURL: url!)
                    cell.imgCertificate.sd_setImage(with: url!, placeholderImage:UIImage(named: "gallery_placeholder"))
                }else{
                    cell.imgCertificate.image =  #imageLiteral(resourceName: "gallery_placeholder")
                }

                return cell
            }else{
                return UITableViewCell()
            }
        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            
            // let isNavigate = true
            let objFeed = arrCertificate[indexPath.row]
            let sb = UIStoryboard(name: "Add", bundle: Bundle.main)
            let objShowImage = sb.instantiateViewController(withIdentifier:"showImagesVC") as! showImagesVC
            objShowImage.isTypeIsUrl = true
            // objShowImage.lblHeaderName.text = "Certificate"
            let obj = feedData(dict: ["feedPost":objFeed.certificateImage,"videoThumb":objFeed.certificateImage])
            var arr = [feedData]()
            arr.append(obj!)
            objShowImage.strHeaderName = "Qualification"
            objShowImage.arrFeedImages = arr
            objShowImage.modalPresentationStyle = .fullScreen
            present(objShowImage, animated: true)// { _ in }
        }
        
    }
    
    //MARK:- Webservice Call
    extension ArtistCertificateVC {
        
        func callWebserviceForCertificateList(dict: [String : Any]){
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            objActivity.startActivityIndicator()
            objServiceManager.requestPost(strURL: WebURL.getAllCertificate, params: dict  , success: { response in
                self.arrCertificate.removeAll()
                objServiceManager.StopIndicator()
                let keyExists = response["responseCode"] != nil
                if  keyExists {
                    sessionExpireAlertVC(controller: self)
                }else{
                    let strStatus =  response["status"] as? String ?? ""
                    if strStatus == k_success{
                        self.parseResponce(response:response)
                    }else{
                        self.tblCertificate.reloadData()
                        // let msg = response["message"] as! String
                        //objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                    }
                }
            }){ error in
                self.tblCertificate.reloadData()
                objServiceManager.StopIndicator()
                objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
            }
        }
        
        func parseResponce(response:[String : Any]){
            if let arr = response["allCertificate"] as? [[String:Any]]{
                if arr.count > 0{
                    for dictArtistData in arr {
                        let objArtistList = CertificateModel.init(dict: dictArtistData)
                        self.arrCertificate.append(objArtistList!)
                    }
                }
                self.tblCertificate.reloadData()
            }
        }
    }
    
    //MARK: - button extension
    extension ArtistCertificateVC{
        
        @IBAction func btnBackAction(_ sender: UIButton) {
            self.navigationController?.popViewController(animated: true)
        }
}

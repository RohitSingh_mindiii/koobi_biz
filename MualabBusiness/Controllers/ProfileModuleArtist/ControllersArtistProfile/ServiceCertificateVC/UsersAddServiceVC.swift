//
//  UsersAddServiceVC.swift
//  MualabBusiness
//
//  Created by mac on 09/04/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

fileprivate enum day:Int {
    case Monday
    case Tuesday
    case Wednesday
    case Thursday
    case Friday
    case Saturday
    case Sunday
    
    var string: String {
        return String(describing: self)
    }
}

class UsersAddServiceVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {
    fileprivate var arrOpeningTimes = [openingTimes]()

    var objArtistList = ArtistList(dict: [:])
    

    @IBOutlet weak var btnSave: UIButton!
    //table view outlate
    @IBOutlet weak var viewTableUserName: UIView!
    @IBOutlet weak var tblUserName: UITableView!
    
//header view outlate
    @IBOutlet weak var imgUserProfile: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
        
 //label view outlate
    @IBOutlet weak var lblJobTitle: UILabel!
    @IBOutlet weak var lblSocialMediaAccess: UILabel!
    @IBOutlet weak var lblServiceName: UILabel!
    @IBOutlet weak var lblHolidayAllocation: UITextField!
    
    private let arrJobTitle = ["Beginner","Moderate","Expert"]
    private let arrSocialMediaAccess = ["Admin","Editor","Moderator"]
    private var arrList = ["","",""]
    private var fromTableOne = false
    
    var arrStaffInfo = [AddStaffInfo]()

   override func viewDidLoad() {
        super.viewDidLoad()
        self.ViewConfigure()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.updateArtistInfo()
        self.dataParsing()
        self.timeSloats()
        
    }
 }

//MARK: - custome merhod extension
extension UsersAddServiceVC{
    
    func UnderDevelopment(){
        objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
    }
    
    func ViewConfigure(){
        
        self.tblUserName.delegate = self
        self.tblUserName.dataSource = self
        self.viewTableUserName.isHidden = true
        self.lblHolidayAllocation.delegate = self
        self.imgUserProfile.layer.masksToBounds = true
        self.imgUserProfile.layer.cornerRadius = self.imgUserProfile.layer.frame.width/2
        
        self.addAccesorryToKeyBoard()
    }
    
    func dataParsing(){
 
        if objAppShareData.fromStaffSelectCell == true && objAppShareData.objUsersAddServiceVC.arrStaffInfo.count > 0 {
                objAppShareData.fromStaffSelectCell = false
            self.lblSocialMediaAccess.text = objAppShareData.objUsersAddServiceVC.arrStaffInfo[0].mediaAccess
            self.lblJobTitle.text = objAppShareData.objUsersAddServiceVC.arrStaffInfo[0].job
            self.lblHolidayAllocation.text = objAppShareData.objUsersAddServiceVC.arrStaffInfo[0].holiday
            self.lblUserName.text = objModelStaffDetail.name
            if objModelStaffDetail.image != "" {
                if let url = URL(string: objModelStaffDetail.image){
                    //self.imgUserProfile.af_setImage(withURL: url, placeholderImage: UIImage(named: "cellBackground"))
                    self.imgUserProfile.sd_setImage(with: url, placeholderImage:UIImage(named: "cellBackground"))
                }
                
         }
        }}
    func updateArtistInfo(){
        
        if objArtistList.profileImage != "" {
            if let url = URL(string: objArtistList.profileImage){
                //self.imgUserProfile.af_setImage(withURL: url, placeholderImage: UIImage(named: "cellBackground"))
                self.imgUserProfile.sd_setImage(with: url, placeholderImage:UIImage(named: "cellBackground"))
            }
        }
        self.lblUserName.text = objArtistList.userName
        if self.lblUserName.text == "" { self.lblUserName.text = objModelStaffDetail.name }
    }
}

//MARK: - Textfield Delegate Methods
extension UsersAddServiceVC{
public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
      if textField == lblHolidayAllocation {
         lblHolidayAllocation.resignFirstResponder()
    }
    return true
}
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var newLength = 0
        var searchString: String? = nil
        if textField == self.lblHolidayAllocation {
            if (string.count ) != 0
            {
                searchString = self.lblHolidayAllocation.text! + (string).uppercased()
                newLength = (self.lblHolidayAllocation.text?.count)! + string.count - range.length
            }
            else {
                searchString = (self.lblHolidayAllocation.text as NSString?)?.substring(to: (self.lblHolidayAllocation.text?.count)! - 1).uppercased()
            }
            if (searchString?.count ?? 0) == 3 {
                self.lblHolidayAllocation.text = searchString
                searchString = nil
                return false
            }
            else {
            }
        }
            return newLength<=3
     
    }
}
//MARK: - tableView extension
extension UsersAddServiceVC{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrList.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cellIdentifier = "CellUserListAddStaff"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? CellUserListAddStaff
        cell?.lblUserName.text = arrList[indexPath.row]
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.view.endEditing(true)
        if  fromTableOne == false{
            self.lblSocialMediaAccess.text = arrList[indexPath.row]
        }else{
            self.lblJobTitle.text = arrList[indexPath.row]
        }
        objAppShareData.editStaff = false
        self.viewTableUserName.isHidden = true
    }
    
    
}

//MARK: - button extension
extension UsersAddServiceVC{
    @IBAction func btnTblHidden(_ sender: UIButton) {
        self.viewTableUserName.isHidden = true
    }
    
    @IBAction func btnJobTitle(_ sender: UIButton) {
        self.view.endEditing(true)
        self.viewTableUserName.isHidden = false
        fromTableOne = true
        self.arrList = self.arrJobTitle
        self.tblUserName.reloadData()
    }
    
    @IBAction func btnService(_ sender: UIButton) {
        self.view.endEditing(true)
//       
//        let sb = UIStoryboard(name:"BusinessAdmin",bundle:Bundle.main)
//        if let objVC = sb.instantiateViewController(withIdentifier:"CategoryAddStaffVC") as? CategoryAddStaffVC{
//            objVC.hidesBottomBarWhenPushed = true
//            self.navigationController?.pushViewController(objVC, animated: true)
//        }
    }
    
    @IBAction func btnSocialMediaAccess(_ sender: UIButton) {
        self.view.endEditing(true)
        fromTableOne = false
        self.viewTableUserName.isHidden = false
        self.arrList = self.arrSocialMediaAccess
        self.tblUserName.reloadData()
    }
    
 
    @IBAction func btnEditAction(_ sender: UIButton) {
        self.view.endEditing(true)
       
        let sb: UIStoryboard = UIStoryboard(name:"BusinessAdmin", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"StaffWorkingHoursVC") as? StaffWorkingHoursVC {
            objVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(objVC, animated: true)
        }
    }
    @IBAction func btnSaveAction(_ sender: UIButton) {
        self.view.endEditing(true)

            self.lblJobTitle.text = self.lblJobTitle.text?.trimmingCharacters(in: .whitespacesAndNewlines)
            self.lblSocialMediaAccess.text = self.lblSocialMediaAccess.text?.trimmingCharacters(in: .whitespacesAndNewlines)
            self.lblServiceName.text = self.lblServiceName.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        self.lblHolidayAllocation.text = self.lblHolidayAllocation.text?.trimmingCharacters(in: .whitespacesAndNewlines)

        
        
            if self.lblJobTitle.text == "" || self.lblJobTitle.text == "Enter Job Title"{
                objAppShareData.showAlert(withMessage: "Select job title", type: alertType.bannerDark, on: self)
            }else if objAppShareData.objModelHoldAddStaff.arrParseData.count == 0{
                objAppShareData.showAlert(withMessage: "Select at least one service for staff", type: alertType.bannerDark, on: self)
            }else if self.lblSocialMediaAccess.text == "" || self.lblSocialMediaAccess.text == "Social Media Access"{
                objAppShareData.showAlert(withMessage: "Select social media access", type: alertType.bannerDark, on: self)
            }else{
                 AddStaff()
        }
    
    }
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.view.endEditing(true)
        if objAppShareData.fromEditStaff == true{
            if objAppShareData.editStaff == true && self.lblHolidayAllocation.text == objAppShareData.objUsersAddServiceVC.arrStaffInfo[0].holiday {
                print("live = ",self.lblHolidayAllocation.text ?? "")
                objAppShareData.objModelHoldAddStaff.arrParseData.removeAll()
                objAppShareData.objBookSerSelectionVC.arrSelectedServicess.removeAll()
                objAppShareData.objAddStaffDoneSubSubServices.arrDoneTimeSloartJson = ""
                objAppShareData.objModelEditTimeSloat.arrFinalTimes.removeAll()
                self.navigationController?.popViewController(animated: true)
            }else{
                backAction()
            }
        }else{
        if objAppShareData.editStaff == true && self.lblHolidayAllocation.text == ""{
            objAppShareData.objModelHoldAddStaff.arrParseData.removeAll()
            objAppShareData.objBookSerSelectionVC.arrSelectedServicess.removeAll()
            objAppShareData.objAddStaffDoneSubSubServices.arrDoneTimeSloartJson = ""
            objAppShareData.objModelEditTimeSloat.arrFinalTimes.removeAll()
                self.navigationController?.popViewController(animated: true)
            }else{
               backAction()
            }
        }
    }
    }

//MARK:- phonepad return keyboard
extension UsersAddServiceVC{
    
    func addAccesorryToKeyBoard(){
       
        let keyboardDoneButtonView = UIToolbar()
        keyboardDoneButtonView.sizeToFit()
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style:.plain, target: self, action: #selector(self.resignKeyBoard))
        doneButton.tintColor = #colorLiteral(red: 0.05882352963, green: 0.180392161, blue: 0.2470588237, alpha: 1)
        
        keyboardDoneButtonView.items = [flexBarButton, doneButton]
        
        lblHolidayAllocation.inputAccessoryView = keyboardDoneButtonView
    }
    
    @objc func resignKeyBoard(){
        lblHolidayAllocation.endEditing(true)
        self.lblHolidayAllocation.text = self.lblHolidayAllocation.text?.trimmingCharacters(in: .whitespacesAndNewlines)
    }
}

//MARK: - back action extension
extension UsersAddServiceVC{
    func backAction(){
    let alertController = UIAlertController(title: "Alert", message: "Are you sure want to discard changes?", preferredStyle: .alert)
    let OKAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default){
    UIAlertAction in
        objAppShareData.objModelHoldAddStaff.arrParseData.removeAll()
        objAppShareData.objBookSerSelectionVC.arrSelectedServicess.removeAll()
        objAppShareData.objAddStaffDoneSubSubServices.arrDoneTimeSloartJson = ""
        objAppShareData.objModelEditTimeSloat.arrFinalTimes.removeAll()

        self.navigationController?.popViewController(animated: true)
    }
    let CancelAction = UIAlertAction(title: "No", style: UIAlertAction.Style.default){
    UIAlertAction in
    }
    alertController.addAction(OKAction)
    alertController.addAction(CancelAction)
    self.present(alertController, animated: true, completion: nil)
    }
}

//MARK: - AddStaff data parsing action extension
extension UsersAddServiceVC{
    func AddStaff(){
        
        var serviceJosn = [String: Any]()
        var arrData = [String]()
        
        objAppShareData.objModelHoldAddStaff.arrParseData = Array(Set(objAppShareData.objModelHoldAddStaff.arrParseData))
        for i in 0..<objAppShareData.objModelHoldAddStaff.arrParseData.count{
            let objMain = objAppShareData.objModelHoldAddStaff.arrParseData[i]
            
            //serviceJosn["_id"] = objMain.
            serviceJosn["artistServiceId"] = objMain.artistServiceId
            arrData.append(objMain.artistServiceId)
            
        }
        arrData = Array(Set(arrData))

        if let objectData = try? JSONSerialization.data(withJSONObject: arrData, options: JSONSerialization.WritingOptions(rawValue: 0)) {
            objAppShareData.objModelHoldAddStaff.strArrParseDataIds = String(data: objectData, encoding: .utf8)!
        }
        
        var staffType = ""
        var editId = ""
        var holiday = ""

        let businessId = objAppShareData.objModelHoldAddStaff.businessId
        let artistId = objAppShareData.objModelHoldAddStaff.artistId
        let strArrParseDataIds = objAppShareData.objModelHoldAddStaff.strArrParseDataIds
        
        
      
        

        if self.lblHolidayAllocation.text == "Holiday Allocation" || self.lblHolidayAllocation.text == ""{
            holiday = ""
        }else{
            holiday = self.lblHolidayAllocation.text!
        }
        if objAppShareData.fromEditStaff == true {
            staffType = "edit"
            editId = objAppShareData.objModelHoldAddStaff._id
        } else {
            staffType = ""
            editId = ""
        }
        
        let param = ["artistId":artistId,
                     "businessId":businessId,
                     "job":self.lblJobTitle.text!,
                     "mediaAccess":self.lblSocialMediaAccess.text!,
                     "staffService":strArrParseDataIds,
                     "staffHours":objAppShareData.addStaffTimeSloat,
                     "type":staffType,
                     "holiday":holiday,
        "editId":editId,"serviceType":"3"]
    
    print("param = ",param)
       callWebServicesForAddStaff(dict: param)
    }
}


//MARK: - AddStaff data parsing action extension
extension UsersAddServiceVC{
    func callWebServicesForAddStaff(dict:[String:Any]){

        objWebserviceManager.StartIndicator()

        objWebserviceManager.requestPostForJson(strURL: WebURL.addStaff, params: dict , success: { response in
            objWebserviceManager.StopIndicator()
            objActivity.stopActivity()
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
                
            }else {
            
            
            let strSucessStatus = response["status"] as? String
            if strSucessStatus == k_success{
                objAppShareData.objModelHoldAddStaff.arrParseData.removeAll()
                objAppShareData.objBookSerSelectionVC.arrSelectedServicess.removeAll()
                objAppShareData.objAddStaffDoneSubSubServices.arrDoneTimeSloartJson = ""
                objAppShareData.objModelEditTimeSloat.arrFinalTimes.removeAll()

                let controllers = self.navigationController?.viewControllers
                for vc in controllers! {
                    if vc is UserListAddStaffVC {
                        _ = self.navigationController?.popToViewController(vc as! UserListAddStaffVC, animated: true)
                    }
                }
                
            }else{
                if strSucessStatus == "fail"{
                    
                }else{
                    let msg = response["message"] as? String ?? ""
                    objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                }
            }
            }
        }) { (error) in
           objWebserviceManager.StartIndicator()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
}
}
//MARK Get TimeSloat
extension UsersAddServiceVC{
    func timeSloats(){
//           var arrOpeningTimes = [openingTimes]()
        arrOpeningTimes.removeAll()
        let arrDays = ["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"]
        for day in arrDays{
            let objTimeSloat = timeSloat.init(startTime:"10:00 AM",endTime:"07:00 PM")
            let arr = [objTimeSloat] as! [timeSloat]
            var objOpenings:openingTimes?
                objOpenings = openingTimes.init(open: true, day: day, times: arr)
            if objOpenings?.arrTimeSloats.count != 0{
            arrOpeningTimes.append(objOpenings!)
            }
        }
        callWebserviceForGetBusinessProfile()
    }
    
    
    func jsonFromArray(arrBusinessHours:[openingTimes]) ->String{
        var arr = [[String:Any]]()
        var dataString = ""
        for (index,openings) in arrBusinessHours.enumerated(){
            var dic = [:] as [String:Any]
            for time in openings.arrTimeSloats{
                let das = getDayFromSelectedDate(strDate: openings.strDay)
                dic = ["day":das,
                        "startTime":time.strStartTime,
                        "endTime":time.strEndTime]
                //"status":Int(truncating:NSNumber.init(value: openings.isOpen))
                if openings.arrTimeSloats.count > 1{
                arr.append(dic)
                }
            }
            if openings.arrTimeSloats.count == 1{
                arr.append(dic)
            }
        }
       
        if let objectData = try? JSONSerialization.data(withJSONObject: arr, options: JSONSerialization.WritingOptions(rawValue: 0)) {
            let objectString = String(data: objectData,encoding: .utf8)
            dataString = objectString!
            return dataString
        }
        return dataString
    }
}


//MARK: - Navigation method
extension UsersAddServiceVC{
    func popToViewContruller(){
        let controllers = self.navigationController?.viewControllers
        for vc in controllers! {
            if vc is userSearchVC {
                _ = self.navigationController?.popToViewController(vc as! userSearchVC, animated: true)
            }
        }
    }
    
    
    func callWebserviceForGetBusinessProfile(){
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        
        objActivity.startActivityIndicator()
        
        objServiceManager.requestGetForJson(strURL: WebURL.getBusinessProfile, params: [:], success: { response in
            if  response["status"] as! String == "success"{
                print(response)
                let rs = response["artistRecord"] as! NSArray
                let dic = rs[0] as! [String:Any]
                self.saveData(dic)
            }else{
                let msg = response["message"] as? String ?? ""

                objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
            }
        }) { error in
            
            // objAppShareData.showDeafultAlert(withTitle: message.error.title, message: message.error.wrong, on: self)
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
    func saveData(_ dicResponse:[String:Any]){
        print(dicResponse)
        let arr = dicResponse["businessHour"] as! [[String:Any]]
        let bankStatus = dicResponse["bankStatus"] as! Bool
        UserDefaults.standard.set(bankStatus, forKey: UserDefaults.keys.bankStatus)
        UserDefaults.standard.synchronize()
        if arr.count>0{
            self.removeTimeObjectsArray()
            for dic in arr{
                if let d = dic["day"] as? Int{
                    for (index,openings) in self.arrOpeningTimes.enumerated(){
                        if d == index{
                            openings.isOpen = true
                            openings.strDay = (day(rawValue:index)?.string)!
                            let objTimeSloat = timeSloat.init(startTime:dic["startTime"] as! String,endTime:dic["endTime"] as! String)
                            openings.arrTimeSloats.append(objTimeSloat!)
                        }
                    }
                }
            }
            
            if self.arrOpeningTimes.count > 0{
                for dic in self.arrOpeningTimes{
                    let selectIndex = self.arrOpeningTimes.index(of: dic)
                    if dic.arrTimeSloats.count == 0{
                        self.arrOpeningTimes.remove(at: selectIndex!)
                    }
                }
            }
            objAppShareData.objModelEditTimeSloat.arrValidTimes = self.arrOpeningTimes
            if objAppShareData.objAddStaffDoneSubSubServices.arrDoneTimeSloartJson == ""{
               objAppShareData.addStaffTimeSloat = self.jsonFromArray(arrBusinessHours: self.arrOpeningTimes)
            }else{
                objAppShareData.addStaffTimeSloat = objAppShareData.objAddStaffDoneSubSubServices.arrDoneTimeSloartJson
            }
        }
    }
    func removeTimeObjectsArray(){
        for obj in self.arrOpeningTimes{
            obj.arrTimeSloats.removeAll()
            obj.isOpen = false
        }
        
        
    }
    

    func getDayFromSelectedDate(strDate:String)-> Int{
        var Day = 0
        
        if strDate == "Sunday"{
            Day = 6//"Sun"
        }else if strDate == "Monday"{
            Day = 0//Mon"
        }else if strDate == "Tuesday"{
            Day = 1//Tue"
        }else if strDate == "Wednesday"{
            Day = 2//"Wed"
        }else if strDate == "Thursday"{
            Day = 3//"Thu"
        }else if strDate == "Friday"{
            Day = 4//"Fri"
        }else if strDate == "Saturday"{
            Day = 5//"Sat"
        }else{
            Day = 0
        }
        return Day
    }
}



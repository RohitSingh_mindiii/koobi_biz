//
//  BookingServiceSelectionVC.swift
//  MualabCustomer
//
//  Created by Mac on 08/02/18.
//  Copyright © 2018 Mindiii. All rights reserved.

import UIKit

class BookingServiceSelectionVC: UIViewController  ,UITableViewDelegate, UITableViewDataSource , UITextFieldDelegate{
    
    var arrSelectedServicess = [AddStaffStaffService]()
    var arrSubSubServices = [AddStaffSubSubServices]()
    var strServiceName = ""
    

    @IBOutlet weak var imgInCall: UIButton!
    @IBOutlet weak var imgOutCall: UIButton!
    @IBOutlet weak var viewInCall: UIView!
    @IBOutlet weak var viewOutCall: UIView!
    
    @IBOutlet weak var tblSubSubService: UITableView!
    @IBOutlet weak var lblNoDataFound: UILabel!
    @IBOutlet weak var lblHeaderName: UILabel!
    @IBOutlet weak var stackIncallPrice: UIStackView!
    @IBOutlet weak var stackOutcallPrice: UIStackView!

    @IBOutlet weak var heightDetailView: NSLayoutConstraint!
    //cell detail outlate
    @IBOutlet weak var viewCellDetail: UIView!
    @IBOutlet weak var lblServiceName: UILabel!
    @IBOutlet weak var txtIncallPrice: UITextField!
    @IBOutlet weak var txtOutcallPrice: UITextField!
    @IBOutlet weak var txtCompletionTime: UITextField!


    //time picker view outlate
    @IBOutlet weak var timePicker: UIDatePicker!
    @IBOutlet weak var viewTimePicker: UIView!
    
    private var indexPath = 0
    
    //MARK: - System method
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegateMethod()
        self.addAccesorryToKeyBoard()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.viewTimePicker.isHidden = true
        self.lblHeaderName.text = objAppShareData.objBookSerSelectionVC.strServiceName
    }
}
//MARK: - textField extension method
extension BookingServiceSelectionVC{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var edit = true
        var newLength = 0
        var searchString: String? = nil
        if textField == self.txtIncallPrice {
            
                 if (string.count ) != 0
                {
                    searchString = self.txtIncallPrice.text! + (string).uppercased()
                    newLength = (self.txtIncallPrice.text?.count)! + string.count - range.length
                }
                else {
                    searchString = (self.txtIncallPrice.text as NSString?)?.substring(to: (self.txtIncallPrice.text?.count)! - 1).uppercased()
                }
                if (searchString?.count ?? 0) == 7 {
                    self.txtIncallPrice.text = searchString
                    searchString = nil
                     edit = false
                }
                else {
                }
            let a = self.txtIncallPrice.text
            let dotsCount = a!.components(separatedBy: ".").count - 1
            if dotsCount > 0 && (string == "." || string == ",") {
                edit = false
                if dotsCount == 2{
                    let a1 = self.txtIncallPrice.text ?? ""
                    self.txtIncallPrice.text = String(a1.dropLast())
                }
                return false
            }
            if string == "," {
                textField.text! += "."
                edit = false
            }
            if newLength <= 7 && edit == true{
                edit = true
            }else{
                edit = false
            }
                return edit
}else if textField == self.txtOutcallPrice {
           
            if (string.count ) != 0
            {
                searchString = self.txtOutcallPrice.text! + (string).uppercased()
                newLength = (self.txtOutcallPrice.text?.count)! + string.count - range.length
            }
            else {
                searchString = (self.txtOutcallPrice.text as NSString?)?.substring(to: (self.txtOutcallPrice.text?.count)! - 1).uppercased()
            }
            if (searchString?.count ?? 0) == 7  {
                self.txtOutcallPrice.text = searchString
                searchString = nil
                edit = false
            }
            else {
            }
            
            let a = self.txtOutcallPrice.text
            let dotsCount = a!.components(separatedBy: ".").count - 1
            if dotsCount > 0 && (string == "." || string == ",") {
                edit = false
                if dotsCount == 2{
                    let a1 = self.txtOutcallPrice.text ?? ""
                    self.txtOutcallPrice.text = String(a1.dropLast())
                }
            }
            
            if string == "," {
                textField.text! += "."
                edit = false
            }
            
            if newLength <= 7 && edit == true{
                edit = true
            }else{
                edit = false
            }

            return edit
        }
        return edit
    }
}

//MARK: - Custome extension method
fileprivate extension BookingServiceSelectionVC{
    func delegateMethod(){
        self.tblSubSubService.delegate = self
        self.tblSubSubService.dataSource = self
        self.txtCompletionTime.delegate = self
        self.viewCellDetail.isHidden = true
        self.txtOutcallPrice.delegate = self
        self.txtIncallPrice.delegate = self
    }
    
    func UnderDevelopment(){
        objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
    }
    
    
    func addParamMethod(){
        
        let section = 0
        let indexPath = IndexPath(row: self.indexPath, section: section)        
        let cell = tblSubSubService.cellForRow(at: indexPath) as! BookingServiceSelectionCell
       
        let a = Double(self.txtOutcallPrice.text ?? "") ?? 0.0
        let b = Double(self.txtIncallPrice.text ?? "") ?? 0.0

        self.txtOutcallPrice.text = String(format: "%.2f", ceil(a*100)/100)
        self.txtIncallPrice.text = String(format: "%.2f", ceil(b*100)/100)

        
        cell.lblInCallPrice.text =  String(self.txtIncallPrice.text!)
        cell.lblOutCallPrice.text =  String(self.txtOutcallPrice.text!)
        cell.lblCompletionTime.text = objAppShareData.showCompletionTime(title: self.txtCompletionTime.text!)
        cell.viewSelectedCellBG.backgroundColor = UIColor.green
        self.viewCellDetail.isHidden = true
        
        let obj  = ModelHoldAddStaffId(title: "", artistId: objAppShareData.objModelHoldAddStaff.artistId, artistServiceId: objAppShareData.objModelHoldAddStaff.artistServiceId, businessId: objAppShareData.objModelHoldAddStaff.businessId, completionTime: self.txtCompletionTime.text!, inCallPrice: cell.lblInCallPrice.text!, serviceId: objAppShareData.objModelHoldAddStaff.serviceId, outCallPrice: cell.lblOutCallPrice.text!, subserviceId: objAppShareData.objModelHoldAddStaff.subserviceId)
        
        if objAppShareData.objModelHoldAddStaff.arrParseData.count > 0 {
            var sameObject = false
            for obj in objAppShareData.objModelHoldAddStaff.arrParseData {
                let id = obj.artistServiceId
                if id == objAppShareData.objModelHoldAddStaff.artistServiceId {
                    sameObject = true
                    obj.inCallPrice = self.txtIncallPrice.text!
                    obj.outCallPrice = self.txtOutcallPrice.text!
                    obj.completionTime = self.txtCompletionTime.text!
                }
            }
            
            if sameObject == false {
                objAppShareData.objModelHoldAddStaff.arrParseData.append(obj!)
            }
        } else {
            objAppShareData.objModelHoldAddStaff.arrParseData.append(obj!)
        }
        
        print("arrParsData.count = ", objAppShareData.objModelHoldAddStaff.arrParseData.count)
    }
    
 
    func selectCompletionTime(){
        timePicker.minimumDate = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-dd-yy" // This formate is input formated .
        let formateDate = timePicker.date
        dateFormatter.dateFormat = "HH:mm" // Output Formated
        let StrDate = dateFormatter.string(from: formateDate)
        self.txtCompletionTime.text = StrDate
    }
}

//MARK: - textfield delegate datasource method
extension BookingServiceSelectionVC{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        if textField == self.txtCompletionTime{
            resignFirstResponder()
        }
        return true
    }
}

//MARK: - table view delegate datasource method
extension BookingServiceSelectionVC{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if objAppShareData.objBookSerSelectionVC.arrSubSubServices.count > 0 {
            self.lblNoDataFound.isHidden = true
        }else { self.lblNoDataFound.isHidden = false }
        return objAppShareData.objBookSerSelectionVC.arrSubSubServices.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let  cell = tableView.dequeueReusableCell(withIdentifier: "BookingServiceSelectionCell", for: indexPath) as? BookingServiceSelectionCell {
            let obj = objAppShareData.objBookSerSelectionVC.arrSubSubServices[indexPath.row]
            cell.lblName.text = obj.subSubServiceName
            cell.lblInCallPrice.text = String(obj.inCallPrice)
            cell.lblOutCallPrice.text = String(obj.outCallPrice)
            cell.lblCompletionTime.text = objAppShareData.showCompletionTime(title: obj.completionTime)
            cell.viewSelectedCellBG.backgroundColor = UIColor.clear
            
            let objBSSvc = objAppShareData.objBookSerSelectionVC.arrSelectedServicess
            if objBSSvc.count > 0 {
                var id = ""
                for abc in objBSSvc {
                    id =  String(abc.artistServiceId)
                    if id == String(obj.subSubServiceId)
                    {
                        cell.viewSelectedCellBG.backgroundColor = UIColor.green
                        objAppShareData.objModelHoldAddStaff.artistServiceId = String(obj.subSubServiceId)
                        
                        let obj  = ModelHoldAddStaffId(title: "", artistId: objAppShareData.objModelHoldAddStaff.artistId, artistServiceId: objAppShareData.objModelHoldAddStaff.artistServiceId, businessId: objAppShareData.objModelHoldAddStaff.businessId, completionTime: cell.lblCompletionTime.text!, inCallPrice: cell.lblOutCallPrice.text!, serviceId: objAppShareData.objModelHoldAddStaff.serviceId, outCallPrice: cell.lblInCallPrice.text!, subserviceId: objAppShareData.objModelHoldAddStaff.subserviceId)
                        
                        if objAppShareData.objModelHoldAddStaff.arrParseData.count > 0 {
                            var sameObject = false
                                    for obj in objAppShareData.objModelHoldAddStaff.arrParseData {
                                        let id = obj.artistServiceId
                                            if id == objAppShareData.objModelHoldAddStaff.artistServiceId {
                                                sameObject = true
                                            }
                                    }
                            if sameObject == false {
                                objAppShareData.objModelHoldAddStaff.arrParseData.append(obj!)
                            }
                        } else {
                            objAppShareData.objModelHoldAddStaff.arrParseData.append(obj!)
                        }
                    }
                }
            }
            
            if objAppShareData.objModelHoldAddStaff.arrParseData.count > 0 {
                var addedStatus = false
                for objArr in objAppShareData.objModelHoldAddStaff.arrParseData {
                    let id = objArr.artistServiceId
                    if id == String(obj.subSubServiceId) {
                        addedStatus = true
                        cell.lblInCallPrice.text = objArr.inCallPrice
                        cell.lblOutCallPrice.text = objArr.outCallPrice
                        cell.lblCompletionTime.text = objAppShareData.showCompletionTime(title: objArr.completionTime)
                    }
                }
                if addedStatus == true{
                    cell.viewSelectedCellBG.backgroundColor = UIColor.green
                }
            }
            return  cell
        } else{
            return UITableViewCell()
        }
    }
 //did select row at index
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.viewCellDetail.isHidden = false
        let obj = objAppShareData.objBookSerSelectionVC.arrSubSubServices[indexPath.row]
        self.lblServiceName.text =  obj.subSubServiceName
        
        self.indexPath = indexPath.row
        
        objAppShareData.objModelHoldAddStaff.artistServiceId = String(obj.subSubServiceId)
        
        let section = 0
        let indexPath = IndexPath(row: self.indexPath, section: section)
        let cell = tblSubSubService.cellForRow(at: indexPath) as! BookingServiceSelectionCell
        self.txtOutcallPrice.text = cell.lblOutCallPrice.text
        self.txtIncallPrice.text = cell.lblInCallPrice.text
        self.txtCompletionTime.text = obj.completionTime
        
        if objAppShareData.objModelHoldAddStaff.arrParseData.count > 0 {
            for dic in objAppShareData.objModelHoldAddStaff.arrParseData{
                let id = dic.artistServiceId
                if id == String(obj.subSubServiceId){
                    self.txtCompletionTime.text = dic.completionTime
                }
            }
        }

        if self.txtIncallPrice.text == ""{
            self.txtIncallPrice.text = "0"
        }
        if self.txtOutcallPrice.text == ""{
            self.txtOutcallPrice.text = "0"
        }
        
        var inCallPrice = double_t(self.txtIncallPrice.text!)
        inCallPrice = double_t("\(inCallPrice!)")
        var outCallPrice = double_t(self.txtOutcallPrice.text!)
        outCallPrice = double_t("\(outCallPrice!)")
        
            if inCallPrice == 0 || outCallPrice == 0 {
              self.heightDetailView.constant = 238
                if inCallPrice == 0{
                    self.stackIncallPrice.isHidden = true
                    self.stackOutcallPrice.isHidden = false
                    self.imgInCall.setImage(#imageLiteral(resourceName: "inactive_check_box"), for: .normal)
                    self.imgOutCall.setImage(#imageLiteral(resourceName: "active_check_box"), for: .normal)
                    self.viewInCall.isHidden = true
                }else if outCallPrice == 0{
                    self.stackIncallPrice.isHidden = false
                    self.stackOutcallPrice.isHidden = true
                    self.imgOutCall.setImage(#imageLiteral(resourceName: "inactive_check_box"), for: .normal)
                    self.imgInCall.setImage(#imageLiteral(resourceName: "active_check_box"), for: .normal)
                    self.viewOutCall.isHidden = true
                }
                if obj.inCallPrice != 0{
                    if inCallPrice == 0{
                        self.imgInCall.setImage(#imageLiteral(resourceName: "inactive_check_box"), for: .normal)
                        self.txtIncallPrice.text = String(obj.inCallPrice)
                    }else{
                        self.imgInCall.setImage(#imageLiteral(resourceName: "active_check_box"), for: .normal)
                    }
                    self.viewInCall.isHidden = false
                }
                
                if obj.outCallPrice != 0{
                    if outCallPrice == 0{
                        self.imgOutCall.setImage(#imageLiteral(resourceName: "inactive_check_box"), for: .normal)
                        self.txtOutcallPrice.text = String(obj.inCallPrice)
                    }else{
                        self.imgOutCall.setImage(#imageLiteral(resourceName: "active_check_box"), for: .normal)
                    }
                    self.viewOutCall.isHidden = false
                }
            }else{
                fullViewHeight()
            }
    }
    
    
    func fullViewHeight(){
        self.stackIncallPrice.isHidden = false
        self.stackOutcallPrice.isHidden = false
        self.viewOutCall.isHidden = false
        self.viewInCall.isHidden = false
        self.heightDetailView.constant = 265
        self.imgInCall.setImage(#imageLiteral(resourceName: "active_check_box"), for: .normal)
        self.imgOutCall.setImage(#imageLiteral(resourceName: "active_check_box"), for: .normal)
    }

    func returnType(){
        objAppShareData.showAlert(withMessage: "You have to keep at least one staff service", type: alertType.bannerDark, on: self)
        return
    }
    
    @available(iOS 11.0, *)
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
    {
        
        self.indexPath = indexPath.row
        let section = 0
        let indexPath1 = IndexPath(row: self.indexPath, section: section)
        let cell = self.tblSubSubService.cellForRow(at: indexPath1) as! BookingServiceSelectionCell
        if cell.viewSelectedCellBG.backgroundColor == UIColor.green {
        
        let objReminder = objAppShareData.objBookSerSelectionVC.arrSubSubServices[indexPath.row]
        var addedStatus = false
        let objBSSvc = objAppShareData.objBookSerSelectionVC.arrSelectedServicess
        
        if objBSSvc.count > 0 || objAppShareData.objModelHoldAddStaff.arrParseData.count > 0 {
            var id = ""
            
            for abc in objBSSvc {
                id =  String(abc.artistServiceId)
                if id == String(objReminder.subSubServiceId)
                {
                    addedStatus = true
                }
            }
            
            if addedStatus == true{
                
            }else {
                if objAppShareData.objModelHoldAddStaff.arrParseData.count > 0 {
                    for objArr in objAppShareData.objModelHoldAddStaff.arrParseData {
                        let id = objArr.artistServiceId
                        if id == String(objReminder.subSubServiceId) {
                            addedStatus = true
                        }}
                }
            }
        }
        
        let Delete = UIContextualAction(style: .normal, title:  "", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            
           // let objReminder = objAppShareData.objBookSerSelectionVC.arrSubSubServices[indexPath.row]
            objAppShareData.objModelHoldAddStaff.arrParseData = Array(Set(objAppShareData.objModelHoldAddStaff.arrParseData))
            
                if objAppShareData.objModelHoldAddStaff.arrParseData.count < 2 {
                    self.tblSubSubService.reloadData()
                    objAppShareData.showAlert(withMessage: "You have to keep at least one staff service", type: alertType.bannerDark, on: self)
                    return
                }
            
            let objData = objAppShareData.objBookSerSelectionVC.arrSubSubServices [indexPath.row]
            self.indexPath = indexPath.row
            let section = 0
            let indexPath = IndexPath(row: self.indexPath, section: section)
                
            let cell = self.tblSubSubService.cellForRow(at: indexPath) as! BookingServiceSelectionCell
                cell.viewSelectedCellBG.backgroundColor = UIColor.clear
            
            let deleteId = String(objData.subSubServiceId)
            var delete_Id = ""
            var deleteCell = false
            
                for objArr in objAppShareData.objModelHoldAddStaff.arrParseData {
                    let id = objArr.artistServiceId
                    let index = objAppShareData.objModelHoldAddStaff.arrParseData.index(of: objArr)
                    cell.lblInCallPrice.text = String(objData.inCallPrice)
                    cell.lblOutCallPrice.text = String(objData.outCallPrice)
                    cell.lblCompletionTime.text = objAppShareData.showCompletionTime(title: objData.completionTime)
                    if id == deleteId{
                    objAppShareData.objModelHoldAddStaff.arrParseData.remove(at: index!)
                    }
                }
            
                for objDataArray in objAppShareData.objBookSerSelectionVC.arrSelectedServicess {
                    let selectIndex = objAppShareData.objBookSerSelectionVC.arrSelectedServicess.index(of: objDataArray)
                    let selectId = String(objDataArray.artistServiceId)
                    if selectId == deleteId {
                        delete_Id = String(objDataArray._Id)
                        objAppShareData.objBookSerSelectionVC.arrSelectedServicess.remove(at: selectIndex!)
                     deleteCell = true
                  }
                }
            
//            for objDoneDataArray in objAppShareData.objAddStaffDoneSubSubServices.arrDoneSubService {
//                let selectIndex = objAppShareData.objAddStaffDoneSubSubServices.arrDoneSubService.index(of: objDoneDataArray)
//                let selectId = String(objDoneDataArray.artistServiceId)
//                if selectId == deleteId {
//                    objAppShareData.objAddStaffDoneSubSubServices.arrDoneSubService.remove(at: selectIndex!)
//                    delete_Id = String(objDoneDataArray._Id)
//                    deleteCell = true
//                }
//            }

            if deleteCell == true {
                self.callWebServicesForDeleteServices(Id: delete_Id)
            }

            success(true)
        })
        Delete.image = UIImage(named: "delete_Ser")
        Delete.backgroundColor = #colorLiteral(red: 0.9607843137, green: 0.01568627451, blue: 0.01568627451, alpha: 1)

        if addedStatus == true{
            let section = 0
            self.indexPath = indexPath.row
            let indexPath = IndexPath(row: self.indexPath, section: section)
            
            let cell = tblSubSubService.cellForRow(at: indexPath) as! BookingServiceSelectionCell
            if cell.viewSelectedCellBG.backgroundColor == UIColor.green{
                if objAppShareData.objModelHoldAddStaff.arrParseData.count > 0 {
                return UISwipeActionsConfiguration(actions: [Delete])
                }else {
                    return UISwipeActionsConfiguration(actions: [])
                }
            }else  {
                return UISwipeActionsConfiguration(actions: [])
            }
        }else {
            return UISwipeActionsConfiguration(actions: [])
        }
        }
        else{
            return UISwipeActionsConfiguration(actions: [])
        }
    }
}

//MARK: - button extension
extension BookingServiceSelectionVC {
    @IBAction func btnBackAction(_ sender: UIButton) {
        if objAppShareData.objBookSerSelectionVC.arrSelectedServicess.count == 0 {
            if objAppShareData.objModelHoldAddStaff.arrParseData.count == 0{
                self.navigationController?.popViewController(animated: true)
            return
            }
                let alertController = UIAlertController(title: "Alert", message: "You have to keep at least one staff service", preferredStyle: .alert)
                let OKAction = UIAlertAction(title: "Save", style: UIAlertAction.Style.default){
                    UIAlertAction in
                    self.CallAPIAddService()
                    }
                alertController.addAction(OKAction)
                self.present(alertController, animated: true, completion: nil)
        } else{
//            if objAppShareData.objBookSerSelectionVC.arrSelectedServicess.count != objAppShareData.objModelHoldAddStaff.arrParseData.count {
            let alertController = UIAlertController(title: "Alert", message: "Are you sure want to discard changes?", preferredStyle: .alert)
            let OKAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default){
                UIAlertAction in
                self.copyArray()
                let controllers = self.navigationController?.viewControllers
                for vc in controllers! {
                    if vc is UsersAddServiceVC {
                        _ = self.navigationController?.popToViewController(vc as! UsersAddServiceVC, animated: true)
                    }
                }
            }
            let CancelAction = UIAlertAction(title: "No", style: UIAlertAction.Style.default){
                UIAlertAction in
            }
            alertController.addAction(OKAction)
            alertController.addAction(CancelAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    //detail cell alert button action
    @IBAction func btnCancel(_ sender: UIButton) {
        self.view.endEditing(true)
        self.viewCellDetail.isHidden = true
    }
    
    @IBAction func btnHiddenAlert(_ sender: UIButton) {
        self.view.endEditing(true)
        //self.viewCellDetail.isHidden = true
    }
    
    @IBAction func btnDone(_ sender: UIButton) {
        self.txtIncallPrice.text = self.txtIncallPrice.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        self.txtOutcallPrice.text = self.txtOutcallPrice.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        self.txtCompletionTime.text = self.txtCompletionTime.text?.trimmingCharacters(in: .whitespacesAndNewlines)

        if (self.txtIncallPrice.text?.hasPrefix("."))!{
            self.txtIncallPrice.text = "0"+self.txtIncallPrice.text!
        }
        if (self.txtOutcallPrice.text?.hasPrefix("."))!{
            self.txtOutcallPrice.text = "0"+self.txtOutcallPrice.text!
        }
        
        if (self.txtIncallPrice.text?.hasSuffix("."))!{
            self.txtIncallPrice.text = self.txtIncallPrice.text?.substring(to: (self.txtIncallPrice.text?.index(before: (self.txtIncallPrice.text?.endIndex)!))!)
        }
        if (self.txtOutcallPrice.text?.hasSuffix("."))!{
            self.txtOutcallPrice.text = self.txtOutcallPrice.text?.substring(to: (self.txtOutcallPrice.text?.index(before: (self.txtOutcallPrice.text?.endIndex)!))!)
        }
        if self.stackIncallPrice.isHidden == true && self.stackOutcallPrice.isHidden == true {
            objAppShareData.showAlert(withMessage: "Select staff service type", type: alertType.bannerDark, on: self)
        }else if self.stackIncallPrice.isHidden == false &&  self.txtIncallPrice.text == ""{
            objAppShareData.showAlert(withMessage: "Please enter incall price", type: alertType.bannerDark, on: self)
        }else if self.stackOutcallPrice.isHidden == false && self.txtOutcallPrice.text == ""{
            objAppShareData.showAlert(withMessage: "Please enter outcall price", type: alertType.bannerDark, on: self)
        }else if self.txtCompletionTime.text == ""{
            objAppShareData.showAlert(withMessage: "Please select completion time", type: alertType.bannerDark, on: self)
        }else {
            if self.txtIncallPrice.text == ""{
                self.txtIncallPrice.text = "0"
            }
            if self.txtOutcallPrice.text == ""{
                self.txtOutcallPrice.text = "0"
            }
            
            var inCallPrice = float_t(self.txtIncallPrice.text ?? "0")
            var outCallPrice = float_t(self.txtOutcallPrice.text ?? "0")
            
            if inCallPrice == nil {
                inCallPrice = 0
            }
            if outCallPrice == nil {
                outCallPrice = 0
            }
            
            inCallPrice = float_t("\(inCallPrice!)")
            outCallPrice = float_t("\(outCallPrice!)")

//
        if  self.stackIncallPrice.isHidden == false && inCallPrice == 0  {
            objAppShareData.showAlert(withMessage: "Incall price can't be zero", type: alertType.bannerDark, on: self)
        }else if outCallPrice == 0 && self.stackOutcallPrice.isHidden == false {
            objAppShareData.showAlert(withMessage: "Outcall price can't be zero", type: alertType.bannerDark, on: self)
        }else{
            self.view.endEditing(true)
            
            if self.stackIncallPrice.isHidden == true{
                self.txtIncallPrice.text = "0"
            }
            if self.stackOutcallPrice.isHidden == true{
                self.txtOutcallPrice.text = "0"
            }
            addParamMethod()
        }
    }
  }
}


//MARK:- phonepad return keyboard
extension BookingServiceSelectionVC{
    
    func addAccesorryToKeyBoard(){
        let keyboardDoneButtonView = UIToolbar()
        keyboardDoneButtonView.sizeToFit()
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style:.plain, target: self, action: #selector(self.resignKeyBoard))
        doneButton.tintColor = #colorLiteral(red: 0.05882352963, green: 0.180392161, blue: 0.2470588237, alpha: 1)
        
        keyboardDoneButtonView.items = [flexBarButton, doneButton]
        
        txtCompletionTime.inputAccessoryView = keyboardDoneButtonView
        txtIncallPrice.inputAccessoryView = keyboardDoneButtonView
        txtOutcallPrice.inputAccessoryView = keyboardDoneButtonView
    }
    @objc func resignKeyBoard(){
        txtIncallPrice.endEditing(true)
        txtOutcallPrice.endEditing(true)
        txtCompletionTime.endEditing(true)
        self.view.endEditing(true)
        self.txtOutcallPrice.text = self.txtOutcallPrice.text?.trimmingCharacters(in: .whitespacesAndNewlines)
    }
}

//MARK: - time picker method
extension BookingServiceSelectionVC{
    @IBAction func btnselectCompletionTime(_ sender: UIButton) {
        self.view.endEditing(true)
        self.viewTimePicker.isHidden = false
    }
    
    @IBAction func btnDoneTimeAction(_ sender: UIButton) {
        selectCompletionTime()
        self.viewTimePicker.isHidden = true
    }
    
    @IBAction func btnCancelTime(_ sender: UIButton) {
        self.viewTimePicker.isHidden = true
    }
    
    @IBAction func btnChangelTime(_ sender: UIButton) {
        //selectCompletionTime()
    }
    
}

//MARK: - addmore and done service action
extension BookingServiceSelectionVC{
    @IBAction func btnDoneServicess(_ sender: UIButton) {
        if objAppShareData.objModelHoldAddStaff.arrParseData.count > 0 {
        CallAPIAddService()
        }else{
            objAppShareData.showAlert(withMessage: "Select staff service", type: alertType.bannerDark, on: self)
        }
    }
    
    @IBAction func btnAddMoreServicesss(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}


//MARK: - call api
extension BookingServiceSelectionVC{
    func CallAPIAddService(){
        objServiceManager.StartIndicator()
        objAppShareData.objModelHoldAddStaff.arrParseData = Array(Set(objAppShareData.objModelHoldAddStaff.arrParseData))
        var serviceJosn = [String: Any]()
        objAppShareData.objModelHoldAddStaff.serviceJosnArray.removeAll()
        for i in 0..<objAppShareData.objModelHoldAddStaff.arrParseData.count{
            let objMain = objAppShareData.objModelHoldAddStaff.arrParseData[i]
 
                    //serviceJosn["_id"] = objMain.
                    serviceJosn["artistServiceId"] = objMain.artistServiceId
                    serviceJosn["businessId"] = objMain.businessId
                    serviceJosn["completionTime"] = objMain.completionTime
                    serviceJosn["inCallPrice"] = objMain.inCallPrice
                    serviceJosn["outCallPrice"] = objMain.outCallPrice
                    serviceJosn["serviceId"] = objMain.serviceId
                    serviceJosn["subserviceId"] = objMain.subserviceId
                    objAppShareData.objModelHoldAddStaff.serviceJosnArray.append(serviceJosn)
        }
        if let objectData = try? JSONSerialization.data(withJSONObject: objAppShareData.objModelHoldAddStaff.serviceJosnArray, options: JSONSerialization.WritingOptions(rawValue: 0)) {
            objAppShareData.objModelHoldAddStaff.jsonObjectString = String(data: objectData, encoding: .utf8)!
        }
 
        var strType = ""
        if objAppShareData.fromEditStaff == true {
            strType = "edit"
        }
        
        let param = ["type":strType, "artistId":objAppShareData.objModelHoldAddStaff.artistId,"businessId":objAppShareData.objModelHoldAddStaff.businessId,"staffService":objAppShareData.objModelHoldAddStaff.jsonObjectString] as [String : AnyObject]
        objServiceManager.StartIndicator()
        objWebserviceManager.requestPostForJson(strURL: WebURL.addStaffService, params: param , success: { response in
            
            objServiceManager.StopIndicator()
            let strSucessStatus = response["status"] as? String
            if strSucessStatus == k_success{
                self.saveData(dict:response)
                objAppShareData.editStaff = false
                let controllers = self.navigationController?.viewControllers
                for vc in controllers! {
                    if vc is UsersAddServiceVC {
                        _ = self.navigationController?.popToViewController(vc as! UsersAddServiceVC, animated: true)
                    }
                }
            }else{
                objServiceManager.StopIndicator()
                if strSucessStatus == "fail"{
                    
                }else{
                    let msg = response["message"] as? String ?? ""
                    objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                }
            }
         }) { (error) in
            objServiceManager.StopIndicator()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
    
    func saveData(dict:[String:Any]){
        if let dict = dict["staffServices"] as? [[String : Any]]{
            objAppShareData.objBookSerSelectionVC.arrSelectedServicess.removeAll()
            for dicService in dict{
                
                let obj1 = AddStaffDoneSubSubServicesData.init(dict: dicService)
                objAppShareData.objAddStaffDoneSubSubServices.arrDoneSubService.append(obj1)
                
                let obj = AddStaffStaffService.init(dict: dicService)
                objAppShareData.objBookSerSelectionVC.arrSelectedServicess.append(obj)
            }
            self.copyArray()

        }
    }
    
    func copyArray(){
        objAppShareData.objModelHoldAddStaff.arrParseData.removeAll()
        if objAppShareData.objBookSerSelectionVC.arrSelectedServicess.count > 0 {
            for newDic in objAppShareData.objBookSerSelectionVC.arrSelectedServicess{
                let newArtistServiceId = String(newDic.artistServiceId)
                let newArtistId = String(newDic.artistId)
                let newBusinessId = String(newDic.businessId)
                let newCompletionTime = String(newDic.completionTime)
                let newInCallPrice = newDic.inCallPrice
                let newOutCallPrice = newDic.outCallPrice
                let newServiceId = String(newDic.serviceId)
                let newSubserviceId = String(newDic.subServiceId)
                
                let objModel = ModelHoldAddStaffId(title: "", artistId: newArtistId, artistServiceId: newArtistServiceId, businessId: newBusinessId, completionTime: newCompletionTime, inCallPrice: String(newInCallPrice), serviceId: newServiceId, outCallPrice: String(newOutCallPrice), subserviceId: newSubserviceId)
                if objAppShareData.objModelHoldAddStaff.arrParseData.count > 0{
                    for dic in objAppShareData.objModelHoldAddStaff.arrParseData{
                        if newArtistServiceId == dic.artistServiceId{
                        }else{
                            objAppShareData.objModelHoldAddStaff.arrParseData.append(objModel!)
                        }
                    }

                }else{
                    objAppShareData.objModelHoldAddStaff.arrParseData.append(objModel!)
                }
                
            }
        }
    }
}
//MARK: - extension delete service api
extension BookingServiceSelectionVC{
    func callWebServicesForDeleteServices(Id:String){
    let param = [ "addServiceId":Id] as [String : AnyObject]
    
    objWebserviceManager.requestPostForJson(strURL: WebURL.deleteStaffService, params: param , success: { response in
        
        objServiceManager.StopIndicator()
        let strSucessStatus = response["status"] as? String
        if strSucessStatus == k_success{
           let message =  response["message"] as? String ?? "service delete successfully"
            objAppShareData.showDeafultAlert(withTitle: "Alert", message: message, on: self)
        }else{
            objServiceManager.StopIndicator()
            if strSucessStatus == "fail"{
                
            }else{
                let msg = response["message"] as? String ?? ""
                objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
            }
        }
    }) { (error) in
        objServiceManager.StopIndicator()
        objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
    }
}
}

//MARK: - acellDetail alert action
extension BookingServiceSelectionVC{
    @IBAction func btnIncall(_ sender: UIButton) {
        if  self.stackIncallPrice.isHidden == true {
        self.stackIncallPrice.isHidden = false
        self.imgInCall.setImage(#imageLiteral(resourceName: "active_check_box"), for: .normal)
        PlusViewHeight()
        }else{
            self.stackIncallPrice.isHidden = true
            self.imgInCall.setImage(#imageLiteral(resourceName: "inactive_check_box"), for: .normal)
            MinusViewHeight()
        }

    }
    @IBAction func btnOutcall(_ sender: UIButton) {
        if  self.stackOutcallPrice.isHidden == true {
            self.stackOutcallPrice.isHidden = false
            self.imgOutCall.setImage(#imageLiteral(resourceName: "active_check_box"), for: .normal)
            PlusViewHeight()
        }else{
            self.stackOutcallPrice.isHidden = true
            self.imgOutCall.setImage(#imageLiteral(resourceName: "inactive_check_box"), for: .normal)
            MinusViewHeight()
        }
    }
    
    func PlusViewHeight(){
        if self.heightDetailView.constant == 200 {
            self.heightDetailView.constant = 238
        }else if self.heightDetailView.constant == 238 {
            self.heightDetailView.constant = 265
        }
    }
    
    func MinusViewHeight(){
        if self.heightDetailView.constant == 265 {
            self.heightDetailView.constant = 238
        }else if self.heightDetailView.constant == 238 {
            self.heightDetailView.constant = 200
        }
    }
}

//MARK: - Navigation method
extension BookingServiceSelectionVC{
    func popToViewContruller(){
        let controllers = self.navigationController?.viewControllers
        for vc in controllers! {
            if vc is userSearchVC {
                _ = self.navigationController?.popToViewController(vc as! userSearchVC, animated: true)
            }
        }
    }
}

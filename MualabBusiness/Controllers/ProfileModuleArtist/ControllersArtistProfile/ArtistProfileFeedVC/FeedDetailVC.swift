//
//  FeedDetailVC.swift
//  MualabBusiness
//
//  Created by Mindiii on 3/8/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//


import UIKit
import Accounts
import AVFoundation
import AVKit
import Social
import Alamofire
import AlamofireImage
import FirebaseAuth
import TLPhotoPicker
import Photos
import  HCSStarRatingView

class FeedDetailVC:  UIViewController,UITableViewDelegate, UITableViewDataSource,AVPlayerViewControllerDelegate{
    var arrFeedsLocal = [feeds]()
    var indexToScroll:Int = 0
    var objArtistDetails = ArtistDetails(dict: ["":""])
    @IBOutlet weak var lblNoDataFound: UIView!
    @IBOutlet weak var tblFeeds: UITableView!
    @IBOutlet weak var lblHeader: UILabel!

    fileprivate var indexLastViewMoreView = 0
    var myId = ""
     var  selectedOtherIdForProfile = ""
    var isOtherSelectedForProfile = false
    fileprivate var tblView: UITableView?
    fileprivate var suggesionType = ""
    var strType = ""
    var feedId = ""
    var headerTitle = "Feed Detail"
    fileprivate var objUserDetailArtistProfile = UserDetailArtistProfile(dict: ["":""])
        
        fileprivate var arrFeedImages = [UIImage]()
        fileprivate var suggesionArray = [suggessions]()
 
        //MARK: - System method
        override func viewDidLoad() {
            super.viewDidLoad()
             self.lblNoDataFound.isHidden = true
             viewConfigure()
            NotificationCenter.default.addObserver(self, selector: #selector(self.redirectionFromServiceTag), name: NSNotification.Name(rawValue: "redirectionFromServiceTag"), object: nil)
           // self.tblFeeds.isScrollEnabled = false
            objAppShareData.arrFeedsForArtistData.removeAll()
            self.lblHeader.text = "Post"//self.headerTitle
        }
        
        @objc func hideShowBlurViewForSlideMenu(_ objNotify: Notification) {
            print(objNotify)
            self.view.endEditing(true)
            let dict = objNotify.userInfo as! [String:Any]
            //let position = dict["xPosition"] as! String
        }
        override var preferredStatusBarStyle: UIStatusBarStyle {
            return .default
        }
        override func viewWillDisappear(_ animated: Bool) {
            //self.tabBarController?.tabBar.isHidden = false
        }
         override func viewWillAppear(_ anisHidden: Bool) {
            self.tabBarController?.tabBar.isHidden = true
            if DoneEditProfile == true{
                objAppShareData.showAlert(withMessage: "Profile updated successfully.", type: alertType.banner, on: self)
            }
            objAppShareData.onProfile  = false
            
            if let dicUser = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] {
                myId = dicUser["_id"] as? String ?? ""
            }else{
                let userInfo = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] ?? ["":""]
                myId = userInfo["_id"] as? String ?? ""
            }
 
            self.tblFeeds.reloadData()
            if objAppShareData.isExploreGridToAllFeed{
                objAppShareData.arrFeedsForArtistData = self.arrFeedsLocal
                self.openFeedList()
            }else{
                self.loadImagesWithPage()
            }
        }
    func openFeedList(){
        self.tblFeeds.reloadData()
        let indexPath = IndexPath.init(row: indexToScroll, section: 0)
        if objAppShareData.arrFeedsForArtistData.count > indexToScroll{
            self.tblFeeds.scrollToRow(at: indexPath, at: .top, animated: false)
        }
    }
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
        }
        
        override func viewDidDisappear(_ animated: Bool) {
        }
        
        func hideRevealViewController(){
            if revealViewController() != nil {
                revealViewController().setFrontViewPosition(FrontViewPosition.left, animated: true)
            }
        }
    
    }
    
    //MARK: - custome method
    extension FeedDetailVC{
 
        func viewConfigure(){
 
            self.tblFeeds.delegate = self
            self.tblFeeds.dataSource = self
            self.lblNoDataFound.isHidden = true
             //Get my Id
            if let dicUser = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] {
                myId = dicUser["_id"] as? String ?? ""
            }

        }
      
        func underDevelopment(){
            objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
        }
    }
    
    //MARK: - Button action extension
    fileprivate extension FeedDetailVC{
        
        @IBAction func btnBackAction(_ sender: Any){
            objAppShareData.clearNotificationData()
            objAppShareData.arrFeedsForArtistData.removeAll()
            self.navigationController?.popViewController(animated: true)
        }
 
    }
    
    //MARK: - api calling
    extension FeedDetailVC{
        
         func loadImagesWithPage() {
            let dicParam = ["feedId": feedId,  "userId": myId,  ] as [String : Any]
                callWebserviceFor_getFeeds(dicParam: dicParam)
         }
 
        func callWebserviceFor_getFeeds(dicParam: [AnyHashable: Any]){
            
            objAppShareData.arrFeedsForArtistData.removeAll()
            
             if !objServiceManager.isNetworkAvailable(){
                objWebserviceManager.StopIndicator()
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
          
            var parameters = [String:Any]()
            parameters = dicParam as! [String : Any]
            print("parameters",parameters)
            objWebserviceManager.requestPostForJson(strURL: WebURL.feedDetails, params: parameters , success: { response in
           let strSucessStatus = response["status"] as? String
                objWebserviceManager.StopIndicator()
                
                 if strSucessStatus == k_success{
                    print("response = ",response )
                     if let arrDict = response["feedDetail"] as? [[String:Any]] {
                         if arrDict.count > 0 {
                            for dict in arrDict{
                                let obj = feeds.init(dict: dict)
                                          objAppShareData.arrFeedsForArtistData.append(obj!)
                                 }
                        }
                        if objAppShareData.arrFeedsForArtistData.count == 0{
                            self.lblNoDataFound.isHidden = false
                        }else{
                            self.lblNoDataFound.isHidden = true
                        }
                        self.tblFeeds.reloadData()
                    }
                }else{
                     if strSucessStatus == "fail"{
                        if objAppShareData.arrFeedsForArtistData.count==0{
                            self.lblNoDataFound.isHidden = false
                            objAppShareData.showAlert(withMessage: "Post is no longer available", type: alertType.bannerDark,on: self)
                        }
                    }else{
                        if let msg = response["message"] as? String{
                            objAppShareData.showAlert(withMessage: "Post is no longer available", type: alertType.bannerDark,on: self)
                        }
                    }
                    }
                
             }) { (error) in
                objWebserviceManager.StopIndicator()
                 self.updateUI()
                objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
            } }
        
        
        //call webservice for userDetail
        func callWebserviceFor_UserDetailWtih(dictParam : [String  : Any]){
            
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            
            objWebserviceManager.requestPostForJson(strURL: WebURL.getProfile, params: dictParam, success: { (response) in
                //objWebserviceManager.StopIndicator()
                
                if response["message"] as? String ?? "" == "ok" || response["message"] as? String ?? "" == k_success{ if let dict = response["userDetail"] as? [[String : Any]]{
                    print(dict)
                    ////
                    if !objAppShareData.isOtherSelectedForProfile {
                        var userInfo = [:] as! [String:Any]
                        if let dictnnn : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.businessInfoDic){
                            userInfo = dictnnn
                        }else{
                            let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as! Data
                            userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
                        }
                        let newDict = dict[0]
                        userInfo["latitude"] = newDict["latitude"]
                        userInfo["longitude"] = newDict["longitude"]
                        userInfo["address"] = newDict["address"]
                        //userInfo["address"] = newDict["address2"]
                        userInfo["address2"] = newDict["address"]
                        //userInfo["address2"] = newDict["address2"]
                        let data = NSKeyedArchiver.archivedData(withRootObject: userInfo)
                        UserDefaults.standard.set(data, forKey: UserDefaults.keys.userInfo)
                        UserDefaults.standard.synchronize()
                    }
                    ////
                    
                    //self.arrUsers.removeAll()
                    for dicService in dict{
                        self.objUserDetailArtistProfile = UserDetailArtistProfile.init(dict: dicService)
                    }
                     }
                    
                }else{
                    if let msg = response["message"] as? String{
                        objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                    }
                    
                }
            }) { (error) in
                objWebserviceManager.StopIndicator()
                objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
            }
        }
        
        
        //CALL WEBSERVICE FOR FOLOW UNFOLLOW ACTION
        func callWebservice_forFavoriteUnFavoriteArtist() {
            
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            objWebserviceManager.StartIndicator()
            
            //Get my Id
            var id = ""
            if let dicUser = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] {
                let myID = dicUser["_id"] as? String ?? ""
                id = myID
            }else{
                let userInfo = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] ?? ["":""]
                let myID = userInfo["_id"] as? String ?? ""
                id = myID
            }
            var strFType = ""
            if objUserDetailArtistProfile.favoriteStatus == "1" {
                strFType = "unfavorite"
            }else{
                strFType = "favorite"
            }
            
            let dicParam = ["artistId":objUserDetailArtistProfile._id,
                            "userId":id,
                            "type":strFType
                ] as [String : Any]
            
            objWebserviceManager.requestPost(strURL:WebURL.addFavorite, params: dicParam , success: { response in
                
                objServiceManager.StopIndicator()
                
                let keyExists = response["responseCode"] != nil
                if  keyExists {
                    sessionExpireAlertVC(controller: self)
                }
                let strSucessStatus = response["status"] as? String ?? ""
                if strSucessStatus == k_success{
                    
                    if self.objUserDetailArtistProfile.favoriteStatus == "1" {
                        self.objUserDetailArtistProfile.favoriteStatus = "0"
                    }else{
                        self.objUserDetailArtistProfile.favoriteStatus = "1"
                    }
                    
                 
 
                }else{
                    if let msg = response["message"] as? String{
                        objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                    }
                }
                
            }) { error in
                objServiceManager.StopIndicator()
                objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
            }
        }
        
        
        //CALL WEBSERVICE FOR FOLOW UNFOLLOW ACTION
        func callWebservice_forFollowUnFollow(followerId:String) {
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            objWebserviceManager.StartIndicator()
            //Get my Id
            var id = ""
            if let dicUser = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] {
                let myID = dicUser["_id"] as? String ?? ""
                id = myID
            }else{
                let userInfo = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] ?? ["":""]
                let myID = userInfo["_id"] as? String ?? ""
                id = myID
            }
            
            let dicParam = ["followerId":followerId,
                            "userId":id
                ] as [String : Any]
            
            objWebserviceManager.requestPost(strURL:WebURL.followFollowing, params: dicParam , success: { response in
                
                objServiceManager.StopIndicator()
                let keyExists = response["responseCode"] != nil
                if  keyExists {
                    sessionExpireAlertVC(controller: self)
                }else{
                    let strSucessStatus = response["status"] as? String ?? ""
                    if strSucessStatus == k_success{
                    self.loadImagesWithPage()
                     }else{
                        if let msg = response["message"] as? String{
                            objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                        }
                    }
                }
            }) { error in
                objServiceManager.StopIndicator()
                objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
            }
        }
        
        
        func updateUI() -> Void {
            
            if objAppShareData.arrFeedsForArtistData.count > 0{
                self.lblNoDataFound.isHidden = true
            }else{
                self.lblNoDataFound.isHidden = false
            }
                self.tblFeeds.reloadData()
          }
    }
    
    // MARK:- UITableView Delegate and Datasource
    extension FeedDetailVC : feedsTableCellDelegate {
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
            
            if tableView == self.tblFeeds{
                return objAppShareData.arrFeedsForArtistData.count
            }else{
                return 0
            }
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
            
            let cell = UITableViewCell()
            
            if tableView == tblFeeds{
                
 
                if objAppShareData.arrFeedsForArtistData.count>indexPath.row {
                    
                    let objFeeds = objAppShareData.arrFeedsForArtistData[indexPath.row]
                    var cellId = "ImageVideo"
                    
                    if objFeeds.feedType == "text"{
                        cellId = "Text"
                    }else{
                        cellId = "ImageVideo"
                    }
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! feedsTableCell
                    if objFeeds.feedType != "text"{
                        cell.btnShowTagNew.isHidden = true
                    }

                    //amit
                    cell.tag = indexPath.row
                    cell.indexPath = indexPath
                    cell.delegate = self
                    cell.btnHiddenMoreOption.isHidden = true
                    //amit
                    if objFeeds.isSave == 0{
                        cell.btnSaveToFolder.setTitle("Save to folder", for: .normal)
                        cell.btnSaveToFolder.setImage(#imageLiteral(resourceName: "inactive_book_mark_ico"), for: .normal)
                    }else{
                        cell.btnSaveToFolder.setTitle("Remove to folder", for: .normal)
                        cell.btnSaveToFolder.setImage(#imageLiteral(resourceName: "active_book_mark_ico"), for: .normal)
                    }
                    
                    cell.btnSaveToFolder.isHidden = false
                    cell.btnReportThisPost.isHidden = false
                    cell.btnShareThisPost.isHidden = false
                    
                    cell.viewMore.isHidden = true
                    
                    cell.lblUserName.text = objFeeds.userInfo?.userName
                    cell.lblCity.text = objFeeds.location
                    cell.lblTime.text = objFeeds.timeElapsed
                    
                    let url = URL(string: (objFeeds.userInfo?.profileImage)!)
                    if  url != nil {
                        //cell.imgProfile.af_setImage(withURL: url!)
                        cell.imgProfile.sd_setImage(with: url!, placeholderImage:UIImage(named: "cellBackground"))
                    }else{
                        cell.imgProfile.image = #imageLiteral(resourceName: "cellBackground")
                    }
                    
                    cell.txtCapView.text = objFeeds.caption
                    cell.lblCaption.text = objFeeds.caption
                    
                    if objFeeds.feedType == "text"{
//                        let font = UIFont(name: "Nunito-Regular", size: 16.0)
//                        let fontHash = UIFont(name: "Nunito-SemiBold", size: 16.0)
//                        let fontMention = UIFont(name: "Nunito-SemiBold", size: 16.0)
                        let font = UIFont(name: "Nunito-Regular", size: 16.0)
                        let fontHash = UIFont(name: "Nunito-Regular", size: 16.0)
                        let fontMention = UIFont(name: "Nunito-Regular", size: 16.0)
                        //cell.txtCapView.setText(text: objFeeds.caption, withHashtagColor: UIColor.black, andMentionColor: #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1), andCallBack: { (word, type) in
                        cell.txtCapView.setText(text: objFeeds.caption, withHashtagColor: UIColor.black, andMentionColor: UIColor.black, andCallBack: { (word, type) in
                            self.gotoExploreDetailVCWithSearchText(searchText: "\(word)", type: "\(type)", captionString: objFeeds.caption)
                        }, normalFont: font!, hashTagFont: fontHash!, mentionFont: fontMention!, isTappable:true)
                    }else{
//                        let font = UIFont(name: "Nunito-Regular", size: 16.0)
//                        let fontHash = UIFont(name: "Nunito-SemiBold", size: 16.0)
//                        let fontMention = UIFont(name: "Nunito-SemiBold", size: 16.0)
                        let font = UIFont(name: "Nunito-Regular", size: 16.0)
                        let fontHash = UIFont(name: "Nunito-Regular", size: 16.0)
                        let fontMention = UIFont(name: "Nunito-Regular", size: 16.0)
                        cell.txtCapView.setText(text: objFeeds.caption, withHashtagColor: UIColor.black, andMentionColor: UIColor.black, andCallBack: { (word, type) in
                            self.gotoExploreDetailVCWithSearchText(searchText: "\(word)", type: "\(type)", captionString: objFeeds.caption)
                        }, normalFont: font!, hashTagFont: fontHash!, mentionFont: fontMention!, isTappable:true)
                    }
                    
                    
                    if objFeeds.feedType != "text"{
                        cell.btnShowTagNew.isHidden = true
                        cell.pageControll.isHidden=true
                    }
                    if objFeeds.feedType == "video"{
                        cell.pageControll.isHidden=true
                        cell.btnShowTagNew.tag = indexPath.row
                        cell.pageControllView.isHidden = true;
                        if objFeeds.arrPhotoInfo.count > 0{
                            for newObj in objFeeds.arrPhotoInfo{
                                if newObj.arrTags.count > 0 || newObj.arrServiceTags.count > 0{
                                   // cell.btnShowTagNew.isHidden = false
                                }
                            }
                        }else{
                            cell.btnShowTagNew.isHidden = true
                        }
                        let height = self.view.frame.size.width * 0.75
                        cell.heightOfImageRatio.constant = height
                        
                    }else if objFeeds.feedType == "image"{
                        
                        var height = self.view.frame.size.width * 0.75
                        if objFeeds.feedImageRatio == "0"{
                            height = self.view.frame.size.width * 0.75
                        }else if objFeeds.feedImageRatio == "1"{
                            height = self.view.frame.size.width
                        }else{
                            height = self.view.frame.size.width
                        }
                        cell.heightOfImageRatio.constant = height
                        
                        if objFeeds.arrPhotoInfo.count > 0{
                            for newObj in objFeeds.arrPhotoInfo{
                                if newObj.arrTags.count > 0 || newObj.arrServiceTags.count > 0{
                                    cell.btnShowTagNew.isHidden = false
                                }
                            }
                        }else{
                            cell.btnShowTagNew.isHidden = true
                        }
                        cell.pageControll.isHidden=false
                        cell.pageControllView.isHidden = false;
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
                            //DispatchQueue.global().async {
                            cell.setPageControllMethod(objFeeds:objFeeds)
                            //}
                        }
                    }else{
                    }

                    ///////code by deependra////////
                    cell.imgPlay?.tag = indexPath.row
                    cell.imgPlay?.superview?.tag = indexPath.section
                    //
                    let tapForLike = UITapGestureRecognizer(target: self, action: #selector(self.likeOnTap))
                    tapForLike.numberOfTapsRequired = 2
                    
                    cell.btnShare.tag = indexPath.row
                    cell.btnShare.superview?.tag = indexPath.section
                    cell.btnLike.tag = indexPath.row
                    cell.btnLike.superview?.tag = indexPath.section
                    cell.imgFeeds?.tag = indexPath.row
                    cell.setDefaultDesign()
                    
                    
                    cell.lblLikeCount.text = "\(objFeeds.likeCount)"
                    cell.lblCommentCount.text = "\(objFeeds.commentCount)"
                    cell.lblTime.text = objFeeds.timeElapsed
                    if objFeeds.commentCount > 1{
                        cell.comment.text = "Comments"
                    }else{
                        cell.comment.text = "Comment"
                    }
                    
                    if objFeeds.likeCount > 1{
                        cell.likes.text = "Likes"
                    }else{
                        cell.likes.text = "Like"
                    }
                    
                    // cell.imgProfile.setImageFream()
                    
                    if objFeeds.feedType == "video"{
                        if objFeeds.arrFeed[0].videoThumb.count > 0 {
                            //cell.imgFeeds?.af_setImage(withURL:URL.init(string: objFeeds.arrFeed[0].videoThumb)!)
                            cell.imgFeeds?.sd_setImage(with: url!, placeholderImage:UIImage(named: "gallery_placeholder"))
                        }
                        cell.imgPlay?.isHidden = false
                        
                        let tapForPlayVideo = UITapGestureRecognizer(target: self, action: #selector(self.showVideoFromList))
                        tapForPlayVideo.numberOfTapsRequired = 1
                        cell.imgPlay?.addGestureRecognizer(tapForPlayVideo)
                        cell.imgPlay?.addGestureRecognizer(tapForLike)
                        tapForPlayVideo.require(toFail:tapForLike)
                        
                    }else if objFeeds.feedType == "image"{
                        
                        cell.imgPlay?.isHidden = true
                        cell.pageControllView.tag = indexPath.row
                        cell.setDefaultDesign()
                        let tap = UITapGestureRecognizer(target: self, action: #selector(self.showImageFromList))
                        tap.numberOfTapsRequired = 1
                        cell.pageControllView.addGestureRecognizer(tap)
                        cell.pageControllView.addGestureRecognizer(tapForLike)
                        tap.require(toFail:tapForLike)
                    }else{
                        if objAppShareData.validateUrl(objFeeds.caption) {
                            cell.lblCaption.tag = indexPath.row
                        }
                    }
                    
                    if objFeeds.isLike {
                        cell.btnLike.isSelected = true
                    }else {
                        cell.btnLike.isSelected = false
                    }
                    
                    cell.btnComment.tag = indexPath.row
                    cell.btnFollow.tag = indexPath.row
                    cell.lblLikeCount.tag = indexPath.row
                    cell.lblCommentCount.tag = indexPath.row
                    cell.lblCommentCount.isUserInteractionEnabled = true
                    let tapCommentCount = UITapGestureRecognizer(target: self, action: #selector(self.tappedCommentCount))
                    tapCommentCount.numberOfTapsRequired = 1
                    cell.lblCommentCount.addGestureRecognizer(tapCommentCount)
                    
                    if (cell.likes) != nil{
                        cell.likes.tag = indexPath.row
                    }
                    
                    let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.tappedLikeCount))
                    let tap2 = UITapGestureRecognizer(target: self, action: #selector(self.tappedLikeCount))
                    tap1.numberOfTapsRequired = 1
                    tap2.numberOfTapsRequired = 1
                    let myInt = objFeeds.likeCount
                    
                    if myInt > 0{
                        
                        cell.lblLikeCount.isUserInteractionEnabled = true
                        cell.likes.isUserInteractionEnabled = true
                        cell.lblLikeCount.addGestureRecognizer(tap2)
                        cell.likes.addGestureRecognizer(tap1)
                    }else {
                        
                        cell.lblLikeCount.isUserInteractionEnabled = false
                        cell.likes.isUserInteractionEnabled = false
                    }
                    
                    if String(objFeeds.userInfo?._id ?? 0) == myId{
                        cell.btnFollow.isHidden=true;
                    }else{
                        cell.btnFollow.isHidden=false;
                    }
                    
                    cell.btnFollow.tag = indexPath.row
                    cell.btnFollow.superview?.tag = indexPath.section
                    if objFeeds.followerStatus {
                        cell.btnFollow.setTitle("Following", for: .normal)
                        cell.btnFollow.setTitleColor(UIColor.black, for: .normal)
                        cell.btnFollow.layer.borderWidth = 1
                        cell.btnFollow.layer.borderColor = UIColor.black.cgColor
                        cell.btnFollow.backgroundColor = UIColor.clear
                    }else {
                        cell.btnFollow.setTitle("Follow", for: .normal)
                        cell.btnFollow.setTitleColor(UIColor.white, for: .normal)
                        cell.btnFollow.backgroundColor = appColor
                        cell.btnFollow.layer.borderWidth = 0
                    }
                    
                    if strType == "image"{
                        cell.imgPlay?.isHidden = true
                    }
                    
                    if String(objFeeds.userInfo?._id ?? 0) == myId{
                        cell.btnMore.isHidden=false;
                    }else{
                        cell.btnMore.isHidden=false;
                    }
                    
                    cell.btnMore.tag = indexPath.row
                    cell.btnSaveToFolder.tag = indexPath.row
                    cell.btnReportThisPost.tag = indexPath.row
                    cell.btnShareThisPost.tag = indexPath.row
                    cell.btnDeleteThisPost.tag = indexPath.row
                    cell.btnHiddenMoreOption.tag = indexPath.row
                    cell.btnMore.addTarget(self, action: #selector(btnMoreAction(_:)), for: .touchUpInside)
                    cell.btnSaveToFolder.addTarget(self, action: #selector(btnSaveToFolder(_:)), for: .touchUpInside)
                    cell.btnReportThisPost.addTarget(self, action: #selector(btnReportThisPost(_:)), for: .touchUpInside)
                    cell.btnShareThisPost.addTarget(self, action: #selector(btnShareThisPost(_:)), for: .touchUpInside)
                    cell.btnHiddenMoreOption.addTarget(self, action: #selector(btnHiddenMoreOption(_:)), for: .touchUpInside)
                    cell.btnDeleteThisPost.addTarget(self, action: #selector(btnDeleteThisPostMethod(_:)), for: .touchUpInside)
                    //}
                    return cell
                }
            }else {
                return cell
            }
            return cell
        }
        
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
            
            if tableView == tblFeeds {
                
                if objAppShareData.arrFeedsForArtistData.count > indexPath.row{
                    
                    let objFeeds = objAppShareData.arrFeedsForArtistData[indexPath.row]
                    
                    if objFeeds.feedType == "text" {
                        
                        if let height = objFeeds.cellHeight{
                            return height
                        }else{
                            let height = getSizeForText(objFeeds.caption,maxWidth: (tblFeeds.frame.size.width - 12),font:"Nunito-Regular", fontSize: 16.0).height + 112
                            objFeeds.cellHeight = height
                            return height
                        }
                        
                        // return getSizeForText(objFeeds.caption,maxWidth: (tblFeeds.frame.size.width - 12),font:"Nunito-Regular", fontSize: 16.0).height + 112
                    }else {
                        if objFeeds.caption.count > 0 {
                            if let height = objFeeds.cellHeight{
                                return height + 6
                            }else{
                                
                                var height = self.view.frame.size.width * 0.75 + 112
                                if objFeeds.feedImageRatio == "0"{
                                    height = self.view.frame.size.width * 0.75 + 112
                                }else if objFeeds.feedImageRatio == "1"{
                                    height = self.view.frame.size.width  + 112
                                }else if objFeeds.feedImageRatio == "2"{
                                        height = self.view.frame.size.width * 1.25 + 20
                                }else{
                                        height = self.view.frame.size.width *  0.75 + 112
                                }
                                if objFeeds.feedType == "video"{
                                     height = self.view.frame.size.width * 0.75 + 112
                                }
                                
                                
                                    let newHeight = height + getSizeForText(objFeeds.caption, maxWidth: (tblFeeds.frame.size.width - 8), font: "Nunito-Regular", fontSize: 16.0).height
                                objFeeds.cellHeight = newHeight
                               
                                return height + 6
                            }
                            
                        }else{
                            
                            if let height = objFeeds.cellHeight{
                                return height
                            }else{
                                var height = self.view.frame.size.width * 0.75 + 112
                               if objFeeds.feedImageRatio == "0"{
                                 height = self.view.frame.size.width * 0.75 + 112
                               }else if objFeeds.feedImageRatio == "1"{
                                 height = self.view.frame.size.width  + 112
                               }else if objFeeds.feedImageRatio == "2"{
                                height = self.view.frame.size.width * 1.25 + 20
                               }else{
                                height = self.view.frame.size.width *  0.75 + 112
                                }
                                if objFeeds.feedType == "video"{
                                    height = self.view.frame.size.width * 0.75 + 112
                                }
                                objFeeds.cellHeight = height
                                return height
                            }
                            //return self.view.frame.size.width * 0.75 + 100
                        }
                    }
                }
                
            }else {
                return 30
            }
            return 0;
        }
        
        private func tableView(_ tableView: UITableView, willDisplay cell: SuggessionTableCell, forRowAt indexPath: IndexPath){
            
            let objSuggession = self.suggesionArray[indexPath.row]
            if suggesionType == ""{
                cell.lblTag.text = "#"+objSuggession.tag
            }else{
                cell.lblName.text = "\(objSuggession.firstName) \(objSuggession.lastName)"
                cell.lblUserName.text = objSuggession.userName
                cell.imgProfile.image = UIImage.customImage.user
                if objSuggession.profileImage.count > 0 {
                    //cell.imgProfile.af_setImage(withURL:URL(string:objSuggession.profileImage)!)
                    cell.imgProfile.sd_setImage(with: URL(string:objSuggession.profileImage)!, placeholderImage:nil)
                }
            }
        }

        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
            let cell = tblView?.cellForRow(at: indexPath) as? feedsTableCell
            cell?.setDefaultDesign()
        }
        
        
        /////////getSizeForText
        func getSizeForText(_ text: String, maxWidth width: CGFloat, font fontName: String, fontSize: Float) -> CGSize{
            
            let constraintSize = CGSize(width: width, height: .infinity)
            let font:UIFont = UIFont(name: fontName, size: CGFloat(fontSize))!;
            let attributes = [NSAttributedString.Key.font : font];
            let frame: CGRect = text.boundingRect(with: constraintSize, options: .usesLineFragmentOrigin,attributes: attributes, context: nil)
            let stringSize: CGSize = frame.size
            return stringSize
        }
}
    
    //MARK: -  like, comment, shear button tablesub method
    extension FeedDetailVC{
        
        
        @objc func btnMoreAction(_ sender: UIButton)
        {
            let objData = objAppShareData.arrFeedsForArtistData[sender.tag]
            
            let indexPath = IndexPath(row:(sender as AnyObject).tag, section: (sender as AnyObject).superview!!.tag)
           if let cell = tblFeeds.cellForRow(at: indexPath) as? feedsTableCell{
            if cell.viewMore.isHidden == true{
                let id = UserDefaults.standard.string(forKey: UserDefaults.keys.userId) ?? ""
                if String(objData.userId) == id {
                    cell.btnReportThisPost.isHidden = true
                    cell.btnDeleteThisPost.isHidden = false
                    cell.btnShareThisPost.isHidden = false
                }else{
                    cell.btnReportThisPost.isHidden = false
                    cell.btnDeleteThisPost.isHidden = true
                    cell.btnShareThisPost.isHidden = false
                }
                cell.btnSaveToFolder.isHidden = false
                cell.viewMore.isHidden = false
                cell.btnHiddenMoreOption.isHidden = false
            } else {
                cell.btnSaveToFolder.isHidden = true
                cell.btnReportThisPost.isHidden = true
                cell.viewMore.isHidden = true
                cell.btnHiddenMoreOption.isHidden = true
                cell.setDefaultDesign()
            }
            //cell?.setDefaultDesign()
            
            let indexPathOld = IndexPath(row:indexLastViewMoreView, section: 0)
            //self.tblView?.reloadRows(at: [indexPathOld], with: UITableViewRowAnimation.fade)
            if let cellOld = (tblView?.cellForRow(at: indexPathOld) as? feedsTableCell){
                if indexPath != indexPathOld{
                    cellOld.btnSaveToFolder.isHidden = true
                    cellOld.btnReportThisPost.isHidden = true
                    cellOld.viewMore.isHidden = true
                    cellOld.setDefaultDesign()
                }
            }
            indexLastViewMoreView = (sender as AnyObject).tag
            }
        }
        @objc func btnSaveToFolder(_ sender: UIButton)
        {
   
            let objData = objAppShareData.arrFeedsForArtistData[sender.tag]
            let indexPath = IndexPath(row:(sender as AnyObject).tag, section: (sender as AnyObject).superview!!.tag)
            let cell = (tblFeeds?.cellForRow(at: indexPath) as? feedsTableCell)!
            cell.btnSaveToFolder.isHidden = false
            cell.btnReportThisPost.isHidden = true
            cell.setDefaultDesign()
            if objData.isSave == 0{
                //objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
                let sb: UIStoryboard = UIStoryboard(name: "Feed", bundle: Bundle.main)
                if let objVC = sb.instantiateViewController(withIdentifier:"SaveToFolderVC") as? SaveToFolderVC{
                    objVC.strFeedId = String(objData._id)
                    objAppShareData.btnAddHiddenONMyFolder = false
                    navigationController?.pushViewController(objVC, animated: true)
                }}else{
                self.webServiceForEditFolder(feedId:String(objData._id))
            }

            
         //   let objData = objAppShareData.arrFeedsForArtistData[sender.tag]
         //   let indexPath = IndexPath(row:(sender as AnyObject).tag, section: (sender as AnyObject).superview!!.tag)
            if  let cell = tblFeeds?.cellForRow(at: indexPath) as? feedsTableCell{
            cell.btnSaveToFolder.isHidden = true
            cell.btnReportThisPost.isHidden = true
            cell.btnHiddenMoreOption.isHidden = true
            cell.setDefaultDesign()
        }
    }
        
        
        @objc func btnDeleteThisPostMethod(_ sender: UIButton)
        {
          let objData = objAppShareData.arrFeedsForArtistData[sender.tag]
            let indexPath = IndexPath(row:(sender as AnyObject).tag, section: (sender as AnyObject).superview!!.tag)
            if  let cell = tblFeeds?.cellForRow(at: indexPath) as? feedsTableCell{
                cell.btnSaveToFolder.isHidden = true
                cell.btnReportThisPost.isHidden = true
                cell.btnHiddenMoreOption.isHidden = true
                cell.setDefaultDesign()
                var myId = ""
                if let dict = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] {
                    myId = dict[UserDefaults.keys.id] as? String ?? ""
                }
               
                let param = ["feedType":objData.feedType, "id":String(objData._id), "userId":myId]
                self.deleteCommentAlert(param: param, indexPath: indexPath)
              //  objAppShareData.showAlert(withMessage: "Under development", type: alertType.bannerDark,on: self)

            }
        }
        
        
        
        
        func deleteCommentAlert(param:[String:Any],indexPath:IndexPath){
            // Create the alert controller
            let alertController = UIAlertController(title: "Alert", message: "Are you sure want to remove this post?", preferredStyle: .alert)
            // Create the actions
            let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default) {
                UIAlertAction in
//                objAppShareData.arrFeedsForArtistData.remove(at: indexPath.row)
//                self.tblFeeds.reloadData()
                self.webServiceForDeletePost(param: param)
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
                UIAlertAction in
            }
            cancelAction.setValue(#colorLiteral(red: 1, green: 0.1490196078, blue: 0, alpha: 1), forKey: "titleTextColor")
            okAction.setValue(#colorLiteral(red: 0, green: 0.851996243, blue: 0.8281483054, alpha: 1), forKey: "titleTextColor")

            alertController.addAction(okAction)
            alertController.addAction(cancelAction)
            alertController.modalPresentationStyle = .fullScreen
            self.present(alertController, animated: true, completion: nil)
        }
        
        
        @objc func btnShareThisPost(_ sender: UIButton)
        {
            let objData = objAppShareData.arrFeedsForArtistData[sender.tag]
            
            let sb = UIStoryboard(name:"Voucher",bundle:Bundle.main)
            let objChooseType = sb.instantiateViewController(withIdentifier:"ShareVoucherCodeVC") as! ShareVoucherCodeVC
            objChooseType.objFeeds = objData
            objChooseType.fromVoucher = false
            self.navigationController?.pushViewController(objChooseType, animated: true)
/*
            
            
            let indexPath = IndexPath(row:(sender as AnyObject).tag, section: (sender as AnyObject).superview!!.tag)
if  let cell = tblFeeds?.cellForRow(at: indexPath) as? feedsTableCell{
    cell.btnHiddenMoreOption.isHidden = true
            cell.setDefaultDesign()
            //  objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
            let firstActivityItem = objData.caption
            var urlString = ""
    var deeplink = WebURL.Deeplinking+"feedDetail/"
    if objData.arrFeed.count > 0{
        urlString = objData.arrFeed[0].feedPost
        deeplink = deeplink+String( objData._id)
    }
    let secondActivityItem : NSURL = NSURL(string:urlString)!
    let deeplinhActivityItem : NSURL = NSURL(string:deeplink)!
    // If you want to put an image
    let image : UIImage = #imageLiteral(resourceName: "ShareImageOnSocial")
    
    let activityViewController : UIActivityViewController = UIActivityViewController(
        activityItems: [firstActivityItem, secondActivityItem, image,deeplinhActivityItem], applicationActivities: nil)
    
            // This lines is for the popover you need to show in iPad
            activityViewController.popoverPresentationController?.sourceView = (sender as! UIButton)
            
            // This line remove the arrow of the popover to show in iPad
            //    activityViewController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.allZeros
            activityViewController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.any
            activityViewController.popoverPresentationController?.sourceRect = CGRect(x: 150, y: 150, width: 0, height: 0)
            
            
            self.present(activityViewController, animated: true, completion: nil)
        }
           */
        }
        @objc func btnHiddenMoreOption(_ sender: UIButton)
        {
     //       let objData = objAppShareData.arrFeedsForArtistData[sender.tag]
            let indexPath = IndexPath(row:(sender as AnyObject).tag, section: (sender as AnyObject).superview!!.tag)
        if  let cell = tblFeeds?.cellForRow(at: indexPath) as? feedsTableCell{
            cell.btnHiddenMoreOption.isHidden = true
            cell.setDefaultDesign()
            }
        }
        
        
        
        
        func webServiceForDeletePost(param:[String:Any]){
            if !objServiceManager.isNetworkAvailable(){
                return
            }
            objWebserviceManager.StartIndicator()
           
            objWebserviceManager.requestPost(strURL: WebURL.deleteFeed, params: param  , success: { response in
                let keyExists = response["responseCode"] != nil
                if  keyExists {
                    objWebserviceManager.StopIndicator()
                }else{
                    let strStatus =  response["status"] as? String ?? ""
                    if strStatus == k_success{
                        self.viewWillAppear(true)
                    }else{
                        objWebserviceManager.StopIndicator()
                        if let msg = response["message"] as? String{
                            objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)  }}  }
            }){ error in
                objWebserviceManager.StopIndicator()
                showAlertVC(title: kAlertTitle, message: kErrorMessage, controller: self)
            } }
        
        func webServiceForEditFolder(feedId:String){
            if !objServiceManager.isNetworkAvailable(){
                return
            }
            objWebserviceManager.StartIndicator()
            let parameters : Dictionary = [
                "feedId" : feedId,
                "folderId":""] as [String : Any]
            
            objWebserviceManager.requestPost(strURL: WebURL.removeToFolder, params: parameters  , success: { response in
                
                let keyExists = response["responseCode"] != nil
                if  keyExists {
                    objWebserviceManager.StopIndicator()
                    
                }else{
                    
                    let strStatus =  response["status"] as? String ?? ""
                    if strStatus == k_success{
                        self.viewWillAppear(true)
                    }else{
                        objWebserviceManager.StopIndicator()
                        if let msg = response["message"] as? String{
                            objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                        }
                        
                    }
                }
            }){ error in
                objWebserviceManager.StopIndicator()
                showAlertVC(title: kAlertTitle, message: kErrorMessage, controller: self)
            }
        }
        
        @objc func btnReportThisPost(_ sender: UIButton)
        {
            let objData = objAppShareData.arrFeedsForArtistData[sender.tag]
            let indexPath = IndexPath(row:(sender as AnyObject).tag, section: (sender as AnyObject).superview!!.tag)
if  let cell = tblFeeds?.cellForRow(at: indexPath) as? feedsTableCell{
            cell.btnSaveToFolder.isHidden = true
            cell.btnReportThisPost.isHidden = true
            cell.btnHiddenMoreOption.isHidden = true
            cell.setDefaultDesign()
            let sb: UIStoryboard = UIStoryboard(name: "Feed", bundle: Bundle.main)
            if let objVC = sb.instantiateViewController(withIdentifier:"FeedReportVC") as? FeedReportVC{
                objVC.objModel = objData
                navigationController?.pushViewController(objVC, animated: true)
            }
            }
        }
        
        
        @objc func tappedLikeCount(_ myLabel: UITapGestureRecognizer) {
            let sender = myLabel.view
            let indexPath = IndexPath(row:sender!.tag,section: sender!.superview!.tag)
            if let cell = tblFeeds?.cellForRow(at: indexPath) as? feedsTableCell {
            cell.setDefaultDesign()
            
            let objFeeds = objAppShareData.arrFeedsForArtistData[(myLabel.view?.tag)!]
            let sb: UIStoryboard = UIStoryboard(name: "Feed", bundle: Bundle.main)
            if let objVC = sb.instantiateViewController(withIdentifier:"LikesListVC") as? LikesListVC{
                objVC.objFeeds = objFeeds
                objVC.hidesBottomBarWhenPushed = true
                navigationController?.pushViewController(objVC, animated: true)
            }
            }
        }
        
        @objc func likeOnTap(fromList recognizer: UITapGestureRecognizer) {
            let sender = recognizer.view
            let indexPath = IndexPath(row:sender!.tag,section: sender!.superview!.tag)
 if let cell = tblFeeds?.cellForRow(at: indexPath) as? feedsTableCell {
        let index = (sender as AnyObject).tag
            cell.setDefaultDesign()
            let objFeeds = objAppShareData.arrFeedsForArtistData[index!]
            //showAnimatedLikeUnlikeOn(cell: cell, objFeeds: objFeeds)
            if !objFeeds.isLike{
                likeUnlikePost(cell: cell, objFeeds: objFeeds)
            }
            }
        }
        
        @objc func tappedCommentCount(_ myLabel: UITapGestureRecognizer) {
            let sender = myLabel.view
            let indexPath = IndexPath(row:sender!.tag,section: sender!.superview!.tag)
            if let cell = tblFeeds?.cellForRow(at: indexPath) as? feedsTableCell {
            cell.setDefaultDesign()
            
            let sb: UIStoryboard = UIStoryboard(name: "Feed", bundle: Bundle.main)
            if let objVC = sb.instantiateViewController(withIdentifier:"CommentListVC") as? CommentListVC{
                
                objVC.selectedIndex = (myLabel.view?.tag)!
                objVC.isOtherThanFeedScreen = true
                
                navigationController?.pushViewController(objVC, animated: true)
                }
            }
        }
        
        @objc func showImageFromList(fromList recognizer: UITapGestureRecognizer) {
            let sender = recognizer.view
            let indexPath = IndexPath(row:sender!.tag,section: sender!.superview!.tag)
            let cell = tblFeeds?.cellForRow(at: indexPath) as! feedsTableCell
            cell.setDefaultDesign()
            cell.setTagsHidden(!cell.tagsHidden)

//
            return
            let objFeed = objAppShareData.arrFeedsForArtistData[(recognizer.view?.tag)!]
            let sb: UIStoryboard = UIStoryboard(name: "Add", bundle: Bundle.main)
            if let objVC = sb.instantiateViewController(withIdentifier:"showImagesVC") as? showImagesVC {
                objVC.isTypeIsUrl = true
                objVC.arrFeedImages = objFeed.arrFeed
                objVC.objFeeds = objFeed
                objVC.modalPresentationStyle = .fullScreen
                present(objVC, animated: true)// { _ in }
            }
        }
        
        @objc func showVideoFromList(fromList recognizer: UITapGestureRecognizer) {
            let sender = recognizer.view
            let indexPath = IndexPath(row:sender!.tag,section: sender!.superview!.tag)
 if let cell = tblFeeds?.cellForRow(at: indexPath) as? feedsTableCell {
    cell.setDefaultDesign()
            let objFeed = objAppShareData.arrFeedsForArtistData[(recognizer.view?.tag)!]
            if objFeed.arrFeed.count > 0 {
                addVideo(toVC: URL(string: (objFeed.arrFeed[0].feedPost))!)
            }
            }
        }
        
        func addVideo(toVC url: URL) {
            let controller = AVPlayerViewController()
            controller.player = AVPlayer(url: url)
            controller.delegate = self
            controller.player?.play()
            controller.modalPresentationStyle = .fullScreen
            present(controller, animated: true)// { _ in }
        }
    }
    
    //MARK: - click like,comment, shear button
    extension FeedDetailVC{
        
        @IBAction func btnSharePost(_ sender: Any) {
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            
            let indexPath = IndexPath(row: (sender as AnyObject).tag, section: (sender as AnyObject).superview!!.tag)
            let cell = tblView?.cellForRow(at: indexPath) as? feedsTableCell
            // let objFeeds: feeds? = objAppshare.arrFeeds[(sender as AnyObject).tag]
            let index = (sender as AnyObject).tag
            let objFeeds = objAppShareData.arrFeedsForArtistData[index!]
            if (objFeeds.feedType == "video"){
                showAlertVC(title: kAlertTitle, message: "Under development", controller: self)
            } else {
                showShareActionSheet(withObject: objFeeds, andTableCell: cell!)
            }
        }
        
        func showShareActionSheet(withObject objFeeds: feeds, andTableCell cell: feedsTableCell){
            showAlertVC(title: kAlertTitle, message: "Under development", controller: self)
        }
        
        @IBAction func btnComment(onFeed sender: Any) {
            
            let sb: UIStoryboard = UIStoryboard(name: "Feed", bundle: Bundle.main)
            if let objVC = sb.instantiateViewController(withIdentifier:"CommentListVC") as? CommentListVC{
                objVC.selectedIndex = (sender as AnyObject).tag
                objVC.isOtherThanFeedScreen = true
                navigationController?.pushViewController(objVC, animated: true)
            }
            
        }
        
        @IBAction func btnLikeFeed(_ sender: Any){
            let indexPath = IndexPath(row:(sender as AnyObject).tag, section: (sender as AnyObject).superview!!.tag)
          if  let cell = tblFeeds.cellForRow(at: indexPath) as? feedsTableCell{
            let index = (sender as AnyObject).tag
            let objFeeds = objAppShareData.arrFeedsForArtistData[index!]
            likeUnlikePost(cell: cell, objFeeds: objFeeds)
            }
        }
        @IBAction func btnFollow(_ sender: Any){
            let indexPath = IndexPath(row:(sender as AnyObject).tag, section: (sender as AnyObject).superview!!.tag)
            if  let cell = tblFeeds.cellForRow(at: indexPath) as? feedsTableCell{
                let index = (sender as AnyObject).tag
                let objFeeds = objAppShareData.arrFeedsForArtistData[index!]
              //  likeUnlikePost(cell: cell, objFeeds: objFeeds)
                self.callWebservice_forFollowUnFollow(followerId: String(objFeeds.userId))
            }
        }
   
        func likeUnlikePost(cell:feedsTableCell, objFeeds:feeds)->Void{
            
            if (objFeeds.isLike){
                objFeeds.isLike = false
                UIView.animate(withDuration: 0.2, animations: {() -> Void in
                    cell.btnLike.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
                }, completion: {(_ finished: Bool) -> Void in
                    UIView.animate(withDuration: 0.2, animations: {() -> Void in
                        cell.btnLike.transform = CGAffineTransform.identity
                        cell.btnLike.isSelected = false
                    })
                })
                objFeeds.likeCount = objFeeds.likeCount - 1
                if objFeeds.likeCount > 1{
                    cell.likes.text = "Likes"
                }else{
                    cell.likes.text = "Like"
                }
            }else{
                objFeeds.isLike = true
                UIView.animate(withDuration: 0.2, animations: {() -> Void in
                    cell.btnLike.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
                }, completion: {(_ finished: Bool) -> Void in
                    UIView.animate(withDuration: 0.2, animations: {() -> Void in
                        cell.btnLike.transform = CGAffineTransform.identity
                        cell.btnLike.isSelected = true
                    })
                })
                objFeeds.likeCount = objFeeds.likeCount + 1
                if objFeeds.likeCount > 1{
                    cell.likes.text = "Likes"
                }else{
                    cell.likes.text = "Like"
                }
            }
            cell.lblLikeCount.text = "\(objFeeds.likeCount)"
            
            
            //let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userData) as! [String:Any]
            
            var dob = "2000-01-01"
            var gender = "male"
            
            if let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] {
                dob = decoded["dob"] as? String ?? "2000-01-01"
                gender = decoded["gender"] as? String ?? "male"
            }
            
            
            let dicParam  = ["feedId": objFeeds._id,
                             "userId":objFeeds.userInfo?._id ?? 0,
                             "likeById":self.myId,
                             "age":objAppShareData.getAge(from: dob),
                             "gender":gender,
                             "city":objLocationManager.currentCLPlacemark?.locality ?? "",
                             "state":objLocationManager.currentCLPlacemark?.administrativeArea ?? "",
                             "country":objLocationManager.currentCLPlacemark?.country ?? "",
                             "type":"feed"
                ]  as [String : Any]
            
            self.callWebserviceFor_LikeUnlike(dicParam: dicParam)
            
            //   objAppshare.playButtonClickSound()
            let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.tappedLikeCount))
            let tap2 = UITapGestureRecognizer(target: self, action: #selector(self.tappedLikeCount))
            tap1.numberOfTapsRequired = 1
            tap2.numberOfTapsRequired = 1
            if objFeeds.likeCount > 0{
                cell.lblLikeCount.isUserInteractionEnabled = true
                cell.likes.isUserInteractionEnabled = true
                cell.lblLikeCount.addGestureRecognizer(tap2)
                cell.likes.addGestureRecognizer(tap1)
            }else {
                cell.lblLikeCount.isUserInteractionEnabled = false
                cell.likes.isUserInteractionEnabled = false
            }
        }
        
        
        
        func callWebserviceFor_LikeUnlike(dicParam: [AnyHashable: Any]){
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            var parameters = [String:Any]()
            parameters = dicParam as! [String : Any]
            objWebserviceManager.requestPost(strURL: WebURL.like, params: parameters, success: { response in
                objWebserviceManager.StopIndicator()
            }){ error in
                objWebserviceManager.StopIndicator()
            }
        }
        
        
        func gotoExploreDetailVCWithSearchText(searchText:String, type:String,captionString:String){
            
            if type == "hashtag" {
                
                let dicParam = [
                    "tabType" : "hasTag",
                    "tagId": "",
                    "title": searchText
                    ] as [String : Any]
                
                let sb: UIStoryboard = UIStoryboard(name: "Explore", bundle: Bundle.main)
                if let objVC = sb.instantiateViewController(withIdentifier:"ExploreDetailVC") as? ExploreDetailVC{
                    objVC.sharedDict = dicParam
                    navigationController?.pushViewController(objVC, animated: true)
                }
            }else{
                self.view.endEditing(true)
                
                if !objServiceManager.isNetworkAvailable(){
                    objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                    return
                }                
                
                let dicParam = ["userName":searchText]
                
                objServiceManager.requestPost(strURL: WebURL.profileByUserName, params: dicParam, success: { response in
                    if response["status"] as? String ?? "" == "success"{
                        var strId = ""
                        var strType = ""
                        if let dictUser = response["userDetail"] as? [String : Any]{
                            let myId = dictUser["_id"] as? Int ?? 0
                            strId = String(myId)
                            strType = dictUser["userType"] as? String ?? ""
                        }
                        let dic = [
                            "tabType" : "people",
                            "tagId": strId,
                            "userType":strType,
                            "title": searchText
                            ] as [String : Any]
                        self.openProfileForSelectedTagPopoverWithInfo(dict: dic)
                    }
                }) { error in
                }
            }
            
        }
    }
    
    // MARK: - Notification Actions
    extension FeedDetailVC {
        
        func didReceiveSingleTapAt(indexPath : IndexPath,feedsTableCell: feedsTableCell ){
            
            let objFeed = objAppShareData.arrFeedsForArtistData[indexPath.row]
            if objFeed.feedType == "video"{
                
                self.addVideo(toVC: URL(string: (objFeed.arrFeed[0].feedPost))!)
                
            }else if objFeed.feedType == "image" {
                
                let sb = UIStoryboard(name: "Add", bundle: Bundle.main)
                if let objShowImage = sb.instantiateViewController(withIdentifier:"showImagesVC") as? showImagesVC{
                    
                    objShowImage.isTypeIsUrl = true
                    objShowImage.arrFeedImages = objFeed.arrFeed
                    objShowImage.objFeeds = objFeed
                    objShowImage.modalPresentationStyle = .fullScreen
                    present(objShowImage, animated: true)
                }
            }
        }
        
        func didReceiveDoubleTapAt(indexPath : IndexPath,feedsTableCell: feedsTableCell ){
            
            let objFeed = objAppShareData.arrFeedsForArtistData[indexPath.row]
            if !objFeed.isLike{
                likeUnlikePost(cell: feedsTableCell, objFeeds: objFeed)
            }
            
        }
        
        func didReceiveLondPressAt(indexPath : IndexPath,feedsTableCell: feedsTableCell ){
            
        }
        
        func tagPopoverDidReceiveSingleTapWithInfo(dict: [AnyHashable: Any]){
            print("tagPopover dict = %@",dict)
            //NO redirection from Artist profile
            self.openProfileForSelectedTagPopoverWithInfo(dict: dict)
        }
        
        func openProfileForSelectedTagPopoverWithInfo(dict : [AnyHashable: Any]){
            
            var dictTemp : [AnyHashable : Any]?
            
            dictTemp = dict
            
            if let dict1 = dict as? [String:[String :Any]] {
                if let dict2 = dict1.first?.value {
                    dictTemp = dict2
                }
            }
            
            guard let dictFinal = dictTemp as? [String : Any] else { return }
            
            var strUserType: String?
            var tagId : Int?
            if  dictFinal["tabType"] as? String == "service"{
                
                self.view.endEditing(true)
                let sb: UIStoryboard = UIStoryboard(name: "ArtistProfile", bundle: Bundle.main)
                if let objVC = sb.instantiateViewController(withIdentifier:"ServiceDetailVC") as? ServiceDetailVC{
                    objVC.hidesBottomBarWhenPushed = true
                    objVC.param = dictFinal
                    if objAppShareData.onProfile  == false{
                    navigationController?.pushViewController(objVC, animated: true)
                }
                }


            }else{
            if let userType = dictFinal["userType"] as? String{
                strUserType = userType
                if let idTag = dictFinal["tagId"] as? Int{  tagId = idTag
                }else{  if let idTag = dictFinal["tagId"] as? String{  tagId = Int(idTag)
                    }
                }
                
                if self.myId == String(tagId ?? 0) {
                    objAppShareData.isOtherSelectedForProfile = false
                  //  self.gotoProfileVC()
                    return
                }
                if let strUserType = strUserType, let tagId =  tagId {
                    objAppShareData.selectedOtherIdForProfile  = String(tagId)
                    objAppShareData.isOtherSelectedForProfile = true
                    objAppShareData.selectedOtherTypeForProfile = strUserType  //"artist" or "user"
                    self.gotoProfileVC()
                }
                }
            }
        }
        
        func gotoProfileVC (){
            self.view.endEditing(true)
            let sb: UIStoryboard = UIStoryboard(name: "ArtistProfile", bundle: Bundle.main)
            if let objVC = sb.instantiateViewController(withIdentifier:"SWRevealViewController") as? SWRevealViewController{
                objVC.hidesBottomBarWhenPushed = true
                navigationController?.pushViewController(objVC, animated: true)
            }
        }
        
        
        @objc func redirectionFromServiceTag(_ objNotify: Notification){
            print(objNotify)
            print(objNotify.userInfo as! [String:Any])
            print("redirectionFromServiceTag")
            if objAppShareData.onProfile  == false {
            let dictFinal = objNotify.userInfo as! [String:Any]
            if  dictFinal["tabType"] as? String == "service"{
                let sb: UIStoryboard = UIStoryboard(name: "ArtistProfile", bundle: Bundle.main)
                if let objVC = sb.instantiateViewController(withIdentifier:"ServiceDetailVC") as? ServiceDetailVC{
                    objVC.hidesBottomBarWhenPushed = true
                    objVC.param = dictFinal
                    self.navigationController?.pushViewController(objVC, animated: true)
                }
            }
        }
        }
}




//
//  ArtistProfileVC.swift
//  MualabBusiness
//
//  Created by mac on 23/05/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

import UIKit
import Accounts
import AVFoundation
import AVKit
import Social
import Alamofire
import AlamofireImage
import FirebaseAuth
import TLPhotoPicker
import Photos
import HCSStarRatingView
import Firebase
import Kingfisher
import SDWebImage

class ArtistProfileVC: UIViewController,SWRevealViewControllerDelegate,UITableViewDelegate, UITableViewDataSource,AVPlayerViewControllerDelegate,UITextViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UITextFieldDelegate,UIScrollViewDelegate ,UIGestureRecognizerDelegate{
    
    @IBOutlet weak var viewBackAction: UIView!
    
    @IBOutlet weak var indicators: UIActivityIndicatorView!
    // fileprivate let objChatList = ChatHistoryData()
    @IBOutlet weak var topOfFilterView: NSLayoutConstraint!
    var strSearchText = ""
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var heightTableView: NSLayoutConstraint!
    @IBOutlet weak var btnHiddenSearch: UIButton!
    @IBOutlet weak var btnHiddenSearch2: UIButton!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var viewBlackLayer: UIView!
    
    @IBOutlet weak var btnFollow: UIButton!
    @IBOutlet weak var btnBookNow: UIButton!
    @IBOutlet weak var vwChatIcon: UIView!
    @IBOutlet weak var vwProfileMenuIcon: UIView!
    @IBOutlet weak var vwFavoriteIcon: UIView!
    @IBOutlet weak var imgVwFavorite: UIImageView!
    @IBOutlet weak var tblViewMoreFilter: UITableView!
    @IBOutlet weak var viewBottumTable: UIView!
    var objArtistDetails = ArtistDetails(dict: ["":""])
    fileprivate var indexLastViewMoreView = 0
    @IBOutlet weak var lblNoDataFound: UIView!
    @IBOutlet weak var tblFeeds: UITableView!
    
    //Notification count
    @IBOutlet weak var viewNotificationCountMain: UIView!
    @IBOutlet weak var viewNotificationCount: UIView!
    @IBOutlet weak var lblNotificationCount: UILabel!
    
    //profile Header detail Outlate
    @IBOutlet weak var lblArtistDistance: UILabel!
    @IBOutlet weak var lblArtistName: UILabel!
    @IBOutlet weak var lblArtistEmail: UILabel!
    
    //profile image and rating, ratingcount detail Outlate
    @IBOutlet weak var viewRating: HCSStarRatingView!
    
    @IBOutlet weak var viewUploading: UIView!
    
    @IBOutlet weak var imgArtistProfile: UIImageView!
    @IBOutlet weak var imgCertifide: UIImageView!
    
    
    @IBOutlet weak var lblRatingCount: UILabel!
    @IBOutlet weak var lblAboutUs: UILabel!
    
    @IBOutlet weak var imgBackWord: UIImageView!
    @IBOutlet weak var imgForword: UIImageView!
    
    //stack detail Outlate
    @IBOutlet weak var vwPaginationDots: UIView!
    
    //backword
    @IBOutlet weak var vwBackword: UIView!
    @IBOutlet weak var stackBackword: UIStackView!
    @IBOutlet weak var lblServiceCount: UILabel!
    @IBOutlet weak var lblCertificateCount: UILabel!
    
    //forward
    @IBOutlet weak var vwForward: UIView!
    @IBOutlet weak var stackForward: UIStackView!
    @IBOutlet weak var lblFollowersCount: UILabel!
    @IBOutlet weak var lblFollowingCount: UILabel!
    @IBOutlet weak var lblPostCount: UILabel!
    @IBOutlet weak var imgForwardSymbole: UIImageView!
    @IBOutlet weak var imgBackwardSymbole: UIImageView!
    
    //feed image video outlate
    @IBOutlet weak var btnFeeds: UIButton!
    @IBOutlet weak var btnImage: UIButton!
    @IBOutlet weak var btnVideo: UIButton!
    
    ///akash sir code
    @IBOutlet weak var dataScrollView: UIScrollView!
    
    //Deependra Code for new design manage
    @IBOutlet weak var lblFilterType: UILabel!
    @IBOutlet weak var imgSearchIcon: UIImageView!
    @IBOutlet weak var viewTextField: UIView!
    @IBOutlet weak var txtSearchText: UITextField!
    @IBOutlet weak var btnTable: UIButton!
    
    @IBOutlet weak var viewMoreFilterOption: UIView!
    
    @IBOutlet weak var btnGride: UIButton!
    
    @IBOutlet weak var viewBlockSupper: UIView!
    @IBOutlet weak var lblBlockBy: UILabel!
    @IBOutlet weak var viewRatingBlock: HCSStarRatingView!
    @IBOutlet weak var lblRatingCountBlock: UILabel!
    @IBOutlet weak var imgCertifideBlock: UIImageView!
    @IBOutlet weak var lblArtistNameBlock: UILabel!
    @IBOutlet weak var lblArtistEmailBlock: UILabel!
    
    
    var imagePicker = UIImagePickerController()
    fileprivate var userImage:UIImage?
    
    //fileprivate var refreshControl = UIRefreshControl()
    fileprivate var pageNo: Int = 0
    fileprivate var totalCount = 0
    fileprivate let pageSize = 20 // that's up to you, really
    fileprivate let preloadMargin = 5 // or whatever number that makes sense with your page size
    fileprivate var gridType = ""
    fileprivate var fromCamera = false
    fileprivate var lastLoadedPage = 0
    fileprivate var imgData : Data?
    fileprivate var isAdminComission = 0
    fileprivate var strStatus:String? = ""
    
    fileprivate var isNavigate = false
    fileprivate var isOtherSelectedForProfile = false
    fileprivate var selectedOtherIdForProfile: Int = 0
    fileprivate var Image: UIImage?
    fileprivate var strType = ""
    fileprivate var suggesionType = ""
    fileprivate var tblView: UITableView?
    fileprivate var myId:String = ""
    fileprivate var dataToAddCount = 10
    
    //fileprivate var arrUsers = [UserDetailArtistProfile]()
    
    fileprivate var objUserDetailArtistProfile = UserDetailArtistProfile(dict: ["":""])
    
    fileprivate var arrFeedImages = [UIImage]()
    fileprivate var suggesionArray = [suggessions]()
    
    fileprivate var startValue = 0
    fileprivate var limitValue = 10
    fileprivate var nextPages = 0
    fileprivate var int = 0
    fileprivate var tableNo = 0
    fileprivate var stackManage = true
    fileprivate var LoderManage = true
    fileprivate var viewTableType = true
    var ref: DatabaseReference!
    fileprivate var selfVC = false
    var isPresentFromChat = false
    
    //MARK: - refreshControl
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.handleRefresh(_:)), for: UIControl.Event.valueChanged)
        refreshControl.tintColor = appColor
        return refreshControl
    }()
    
    //MARK: - System method
    override func viewDidLoad() {
        super.viewDidLoad()
        if objAppShareData.isFromChatToProfile{
           objAppShareData.isFromChatToProfile = false
           self.isPresentFromChat = true
        }
        if let dicUser = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] {
            myId = dicUser["_id"] as? String ?? ""
        }else{
            let userInfo = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] ?? ["":""]
            myId = userInfo["_id"] as? String ?? ""
        }
        self.viewUploading.isHidden = false
        self.ref = Database.database().reference()
        self.badgeCount()
        self.getBlockStatus()
        self.viewBlockSupper.isHidden = true
        self.tblViewMoreFilter.delegate = self
        self.tblViewMoreFilter.dataSource = self
        self.viewBottumTable.isHidden = true
        fromCamera = false
        objWebserviceManager.StopIndicator()
        self.indicators.stopAnimating()
        let a = self.dataScrollView.layer.frame.height+295//self.topOfFilterView.constant
        self.heightTableView.constant = a
        viewConfigure()
        self.viewBlackLayer.isHidden = true
        LoderManage = true
        self.btnHiddenSearch.isHidden = true
        self.btnHiddenSearch2.isHidden = true
        LoderManage = true
        self.tblFeeds.isScrollEnabled = false
        self.collectionView.isScrollEnabled = false
        objAppShareData.arrFeedsForArtistData.removeAll()
        ////
        if objAppShareData.isOtherSelectedForProfile {
            self.isOtherSelectedForProfile = true
            self.selectedOtherIdForProfile = Int(objAppShareData.selectedOtherIdForProfile) ?? 00
        }
        self.addGesturesToMainView()
        self.addGesturesToView()
        NotificationCenter.default.addObserver(self, selector: #selector(self.hideShowBlurViewForSlideMenu), name: NSNotification.Name(rawValue: "hideShowBlurView"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.redirectionFromServiceTag), name: NSNotification.Name(rawValue: "redirectionFromServiceTag"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.fromBackgroundHideSlideMenu), name: NSNotification.Name(rawValue: "fromBackgroundHideSlideMenu"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.hideUploadLoader), name: NSNotification.Name(rawValue:"hideUploadLoader"), object: nil)
        
        ////
        self.getUserProfileDetailsFromServer()
        //refreshData(page: 0)
        self.addGesturesToViewNew()
        
    }
    func addGesturesToViewNew() -> Void {
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.topToBottomSWIPE))
        swipeRight.direction = .down
        self.viewBottumTable.addGestureRecognizer(swipeRight)
    }
    
    @objc func topToBottomSWIPE() ->Void {
        self.view.endEditing(true)
        self.viewBottumTable.isHidden = true
    }
    
    @objc func hideUploadLoader(){
        self.viewUploading.isHidden = true
        self.dataScrollView.contentOffset.y = 0.0
        self.tblFeeds.contentOffset.y = 0.0
        self.clickOnTable()
    }
    
    @objc func fromBackgroundHideSlideMenu(_ objNotify: Notification) {
        print(objNotify)
        //self.btnManu(UIButton())
        if viewHeightGloble == Int(self.view.frame.height) {
            self.viewBlackLayer.isHidden = true
            self.tabBarController?.tabBar.isHidden = false
            self.tabBarController?.view.frame = CGRect(x:0, y:0, width:self.view.frame.size.width, height:self.view.frame.size.height)
        }else{
            //self.viewBlackLayer.isHidden = false
        }
        
        //        let sb: UIStoryboard = UIStoryboard(name: "ArtistProfile", bundle: Bundle.main)
        //        if let objVC = sb.instantiateViewController(withIdentifier:"SWRevealViewController") as? SWRevealViewController{
        //            objVC.hidesBottomBarWhenPushed = true
        //            navigationController?.pushViewController(objVC, animated: true)
        //        }
        /*
         var storyboard = UIStoryboard(name: "Main", bundle: nil)
         var sidemenuViewController = storyboard.instantiateViewControllerWithIdentifier("sideMenu") as! sw
         sidemenuViewController.delegate = self
         revealViewController().rightViewController = sidemenuViewController
         revealViewController().delegate = self
         sideMenuItem.target = self.revealViewController()
         self.revealViewController().rightViewRevealWidth = self.view.frame.width * 0.8
         sideMenuItem.action = Selector("rightRevealToggle:")
         */
    }
    
    @objc func hideShowBlurViewForSlideMenu(_ objNotify: Notification) {
        print(objNotify)
        self.view.endEditing(true)
        let dict1 = objNotify.userInfo as! [String:Any]
        let position1 = dict1["xPosition"] as! String
        if position1 == "0"{
            self.viewBlackLayer.isHidden = true
        }else if position1 == "1"{
            self.viewBlackLayer.isHidden = false
        }
        self.viewBottumTable.isHidden = true
        
        print(objNotify)
        self.view.endEditing(true)
        let dict = objNotify.userInfo as! [String:Any]
        let position = dict["xPosition"] as! String
        if position == "0"{
            
            self.viewBlackLayer.isHidden = true
            if viewHeightGloble == Int(self.view.frame.height) {
                self.tabBarController?.tabBar.isHidden = false
                self.tabBarController?.view.frame = CGRect(x:0, y:0, width:self.view.frame.size.width, height:self.view.frame.size.height)
            }
            
        }else if position == "1"{
            
            self.viewBlackLayer.isHidden = false
            if viewHeightGloble != Int(self.view.frame.height) {
                self.tabBarController?.tabBar.isHidden = true
                self.tabBarController?.view.frame = CGRect(x:0, y:0, width:self.view.frame.size.width, height:self.view.frame.size.height+(self.tabBarController?.tabBar.frame.size.height)!*2)
            }
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if fromCamera{
            fromCamera = false
            return
        }
        
//        ////
//        if objAppShareData.isFromNotification{
//            if objAppShareData.notificationType == "chat" {
//                let sb: UIStoryboard = UIStoryboard(name: "Chat", bundle: Bundle.main)
//                if let objVC = sb.instantiateViewController(withIdentifier:"ChatHistoryVC") as? ChatHistoryVC{
//                    objVC.hidesBottomBarWhenPushed = true
//                    navigationController?.pushViewController(objVC, animated: false)
//                }
//            }
//        }
//        ////
//        
        selfVC = true
        self.addGesturesToView()
        objAppShareData.onProfile = true
        self.indicators.stopAnimating()
        self.btnBookNow.isHidden = true
        self.btnFollow.isHidden = true
        self.vwFavoriteIcon.isHidden = true
        self.vwForward.isHidden = true
        self.vwBackword.isHidden = true
        LoderManage = true
        self.viewBlackLayer.isHidden = true
        if DoneEditProfile == true{
            objAppShareData.showAlert(withMessage: "Profile updated successfully.", type: alertType.banner, on: self)
        }
        
        //objAppShareData.arrFeedsForArtistData.removeAll()
        if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.businessInfoDic){
            if let imgUrl = dict["profileImage"] as? String {
                if imgUrl != "" {
                    if let url = URL(string:imgUrl){
                        if !objAppShareData.isOtherSelectedForProfile{
                            //self.imgArtistProfile.af_setImage(withURL: url, placeholderImage: #imageLiteral(resourceName: "cellBackground"))
                            self.imgArtistProfile.sd_setImage(with: url, completed: nil)
                        }
                    }
                }
            }
        }
        
        if revealViewController() != nil {
            revealViewController().setFrontViewPosition(FrontViewPosition.left, animated: true)
        }
        
        if let dicUser = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] {
            myId = dicUser["_id"] as? String ?? ""
        }else{
            let userInfo = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] ?? ["":""]
            myId = userInfo["_id"] as? String ?? ""
        }
        objAppShareData.selectedOtherIdForProfile = String(self.selectedOtherIdForProfile)
        
        self.searchTextDataFromTextfield()
        
        if DoneEditProfile == true{
            DoneEditProfile = false
            dismiss(animated: false, completion: nil)
        }
        
        self.getUserProfileDetailsFromServer()
        //self.getUserProfileDetailsFromServer()
        //refreshData(page: 0)
        self.showBackwordStackOption()
        
        if objAppShareData.isFileIsUploding{
            self.viewUploading.isHidden = false
            self.dataScrollView.contentOffset.y = 0.0
            self.tblFeeds.contentOffset.y = 0.0
        }else{
            self.viewUploading.isHidden = true
        }
        
        let aa = Int(self.view.frame.height)
        print("a = ",aa)
        print("viewHeightGloble = ",viewHeightGloble)
        
        if  viewHeightGloble <= aa {
            self.viewBackAction.isHidden = false
            self.viewNotificationCountMain.isHidden = true
        }else{
            self.viewBackAction.isHidden = true
            self.viewNotificationCountMain.isHidden = false
        }
        if self.tabBarController?.tabBar.isHidden == true{
            print("hidden")
        }else{
            print("not hidden")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.indicators.stopAnimating()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print(" self.viewHeight  height ",viewHeightGloble)
        print("self.view.frame height ",self.tabBarController?.view.frame.height)
        
        if  viewHeightGloble < Int(self.tabBarController?.view.frame.height ?? CGFloat(viewHeightGloble)) || self.tabBarController?.tabBar.isHidden == true{
            self.tabBarController?.tabBar.isHidden = false
            let b = Int(self.view.frame.width)
            let a = CGRect(x:0,y:0,width:b,height:viewHeightGloble)
            self.tabBarController?.view.frame = a//CGRect(x:0, y:0, width:self.view.frame.size.width, height:viewHeightGloble)
        }
        
        let aa = Int(self.view.frame.height)
        print("a = ",aa)
        
        if  viewHeightGloble == aa {
            self.viewBackAction.isHidden = false
            self.viewNotificationCountMain.isHidden = true
            self.tabBarController?.tabBar.isHidden = true
        }else{
            self.viewBackAction.isHidden = true
            self.viewNotificationCountMain.isHidden = false
            self.tabBarController?.tabBar.isHidden = false
        }
        
        if isOtherSelectedForProfile == true{
            self.viewBackAction.isHidden = false
            self.tabBarController?.tabBar.isHidden = true
        }else{
            //            if revealViewController() != nil {
            //                revealViewController().rightViewRevealWidth = 265 //self.view.frame.size.width * 0.7
            //                menuButton.addTarget(revealViewController, action: #selector(SWRevealViewController.rightRevealToggle(_:)), for: .touchUpInside)
            //                self.revealViewController().delegate = self
            //                view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            //            }
            self.vwProfileMenuIcon.isHidden = false
        }
    }
    
    
    func hideRevealViewController(){
        if revealViewController() != nil {
            revealViewController().setFrontViewPosition(FrontViewPosition.left, animated: true)
        }
    }
    
}

//MARK: - Up down Swipe
fileprivate extension ArtistProfileVC{
    func addGesturesToView() -> Void {
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.leftToRightSaleSwipe))
        swipeRight.direction = .right
        self.stackForward.addGestureRecognizer(swipeRight)
        
        let swipeRightstackBackword = UISwipeGestureRecognizer(target: self, action: #selector(self.leftToRightSaleSwipe))
        swipeRightstackBackword.direction = .right
        self.stackBackword.addGestureRecognizer(swipeRightstackBackword)
        
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.rightToLeftSaleSwipe))
        swipeLeft.direction = .left
        self.stackForward.addGestureRecognizer(swipeLeft)
        
        
        let swipeLeftstackForward = UISwipeGestureRecognizer(target: self, action: #selector(self.rightToLeftSaleSwipe))
        swipeLeftstackForward.direction = .left
        self.stackBackword.addGestureRecognizer(swipeLeftstackForward)
        
    }
    
    @objc func leftToRightSaleSwipe() ->Void {
        if objAppShareData.isOtherSelectedForProfile {
            if objAppShareData.selectedOtherTypeForProfile == "artist"{
                if stackManage == false{
                    self.showBackwordStackOption()
                } else {
                    self.showForwordStackOption()
                }
            }
        }else{
            if let dicUser = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] {
                let userType = dicUser[UserDefaults.keys.userType] as? String ?? ""
                if userType == "artist"{
                    if stackManage == false{
                        self.showBackwordStackOption()
                    } else {
                        self.showForwordStackOption()
                    }
                }
            }
        }
    }
    
    @objc func rightToLeftSaleSwipe() ->Void {
        if objAppShareData.isOtherSelectedForProfile {
            if objAppShareData.selectedOtherTypeForProfile == "artist"{
                if stackManage == false{
                    self.showBackwordStackOption()
                } else {
                    self.showForwordStackOption()
                }
            }
        }else{
            if let dicUser = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] {
                let userType = dicUser[UserDefaults.keys.userType] as? String ?? ""
                if userType == "artist"{
                    if stackManage == false{
                        self.showBackwordStackOption()
                    } else {
                        self.showForwordStackOption()
                    }
                }
            }
        }
    }
}
//MARK: - custome method
extension ArtistProfileVC{
    
    func updateUIAccordingToUserType() {
        
        if objAppShareData.isOtherSelectedForProfile {
            
            if objAppShareData.selectedOtherTypeForProfile == "artist"{
                
                self.lblAboutUs.text = "About us"
                self.stackBackword.isHidden = false
                self.stackForward.isHidden = true
                //                self.vwForward.isHidden = false
                //                self.vwBackword.isHidden = false
                self.vwPaginationDots.isHidden = false
                self.lblArtistDistance.isHidden = false
                //                self.btnFollow.isHidden = false
                //                self.btnBookNow.isHidden = false
                self.btnBookNow.setTitle("Book Now", for: .normal)
                self.vwChatIcon.isHidden = false
                self.vwProfileMenuIcon.isHidden = true
                //self.vwFavoriteIcon.isHidden = false
                
            }else{
                
                self.lblAboutUs.text = "About us"
                self.stackBackword.isHidden = true
                self.stackForward.isHidden = false
                self.vwForward.isHidden = true
                self.vwBackword.isHidden = true
                self.vwPaginationDots.isHidden = true
                self.lblArtistDistance.isHidden = true
                //self.btnFollow.isHidden = false
                //self.btnBookNow.isHidden = true
                self.btnBookNow.isHidden = false
                self.btnBookNow.setTitle("Message", for: .normal)
                //self.vwChatIcon.isHidden = false
                self.vwChatIcon.isHidden = true
                // self.vwFavoriteIcon.isHidden = true
                self.vwProfileMenuIcon.isHidden = true
            }
            
        }else{
            
            if revealViewController() != nil {
                revealViewController().rightViewRevealWidth = 265 //self.view.frame.size.width * 0.7
                menuButton.addTarget(revealViewController, action: #selector(SWRevealViewController.rightRevealToggle(_:)), for: .touchUpInside)
                
                self.revealViewController().delegate = self
                view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            }
            
            self.lblAboutUs.text = "About us"
            self.stackBackword.isHidden = true
            self.stackForward.isHidden = false
            self.vwForward.isHidden = true
            self.vwBackword.isHidden = true
            self.vwPaginationDots.isHidden = true
            self.lblArtistDistance.isHidden = true
            self.btnFollow.isHidden = true
            self.btnBookNow.isHidden = true
            self.vwChatIcon.isHidden = true
            self.vwFavoriteIcon.isHidden = true
            self.vwProfileMenuIcon.isHidden = false
            
            if let dicUser = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] {
                let userType = dicUser[UserDefaults.keys.userType] as? String ?? ""
                if userType == "artist"{
                    self.vwPaginationDots.isHidden = false
                    //                self.vwForward.isHidden = false
                    //                self.vwBackword.isHidden = false
                    self.stackBackword.isHidden = false
                    self.stackForward.isHidden = true
                }}
            
        }
    }
    
    // UIGestureRecognizerDelegate method
    //    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
    //        if touch.view?.isDescendant(of: self.stackForward) == true {
    //            return false
    //        }
    //        return true
    //    }
    
    func getUserProfileDetailsFromServer() {
        
        if self.isOtherSelectedForProfile {
            
            let dictParam = ["userId":self.selectedOtherIdForProfile,
                             "loginUserId":myId,
                             "viewBy":"user"
                ] as [String : Any]
            
            callWebserviceFor_UserDetailWtih(dictParam: dictParam)
            
        }else{
            
            let dictParam = ["userId":myId,
                             "loginUserId":myId,
                             "viewBy":"user"
                ] as [String : Any]
            callWebserviceFor_UserDetailWtih(dictParam: dictParam)
        }
    }
    
    func viewConfigure(){
        self.txtSearchText.delegate = self
        self.viewTextField.isHidden = true
        self.viewMoreFilterOption.isHidden = true
        self.collectionView.isHidden = true
        self.tblFeeds.isHidden = false
        self.viewTableType = true
        self.btnGride.setImage(#imageLiteral(resourceName: "inactive_listing_ico"), for: .normal)
        self.btnTable.setImage(#imageLiteral(resourceName: "active_list_ico"), for: .normal)
        gridType = ""
        self.tblFeeds.delegate = self
        self.tblFeeds.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
        self.lblNoDataFound.isHidden = true
        
        stackManage = true
        tableNo = 0
        btnFeeds.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        btnImage.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        btnVideo.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        self.stackForward.isHidden = true
        self.stackBackword.isHidden = false
        //        self.imgBackWord.backgroundColor = appColor
        //        self.imgForword.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        //self.imgBackWord.image = #imageLiteral(resourceName: "active_dot_ico")
        //self.imgForword.image = #imageLiteral(resourceName: "inactive_check_box_icon")
        self.imgBackWord.image = #imageLiteral(resourceName: "active_point_img")
        self.imgForword.image = #imageLiteral(resourceName: "inactive_point_img")
        self.imgForwardSymbole.image = #imageLiteral(resourceName: "Forword_ico")
        self.imgBackwardSymbole.image = #imageLiteral(resourceName: "backWordGray_ico")
        
        //Get my Id
        if let dicUser = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] {
            myId = dicUser["_id"] as? String ?? ""
        }else{
            let userInfo = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] ?? ["":""]
            myId = userInfo["_id"] as? String ?? ""
        }
        
        self.updateUIAccordingToUserType()
        
        self.tblFeeds.addSubview(self.refreshControl)
        self.collectionView.addSubview(self.refreshControl)
    }
    
    func dataParsing(){
        
        let objDetail : UserDetailArtistProfile = objUserDetailArtistProfile
        
        objArtistDetails.businessName = objDetail.businessName
        objArtistDetails.businessType = objDetail.businessType
        objArtistDetails._id = Int(objDetail._id) ?? 0
        objArtistDetails.profileImage = objDetail.profileImage
        objArtistDetails.ratingCount = Int(objDetail.ratingCount) ?? 0
        objArtistDetails.userName = objDetail.userName
        //        objChatList.strOpponentName = objDetail.userName
        //        objChatList.strOpponentProfileImage = objDetail.profileImage
        objArtistDetails.aboutUs = objDetail.aboutUs
        
        self.lblArtistName.text = objDetail.firstName+" "+objDetail.lastName//objDetail.businessName
        self.lblArtistNameBlock.text = objDetail.firstName+" "+objDetail.lastName//objDetail.businessName
        self.lblArtistEmail.text = "@"+objDetail.userName
        self.lblArtistEmailBlock.text = "@"+objDetail.userName
        self.lblRatingCount.text = "("+objDetail.reviewCount+")"
        self.lblRatingCountBlock.text = "("+objDetail.reviewCount+")"
        
        self.lblServiceCount.text = objDetail.serviceCount
        self.lblCertificateCount.text = objDetail.certificateCount
        self.lblFollowersCount.text = objDetail.followersCount
        self.lblFollowingCount.text = objDetail.followingCount
        self.lblArtistDistance.text = objDetail.radius + " Miles"
        self.lblPostCount.text = objDetail.postCount
        self.lblBlockBy.text = objDetail.firstName+" "+objDetail.lastName+" has blocked you "
        if objDetail.followerStatus == "1"{
            self.btnFollow.setTitle("Unfollow", for: .normal)
        }else{
            self.btnFollow.setTitle("Follow", for: .normal)
        }
        
        if objDetail.favoriteStatus == "1"{
            self.imgVwFavorite.image = #imageLiteral(resourceName: "active_ico")//#imageLiteral(resourceName: "active_like_ico")
        }else{
            self.imgVwFavorite.image = #imageLiteral(resourceName: "inactive_star_ico_Blue")//#imageLiteral(resourceName: "inactive_like_ico")
        }
        
        let str =  objDetail.ratingCount
        guard let n = NumberFormatter().number(from: str) else { return }
        self.viewRating.value = CGFloat(truncating: n)
        self.viewRatingBlock.value = CGFloat(truncating: n)
        
        if objDetail.profileImage != "" {
            if let url = URL(string: (objDetail.profileImage)){
                //self.imgArtistProfile.af_setImage(withURL: url, placeholderImage:#imageLiteral(resourceName: "cellBackground"))
                self.imgArtistProfile.sd_setImage(with: url, completed: nil)
            }
        }else{
            self.imgArtistProfile.image = #imageLiteral(resourceName: "cellBackground")
        }
        
        //        if objDetail.certificateCount == "0"{
        //            imgCertifide.image = nil
        //        }
        
        if objDetail.isCertificateVerify == "1" {
            self.imgCertifide.isHidden = false
            self.imgCertifideBlock.isHidden = false
        }else{
            self.imgCertifide.isHidden = true
            self.imgCertifideBlock.isHidden = true
            
        }
    }
    
    func underDevelopment(){
        objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
    }
}

//MARK: - Button action extension
fileprivate extension ArtistProfileVC{
    @IBAction func btnReviewRatingAction(_ sender:Any){
        var userId = ""
        if self.isOtherSelectedForProfile {
            userId = String(self.selectedOtherIdForProfile)
        }else{
            userId = myId
        }
        self.view.endEditing(true)
        let sb = UIStoryboard(name:"Review",bundle:Bundle.main)
        let objChooseType = sb.instantiateViewController(withIdentifier:"ReviewListVC") as! ReviewListVC
        objChooseType.userId = String(userId)
        objChooseType.userType = objAppShareData.selectedOtherTypeForProfile
        if objChooseType.userType.count == 0{
            objChooseType.userType = "user"
        }
        objChooseType.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(objChooseType, animated: true)
    }
    @IBAction func btnHiddenBlackLayerAction(_ sender:Any){
        self.viewBlackLayer.isHidden = true
    }
    @IBAction func btnProfileImageZoom(_ sender:Any){
        if objAppShareData.isOtherSelectedForProfile{
            
            let sb = UIStoryboard(name: "Add", bundle: Bundle.main)
            if let objShowImage = sb.instantiateViewController(withIdentifier:"showImagesVC") as? showImagesVC{
                var arr = [Any]()
                arr.append(self.imgArtistProfile.image)
                objShowImage.isTypeIsUrl = false
                objShowImage.arrFeedImages = arr
                //objShowImage.objFeeds = objFeed
                objShowImage.modalPresentationStyle = .fullScreen
                present(objShowImage, animated: true)
            } }else{
            self.selectProfileImage()
        }
    }
    @IBAction func btnGrideAction(_ sender: UIButton) {
        self.view.endEditing(true)
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        self.strSearchText = ""
        self.txtSearchText.text = ""
        self.LoderManage = true
        
        self.tblFeeds.isScrollEnabled = false
        self.viewMoreFilterOption.isHidden = true
        
        self.pageNo = 0
        objAppShareData.arrFeedsForArtistData.removeAll()
        self.collectionView.reloadData()
        gridType = "grid"
        self.collectionView.contentOffset.y = 0.0
        self.view.layoutIfNeeded()
        objWebserviceManager.StartIndicator()
        LoderManage = true
        self.viewTableType = false
        self.viewMoreFilterOption.isHidden = true
        self.collectionView.isHidden = false
        self.tblFeeds.isHidden = true
        self.searchTextDataFromTextfield()
        self.btnGride.setImage(#imageLiteral(resourceName: "active_listing_ico"), for: .normal)
        self.btnTable.setImage(#imageLiteral(resourceName: "list_ico"), for: .normal)
        if  objAppShareData.arrFeedsForArtistData.count > 0{
            // self.collectionView.scrollToItem(at: IndexPath(item:0, section: 0), at: .top, animated: true)
        }
    }
    
    @IBAction func btnTableAction(_ sender: UIButton) {
        self.view.endEditing(true)
        clickOnTable()
    }
    func clickOnTable(){
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        self.strSearchText = ""
        self.txtSearchText.text = ""
        self.LoderManage = true
        
        self.pageNo = 0
        objAppShareData.arrFeedsForArtistData.removeAll()
        self.tblFeeds.reloadData()
        self.tblFeeds.contentOffset.y = 0.0
        self.view.layoutIfNeeded()
        LoderManage = true
        gridType = ""
        self.collectionView.isScrollEnabled = false
        self.viewMoreFilterOption.isHidden = true
        self.viewTableType = true
        self.viewMoreFilterOption.isHidden = true
        self.collectionView.isHidden = true
        self.tblFeeds.isHidden = false
        self.searchTextDataFromTextfield()
        self.btnGride.setImage(#imageLiteral(resourceName: "inactive_listing_ico"), for: .normal)
        self.btnTable.setImage(#imageLiteral(resourceName: "active_list_ico"), for: .normal)
        if  objAppShareData.arrFeedsForArtistData.count > 0{
            // self.tblFeeds.scrollToRow(at: IndexPath(row:0, section: 0), at: .top, animated: true)
        }
    }
    
    @IBAction func btnManu(_ sender: UIButton) {
        if self.tabBarController?.tabBar.isHidden == true{
            print("hidden")
        }else{
            print("not hidden")
        }
        self.view.endEditing(true)
        self.viewMoreFilterOption.isHidden = true
        self.viewBlackLayer.isHidden = false
        if viewHeightGloble != Int(self.view.frame.height) {
            self.tabBarController?.tabBar.isHidden = true
            self.tabBarController?.view.frame = CGRect(x:0, y:0, width:self.view.frame.size.width, height:self.view.frame.size.height+(self.tabBarController?.tabBar.frame.size.height)!*2)
        }
    }
    
    @IBAction func btnHiddenViewBottumTable(_ sender: UIButton) {
        self.viewBottumTable.isHidden = true
        if objAppShareData.isOtherSelectedForProfile{
        }else{
            if  viewHeightGloble < Int(self.tabBarController?.view.frame.height ?? CGFloat(viewHeightGloble)) || self.tabBarController?.tabBar.isHidden == true{
                self.tabBarController?.tabBar.isHidden = false
                let b = Int(self.view.frame.width)
                let a = CGRect(x:0,y:0,width:b,height:viewHeightGloble)
                self.tabBarController?.view.frame = a//CGRect(x:0, y:0, width:self.view.frame.size.width, height:viewHeightGloble)
            }
        }
    }
    @IBAction func btnMoreFilter(_ sender: UIButton) {
        self.view.endEditing(true)
        if objAppShareData.isOtherSelectedForProfile{
            
        }else{
            self.tabBarController?.tabBar.isHidden = true
            self.tabBarController?.view.frame = CGRect(x:0, y:0, width:self.view.frame.size.width, height:self.view.frame.size.height+(self.tabBarController?.tabBar.frame.size.height)!*2)
        }
        self.viewBottumTable.isHidden = false
        self.tblViewMoreFilter.reloadData()
        //        if viewMoreFilterOption.isHidden == false{
        //            viewMoreFilterOption.isHidden = true
        //        }else{
        //            viewMoreFilterOption.isHidden = false
        //        }
    }
    
    @IBAction func btnFollowAction(_ sender: UIButton) {
        self.viewMoreFilterOption.isHidden = true
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        
        self.view.endEditing(true)
        self.callWebservice_forFollowUnFollow()
        
        
        //objDetail.followerStatus
        
        //        if folloStatus == "1"{
        //            self.btnFollow.setTitle("Unfollow", for: .normal)
        //        }else{
        //            self.btnFollow.setTitle("Follow", for: .normal)
        //        }
    }
    
    @IBAction func btnBookNowAction(_ sender: UIButton) {
        // Button text "Message" ---> Chat screen
        // Button text "Book Now" ---> Booking screen
        
        if self.btnBookNow.titleLabel?.text == "Book Now"{
            objAppShareData.selectedOtherIdForProfile = String(self.selectedOtherIdForProfile)
            let sbNew: UIStoryboard = UIStoryboard(name: "BookingNew", bundle: Bundle.main)
            if let objVC = sbNew.instantiateViewController(withIdentifier:"BookingNewVC") as? BookingNewVC{
                objVC.hidesBottomBarWhenPushed = true
                navigationController?.pushViewController(objVC, animated: true)
            }
        }else{
            objAppShareData.showAlert(withMessage:"under developement",type: alertType.bannerDark, on: self)
        }
        return
            self.viewMoreFilterOption.isHidden = true
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        
        self.view.endEditing(true)
        objAppShareData.arrSelectedService.removeAll()
        //  let sb: UIStoryboard = UIStoryboard(name: "Booking", bundle: Bundle.main)
        //        if let objVC = sb.instantiateViewController(withIdentifier:"BookingIndividualVC") as? BookingIndividualVC{
        //
        //            objArtistDetails._id = objAppShareData.selectedOtherIdForProfile
        //
        //            objVC.objArtistDetails = objArtistDetails
        //            objVC.isFromAddMoreService = false
        //            if objAppShareData.isSearchingUsingFilter{
        //                objVC.objArtistDetails.isOutCallSelected =  objAppShareData.objRefineData.isOutCallSelected
        //            }
        //            objVC.hidesBottomBarWhenPushed = true
        //            navigationController?.pushViewController(objVC, animated: true)
        //        }
    }
    
    @IBAction func btnFavoriteAction(_ sender: UIButton) {
        self.viewMoreFilterOption.isHidden = true
        self.view.endEditing(true)
        callWebservice_forFavoriteUnFavoriteArtist()
    }
    
    @IBAction func btnChatAction(_ sender: UIButton) {
        objAppShareData.showAlert(withMessage:"under developement",type: alertType.bannerDark, on: self)
        //        return
        //
        //        self.viewMoreFilterOption.isHidden = true
        //        guard objAppShareData.isOtherSelectedForProfile else {
        //            return
        //        }
        
        /*
         objChatList.strOpponentId = String(objAppShareData.selectedOtherIdForProfile)
         objChatList.strOpponentName = objArtistDetails.userName
         objChatList.strOpponentProfileImage = objArtistDetails.profileImage
         objChatShareData.fromChatVC = true
         
         let sb = UIStoryboard(name: "Chat", bundle: nil)
         if let detailVC = sb.instantiateViewController(withIdentifier: "ChatHistoryVC") as? ChatHistoryVC {
         //     detailVC.objChatHistoryModel = objChatList
         self.present(detailVC, animated: true, completion: nil)
         }
         */
    }
    
    @IBAction func btnForwardAction(_ sender: UIButton) {
        self.showForwordStackOption()
    }
    
    func showForwordStackOption(){
        self.viewMoreFilterOption.isHidden = true
        
        if stackManage == true{
            self.imgForwardSymbole.image = #imageLiteral(resourceName: "forWordGray_ico")
            self.imgBackwardSymbole.image = #imageLiteral(resourceName: "backWord_ico")
            self.imgBackWord.image = #imageLiteral(resourceName: "inactive_point_img")
            self.imgForword.image = #imageLiteral(resourceName: "active_point_img")
            UIView.animate(withDuration: 0.4, animations: {
                self.stackBackword.isHidden = true
                self.stackForward.isHidden = false
            })
            stackManage = false
        }else{
        }
        
    }
    
    @IBAction func btnBackwardAction(_ sender: UIButton) {
        self.showBackwordStackOption()
    }
    
    func showBackwordStackOption(){
        self.viewMoreFilterOption.isHidden = true
        
        if stackManage == false{
            self.imgForwardSymbole.image = #imageLiteral(resourceName: "Forword_ico")
            self.imgBackwardSymbole.image = #imageLiteral(resourceName: "backWordGray_ico")
            self.imgBackWord.image = #imageLiteral(resourceName: "active_point_img")
            self.imgForword.image = #imageLiteral(resourceName: "inactive_point_img")
            UIView.animate(withDuration: 0.4, animations: {
                self.stackForward.isHidden = true
                self.stackBackword.isHidden = false
            })
            stackManage = true
        }
    }
    
    @IBAction func btnBackAction(_ sender: Any){
        objAppShareData.clearNotificationData()
        let dictnnn : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.businessInfoDic) ?? ["":""]
        objAppShareData.arrFeedsForArtistData.removeAll()
        if self.isPresentFromChat{
            self.isPresentFromChat = false
            self.dismiss(animated: false, completion: nil)
        }else{
           navigationController?.popViewController(animated: true)
        }
    }
    
    //stack backWard button action
    @IBAction func btnServiceAction(_ sender: UIButton) {
        self.viewMoreFilterOption.isHidden = true
        if self.lblServiceCount.text == "0"{
            objAppShareData.showAlert(withMessage: "No service added", type: alertType.bannerDark, on: self)
            return
        }
        //objAppShareData.selectedOtherIdForProfile = self.selectedOtherIdForProfile
        if  let objComments = storyboard?.instantiateViewController(withIdentifier:"ArtistServicesVC") as? ArtistServicesVC{
            objComments.staffId = objAppShareData.selectedOtherIdForProfile
            objComments.hidesBottomBarWhenPushed = true
            
            navigationController?.pushViewController(objComments ?? UIViewController(), animated: true)
        }
    }
    
    @IBAction func btnAboutAction(_ sender: UIButton) {
        self.viewMoreFilterOption.isHidden = true
        let dictnnn = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] ?? ["":""]
        let myId = dictnnn[UserDefaults.keys.userId] as? String ?? ""
        if objAppShareData.selectedOtherIdForProfile != myId && objAppShareData.selectedOtherIdForProfile != "0"{
            if objArtistDetails.aboutUs.count==0{
                objAppShareData.showAlert(withMessage: "No about us added", type: alertType.bannerDark, on: self)
                return
            }
        }
        
        let sb: UIStoryboard = UIStoryboard(name: "ArtistProfile", bundle: Bundle.main)
        if  let objComments = sb.instantiateViewController(withIdentifier:"AboutArtistVC") as? AboutArtistVC{
            objComments.strAboutUs = objArtistDetails.aboutUs
            objComments.staffId = String(self.selectedOtherIdForProfile)
            objComments.hidesBottomBarWhenPushed = true
            
            navigationController?.pushViewController(objComments, animated: true)
        }
    }
    
    @IBAction func btnCertificateAction(_ sender: UIButton) {
        self.viewMoreFilterOption.isHidden = true
        if self.lblCertificateCount.text == "0"{
            objAppShareData.showAlert(withMessage: "No qualification added", type: alertType.bannerDark, on: self)
            return
        }
        let sb: UIStoryboard = UIStoryboard(name: "ArtistProfile", bundle: Bundle.main)
        if  let objComments = sb.instantiateViewController(withIdentifier:"ArtistCertificateVC") as? ArtistCertificateVC{
            objComments.artistId = String(self.selectedOtherIdForProfile)
            objComments.hidesBottomBarWhenPushed = true
            
            navigationController?.pushViewController(objComments , animated: true)
        }
    }
    
    //stack ForWard button action
    @IBAction func btnFollowersAction(_ sender: UIButton) {
        self.viewMoreFilterOption.isHidden = true
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        let sb: UIStoryboard = UIStoryboard(name: "Feed", bundle: Bundle.main)
        let objComments = sb.instantiateViewController(withIdentifier:"FollowerVC") as? FollowerVC
        objComments?.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(objComments ?? UIViewController(), animated: true)
    }
    
    @IBAction func btnFollowingAction(_ sender: UIButton) {
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        self.viewMoreFilterOption.isHidden = true
        let sb: UIStoryboard = UIStoryboard(name: "Feed", bundle: Bundle.main)
        let objComments = sb.instantiateViewController(withIdentifier:"FollowingVC") as? FollowingVC
        objComments?.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(objComments ?? UIViewController(), animated: true)
    }
    
    @IBAction func btnPostAction(_ sender: UIButton) {
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        objAppShareData.arrFeedsForArtistData.removeAll()
        self.viewMoreFilterOption.isHidden = true
        
        objWebserviceManager.StartIndicator()
        
        if self.lblFilterType.text == "All"{
            strType = ""
            loadFeedsWithPage(page:0, refresh: true)
            
        }else if self.lblFilterType.text == "Photo"{
            strType = "image"
            loadImagesWithPage(page:0, refresh: true)
            
        }else if self.lblFilterType.text == "Video"{
            strType = "video"
            loadVideosWithPage(page:0, refresh: true)
            
        }else{
            strType = ""
        }
        
        tableNo = 0
        //strType = ""
        btnFeeds.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        btnImage.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        btnVideo.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        //  loadFeedsWithPage(page:0, refresh: true)
    }
    
    @IBAction func btnSearchAction(_ sender: UIButton) {
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        self.viewMoreFilterOption.isHidden = true
        
        self.viewTextField.isHidden = !self.viewTextField.isHidden
        if self.strSearchText != "" || self.txtSearchText.text != ""{
            self.txtSearchText.text = ""
            self.strSearchText = ""
        }
        self.btnHiddenSearch.isHidden = self.viewTextField.isHidden
        self.btnHiddenSearch2.isHidden = self.viewTextField.isHidden
    }
    
    @IBAction func btnHiddenSearchFilter(_ sender: UIButton) {
        if self.txtSearchText.text == ""  || self.strSearchText == ""{
            self.viewTextField.isHidden = true
        }
        self.btnHiddenSearch.isHidden = true
        self.btnHiddenSearch2.isHidden = true
        
        let indexPathOld = IndexPath(row:indexLastViewMoreView, section: 0)
        if let cellOld = tblFeeds.cellForRow(at: indexPathOld) as? feedsTableCell{
            cellOld.btnSaveToFolder.isHidden = true
            cellOld.btnReportThisPost.isHidden = true
            cellOld.viewMore.isHidden = true
            cellOld.setDefaultDesign()
            ////
            if cellOld.tagsHidden == false{
                //cellOld.setTagsHidden(!cellOld.tagsHidden)
            }
            ////
        }
    }
    
    //feed,video,image Button action
    @IBAction func btnFeedsAction(_ sender: UIButton) {
        self.feedaction()
    }
    func feedaction(){
        self.view.endEditing(true)
        self.viewMoreFilterOption.isHidden = true
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        self.strSearchText = ""
        self.txtSearchText.text = ""
        self.LoderManage = true
        self.pageNo = 0
        
        self.collectionView.contentOffset.y = 0.0
        self.tblFeeds.contentOffset.y = 0.0
        self.view.layoutIfNeeded()
        self.lblFilterType.text = "All"
        LoderManage = true
        objAppShareData.arrFeedsForArtistData.removeAll()
        self.collectionView.reloadData()
        self.tblFeeds.reloadData()
        tableNo = 0
        strType = ""
        btnFeeds.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        btnImage.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        btnVideo.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        loadFeedsWithPage(page:0, refresh: true)
        
    }
    
    @IBAction func btnImageAction(_ sender: UIButton) {
        self.imageAction()
    }
    func imageAction(){
        self.viewMoreFilterOption.isHidden = true
        self.view.endEditing(true)
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        
        
        self.strSearchText = ""
        self.txtSearchText.text = ""
        self.LoderManage = true
        
        self.pageNo = 0
        
        self.collectionView.contentOffset.y = 0.0
        self.tblFeeds.contentOffset.y = 0.0
        self.view.layoutIfNeeded()
        objAppShareData.arrFeedsForArtistData.removeAll()
        self.collectionView.reloadData()
        self.tblFeeds.reloadData()
        self.lblFilterType.text = "Photo"
        LoderManage = true
        tableNo = 1
        strType = "image"
        btnFeeds.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        btnImage.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        btnVideo.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        
        loadImagesWithPage(page:0, refresh: true)
        
    }
    @IBAction func btnVideoAction(_ sender: UIButton) {
        self.videoAction()
    }
    func videoAction(){
        self.view.endEditing(true)
        
        self.viewMoreFilterOption.isHidden = true
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        self.strSearchText = ""
        self.txtSearchText.text = ""
        self.LoderManage = true
        self.pageNo = 0
        
        self.collectionView.contentOffset.y = 0.0
        self.tblFeeds.contentOffset.y = 0.0
        self.view.layoutIfNeeded()
        self.lblFilterType.text = "Video"
        LoderManage = true
        objAppShareData.arrFeedsForArtistData.removeAll()
        self.collectionView.reloadData()
        self.tblFeeds.reloadData()
        tableNo = 2
        strType = "video"
        btnFeeds.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        btnImage.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        btnVideo.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        loadVideosWithPage(page:0, refresh: true)
    }
    
}

//MARK: - api calling
extension ArtistProfileVC{
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        LoderManage = false
        self.refreshData(page: 0)
    }
    
    func refreshData(page:Int){
        
        self.lastLoadedPage = page
        self.pageNo = page
        
        if tableNo == 0   {
            loadFeedsWithPage(page:self.pageNo, refresh: true)
        }else if tableNo == 1 {
            loadImagesWithPage(page:self.pageNo, refresh: true)
        }else{
            loadVideosWithPage(page:self.pageNo, refresh: true)
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        self.viewMoreFilterOption.isHidden = true
        let text = textField.text! as NSString
        
        if (text.length == 1)  && (string == "") {
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
            strSearchText = ""
            self.btnHiddenSearch.isHidden = false
            self.btnHiddenSearch2.isHidden = false
            self.searchTextDataFromTextfield()
            
        }else{
            var substring: String = textField.text!
            substring = (substring as NSString).replacingCharacters(in: range, with: string)
            substring = substring.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            strSearchText = substring
            // self.searchTextDataFromTextfield()
            self.searchAutocompleteEntries(withSubstring: substring)
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.txtSearchText.resignFirstResponder()
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
        self.searchTextDataFromTextfield()
        return true
    }
    
    // MARK: - searching operation
    func searchAutocompleteEntries(withSubstring substring: String) {
        if substring != "" {
            // to limit network activity, reload half a second after last key press.
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
            self.perform(#selector(self.reload), with: nil, afterDelay: 0.5)
        }
    }
    
    @objc func reload() {
        self.searchTextDataFromTextfield()
    }
    
    func searchTextDataFromTextfield(){
        objAppShareData.isOtherSelectedForProfile = self.isOtherSelectedForProfile
        if self.lblFilterType.text == "All"{
            strType = ""
            loadFeedsWithPage(page:0, refresh: true)
        }else if self.lblFilterType.text == "Photo"{
            strType = "image"
            loadImagesWithPage(page:0, refresh: true)
            
        }else if self.lblFilterType.text == "Video"{
            strType = "video"
            loadVideosWithPage(page:0, refresh: true)
        }
        
    }
    
    func loadImagesWithPage(page: Int, refresh:Bool) {
        
        var dicParam : [String : Any] = ["": ""]
        if objAppShareData.isOtherSelectedForProfile {
            self.pageNo = page
            
            dicParam = ["feedType": "image",
                        "search": self.strSearchText,
                        "page": pageNo,
                        "grid":self.gridType,
                        "limit": self.pageSize,
                        "userId":objAppShareData.selectedOtherIdForProfile,
                        "loginUserId": myId,
                        ] as [String : Any]
            
        }else{
            
            dicParam = ["feedType": "image",
                        "search": self.strSearchText,
                        "grid":self.gridType,
                        "page": pageNo,
                        "limit": self.pageSize,
                        "userId":myId,
                        "loginUserId": myId,
                        ] as [String : Any]
        }
        
        if !refresh {
            callWebserviceFor_getFeeds(dicParam: dicParam, activity: true)
        }else{
            callWebserviceFor_getFeeds(dicParam: dicParam, activity: false)
        }
    }
    
    func loadVideosWithPage(page: Int, refresh:Bool) {
        
        self.pageNo = page
        self.strType = "video"
        
        
        var dicParam : [String : Any] = ["": ""]
        if objAppShareData.isOtherSelectedForProfile {
            
            
            dicParam = ["feedType": "video",
                        "search": self.strSearchText,
                        "grid":self.gridType,
                        "page": self.pageNo,
                        "limit": self.pageSize,
                        "userId":objAppShareData.selectedOtherIdForProfile,
                        "loginUserId": myId,
                        ] as [String : Any]
        }else{
            dicParam = ["feedType": "video",
                        "search": self.strSearchText,
                        "grid":self.gridType,
                        "page": pageNo,
                        "limit": self.pageSize,
                        "userId":myId,
                        "loginUserId": myId,
                        ] as [String : Any]
            
        }
        
        if !refresh {
            callWebserviceFor_getFeeds(dicParam: dicParam, activity: true)
        }else{
            callWebserviceFor_getFeeds(dicParam: dicParam, activity: false)
        }
    }
    
    func loadFeedsWithPage(page: Int, refresh:Bool) {
        
        pageNo = page
        lastLoadedPage = page
        
        var dicParam : [String : Any] = ["": ""]
        if objAppShareData.isOtherSelectedForProfile == true {
            
            dicParam = ["feedType": "",
                        "search": self.strSearchText,
                        "page": pageNo,
                        "grid":self.gridType,
                        "limit": self.pageSize,
                        "userId":objAppShareData.selectedOtherIdForProfile,
                        "loginUserId": myId,
                        ] as [String : Any]
            
        }else{
            
            dicParam = ["feedType": "",
                        "search": self.strSearchText,
                        "grid":self.gridType,
                        "page": pageNo,
                        "limit": self.pageSize,
                        "userId":myId,
                        "loginUserId": myId,
                        ] as [String : Any]
            
        }
        
        print("himanshu api call times")
        if !refresh {
            callWebserviceFor_getFeeds(dicParam: dicParam, activity: true)
        }else{
            callWebserviceFor_getFeeds(dicParam: dicParam, activity: false)
        }
    }
    
    func callWebserviceFor_getFeeds(dicParam: [AnyHashable: Any] , activity:Bool){
        if !objServiceManager.isNetworkAvailable(){
            objWebserviceManager.StopIndicator()
            self.indicators.stopAnimating()
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        
        if strSearchText != ""{
            self.indicators.startAnimating()
        }else if LoderManage{
            objWebserviceManager.StartIndicator()
        }
        self.LoderManage = false
        
        var parameters = [String:Any]()
        parameters = dicParam as! [String : Any]
        print("parameters",parameters)
        objWebserviceManager.requestPostForJson(strURL: WebURL.profileFeed, params: parameters , success: { response in
            
            print(response)
            self.refreshControl.endRefreshing()
            
            if self.pageNo == 0 {
                objAppShareData.arrFeedsForArtistData.removeAll()
            }
            
            let strSucessStatus = response["status"] as? String
            
            self.strStatus = response["message"] as? String
            if strSucessStatus == k_success{
                
                self.totalCount = response["total"] as? Int ?? 0
                if let arrDict = response["AllFeeds"] as? [[String:Any]] {
                    self.dataToAddCount = arrDict.count
                    if arrDict.count > 0 {
                        for dict in arrDict{
                            let obj = feeds.init(dict: dict)
                            if self.viewTableType == true{
                                if self.strType == "image"{
                                    if obj?.feedType == "image"{
                                        objAppShareData.arrFeedsForArtistData.append(obj!)
                                    }
                                }else if self.strType == "video"{
                                    if obj?.feedType == "video"{
                                        objAppShareData.arrFeedsForArtistData.append(obj!)
                                    }
                                }else{
                                    objAppShareData.arrFeedsForArtistData.append(obj!)
                                }
                            }else{
                                if obj?.feedType != "text"{
                                    if self.strType == "image"{
                                        if obj?.feedType == "image"{
                                            objAppShareData.arrFeedsForArtistData.append(obj!)
                                        }
                                    }else if self.strType == "video"{
                                        if obj?.feedType == "video"{
                                            objAppShareData.arrFeedsForArtistData.append(obj!)
                                        }
                                    }else{
                                        objAppShareData.arrFeedsForArtistData.append(obj!)
                                    }
                                }
                            }
                        }
                    }
                }
                if self.viewTableType == true{
                    // self.tblFeeds.reloadData()
                    if objAppShareData.fromLikeComment == false{
                        if  objAppShareData.arrFeedsForArtistData.count > 0{
                            //self.tblFeeds.scrollToRow(at: IndexPath(item:0, section: 0), at: .top, animated: true)
                        }}
                    //DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(3000)) {
                    //objWebserviceManager.StopIndicator()
                    //}
                }else{
                    self.collectionView.reloadData()
                    if objAppShareData.fromLikeComment == false{
                    }
                    //DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(3000)) {
                    //objWebserviceManager.StopIndicator()
                    //}
                }
                
                objAppShareData.fromLikeComment = false
                
                
                
            }else{
                self.indicators.stopAnimating()
                
                if self.viewTableType == true{
                    // self.tblFeeds.reloadData()
                    if  objAppShareData.arrFeedsForArtistData.count > 0{
                        //self.tblFeeds.scrollToRow(at: IndexPath(item:0, section: 0), at: .top, animated: true)
                    }
                }else{
                    //self.collectionView.reloadData()
                    if  objAppShareData.arrFeedsForArtistData.count > 0{
                        //self.collectionView.scrollToItem(at: IndexPath(item:0, section: 0), at: .top, animated: true)
                    }
                }
                objWebserviceManager.StopIndicator()
                
                if strSucessStatus == "fail"{
                    self.indicators.stopAnimating()
                    
                    if objAppShareData.arrFeedsForArtistData.count==0{
                        self.lblNoDataFound.isHidden = false
                        
                    }
                }else{
                    self.indicators.stopAnimating()
                    
                    if let msg = response["message"] as? String{
                        objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                    }
                    
                }
                
            }
            self.updateUI()
            self.indicators.stopAnimating()
            
        }) { (error) in
            objWebserviceManager.StopIndicator()
            self.indicators.stopAnimating()
            
            self.refreshControl.endRefreshing()
            self.updateUI()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        } }
    
    //call webservice for userDetail
    func callWebserviceFor_UserDetailWtih(dictParam : [String  : Any]){
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        
        objWebserviceManager.requestPostForJson(strURL: WebURL.getProfile, params: dictParam, success: { (response) in
            //objWebserviceManager.StopIndicator()
            
            if response["message"] as? String ?? "" == "ok" || response["message"] as? String ?? "" == k_success{ if let dict = response["userDetail"] as? [[String : Any]]{
                print(dict)
                
                ////
                if !objAppShareData.isOtherSelectedForProfile {
                    var userInfo = [:] as! [String:Any]
                    if let dictnnn : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.businessInfoDic){
                        userInfo = dictnnn
                    }
                    let newDict = dict[0]
                    
                    let isAdminComission = newDict["isAdminComission"] as? Int ?? 5
                    self.isAdminComission = isAdminComission
                    if let isAdminComission = newDict["isAdminComission"] as? String{
                        self.isAdminComission = Int(isAdminComission) ?? 0
                    }
                    userInfo["latitude"] = newDict["latitude"]
                    userInfo["longitude"] = newDict["longitude"]
                    userInfo["address"] = newDict["address"]
                    //userInfo["address"] = newDict["address2"]
                    userInfo["address2"] = newDict["address"]
                    //userInfo["address2"] = newDict["address2"]
                    let data = NSKeyedArchiver.archivedData(withRootObject: userInfo)
                    UserDefaults.standard.set(data, forKey: UserDefaults.keys.userInfo)
                    UserDefaults.standard.synchronize()
                }
                ////
                
                //self.arrUsers.removeAll()
                for dicService in dict{
                    self.objUserDetailArtistProfile = UserDetailArtistProfile.init(dict: dicService)
                }
                self.dataParsing()
                }
                
            }else{
                if let msg = response["message"] as? String{
                    objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                }
                
            }
        }) { (error) in
            objWebserviceManager.StopIndicator()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
    //CALL WEBSERVICE FOR FOLOW UNFOLLOW ACTION
    func callWebservice_forFavoriteUnFavoriteArtist() {
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        objWebserviceManager.StartIndicator()
        
        //Get my Id
        var id = ""
        if let dicUser = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] {
            let myID = dicUser["_id"] as? String ?? ""
            id = myID
        }else{
            let userInfo = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] ?? ["":""]
            let myID = userInfo["_id"] as? String ?? ""
            id = myID
        }
        var strFType = ""
        if objUserDetailArtistProfile.favoriteStatus == "1" {
            strFType = "unfavorite"
        }else{
            strFType = "favorite"
        }
        
        let dicParam = ["artistId":objUserDetailArtistProfile._id,
                        "userId":id,
                        "type":strFType
            ] as [String : Any]
        
        objWebserviceManager.requestPost(strURL:WebURL.addFavorite, params: dicParam , success: { response in
            
            objServiceManager.StopIndicator()
            
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }
            let strSucessStatus = response["status"] as? String ?? ""
            if strSucessStatus == k_success{
                
                if self.objUserDetailArtistProfile.favoriteStatus == "1" {
                    self.objUserDetailArtistProfile.favoriteStatus = "0"
                }else{
                    self.objUserDetailArtistProfile.favoriteStatus = "1"
                }
                
                if self.objUserDetailArtistProfile.favoriteStatus == "1"{
                    self.imgVwFavorite.image = #imageLiteral(resourceName: "active_ico")//#imageLiteral(resourceName: "active_like_ico")
                }else{
                    self.imgVwFavorite.image = #imageLiteral(resourceName: "inactive_star_ico_Blue")//#imageLiteral(resourceName: "inactive_like_ico")
                }
                self.getUserProfileDetailsFromServer()
                
            }else{
                if let msg = response["message"] as? String{
                    objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                }
            }
            
        }) { error in
            objServiceManager.StopIndicator()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
    //CALL WEBSERVICE FOR FOLOW UNFOLLOW ACTION
    func callWebservice_forFollowUnFollow() {
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        objWebserviceManager.StartIndicator()
        //Get my Id
        var id = ""
        if let dicUser = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] {
            let myID = dicUser["_id"] as? String ?? ""
            id = myID
        }else{
            let userInfo = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] ?? ["":""]
            let myID = userInfo["_id"] as? String ?? ""
            id = myID
        }
        
        let dicParam = ["followerId":objUserDetailArtistProfile._id,
                        "userId":id
            ] as [String : Any]
        
        objWebserviceManager.requestPost(strURL:WebURL.followFollowing, params: dicParam , success: { response in
            
            objServiceManager.StopIndicator()
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else{
                let strSucessStatus = response["status"] as? String ?? ""
                if strSucessStatus == k_success{
                    
                    if self.objUserDetailArtistProfile.followerStatus == "1" {
                        self.objUserDetailArtistProfile.followerStatus = "0"
                    }else{
                        self.objUserDetailArtistProfile.followerStatus = "1"
                    }
                    
                    if self.objUserDetailArtistProfile.followerStatus == "1"{
                        self.btnFollow.setTitle("Unfollow", for: .normal)
                    }else{
                        self.btnFollow.setTitle("Follow", for: .normal)
                    }
                    self.getUserProfileDetailsFromServer()
                }else{
                    if let msg = response["message"] as? String{
                        objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                    }
                }
            }
        }) { error in
            objServiceManager.StopIndicator()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
        
    }
    
    func updateUI() -> Void {
        
        if objAppShareData.arrFeedsForArtistData.count > 0{
            self.lblNoDataFound.isHidden = true
        }else{
            self.lblNoDataFound.isHidden = false
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
            
            if self.viewTableType == true{
                self.tblFeeds.reloadData()
                if  objAppShareData.arrFeedsForArtistData.count > 0{
                    //self.tblFeeds.scrollToRow(at: IndexPath(item:0, section: 0), at: .top, animated: true)
                }
            }else{
                self.collectionView.reloadData()
                if  objAppShareData.arrFeedsForArtistData.count > 0{
                    //self.collectionView.scrollToItem(at: IndexPath(item:0, section: 0), at: .top, animated: true)
                }
            }
            if self.pageNo == 0 {
                self.tblFeeds.contentOffset.y = 0
            }
        }
    }
}
// MARK:- UITableView Delegate and Datasource
extension ArtistProfileVC :UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return objAppShareData.arrFeedsForArtistData.count
    }
    
    @objc func cellAPIMethod(){
        self.indicators.startAnimating()
        self.LoderManage = false
        loadFeedsWithPage(page: self.nextPages , refresh: true)
    }
    func collectionView(_ collectionView: UICollectionView,cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        if objAppShareData.arrFeedsForArtistData.count < self.totalCount {
            
            let nextPage: Int = Int(indexPath.item / pageSize) + 1 //1
            let preloadIndex = nextPage * pageSize - preloadMargin //15
            
            print(" indexPath.item ",indexPath.item)
            print(" preloadIndex ",preloadIndex)
            print(" lastLoadedPage ",lastLoadedPage)
            print(" nextPage ",nextPage)
            print(" pageSize ",pageSize)
            
            print(" naw ",(indexPath.item >= preloadIndex && lastLoadedPage < nextPage))
            
            if (indexPath.item >= preloadIndex && lastLoadedPage < nextPage){
                print(" naw ",(indexPath.item >= preloadIndex && lastLoadedPage < nextPage))
                //LoderManage = true
                if tableNo == 0   {
                    NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.cellAPIMethod), object: nil)
                    self.perform(#selector(self.cellAPIMethod), with: nil, afterDelay: 0.5)
                    nextPages = nextPage
                }else if tableNo == 1 {
                    NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.cellAPIMethod), object: nil)
                    self.perform(#selector(self.cellAPIMethod), with: nil, afterDelay: 0.5)
                    nextPages = nextPage
                }else{
                    NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.cellAPIMethod), object: nil)
                    self.perform(#selector(self.cellAPIMethod), with: nil, afterDelay: 0.5)
                    nextPages = nextPage
                }
            }
        }
        let cell = (collectionView.dequeueReusableCell(withReuseIdentifier: "ExploreCollectionCell", for:
            indexPath) as? ExploreCollectionCell)
        //      cell?.imgVwPost.layer.cornerRadius = 2
        //      cell?.imgVwPost.layer.masksToBounds = true
        cell?.imgVwPlay?.isHidden = true
        cell?.imgVwPost.image = UIImage(named: "gallery_placeholder")
        if let index = indexPath.row as? Int {
            let objFeeds = objAppShareData.arrFeedsForArtistData[indexPath.row]
            
            if objFeeds.feedType == "video"{
                if objFeeds.arrFeed.count > 0 {
                    if objFeeds.arrFeed[0].videoThumb != "" {
                        cell?.imgVwPost.image = nil
                        if let url = URL(string: objFeeds.arrFeed[0].videoThumb){
                            
                            //cell?.imgVwPost.af_setImage(withURL: url, placeholderImage: UIImage(named: "gallery_placeholder"))
                            //cell?.imgVwPost.af_setImage(withURL: url, placeholderImage: UIImage(named: ""))
                            cell?.imgVwPost.sd_setImage(with: url, completed: nil)
                        }
                    }
                    cell?.imgVwPlay?.isHidden = false
                    cell?.imgVwPlay?.tag = indexPath.row
                    cell?.imgVwPlay?.superview?.tag = indexPath.section
                    let tapForPlayVideo = UITapGestureRecognizer(target: self, action: #selector(self.showVideoFromList))
                    tapForPlayVideo.numberOfTapsRequired = 1
                    cell?.imgVwPlay?.addGestureRecognizer(tapForPlayVideo)
                }
            }else if objFeeds.feedType == "image"{
                cell?.imgVwPost.image = UIImage(named: "gallery_placeholder")
                if objFeeds.arrFeed.count > 0 {
                    if objFeeds.arrFeed[0].feedPost != "" {
                        if let url = URL(string: objFeeds.arrFeed[0].feedPost){
                            //cell?.imgVwPost.af_setImage(withURL: url, placeholderImage: UIImage(named: "gallery_placeholder"))
                            //cell?.imgVwPost.af_setImage(withURL: url, placeholderImage: UIImage(named: ""))
                            let processor = DownsamplingImageProcessor(size: cell!.imgVwPost.frame.size)
                                >> RoundCornerImageProcessor(cornerRadius: 0)
                            cell?.imgVwPost.kf.indicatorType = .activity
                            cell?.imgVwPost.kf.setImage(
                                with: url,
                                placeholder: UIImage.init(named:"gallery_placeholder"),
                                options: [
                                    .processor(processor),
                                    .scaleFactor(UIScreen.main.scale),
                                    .transition(.fade(0.7)),
                                    .cacheOriginalImage
                                ])
                            {
                                result in
                                switch result {
                                case .success(let value):
                                    print("Task done for: \(value.source.url?.absoluteString ?? "")")
                                    
                                case .failure(let error):
                                    cell?.imgVwPost.sd_setImage(with: url, placeholderImage: UIImage(named: "gallery_placeholder"))
                                    print("Job failed: \(error.localizedDescription)")
                                }
                            }
                        }
                    }
                }
            }
        }
        return cell!
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view?.isDescendant(of: self.collectionView) == true {
            return false
        }
        //        if touch.view?.isDescendant(of: self.stackForward) == true {
        //            return false
        //        }
        //        if touch.view?.isDescendant(of: self.stackBackword) == true {
        //            return false
        //        }
        return true
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        var cellWidth:CGFloat = 0
        var cellHeight:CGFloat = 20
        cellWidth = CGFloat((self.collectionView.frame.size.width-3)/3)
        cellHeight = cellWidth
        return CGSize(width: cellWidth, height: cellHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        /*
         let objFeeds = objAppShareData.arrFeedsForArtistData[indexPath.row]
         let sb: UIStoryboard = UIStoryboard(name: "Explore", bundle: Bundle.main)
         if let objVC = sb.instantiateViewController(withIdentifier:"ExpPostDetailVC") as? ExpPostDetailVC{
         objVC.objFeeds = objFeeds
         objAppShareData.isExploreGridToAllFeed = true
         objVC.arrFeedsLocal = objAppShareData.arrFeedsForArtistData
         objVC.indexToScroll = indexPath.item
         navigationController?.pushViewController(objVC, animated: true)
         }*/
        
        let objFeed = objAppShareData.arrFeedsForArtistData[indexPath.row]
        //if objFeed.feedType == "image"{
        let sb: UIStoryboard = UIStoryboard(name: "ArtistProfile", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"FeedDetailVC") as? FeedDetailVC  {
            objVC.feedId = String(objFeed._id)
            isNavigate = true
            objAppShareData.isExploreGridToAllFeed = true
            objVC.arrFeedsLocal = objAppShareData.arrFeedsForArtistData
            objVC.indexToScroll = indexPath.row
            objVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(objVC, animated: true)
        }
        //            }else if objFeed.feedType == "video"{
        //            }
    }
}

// MARK:- UITableView Delegate and Datasource
extension ArtistProfileVC : feedsTableCellDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        if tableView == self.tblFeeds{
            return objAppShareData.arrFeedsForArtistData.count
        }else if tableView == tblViewMoreFilter{
            return 3
        }else{ return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell = UITableViewCell()
        
        if tableView == tblFeeds{
            
            //RUN pagination
            if objAppShareData.arrFeedsForArtistData.count < self.totalCount {
                if indexPath.row >= objAppShareData.arrFeedsForArtistData.count - 4{
                    self.pageNo = pageNo + 1
                    //LoderManage = true
                    self.indicators.startAnimating()
                    self.LoderManage = false
                    if tableNo == 0   {
                        loadFeedsWithPage(page: self.pageNo , refresh: true)
                    }else if tableNo == 1 {
                        loadImagesWithPage(page: self.pageNo , refresh: true)
                    }else{
                        loadVideosWithPage(page: self.pageNo , refresh: true)
                    }
                }
            }
            
            tblView = tableView
            if objAppShareData.arrFeedsForArtistData.count>indexPath.row {
                
                let objFeeds = objAppShareData.arrFeedsForArtistData[indexPath.row]
                var cellId = "ImageVideo"
                
                if objFeeds.feedType == "text"{
                    cellId = "Text"
                }else{
                    cellId = "ImageVideo"
                }
                
                let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! feedsTableCell
                
                
                //amit
                cell.tag = indexPath.row
                cell.indexPath = indexPath
                cell.delegate = self
                cell.btnHiddenMoreOption.isHidden = true
                //amit
                if objFeeds.isSave == 0{
                    cell.btnSaveToFolder.setTitle("Save to folder", for: .normal)
                    cell.btnSaveToFolder.setImage(#imageLiteral(resourceName: "inactive_book_mark_ico"), for: .normal)
                }else{
                    cell.btnSaveToFolder.setTitle("Remove to folder", for: .normal)
                    cell.btnSaveToFolder.setImage(#imageLiteral(resourceName: "active_book_mark_ico"), for: .normal)
                }
                
                cell.btnSaveToFolder.isHidden = false
                cell.btnReportThisPost.isHidden = false
                cell.btnShareThisPost.isHidden = false
                
                cell.viewMore.isHidden = true
                
                cell.lblUserName.text = objFeeds.userInfo?.userName
                cell.lblCity.text = objFeeds.location
                cell.lblTime.text = objFeeds.timeElapsed
                
                let url = URL(string: (objFeeds.userInfo?.profileImage)!)
                if  url != nil {
                    //cell.imgProfile.af_setImage(withURL: url!)
                    cell.imgProfile.sd_setImage(with: url!, completed: nil)
                }else{
                    cell.imgProfile.image = #imageLiteral(resourceName: "cellBackground")
                }
                
                cell.txtCapView.text = objFeeds.caption
                cell.lblCaption.text = objFeeds.caption
                
                if objFeeds.feedType == "text"{
//                    let font = UIFont(name: "Nunito-Regular", size: 16.0)
//                    let fontHash = UIFont(name: "Nunito-SemiBold", size: 16.0)
//                    let fontMention = UIFont(name: "Nunito-SemiBold", size: 16.0)
                    let font = UIFont(name: "Nunito-Regular", size: 16.0)
                    let fontHash = UIFont(name: "Nunito-Regular", size: 16.0)
                    let fontMention = UIFont(name: "Nunito-Regular", size: 16.0)
                    //cell.txtCapView.setText(text: objFeeds.caption, withHashtagColor: UIColor.black, andMentionColor: #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1), andCallBack: { (word, type) in
                    cell.txtCapView.setText(text: objFeeds.caption, withHashtagColor: UIColor.black, andMentionColor: UIColor.black, andCallBack: { (word, type) in
                        self.gotoExploreDetailVCWithSearchText(searchText: "\(word)", type: "\(type)")
                    }, normalFont: font!, hashTagFont: fontHash!, mentionFont: fontMention!, isTappable:true)
                }else{
//                    let font = UIFont(name: "Nunito-Regular", size: 16.0)
//                    let fontHash = UIFont(name: "Nunito-SemiBold", size: 16.0)
//                    let fontMention = UIFont(name: "Nunito-SemiBold", size: 16.0)
                    let font = UIFont(name: "Nunito-Regular", size: 16.0)
                    let fontHash = UIFont(name: "Nunito-Regular", size: 16.0)
                    let fontMention = UIFont(name: "Nunito-Regular", size: 16.0)
                    cell.txtCapView.setText(text: objFeeds.caption, withHashtagColor: UIColor.black, andMentionColor: UIColor.black, andCallBack: { (word, type) in
                        self.gotoExploreDetailVCWithSearchText(searchText: "\(word)", type: "\(type)")
                    }, normalFont: font!, hashTagFont: fontHash!, mentionFont: fontMention!, isTappable:true)
                }
                
                
                //  if objFeeds.feedType == "image"{
                cell.scrollView.removeFromSuperview()
                //}
                //   cell.pageControll.isHidden=true
                if objFeeds.feedType == "video"{
                    cell.pageControll.isHidden=true
                    cell.btnShowTagNew.tag = indexPath.row
                    cell.pageControllView.isHidden = true;
                    if objFeeds.arrPhotoInfo.count > 0{
                        for newObj in objFeeds.arrPhotoInfo{
                            if newObj.arrTags.count > 0 || newObj.arrServiceTags.count > 0{
                                //cell.btnShowTagNew.isHidden = false
                                cell.btnShowTagNew.isHidden = true
                            }
                        }
                    }else{
                        cell.btnShowTagNew.isHidden = true
                    }
                    let height = self.view.frame.size.width
                    cell.heightOfImageRatio.constant = height
                    
                }else if objFeeds.feedType == "image"{
                    
                    var height = self.view.frame.size.width * 0.75
                    if objFeeds.feedImageRatio == "0"{
                        height = self.view.frame.size.width * 0.75
                    }else if objFeeds.feedImageRatio == "1"{
                        height = self.view.frame.size.width
                    }else{
                        height = self.view.frame.size.width*1.25
                    }
                    cell.heightOfImageRatio.constant = height
                    if objFeeds.arrPhotoInfo.count > 0{
                        for newObj in objFeeds.arrPhotoInfo{
                            if newObj.arrTags.count > 0 || newObj.arrServiceTags.count > 0{
                                //cell.btnShowTagNew.isHidden = false
                                cell.btnShowTagNew.isHidden = true
                            }
                        }
                    }else{
                        cell.btnShowTagNew.isHidden = true
                    }
                    cell.pageControll.isHidden=false
                    cell.pageControllView.isHidden = false;
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
                        //DispatchQueue.global().async {
                        cell.setPageControllMethod(objFeeds:objFeeds)
                        //}
                    }
                }else{
                }
                
                
                ///////code by deependra////////
                cell.imgPlay?.tag = indexPath.row
                cell.imgPlay?.superview?.tag = indexPath.section
                //
                let tapForLike = UITapGestureRecognizer(target: self, action: #selector(self.likeOnTap))
                tapForLike.numberOfTapsRequired = 2
                
                cell.btnShare.tag = indexPath.row
                cell.btnShare.superview?.tag = indexPath.section
                
                cell.btnLike.tag = indexPath.row
                cell.btnLike.superview?.tag = indexPath.section
                
                cell.imgFeeds?.tag = indexPath.row
                cell.setDefaultDesign()
                
                cell.lblLikeCount.text = "\(objFeeds.likeCount)"
                cell.lblCommentCount.text = "\(objFeeds.commentCount)"
                cell.lblTime.text = objFeeds.timeElapsed
                if objFeeds.commentCount > 1{
                    cell.comment.text = "Comments"
                }else{
                    cell.comment.text = "Comment"
                }
                
                if objFeeds.likeCount > 1{
                    cell.likes.text = "Likes"
                }else{
                    cell.likes.text = "Like"
                }
                
                // cell.imgProfile.setImageFream()
                
                if objFeeds.feedType == "video"{
                    if objFeeds.arrFeed[0].videoThumb.count > 0 {
                        //cell.imgFeeds?.af_setImage(withURL:URL.init(string: objFeeds.arrFeed[0].videoThumb)!)
                        cell.imgFeeds?.sd_setImage(with: URL.init(string: objFeeds.arrFeed[0].videoThumb)!, completed: nil)
                    }
                    cell.imgPlay?.isHidden = false
                    
                    let tapForPlayVideo = UITapGestureRecognizer(target: self, action: #selector(self.showVideoFromList))
                    tapForPlayVideo.numberOfTapsRequired = 1
                    cell.imgPlay?.addGestureRecognizer(tapForPlayVideo)
                    cell.imgPlay?.addGestureRecognizer(tapForLike)
                    tapForPlayVideo.require(toFail:tapForLike)
                    
                }else if objFeeds.feedType == "image"{
                    
                    cell.imgPlay?.isHidden = true
                    cell.pageControllView.tag = indexPath.row
                    cell.setDefaultDesign()
                    let tap = UITapGestureRecognizer(target: self, action: #selector(self.showImageFromList))
                    tap.numberOfTapsRequired = 1
                    cell.pageControllView.addGestureRecognizer(tap)
                    cell.pageControllView.addGestureRecognizer(tapForLike)
                    tap.require(toFail:tapForLike)
                }else{
                    if objAppShareData.validateUrl(objFeeds.caption) {
                        cell.lblCaption.tag = indexPath.row
                    }
                }
                
                if objFeeds.isLike {
                    cell.btnLike.isSelected = true
                }else {
                    cell.btnLike.isSelected = false
                }
                
                cell.btnComment.tag = indexPath.row
                //cell.btnViewComment.tag = indexPath.row
                cell.lblLikeCount.tag = indexPath.row
                cell.lblCommentCount.tag = indexPath.row
                cell.lblCommentCount.isUserInteractionEnabled = true
                let tapCommentCount = UITapGestureRecognizer(target: self, action: #selector(self.tappedCommentCount))
                tapCommentCount.numberOfTapsRequired = 1
                cell.lblCommentCount.addGestureRecognizer(tapCommentCount)
                
                if (cell.likes) != nil{
                    cell.likes.tag = indexPath.row
                }
                
                let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.tappedLikeCount))
                let tap2 = UITapGestureRecognizer(target: self, action: #selector(self.tappedLikeCount))
                tap1.numberOfTapsRequired = 1
                tap2.numberOfTapsRequired = 1
                let myInt = objFeeds.likeCount
                
                if myInt > 0{
                    
                    cell.lblLikeCount.isUserInteractionEnabled = true
                    cell.likes.isUserInteractionEnabled = true
                    cell.lblLikeCount.addGestureRecognizer(tap2)
                    cell.likes.addGestureRecognizer(tap1)
                }else {
                    
                    cell.lblLikeCount.isUserInteractionEnabled = false
                    cell.likes.isUserInteractionEnabled = false
                }
                
                if String(objFeeds.userInfo?._id ?? 0) == myId{
                    cell.btnFollow.isHidden=true;
                }else{
                    cell.btnFollow.isHidden=false;
                }
                
                cell.btnFollow.tag = indexPath.row
                cell.btnFollow.superview?.tag = indexPath.section
                if objFeeds.followerStatus {
                    cell.btnFollow.setTitle("Following", for: .normal)
                    cell.btnFollow.setTitleColor(UIColor.theameColors.blueColor, for: .normal)
                    cell.btnFollow.layer.borderWidth = 1
                    cell.btnFollow.layer.borderColor = UIColor.theameColors.blueColor.cgColor
                    cell.btnFollow.backgroundColor = UIColor.clear
                }else {
                    cell.btnFollow.setTitle("Follow", for: .normal)
                    cell.btnFollow.setTitleColor(UIColor.white, for: .normal)
                    cell.btnFollow.backgroundColor = appColor
                    cell.btnFollow.layer.borderWidth = 0
                }
                
                if strType == "image"{
                    cell.imgPlay?.isHidden = true
                }
                
                if String(objFeeds.userInfo?._id ?? 0) == myId{
                    cell.btnMore.isHidden=false;
                }else{
                    cell.btnMore.isHidden=false;
                }
                
                cell.btnMore.tag = indexPath.row
                cell.btnSaveToFolder.tag = indexPath.row
                cell.btnReportThisPost.tag = indexPath.row
                cell.btnShareThisPost.tag = indexPath.row
                cell.btnHiddenMoreOption.tag = indexPath.row
                cell.btnDeleteThisPost.tag = indexPath.row
                
                cell.btnMore.addTarget(self, action: #selector(btnMoreAction(_:)), for: .touchUpInside)
                cell.btnDeleteThisPost.addTarget(self, action: #selector(btnDeleteThisPostMethod(_:)), for: .touchUpInside)
                cell.btnSaveToFolder.addTarget(self, action: #selector(btnSaveToFolder(_:)), for: .touchUpInside)
                cell.btnReportThisPost.addTarget(self, action: #selector(btnReportThisPost(_:)), for: .touchUpInside)
                cell.btnShareThisPost.addTarget(self, action: #selector(btnShareThisPost(_:)), for: .touchUpInside)
                cell.btnHiddenMoreOption.addTarget(self, action: #selector(btnHiddenMoreOption(_:)), for: .touchUpInside)
                //}
                return cell
            }
        }else if tableView == tblViewMoreFilter{
            let cellIdentifier = "CellBottumTableList"
            if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? CellBottumTableList{
                let arr = ["All","Photo","Video"]
                let obj = arr[indexPath.row]
                cell.lblTitle.text = obj
                return cell
            }
        }else{
            return cell
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        
        if tableView == tblFeeds {
            
            if objAppShareData.arrFeedsForArtistData.count > indexPath.row{
                let objFeeds = objAppShareData.arrFeedsForArtistData[indexPath.row]
                
                if objFeeds.feedType == "text" {
                    
                    if let height = objFeeds.cellHeight{
                        return height
                    }else{
                        let height = getSizeForText(objFeeds.caption,maxWidth: (tblFeeds.frame.size.width - 12),font:"Nunito-Regular", fontSize: 16.0).height + 112
                        objFeeds.cellHeight = height
                        return height
                    }
                    // return getSizeForText(objFeeds.caption,maxWidth: (tblFeeds.frame.size.width - 12),font:"Roboto-Regular", fontSize: 16.0).height + 112
                }else {
                    if objFeeds.caption.count > 0 {
                        if let height = objFeeds.cellHeight{
                            return height + 6
                        }else{
                            var height = self.view.frame.size.width * 0.75 + 112
                            if objFeeds.feedImageRatio == "0"{
                                height = self.view.frame.size.width * 0.75 + 112
                            }else if objFeeds.feedImageRatio == "1"{
                                height = self.view.frame.size.width + 112
                            }else if objFeeds.feedImageRatio == "2"{
                                height = self.view.frame.size.width * 1.25 + 112
                            }else{
                                height = self.view.frame.size.width *  0.75 + 112
                            }
                            if objFeeds.feedType == "video"{
                                height = self.view.frame.size.width + 112
                            }
                            let newHeight = height + getSizeForText(objFeeds.caption, maxWidth: (tblFeeds.frame.size.width - 8), font: "Nunito-Regular", fontSize: 16.0).height
                            objFeeds.cellHeight = newHeight
                            return height + 6
                        }
                    }else{
                        if let height = objFeeds.cellHeight{
                            return height
                        }else{
                            var height = self.view.frame.size.width * 0.75 + 112
                            if objFeeds.feedImageRatio == "0"{
                                height = self.view.frame.size.width * 0.75 + 112
                            }else if objFeeds.feedImageRatio == "1"{
                                height = self.view.frame.size.width + 112
                            }else if objFeeds.feedImageRatio == "2"{
                                height = self.view.frame.size.width * 1.25 + 112
                            }else{
                                height = self.view.frame.size.width *  0.75 + 112
                            }
                            if objFeeds.feedType == "video"{
                                height = self.view.frame.size.width + 112
                            }
                            objFeeds.cellHeight = height
                            return height
                        }
                        //return self.view.frame.size.width * 0.75 + 100
                    }
                }
            }
            
        }else if tableView == tblViewMoreFilter{
            return 45
        }
        return 0;
    }
    
    private func tableView(_ tableView: UITableView, willDisplay cell: SuggessionTableCell, forRowAt indexPath: IndexPath){
        if tableView != tblViewMoreFilter{
            let objSuggession = self.suggesionArray[indexPath.row]
            if suggesionType == ""{
                cell.lblTag.text = "#"+objSuggession.tag
            }else{
                cell.lblName.text = "\(objSuggession.firstName) \(objSuggession.lastName)"
                cell.lblUserName.text = objSuggession.userName
                cell.imgProfile.image = UIImage.customImage.user
                if objSuggession.profileImage.count > 0 {
                    //cell.imgProfile.af_setImage(withURL:URL(string:objSuggession.profileImage)!)
                    cell.imgProfile.sd_setImage(with: URL(string:objSuggession.profileImage)!, completed: nil)
                }
            }}
    }
    
    //    public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    //        if let cell =  cell  as? feedsTableCell {
    //            self.tableView(self.tblFeeds, willDisplay: cell , forRowAt: indexPath)
    //        }
    //    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        self.viewBottumTable.isHidden = true
        
        if tableView == tblViewMoreFilter{
            let arr = ["All","Photo","Video"]
            let obj = arr[indexPath.row]
            if obj == "All"{
                self.feedaction()
            }else if obj == "Photo"{
                self.imageAction()
            }else if obj == "Video"{
                self.videoAction()
            }
            if  viewHeightGloble < Int(self.tabBarController?.view.frame.height ?? CGFloat(viewHeightGloble)) || self.tabBarController?.tabBar.isHidden == true{
                self.tabBarController?.tabBar.isHidden = false
                let b = Int(self.view.frame.width)
                let a = CGRect(x:0,y:0,width:b,height:viewHeightGloble)
                self.tabBarController?.view.frame = a//CGRect(x:0, y:0, width:self.view.frame.size.width, height:viewHeightGloble)
            }
        }else{
            let cell = tblView?.cellForRow(at: indexPath) as? feedsTableCell
            cell?.setDefaultDesign()
        }
    }
    
    /////////getSizeForText
    func getSizeForText(_ text: String, maxWidth width: CGFloat, font fontName: String, fontSize: Float) -> CGSize{
        
        let constraintSize = CGSize(width: width, height: .infinity)
        let font:UIFont = UIFont(name: fontName, size: CGFloat(fontSize))!;
        let attributes = [NSAttributedString.Key.font : font];
        let frame: CGRect = text.boundingRect(with: constraintSize, options: .usesLineFragmentOrigin,attributes: attributes, context: nil)
        let stringSize: CGSize = frame.size
        return stringSize
    }
}

//MARK: -  like, comment, shear button tablesub method
extension ArtistProfileVC{
    func addGesturesToMainView() {
        let socialLoginTap = UITapGestureRecognizer(target: self, action: #selector(handleSocialLoginTap(gestureRecognizer:)))
        socialLoginTap.delegate = self
        socialLoginTap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(socialLoginTap)
    }
    @objc func handleSocialLoginTap(gestureRecognizer: UIGestureRecognizer) {
        ////
        let indexPathOld = IndexPath(row:indexLastViewMoreView, section: 0)
        //self.tblView?.reloadRows(at: [indexPathOld], with: UITableViewRowAnimation.fade)
        
        if let cellOld = (tblView?.cellForRow(at: indexPathOld) as? feedsTableCell){
            //if indexPath != indexPathOld{
            cellOld.btnSaveToFolder.isHidden = false
            cellOld.btnReportThisPost.isHidden = true
            cellOld.viewMore.isHidden = true
            cellOld.setDefaultDesign()
            if cellOld.tagsHidden == false{
                //cellOld.setTagsHidden(!cellOld.tagsHidden)
            }
        }
        //}
        ////
    }
    
    @objc func btnMoreAction(_ sender: UIButton)
    {
        let objData = objAppShareData.arrFeedsForArtistData[sender.tag]
        let indexPath = IndexPath(row:(sender as AnyObject).tag, section: (sender as AnyObject).superview!!.tag)
        let cell = (tblView?.cellForRow(at: indexPath) as? feedsTableCell)!
        if cell.viewMore.isHidden == true{
            let id = UserDefaults.standard.string(forKey: UserDefaults.keys.userId) ?? ""
            if String(objData.userId) == id {
                cell.btnReportThisPost.isHidden = true
                cell.btnDeleteThisPost.isHidden = false
                cell.btnShareThisPost.isHidden = false
            }else{
                cell.btnReportThisPost.isHidden = false
                cell.btnDeleteThisPost.isHidden = true
                cell.btnShareThisPost.isHidden = false
            }
            cell.btnSaveToFolder.isHidden = false
            cell.viewMore.isHidden = false
            cell.btnHiddenMoreOption.isHidden = false
        } else {
            cell.btnSaveToFolder.isHidden = true
            cell.btnReportThisPost.isHidden = true
            cell.viewMore.isHidden = true
            cell.btnHiddenMoreOption.isHidden = true
            cell.setDefaultDesign()
        }
        ////
        let indexPathOld = IndexPath(row:indexLastViewMoreView, section: 0)
        //self.tblView?.reloadRows(at: [indexPathOld], with: UITableViewRowAnimation.fade)
        if let cellOld = (tblView?.cellForRow(at: indexPathOld) as? feedsTableCell){
            if indexPath != indexPathOld{
                cellOld.btnSaveToFolder.isHidden = true
                cellOld.btnReportThisPost.isHidden = true
                cellOld.viewMore.isHidden = true
                cellOld.setDefaultDesign()
            }
        }
        indexLastViewMoreView = (sender as AnyObject).tag
        ////
        
        //cell?.setDefaultDesign()
    }
    @objc func btnSaveToFolder(_ sender: UIButton)
    {
        
        let objData = objAppShareData.arrFeedsForArtistData[sender.tag]
        let indexPath = IndexPath(row:(sender as AnyObject).tag, section: (sender as AnyObject).superview!!.tag)
        let cell = (tblFeeds?.cellForRow(at: indexPath) as? feedsTableCell)!
        cell.btnSaveToFolder.isHidden = false
        cell.btnReportThisPost.isHidden = true
        cell.setDefaultDesign()
        if objData.isSave == 0{
            //objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
            let sb: UIStoryboard = UIStoryboard(name: "Feed", bundle: Bundle.main)
            if let objVC = sb.instantiateViewController(withIdentifier:"SaveToFolderVC") as? SaveToFolderVC{
                objVC.strFeedId = String(objData._id)
                objAppShareData.btnAddHiddenONMyFolder = false
                objVC.hidesBottomBarWhenPushed = true
                
                navigationController?.pushViewController(objVC, animated: true)
            }}else{
            self.webServiceForEditFolder(feedId:String(objData._id))
        }
        //   let objData = objAppShareData.arrFeedsForArtistData[sender.tag]
        //   let indexPath = IndexPath(row:(sender as AnyObject).tag, section: (sender as AnyObject).superview!!.tag)
        if  let cell = tblFeeds?.cellForRow(at: indexPath) as? feedsTableCell{
            cell.btnSaveToFolder.isHidden = true
            cell.btnReportThisPost.isHidden = true
            cell.btnHiddenMoreOption.isHidden = true
            cell.setDefaultDesign()
        }
    }
    
    
    @objc func btnShareThisPost(_ sender: UIButton)
    {
        
        let objData = objAppShareData.arrFeedsForArtistData[sender.tag]
        let sb = UIStoryboard(name:"Voucher",bundle:Bundle.main)
        let objChooseType = sb.instantiateViewController(withIdentifier:"ShareVoucherCodeVC") as! ShareVoucherCodeVC
        objChooseType.objFeeds = objData
        objChooseType.fromVoucher = false
        objChooseType.hidesBottomBarWhenPushed = true
        
        self.navigationController?.pushViewController(objChooseType, animated: true)
        /*
         
         let indexPath = IndexPath(row:(sender as AnyObject).tag, section: (sender as AnyObject).superview!!.tag)
         let cell = (tblFeeds?.cellForRow(at: indexPath) as? feedsTableCell)!
         cell.btnHiddenMoreOption.isHidden = true
         cell.setDefaultDesign()
         //  objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
         let firstActivityItem = objData.caption
         var urlString = ""
         var deeplink = WebURL.Deeplinking+"feedDetail/"
         if objData.arrFeed.count > 0{
         urlString = objData.arrFeed[0].feedPost
         deeplink = deeplink+String( objData._id)
         }
         let secondActivityItem : NSURL = NSURL(string:urlString)!
         let deeplinhActivityItem : NSURL = NSURL(string:deeplink)!
         // If you want to put an image
         let image : UIImage = #imageLiteral(resourceName: "ShareImageOnSocial")
         
         let activityViewController : UIActivityViewController = UIActivityViewController(
         activityItems: [firstActivityItem, secondActivityItem, image,deeplinhActivityItem], applicationActivities: nil)
         
         // This lines is for the popover you need to show in iPad
         activityViewController.popoverPresentationController?.sourceView = (sender as! UIButton)
         
         // This line remove the arrow of the popover to show in iPad
         //    activityViewController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.allZeros
         activityViewController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.any
         activityViewController.popoverPresentationController?.sourceRect = CGRect(x: 150, y: 150, width: 0, height: 0)
         
         self.present(activityViewController, animated: true, completion: nil)
         */
        
    }
    @objc func btnHiddenMoreOption(_ sender: UIButton)
    {
        let objData = objAppShareData.arrFeedsForArtistData[sender.tag]
        let indexPath = IndexPath(row:(sender as AnyObject).tag, section: (sender as AnyObject).superview!!.tag)
        let cell = (tblView?.cellForRow(at: indexPath) as? feedsTableCell)!
        cell.btnHiddenMoreOption.isHidden = true
        if strSearchText == "" || self.txtSearchText.text == "" || self.txtSearchText.text == nil{
            self.viewTextField.isHidden = true
        }
        cell.setDefaultDesign()
    }
    
    func webServiceForEditFolder(feedId:String){
        if !objServiceManager.isNetworkAvailable(){
            return
        }
        objWebserviceManager.StartIndicator()
        let parameters : Dictionary = [
            "feedId" : feedId,
            "folderId":""] as [String : Any]
        
        objWebserviceManager.requestPost(strURL: WebURL.removeToFolder, params: parameters  , success: { response in
            
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                objWebserviceManager.StopIndicator()
                
            }else{
                
                let strStatus =  response["status"] as? String ?? ""
                
                if strStatus == k_success{
                    self.viewWillAppear(true)
                }else{
                    objWebserviceManager.StopIndicator()
                    if let msg = response["message"] as? String{
                        objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                    }
                    
                }
            }
        }){ error in
            objWebserviceManager.StopIndicator()
            showAlertVC(title: kAlertTitle, message: kErrorMessage, controller: self)
        }
    }
    
    @objc func btnReportThisPost(_ sender: UIButton)
    {
        let objData = objAppShareData.arrFeedsForArtistData[sender.tag]
        let indexPath = IndexPath(row:(sender as AnyObject).tag, section: (sender as AnyObject).superview!!.tag)
        let cell = (tblView?.cellForRow(at: indexPath) as? feedsTableCell)!
        cell.btnSaveToFolder.isHidden = true
        cell.btnReportThisPost.isHidden = true
        cell.btnHiddenMoreOption.isHidden = true
        cell.setDefaultDesign()
        
        let sb: UIStoryboard = UIStoryboard(name: "Feed", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"FeedReportVC") as? FeedReportVC{
            objVC.objModel = objData
            objVC.hidesBottomBarWhenPushed = true
            
            navigationController?.pushViewController(objVC, animated: true)
        }
        //cell.setDefaultDesign()
    }
    
    @objc func tappedLikeCount(_ myLabel: UITapGestureRecognizer) {
        let sender = myLabel.view
        let indexPath = IndexPath(row:sender!.tag,section: sender!.superview!.tag)
        let cell = tblView?.cellForRow(at: indexPath) as! feedsTableCell
        cell.setDefaultDesign()
        
        let objFeeds = objAppShareData.arrFeedsForArtistData[(myLabel.view?.tag)!]
        let sb: UIStoryboard = UIStoryboard(name: "Feed", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"LikesListVC") as? LikesListVC{
            objVC.objFeeds = objFeeds
            isNavigate = true
            objVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(objVC, animated: true)
        }
    }
    
    @objc func btnDeleteThisPostMethod(_ sender: UIButton)
    {
        let objData = objAppShareData.arrFeedsForArtistData[sender.tag]
        let indexPath = IndexPath(row:(sender as AnyObject).tag, section: (sender as AnyObject).superview!!.tag)
        if  let cell = tblFeeds?.cellForRow(at: indexPath) as? feedsTableCell{
            cell.btnSaveToFolder.isHidden = true
            cell.btnReportThisPost.isHidden = true
            cell.btnHiddenMoreOption.isHidden = true
            cell.setDefaultDesign()
            var myId = ""
            if let dict = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] {
                myId = dict[UserDefaults.keys.id] as? String ?? ""
            }
            
            let param = ["feedType":objData.feedType, "id":String(objData._id), "userId":myId]
            // objAppShareData.showAlert(withMessage: "Under development", type: alertType.bannerDark,on: self)
            self.deleteCommentAlert(param: param, indexPath: indexPath)
        }
    }
    
    func deleteCommentAlert(param:[String:Any],indexPath:IndexPath){
        // Create the alert controller
        let alertController = UIAlertController(title: "Alert", message: "Are you sure want to remove this post?", preferredStyle: .alert)
        // Create the actions
        
        let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.webServiceForDeletePost(param: param)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
        }
        cancelAction.setValue(#colorLiteral(red: 1, green: 0.1490196078, blue: 0, alpha: 1), forKey: "titleTextColor")
        okAction.setValue(#colorLiteral(red: 0, green: 0.851996243, blue: 0.8281483054, alpha: 1), forKey: "titleTextColor")
        
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    @objc func likeOnTap(fromList recognizer: UITapGestureRecognizer) {
        let sender = recognizer.view
        let indexPath = IndexPath(row:sender!.tag,section: sender!.superview!.tag)
        let cell = tblView?.cellForRow(at: indexPath) as! feedsTableCell
        let index = (sender as AnyObject).tag
        cell.setDefaultDesign()
        
        let objFeeds = objAppShareData.arrFeedsForArtistData[index!]
        //showAnimatedLikeUnlikeOn(cell: cell, objFeeds: objFeeds)
        if !objFeeds.isLike{
            likeUnlikePost(cell: cell, objFeeds: objFeeds)
        }
    }
    
    @objc func tappedCommentCount(_ myLabel: UITapGestureRecognizer) {
        let sender = myLabel.view
        let indexPath = IndexPath(row:sender!.tag,section: sender!.superview!.tag)
        let cell = tblView?.cellForRow(at: indexPath) as! feedsTableCell
        cell.setDefaultDesign()
        
        let sb: UIStoryboard = UIStoryboard(name: "Feed", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"CommentListVC") as? CommentListVC{
            
            objVC.selectedIndex = (myLabel.view?.tag)!
            objVC.isOtherThanFeedScreen = true
            isNavigate = true
            objVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(objVC, animated: true)
        }
    }
    
    @objc func showImageFromList(fromList recognizer: UITapGestureRecognizer) {
        let sender = recognizer.view
        let indexPath = IndexPath(row:sender!.tag,section: sender!.superview!.tag)
        let cell = tblView?.cellForRow(at: indexPath) as! feedsTableCell
        cell.setDefaultDesign()
        cell.setTagsHidden(!cell.tagsHidden)
        let objFeed = objAppShareData.arrFeedsForArtistData[(recognizer.view?.tag)!]
        
        let indexPathOld = IndexPath(row:indexLastViewMoreView, section: 0)
        if let cellOld = (tblView?.cellForRow(at: indexPathOld) as? feedsTableCell){
            //cellOld.btnSaveToFolder.isHidden = false
            //cellOld.btnReportThisPost.isHidden = true
            cellOld.viewMore.isHidden = true
            cellOld.setDefaultDesign()
        }
        
        //        let sb: UIStoryboard = UIStoryboard(name: "Add", bundle: Bundle.main)
        //        if let objVC = sb.instantiateViewController(withIdentifier:"showImagesVC") as? showImagesVC {
        //            isNavigate = true
        //            objVC.isTypeIsUrl = true
        //            objVC.arrFeedImages = objFeed.arrFeed
        //            objVC.objFeeds = objFeed
        //
        //           present(objVC, animated: true)// { _ in }
        
        //            let sb: UIStoryboard = UIStoryboard(name: "ArtistProfile", bundle: Bundle.main)
        //            if let objVC = sb.instantiateViewController(withIdentifier:"FeedLikeCommentVC") as? FeedLikeCommentVC  {
        //                objVC.feedId = String(objFeed._id)
        //                isNavigate = true
        //                navigationController?.pushViewController(objVC, animated: true)
        //            }
        
    }
    
    //    }
    
    @objc func showVideoFromList(fromList recognizer: UITapGestureRecognizer) {
        isNavigate = true
        
        let sender = recognizer.view
        let indexPath = IndexPath(row:sender!.tag,section: sender!.superview!.tag)
        //        let cell = tblFeeds?.cellForRow(at: indexPath) as! feedsTableCell
        //        cell.setDefaultDesign()
        
        let objFeed = objAppShareData.arrFeedsForArtistData[(recognizer.view?.tag)!]
        
        if self.gridType == ""{
            if objFeed.arrFeed.count > 0 {
                addVideo(toVC: URL(string: (objFeed.arrFeed[0].feedPost))!)
            }
        }else{
            let sb: UIStoryboard = UIStoryboard(name: "ArtistProfile", bundle: Bundle.main)
            if let objVC = sb.instantiateViewController(withIdentifier:"FeedDetailVC") as? FeedDetailVC  {
                objVC.feedId = String(objFeed._id)
                isNavigate = true
                objVC.hidesBottomBarWhenPushed = true
                navigationController?.pushViewController(objVC, animated: true)
            }
        }
    }
    
    func addVideo(toVC url: URL) {
        let controller = AVPlayerViewController()
        controller.player = AVPlayer(url: url)
        controller.delegate = self
        controller.player?.play()
        controller.modalPresentationStyle = .fullScreen
        present(controller, animated: true)// { _ in }
    }
}

//MARK: - click like,comment, shear button
extension ArtistProfileVC{
    
    @IBAction func btnSharePost(_ sender: Any) {
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        
        let indexPath = IndexPath(row: (sender as AnyObject).tag, section: (sender as AnyObject).superview!!.tag)
        let cell = tblView?.cellForRow(at: indexPath) as? feedsTableCell
        // let objFeeds: feeds? = objAppshare.arrFeeds[(sender as AnyObject).tag]
        let index = (sender as AnyObject).tag
        let objFeeds = objAppShareData.arrFeedsForArtistData[index!]
        if (objFeeds.feedType == "video"){
            showAlertVC(title: kAlertTitle, message: "Under development", controller: self)
        } else {
            showShareActionSheet(withObject: objFeeds, andTableCell: cell!)
        }
    }
    
    func showShareActionSheet(withObject objFeeds: feeds, andTableCell cell: feedsTableCell){
        showAlertVC(title: kAlertTitle, message: "Under development", controller: self)
    }
    
    @IBAction func btnComment(onFeed sender: Any) {
        
        let sb: UIStoryboard = UIStoryboard(name: "Feed", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"CommentListVC") as? CommentListVC{
            objVC.selectedIndex = (sender as AnyObject).tag
            objVC.isOtherThanFeedScreen = true
            isNavigate = true
            objVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(objVC, animated: true)
        }
        
    }
    
    @IBAction func btnLikeFeed(_ sender: Any){
        let indexPath = IndexPath(row:(sender as AnyObject).tag, section: (sender as AnyObject).superview!!.tag)
        let cell = (tblView?.cellForRow(at: indexPath) as? feedsTableCell)!
        let index = (sender as AnyObject).tag
        let objFeeds = objAppShareData.arrFeedsForArtistData[index!]
        likeUnlikePost(cell: cell, objFeeds: objFeeds)
        
        
    }
    
    
    func likeUnlikePost(cell:feedsTableCell, objFeeds:feeds)->Void{
        
        if (objFeeds.isLike){
            objFeeds.isLike = false
            UIView.animate(withDuration: 0.2, animations: {() -> Void in
                cell.btnLike.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
            }, completion: {(_ finished: Bool) -> Void in
                UIView.animate(withDuration: 0.2, animations: {() -> Void in
                    cell.btnLike.transform = CGAffineTransform.identity
                    cell.btnLike.isSelected = false
                })
            })
            objFeeds.likeCount = objFeeds.likeCount - 1
            if objFeeds.likeCount > 1{
                cell.likes.text = "Likes"
            }else{
                cell.likes.text = "Like"
            }
        }else{
            objFeeds.isLike = true
            UIView.animate(withDuration: 0.2, animations: {() -> Void in
                cell.btnLike.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
            }, completion: {(_ finished: Bool) -> Void in
                UIView.animate(withDuration: 0.2, animations: {() -> Void in
                    cell.btnLike.transform = CGAffineTransform.identity
                    cell.btnLike.isSelected = true
                })
            })
            objFeeds.likeCount = objFeeds.likeCount + 1
            if objFeeds.likeCount > 1{
                cell.likes.text = "Likes"
            }else{
                cell.likes.text = "Like"
            }
        }
        cell.lblLikeCount.text = "\(objFeeds.likeCount)"
        
        
        //let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.userData) as! [String:Any]
        
        var dob = "2000-01-01"
        var gender = "male"
        
        if let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] {
            dob = decoded["dob"] as? String ?? "2000-01-01"
            gender = decoded["gender"] as? String ?? "male"
        }
        
        
        let dicParam  = ["feedId": objFeeds._id,
                         "userId":objFeeds.userInfo?._id ?? 0,
                         "likeById":self.myId,
                         "age":objAppShareData.getAge(from: dob),
                         "gender":gender,
                         "city":objLocationManager.currentCLPlacemark?.locality ?? "",
                         "state":objLocationManager.currentCLPlacemark?.administrativeArea ?? "",
                         "country":objLocationManager.currentCLPlacemark?.country ?? "",
                         "type":"feed"
            ]  as [String : Any]
        
        self.callWebserviceFor_LikeUnlike(dicParam: dicParam)
        
        //   objAppshare.playButtonClickSound()
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.tappedLikeCount))
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(self.tappedLikeCount))
        tap1.numberOfTapsRequired = 1
        tap2.numberOfTapsRequired = 1
        if objFeeds.likeCount > 0{
            cell.lblLikeCount.isUserInteractionEnabled = true
            cell.likes.isUserInteractionEnabled = true
            cell.lblLikeCount.addGestureRecognizer(tap2)
            cell.likes.addGestureRecognizer(tap1)
        }else {
            cell.lblLikeCount.isUserInteractionEnabled = false
            cell.likes.isUserInteractionEnabled = false
        }
    }
    
    func callWebserviceFor_LikeUnlike(dicParam: [AnyHashable: Any]){
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        var parameters = [String:Any]()
        parameters = dicParam as! [String : Any]
        objWebserviceManager.requestPost(strURL: WebURL.like, params: parameters, success: { response in
            objWebserviceManager.StopIndicator()
        }){ error in
            objWebserviceManager.StopIndicator()
        }
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        //RUN pagination
        //        if objAppShareData.arrFeedsForArtistData.count < self.totalCount {
        //            let arr = self.collectionView.indexPathsForVisibleItems
        //            print(self.collectionView.indexPathsForVisibleItems.last?.row)
        //            if arr.count>4{
        //                if arr[arr.count-2].row+4 >= objAppShareData.arrFeedsForArtistData.count-1{
        //                    self.pageNo = pageNo + 1
        //                    //LoderManage = true
        //                    if tableNo == 0   {
        //                        loadFeedsWithPage(page: self.pageNo , refresh: true)
        //                    }else if tableNo == 1 {
        //                        loadImagesWithPage(page: self.pageNo , refresh: true)
        //                    }else{
        //                        loadVideosWithPage(page: self.pageNo , refresh: true)
        //                    }
        //                }
        //            }
        //        }
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.view.endEditing(true)
        self.btnHiddenSearch.isHidden = true
        self.btnHiddenSearch2.isHidden = true
        self.viewMoreFilterOption.isHidden = true
        if objAppShareData.arrFeedsForArtistData.count <= 0{
            //self.dataScrollView.isScrollEnabled = false
            let a = self.dataScrollView.layer.frame.height + 295
            self.heightTableView.constant = a//self.heightTableView.constant
        }else if self.viewTableType == false {
            print(self.dataScrollView.contentOffset.y)
            print(self.collectionView.contentOffset.y)
            if (self.collectionView.contentOffset.y == 0.0 || self.collectionView.contentOffset.y == 2.0) && (self.dataScrollView.contentOffset.y == 0.0){
                self.collectionView.isScrollEnabled = false
            }else if (self.collectionView.contentOffset.y == 0.0 || self.collectionView.contentOffset.y == 2.0) && (self.dataScrollView.contentOffset.y == 295.0){
                self.collectionView.isScrollEnabled = true
            }
            let a = self.dataScrollView.layer.frame.height + 295
            self.heightTableView.constant = a
            print(self.heightTableView.constant)
            //dataScrollView.contentOffset = tblFeed
        }else{
            //self.dataScrollView.isScrollEnabled = true
            print(self.dataScrollView.contentOffset.y)
            print(self.tblFeeds.contentOffset.y)
            if (self.tblFeeds.contentOffset.y == 0.0) && (self.dataScrollView.contentOffset.y == 0.0){
                self.tblFeeds.isScrollEnabled = false
            }else if (self.tblFeeds.contentOffset.y == 0.0) && (self.dataScrollView.contentOffset.y == 295.0){
                self.tblFeeds.isScrollEnabled = true
            }
            let a = self.dataScrollView.layer.frame.height + 295
            self.heightTableView.constant = a
            print(self.heightTableView.constant)
            //dataScrollView.contentOffset = tblFeeds.contentOffset
        }
    }
    
    func gotoExploreDetailVCWithSearchText(searchText:String, type:String){
        objWebserviceManager.StartIndicator()
        if type == "hashtag" {
            
            let dicParam = [
                "tabType" : "hasTag",
                "tagId": "",
                "title": searchText
                ] as [String : Any]
            objWebserviceManager.StopIndicator()
            
            let sb: UIStoryboard = UIStoryboard(name: "Explore", bundle: Bundle.main)
            if let objVC = sb.instantiateViewController(withIdentifier:"ExploreDetailVC") as? ExploreDetailVC{
                objVC.sharedDict = dicParam
                objVC.hidesBottomBarWhenPushed = true
                navigationController?.pushViewController(objVC, animated: true)
            }
        }else{
            self.view.endEditing(true)
            
            if !objServiceManager.isNetworkAvailable(){
                objWebserviceManager.StopIndicator()
                
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }
            let dicParam = ["userName":searchText]
            
            objServiceManager.requestPost(strURL: WebURL.profileByUserName, params: dicParam, success: { response in
                if response["status"] as? String ?? "" == "success"{
                    var strId = ""
                    var strType = ""
                    if let dictUser = response["userDetail"] as? [String : Any]{
                        let myId = dictUser["_id"] as? Int ?? 0
                        strId = String(myId)
                        strType = dictUser["userType"] as? String ?? ""
                    }
                    let dic = [
                        "tabType" : "people",
                        "tagId": strId,
                        "userType":strType,
                        "title": searchText
                        ] as [String : Any]
                    self.openProfileForSelectedTagPopoverWithInfo(dict: dic)
                }
            }) { error in
                objWebserviceManager.StopIndicator()
                
            }
        }
        
    }
}

// MARK: - Notification Actions
extension ArtistProfileVC {
    
    func didReceiveSingleTapAt(indexPath : IndexPath,feedsTableCell: feedsTableCell ){
        
        let objFeed = objAppShareData.arrFeedsForArtistData[indexPath.row]
        if objFeed.feedType == "video"{
            isNavigate = true
            self.addVideo(toVC: URL(string: (objFeed.arrFeed[0].feedPost))!)
            
        }else if objFeed.feedType == "image" {
            
            let sb = UIStoryboard(name: "Add", bundle: Bundle.main)
            if let objShowImage = sb.instantiateViewController(withIdentifier:"showImagesVC") as? showImagesVC{
                isNavigate = true
                objShowImage.isTypeIsUrl = true
                objShowImage.arrFeedImages = objFeed.arrFeed
                objShowImage.objFeeds = objFeed
                objShowImage.modalPresentationStyle = .fullScreen
                present(objShowImage, animated: true)
                //                let sb: UIStoryboard = UIStoryboard(name: "ArtistProfile", bundle: Bundle.main)
                //                if let objVC = sb.instantiateViewController(withIdentifier:"FeedDetailVC") as? FeedDetailVC  {
                //                    objVC.feedId = String(objFeed._id)
                //                    isNavigate = true
                //                    navigationController?.pushViewController(objVC, animated: true)
            }
        }
    }
    
    func didReceiveDoubleTapAt(indexPath : IndexPath,feedsTableCell: feedsTableCell ){
        
        let objFeed = objAppShareData.arrFeedsForArtistData[indexPath.row]
        if !objFeed.isLike{
            likeUnlikePost(cell: feedsTableCell, objFeeds: objFeed)
        }
        
    }
    
    func didReceiveLondPressAt(indexPath : IndexPath,feedsTableCell: feedsTableCell ){
        
    }
    
    func tagPopoverDidReceiveSingleTapWithInfo(dict: [AnyHashable: Any]){
        print("tagPopover dict = %@",dict)
        self.openProfileForSelectedTagPopoverWithInfo(dict: dict)
        
    }
    
    func openProfileForSelectedTagPopoverWithInfo(dict : [AnyHashable: Any]){
        
        var dictTemp : [AnyHashable : Any]?
        
        dictTemp = dict
        
        if let dict1 = dict as? [String:[String :Any]] {
            if let dict2 = dict1.first?.value {
                dictTemp = dict2
            }
        }
        
        guard let dictFinal = dictTemp as? [String : Any] else { return }
        
        var strUserType: String?
        var tagId : Int?
        if  dictFinal["tabType"] as? String == "service"{
            self.view.endEditing(true)
            let sb: UIStoryboard = UIStoryboard(name: "ArtistProfile", bundle: Bundle.main)
            if let objVC = sb.instantiateViewController(withIdentifier:"ServiceDetailVC") as? ServiceDetailVC{
                objVC.hidesBottomBarWhenPushed = true
                objVC.param = dictFinal
                if objAppShareData.onProfile  == true{
                    navigationController?.pushViewController(objVC, animated: true)
                }
            }
        }else{
            if let userType = dictFinal["userType"] as? String{
                strUserType = userType
                
                if let idTag = dictFinal["tagId"] as? Int{
                    tagId = idTag
                }else{
                    if let idTag = dictFinal["tagId"] as? String{
                        tagId = Int(idTag)
                    }
                }
                
                if self.myId == String(tagId ?? 0) {
                    isNavigate = true
                    objAppShareData.isOtherSelectedForProfile = false
                    //  self.gotoProfileVC()
                    return
                }
                
                if let strUserType = strUserType, let tagId =  tagId {
                    objAppShareData.selectedOtherIdForProfile  = String(tagId)
                    objAppShareData.isOtherSelectedForProfile = true
                    objAppShareData.selectedOtherTypeForProfile = strUserType  //"artist" or "user"
                    self.gotoProfileVC()
                }
                
            }
        }
    }
    
    func gotoProfileVC (){
        objWebserviceManager.StopIndicator()
        
        self.view.endEditing(true)
        let sb: UIStoryboard = UIStoryboard(name: "ArtistProfile", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"SWRevealViewController") as? SWRevealViewController{
            objVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(objVC, animated: true)
        }
    }
    
    @objc func redirectionFromServiceTag(_ objNotify: Notification){
        
        if objAppShareData.onProfile  == true && selfVC == true{
            selfVC = false
            print(objNotify)
            print(objNotify.userInfo as! [String:Any])
            print("redirectionFromServiceTag")
            let dictFinal = objNotify.userInfo as! [String:Any]
            if  dictFinal["tabType"] as? String == "service"{
                let sb: UIStoryboard = UIStoryboard(name: "ArtistProfile", bundle: Bundle.main)
                if let objVC = sb.instantiateViewController(withIdentifier:"ServiceDetailVC") as? ServiceDetailVC{
                    objVC.hidesBottomBarWhenPushed = true
                    objVC.param = dictFinal
                    self.navigationController?.pushViewController(objVC, animated: true)
                }
            }
        }
    }
}


extension ArtistProfileVC:UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    func selectProfileImage() {
        let selectImage = UIAlertController(title: "Select Profile Image", message: nil, preferredStyle: .actionSheet)
        imagePicker.delegate = self
        let btn0 = UIAlertAction(title: "Cancel", style: .cancel, handler: {(_ action: UIAlertAction) -> Void in
        })
        
        let btn1 = UIAlertAction(title: "Camera", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            self.selectImageFromCamera()
        })
        
        let btn2 = UIAlertAction(title: "Photo Library", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            self.checkPhotoLibraryPermission()
            
        })
        fromCamera = true
        selectImage.addAction(btn0)
        selectImage.addAction(btn1)
        selectImage.addAction(btn2)
        present(selectImage, animated: true)
    }
    
    
    func checkPhotoLibraryPermission() {
        fromCamera = true
        let status = PHPhotoLibrary.authorizationStatus()
        switch status {
        case .authorized:
            //handle authorized status
            
            
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
                self.imagePicker.sourceType = .photoLibrary
                //  self.imagePicker.allowsEditing = true;
                self.imagePicker.modalPresentationStyle = .fullScreen
                self.present(self.imagePicker, animated: true)
            }
            
            
        //self.openGallary()
        case .denied, .restricted :
            //handle denied status
            let alert = UIAlertController(title: "Unable to access the Gallary",
                                          message: "To enable access, go to Settings > Privacy > Gallary and turn on Gallary access for this app.",
                                          preferredStyle: UIAlertController.Style.alert)
            
            let okAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alert.addAction(okAction)
            
            let settingsAction = UIAlertAction(title: "Settings", style: .default, handler: { _ in
                
                guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else { return }
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success)
                        in
                        
                    })
                }
            })
            alert.addAction(settingsAction)
            
            present(alert, animated: true, completion: nil)
        case .notDetermined:
            // ask for permissions
            PHPhotoLibrary.requestAuthorization() { status in
                switch status {
                case .authorized: break
                // as above
                case .denied, .restricted: break
                // as above
                case .notDetermined: break
                    // won't happen but still
                }
            }
        }
    }
    
    
    func selectImageFromCamera() //to Access Camera
    {
        fromCamera = true
        let authStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        if authStatus == AVAuthorizationStatus.denied {
            
            let alert = UIAlertController(title: "Unable to access the Camera",
                                          message: "To enable access, go to Settings > Privacy > Camera and turn on Camera access for this app.",
                                          preferredStyle: UIAlertController.Style.alert)
            
            let okAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alert.addAction(okAction)
            
            let settingsAction = UIAlertAction(title: "Settings", style: .default, handler: { _ in
                
                guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else { return }
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success)
                        in
                        
                    })
                }
            })
            alert.addAction(settingsAction)
            
            present(alert, animated: true, completion: nil)
        }
        else if (authStatus == AVAuthorizationStatus.notDetermined) {
            
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (granted) in
                if granted {
                    DispatchQueue.main.async {
                        self.imagePicker.sourceType = UIImagePickerController.SourceType.camera
                    }
                }
            })
        } else {
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                self.imagePicker.sourceType = .camera
                //self.imagePicker.allowsEditing = false;
                self.imagePicker.modalPresentationStyle = .fullScreen
                self.present(self.imagePicker, animated: true)
            }
        }
    }
}

extension ArtistProfileVC:ImageCropperDelegate{
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
            cropeImage(image: pickedImage)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true)
    }
    func cropeImage(image: UIImage){
        let croper = ImageCropperViewController(image:image)
        croper.delegate = self
        croper.modalPresentationStyle = .fullScreen
        self.imagePicker.present(croper, animated: true, completion: nil)
    }
    
    func imageCropperDidFinishCroppingImage(cropprdImage: UIImage){
        self.imgArtistProfile.image = cropprdImage
        self.userImage = cropprdImage
        
        self.dismiss(animated: true, completion: nil)
        self.callWebserviceForRegistration()
    }
    
    func imageCropperDidCancel(){
        dismiss(animated: true, completion: nil)
    }
    
    func callWebserviceForRegistration(){
        
        if let img = self.userImage{
            imgData = img.jpegData(compressionQuality: 0.1)
        }
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        
        objActivity.startActivityIndicator()
        objServiceManager.uploadMultipartData(strURL: WebURL.profileUpdate, params: [:], imageData: imgData, fileName: "file.jpg", key: "profileImage", mimeType: "image/jpg", success: { response in
            print(response)
            if response["status"] as? String ?? "" == "success"{
                
                if let dictUser = response["users"] as? [String : Any]{
                    objAppShareData.objAppdelegate.parseBusinessSignUpDetail(dictOfInfo: dictUser)
                    strAuthToken = UserDefaults.standard.string(forKey:UserDefaults.keys.authToken)!
                }
                if  let dict = response["users"] as? [String:Any]{
                    self.writeUserDataToFireBase(dict: dict)
                    objAppShareData.objAppdelegate.parseBusinessSignUpDetail(dictOfInfo: dict)
                }
                self.viewWillAppear(false)
            }else{
                if let msg = response["message"] as? String{
                    objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark, on: self)
                }
            }
        }) { error in
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
    func webServiceForDeletePost(param:[String:Any]){
        if !objServiceManager.isNetworkAvailable(){
            return
        }
        objWebserviceManager.StartIndicator()
        
        objWebserviceManager.requestPost(strURL: WebURL.deleteFeed, params: param  , success: { response in
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                objWebserviceManager.StopIndicator()
            }else{
                let strStatus =  response["status"] as? String ?? ""
                if strStatus == k_success{  self.viewWillAppear(true)    }else{
                    objWebserviceManager.StopIndicator()
                    if let msg = response["message"] as? String{
                        objAppShareData.showAlert(withMessage: "Post deleted successfully", type: alertType.bannerDark,on: self)
                    }}  }
        }){ error in
            objWebserviceManager.StopIndicator()
            showAlertVC(title: kAlertTitle, message: kErrorMessage, controller: self)
        } }
}

//MARK: - firebase method
extension ArtistProfileVC{
    func getBlockStatus(){
        if objAppShareData.isOtherSelectedForProfile {
            var chieldNode = "0"
            var strMyId = 0
            if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.businessInfoDic){
                if let userId = dict["_id"] as? String {
                    strMyId = Int(userId) ?? 0
                }
                var opponentId = 0
                if  objAppShareData.selectedOtherIdForProfile != "" && objAppShareData.selectedOtherIdForProfile != nil{
                    opponentId = Int(objAppShareData.selectedOtherIdForProfile) ?? 0
                }
                
                if opponentId > strMyId{
                    chieldNode = String(strMyId)+"_"+String(opponentId)
                }else{
                    chieldNode = String(opponentId)+"_"+String(strMyId)
                }
                
                self.viewBlockSupper.isHidden = true
                
                if strMyId != 0{
                    ref.child("blockUserArtist").child(chieldNode).observe(.value, with: { (snapshot) in
                        let dict = snapshot.value as? [String:Any]
                        var bagCount = "0"
                        if dict == nil{
                            self.viewBlockSupper.isHidden = true
                            
                        }else{
                            if let blockedBy = dict?["blockedBy"] as? Int {
                                if blockedBy == strMyId{
                                }else{
                                    self.viewBlockSupper.isHidden = false
                                }
                            }else if let blockedBy = dict?["blockedBy"] as? String {
                                if blockedBy == String(strMyId){
                                }else{
                                    self.viewBlockSupper.isHidden = false
                                }
                            }
                        }
                    })
                }
            }
        }
    }
}

//MARK: - FireBaseRegistration Method
extension ArtistProfileVC {
    
    func writeUserDataToFireBase(dict:[String : Any]){
        let calendarDate = ServerValue.timestamp()
        
        var id = 0
        if let ids = dict["_id"] as? Int{
            id = ids
        }else if let ids = dict["_id"] as? String{
            id = Int(ids)!
        }
        
        var img = dict["profileImage"] as? String ?? ""
        if (dict["profileImage"] as? String ?? "") != ""{
        }else{
            img = "http://koobi.co.uk:3000/uploads/default_user.png"
        }
        
        let dict1 = [
            "firebaseToken":"",
            //"firebaseToken":checkForNULL(obj: dict["firebaseToken"]),
            "isOnline":checkForNULL(obj:1),
            "lastActivity":checkForNULL(obj:calendarDate),
            "profilePic":checkForNULL(obj:img),
            "uId":checkForNULL(obj:id),
            "userName":checkForNULL(obj:dict["userName"]) ]
        
        let ref = Database.database().reference()
        let myId:String = String(dict["_id"] as? Int ?? 0000000)
        ref.child("users").child(myId).setValue(dict1)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            objWebserviceManager.StopIndicator()
        }
    }
    
    func badgeCount(){
        
        var strMyId = ""
        if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.businessInfoDic){
            if let userId = dict["_id"] as? String {
                strMyId = userId
            }
            if strMyId != ""{
                ref.child("socialBookingBadgeCount").child( UserDefaults.standard.string(forKey: UserDefaults.keys.userId) ?? strMyId).observe(.value, with: { (snapshot) in
                    
                    let dict = snapshot.value as? [String:Any]
                    
                    var bagCount = "0"
                    if let count = dict?["businessInvitationCount"] as? Int {
                        if count >= 100{
                            bagCount = "99+"
                        }else{
                            bagCount = String(count)
                        }
                    }else if let count = dict?["businessInvitationCount"] as? String {
                        bagCount = count
                    }
                    if bagCount == "0"{
                    }else{
                    }
                    
                    var WalkinhbagCount = "0"
                    if let count = dict?["commissionCount"] as? Int {
                        if count >= 100{
                            WalkinhbagCount = "99+"
                        }else{
                            WalkinhbagCount = String(count)
                        }
                    }else if let count = dict?["commissionCount"] as? String {
                        WalkinhbagCount = count
                    }
                    if WalkinhbagCount == "0"{
                        
                    }else{
                        
                    }
                    
                })
                
                var strMyId = ""
                if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.businessInfoDic){
                    if let userId = dict["_id"] as? String {
                        strMyId = userId
                    }
                    ref.child("businessBookingBadgeCount").child(strMyId).observe(.value, with: { [weak self] (snapshot) in
                        guard let strongSelf = self else {
                            return
                        }
                        let dict = snapshot.value as? [String:Any]
                        print(dict)
                        var totalCount = 0
                        if let count = dict?["socialCount"] as? Int {
                            totalCount = count+totalCount
                        }else if let count = dict?["socialCount"] as? String {
                            totalCount = (Int(count) ?? 0)+totalCount
                        }
                        if let count = dict?["bookingCount"] as? Int {
                            totalCount = count+totalCount
                        }else if let count = dict?["bookingCount"] as? String {
                            totalCount = (Int(count) ?? 0)+totalCount
                        }
                        if totalCount == 0{
                            self?.viewNotificationCount.isHidden = true
                            self?.lblNotificationCount.text =  "0"
                        }else{
                            self?.viewNotificationCount.isHidden = false
                            self?.lblNotificationCount.text =  String(totalCount)
                            if  totalCount >= 100{
                                self?.lblNotificationCount.text = "99+"
                            }
                        }
                    })
                }
            }
        }
    }
    @IBAction func btnProfileOption(_ sender: UIButton) {
        self.view.endEditing(true)
        let sb: UIStoryboard = UIStoryboard(name: "Notification", bundle: Bundle.main)
        let objChooseType = sb.instantiateViewController(withIdentifier:"NotificationVC") as! NotificationVC
        objChooseType.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(objChooseType, animated: true)
    }
}

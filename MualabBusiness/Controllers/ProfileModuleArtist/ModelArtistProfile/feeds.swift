    //
//  feeds.swift
//  Mualab
//
//  Created by MINDIII on 10/27/17.
//  Copyright © 2017 MINDIII. All rights reserved.
//

import UIKit
    
    class feeds: NSObject {
        
        var cellHeight : CGFloat?
        
        var _id : Int = 0
        var caption: String = ""
        var commentCount: Int = 0
        var feedType:String = ""
        var location: String = ""
        var likeCount: Int = 0
        var arrFeed : [feedData] = []
        var timeElapsed: String = ""
        var userInfo : storyUser?
        var isLike : Bool = false
        var followerStatus : Bool = false
        var arrPhotoInfo = [PhotoInfo]()
        var arrOfPeopleTagArray = [[PeopleTag]]()
        var isSave = 0
        var userId = 0
        var arrUser = [UserDetail]()

        var feedImageRatio = "0"
        init?(dict : [String:Any]){
            
            if let id = dict["_id"] as? Int{
                self._id = id
            }
            if let userIds = dict["userId"] as? Int{
                self.userId = userIds
            }
            if let id = dict["isSave"] as? Int{
                self.isSave = id
            }
            if let likeCount = dict["likeCount"] as? Int{
                self.likeCount = likeCount
            }
            
            if let commentCount = dict["commentCount"] as? Int{
                self.commentCount = commentCount
            }
            
            if let arr = dict["feedData"] as? [[String:Any]]{
                for dic in arr{
                    let obj = feedData(dict:dic)
                    self.arrFeed.append(obj!)
                }
            }
            
            if let feedType = dict["feedType"] as? String{
                self.feedType = feedType
            }
            
            if let caption = dict["caption"] as? String{
                self.caption = caption.decode(caption) ?? ""
                //self.caption = caption
            }
            
            if let location = dict["location"] as? String{
                self.location = location
            }
            
            if let timeElapsed = dict["timeElapsed"] as? String{
                self.timeElapsed = timeElapsed
            }
            
            if let isLike = dict["isLike"] as? Bool{
                self.isLike = isLike
            }
            
            if let arrUser = dict["userInfo"] as? NSArray{
                if let userDic = arrUser[0] as? [String:Any]{
                    self.userInfo = storyUser.init(dict: userDic)
                }
            }
            
            if let followerStatus = dict["followerStatus"] as? Int{
                if followerStatus == 1{
                    self.followerStatus = true
                }else{
                    self.followerStatus = false
                }
            }
            
            if self.feedType == "image"{
                
                guard self.arrFeed.count > 0 else{return}
                
                var arrOfTagInfoArray = [[TagInfo]]()
                
                if let arrOfarrTags = dict["peopleTag"] as? NSArray{
                    for arrTags in arrOfarrTags {
                        var arrTemp = [TagInfo]()
                        if let arrTags = arrTags as? [[String : Any]] {
                            for dict in arrTags {
                                let objTagInfo = TagInfo.initWithServerData(dict: dict,dictDetail:dict["tagDetails"] as! [String:Any])
                                arrTemp.append(objTagInfo)
                            }
                        }
                        arrOfTagInfoArray.append(arrTemp)
                    }
                }
          
                
                var arrOfServiceTagInfoArray = [[TagInfo]]()
                if let arrOfarrTags = dict["serviceTag"] as? NSArray{
                    ////
                    ////
                    var indexGlobal = 0
                    var index = 0
                    var arrNewTemp = [TagInfo]()
                    for arrTags in arrOfarrTags {
                        if let arrTags = arrTags as? [String : Any] {
                            //for dict in arrTags {
                            
                            //// For crash handle
                            let dictArtistServiceTagDetailNN = arrTags["arstistServiceTagDetail"] as? [String:Any]
                            let dictStaffServiceTagDetailNN = arrTags["staffServiceTagDetail"] as? [String:Any]
                            let buisnessTypeIDNN = dictArtistServiceTagDetailNN!["buisnessTypeID"] as? [Int]
                            let buisnessTypeIDStaffNN = dictStaffServiceTagDetailNN!["buisnessTypeID"] as? [Int]
                            if buisnessTypeIDNN!.count == 0 && buisnessTypeIDStaffNN!.count == 0{
                                break
                            }else{
                            }
                            ////
                           
                            let dictTag = arrTags["tag"] as? [String:Any]
                            let ind = dictTag!["index"] as? Int ?? 0
                            index = ind
                            /*
                             {
                                 print(objNotify)
                                 print(objNotify.userInfo as! [String:Any])
                                 print("redirectionFromServiceTag")
                                 let dictInfo = objNotify.userInfo as! [String:Any]
                                 objAppShareData.isFromServiceTag = true
                                 objAppShareData.selectedOtherIdForProfile = Int(dictInfo["artistId"] as? String ?? "") ?? 0
                                 let strInCallPrice = dictInfo["incallPrice"] as? String ?? ""
                                 let strOutCallPrice = dictInfo["outcallPrice"] as? String ?? ""
                                 let isOutCall = dictInfo["isOutCall"] as? String ?? ""
                                 var strIncallOrOutCall = ""
                                 if isOutCall == "0"{
                                    strIncallOrOutCall = "In Call"
                                 }else{
                                    strIncallOrOutCall = "Out Call"
                                 }
                                 /*if strInCallPrice.count == 0 || strInCallPrice == "0" || strInCallPrice == "0.0"{
                                     strIncallOrOutCall = "Out Call"
                                 }else{
                                     strIncallOrOutCall = "In Call"
                                 }
                                 */
                                 let objService = SubSubService.init(dict: [:])
                                 objService.artistId = objAppShareData.selectedOtherIdForProfile
                                 objService.serviceId = dictInfo["businessTypeId"] as? Int ?? 0
                                 objService.subServiceId = dictInfo["categoryId"] as? Int ?? 0
                                 objService.subSubServiceId = dictInfo["tagId"] as? Int ?? 0
                                 if let id = dictInfo["businessTypeId"] as? String {
                                     objService.serviceId = Int(id) ?? 0
                                 }
                                 if let id = dictInfo["categoryId"] as? String {
                                     objService.subServiceId = Int(id) ?? 0
                                 }
                                 if let id = dictInfo["tagId"] as? String {
                                     objService.subSubServiceId = Int(id) ?? 0
                                 }
                                 if let id = dictInfo["staffId"] as? String {
                                     objService.staffIdForEdit = Int(id) ?? 0
                                 }
                                 //// Himanshu Test
                                 //objService.staffIdForEdit = 28
                                 ////
                                 objAppShareData.isFromServiceTagBook = true
                                 objAppShareData.isBookingFromService = true
                                 objAppShareData.objServiceForEditBooking = objService
                                 
                                 let sbNew: UIStoryboard = UIStoryboard(name: "BookingNew", bundle: Bundle.main)
                                 if let objVC = sbNew.instantiateViewController(withIdentifier:"BookingNewVC") as? BookingNewVC{
                                     objVC.strInCallOrOutCallFromService = strIncallOrOutCall
                                     objVC.hidesBottomBarWhenPushed = true
                                     self.navController?.pushViewController(objVC, animated: true)
                                 }
                             }
                             */
                            
                            ////
                            let dictDetail = dictTag!["tagDetails"] as? [String:Any]
                            //let dictArtistDetail = arrTags["artistDetail"] as? [String:Any]
                            //let dictStaffDetail = arrTags["staffDetail"] as? [String:Any]
                            let dictArtistServiceTagDetail = arrTags["arstistServiceTagDetail"] as? [String:Any]
                            let dictStaffServiceTagDetail = arrTags["staffServiceTagDetail"] as? [String:Any]
                            let buisnessTypeID = dictArtistServiceTagDetail!["buisnessTypeID"] as? [Int]
                            var dictForServiceDetail = dictStaffServiceTagDetail
                            var staffId = 0
                            if buisnessTypeID!.count == 0{
                                dictForServiceDetail = dictStaffServiceTagDetail
                                //serviceId = dictForServiceDetail!["staffServiceID"] as? Int ?? 0
                                staffId = dictDetail!["staffId"] as? Int ?? 0
                            }else{
                                dictForServiceDetail = dictArtistServiceTagDetail
                                //serviceId = dictForServiceDetail!["artistServiceID"] as? Int ?? 0
                                staffId = dictDetail!["artistId"] as? Int ?? 0
                            }
                            let idB = dictForServiceDetail!["buisnessTypeID"] as? [Int]
                            let businessTypeIdN = idB![0]
                            let idC = dictForServiceDetail!["categoryID"] as? [Int]
                            let catTypeIdN = idC![0]
                            
                            let bName = dictForServiceDetail!["buisnessTypeName"] as? [String]
                            let businessTypeNameN = bName![0]
                            let cName = dictForServiceDetail!["categoryName"] as? [String]
                            let catTypeNameN = cName![0]
                            var serviceNameN = ""
                            if let tagName = dictForServiceDetail!["title"] as? [String]{
                                serviceNameN = tagName[0]
                            }
                            if serviceNameN.count == 0{
                               serviceNameN = dictForServiceDetail!["title"] as? String ?? ""
                            }
                            var description = ""
                            if let tagName = dictForServiceDetail!["description"] as? [String]{
                                description = tagName[0]
                            }
                            if description.count == 0{
                               description = dictForServiceDetail!["description"] as? String ?? ""
                            }
                            let completionTime = dictForServiceDetail!["completionTime"] as? String ?? ""
                            
                            
                            let isOutCall = dictDetail!["isOutCall"] as? String ?? "0"
                            index = ind
                            let dictTagDetail = ["artistId":String(dictDetail!["artistIdFeed"] as? Int ?? 0),
                                                 "incallPrice":dictForServiceDetail!["inCallPrice"] as? String ?? "0.0",
                                "outcallPrice":dictForServiceDetail!["outCallPrice"] as? String ?? "0.0",
                                "tagId":dictDetail!["tagId"] as? Int ?? 0,
                                "businessTypeId":businessTypeIdN,
                                "categoryId":catTypeIdN,
                                "staffId":staffId,
                                "staffInCallPrice":dictForServiceDetail!["inCallPrice"] as? String ?? "0.0",
                                "staffOutCallPrice":dictForServiceDetail!["outCallPrice"] as? String ?? "0.0",
                                "isOutCall":isOutCall,
                                "tabType":"service",
                                "businessTypeName":businessTypeNameN,
                                "categoryName":catTypeNameN,
                                "title":serviceNameN,
                                "description":description,
                                "completionTime":completionTime
                                ] as [String : Any]
                            print(dictTagDetail)
                            ////
                            
                            let objTagInfo = TagInfo.initWithServerData(dict: (arrTags["tag"] as? [String:Any])!, dictDetail:dictTagDetail)
                            //objTagInfo.strTagType = "service"
                            if index == indexGlobal{
                                arrNewTemp.append(objTagInfo)
                            }else{
                                indexGlobal = index
                                arrOfServiceTagInfoArray.append(arrNewTemp)
                                arrNewTemp.removeAll()
                                arrNewTemp.append(objTagInfo)
                            }
                            //}
                        }
                    }
                    if arrOfServiceTagInfoArray.count == 0{
                       arrOfServiceTagInfoArray.append(arrNewTemp)
                    }
                }
                
                //guard self.arrFeed.count == arrOfTagInfoArray.count else{return}
                
                for index in 0..<self.arrFeed.count {
                    let objPhotoInfo = PhotoInfo.init(photoInfo: ["":""])
                 
                    if arrOfTagInfoArray.count > index{
                        objPhotoInfo.arrTags = arrOfTagInfoArray[index]
                    }
                    
                    if arrOfServiceTagInfoArray.count > index{
                            objPhotoInfo.arrServiceTags = arrOfServiceTagInfoArray[index]
                    }
                    
                    self.arrPhotoInfo.append(objPhotoInfo)
                }
             }
            
            if let feedImageRatio1 = dict["feedImageRatio"] as? Int{
                self.feedImageRatio = String(feedImageRatio1)
            }else if let feedImageRatio1 = dict["feedImageRatio"] as? String{
                self.feedImageRatio = feedImageRatio1
            }
            
            if let arrUserInfo = dict["userInfo"] as? [[String:Any]]{
                for objDictArray in arrUserInfo{
                   let objs = UserDetail.init(dict: objDictArray)
                    self.arrUser.append(objs)
                }
            }
            
        }
    }


    class PeopleTag {
        
        var x_axis : Double = 0
        var y_axis : Double = 0
        var unique_tag_id : String = ""
        var dictTagDetails : [String : Any]?
        
        init(dict : [String:Any]){
            
            if let unique_tag_id = dict["unique_tag_id"] as? String{
                self.unique_tag_id = unique_tag_id
            }
            
            if let x_axis = dict["x_axis"] as? Double{
                self.x_axis = x_axis
            }else{
                if let x_axis = dict["x_axis"] as? String{
                    self.x_axis = Double(x_axis) ?? 0
                }
            }
            
            if let y_axis = dict["y_axis"] as? Double{
                self.y_axis = y_axis
            }else{
                if let y_axis = dict["y_axis"] as? String{
                    self.y_axis = Double(y_axis) ?? 0
                }
            }
            
            if let dictTagDetails = dict["tagDetails"] as? [String : Any]{
                self.dictTagDetails = dictTagDetails
            }
        }
    }

    class user: NSObject {
        
        var strUserName:String!
        var strUserId:String!
        var strEmail:String!
        var strPassword:String!
        var strProfileImage:String!
        var strContactNo:String!
        var strCountryCode:String!
        var strUserType:String!
        var strSocialId:String!
        var strChatId:String!
        var strFirebaseToken:String!
        var strTime:String!
        var strPhoneNo:String!
        var strCode:String!
        var strUserComment:String!
        var strCommentId = ""
        var commentsLikeCount:Int!
        var isFollowing:Bool = false
        var isFollower:Bool = false
        var isLikeComment:Bool = false
        
    }
    
    class storyUser {
        
        var _id : Int = 0
        var storyCount:Int = 1
        var firstName : String = ""
        var lastName : String = ""
        var profileImage : String = ""
        var userName : String = ""
        var followerStatus : Bool = false
        
        init?(dict : [String:Any]) {
            if let id = dict["_id"] as? Int{
                self._id = id
            }
            
            if let firstName = dict["firstName"] as? String{
                self.firstName = firstName
            }
            if let lastName = dict["lastName"] as? String{
                self.lastName = lastName
            }
            if let profileImage = dict["profileImage"] as? String{
                self.profileImage = profileImage
            }
            if let userName = dict["userName"] as? String{
                self.userName = userName
            }
            if let storyCount = dict["count"] as? Int{
                self.storyCount = storyCount
            }
            
        }
    }
    
    class likeUser {
        
        var _id : Int = 0
        var likeById : Int = 0
        var firstName : String = ""
        var lastName : String = ""
        var profileImage : String = ""
        var userName : String = ""
        var followerStatus : Bool = false
        
        init?(dict : [String:Any]) {
            if let id = dict["_id"] as? Int{
                self._id = id
            }
            if let likeById = dict["likeById"] as? Int{
                self.likeById = likeById
            }
            if let firstName = dict["firstName"] as? String{
                self.firstName = firstName
            }
            if let lastName = dict["lastName"] as? String{
                self.lastName = lastName
            }
            if let profileImage = dict["profileImage"] as? String{
                self.profileImage = profileImage
            }
            if let userName = dict["userName"] as? String{
                self.userName = userName
            }
            if let followerStatus = dict["followerStatus"] as? Bool{
                self.followerStatus = followerStatus
            }
        }
    }
    
    class commentUser {
        
        var _id : Int = 0
        var commentById : Int = 0
        var firstName : String = ""
        var lastName : String = ""
        var profileImage : String = ""
        var userName : String = ""
        var commentLikeCount:Int = 0
        var comment : String = ""
        var timeElapsed: String = ""
        var likeStatus:Bool = false
        var type : String = ""
        var feedId : String = ""

        init?(dict : [String:Any]) {
            if let id = dict["_id"] as? Int{
                self._id = id
            }
          
            feedId = dict["feedId"] as? String ?? ""
            if let id = dict["feedId"] as? Int{
                self.feedId = String(id)
            }
            
            if let commentById = dict["commentById"] as? Int{
                self.commentById = commentById
            }
            
            if let commentLikeCount = dict["commentLikeCount"] as? Int{
                self.commentLikeCount = commentLikeCount
            }
            
            if let firstName = dict["firstName"] as? String{
                self.firstName = firstName
            }
            
            if let lastName = dict["lastName"] as? String{
                self.lastName = lastName
            }
            
            if let profileImage = dict["profileImage"] as? String{
                self.profileImage = profileImage
            }
            
            if let userName = dict["userName"] as? String{
                self.userName = userName
            }
            
            if let comment = dict["comment"] as? String{
                self.comment = comment
            }
            
            if let type = dict["type"] as? String{
                self.type = type
            }
            
            if let timeElapsed = dict["timeElapsed"] as? String{
                self.timeElapsed = timeElapsed
            }
            if let likeStatus = dict["isLike"] as? Bool{
                self.likeStatus = likeStatus
            }
        }
    }

    
    
    class UserDetailArtistProfile {
        
        var _id : String = ""
        var firstName: String = ""
        var lastName : String = ""
        var profileImage: String = ""
        var userName : String = ""
        var address : String = ""
        var address2 : String = ""
        
        var bio : String = ""
        var buildingNumber : String = ""
        var businessName : String = ""
        var businessType: String = ""
        var businesspostalCode : String = ""
        var certificateCount : String = ""
        var contactNo : String = ""
        var countryCode : String = ""
        var dob : String = ""
        var email : String = ""
        var followersCount : String = ""
        var followerStatus : String = ""
        
        var favoriteStatus : String = ""
        var followingCount : String = ""
        var gender: String = ""
        var postCount : String = ""
        var ratingCount : String = ""
        var reviewCount : String = ""
        var serviceCount: String = ""
        var serviceType : String = ""
        var userType : String = ""
        var aboutUs : String = ""
        var radius : String = ""
        var isCertificateVerify : String = ""
        
        
        
        init(dict : [String:Any]) {
            
            if let radius = dict["radius"] as? Int{
                self.radius = String(radius)
            }else if let radius = dict["radius"] as? String{
                self.radius = radius
            }
            
            if let isCertificateVerify = dict["isCertificateVerify"] as? Int{
                self.isCertificateVerify = String(isCertificateVerify)
            }else if let isCertificateVerify = dict["isCertificateVerify"] as? String{
                self.isCertificateVerify = isCertificateVerify
            }
            
            if let id = dict["_id"] as? Int{
                self._id = String(id)
            }else if let id1 = dict["_id"] as? String{
                self._id = id1
            }
            
            if let aboutUs = dict["bio"] as? String{
                self.aboutUs = aboutUs
            }
            
            if let firstName = dict["firstName"] as? String{
                self.firstName = firstName
            }
            
            if let lastName = dict["lastName"] as? String{
                self.lastName = lastName
            }
            
            if let profileImage = dict["profileImage"] as? String{
                self.profileImage = profileImage
            }
            
            if let userName = dict["userName"] as? String{
                self.userName = userName
            }
            
            if let address1 = dict["address"] as? String{
                self.address = address1
            }
            
            if let addres = dict["address2"] as? String{
                self.address2 = addres
            }
            
            if let bio1 = dict["bio"] as? String{
                self.bio = bio1
            }
            
            if let businessNames = dict["businessName"] as? String{
                self.businessName = businessNames
            }
            
            if let businessTypes = dict["businessType"] as? String{
                self.businessType = businessTypes
            }
            
            if let genders = dict["gender"] as? String{
                self.gender = genders
            }
            
            if let emails = dict["email"] as? String{
                self.email = emails
            }
            
            if let userTypes = dict["userType"] as? String{
                self.userType = userTypes
            }
            if let serviceTypes = dict["serviceType"] as? String{
                self.serviceType = serviceTypes
            }
            
            if let buildingNumber1 = dict["buildingNumber"] as? Int{
                self.buildingNumber = String(buildingNumber1)
            }else if let buildingNumber2 = dict["buildingNumber"] as? String{
                self.buildingNumber = buildingNumber2
            }
            
            if let businesspostalCode1 = dict["businesspostalCode"] as? Int{
                self.businesspostalCode = String(businesspostalCode1)
            }else if let businesspostalCode2 = dict["businesspostalCode"] as? String{
                self.businesspostalCode = businesspostalCode2
            }
            
            if let certificateCount1 = dict["certificateCount"] as? Int{
                self.certificateCount = String(certificateCount1)
            }else if let certificateCount2 = dict["certificateCount"] as? String{
                self.certificateCount = certificateCount2
            }
            
            if let contactNo1 = dict["contactNo"] as? Int{
                self.contactNo = String(contactNo1)
            }else if let contactNo2 = dict["contactNo"] as? String{
                self.contactNo = contactNo2
            }
            
            if let countryCode1 = dict["countryCode"] as? Int{
                self.countryCode = String(countryCode1)
            }else if let countryCode2 = dict["countryCode"] as? String{
                self.countryCode = countryCode2
            }
            
            if let dob1 = dict["dob"] as? Int{
                self.dob = String(dob1)
            }else if let dob2 = dict["dob"] as? String{
                self.dob = dob2
            }
            
            if let followersCount1 = dict["followersCount"] as? Int{
                self.followersCount = String(followersCount1)
            }else if let followersCount2 = dict["followersCount"] as? String{
                self.followersCount = followersCount2
            }
            
            if let favoriteStatus = dict["favoriteStatus"] as? Int{
                self.favoriteStatus = String(favoriteStatus)
            }else if let favoriteStatus = dict["favoriteStatus"] as? String{
                self.favoriteStatus = favoriteStatus
            }
            
            if let followerStatus = dict["followerStatus"] as? Int{
                self.followerStatus = String(followerStatus)
            }else if let followerStatus = dict["followerStatus"] as? String{
                self.followerStatus = followerStatus
            }
            
            if let followingCount1 = dict["followingCount"] as? Int{
                self.followingCount = String(followingCount1)
            }else if let followingCount2 = dict["followingCount"] as? String{
                self.followingCount = followingCount2
            }
            
            if let postCount1 = dict["postCount"] as? Int{
                self.postCount = String(postCount1)
            }else if let postCount2 = dict["postCount"] as? String{
                self.postCount = postCount2
            }
            
            if let ratingCount1 = dict["ratingCount"] as? Int{
                self.ratingCount = String(ratingCount1)
            }else if let ratingCount2 = dict["ratingCount"] as? String{
                self.ratingCount = ratingCount2
            }else if let ratingCount3 = dict["ratingCount"] as? Double{
                self.ratingCount = String(ratingCount3)
            }
            
            if let reviewCount1 = dict["reviewCount"] as? Int{
                self.reviewCount = String(reviewCount1)
            }else if let reviewCount2 = dict["reviewCount"] as? String{
                self.reviewCount = reviewCount2
            }
            
            if let serviceCount1 = dict["serviceCount"] as? Int{
                self.serviceCount = String(serviceCount1)
            }else if let serviceCount2 = dict["serviceCount"] as? String{
                self.serviceCount = serviceCount2
            }
        }
    }

    
    

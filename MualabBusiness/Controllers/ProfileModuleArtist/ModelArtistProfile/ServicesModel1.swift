//
//  ServicesModel.swift
//  MualabBusiness
//
//  Created by mac on 28/05/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

class ServicesModel: NSObject {
    var serviceId : Int = 0
    var serviceName : String = ""
    var arrSubServices : [SubServicesModel] = [SubServicesModel]()
    
    init(dict: [String : Any]){
        
        if let serviceId = dict["serviceId"] as? Int{
            self.serviceId = serviceId
        }
        if let serviceName = dict["serviceName"] as? String{
            self.serviceName = serviceName
        }
        
        if let arr = dict["subServies"] as? [[String : Any]]{
            for dict in arr{
                let objSubService = SubServicesModel.init(dict: dict)
                self.arrSubServices.append(objSubService)
            }
        }
    }
}


class SubServicesModel: NSObject {
    
var serviceId : Int = 0
var Id : Int = 0
var serviceName : String = ""
var subServiceId : Int = 0
var subServiceName : String = ""

var arrSubSubService : [SubSubServices] = [SubSubServices]()

init(dict : [String : Any]){
    
    Id = dict["_id"] as? Int ?? 0
    serviceId = dict["serviceId"] as? Int ?? 0
    subServiceId = dict["subServiceId"] as? Int ?? 0
    subServiceName = dict["subServiceName"] as? String ?? ""
    
    if let arr = dict["artistservices"] as? [[String : Any]]{
        for dict in arr{
            let obj = SubSubServices.init(dict: dict)
            self.arrSubSubService.append(obj)
        }
    }
}
}


  class SubSubServices:NSObject {
    var subSubServiceId : Int = 0
    var inCallPrice : Double = 0
    var outCallPrice : Double = 0
    var subSubServiceName : String = ""
    var completionTime : String = ""
    
    init(dict : [String : Any]){
        subSubServiceId = dict["_id"] as? Int ?? 0
        subSubServiceName = dict["title"] as? String ?? ""
        inCallPrice = dict["inCallPrice"] as? Double ?? 0
        outCallPrice = dict["outCallPrice"] as? Double ?? 0
        completionTime = dict["completionTime"] as? String ?? ""
    }
}

//
//  CertificateModel.swift
//  MualabBusiness
//
//  Created by mac on 28/05/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

class CertificateModel: NSObject {
    var _id : String = "0"
    var __v: String = "0"
    var artistId: String = "0"
    var status: String = "0"

    var certificateImage  = ""
    var crd: String = ""
    var upd: String = ""
    
    var title: String = ""
    
    var descriptions  = ""

    
    init?(dict : [String:Any]){
        
        if let id = dict["_id"] as? Int{
            self._id = String(id)
        }else if let id = dict["_id"] as? String{
            self._id = id
        }
        
        if let __v1 = dict["__v"] as? Int{
            self.__v = String(__v1)
        }else if let __v1 = dict["__v"] as? String{
            self.__v = __v1
        }
 
        if let artistId1 = dict["artistId"] as? Int{
            self.artistId = String(artistId1)
        }else if let artistId1 = dict["artistId"] as? String{
            self.artistId = artistId1
        }
        
        if let status1 = dict["status"] as? Int{
            self.status = String(status1)
        }else if let status1 = dict["status"] as? String{
            self.status = status1
        }
        
        if let certificateImage1 = dict["certificateImage"] as? String{
            self.certificateImage = certificateImage1
        }
        
        if let crd1 = dict["crd"] as? String{
            self.crd = crd1
        }
        if let crd1 = dict["title"] as? String{
            self.title = crd1
        }
        if let crd1 = dict["description"] as? String{
            self.descriptions = crd1
        }
        if let upd1 = dict["upd"] as? String{
            self.upd = upd1
        }
    }
    
}
class VoucherModel: NSObject {
    var _id : String = "0"
    var artistId: String = "0"
    var voucherCode  = ""

    var amount  = ""
    
    var discountType: String = ""
    
    var endDate: String = ""
    var startDate: String = ""
    var status  = ""
    
    
    init?(dict : [String:Any]){
        
        if let id = dict["_id"] as? Int{
            self._id = String(id)
        }else if let id = dict["_id"] as? String{
            self._id = id
        }
        if let certificateImage1 = dict["voucherCode"] as? String{
            self.voucherCode = certificateImage1
        }
        
        if let artistId1 = dict["artistId"] as? Int{
            self.artistId = String(artistId1)
        }else if let artistId1 = dict["artistId"] as? String{
            self.artistId = artistId1
        }
        
        if let artistId1 = dict["amount"] as? Int{
            self.amount = String(artistId1)
        }else if let artistId1 = dict["amount"] as? String{
            self.amount = artistId1
        }
        
        if let artistId1 = dict["discountType"] as? Int{
            self.discountType = String(artistId1)
        }else if let artistId1 = dict["discountType"] as? String{
            self.discountType = artistId1
        }
        
        if let artistId1 = dict["status"] as? Int{
            self.status = String(artistId1)
        }else if let artistId1 = dict["status"] as? String{
            self.status = artistId1
        }
        
        
        if let crd1 = dict["startDate"] as? String{
            self.startDate = crd1
        }
        if let crd1 = dict["endDate"] as? String{
            self.endDate = crd1
        }
        
        
        let a = self.endDate.components(separatedBy: "-")
        if a.count > 2{
            self.endDate = a[2]+"/"+a[1]+"/"+a[0]
        }
        
        let b = self.startDate.components(separatedBy: "-")
        if b.count > 2{
            self.startDate = b[2]+"/"+b[1]+"/"+b[0]
        }
        
    }
    
}

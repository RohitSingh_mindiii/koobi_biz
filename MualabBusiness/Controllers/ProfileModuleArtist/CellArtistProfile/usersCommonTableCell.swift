//
//  usersCommonTableCell.swift
//  Mualab
//
//  Created by MINDIII on 10/30/17.
//  Copyright © 2017 MINDIII. All rights reserved.
//

import UIKit

class usersCommonTableCell: UITableViewCell {
    
    
    //
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var imgComment: UIImageView!
    //
    @IBOutlet weak var lblComment: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    //
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var viewBlur: UIView!
    //
    @IBOutlet weak var btnLike: UIButton?
    @IBOutlet weak var btnFollow: UIButton!
    @IBOutlet weak var btnProfile: UIButton!

    //
   

    override func awakeFromNib() {
        super.awakeFromNib()
        //imgProfile.contentMode = .scaleAspectFill
        self.btnLike?.imageView?.contentMode = .scaleAspectFit
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func scaleImage(_ recognizer: UIPinchGestureRecognizer) {
        recognizer.view?.transform = (recognizer.view?.transform.scaledBy(x: recognizer.scale, y: recognizer.scale))!
        recognizer.scale = 1

    }

}

//
//  feedsTableCell.swift
//  Mualab
//
//  Created by MINDIII on 10/27/17.
//  Copyright © 2017 MINDIII. All rights reserved.
//

import UIKit
import AttributedTextView
import SDWebImage
import Kingfisher

protocol feedsTableCellDelegate {
    
    func didReceiveSingleTapAt(indexPath : IndexPath,feedsTableCell: feedsTableCell )
    func didReceiveDoubleTapAt(indexPath : IndexPath,feedsTableCell: feedsTableCell )
    func didReceiveLondPressAt(indexPath : IndexPath,feedsTableCell: feedsTableCell )
    func tagPopoverDidReceiveSingleTapWithInfo(dict: [AnyHashable: Any])
    
}

class feedsTableCell: UITableViewCell,UIScrollViewDelegate, EBTagPopoverDelegate, EBPhotoViewDelegate {
    
    private(set) var tagsHidden = true
    var objFeeds:feeds?
    var indexPath : IndexPath!
    var delegate  : feedsTableCellDelegate!
    //more option view
    var placeholderImage:UIImage?
    @IBOutlet weak var viewShowTag: UIView!
    @IBOutlet weak var btnShowTag: UIButton!
    @IBOutlet weak var btnMore: UIButton!
    @IBOutlet weak var viewMore: UIView!
    @IBOutlet weak var btnSaveToFolder: UIButton!
    @IBOutlet weak var btnReportThisPost: UIButton!
    @IBOutlet weak var btnShareThisPost: UIButton!
    @IBOutlet weak var btnDeleteThisPost: UIButton!
    @IBOutlet weak var btnHiddenMoreOption: UIButton!
    @IBOutlet weak var btnProfile: UIButton!
    //
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var imgFeeds: UIImageView?
    @IBOutlet weak var imgPlay : UIImageView?
    //
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var btnComment: UIButton!
    // @IBOutlet weak var btnViewComment: UIButton!
    @IBOutlet weak var btnFollow: UIButton!
    @IBOutlet weak var btnShowTagNew: UIButton!
    @IBOutlet weak var viewTopOption: UIView!
    @IBOutlet weak var viewSeparatorViewMore: UIView!
    @IBOutlet weak var heightOfImageRatio: NSLayoutConstraint!

    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblLikeCount: UILabel!
    @IBOutlet weak var likes: UILabel!
    @IBOutlet weak var comment: UILabel!
    @IBOutlet weak var lblCommentCount: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblCaption: UILabel!
    //
    @IBOutlet weak var imageVideoView: UIView!
    @IBOutlet weak var pageControllView: UIView!
    //
    @IBOutlet weak var txtCapView: AttrTextView!
    @IBOutlet weak var pageControll: UIPageControl!
    
    
    let scrollView = UIScrollView()
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        self.txtCapView.textContainerInset = UIEdgeInsets.zero
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func btnShowTagAction( sender: UIButton){
        self.setTagsHidden(!tagsHidden)
    }
    
    func setPageControllMethod(objFeeds:feeds){
    
        self.objFeeds = objFeeds
        let arrFeed = objFeeds.arrFeed
        
        let scrollWidth = self.imageVideoView.frame.size.width
        let scrollHeight = self.imageVideoView.frame.size.height
        
        self.scrollView.delegate = self
        self.scrollView.frame = CGRect(x: 0, y:0, width: scrollWidth, height: scrollHeight)
        self.scrollView.contentSize = CGSize(width:scrollWidth * CGFloat(arrFeed.count), height:scrollHeight)
        self.scrollView.isPagingEnabled = true
        self.scrollView.showsHorizontalScrollIndicator=false
        self.scrollView.bounces = false
        
        self.pageControll.hidesForSinglePage = true
        self.pageControll.numberOfPages = arrFeed.count
        self.pageControll.currentPage = 0
        self.pageControll.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
        
        
        for index in  0..<arrFeed.count {
            
            //amit
            let viewOuter = UIView.init(frame: CGRect(x: CGFloat(index) * scrollWidth, y: 0, width: scrollWidth, height: scrollHeight))
            viewOuter.backgroundColor = UIColor.white
            
            let scrollVwEBPhotoView =   EBPhotoView.init(frame: CGRect(x: 0, y: 0, width: scrollWidth, height: scrollHeight))
            
            scrollVwEBPhotoView.tag = index
            scrollVwEBPhotoView.myDelegate = self
//            if objFeeds.arrPhotoInfo.count > 1{
//                scrollVwEBPhotoView.imageView.contentMode = .scaleAspectFit
//            }else{
//                scrollVwEBPhotoView.imageView.contentMode = .scaleAspectFit
//            }
            
            /*
            scrollVwEBPhotoView.imageView.contentMode = .scaleAspectFit
            if self.objFeeds?.feedImageRatio == "0"{
                scrollVwEBPhotoView.imageView.contentMode = .scaleToFill
            }else{
                scrollVwEBPhotoView.imageView.contentMode = .scaleAspectFit
            }
            */
            scrollVwEBPhotoView.imageView.clipsToBounds = true
            scrollVwEBPhotoView.adjustsContentModeForImageSize = false
            
            scrollVwEBPhotoView.maximumZoomScale = 1.0
            scrollVwEBPhotoView.minimumZoomScale = 1.0
         
            ////
            if self.objFeeds!.arrPhotoInfo.count > 1{
                //if indexPath.item == 0{
                if index == 0{
                    placeholderImage = UIImage.init(named:"gallery_placeholder_square")!
                    scrollVwEBPhotoView.imageView.contentMode = .scaleToFill
                }else{
                    placeholderImage = UIImage.init(named:"gallery_placeholder_square")!
                    scrollVwEBPhotoView.imageView.contentMode = .scaleAspectFit
                }
            }else{
                if self.objFeeds?.feedImageRatio == "0"{
                    placeholderImage = UIImage.init(named:"gallery_placeholder")!
                    scrollVwEBPhotoView.imageView.contentMode = .scaleToFill
                }else if self.objFeeds?.feedImageRatio == "1"{
                    placeholderImage = UIImage.init(named:"gallery_placeholder_square")!
                    scrollVwEBPhotoView.imageView.contentMode = .scaleToFill
                }else{
                    placeholderImage = UIImage.init(named:"gallery_placeholder_45")!
                    scrollVwEBPhotoView.imageView.contentMode = .scaleAspectFit
                }
            }
            ////
            
            if arrFeed[index].feedPost.count>0{
                
                //scrollVwEBPhotoView.imageView.af_setImage(withURL:URL(string:arrFeed[index].feedPost)!)
                
                if let urlImage = URL(string:arrFeed[index].feedPost){
                    //scrollVwEBPhotoView.imageView.af_setImage(withURL: urlImage, placeholderImage: UIImage(named:"gallery_placeholder"))
                    //scrollVwEBPhotoView.imageView.sd_setImage(with: urlImage, placeholderImage: UIImage(named: "gallery_placeholder"))
                    let processor = DownsamplingImageProcessor(size: scrollVwEBPhotoView.imageView.frame.size)
                        >> RoundCornerImageProcessor(cornerRadius: 0)
                    scrollVwEBPhotoView.imageView.kf.indicatorType = .activity
                    scrollVwEBPhotoView.imageView.kf.setImage(
                        with: urlImage,
                        placeholder: self.placeholderImage,
                        options: [
                            .processor(processor),
                            .scaleFactor(UIScreen.main.scale),
                            .transition(.fade(0.7)),
                            .cacheOriginalImage
                        ])
                    {
                        result in
                        switch result {
                        case .success(let value):
                            print("Task done for: \(value.source.url?.absoluteString ?? "")")
                            
                        case .failure(let error):
                            scrollVwEBPhotoView.imageView.sd_setImage(with: urlImage, placeholderImage:self.placeholderImage)
                            print("Job failed: \(error.localizedDescription)")
                        }
                    }
                }else{
                    //scrollVwEBPhotoView.imageView.image = UIImage(named:"gallery_placeholder")
                }
            }
            
            viewOuter.addSubview(scrollVwEBPhotoView)
            self.scrollView.addSubview(viewOuter)
            
            ////
            let btnShowTag = UIButton.init(frame: CGRect(x: 10, y: scrollVwEBPhotoView.frame.height-42, width: 32, height: 32))
            btnShowTag.isEnabled = true
            btnShowTag.isUserInteractionEnabled = true
            btnShowTag.setBackgroundImage(UIImage.init(named:"tag_img-1"), for: .normal)
            btnShowTag.addTarget(self, action:#selector(btnShowHideTags(sender:)) , for: .touchUpInside)
            
            if objFeeds.arrPhotoInfo[index].arrTags.count>0{
                btnShowTag.isHidden = false
                //self.scrollView.addSubview(btnShowTag)
                //self.scrollView.bringSubview(toFront: btnShowTag)
            }else{
                //btnShowTag.removeFromSuperview()
                btnShowTag.isHidden = true
            }
            ////
            
            if objFeeds.arrPhotoInfo.count > index{
                let objPhotoInfo = objFeeds.arrPhotoInfo[index]
                objPhotoInfo.objEBPhotoView = scrollVwEBPhotoView
            }
 
            if objFeeds.arrFeed.count > 1{
                self.pageControll.isHidden = false
                self.pageControllView.isHidden = false
                
            }
        }
        
        self.pageControllView.addSubview(self.scrollView)
        if objFeeds.arrFeed.count > 1{
            self.pageControll.isHidden = false
            self.pageControllView.isHidden = false
        }
        //self.pageControll.layoutIfNeeded()
        self.setTagOnImages(objFeeds:objFeeds)
    }
    
    func setDefaultDesign(){
        self.viewMore.isHidden = true
    }
    
    
    func setTagOnImages(objFeeds:feeds){


        guard objFeeds.arrPhotoInfo.count > 0 else{return}
        
        self.btnShowTagNew.isHidden = true
        
        for objPhotoInfo in objFeeds.arrPhotoInfo {
            if objPhotoInfo.arrTags.count > 0  {
                self.btnShowTagNew.isHidden = false
                var arrTagPopovers = [EBTagPopover]()
                
                for (index, objTagInfo) in objPhotoInfo.arrTags.enumerated() {

                    if objTagInfo.tagPosition.y>1.0{
                        UserDefaults.standard.set("down", forKey: "tagPosition")
                        UserDefaults.standard.synchronize()
                    }else{
                        UserDefaults.standard.set("top", forKey: "tagPosition")
                        UserDefaults.standard.synchronize()
                    }
                    
                    let tagPopover = EBTagPopover.init(tag: objTagInfo)
                    tagPopover?.strTagType  = "people"
                    
                    if objTagInfo.tagPosition.x>=0.87{
                        tagPopover?.strLeftOrRight = "right"
                    }else{
                        if objTagInfo.tagPosition.x<=0.05{
                            tagPopover?.strLeftOrRight = "left"
                        }else{
                            tagPopover?.strLeftOrRight = ""
                        }
                    }
                    
                    //// New
//                    tagPopover?.strTagType = "android"
//                    if (tagPopover?.strTagType == "android"){
//                        if objTagInfo.tagPosition.x<0.45 {
//                            objTagInfo.tagPosition.x = objTagInfo.tagPosition.x-0.05
//                        }else if objTagInfo.tagPosition.x>0.55 {
//                          objTagInfo.tagPosition.x = objTagInfo.tagPosition.x+0.05
//                        }else{
//                            tagPopover?.normalizedArrowPoint = objTagInfo.normalizedPosition()
//                        }
//                    }else{
//                        tagPopover?.normalizedArrowPoint = objTagInfo.normalizedPosition()
//                    }
                    ////
                   
                    tagPopover?.normalizedArrowPoint = objTagInfo.normalizedPosition()
                    tagPopover?.delegate = self
                    tagPopover?.alpha = 0
                    tagPopover?.tag = index
                    
                    if let tagPopover = tagPopover {
                        arrTagPopovers.append(tagPopover)
                    }
                }
                
                self.tagsHidden = true
                objPhotoInfo.arrTagPopovers = arrTagPopovers
                if arrTagPopovers.count > 0{
                    for tagPopover in arrTagPopovers{
                        objPhotoInfo.objEBPhotoView?.addSubview(tagPopover)
                  }
                }
            }
            
            if objPhotoInfo.arrServiceTags.count > 0  {
                self.btnShowTagNew.isHidden = false
                var arrTagPopovers = [EBTagPopover]()
                
                for (index, objTagInfo) in objPhotoInfo.arrServiceTags.enumerated() {

                    if objTagInfo.tagPosition.y>1.0{
                        UserDefaults.standard.set("down", forKey: "tagPosition")
                        UserDefaults.standard.synchronize()
                    }else{
                        UserDefaults.standard.set("top", forKey: "tagPosition")
                        UserDefaults.standard.synchronize()
                    }
                    let tagPopover = EBTagPopover.init(tag: objTagInfo)
                    
                    tagPopover?.strTagType  = "service"
                    //tagPopover?.backgroundColor = UIColor.green
                    //tagPopover?.setViewShadow(20.0)
                    tagPopover?.strTagType = "service"
                    //incallPrice outcallPrice
                    let strInCallPrice = objTagInfo.metaData["incallPrice"] as? String ?? ""
                    let strOutCallPrice = objTagInfo.metaData["outcallPrice"] as? String ?? ""
                    
                    /*if strInCallPrice.count == 0 || strInCallPrice == "0" || strInCallPrice == "0.0"{
                        tagPopover?.strPrice = strOutCallPrice
                    }else{
                        tagPopover?.strPrice = strInCallPrice
                    }
                    */
                    let isOutcall = objTagInfo.metaData["isOutCall"] as? String ?? ""
                    if isOutcall == "0"{
                        tagPopover?.strPrice = strInCallPrice
                    }else{
                        tagPopover?.strPrice = strOutCallPrice
                    }
                    if objTagInfo.tagPosition.y>0.80{
                        tagPopover?.strTopOrBottom = "bottom"
                    }else{
                        tagPopover?.strTopOrBottom = "top"
                    }
                    
                    
                    if objTagInfo.tagPosition.x>=0.87{
                        tagPopover?.strLeftOrRight = "right"
                    }else{
                        if objTagInfo.tagPosition.x<=0.05{
                            tagPopover?.strLeftOrRight = "left"
                        }else{
                            tagPopover?.strLeftOrRight = ""
                        }
                    }
                    tagPopover?.normalizedArrowPoint = objTagInfo.normalizedPosition()
                    tagPopover?.delegate = self
                    tagPopover?.alpha = 0
                    tagPopover?.tag = index
                    
                    if let tagPopover = tagPopover {
                        arrTagPopovers.append(tagPopover)
                    }

                }
                
                self.tagsHidden = true
                objPhotoInfo.arrServiceTagPopovers = arrTagPopovers
                if arrTagPopovers.count > 0{
                    for tagPopover in arrTagPopovers{
                        objPhotoInfo.objEBPhotoView?.addSubview(tagPopover)
                    }
                }
            }
        }
    }
    
    //MARK: - ScrollView Delegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageWidth: CGFloat = scrollView.frame.size.width
        pageControll.currentPage = (Int(scrollView.contentOffset.x) / Int(pageWidth))
        if self.objFeeds!.arrPhotoInfo[pageControll.currentPage].arrTags.count>0{
            self.btnShowTagNew.isHidden = false
        }else{
            self.btnShowTagNew.isHidden = true
        }
    }
    
    
}

// MARK: - Tag Hide/Show
extension feedsTableCell {
    
    func setTagsHidden(_ tagsHidden: Bool) {
        tagsHidden ? hideTags() : showTags()
    }
    
    func showTags() {
        setTagsAsHidden(false)
    }
    
    func hideTags() {
        setTagsAsHidden(true)
    }
    
    func setTagsAsHidden(_ hidden: Bool) {
        
        guard let objFeeds = self.objFeeds,  objFeeds.arrPhotoInfo.count > 0   else{return}
        tagsHidden = hidden
        let alpha: CGFloat = hidden ? 0 : 1
        UIView.animate(withDuration: 0.2, delay: 0, options: [.beginFromCurrentState, .curveEaseOut], animations: {
            
            for objPhotoInfo in objFeeds.arrPhotoInfo {
                for tag in objPhotoInfo.arrTagPopovers {
                    tag.alpha = alpha
                }
                for tag in objPhotoInfo.arrServiceTagPopovers {
                    tag.alpha = alpha
                }
            }
            
        }) { finished in
            
            for objPhotoInfo in objFeeds.arrPhotoInfo{
                for tag in objPhotoInfo.arrTagPopovers {
                    tag.alpha = alpha
                }
                for tag in objPhotoInfo.arrServiceTagPopovers {
                    tag.alpha = alpha
                    tag.backgroundColor = UIColor.clear
                 }
            }
        }
    }
}


// MARK: - Notification Actions
extension feedsTableCell {
    
    @objc func photoView(_ photoView: EBPhotoView!, didReceiveSingleTap singleTap: UITapGestureRecognizer!) {
        self.setTagsHidden(!tagsHidden)
        delegate.didReceiveSingleTapAt(indexPath: indexPath, feedsTableCell: self)
    }
    
    @objc func photoView(_ photoView: EBPhotoView!, didReceiveDoubleTap doubleTap: UITapGestureRecognizer!) {
        delegate.didReceiveDoubleTapAt(indexPath: indexPath, feedsTableCell: self)
    }
    
    @objc func photoView(_ photoView: EBPhotoView!, didReceiveLongPress longPress: UILongPressGestureRecognizer!) {
        //delegate.didReceiveLondPressAt(indexPath: indexPath, feedsTableCell: self)
        self.setTagsHidden(!tagsHidden)
    }
    
    //tag delegate
    func tagPopoverDidEndEditing(_ tagPopover: EBTagPopover!) {
        
    }
    
    func tagPopover(_ tagPopover: EBTagPopover!, didReceiveSingleTap singleTap: UITapGestureRecognizer!) {
        
        guard let objFeeds = self.objFeeds, pageControll.currentPage < objFeeds.arrPhotoInfo.count else {
            return
        }
        
        let objPhotoInfo = objFeeds.arrPhotoInfo[pageControll.currentPage]
        for (index, objTagPopover) in objPhotoInfo.arrTagPopovers.enumerated(){
            if tagPopover.text() == objTagPopover.text() {
                let objTagInfo = objPhotoInfo.arrTags[index]
                let dict = objTagInfo.metaData
                delegate.tagPopoverDidReceiveSingleTapWithInfo(dict: dict)
            }
        }
        for (index, objTagPopover) in objPhotoInfo.arrServiceTagPopovers.enumerated(){
            if tagPopover.text() == objTagPopover.text() {
                let objTagInfo = objPhotoInfo.arrServiceTags[index]
                let dict = objTagInfo.metaData
                delegate.tagPopoverDidReceiveSingleTapWithInfo(dict: dict)
            }
        }
    }
    
    @objc func btnShowHideTags(sender: UIButton!){
       self.setTagsHidden(!tagsHidden)
    }
    @objc func imageViewTapped(){
         self.setTagsHidden(!tagsHidden)
    }
}

class ExploreCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var vwBg: UIView!
    @IBOutlet weak var imgVwPost: UIImageView!
    @IBOutlet weak var imgVwPlay : UIImageView?
}

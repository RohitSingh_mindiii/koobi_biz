//
//  MyMenuVC.swift
//  Wedding
//
//  Created by mindiii on 11/16/17.
//  Copyright © 2017 mindiii. All rights reserved.
//

import UIKit
import Firebase


class MyMenuVC: UIViewController, UITableViewDelegate,UITableViewDataSource {
    
    // Fro Vendor
    var arrOptions:[String] = []
    var arrImgOptions:[UIImage] = []
    
    var ref: DatabaseReference!
    var messages: [DataSnapshot]! = []
    fileprivate var _refHandle: DatabaseHandle?
    
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var viewForSide: UIView!
    @IBOutlet weak var tableSide: UITableView!
    @IBOutlet weak var btnBack: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.ConfigureView()
    }
    
    @IBAction func btnBackAction(_ sender: Any){
        navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.updateArtistInfo()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.view.layoutIfNeeded()
    }
    
    func updateArtistInfo(){
        
        if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.userInfo){
            if let imgUrl = dict["profileImage"] as? String {
                if imgUrl != "" {
                    if let url = URL(string:imgUrl){
                        //self.imgProfile.af_setImage(withURL: url, placeholderImage: UIImage(named: "cellBackground"))
                        self.imgProfile.sd_setImage(with: url, placeholderImage:UIImage(named: "cellBackground"))
                    }
                }
            }
            if let userName = dict["userName"] as? String {
                self.lblUsername.text = userName
            }
        }else{
            let decoded = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] ?? ["":""]
            let img = decoded[UserDefaults.myDetail.profileImage] as? String ?? ""
            if img !=  "" {
                if let url = URL(string: img){
                    self.imgProfile.af_setImage(withURL: url, placeholderImage: UIImage(named: "cellBackground"))
                    self.imgProfile.sd_setImage(with: url, placeholderImage:UIImage(named: "cellBackground"))
                }
            }
            if let userName = decoded[UserDefaults.myDetail.userName] as? String {
                self.lblUsername.text = userName
            }
        }
    }
}

// MARK: - Custom Methods
extension MyMenuVC{
    
    func ConfigureView(){
        
        tableSide.delegate = self
        tableSide.dataSource = self
        
        //imgProfile.layer.cornerRadius = 53
        //imgProfile.layer.masksToBounds = true
        
        
        
        
//        arrOptions = ["Edit Profile",
//                      "Inbox",
//                      "Bookings",
//                      "Payment History",
//                      "Rate this app",
//                      "Payment Info",
//                      "Settings",
//                      "About Koobi",
//                      "Logout"]
        arrOptions = ["Edit Profile",
                      "Inbox",
                      "Booking History",
                      "Payment History",
                      //"Rate this app",
                      "My Folders",
                      "Payment Info",
                      "Booking Option",
                      "Settings",
                      //"About Koobi",
                      "Logout"]
        
        let img1 = #imageLiteral(resourceName: "edit-prfle")
        let img2 = #imageLiteral(resourceName: "inbox")
        let img3 = #imageLiteral(resourceName: "booking_ico")
        let img4 = #imageLiteral(resourceName: "payment-history")
        let img5 = #imageLiteral(resourceName: "rate-app")
        let img6 = #imageLiteral(resourceName: "folder_ico-1")
        let img7 = #imageLiteral(resourceName: "payment_info_ico")
        let img8 = #imageLiteral(resourceName: "booking_options_ico")
        let img9 = #imageLiteral(resourceName: "setting_ico")
        //let img10 = #imageLiteral(resourceName: "about")
        let img11 = #imageLiteral(resourceName: "logout")
        
        arrImgOptions = [img1,
                         img2,
                         img3,img4,
                         //img5,
                         img6,img7,img8,img9,
                         //img10,
                         img11]
        tableSide.reloadData()
    }
    
    func ProfileVC(){
        
    }
}

// MARK: - Tableview delegate methods
extension MyMenuVC{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOptions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cellIdentifier = "cellForSideIdentifier"
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? MyMenuCell{
            
            cell.lblOptions.text = arrOptions[indexPath.row]
            cell.imgOption.image = arrImgOptions[indexPath.row]
            
            //            cell.viewOption.backgroundColor = UIColor.clear
            //            cell.lblOptions.textColor = UIColor.white
            
            return cell
        }else{
            return UITableViewCell()
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        //   "Edit Profile", "Inbox", "Bookings","Payments", "Rate this app","Payment Info","About Koobi", "Logout"
        
        if let cell = tableView.cellForRow(at: indexPath) as? MyMenuCell {
            
            if cell.lblOptions.text == "Edit Profile" {
                //objAppShareData.showAlert(withMessage: "Under development", type: alertType.bannerDark, on: self)
                DoneEditProfile = false
                self.gotoTempVCWith(headerName : cell.lblOptions.text!)
            }
            else if cell.lblOptions.text == "Business Invitation"{
                let sb: UIStoryboard = UIStoryboard(name: "Invitation", bundle: Bundle.main)
                if let objVC = sb.instantiateViewController(withIdentifier:"InvitationListVC") as? InvitationListVC{
                    objVC.hidesBottomBarWhenPushed = true
                    navigationController?.pushViewController(objVC, animated: true)
                }
            }else if cell.lblOptions.text == "Inbox"{
                let sb: UIStoryboard = UIStoryboard(name: "Chat", bundle: Bundle.main)
                if let objVC = sb.instantiateViewController(withIdentifier:"ChatHistoryVC") as? ChatHistoryVC{
                    objVC.hidesBottomBarWhenPushed = true
                    navigationController?.pushViewController(objVC, animated: true)
                }
            }else if cell.lblOptions.text == "Booking History"{
                self.gotoBookingDetailModule()
            }
            else if cell.lblOptions.text == "Settings"{
                let sb: UIStoryboard = UIStoryboard(name: "Setting", bundle: Bundle.main)
                if let objVC = sb.instantiateViewController(withIdentifier:"SettingListVC") as? SettingListVC{
                    objVC.hidesBottomBarWhenPushed = true
                    navigationController?.pushViewController(objVC, animated: true)
                }
            }
            else if cell.lblOptions.text == "Payment History" {
                                let sb: UIStoryboard = UIStoryboard(name: "Payment", bundle: Bundle.main)
                                if let objVC = sb.instantiateViewController(withIdentifier:"PaymentHistoryVC") as? PaymentHistoryVC{
                                    objVC.hidesBottomBarWhenPushed = true
                                    navigationController?.pushViewController(objVC, animated: true)
                                }
            }
            else if cell.lblOptions.text == "Rate this app" {
                // self.gotoTempVCWith(headerName : cell.lblOptions.text!)
            }
            else if cell.lblOptions.text == "My Folders" {
                let sb: UIStoryboard = UIStoryboard(name: "Feed", bundle: Bundle.main)
                if let objVC = sb.instantiateViewController(withIdentifier:"SaveToFolderVC") as? SaveToFolderVC{
                    objAppShareData.btnAddHiddenONMyFolder = true
                    navigationController?.pushViewController(objVC, animated: true)
                }
            }
            else if cell.lblOptions.text == "Payment Info"{
                 self.gotoPaymentInfoVC()
            }
            else if cell.lblOptions.text == "Booking Option"{
                self.gotoBookingSettingVC()
            }
            
                
                
            else if cell.lblOptions.text == "About Koobi"{
                self.view.endEditing(true)
                let sb: UIStoryboard = UIStoryboard(name: "ArtistProfile", bundle: Bundle.main)
                if let objVC = sb.instantiateViewController(withIdentifier:"AboutKoobiVC") as? AboutKoobiVC {
                    // objVC.headerName = headerName
                    objVC.hidesBottomBarWhenPushed = true
                    navigationController?.pushViewController(objVC, animated: true)
                }
            }
            else if cell.lblOptions.text == "Logout"{
//                let myId = UserDefaults.standard.string(forKey: UserDefaults.keys.myId) ?? ""
//                let calendarDate = ServerValue.timestamp()
//                let dict = ["isOnline":0,
//                            "lastActivity":calendarDate] as [String : Any]
//                Database.database().reference().child("users").child(myId).updateChildValues(dict)
                appDelegate.logout()
            }else{
            }
        }
    }
    
    func gotoBookingDetailModule(){
        self.view.endEditing(true)
        let sb: UIStoryboard = UIStoryboard(name: "BookingDetailModule", bundle: Bundle.main)
       if let objVC = sb.instantiateViewController(withIdentifier:"PastFutureBookingVC") as? PastFutureBookingVC {
            objVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(objVC, animated: true)
        }
    }
    
    func gotoPaymentHistoryVC(){
        
        self.view.endEditing(true)
//        let sb: UIStoryboard = UIStoryboard(name: "PaymentModule", bundle: Bundle.main)
//        if let objVC = sb.instantiateViewController(withIdentifier:"PaymentHistoryVC") as? PaymentHistoryVC {
//            objAppShareData.isFromPaymentScreen = false
//            objVC.hidesBottomBarWhenPushed = true
//            navigationController?.pushViewController(objVC, animated: true)
//        }
    }
    
    func gotoPaymentInfoVC(){
        let sb = UIStoryboard(name:"PaymentModule",bundle:Bundle.main)
        let objChooseType = sb.instantiateViewController(withIdentifier:"MakePaymentVC") as! MakePaymentVC
        objChooseType.strBookingId = ""//.arrBookedMainService[0]._id
        self.navigationController?.pushViewController(objChooseType, animated: true)
        
        
        
        self.view.endEditing(true)
//        let sb = UIStoryboard(name:"SignUpProcess",bundle:Bundle.main)
//        let objChooseType = sb.instantiateViewController(withIdentifier:"AddBankDetailVC") as! AddBankDetailVC
//        objAppShareData.editSelfServices = true
//        objChooseType.hidesBottomBarWhenPushed = true
//        self.navigationController?.pushViewController(objChooseType, animated: false)
    }
    
    func gotoBookingSettingVC(){
        self.view.endEditing(true)
        let sb = UIStoryboard(name:"SignUpProcess",bundle:Bundle.main)
        let objChooseType = sb.instantiateViewController(withIdentifier:"BookingManageVC") as! BookingManageVC
        objAppShareData.editSelfServices = true
        objChooseType.fromSignUp = false
        objChooseType.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(objChooseType, animated: false)
    }
    
    
    func gotoTempVCWith(headerName : String){
        self.view.endEditing(true)
        let sb: UIStoryboard = UIStoryboard(name: "ArtistProfile", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"EditProfileVC") as? EditProfileVC {
            // objVC.headerName = headerName
            objVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(objVC, animated: true)
        }
    }
}







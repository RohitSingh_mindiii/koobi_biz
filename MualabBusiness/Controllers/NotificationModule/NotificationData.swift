//
//  NotificationData.swift
//  MualabCustomer
//
//  Created by Mac on 12/06/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import Foundation

class NotificationData {
    
    

    var _id : Int = 0
    var senderId : Int?
    var firstName : String = ""
    var lastName : String = ""
    var message : String = ""
    var messageAtt : NSMutableAttributedString = NSMutableAttributedString(string:"")
    var notifincationType : Int = 0
    var profileImage : String = ""
    var readStatus : String = ""
    var userName : String = ""
    var date : String = ""
    var type : String = ""
    var notifyId : Int?
    var userType : String = ""
    var strTimeAgo : String = ""

    init(dict : [String : Any]){
        _id = dict["_id"] as? Int ?? 0
       // senderId = dict["senderId"] as? Int ?? 0
        firstName = dict["firstName"] as? String ?? ""
        lastName = dict["lastName"] as? String ?? ""
        message = dict["message"] as? String ?? ""
        profileImage = dict["profileImage"] as? String ?? ""
        //readStatus = dict["readStatus"] as? Int ?? ""
        
        let strDate = dict["crd"] as? String ?? ""
        let dateNew = objAppShareData.getDateFromCRD(strDate: strDate)
        strTimeAgo = objAppShareData.timeAgoSinceDateForNotificationList(date: dateNew as NSDate, numericDates: false)
        
        if let status = dict["readStatus"] as? Int{
            readStatus = String(status)
        }else if let status = dict["readStatus"] as? String{
            readStatus = status
        }
        
        userName = dict["userName"] as? String ?? ""
        userType = dict["userType"] as? String ?? ""
        date = dict["timeElapsed"] as? String ?? ""
        type = dict["type"] as? String ?? ""
        
        if let notiId  = dict["notifyId"] as? Int{
            notifyId = notiId
        }else{
            if let notiId  = dict["notifyId"] as? String{
                notifyId = Int(notiId)
            }
        }
        
        if let notiId  = dict["senderId"] as? Int{
            senderId = notiId
        }else{
            if let notiId  = dict["senderId"] as? String{
                senderId = Int(notiId)
            }
        }
        
        if let notiType  = dict["notifincationType"] as? Int{
            notifincationType = notiType
        }else{
            if let notiType  = dict["notifincationType"] as? String{
                notifincationType = Int(notiType) ?? 0
            }
        }
    }
}

extension String {
    
    func highlightWordsIn(highlightedWords: String, attributes: [[NSAttributedString.Key: Any]]) -> NSMutableAttributedString {
        
        let result = NSMutableAttributedString(string: self)
        
        let strSentence = self.lowercased()
        let strWord = highlightedWords.lowercased()
        
        if strSentence.contains(strWord) {
            
            let range =  (strSentence as NSString).range(of: strWord)
            for attribute in attributes {
                result.addAttributes(attribute, range: range)
            }
        }
        
        return result
    }
}

class NewAddress {
//                   if city.count == 0{
//                       city = res.locality!
//                   }
//                   if country.count == 0{
//                       country = res.country!
//                   }
//                   if fAddress.count == 0{
//                    self.placeName = (res.lines?.first)!
//                    fAddress = (res.lines?.joined(separator: ", "))!
    var senderId : String = ""
    var firstName : String = ""
    var lastName : String = ""
    var message : String = ""
   
}

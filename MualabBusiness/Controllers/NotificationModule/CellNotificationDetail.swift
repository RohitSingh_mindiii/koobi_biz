//
//  CellNotificationDetail.swift
//  MualabCustomer
//
//  Created by Mac on 31/05/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

class CellNotificationDetail: UITableViewCell {

    @IBOutlet weak var imgVwOuter: UIImageView!
    @IBOutlet weak var imgVwProfile: UIImageView!
    @IBOutlet weak var btnProfile: UIButton!

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblMsg: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblDateTimeAgo: UILabel!

    var indexPath:IndexPath!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}

class CellSocialNotificationDetail: UITableViewCell {
    
    @IBOutlet weak var imgVwOuter: UIImageView!
    @IBOutlet weak var imgVwProfile: UIImageView!
    @IBOutlet weak var btnProfile: UIButton!

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblMsg: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblDateTimeAgo: UILabel!

    var indexPath:IndexPath!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}


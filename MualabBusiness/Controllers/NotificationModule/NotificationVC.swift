
//
//  BookingDetailVC.swift
//  MualabCustomer
//
//  Created by Mac on 04/06/18.
//  Copyright © 2018 Mindiii. All rights reserved.


import UIKit
import Firebase

class NotificationVC: UIViewController {
    var ref: DatabaseReference!
    var messages: [DataSnapshot]! = []
    fileprivate var _refHandle: DatabaseHandle?
    fileprivate var attributes : [[NSAttributedString.Key : Any]] = {
        //        [[NSAttributedStringKey.foregroundColor:UIColor.black],
        //         [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 15)]]
        [[NSAttributedString.Key.foregroundColor:UIColor.black],
         [NSAttributedString.Key.font: UIFont(name: "Nunito-SemiBold", size: 16)!]]
    }()
    
    @IBOutlet weak var tblMoreOption: UITableView!
    @IBOutlet weak var viewBottumTable: UIView!
    @IBOutlet weak var lblTableHeader: UILabel!
    @IBOutlet weak var HeightViewBottumTable: NSLayoutConstraint!
    @IBOutlet weak var btnOptionDots: UIButton!
    
    private let arrMoreOption = ["Clear All Completed"]
    
    @IBOutlet weak var viewChatbadgeCount: UIView!
    @IBOutlet weak var lblChatbadgeCount: UILabel!
    @IBOutlet weak var viewSocialbadgeCount: UIView!
    @IBOutlet weak var lblSocialbadgeCount: UILabel!
    @IBOutlet weak var viewBookingbadgeCount: UIView!
    @IBOutlet weak var lblBookingbadgeCount: UILabel!
    @IBOutlet weak var imgBooking: UIImageView!
    @IBOutlet weak var imgSocial: UIImageView!
    @IBOutlet weak var btnSocial: UIButton!
    @IBOutlet weak var btnBooking: UIButton!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var viewPastBooking: UIView!
    @IBOutlet weak var viewFutureBooking: UIView!
    @IBOutlet weak var segmentController: UISegmentedControl!
    @IBOutlet weak var tblPastBooking: UITableView!
    @IBOutlet weak var tblFutureBooking: UITableView!
    
    @IBOutlet weak var activity: UIActivityIndicatorView!
    @IBOutlet weak var lblNoRecord: UIView!
    
    var selectedIndexPath : IndexPath?
    var arrBookingNotificationData : [NotificationData] = []
    var arrSocialNotificationData : [NotificationData] = []
    var isLoading = false
    fileprivate var strUserId : String = ""
    fileprivate var isSocial = false
    fileprivate var isOnNotificationScreen = false
    fileprivate var pageNo: Int = 0
    fileprivate var totalCount = 0
    fileprivate var lastLoadedPage = 0
    
    fileprivate var pageNo2: Int = 0
    fileprivate var totalCount2 = 0
    fileprivate var lastLoadedPage2 = 0
    
    fileprivate let pageSize = 20 // that's up to you, really
    fileprivate let preloadMargin = 5 // or whatever number that makes sense with your page size
    
    fileprivate let reviewTextLimit = 150
    
    //MARK: - refreshControl
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(self.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1)
        return refreshControl
    }()
    
    lazy var refreshControl2: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(self.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1)//UIColor.theameColors.pinkColor
        return refreshControl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblMoreOption.delegate = self
        self.tblMoreOption.dataSource = self
        configureView()
        self.viewBottumTable.isHidden = true
        NotificationCenter.default.addObserver(self, selector: #selector(self.refreshChatBadgeCount), name: NSNotification.Name(rawValue: "refreshChatBadgeCount"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.refreshSocialBookingBadgeCount), name: NSNotification.Name(rawValue: "refreshSocialBookingBadgeCount"), object: nil)
        //self.getNotificationDataWith(0)
  
        self.addGesturesToView()

    
        segmentController.selectedSegmentIndex = 0
        self.viewPastBooking.isHidden = false
        self.viewFutureBooking.isHidden = true
        self.btnOptionDots.isHidden = true
        
        self.isSocial = true
        self.showSocialTabSelected()
    
    }
    @objc func refreshChatBadgeCount(_ objNotify: Notification){
//        if objChatShareData.chatBadgeCount == 0{
//            self.viewChatbadgeCount.isHidden = true
//        }else{
//            self.lblChatbadgeCount.text = String(objChatShareData.chatBadgeCount)
//            self.viewChatbadgeCount.isHidden = false
//        }
    }
    @objc func refreshSocialBookingBadgeCount(_ objNotify: Notification){
        if objAppShareData.badgeContSocialNotification == 0{
            self.viewSocialbadgeCount.isHidden = true
        }else{
            self.lblSocialbadgeCount.text = String(objAppShareData.badgeContSocialNotification)
            self.viewSocialbadgeCount.isHidden = false
        }
        if objAppShareData.badgeContBookingNotification == 0{
            self.viewBookingbadgeCount.isHidden = true
        }else{
            self.lblBookingbadgeCount.text = String(objAppShareData.badgeContBookingNotification)
            self.viewBookingbadgeCount.isHidden = false
        }
    }
    
    
    @IBAction func btnMoreOption(_ sender: UIButton) {
        self.viewBottumTable.isHidden = false
        self.HeightViewBottumTable.constant = CGFloat(self.arrMoreOption.count*50)
        self.tblMoreOption.reloadData()
    }
    @IBAction func btnMoreOptionHidden(_ sender: UIButton) {
        self.viewBottumTable.isHidden = true
    }
    @IBAction func btnLogout (_ sender:Any){
        appDelegate.logout()
    }
    override func viewDidDisappear(_ animated: Bool) {
        self.isOnNotificationScreen = false
    }
    override func viewWillAppear(_ animated: Bool) {
        self.isOnNotificationScreen = true
        self.viewSocialbadgeCount.isHidden = true
        self.viewBookingbadgeCount.isHidden = true
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshChatBadgeCount"), object: nil)
        self.lblNoRecord.isHidden = true
//        segmentController.selectedSegmentIndex = 0
//        self.viewPastBooking.isHidden = false
//        self.viewFutureBooking.isHidden = true
//
       self.updateArtistInfo()
//        self.isSocial = true
//        self.showSocialTabSelected()
        self.getNotificationDataWith(0)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        selectedIndexPath = nil
        self.getNotificationDataWith(0)
        //API Call
    }
}

//MARK: - custome method extension
extension NotificationVC {
    
    func configureView(){
        self.tblPastBooking.estimatedRowHeight = 50.0
        self.tblPastBooking.rowHeight = UITableView.automaticDimension
        self.tblFutureBooking.estimatedRowHeight = 50.0
        self.tblFutureBooking.rowHeight = UITableView.automaticDimension
        if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.businessInfoDic){
            if let userId = dict["_id"] as? String {
                self.strUserId = userId
            }
        }else{
        let userInfo = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] ?? ["":""]
            if let userId = userInfo["_id"] as? String {
                self.strUserId = userId
            }
        }
        
        ref = Database.database().reference()
        _refHandle = ref.child("businessBookingBadgeCount").child(self.strUserId).observe(.value, with: { [weak self] (snapshot) in
            guard let strongSelf = self else {
                return
            }
            let dict = snapshot.value as? [String:Any]
            print(dict)
            if let count = dict?["socialCount"] as? Int {
                objAppShareData.badgeContSocialNotification = count
            }else if let count = dict?["socialCount"] as? String {
                objAppShareData.badgeContSocialNotification = Int(count) ?? 0
            }
            if let count = dict?["bookingCount"] as? Int {
                objAppShareData.badgeContBookingNotification = count
            }else if let count = dict?["bookingCount"] as? String {
                objAppShareData.badgeContBookingNotification = Int(count) ?? 0
            }
            if strongSelf.isOnNotificationScreen {
                if strongSelf.isSocial{
                    strongSelf.ref.child("businessBookingBadgeCount").child(strongSelf.strUserId).updateChildValues(["socialCount":0])
                    objAppShareData.badgeContSocialNotification = 0
                }else{
                    strongSelf.ref.child("businessBookingBadgeCount").child(strongSelf.strUserId).updateChildValues(["bookingCount":0])
                    objAppShareData.badgeContBookingNotification = 0
                }
            }
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshSocialBookingBadgeCount"), object: nil)
        })
        
        //        outerView.layer.cornerRadius = 10
        //        outerView.layer.borderColor =             UIColor.theameColors.pinkColor.cgColor
        //        outerView.layer.borderWidth = 1
        //        outerView.clipsToBounds = true
        
        self.tblPastBooking.delegate = self
        self.tblPastBooking.dataSource = self
        self.tblFutureBooking.delegate = self
        self.tblFutureBooking.dataSource = self
        
        self.tblPastBooking.addSubview(self.refreshControl)
        self.tblFutureBooking.addSubview(self.refreshControl2)
        
    }
    
    func updateArtistInfo(){
        self.imgProfile.image = UIImage(named: "cellBackground")
        
        if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.businessInfoDic){
            if let imgUrl = dict["profileImage"] as? String {
                if imgUrl != "" {
                    if let url = URL(string:imgUrl){
                        //self.imgProfile.af_setImage(withURL: url, placeholderImage: UIImage(named: "cellBackground"))
                        self.imgProfile.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "cellBackground"))
                    }
                }
            }
        }else{
            let userInfo = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] ?? ["":""]
            if let imgUrl = userInfo["profileImage"] as? String {
                if imgUrl != "" {
                    if let url = URL(string:imgUrl){
                        //self.imgProfile.af_setImage(withURL: url, placeholderImage: #imageLiteral(resourceName: "cellBackground"))
                        self.imgProfile.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "cellBackground"))
                    }
                }
            }
        }
    }
}

//MARK: - btn Actions
extension NotificationVC {
    @IBAction func btnChatAction(_ sender: UIButton) {
//        let sb: UIStoryboard = UIStoryboard(name: "Chat", bundle: Bundle.main)
//        if let objVC = sb.instantiateViewController(withIdentifier:"ChatHistoryVC") as? ChatHistoryVC{
//            objVC.hidesBottomBarWhenPushed = true
//            navigationController?.pushViewController(objVC, animated: true)
//        }
    }
    @IBAction func btnBackAction(_ sender: Any){
        navigationController?.popViewController(animated: true)
    }
    
    //    @IBAction func btnProfileAction(_ sender: Any) {
    //        self.view.endEditing(true)
    //        objAppShareData.showAlert(withMessage: "Under Development", type: alertType.bannerDark, on: self)
    //    }
    
    @IBAction func btnProfileAction(_ sender: Any) {
        self.view.endEditing(true)
        objAppShareData.isOtherSelectedForProfile = false
        self.gotoProfileVC()
    }
    
    
    @IBAction func segmentButtonSocialOrBooking(_ sender: UIButton) {
        self.changeSegment(sender: sender.tag)
    }
    
    func changeSegment(sender:Int){
        if self.isLoading{
            return
        }
        
        if sender == 0 {
            if self.isSocial{
                return
            }
            self.btnOptionDots.isHidden = true
            showSocialTabSelected()
            isSocial = true
            //viewPastBooking.isHidden = false
            //viewFutureBooking.isHidden = true
            self.lblNoRecord.isHidden = true
            //if arrSocialNotificationData.count == 0{
            self.getNotificationDataWith(0)
            //}
            
        }else if sender == 1{
            if !isSocial{
                return
            }
            self.btnOptionDots.isHidden = false
            showBookingTabSelected()
            isSocial = false
            //viewPastBooking.isHidden = true
            //viewFutureBooking.isHidden = false
            self.lblNoRecord.isHidden = true
            //if arrFutureBookingData.count == 0{
            self.getNotificationDataWith(0)
            //}
        }
    }
    func showBookingTabSelected() {
        self.imgSocial.backgroundColor = UIColor.lightGray
        self.imgBooking.backgroundColor = UIColor.theameColors.skyBlueNewTheam
        self.btnSocial.setTitleColor(UIColor.lightGray, for: .normal)
        self.btnBooking.setTitleColor(UIColor.theameColors.skyBlueNewTheam, for: .normal)
        self.ref.child("businessBookingBadgeCount").child(self.strUserId).updateChildValues(["bookingCount":0])
    }
    
    func showSocialTabSelected() {
        self.imgBooking.backgroundColor = UIColor.lightGray
        self.imgSocial.backgroundColor = UIColor.theameColors.skyBlueNewTheam
        self.btnSocial.setTitleColor(UIColor.theameColors.skyBlueNewTheam, for: .normal)
        self.btnBooking.setTitleColor(UIColor.lightGray, for: .normal)
        self.ref.child("businessBookingBadgeCount").child(self.strUserId).updateChildValues(["socialCount":0])
    }
    
    @IBAction func segmentPastFuture(_ sender: UISegmentedControl) {
        
        //        switch segmentController.selectedSegmentIndex {
        //        case 0:
        //            viewPastBooking.isHidden = false
        //            viewFutureBooking.isHidden = true
        //            self.lblNoRecord.isHidden = true
        //           // self.lblHeader.text = "Past Booking"
        //            if arrBookingNotificationData.count == 0{
        //                self.getNotificationDataWith(0)
        //            }
        //
        //        case 1:
        //            viewPastBooking.isHidden = true
        //            viewFutureBooking.isHidden = false
        //            self.lblNoRecord.isHidden = true
        //           // self.lblHeader.text = "Future Booking"
        //            if arrSocialNotificationData.count == 0{
        //                self.getNotificationDataWith(0)
        //            }
        //
        //        default:
        //            break;
        //        }
    }
}

// MARK: - table view Delegate and Datasource
extension NotificationVC : UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - Tableview delegate methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblMoreOption{
            return self.arrMoreOption.count
        }else{
        if self.isSocial{
            return arrSocialNotificationData.count
        }else{
            return arrBookingNotificationData.count
        }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if tableView == tblMoreOption{
            let cellIdentifier = "CellBottumTableList"
            if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? CellBottumTableList{
                     let obj = self.arrMoreOption[indexPath.row]
                    cell.lblTitle.text = obj
                return cell
            }
              return UITableViewCell()
        }else{
        
        if self.isLoading{
            return UITableViewCell()
        }
        
        if tableView == tblPastBooking {
            if self.isLoading{
                return UITableViewCell()
            }
            
            if !self.isSocial{
                if self.arrBookingNotificationData.count < self.totalCount {
                    
                    let nextPage: Int = Int(indexPath.item / pageSize) + 1
                    let preloadIndex = nextPage * pageSize - preloadMargin
                    
                    // trigger the preload when you reach a certain point AND prevent multiple loads and updates
                    if (indexPath.item >= preloadIndex && lastLoadedPage < nextPage) {
                        self.getNotificationDataWith(nextPage)
                    }
                }
            }else{
                if self.arrSocialNotificationData.count < self.totalCount2 {
                    
                    let nextPage: Int = Int(indexPath.item / pageSize) + 1
                    let preloadIndex = nextPage * pageSize - preloadMargin
                    
                    // trigger the preload when you reach a certain point AND prevent multiple loads and updates
                    if (indexPath.item >= preloadIndex && lastLoadedPage2 < nextPage) {
                        self.getNotificationDataWith(nextPage)
                    }
                }
            }
            
        }else{
            if self.isLoading{
                return UITableViewCell()
            }
            if self.arrSocialNotificationData.count < self.totalCount2 {
                
                let nextPage: Int = Int(indexPath.item / pageSize) + 1
                let preloadIndex = nextPage * pageSize - preloadMargin
                
                // trigger the preload when you reach a certain point AND prevent multiple loads and updates
                if (indexPath.item >= preloadIndex && lastLoadedPage2 < nextPage) {
                    self.getNotificationDataWith(nextPage)
                }
            }
        }
        
        var obj : NotificationData = NotificationData.init(dict: ["":""])
        if self.isSocial{
            obj = arrSocialNotificationData[indexPath.row]
        }else{
            obj = arrBookingNotificationData[indexPath.row]
        }
        
        if obj.type == "booking" {
            
            if let cell : CellNotificationDetail = tableView.dequeueReusableCell(withIdentifier: "CellNotificationDetail")! as? CellNotificationDetail {
                
                //if let cell : CellSocialNotificationDetail = tableView.dequeueReusableCell(withIdentifier: "CellSocialNotificationDetail")! as? CellSocialNotificationDetail {
                
                cell.lblName.text  = obj.userName
                cell.lblMsg.attributedText  = obj.messageAtt  //obj.message
                cell.lblTime.text  = obj.date
                
                let strImg = obj.profileImage
                if strImg != "" {
                    if let url = URL(string: strImg){
                        //cell.imgVwProfile.af_setImage(withURL: url, placeholderImage: UIImage(named: "cellBackground"))
                        cell.imgVwProfile.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "cellBackground"))
                    }else{
                        cell.imgVwProfile.image = UIImage(named: "cellBackground")
                    }
                }else{
                    cell.imgVwProfile.image = UIImage(named: "cellBackground")
                }
                
                cell.lblDateTimeAgo.text = obj.strTimeAgo
                if indexPath.row == 0{
                    cell.lblDateTimeAgo.isHidden = false
                }else{
                    let indexOldData = arrBookingNotificationData[indexPath.row-1].strTimeAgo
                    if indexOldData == obj.strTimeAgo{
                        cell.lblDateTimeAgo.isHidden = true
                    }else{
                        cell.lblDateTimeAgo.isHidden = false
                    }
                }
                
                cell.lblDateTimeAgo.text  = obj.strTimeAgo
                if indexPath.row == 0{
                    cell.lblDateTimeAgo.isHidden = false
                }else{
                    if tableView == tblPastBooking{
                        let indexOldData = arrBookingNotificationData[indexPath.row-1].strTimeAgo
                        if indexPath.row == 0{
                            cell.lblDateTimeAgo.isHidden = false
                        }else{
                            if indexOldData == obj.strTimeAgo{
                                cell.lblDateTimeAgo.isHidden = true
                            }else{
                                cell.lblDateTimeAgo.isHidden = false
                            }
                        }
                    }else{
                        let indexOldData = arrSocialNotificationData[indexPath.row-1].strTimeAgo
                        if indexPath.row == 0{
                            cell.lblDateTimeAgo.isHidden = false
                        }else{
                            if indexOldData == obj.strTimeAgo{
                                cell.lblDateTimeAgo.isHidden = true
                            }else{
                                cell.lblDateTimeAgo.isHidden = false
                            }
                        }
                    }
                }
                cell.btnProfile.accessibilityHint = obj.userName
                cell.btnProfile.tag = indexPath.row
                cell.btnProfile.superview?.tag = indexPath.section
                return cell
            }else{
                return UITableViewCell()
            }
        }else{
            
            if let cell : CellSocialNotificationDetail = tableView.dequeueReusableCell(withIdentifier: "CellSocialNotificationDetail")! as? CellSocialNotificationDetail {
                
                cell.lblName.text  = obj.userName
                cell.lblMsg.attributedText = obj.messageAtt  //obj.message
                cell.lblTime.text = obj.date
                
                let strImg = obj.profileImage
                if strImg != "" {
                    if let url = URL(string: strImg){
                        //cell.imgVwProfile.af_setImage(withURL: url, placeholderImage: UIImage(named: "cellBackground"))
                        cell.imgVwProfile.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "cellBackground"))
                    }else{
                        cell.imgVwProfile.image = UIImage(named: "cellBackground")
                    }
                }else{
                    cell.imgVwProfile.image = UIImage(named: "cellBackground")
                }
                
                cell.lblDateTimeAgo.text  = obj.strTimeAgo
                if indexPath.row == 0{
                    cell.lblDateTimeAgo.isHidden = false
                }else{
                    if tableView == tblPastBooking{
                        let indexOldData = arrSocialNotificationData[indexPath.row-1].strTimeAgo
                        if indexPath.row == 0{
                            cell.lblDateTimeAgo.isHidden = false
                        }else{
                            if indexOldData == obj.strTimeAgo{
                                cell.lblDateTimeAgo.isHidden = true
                            }else{
                                cell.lblDateTimeAgo.isHidden = false
                            }
                        }
                    }else{
                        let indexOldData = arrSocialNotificationData[indexPath.row-1].strTimeAgo
                        if indexPath.row == 0{
                            cell.lblDateTimeAgo.isHidden = false
                        }else{
                            if indexOldData == obj.strTimeAgo{
                                cell.lblDateTimeAgo.isHidden = true
                            }else{
                                cell.lblDateTimeAgo.isHidden = false
                            }
                        }
                    }
                }
                cell.btnProfile.accessibilityHint = obj.userName
                cell.btnProfile.tag = indexPath.row
                cell.btnProfile.superview?.tag = indexPath.section
                return cell
            }else{
                return UITableViewCell()
            }
        }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        if tableView == tblMoreOption{
            self.viewBottumTable.isHidden = true
            self.webServiceForEditFolder()
        }else{
        if isLoading{
            return
        }
        var obj : NotificationData = NotificationData.init(dict: ["":""])
        if self.isSocial{
            obj = arrSocialNotificationData[indexPath.row]
        }else{
            obj = arrBookingNotificationData[indexPath.row]
        }
        self.redirectNotificationWith(obj: obj)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}


//MARK: - custome method extension
extension NotificationVC {
    
    func getNotificationDataWith(_ page: Int) {
        
        var parameters = [String:Any]()
        
        //var strBookingType : String = ""
        
        if self.segmentController.selectedSegmentIndex == 0 {
            self.lastLoadedPage = page
            self.pageNo = page
            // strBookingType = "booking"
        }else{
            self.lastLoadedPage2 = page
            self.pageNo2 = page
            //strBookingType = "social"
        }
        
        parameters = [
            "userId":strUserId,
            "page":page,
            //"limit":self.pageSize,
            "limit":"5000",
            //"type": strBookingType
        ]
        self.webServiceCall_getBookingData(parameters: parameters)
    }
    
    
    func webServiceCall_getBookingData(parameters : [String:Any]){
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        
        if self.segmentController.selectedSegmentIndex == 0 {
            
            if  !self.refreshControl.isRefreshing && self.pageNo == 0 {
                self.activity.startAnimating()
            }
            
        }else{
            
            if  !self.refreshControl2.isRefreshing && self.pageNo2 == 0 {
                self.activity.startAnimating()
            }
        }
        isLoading = true
        self.lblNoRecord.isHidden = true
        objWebserviceManager.requestPost(strURL:  WebURL.getNotificationList, params: parameters as [String : AnyObject] , success: { response in
            print(response)
            self.activity.stopAnimating()
            self.refreshControl.endRefreshing()
            self.refreshControl2.endRefreshing()
            self.isLoading = false
            if self.segmentController.selectedSegmentIndex == 0 {
                
                if self.pageNo == 0 {
                    self.arrBookingNotificationData.removeAll()
                    self.arrSocialNotificationData.removeAll()
                    self.tblPastBooking.reloadData()
                }
            }else{
                
                if self.pageNo2 == 0 {
                    self.arrSocialNotificationData.removeAll()
                    self.tblFutureBooking.reloadData()
                }
            }
            
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else{
                let strStatus =  response["status"] as? String ?? ""
                
                if strStatus == k_success{
                    
                    if self.segmentController.selectedSegmentIndex == 0 {
                        self.arrSocialNotificationData.removeAll()
                        self.arrBookingNotificationData.removeAll()
                        if let totalCount = response["total"] as? Int{
                            self.totalCount = totalCount
                        }
                        if let arr = response["notificationList"] as? [[String : Any]]{
                            for dict in arr{
                                let obj = NotificationData.init(dict: dict)
                                //obj.date =  self.getFormattedBookingDateToShowFrom(strDate: obj.date)
                                obj.messageAtt = obj.message.highlightWordsIn(highlightedWords: obj.userName, attributes:  self.attributes)
                                
                                if self.isSocial{
                                    if obj.type == "social"{
                                        self.arrSocialNotificationData.append(obj)
                                    }
                                }else{
                                    if obj.type == "booking"{
                                        self.arrBookingNotificationData.append(obj)
                                    }
                                }
                            }
                        }
                        if self.isSocial{
                            self.totalCount =  self.arrSocialNotificationData.count
                        }else{
                            self.totalCount = self.arrBookingNotificationData.count
                        }
                    }else{
                        
                        if let totalCount = response["total"] as? Int{
                            self.totalCount2 = totalCount
                        }
                        if let arr = response["notificationList"] as? [[String : Any]]{
                            for dict in arr{
                                let obj = NotificationData.init(dict: dict)
                                //obj.date =  self.getFormattedBookingDateToShowFrom(strDate: obj.date)
                                obj.messageAtt = obj.message.highlightWordsIn(highlightedWords: obj.userName, attributes:  self.attributes)
                                self.arrSocialNotificationData.append(obj)
                            }
                        }
                    }
                }else{
                    
                    if strStatus == "fail"{
                        
                    }else{
                        
                        if let msg = response["message"] as? String{
                            objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                        }
                    }
                }
            }
            
            if self.segmentController.selectedSegmentIndex == 0 {
                self.tblPastBooking.reloadData()

                self.tblPastBooking.contentOffset.y = 0.0
                if self.isSocial{
                    if self.arrSocialNotificationData.count == 0 {
                        self.lblNoRecord.isHidden = false
                    }else{
                        self.tblPastBooking.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
                        self.lblNoRecord.isHidden = true
                    }
                }else{
                    if self.arrBookingNotificationData.count == 0 {
                        self.lblNoRecord.isHidden = false
                    }else{
                        self.tblPastBooking.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
                        self.lblNoRecord.isHidden = true
                    }
                }
            }else{
                self.tblFutureBooking.reloadData()
                if self.arrSocialNotificationData.count == 0 {
                    self.lblNoRecord.isHidden = false
                }else{
                    self.lblNoRecord.isHidden = true
                    self.tblFutureBooking.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
                }
            }
            
        }) { (error) in
            self.isLoading = false
            self.activity.stopAnimating()
            self.refreshControl.endRefreshing()
            self.refreshControl2.endRefreshing()
            
            if self.segmentController.selectedSegmentIndex == 0 {
                self.tblPastBooking.reloadData()
                if self.arrBookingNotificationData.count == 0 {
                    self.lblNoRecord.isHidden = false
                }else{
                    self.lblNoRecord.isHidden = true
                }
            }else{
                self.tblFutureBooking.reloadData()
                if self.arrSocialNotificationData.count == 0 {
                    self.lblNoRecord.isHidden = false
                }else{
                    self.lblNoRecord.isHidden = true
                }
            }
            
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
}

//MARK: - custome method extension
extension NotificationVC {
    
    func getFormattedBookingDateToShowFrom(strDate : String) -> String{
        
        if strDate == "" {
            return ""
        }
        
        let formatter  = DateFormatter()
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        formatter.dateFormat = "yyyy-MM-dd"
        if let date = formatter.date(from: strDate){
            formatter.dateFormat = "dd/MM/yyyy" //"dd MMMM yyyy"
            let strDate = formatter.string(from: date)
            return strDate
        }
        return ""
    }
    
    
    
}

extension NotificationVC  {
    
    
    
    func redirectNotificationWith(obj : NotificationData) {
        
        var notifincationType : String = ""
        
        switch obj.notifincationType {
            
        case 1:
            //var body = 'sent a booking request.';
            //var title = 'Booking request';
            notifincationType = "booking" //Artist side
            break;
            
        case 2:
            //var body = 'accepted your booking request.'
            //var title = 'Booking accept';
            notifincationType = "booking" //->Checked
            break;
            
        case 3:
            //var body = 'rejected your booking request.';
            //var title = 'Booking reject';
            notifincationType = "booking" //->Checked
            break;
            
        case 4:
            //var body = 'cancelled your booking request.';
            //var title = 'Booking cancel';
            notifincationType = "booking" //->Checked
            break;
            
        case 5:
            //var body = 'completed your booking request.';
            //var title = 'Booking complete';
            notifincationType = "booking"
            break;
            
        case 6:
            //var body = 'given review for booking.';
            //var title = 'Booking Review';
            notifincationType = "booking"
            break;
            
        case 7:
            //var body = 'added a new post.';
            //var title = 'new post';
            notifincationType = "feed" //->Checked
            break;
            
        case 8:
            //var body = 'Payment has completed by';
            //var title = 'Payment';
            notifincationType = "booking" //Artist side //on back open payment history
            break;
            
        case 9:
            //var body = 'commented on your post.';
            //var title = 'Comment';
            notifincationType = "feed" //->Checked
            break;
            
        case 10:
            //var body = 'likes your post.';
            //var title = 'Post like';
            notifincationType = "feed" //->Checked
            break;
            
        case 11:
            //var body = 'likes your comment.';
            //var title = 'Comment like';
            notifincationType = "feed" //->Checked
            break;
            
        case 12:
            //var body = 'started following you.';
            //var title = 'Following';
            notifincationType = "profile" //->Checked
            break;
            
        case 13:
            //var body = 'added to their story.';
            //var title = 'Story';
            notifincationType = "story" // open story //->Checked
            break;
            
        case 14:
            //var body = 'added you as a favorite.';
            //var title = 'Story';
            obj.notifyId = obj.senderId
            notifincationType = "profile" //Artist side
            break;
            
        case 15:
            //var body = 'chat.';
            //var title = 'chat';
            notifincationType = "chat"
            break;
            
        case 16:
            //var body = 'tagged you in a post.';
            //var title = 'Tagged';
            notifincationType = "feed" //->Checked
            break;
        case 18:
            //var body = 'tagged you in a post.';
            //var title = 'Tagged';
            notifincationType = "invitation" //->Checked
            break;
            
        default:
            print("default : No condition matched")
        }
        
        
        //notificationType : "booking","feed","profile","chat","story"
        guard notifincationType != ""  else {
            return
        }
        
        
        if notifincationType == "booking" {
            
            let objPastFutureBookingData  : PastFutureBookingData = PastFutureBookingData(dict: ["":""])
            
            if let notifyId = obj.notifyId {
                objPastFutureBookingData._id = notifyId
                objPastFutureBookingData.userName = obj.userName
                objPastFutureBookingData.profileImage = obj.profileImage
                self.gotoAppoitmentBookingVC(obj: objPastFutureBookingData)
            }
            
        }else if notifincationType == "feed" {
            objAppShareData.notificationInfoDict?.removeAll()
            if let notifyId = obj.notifyId {
                objAppShareData.isFromNotification = true
                let dict = ["notifyId":notifyId]
                objAppShareData.notificationInfoDict = dict
                self.gotoExpPostDetailVC(objFeeds:dict)
            }
            
        }else if notifincationType == "story" {
 
        }else if notifincationType == "invitation" {
            self.gotoStaffList()
       }else if notifincationType == "profile" {
            
            if let notifyId  = obj.notifyId {
                objAppShareData.selectedOtherIdForProfile  = String(notifyId)
                objAppShareData.isOtherSelectedForProfile = true
                objAppShareData.selectedOtherTypeForProfile = obj.userType
                self.gotoProfileVC()
                return
            }
        }
        
    }
    
   
    func gotoAppoitmentBookingVC(obj : PastFutureBookingData) {
        self.view.endEditing(true)
        let sb: UIStoryboard = UIStoryboard(name: "Booking", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"ConfirmBookingVC") as? ConfirmBookingVC {
            objVC.strBookingId = String(obj._id)
            objVC.hidesBottomBarWhenPushed = true
            if objAppShareData.isFromNotification {
                navigationController?.pushViewController(objVC, animated: false)
            }else{
                navigationController?.pushViewController(objVC, animated: true)
            }
        }
    }
    
    func gotoStaffList() {
        self.view.endEditing(true)
        let sb: UIStoryboard = UIStoryboard(name: "StaffList", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"UserListAddStaffVC") as? UserListAddStaffVC {
            navigationController?.pushViewController(objVC, animated: false)
            }
        }
    
    
    func gotoExpPostDetailVC(objFeeds:[String:Any]){
        
        let sb: UIStoryboard = UIStoryboard(name: "ArtistProfile", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"FeedDetailVC") as? FeedDetailVC{
            objAppShareData.arrFeedsForArtistData.removeAll()
                    objVC.feedId =  String(objFeeds["notifyId"] as? Int ?? 0)
                    objVC.headerTitle = "Post"
            objVC.hidesBottomBarWhenPushed = true
            
              //  objAppShareData.arrFeedsForArtistData.append(objFeeds)
                navigationController?.pushViewController(objVC, animated: true)
           
        }
  
        
        
    }
    
    
    
    func gotoProfileVC (){
        self.view.endEditing(true)
        let sb: UIStoryboard = UIStoryboard(name: "ArtistProfile", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"SWRevealViewController") as? SWRevealViewController{
            objVC.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(objVC, animated: true)
        }
    }
    
    func recall() {
    }
    
    @IBAction func btnProfileListAction(_ sender: UIButton){
        //let section = 0
        //let row = (sender as AnyObject).tag
        //let indexPath = IndexPath(row: row!, section: section)
        //let objUser = arrLikeUsersList[indexPath.row]
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        let dicParam = ["userName":(sender.accessibilityHint)!]
        objServiceManager.requestPost(strURL: WebURL.profileByUserName, params: dicParam, success: { response in
            if response["status"] as? String ?? "" == "success"{
                var strId = ""
                var strType = ""
                if let dictUser = response["userDetail"] as? [String : Any]{
                    let myId = dictUser["_id"] as? Int ?? 0
                    strId = String(myId)
                    strType = dictUser["userType"] as? String ?? ""
                }
                let dic = [
                    "tabType" : "people",
                    "tagId": strId,
                    "userType":strType,
                    "title": sender.accessibilityHint ?? ""
                    ] as [String : Any]
                self.openProfileForSelectedTagPopoverWithInfo(dict: dic)
            }
        }) { error in
        }
    }
    func openProfileForSelectedTagPopoverWithInfo(dict : [AnyHashable: Any]){
        
        var dictTemp : [AnyHashable : Any]?
        
        dictTemp = dict
        
        if let dict1 = dict as? [String:[String :Any]] {
            if let dict2 = dict1.first?.value {
                dictTemp = dict2
            }
        }
        
        guard let dictFinal = dictTemp as? [String : Any] else { return }
        
        var strUserType: String?
        var tagId : Int?
        
        if let userType = dictFinal["userType"] as? String{
            strUserType = userType
            
            if let idTag = dictFinal["tagId"] as? Int{
                tagId = idTag
            }else{
                if let idTag = dictFinal["tagId"] as? String{
                    tagId = Int(idTag)
                }
            }
            
            var myId = ""
            if let dicUser = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] {
                myId = dicUser["_id"] as? String ?? ""
            }
            if myId == String(tagId ?? 0) ?? "" {
                //isNavigate = true
                objAppShareData.isOtherSelectedForProfile = false
                self.gotoProfileVC()
                return
            }
            
            if let strUserType = strUserType, let tagId =  tagId {
                
                objAppShareData.selectedOtherIdForProfile  = String(tagId)
                objAppShareData.isOtherSelectedForProfile = true
                objAppShareData.selectedOtherTypeForProfile = strUserType  //"artist" or "user"
                //isNavigate = true
                self.gotoProfileVC()
            }
        }
    }
    
}




//MARK: - Up down Swipe
fileprivate extension NotificationVC{
    
    
    @IBAction func btnBack(_ sender: UIButton) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func addGesturesToView() -> Void {
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.leftToRight))
        swipeRight.direction = .right
        self.viewPastBooking.addGestureRecognizer(swipeRight)
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.rightToLeft))
        swipeLeft.direction = .left
        self.viewPastBooking.addGestureRecognizer(swipeLeft)
    }
    
            @objc func leftToRight() ->Void {
                changeSegment(sender: 0)
            }
            @objc func rightToLeft() ->Void {
                changeSegment(sender: 1)
            }
}

//MARK: - delete completed notifications
extension NotificationVC{
    
    func webServiceForEditFolder(){
        if !objServiceManager.isNetworkAvailable(){
            return
        }
        
 
        objWebserviceManager.StartIndicator()
        let parameters : Dictionary = [
            "userId" : self.strUserId,
            "userType":"business"] as [String : Any]
        
        objWebserviceManager.requestPost(strURL: WebURL.notificationClear, params: parameters  , success: { response in
            
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                objWebserviceManager.StopIndicator()
            }else{
                
                let strStatus =  response["status"] as? String ?? ""
                if strStatus == k_success{
                    if let msg = response["message"] as? String{
                      //  objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                    }
                    self.viewWillAppear(true)
                }else{
                    objWebserviceManager.StopIndicator()
                    if let msg = response["message"] as? String{
                        objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                    }
                    
                }
            }
        }){ error in
            objWebserviceManager.StopIndicator()
            showAlertVC(title: kAlertTitle, message: kErrorMessage, controller: self)
        }
    }
}

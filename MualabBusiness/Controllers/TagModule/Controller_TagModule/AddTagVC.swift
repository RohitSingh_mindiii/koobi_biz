//
//  AddTagVC.swift
//  MualabCustomer
//
//  Created by Mac on 06/07/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

protocol AddTagVCDelegate {
    func finishTagSelectionWithData(arrFeedImagesWithTagInfo: [PhotoInfo])
    func finishServiceTagSelectionWithData(arrFeedImagesWithTagInfo: [PhotoInfo])
}

class AddTagVC: UIViewController, UIScrollViewDelegate, EBPhotoViewDelegate {
    
    @IBOutlet weak var tagListView: TagListView!
    @IBOutlet weak var heightFram: NSLayoutConstraint!

    private(set) var tagsHidden = false
    
    var arrFeedImages = [PhotoInfo]()
    var delegate: AddTagVCDelegate?
    var arrServiceTageCategoryId = [[String:Any]]()
    fileprivate var scrollSize: CGFloat = 300
    var strHeaderName = ""
    var arrSelectedImages = [UIImage]()
    var isTypeIsUrl = false
    var forServiceTag = false
    //var arrSelectedTag = [String]()
    
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var scrollView : UIScrollView!
    @IBOutlet weak var lblHeader : UILabel!
    @IBOutlet weak var lblSuggestion : UILabel!
    @IBOutlet weak var pageView: UIView!
    
    var selectedNotification: Notification?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.arrFeedImages.count > 1{
            //// Himanshu new change
            self.heightFram.constant = self.view.frame.width
            //self.heightFram.constant = self.view.frame.width*0.75
        }else{
            
            if self.arrFeedImages.count == 1{
                if imageRatioDynamic == 0{
                    self.heightFram.constant = self.view.frame.width*0.75
                }else if imageRatioDynamic == 1{
                    self.heightFram.constant = self.view.frame.width*1
                }else if imageRatioDynamic == 2{
                    self.heightFram.constant = self.view.frame.width*1.25
                }else{
                    self.heightFram.constant = self.view.frame.width*0.75
                }
            }else{
                //self.heightFram.constant = self.view.frame.width*0.75
                self.heightFram.constant = self.view.frame.width
            }
        }
        if forServiceTag == true{
            self.lblHeader.text = "Tag Booking"
            self.lblSuggestion.text = "Tap the selected area of image to Tag Services."
        }else {
            self.lblSuggestion.text = "Tap the selected area of image to Tag People."
            self.lblHeader.text = "Tag People"
        }
        self.configureView()
        self.beginObservations()
        self.configureTagListView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UserDefaults.standard.setValue("yes", forKey: "isTakeTagTouch")
        UserDefaults.standard.synchronize()
        if strHeaderName != "" {
            self.lblHeader.text = strHeaderName
        }
        if forServiceTag == true{
            //self.lblHeader.text = "Tag Bookings"
            self.lblHeader.text = "Tag Booking"
        }else {
            self.lblHeader.text = "Tag People"
        }
        self.showTagListTagAccordingToSelectedImage()
        
        scrollView.backgroundColor = UIColor.white
        let deadlineTime = DispatchTime.now() + .seconds(1/4)
        
        DispatchQueue.main.asyncAfter(deadline: deadlineTime){
            self.scrollSize = self.view.frame.size.width
            self.setPageControll()
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    deinit {
        stopObservations()
    }
   
    func configureView(){
        
        self.scrollView.maximumZoomScale = 1
        
        self.scrollView.bounces=false
        self.scrollView.zoomScale = self.scrollView.minimumZoomScale
        self.scrollView.delegate = self
        self.scrollView.isPagingEnabled = true
        ////
//        self.scrollView.isPagingEnabled = false
//        self.scrollView.isScrollEnabled = false
        ////
        self.scrollView.showsHorizontalScrollIndicator=false
        self.scrollView.backgroundColor = UIColor.white
        
        let deadlineTime = DispatchTime.now() + .seconds(1/4)
        DispatchQueue.main.asyncAfter(deadline: deadlineTime){
            self.setPageControll()
        }
    }
    
    func addImagesTemp(){
        
        if arrSelectedImages.count > 0 {
 
            for img in arrSelectedImages {
                    let objPhotoInfo = PhotoInfo.init(photoInfo: ["imageFile": img, "tags": [] ])
                    self.arrFeedImages.append(objPhotoInfo)
            }
            
        }
    }
    
    private func setPageControll(){
        
        let pageViewWidth = self.view.frame.size.width
        let pageViewHeight = self.pageView.frame.size.height
        
        self.scrollView.contentSize = CGSize(width: pageViewWidth * CGFloat(arrFeedImages.count), height: pageViewHeight)
        
        pageControl.numberOfPages = arrFeedImages.count
        
        for (index, objPhotoInfo)  in  arrFeedImages.enumerated() {
            
            let viewOuter = UIView.init(frame:  CGRect(x: CGFloat(index) * pageViewWidth, y: 0, width: pageViewWidth, height: pageViewHeight))
            viewOuter.backgroundColor = UIColor.white
            
            let scrollVwEBPhotoView = EBPhotoView.init(frame: CGRect(x: 0, y: 0, width: pageViewWidth, height: pageViewHeight))
            
            //            if isTypeIsUrl {
            //                scrollVwEBPhotoView.imageView.af_setImage(withURL:URL.init(string:(arrFeedImages[index] as! feedData).feedPost)!)
            //            }else{
            //                scrollVwEBPhotoView.setImage(objPhotoInfo.image)
            //            }
            
            
        scrollVwEBPhotoView.setImage(objPhotoInfo.image)
        scrollVwEBPhotoView.tag = index
            
            //// Himanshu new change
            //scrollVwEBPhotoView.imageView.contentMode = .scaleAspectFill
            if arrFeedImages.count > 1{
                if index == 0{
                    scrollVwEBPhotoView.imageView.contentMode = .scaleToFill
                }else{
                    scrollVwEBPhotoView.imageView.contentMode = .scaleAspectFit
                }
            }else{
                if imageRatioDynamic == 1{
                    scrollVwEBPhotoView.imageView.contentMode = .scaleToFill
                    
                }else if imageRatioDynamic == 0{
                    scrollVwEBPhotoView.imageView.contentMode = .scaleToFill
                    
                }else{
                    scrollVwEBPhotoView.imageView.contentMode = .scaleAspectFit
                }
            }
            //////
            
//         scrollVwEBPhotoView.setImage(objPhotoInfo.image)
//         scrollVwEBPhotoView.tag = index
            
            scrollVwEBPhotoView.imageView.clipsToBounds = true
            scrollVwEBPhotoView.adjustsContentModeForImageSize = false
            scrollVwEBPhotoView.myDelegate = self
            
            objPhotoInfo.objEBPhotoView = scrollVwEBPhotoView
            viewOuter.addSubview(scrollVwEBPhotoView)
            
            self.scrollView.addSubview(viewOuter)
        }
        self.setTagOnImages()
    }
    
    func setTagOnImages(){
        
        for objPhotoInfo in self.arrFeedImages {
            
            var arrTagPopovers = [EBTagPopover]()
            var arrNew : [TagInfo] = []
            if  forServiceTag == false{
                 arrNew = objPhotoInfo.arrTags
            }else{
                 arrNew = objPhotoInfo.arrServiceTags
            }
            
            for (index, objTagInfo) in arrNew.enumerated() {
               
                if objTagInfo.tagPosition.y>1.0{
                    UserDefaults.standard.set("down", forKey: "tagPosition")
                    UserDefaults.standard.synchronize()
                }else{
                    UserDefaults.standard.set("top", forKey: "tagPosition")
                    UserDefaults.standard.synchronize()
                }
                
                let tagPopover = EBTagPopover.init(tag: objTagInfo)
                if forServiceTag == false{
                    tagPopover?.tagPosition = objTagInfo.tagPosition
                }else{
                tagPopover?.strTagType  = "service"
                //tagPopover?.backgroundColor = UIColor.green
                //tagPopover?.setViewShadow(20.0)
                tagPopover?.strTagType = "service"
                //incallPrice outcallPrice
                let strInCallPrice = objTagInfo.metaData["incallPrice"] as? String ?? ""
                let strOutCallPrice = objTagInfo.metaData["outcallPrice"] as? String ?? ""
                let isOutCall = objTagInfo.metaData["isOutCall"] as? String ?? ""
                /*if strInCallPrice.count == 0 || strInCallPrice == "0" || strInCallPrice == "0.0"{
                    tagPopover?.strPrice = strOutCallPrice
                }else{
                    tagPopover?.strPrice = strInCallPrice
                }
                */
                    if isOutCall == "0"{
                       tagPopover?.strPrice = strInCallPrice
                    }else{
                       tagPopover?.strPrice = strOutCallPrice
                    }
                if objTagInfo.tagPosition.y>1.0{
                    tagPopover?.strTopOrBottom = "bottom"
                }else{
                    tagPopover?.strTopOrBottom = "top"
                }
                    tagPopover?.fromAddTag = "YES"
                    
                    if objTagInfo.tagPosition.x>=0.87{
                        tagPopover?.strLeftOrRight = "right"
                    }else{
                        if objTagInfo.tagPosition.x<=0.05{
                            tagPopover?.strLeftOrRight = "left"
                        }else{
                            tagPopover?.strLeftOrRight = ""
                        }
                    }
//                if objTagInfo.tagPosition.x>=0.87{
//                    tagPopover?.strLeftOrRight = "right"
//                }else{
//                    if objTagInfo.tagPosition.x<=0.05{
//                        tagPopover?.strLeftOrRight = "left"
//                    }else{
//                        tagPopover?.strLeftOrRight = ""
//                    }
//                }
                    
                }
                
                tagPopover?.normalizedArrowPoint = objTagInfo.normalizedPosition()
                tagPopover?.delegate = self as EBTagPopoverDelegate
                tagPopover?.alpha = 1
                tagPopover?.tag = index
                
                if let tagPopover = tagPopover {
                    arrTagPopovers.append(tagPopover)
                }
            }
            
            //self.setTagsHidden(true)
            if  forServiceTag == false{
                objPhotoInfo.arrTagPopovers = arrTagPopovers
            }else{
                objPhotoInfo.arrServiceTagPopovers = arrTagPopovers
            }

            if arrTagPopovers.count > 0{
                for tagPopover in arrTagPopovers{
                    objPhotoInfo.objEBPhotoView?.addSubview(tagPopover)
                }
            }
        }
    }

    //MARK: - ScrollView Delegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.view.endEditing(true)
        if scrollView == self.scrollView {
            let pageWidth: CGFloat = scrollView.frame.size.width
            pageControl.currentPage = (Int(scrollView.contentOffset.x) / Int(pageWidth))
            self.showTagListTagAccordingToSelectedImage()
        }
    }
    
 
    
    // MARK: - IBActions
    @IBAction func btnBackAction(_ sender: Any) {
        if forServiceTag == false{
            self.delegate?.finishTagSelectionWithData(arrFeedImagesWithTagInfo: self.arrFeedImages)
        }else{
            self.delegate?.finishServiceTagSelectionWithData(arrFeedImagesWithTagInfo: self.arrFeedImages)
        }
    }
    
    @IBAction func btnDoneTagSelectionAction(_ sender: Any) {
        if forServiceTag == false{
        self.delegate?.finishTagSelectionWithData(arrFeedImagesWithTagInfo: self.arrFeedImages)
        }else{
            self.delegate?.finishServiceTagSelectionWithData(arrFeedImagesWithTagInfo: self.arrFeedImages)
        }
    }
}

// MARK:- Custom Methods
extension AddTagVC : SearchServiceTagVCDelegate{
    
    func finishSectionOnSearchServiceTagVCWithData(objExpSearchTag: ExpSearchTag?){
        dismiss(animated: true, completion: nil)
        guard let objExpSearchTag = objExpSearchTag, objExpSearchTag.title != "" else {
            self.selectedNotification =  nil
            return
        }
        
        guard self.pageControl.currentPage < self.arrFeedImages.count else {
            return
        }
        
        let objPhotoInfo = self.arrFeedImages[self.pageControl.currentPage]
        for (index, objTagPopover) in objPhotoInfo.arrServiceTagPopovers.enumerated(){
            if objExpSearchTag.title == objTagPopover.text() {
                objPhotoInfo.arrServiceTags.remove(at: index)
                objPhotoInfo.arrServiceTagPopovers.remove(at: index)
                self.tagListView.removeTag(objExpSearchTag.title)
                objTagPopover.removeFromSuperview()
                break
            }
        }
        var strUserId = ""
        let userInfo = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]  ?? ["":""]
            if let userId = userInfo["_id"] as? String {
                strUserId = "\(userId)"
            }
        let dicParam = [
            "tabType": "service",
            "tagId": objExpSearchTag.id,
            "title": objExpSearchTag.title,
            "artistId": strUserId,
            "businessTypeId": objExpSearchTag.serviceId,
            "categoryId": objExpSearchTag.subserviceId,
            "incallPrice": objExpSearchTag.inCallPrice,
            "outcallPrice": objExpSearchTag.outCallPrice,
            "description": objExpSearchTag.description,
            "completionTime": objExpSearchTag.completionTime,
            "businessTypeName": objExpSearchTag.businessName,
            "categoryName": objExpSearchTag.categoryName,
            "staffId": objExpSearchTag.staffId,
            "staffName": objExpSearchTag.staffName,
            "isOutCall":objExpSearchTag.isOutCall,
            "staffInCallPrice": objExpSearchTag.staffInCallPrice,
            "staffPrimaryId": objExpSearchTag.staffPrimaryId,
            "staffCompletionTime": objExpSearchTag.staffInCallPrice,
            "staffOutCallPrice":objExpSearchTag.staffOutCallPrice] as [String : Any]
        
        arrServiceTageCategoryId.append(dicParam)
        self.addTagwith(withText: objExpSearchTag.title, dictTagDetail: dicParam)
    }
}

// MARK:- Custom Methods
extension AddTagVC : SearchTagVCDelegate{
   
    func finishSectionOnSearchTagVCWithData(objExpSearchTag: ExpSearchTag?) {
       
        dismiss(animated: true, completion: nil)
        guard let objExpSearchTag = objExpSearchTag, objExpSearchTag.title != "" else {
            self.selectedNotification =  nil
            return
        }
        
        guard self.pageControl.currentPage < self.arrFeedImages.count else {
            return
        }
        
        let objPhotoInfo = self.arrFeedImages[self.pageControl.currentPage]
        for (index, objTagPopover) in objPhotoInfo.arrTagPopovers.enumerated(){
            if objExpSearchTag.title == objTagPopover.text() {
                objPhotoInfo.arrTags.remove(at: index)
                objPhotoInfo.arrTagPopovers.remove(at: index)
                self.tagListView.removeTag(objExpSearchTag.title)
                objTagPopover.removeFromSuperview()
                break
            }
        }
        
        let dicParam = [
            "tabType": objExpSearchTag.tabType,
            "tagId": objExpSearchTag.id,
            "title": objExpSearchTag.title,
            "userType": objExpSearchTag.userType
            ] as [String : Any]

        self.addTagwith(withText: objExpSearchTag.title, dictTagDetail: dicParam)
    }

    
    
    
    
    func gotoSearchTagVC(){
        
        self.view.endEditing(true)
        let sb: UIStoryboard = UIStoryboard(name: "Tag", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"SearchTagVC") as? SearchTagVC {
            objVC.modalPresentationStyle = .overCurrentContext
            objVC.delegate = self
            present(objVC, animated: true, completion: nil)
        }
    }
    
    func gotoServiceTagVC(){
        
        self.view.endEditing(true)
        let sb: UIStoryboard = UIStoryboard(name: "Tag", bundle: Bundle.main)
        if let objVC = sb.instantiateViewController(withIdentifier:"SearchServiceTagVC") as? SearchServiceTagVC {
            objVC.modalPresentationStyle = .overCurrentContext
            objVC.delegate = self as? SearchServiceTagVCDelegate
            present(objVC, animated: true, completion: nil)
        }
    }
    
}

// MARK: - Notification observing
extension AddTagVC {
    
    func beginObservations() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.didEndSingleTouchOnPhoto(with:)), name: NSNotification.Name(rawValue: EBPhotoViewTouchDidEndNotification), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.didRecognizeSingleTap(with:)), name: NSNotification.Name(rawValue: EBPhotoViewSingleTapNotification), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.didRecognizeDoubleTap(with:)), name: NSNotification.Name(rawValue: EBPhotoViewDoubleTapNotification), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.didRecognizeLongPress(with:)), name: NSNotification.Name(rawValue: EBPhotoViewLongPressNotification), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.didLoadNewMetaData(with:)), name: NSNotification.Name(rawValue: EBPhotoViewControllerDidSetMetaDataNotification), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.didLoadNewTags(with:)), name: NSNotification.Name(rawValue: EBPhotoViewControllerDidUpdateTagsNotification), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.photoViewControllerDidCreateNewTag(with:)), name: NSNotification.Name(rawValue: EBPhotoViewControllerDidCreateTagNotification), object: nil)
    }
    
    func stopObservations() {
        NotificationCenter.default.removeObserver(self)
    }
    
}

// MARK: - EBTagPopover Delegate
extension AddTagVC : EBTagPopoverDelegate {
    
    func tagPopoverDidEndEditing(_ tagPopover: EBTagPopover!) {
        
    }
    
    func tagPopover(_ tagPopover: EBTagPopover!, didReceiveSingleTap singleTap: UITapGestureRecognizer!) {

        //removeTagWith(tagPopover: tagPopover, forPhotoAt: (tagPopover.superview?.tag)!)
    }
    
}

// MARK: - Tag Hide/Show
extension AddTagVC {
    
    func setTagsHidden(_ tagsHidden: Bool) {
        tagsHidden ? hideTags() : showTags()
    }
    
    func showTags() {
        setTagsAsHidden(false)
    }
    
    func hideTags() {
        setTagsAsHidden(true)
    }
    
    func setTagsAsHidden(_ hidden: Bool) {
        
        tagsHidden = hidden
        let alpha: CGFloat = hidden ? 0 : 1
        UIView.animate(withDuration: 0.2, delay: 0, options: [.beginFromCurrentState, .curveEaseOut], animations: {
            
            for objPhotoInfo in self.arrFeedImages {
                for tag in objPhotoInfo.arrTagPopovers {
                    tag.alpha = alpha
                }
            }
        }) { finished in
             for objPhotoInfo in self.arrFeedImages {
                for tag in objPhotoInfo.arrTagPopovers {
                    tag.alpha = alpha
                }
            }
        }
    }
}

// MARK: - Add and Remove Tags

extension AddTagVC {
    
    func addTagwith(withText text : String, dictTagDetail : [String : Any] ){
      
        guard let aNotification = self.selectedNotification, let photoView = aNotification.object as? EBPhotoView else {
            return
        }
        
        guard let touchInfo = aNotification.userInfo, let normalPointValue = touchInfo["normalizedPointInPhoto"] as? NSValue else {
            return
        }
        
        let normalizedPoint: CGPoint = normalPointValue.cgPointValue
        self.addNewTagwith(tagLocation: normalizedPoint, withText: text, forPhotoAt: (photoView.tag), dictTagDetail :dictTagDetail)
    }
    
    func addNewTagwith(tagLocation: CGPoint, withText tagText: String, forPhotoAt index: Int, dictTagDetail : [String : Any]){
        
        let objPhotoInfo = self.arrFeedImages[index]
        let tagNew = TagInfo.init(tagInfo: ["tagPosition":tagLocation , "tagText": tagText, "metaData": dictTagDetail])
        let tagPopover = EBTagPopover.init(tag: tagNew)
        
        if let tagPopover = tagPopover {
            
            tagPopover.normalizedArrowPoint = tagNew.normalizedPosition()
            tagPopover.delegate = self as EBTagPopoverDelegate
            tagPopover.alpha = 1
 
            if forServiceTag == false{
                tagPopover.tagPosition = tagNew.tagPosition
                
                
                if tagNew.tagPosition.y>1.0{
                    tagPopover.strTopOrBottom = "bottom"
                }else{
                    tagPopover.strTopOrBottom = "top"
                }
                tagPopover.fromAddTag = "YES"
                
                if tagNew.tagPosition.x>=1.0{
                    tagPopover.strLeftOrRight = "right"
                }else{
                    if tagNew.tagPosition.x<=0.05{
                        tagPopover.strLeftOrRight = "left"
                    }else{
                        tagPopover.strLeftOrRight = ""
                    }
                }
                
                
                tagPopover.strTagType = "people"

            }else{
                tagPopover.strTagType = "service"
                //incallPrice outcallPrice
                let strInCallPrice = tagNew.metaData["incallPrice"] as? String ?? ""
                let strOutCallPrice = tagNew.metaData["outcallPrice"] as? String ?? ""
                let isOutCall = tagNew.metaData["isOutCall"] as? String ?? ""
                if isOutCall == "0"{
                  tagPopover.strPrice = strInCallPrice
                }else{
                  tagPopover.strPrice = strOutCallPrice
                }
                /*if strInCallPrice.count == 0 || strInCallPrice == "0" || strInCallPrice == "0.0"{
                    tagPopover.strPrice = strOutCallPrice
                }else{
                    tagPopover.strPrice = strInCallPrice
                }
                */
                if tagNew.tagPosition.y>1.0{
                    tagPopover.strTopOrBottom = "bottom"
                }else{
                    tagPopover.strTopOrBottom = "top"
                }
                tagPopover.fromAddTag = "YES"

                if tagNew.tagPosition.x>=1.0{
                    tagPopover.strLeftOrRight = "right"
                }else{
                    if tagNew.tagPosition.x<=0.05{
                        tagPopover.strLeftOrRight = "left"
                    }else{
                        tagPopover.strLeftOrRight = ""
                    }
                }
            }
            
            if forServiceTag == false{
                tagPopover.tag = objPhotoInfo.arrTags.count
                objPhotoInfo.arrTags.append(tagNew)
                self.tagListView.addTag(tagText, staffName: "NewOne", forService: false)
                objPhotoInfo.arrTagPopovers.append(tagPopover)
            }else{
                tagPopover.tag = objPhotoInfo.arrServiceTags.count
                objPhotoInfo.arrServiceTags.append(tagNew)
                let staffName = dictTagDetail["staffName"] as? String ?? ""
                self.tagListView.addTag(tagText, staffName: staffName, forService: true)
                objPhotoInfo.arrServiceTagPopovers.append(tagPopover)
            }
            objPhotoInfo.objEBPhotoView?.addSubview(tagPopover)
        }
    }
    
    func removeTagWith(tagPopover: EBTagPopover , forPhotoAt index: Int){
        
        let objPhotoInfo = self.arrFeedImages[index]
        
        for (index, objTagPopover) in objPhotoInfo.arrTagPopovers.enumerated(){
            
            if objTagPopover.tag == tagPopover.tag {
                objPhotoInfo.arrTags.remove(at: index)
                objPhotoInfo.arrTagPopovers.remove(at: index)
                tagPopover.removeFromSuperview()
                break
            }
        }
    }
}

// MARK: - Notification Actions
extension AddTagVC {
    
    // MARK: - Event Hooks
    @objc func didEndSingleTouchOnPhoto(with aNotification: Notification?) {
      
        guard let aNotification = aNotification else {
            return
        }
        self.selectedNotification = aNotification
         if  forServiceTag == false{
            self.gotoSearchTagVC()
        }else{
            self.gotoServiceTagVC()
        }

    }
    
    @objc func didRecognizeSingleTap(with aNotification: Notification?) {
 
    }
    
    @objc func didRecognizeDoubleTap(with aNotification: Notification?) {
        
    }
    
    @objc func didRecognizeLongPress(with aNotification: Notification?) {
 
    }
    
    
    @objc func didLoadNewMetaData(with aNotification: Notification?) {
        
       
    }
    
    @objc func didLoadNewTags(with aNotification: Notification?) {
        
       
    }
    
    @objc func photoViewControllerDidCreateNewTag(with aNotification: Notification?) {

    }
}

extension AddTagVC : TagListViewDelegate {
    
    func configureTagListView(){
        tagListView.delegate = self
        tagListView.textFont = UIFont.boldSystemFont(ofSize: 13)//UIFont.systemFont(ofSize: 20)
    }
    
    func showTagListTagAccordingToSelectedImage(){
        
        guard self.pageControl.currentPage < self.arrFeedImages.count else {
            return
        }
        let objPhotoInfo = self.arrFeedImages[self.pageControl.currentPage]
        self.tagListView.removeAllTags()
        if forServiceTag == false{
        for (index, objTagPopover) in objPhotoInfo.arrTagPopovers.enumerated(){
            let tagView = self.tagListView.addTag(objTagPopover.text(), staffName: "NewOne", forService: false)
           tagView.tag = index
            }
        }else{
            for (index, objTagPopover) in objPhotoInfo.arrServiceTagPopovers.enumerated(){
                let obj = objPhotoInfo.arrServiceTags[index].metaData
                let staffName = obj["staffName"] as? String ?? ""
                let tagView = self.tagListView.addTag(objTagPopover.text(), staffName: staffName, forService: true)
                tagView.tag = index
            }
        }
        
    }
    
    // MARK: TagListViewDelegate
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) {
        tagView.isSelected = !tagView.isSelected
    }
    
    func tagRemoveButtonPressed(_ title: String, tagView: TagView, sender: TagListView) {
                
        guard self.pageControl.currentPage < self.arrFeedImages.count else {
            return
        }
        
        let objPhotoInfo = self.arrFeedImages[self.pageControl.currentPage]
        if forServiceTag == false{
            
            for (index, objTagPopover) in objPhotoInfo.arrTagPopovers.enumerated(){
                if title == objTagPopover.text() {
                    sender.removeTagView(tagView)
                    objPhotoInfo.arrTags.remove(at: index)
                    objPhotoInfo.arrTagPopovers.remove(at: index)
                    objTagPopover.removeFromSuperview()
                    break
                }
            }
        }else{
            for (index, objTagPopover) in objPhotoInfo.arrServiceTagPopovers.enumerated(){
                if title == objTagPopover.text() {
                    sender.removeTagView(tagView)
                    objPhotoInfo.arrServiceTags.remove(at: index)
                    objPhotoInfo.arrServiceTagPopovers.remove(at: index)
                    objTagPopover.removeFromSuperview()
                    break
                }
            }
        }
        
    }
}

// MARK: - Notification Actions
extension AddTagVC {
    
    @objc func photoView(_ photoView: EBPhotoView!, didReceiveSingleTap singleTap: UITapGestureRecognizer!) {
        //delegate.didReceiveSingleTapAt(indexPath: indexPath, feedsTableCell: self)
    }
    
    @objc func photoView(_ photoView: EBPhotoView!, didReceiveDoubleTap doubleTap: UITapGestureRecognizer!) {
        //delegate.didReceiveDoubleTapAt(indexPath: indexPath, feedsTableCell: self)
    }
    
    @objc func photoView(_ photoView: EBPhotoView!, didReceiveLongPress longPress: UILongPressGestureRecognizer!) {
        //delegate.didReceiveLondPressAt(indexPath: indexPath, feedsTableCell: self)
        //self.setTagsHidden(!tagsHidden)
    }
    
}




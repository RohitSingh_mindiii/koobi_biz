//
//  SearchServiceTagVC.swift
//  MualabBusiness
//
//  Created by Mindiii on 2/4/19.
//  Copyright © 2019 Mindiii. All rights reserved.
//

import UIKit
protocol SearchServiceTagVCDelegate {
    func finishSectionOnSearchServiceTagVCWithData(objExpSearchTag: ExpSearchTag?)
}

class SearchServiceTagVC:  UIViewController, UITableViewDataSource, UITableViewDelegate {
        
        var delegate: SearchServiceTagVCDelegate?
    var objHoldService = ExpSearchTag(dict: ["":""], type: "service")

        @IBOutlet var tblView : UITableView!
        @IBOutlet weak var lblNoResults: UIView!
        @IBOutlet weak var activity: UIActivityIndicatorView!
        
        @IBOutlet weak var searchView: UIView!
        @IBOutlet weak var btnSearch: UIButton!
        @IBOutlet weak var txtSearch: UITextField!
    
    @IBOutlet weak var tblCompany: UITableView!
    @IBOutlet weak var viewBottumTable: UIView!
    private var arrStaffList  = [ServiceStaffDetail]()
    
    
    @IBOutlet weak var btnIncall:UIButton!
    @IBOutlet weak var btnOutcall:UIButton!
    @IBOutlet weak var viewBtnIncallBottum:UIView!
    @IBOutlet weak var viewBtnOutcallBottum:UIView!
    @IBOutlet weak var HeightViewBottumTable: NSLayoutConstraint!


        fileprivate var strSearchValue : String = ""
        
        var tabType : String = "serviceTag"
        var previousSearchText : String = ""
 
        fileprivate var arrExpSearchServiceTag = [ExpSearchTag]()
        fileprivate var arrAllServiceTagData = [ExpSearchTag]()
    
        var segmentIncall = true
            
      override func viewDidLoad() {
            super.viewDidLoad()
            segmentIncall = true
            self.viewBottumTable.isHidden = true
            self.txtSearch.delegate = self
            self.tblView.delegate = self
            self.tblView.dataSource = self
            self.tblCompany.delegate = self
            self.tblCompany.dataSource = self
            
            self.btnIncall.setTitleColor(#colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1), for: .normal)
            self.btnOutcall.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            self.viewBtnIncallBottum.backgroundColor = #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1)
            self.viewBtnOutcallBottum.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
            
            self.addGesturesToView()
    }
    func addGesturesToView() -> Void {
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.topToBottomSWIPE))
        swipeRight.direction = .down
        self.viewBottumTable.addGestureRecognizer(swipeRight)
    }
    
    @objc func topToBottomSWIPE() ->Void {
        self.view.endEditing(true)
        self.viewBottumTable.isHidden = true
    }
        
        override func viewWillAppear(_ animated: Bool) {
            segmentIncall = true
            self.viewBottumTable.isHidden = true
            loadDataWithPageCount(strSearchText: "")
        }
        override var preferredStatusBarStyle: UIStatusBarStyle {
            return .default
        }
    }
    
    // MARK:- Btn Action
    extension SearchServiceTagVC  {
        
        @IBAction func btnCancelAction(_ sender: Any) {
     //       dismiss(animated: true, completion: nil)
            delegate?.finishSectionOnSearchServiceTagVCWithData(objExpSearchTag: nil)
        }
        
        @IBAction func btnSearchAction(_ sender: Any) {
            btnSearch.isHidden = true
            txtSearch.becomeFirstResponder()
        }
    }
    
    // MARK:- TableView Delegates
    extension SearchServiceTagVC  {
        
        public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
            if tableView == tblCompany{
                return self.arrStaffList.count
            }else{
            return self.arrExpSearchServiceTag.count
        }
        }
        
        public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
            if tableView == tblCompany{
                let cellIdentifier = "CellBottumTableList"
                if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? CellBottumTableList{
                    let obj = self.arrStaffList[indexPath.row]
                    cell.lblTitle.text = obj.satffName//.capitalized
                    let url = URL(string: obj.staffImage)
                    if url != nil{
                        //cell.imgCompany.af_setImage(withURL: url!)
                        cell.imgCompany.sd_setImage(with: url!, placeholderImage:#imageLiteral(resourceName: "cellBackground"))
                    }else{
                        cell.imgCompany.image =  #imageLiteral(resourceName: "cellBackground")
                    }
                    return cell
                }else{
                    return UITableViewCell()
                }
            }else{
            if let cell : CellTop = tableView.dequeueReusableCell(withIdentifier: "CellTop")! as? CellTop{
                
                let objExpSearchTag = self.arrExpSearchServiceTag[indexPath.row]
                cell.lblName.text = objExpSearchTag.title
                cell.lblPostDetails.text = objExpSearchTag.completionTime
                if self.segmentIncall{
                    let doubleStrNN = String(format: "%.2f", Double(objExpSearchTag.inCallPrice)!)
                    cell.lblPrice.text = "£"+doubleStrNN
                }else{
                    let doubleStrNN = String(format: "%.2f", Double(objExpSearchTag.outCallPrice)!)
                    cell.lblPrice.text = "£"+doubleStrNN
                }
                /*if objExpSearchTag.inCallPrice != "" {
                    let a  = Double(objExpSearchTag.inCallPrice)
                    if a == 0{
                        let doubleStrNN = String(format: "%.2f", ceil(Double(objExpSearchTag.outCallPrice)!))
                        //cell.lblPrice.text = "£"+objExpSearchTag.outCallPrice
                        cell.lblPrice.text = "£"+doubleStrNN
                    }else{
                        let doubleStrNN = String(format: "%.2f", ceil(Double(objExpSearchTag.inCallPrice)!))
                        //cell.lblPrice.text = "£"+objExpSearchTag.inCallPrice
                        cell.lblPrice.text = "£"+doubleStrNN
                    }
                }
                */
                cell.lblPostDetails.isHidden = false
                return cell
            }else{
                return UITableViewCell()
            }
            }
        }
        
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            if tableView == tblCompany{
                return 50
            }else{
            return 71
            }
        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            if tableView != tblCompany{
                let objExpSearchTag = self.arrExpSearchServiceTag[indexPath.row]
                var userId = ""
                if let dictUser = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]{
                    if let busId = dictUser[UserDefaults.keys.userId] as? String{
                        userId = busId
                    }else if let busId = dictUser[UserDefaults.keys.userId] as? Int{
                        userId = String(busId)
                    }
                }
             
                let param = ["artistServiceId":String(objExpSearchTag.id),"businessId":userId]
                objHoldService = objExpSearchTag
                callWebserviceForCompanyList(dict: param)
            }else{
                var userId = ""
                if let dictUser = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any]{
                    if let busId = dictUser[UserDefaults.keys.userId] as? String{
                        userId = busId
                    }else if let busId = dictUser[UserDefaults.keys.userId] as? Int{
                        userId = String(busId)
                    }
                }
                self.viewBottumTable.isHidden = true
                let objExpSearchTag = self.arrStaffList[indexPath.row]
                if segmentIncall == true{
                    self.objHoldService.isOutCall = "0"
                }else{
                    self.objHoldService.isOutCall = "1"
                }
                self.objHoldService.staffOutCallPrice = objExpSearchTag.outCallPrice
                self.objHoldService.staffInCallPrice = objExpSearchTag.inCallPrice
                self.objHoldService.staffId = objExpSearchTag._id
                self.objHoldService.staffName = objExpSearchTag.satffName
                self.objHoldService.staffCompletionTime = objExpSearchTag.completionTime
                self.objHoldService.staffPrimaryId = objExpSearchTag.staffServicePrimaryID
                if objExpSearchTag._id == userId{
                    self.objHoldService.staffInCallPrice = self.objHoldService.inCallPrice
                    self.objHoldService.staffOutCallPrice =  self.objHoldService.outCallPrice
                    self.objHoldService.staffCompletionTime =  self.objHoldService.completionTime
                }
                delegate?.finishSectionOnSearchServiceTagVCWithData(objExpSearchTag: self.objHoldService)
            }
        }
        
        func scrollViewDidScroll(_ scrollView: UIScrollView) {
            self.view.endEditing(true)
            self.parent?.view.endEditing(true)
        }

    @IBAction func btnHiddenBottumTable(_ sender: UIButton) {
        self.viewBottumTable.isHidden = true
        if  viewHeightGloble < Int(self.tabBarController?.view.frame.height ?? CGFloat(viewHeightGloble)) || self.tabBarController?.tabBar.isHidden == true{
            self.tabBarController?.tabBar.isHidden = false
            let b = Int(self.view.frame.width)
            let a = CGRect(x:0,y:0,width:b,height:viewHeightGloble)
            self.tabBarController?.view.frame = a//CGRect(x:0, y:0, width:self.view.frame.size.width, height:viewHeightGloble)
        }
}
}


    // MARK: - Webservices call
    
    extension SearchServiceTagVC {
        
        func loadDataWithPageCount(strSearchText : String = "") {
 
            var strUserId : String = ""
            if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.businessInfoDic){
                if let userId = dict["_id"] as? String {
                    strUserId = "\(userId)"
                }
            }else{
                let dict = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] ?? ["":""]
                if let userId = dict["_id"] as? String {
                    strUserId = "\(userId)"
                }
            }
            previousSearchText = strSearchText
            let dicParam = [ "search": strSearchText, "artistId":strUserId ] as [String : Any]
            callWebserviceFor_getData(dicParam: dicParam)
        }
        
        func callWebserviceFor_getData(dicParam: [AnyHashable: Any]){
            
            if !objServiceManager.isNetworkAvailable(){
                objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
                return
            }

            self.lblNoResults.isHidden = true
            var parameters = [String:Any]()
            parameters = dicParam as! [String : Any]
            
            objWebserviceManager.requestPostForJson(strURL: WebURL.artistServices, params: parameters , success: { response in
                
                self.activity.stopAnimating()
                
                    self.arrAllServiceTagData.removeAll()
                objAppShareData.arrExpSearchServiceTag.removeAll()
                print(response)
                let strSucessStatus = response["status"] as? String
                if strSucessStatus == k_success{
                    
                    var arrTemp = [[String : Any]]()
                  
                    if response.keys.contains("topList"){
                        if let arrDict = response["topList"] as? [[String:Any]]{
                            arrTemp = arrDict
                        }
                        
                    }else if response.keys.contains("peopleList"){
                        if let arrDict = response["peopleList"] as? [[String:Any]]{
                            arrTemp = arrDict
                        }
                        
                    }else if response.keys.contains("hasTagList"){
                        if let arrDict = response["hasTagList"] as? [[String:Any]]{
                            arrTemp = arrDict
                        }
                        
                    }else if response.keys.contains("placeList"){
                        if let arrDict = response["placeList"] as? [[String:Any]]{
                            arrTemp = arrDict
                        }
                        
                    }else if response.keys.contains("serviceTagList"){
                        if let arrDict = response["serviceTagList"] as? [[String:Any]]{
                            arrTemp = arrDict
                        }
                    }else if response.keys.contains("artistServices"){
                        if let arrDict = response["artistServices"] as? [[String:Any]]{
                            arrTemp = arrDict
                        }
                    }
                    
                    
                    if arrTemp.count > 0 {
                        
                        for dict in arrTemp{
                            
                            let objExpSearchTag = ExpSearchTag.init(dict: dict, type: "service")
                            
                            switch (self.tabType){
                                
                            case "top":
                                objExpSearchTag.type = 0;
                                objExpSearchTag.tabType = "top"
                                objExpSearchTag.title = objExpSearchTag.uniTxt;
                                objExpSearchTag.desc =  "\(objExpSearchTag.postCount) " + "post";
                                break;
                                
                            case "people":
                                objExpSearchTag.type = 1;
                                objExpSearchTag.tabType = "people"
                                objExpSearchTag.title = objExpSearchTag.uniTxt;
                                objExpSearchTag.desc = "\(objExpSearchTag.postCount) " + "post";
                                break;
                                
                            case "hasTag":
                                objExpSearchTag.type = 2;
                                objExpSearchTag.tabType = "hasTag"
                                objExpSearchTag.title = objExpSearchTag.tag;
                                objExpSearchTag.desc = "\(objExpSearchTag.tagCount) " + "public post";
                                break;
                                
                            case "serviceTag":
                                objExpSearchTag.type = 3;
                                objExpSearchTag.tabType = "serviceTag"
                                objExpSearchTag.title = objExpSearchTag.uniTxt;
                                objExpSearchTag.desc = "\(objExpSearchTag.tagCount) " + "public post";
                                break;
                                
                            case "place":
                                objExpSearchTag.type = 4;
                                objExpSearchTag.tabType = "place"
                                objExpSearchTag.title = objExpSearchTag.uniTxt;
                                objExpSearchTag.desc = "\(objExpSearchTag.postCount) " + "public post";
                                break;
                                
                            case "artistServices":
                                objExpSearchTag.type = 4;
                                objExpSearchTag.tabType = "serviceTag"
                                objExpSearchTag.title = objExpSearchTag.uniTxt;
                                objExpSearchTag.desc = "\(objExpSearchTag.postCount) " + "public post";
                                break;
                                
                            default:
                                break;
                            }
                            self.arrAllServiceTagData.append(objExpSearchTag)
                            objAppShareData.arrExpSearchServiceTag.append(objExpSearchTag)

                        }
                    }
                    
                }else{
                    if strSucessStatus == "fail"{
                        
                    }else{
                        if let msg = response["message"] as? String{
                            objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                        }
                        
                    }
                }
                
                self.manageSegmentAction()
                if self.arrExpSearchServiceTag.count==0{
                //    self.lblNoResults.isHidden = false
                }else{
                  //  self.lblNoResults.isHidden = true
                }
                
            }) { (error) in
                
                self.activity.stopAnimating()
                self.manageSegmentAction()
                if self.arrExpSearchServiceTag.count==0{
                    self.lblNoResults.isHidden = false
                }else{
                    self.lblNoResults.isHidden = true
                }
                
                objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
            }
        }
        
        
        
        func manageSegmentAction(){
            self.arrExpSearchServiceTag.removeAll()
            if segmentIncall == true{
                
                for obj in self.arrAllServiceTagData{
                    if obj.bookingType == "Both" || obj.bookingType == "Incall"{
                        self.arrExpSearchServiceTag.append(obj)
                    }
                }
                
                self.btnIncall.setTitleColor(#colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1), for: .normal)
                self.btnOutcall.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
                self.viewBtnIncallBottum.backgroundColor = #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1)
                self.viewBtnOutcallBottum.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
                self.tblView.reloadData()

            }else{
                
                for obj in self.arrAllServiceTagData{
                    if obj.bookingType == "Both" || obj.bookingType == "Outcall"{
                        self.arrExpSearchServiceTag.append(obj)
                    }
                }
                
                self.btnOutcall.setTitleColor(#colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1), for: .normal)
                self.btnIncall.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
                self.viewBtnIncallBottum.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
                self.viewBtnOutcallBottum.backgroundColor = #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1)
                self.tblView.reloadData()

            }
        }
    }
    
    // MARK: - UITextfield Delegate
    extension SearchServiceTagVC : UITextFieldDelegate{
        
        func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
            
            let text = textField.text! as NSString
            
            if (text.length == 1)  && (string == "") {
                NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
                //getLikesUsersData(0, andSearchText: "")
                loadDataWithPageCount(strSearchText: "")
            }
            var substring: String = textField.text!
            substring = (substring as NSString).replacingCharacters(in: range, with: string)
            substring = substring.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            searchAutocompleteEntries(withSubstring: substring)
            return true
        }
        
        func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            let text = textField.text! as NSString
            if (text.length == 0){
                btnSearch.isHidden = false
            }
            textField.resignFirstResponder()
            return true
        }
        
        func textFieldShouldClear(_ textField: UITextField) -> Bool {
            self.view.endEditing(true)
            
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
            loadDataWithPageCount(strSearchText: "")
            return true
        }
        
        // MARK: - searching operation
        func searchAutocompleteEntries(withSubstring substring: String) {
            if substring != "" {
                strSearchValue = substring
                // to limit network activity, reload half a second after last key press.
                NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
                self.perform(#selector(self.reload), with: nil, afterDelay: 0.5)
            }
        }
        
        @objc func reload() {
            //getLikesUsersData(0, andSearchText: strSearchValue)
            loadDataWithPageCount(strSearchText: strSearchValue)
        }
}



//MARK:- Webservice Call
extension SearchServiceTagVC {
    
    func callWebserviceForCompanyList(dict: [String : Any]){
        
        if !objServiceManager.isNetworkAvailable(){
            objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        
        objActivity.startActivityIndicator()
        objServiceManager.requestPost(strURL: WebURL.tagStaffServices, params: dict  , success: { response in
            objServiceManager.StopIndicator()
            
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                sessionExpireAlertVC(controller: self)
            }else{
                let strStatus =  response["status"] as? String ?? ""
                if strStatus == k_success{
                    self.parseResponceForStaff(response:response)
                }else{
                    let msg = response["message"] as! String
                    objAppShareData.showAlert(withMessage: msg, type: alertType.bannerDark,on: self)
                }
            }
            
        }){ error in
            objServiceManager.StopIndicator()
            objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
    
    
    func parseResponceForStaff(response:[String : Any]){
        self.arrStaffList.removeAll()
              if let arr = response["staffInfo"] as? [[String:Any]]{
            if arr.count > 0{
                var bookType = "Both"
                if segmentIncall == true{
                    bookType = "Incall"
                }else{
                    bookType = "Outcall"
                }
                
                for dictArtistData in arr {
                    let objArtistList = ServiceStaffDetail.init(dict: dictArtistData)
                    if objArtistList.bookingType == "Both" || objArtistList.bookingType.uppercased() == bookType.uppercased(){
                        self.arrStaffList.append(objArtistList)
                    }
                }
                self.HeightViewBottumTable.constant = CGFloat(self.arrStaffList.count*50)
                self.tblCompany.reloadData()
                self.viewBottumTable.isHidden = false
            }
        }
    }
}

//MARK: - segment extension
extension SearchServiceTagVC{
    @IBAction func btnIncall(_ sender:Any){
        self.segmentIncall = true
        self.manageSegmentAction()
    //    if self.serType == "I" {
            self.btnIncall.setTitleColor(#colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1), for: .normal)
            self.btnOutcall.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            self.viewBtnIncallBottum.backgroundColor = #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1)
            self.viewBtnOutcallBottum.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
//        }else{
//            self.btnOutcall.setTitleColor(#colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1), for: .normal)
//            self.btnIncall.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
//            self.viewBtnIncallBottum.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
//            self.viewBtnOutcallBottum.backgroundColor = #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1)
//        }
    }
    
    @IBAction func btnOutcall(_ sender:Any){
        self.segmentIncall = false
        self.manageSegmentAction()
//        if self.serType == "I" {
//            self.btnIncall.setTitleColor(#colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1), for: .normal)
//            self.btnOutcall.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
//            self.viewBtnIncallBottum.backgroundColor = #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1)
//            self.viewBtnOutcallBottum.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
//        }else{
            self.btnOutcall.setTitleColor(#colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1), for: .normal)
            self.btnIncall.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            self.viewBtnIncallBottum.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
            self.viewBtnOutcallBottum.backgroundColor = #colorLiteral(red: 0, green: 0.8274509804, blue: 0.7882352941, alpha: 1)
//        }
//        /*self.tblBusinessLIst.reloadData()*/ self.tblReloadDataForIncallOutcall()
    }
    
    
}

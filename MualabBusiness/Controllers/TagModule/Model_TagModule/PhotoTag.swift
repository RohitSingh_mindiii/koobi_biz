
//
//  PhotoTag.swift
//  DemoImageTagSwift
//
//  Created by Mac on 10/07/18.
//  Copyright © 2018 Amit. All rights reserved.
//

import Foundation
import UIKit

class PhotoInfo: NSObject {

    var image: UIImage?
    var arrTags: [TagInfo] = []
    var arrServiceTags: [TagInfo] = []
    
    var arrTagPopovers: [EBTagPopover] = []
    var arrServiceTagPopovers: [EBTagPopover] = []

    var objEBPhotoView : EBPhotoView?
    
    init(photoInfo: [AnyHashable : Any]) {
        super.init()
        self.image = photoInfo["imageFile"] as? UIImage
      
        if let arr = photoInfo["tags"] as? [TagInfo]{
             self.arrTags = arr
            self.arrServiceTags = arr
        }
        
    }
}

class TagInfo: NSObject, EBPhotoTagProtocol {
    
    var tagPosition = CGPoint.zero
    var attributedText: NSAttributedString?
    var text = ""
    var metaData: [AnyHashable : Any] = [:]
    
    init(tagInfo: [AnyHashable : Any]) {
        
            super.init()
            let tagPositionValue = tagInfo["tagPosition"] as? NSValue
            self.tagPosition = tagPositionValue?.cgPointValue ?? CGPoint.zero
            self.text = tagInfo["tagText"] as? String ?? ""
            attributedText = tagInfo["attributedTagText"] as? NSAttributedString
        
        if let anInfo = tagInfo["metaData"] as? [AnyHashable : Any] {
                self.metaData = anInfo
            }
    }
    
    class func initWithServerData(dict : [String : Any], dictDetail : [String : Any]) -> TagInfo {

        let objTagInfo = TagInfo.init(tagInfo: ["" : ""])
        
        if let unique_tag_id = dict["unique_tag_id"] as? String{
            objTagInfo.text = unique_tag_id
        }
        
//        if let dictTagDetails = dict["tagDetails"] as? [String : Any]{
//            objTagInfo.metaData = dictTagDetails
//        }
        objTagInfo.metaData = dictDetail
        
        var x_axis : Double = 0
        var y_axis : Double = 0
        
        if let x = dict["x_axis"] as? Double{
              x_axis = x
        }else{
            if let x = dict["x_axis"] as? String{
                x_axis = Double(x) ?? 0
            }
        }
        
        if let y = dict["y_axis"] as? Double{
            y_axis = y
        }else{
            if let y = dict["y_axis"] as? String{
                y_axis = Double(y) ?? 0
            }
        }
        objTagInfo.tagPosition.x =  CGFloat(x_axis/100)
        objTagInfo.tagPosition.y =  CGFloat(y_axis/100)
        
        return objTagInfo
    }
    
    func normalizedPosition() -> CGPoint {
        return tagPosition
    }
    
    func attributedTagText() -> NSAttributedString? {
        return attributedText
    }
    
    func tagText() -> String? {
        return text
    }
    
    func metaInfo() -> [AnyHashable : Any]? {
        return metaData
    }
}


class ExpSearchTag {
    
    var id : Int = 0
    var type : Int = 0
    var title : String = ""
    var desc : String = ""
    var tabType : String = ""
    var imageUrl : String = ""
    var uniTxt : String = ""
    var postCount : String = "0"
    var fullName : String = ""
    var tag : String = ""
    var tagCount : String = "0"
    var userType : String = ""
    var isOutCall = "0"
    var strCreatUserStatus = false
    
      var completionTime = "";
     var description = ""
     var inCallPrice = ""
     var outCallPrice = ""
     var serviceId = ""
     var subserviceId = ""
    var businessName = "";
    var categoryName = "";
    var bookingType = ""
    var staffId = "";
    var staffName = "";
    var staffInCallPrice = "";
    var staffOutCallPrice = "";
    var staffCompletionTime = "";
    var staffPrimaryId = 0;

    init(dict : [String : Any],type:String){
        self.strCreatUserStatus = false
        if type == ""{
                if let id = dict["_id"] as? Int{   self.id = id  }
                if let firstName = dict["firstName"] as? String{  self.fullName = firstName  }
                if let lastName = dict["lastName"] as? String{   self.fullName =  self.fullName + lastName  }
                if let userName = dict["userName"] as? String{   self.uniTxt =  userName   }else{  if let location = dict["location"] as? String{   self.uniTxt =  location   }  }
                if let imageUrl = dict["profileImage"] as? String{   self.imageUrl = imageUrl  }
                if let isOutCall = dict["isOutCall"] as? String{   self.isOutCall = isOutCall  }
                if let postCount = dict["postCount"] as? String{  self.postCount = postCount   }
                if let tag = dict["tag"] as? String{   self.tag = tag   }
                if let tagCount = dict["tagCount"] as? String{    self.tagCount = tagCount   }else{  if let tagCount = dict["tagCount"] as? Int{  self.tagCount = "\(tagCount)"   }   }
                if let userType = dict["userType"] as? String{  self.userType = userType   }
        }else{
            if let id = dict["_id"] as? Int{   self.id = id  }
            if let id = dict["subserviceId"] as? Int{   self.subserviceId = String(id)  }
            if let id = dict["serviceId"] as? Int{   self.serviceId = String(id)  }
            if let userType = dict["completionTime"] as? String{  self.completionTime = userType   }
            if let userType = dict["description"] as? String{  self.description = userType   }
            if let userType = dict["inCallPrice"] as? String{  self.inCallPrice = userType   }
            if let isOutCall = dict["isOutCall"] as? String{   self.isOutCall = isOutCall  }
            if let userType = dict["outCallPrice"] as? String{  self.outCallPrice = userType   }
            if let userType = dict["title"] as? String{  self.title = userType   }
            if let userType = dict["title"] as? String{  self.uniTxt = userType   }
            
            if let userType = dict["staffId"] as? String{  self.staffId = userType   }
            if let userType = dict["staffName"] as? String{  self.staffName = userType   }
            if let userType = dict["staffInCallPrice"] as? String{  self.staffInCallPrice = userType   }
            if let userType = dict["staffOutCallPrice"] as? String{  self.staffOutCallPrice = userType   }
            if let userType = dict["staffCompletionTime"] as? String{  self.staffCompletionTime = userType   }

            
            if self.inCallPrice != "" && self.outCallPrice != ""{
                if Double(self.inCallPrice ) != 0 && Double(self.outCallPrice ) != 0{
                    bookingType = "Both"
                }else if Double(self.inCallPrice ) != 0{
                    bookingType = "Incall"
                }else if Double(self.outCallPrice ) != 0{
                    bookingType = "Outcall"
                }
            }
            
            
            
            
            
            
            
            if let arrBusinessType = dict["businessType"] as? [[String:Any]]{
                if arrBusinessType.count > 0 {
                 let index = arrBusinessType[0]
                if let businessNames = index["title"] as? String{
                    businessName = businessNames
                }
            }
              }
            if let arrCategory = dict["category"] as? [[String:Any]]{
                if arrCategory.count > 0 {
                let index = arrCategory[0]
                if let categoryNames = index["title"] as? String{
                    categoryName = categoryNames
                }
            }
            }
        }
    }
    
}


class ExpSearchServiceTag {
    
    var id : Int = 0
    var type : Int = 0
    var title : String = ""
    var desc : String = ""
    var tabType : String = ""
    
    var imageUrl : String = ""
    var uniTxt : String = ""
    var postCount : String = "0"
    var fullName : String = ""
    
    var tag : String = ""
    var tagCount : String = "0"
    
    var userType : String = ""
    var strCreatUserStatus = false
    
    init(dict : [String : Any]){
        self.strCreatUserStatus = false
        if let id = dict["_id"] as? Int{
            self.id = id
        }
        if let firstName = dict["firstName"] as? String{
            self.fullName = firstName
        }
        if let lastName = dict["lastName"] as? String{
            self.fullName =  self.fullName + lastName
        }
        if let userName = dict["userName"] as? String{
            self.uniTxt =  userName
        }else{
            if let location = dict["location"] as? String{
                self.uniTxt =  location
            }
        }
        if let imageUrl = dict["profileImage"] as? String{
            self.imageUrl = imageUrl
        }
        if let postCount = dict["postCount"] as? String{
            self.postCount = postCount
        }
        
        if let tag = dict["tag"] as? String{
            self.tag = tag
        }
        if let tagCount = dict["tagCount"] as? String{
            self.tagCount = tagCount
        }else{
            if let tagCount = dict["tagCount"] as? Int{
                self.tagCount = "\(tagCount)"
            }
        }
        
        if let userType = dict["userType"] as? String{
            self.userType = userType
        }
    }
    
}

//
//  TimeTableCell.swift
//  MualabBusiness
//
//  Created by Mac on 04/01/2018 .
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

class TimeTableCell: UITableViewCell {
    
    @IBOutlet weak var lblStartTime : UILabel?
    @IBOutlet weak var lblEndTime : UILabel?
    @IBOutlet weak var btnDelete : UIButton?
    
    @IBOutlet weak var timeView : UIView?
    @IBOutlet weak var imgDottedLine:UIImageView?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//
//  BusinessHoursTableCell.swift
//  MualabBusiness
//
//  Created by Mac on 04/01/2018 .
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

protocol BusinessHoursTableCellDelegate {
    func selectedTimeSloat(withSenderTable tableView:UITableView, timeArrayIndex:Int)
    func removeTimeSloat(withSenderTable tableView:UITableView, timeArrayIndex:Int)
}

class BusinessHoursTableCell: UITableViewCell {
    
    @IBOutlet weak var btnIsOpen : UIButton?
    @IBOutlet weak var btnAdd : UIButton?
    
    @IBOutlet weak var lblDay : UILabel?
    @IBOutlet weak var lblNotWorking : UILabel?
    @IBOutlet weak var tblTime : UITableView?
    @IBOutlet weak var tblTimeHeight : NSLayoutConstraint?
    
    var arrTime = [timeSloat]()
    var delegate:BusinessHoursTableCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        tblTime?.delegate = self
        tblTime?.dataSource = self 
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension BusinessHoursTableCell: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrTime.count+1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblTime?.dequeueReusableCell(withIdentifier: "TimeTableCell") as! TimeTableCell
        if indexPath.row == arrTime.count{
            cell.timeView?.isHidden = true
        }else{
            cell.timeView?.isHidden = false
            let objTimes = self.arrTime[indexPath.row]
            cell.lblEndTime?.text = objTimes.strEndTime
            cell.lblStartTime?.text = objTimes.strStartTime
        }
        cell.btnDelete?.isHidden = false
        cell.btnDelete?.tag = indexPath.row

        if objAppShareData.imageShow == true{
        }
        if arrTime.count == 1{
            cell.btnDelete?.isHidden = true
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        let userInfo = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] ?? ["":""]
        let type = userInfo[UserDefaults.keys.businessType] as? String ?? ""
//        if type == "independent"{
//        } else{
            if indexPath.row == arrTime.count{
            }else{
                self.delegate?.selectedTimeSloat(withSenderTable:tableView ,timeArrayIndex:indexPath.row)
            }
       // }
    }
}

fileprivate extension BusinessHoursTableCell{
    @IBAction func btnRemoveObject(_ sender:UIButton){
        self.delegate?.removeTimeSloat(withSenderTable: self.tblTime!, timeArrayIndex: sender.tag)
    }
}

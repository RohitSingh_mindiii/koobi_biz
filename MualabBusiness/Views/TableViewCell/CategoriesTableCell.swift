//
//  CategoriesTableCell.swift
//  MualabBusiness
//
//  Created by Mac on 20/01/2018 .
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

class CategoriesTableCell: UITableViewCell {

    @IBOutlet weak var btnAdd:UIButton!
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var imgDropDown:UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

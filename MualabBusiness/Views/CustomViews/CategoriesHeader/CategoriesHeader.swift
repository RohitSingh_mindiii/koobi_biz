//
//  CategoriesHeader.swift
//  MualabBusiness
//
//  Created by Mac on 20/01/2018 .
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

class CategoriesHeader: UITableViewHeaderFooterView {
    
    @IBOutlet weak var btnAdd:UIButton!
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var imgDropDown:UIImageView!
    @IBOutlet weak var lblBotumeLayer: UILabel!
    @IBOutlet weak var viewHeaderBG: UIView!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}


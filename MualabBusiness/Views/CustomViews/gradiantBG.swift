//
//  gradiantBG.swift
//  MualabBusiness
//
//  Created by Mac on 21/12/2017 .
//  Copyright © 2017 Mindiii. All rights reserved.
//

import UIKit

class gradiantBG: UIView {

    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
            let gradientLayer = CAGradientLayer()
            gradientLayer.frame = self.bounds
            let color1 = UIColor.theameColors.darkColor
            let color2 = UIColor.theameColors.pinkColor
        if self.frame.size.height > 200{
            gradientLayer.colors = [color1.cgColor,color2.cgColor]
            gradientLayer.startPoint = CGPoint.init(x: 0.6, y: 0.0)
            gradientLayer.endPoint = CGPoint.init(x: 1.0, y: 1.0)
        }else{
            gradientLayer.colors = [color2.cgColor,color1.cgColor]
        }
            //self.layer.insertSublayer(gradientLayer,at:0)
              self.layer.addSublayer(gradientLayer)
    }

}

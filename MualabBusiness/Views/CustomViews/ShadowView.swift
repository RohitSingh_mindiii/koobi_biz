//
//  ShadowView.swift
//  MualabBusiness
//
//  Created by Mac on 03/01/2018 .
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit

class ShadowView: UIView {
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.56
        self.layer.shadowOffset = CGSize(width:0,height:0)
       // self.layer.shadowRadius = 0.0
        self.layer.shadowPath = UIBezierPath(rect: CGRect.init(x:-2, y: 2, width: self.frame.size.width+2, height: self.frame.size.height-2)).cgPath
    }
}


class ShadowNormel: UIView {
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.25
        self.layer.shadowOffset = CGSize(width:0,height:0)
        // self.layer.shadowRadius = 0.0
        self.layer.shadowPath = UIBezierPath(rect: CGRect.init(x:-2, y: 2, width: self.frame.size.width+2, height: self.frame.size.height-2)).cgPath
    }
}

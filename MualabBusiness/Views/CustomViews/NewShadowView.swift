//
//  NewShadowView.swift
//  MualabBusiness
//
//  Created by Mac on 16/03/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import Foundation
import UIKit

class NewShadowView: UIView {
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    
    override func draw(_ rect: CGRect) {
        // Drawing code
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOpacity = 0.4//1.0
        self.layer.shadowOffset = CGSize(width:0,height:0)
        self.layer.shadowPath = UIBezierPath(rect: CGRect.init(x: -0.5, y: 0.5, width: self.frame.size.width + 0.5 , height: self.frame.size.height - 0.5)).cgPath
    }
}

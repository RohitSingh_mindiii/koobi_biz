//
//  IBDesignableExtension.swift
//  MualabCustomer
//
//  Created by Mac on 16/01/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import Foundation
import UIKit

//MARK: - UIView
//@IBDesignable 
//extension  UIView{
//
//    @IBInspectable var cornerRadius: CGFloat {
//            get {
//                return layer.cornerRadius
//            }
//            set {
//                layer.cornerRadius = newValue
//                layer.masksToBounds = newValue > 0
//            }
//    }
//
//    @IBInspectable var borderWidth: CGFloat {
//        get {
//            return layer.borderWidth
//        }
//        set {
//            layer.borderWidth = newValue
//        }
//    }
//
//    @IBInspectable var borderColor: UIColor? {
//
//        set {
//            guard let uiColor = newValue else { return }
//            layer.borderColor = uiColor.cgColor
//        }
//        get {
//            guard let color = layer.borderColor else { return nil }
//            return UIColor(cgColor: color)
//        }
//    }
//}

//@IBDesignable extension UIButton {
//
//    @IBInspectable override var borderWidth: CGFloat {
//        set {
//            layer.borderWidth = newValue
//        }
//        get {
//            return layer.borderWidth
//        }
//    }
//
//    @IBInspectable var cornerRadius: CGFloat {
//        set {
//            layer.cornerRadius = newValue
//        }
//        get {
//            return layer.cornerRadius
//        }
//    }
//
//    @IBInspectable override var borderColor: UIColor? {
//        set {
//            guard let uiColor = newValue else { return }
//            layer.borderColor = uiColor.cgColor
//        }
//        get {
//            guard let color = layer.borderColor else { return nil }
//            return UIColor(cgColor: color)
//        }
//    }
//}


//extension UIView{
//
//    //    @IBInspectable var cornerRadius: CGFloat {
//    //        get {
//    //            return layer.cornerRadius
//    //        }
//    //        set {
//    //            layer.cornerRadius = newValue
//    //            layer.masksToBounds = newValue > 0
//    //        }
//    //    }
//
//    @IBInspectable var borderWidth: CGFloat {
//        get {
//            return layer.borderWidth
//        }
//        set {
//            layer.borderWidth = newValue
//        }
//    }
//
//    @IBInspectable var borderColor: UIColor? {
//        get {
//            return UIColor(cgColor: layer.borderColor!)
//        }
//        set {
//            layer.borderColor = newValue?.cgColor
//        }
//    }
//}



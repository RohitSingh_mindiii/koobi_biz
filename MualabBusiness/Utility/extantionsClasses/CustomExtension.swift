//
//  CustomExtension.swift
//  Mualab
//
//  Created by MINDIII on 11/1/17.
//  Copyright © 2017 MINDIII. All rights reserved.
//

import UIKit
import CoreLocation
import Foundation
var strAddress = ""
var latitude : String? = ""
var longitude : String? = ""
let objCustom:CustomExtension = CustomExtension()

let colors:UIColor = #colorLiteral(red: 0.3672305346, green: 0.5523084402, blue: 0.5752130747, alpha: 1)
class CustomExtension: NSObject {
    var locationManager = CLLocationManager()

}
 var locationManager = CLLocationManager()
extension UIImageView
{
    func setImageProperty() {
        layer.backgroundColor = UIColor.clear.cgColor
        layer.borderWidth = 1.5
        //self.layer.borderColor=[commenColorRed CGColor];
    }
    func setBound(){
        layer.cornerRadius = layer.frame.size.height / 2
        layer.masksToBounds = true
    }
    
    func setImageFream() {
        layer.cornerRadius = layer.frame.size.height / 2
        layer.masksToBounds = true
        layer.borderWidth = 3.0
        layer.borderColor = colors.cgColor
    }
    func setImageFreamWhite() {
        layer.cornerRadius = layer.frame.size.height / 2
        layer.masksToBounds = true
        layer.borderWidth = 3.0
        layer.borderColor = UIColor.white.cgColor
    }
    func setImageFreamNew() {
        let borderColor = #colorLiteral(red: 0.3672305346, green: 0.5523084402, blue: 0.5752130747, alpha: 1)
        layer.cornerRadius = layer.frame.size.height / 2
        layer.masksToBounds = true
        layer.borderWidth = 3.0
        layer.borderColor = borderColor.cgColor
    }

    
}
extension UIView
{
    func setViewShadow(_ opacity: Float) {
        let shadowPath = UIBezierPath(rect: CGRect(x: 0, y: 0, width: frame.size.width, height: frame.size.height + 1))
        //   self.backgroundColor     = [UIColor whiteColor];
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        layer.shadowOpacity = opacity
        layer.shadowPath = shadowPath.cgPath
    }
    
    func setShadow3D(_ opacity: Float) {
        let shadowPath = UIBezierPath(rect: CGRect(x: 2, y: 2, width: frame.size.width - 2, height: frame.size.height - 2))
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        layer.shadowOpacity = opacity
        layer.shadowPath = shadowPath.cgPath
    }
}




extension UIViewController{
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
}

//MARK:- Image rotation extension code
extension UIImage {
    
    func fixedOrientation() -> UIImage {
        // No-op if the orientation is already correct
        if (imageOrientation == UIImage.Orientation.up) {
            return self
        }

        var transform:CGAffineTransform = CGAffineTransform.identity
        
        ////
        //var isDone = false
        if (UIDevice.current.orientation == .landscapeLeft) {
            transform = transform.translatedBy(x: 0, y: size.height);
            //isDone = true
            transform = transform.rotated(by: CGFloat(-M_PI_2));
            print("landscapeLeft")
        }
        if (UIDevice.current.orientation == .landscapeRight) {
            transform = transform.translatedBy(x: 0, y: size.height);
            transform = transform.rotated(by: CGFloat(-M_PI_2));
        }
        ////
        
        if (imageOrientation == UIImage.Orientation.down
            || imageOrientation == UIImage.Orientation.downMirrored) {
            
            transform = transform.translatedBy(x: size.width, y: size.height)
            transform = transform.rotated(by: CGFloat(M_PI))
        }
        if (imageOrientation == UIImage.Orientation.left
            || imageOrientation == UIImage.Orientation.leftMirrored) {
            
            transform = transform.translatedBy(x: size.width, y: 0)
            transform = transform.rotated(by: CGFloat(M_PI_2))
        }
        
        if (imageOrientation == UIImage.Orientation.right
            || imageOrientation == UIImage.Orientation.rightMirrored) {
            //if !isDone{
                transform = transform.translatedBy(x: 0, y: size.height);
                transform = transform.rotated(by: CGFloat(-M_PI_2));
            //}else{
                //isDone = false
            //}
        }
        
        if (imageOrientation == UIImage.Orientation.upMirrored
            || imageOrientation == UIImage.Orientation.downMirrored) {
            
            transform = transform.translatedBy(x: size.width, y: 0)
            transform = transform.scaledBy(x: -1, y: 1)
        }
        
        if (imageOrientation == UIImage.Orientation.leftMirrored
            || imageOrientation == UIImage.Orientation.rightMirrored) {
            
            transform = transform.translatedBy(x: size.height, y: 0);
            transform = transform.scaledBy(x: -1, y: 1);
        }
        
        // Now we draw the underlying CGImage into a new context, applying the transform
        // calculated above.
        let ctx:CGContext = CGContext(data: nil, width: Int(size.width), height: Int(size.height),
                                      bitsPerComponent: cgImage!.bitsPerComponent, bytesPerRow: 0,
                                      space: cgImage!.colorSpace!,
                                      bitmapInfo: cgImage!.bitmapInfo.rawValue)!
        
        ctx.concatenate(transform)
        
        
        if (imageOrientation == UIImage.Orientation.left
            || imageOrientation == UIImage.Orientation.leftMirrored
            || imageOrientation == UIImage.Orientation.right
            || imageOrientation == UIImage.Orientation.rightMirrored
            ) {
            ctx.draw(cgImage!, in: CGRect(x:0,y:0,width:size.height,height:size.width))
        } else {
            ctx.draw(cgImage!, in: CGRect(x:0,y:0,width:size.width,height:size.height))
        }
        
        // And now we just create a new UIImage from the drawing context
        let cgimg:CGImage = ctx.makeImage()!
        var imgEnd:UIImage = UIImage(cgImage: cgimg)
        
        ////
        if (UIDevice.current.orientation == .landscapeLeft) {
            imgEnd = imgEnd.rotate(byAngle: CGFloat(M_PI))
        }
        ////
        return imgEnd
    }
}

extension CustomExtension:CLLocationManagerDelegate {
    func getLocation() -> Void {
        if (CLLocationManager.locationServicesEnabled()){
            locationManager.delegate = self;
            locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
            DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
              //  self.locationManager.stopUpdatingLocation()
            }
        }else{
        }
    }
    
    //MARK: - Delegates
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let locValue:CLLocationCoordinate2D = (manager.location?.coordinate)!
        

        
        let VarLat  = Double(locValue.latitude).rounded(toPlaces:5)
        let VarLong  = Double(locValue.longitude).rounded(toPlaces:5)
 
        latitude = String(VarLat)
        longitude = String(VarLong)
        
        let lcc = CLLocation.init(latitude: VarLat, longitude: VarLong)
        
        strAddress = getAddressFromLocation(location: lcc)
    }
    
    func getAddressFromLocation(location:CLLocation) ->String{
        var addressString = String()
        
        CLGeocoder().reverseGeocodeLocation(location,completionHandler: {(placemarks, error) -> Void in
            self.locationManager.stopUpdatingLocation()
            if error != nil {
                return
            }
            
            if (placemarks?.count)! > 0 {
                let pm = placemarks?.last
                
                if let formattedAddress = pm?.addressDictionary?["FormattedAddressLines"] as? [String] {
                    addressString = formattedAddress.joined(separator: ", ")
                    strAddress = addressString
                }
                
 
            }
            else {
             }
        })
        return addressString
    }
}

extension FloatingPoint {
    
    typealias Exponent = Int // is this okay? how can I write where `Exponent: Int` ?
    
    public func rounded(toPlaces places: Int) -> Self {
        guard places >= 0 else { return self }
        let divisor = Self(Int(pow(10.0, Double(places))))
        //let divisor = Self(sign: .plus, exponent: places, significand: Self(Int(pow(5, Double(places)))))
        return (self * divisor).rounded() / divisor
    }
}
extension CustomExtension{
    func converteDateIntoNewFormate(strDateFromServer: String) -> (String) {
        
        // "yyyy-MM-dd"  -> "dd MMM yyyy"
        // 2017-12-30  -> 30 Dec 2017
        var strConvertedDate : String = ""
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let dateFromServer: Date? = dateFormatter.date(from: strDateFromServer)
        if let dateFromServer = dateFromServer {
            dateFormatter.dateFormat = "dd/MM/yyyy"
            let strDate:String? = dateFormatter.string(from: dateFromServer)
            if let strDate = strDate {
                strConvertedDate = strDate
            }
        }

        return strConvertedDate
    }
    
    
    
//    let strDateFromServer = "2017-12-30"
//    let strDateConverted = converteDateIntoNewFormate(strDateFromServer: strDateFromServer)
    
    
    func converteTimeIntoNewFormate(strTimeFromServer: String) -> (String) {
        
        var strConvertedTime : String = ""
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss"
        
        let timeFromServer: Date? = dateFormatter.date(from: strTimeFromServer)
        
        if let timeFromServer = timeFromServer {
            
            dateFormatter.dateFormat = "hh:mm a"
            
            let strTime:String? = dateFormatter.string(from: timeFromServer)
            
            if let strTime = strTime {
                strConvertedTime = strTime
            }
        }

        return strConvertedTime
    }
}

//MARK: - image compreation
extension CustomExtension{
    
    func compressImage(image:UIImage) -> Data? {
        // Reducing file size to a 10th
        
        var actualHeight : CGFloat = image.size.height
        var actualWidth : CGFloat = image.size.width
        let maxHeight : CGFloat = 681.0
        let maxWidth : CGFloat = 384.0
        var imgRatio : CGFloat = actualWidth/actualHeight
        let maxRatio : CGFloat = maxWidth/maxHeight
        var compressionQuality : CGFloat = 0.5
        
        if (actualHeight > maxHeight || actualWidth > maxWidth){
            if(imgRatio < maxRatio){
                //adjust width according to maxHeight
                imgRatio = maxHeight / actualHeight
                actualWidth = imgRatio * actualWidth
                actualHeight = maxHeight
            }
            else if(imgRatio > maxRatio){
                //adjust height according to maxWidth
                imgRatio = maxWidth / actualWidth
                actualHeight = imgRatio * actualHeight
                actualWidth = maxWidth
            }
            else{
                actualHeight = maxHeight
                actualWidth = maxWidth
                compressionQuality = 1
            }
        }
        
        let rect = CGRect(x: 0.0, y: 0.0, width: actualWidth, height: actualHeight)
        UIGraphicsBeginImageContext(rect.size)
        image.draw(in: rect)
        guard let img = UIGraphicsGetImageFromCurrentImageContext() else {
            return nil
        }
        UIGraphicsEndImageContext()
        guard let imageData = img.jpegData(compressionQuality: compressionQuality)else{
            return nil
        }
        return imageData
    }
    
    func compressImageCertificate(image:UIImage) -> Data? {
        // Reducing file size to a 10th
        
        var actualHeight : CGFloat = image.size.height
        var actualWidth : CGFloat = image.size.width
        let maxHeight : CGFloat = 681.0
        let maxWidth : CGFloat = 384.0
        var imgRatio : CGFloat = actualWidth/actualHeight
        let maxRatio : CGFloat = maxWidth/maxHeight
        var compressionQuality : CGFloat = 0.8
        
        if (actualHeight > maxHeight || actualWidth > maxWidth){
            if(imgRatio < maxRatio){
                //adjust width according to maxHeight
                imgRatio = maxHeight / actualHeight
                actualWidth = imgRatio * actualWidth
                actualHeight = maxHeight
            }
            else if(imgRatio > maxRatio){
                //adjust height according to maxWidth
                imgRatio = maxWidth / actualWidth
                actualHeight = imgRatio * actualHeight
                actualWidth = maxWidth
            }
            else{
                actualHeight = maxHeight
                actualWidth = maxWidth
                compressionQuality = 1
            }
        }
        
        let rect = CGRect(x: 0.0, y: 0.0, width: actualWidth, height: actualHeight)
        UIGraphicsBeginImageContext(rect.size)
        image.draw(in: rect)
        guard let img = UIGraphicsGetImageFromCurrentImageContext() else {
            return nil
        }
        UIGraphicsEndImageContext()
        guard let imageData = img.jpegData(compressionQuality: compressionQuality)else{
            return nil
        }
        return imageData
    }
}

extension Date {
    var ticksNewDefault: UInt64 {
        return UInt64((self.timeIntervalSince1970 + 62_135_596_800) * 10_000_000)
    }
}
//MARK: - Current Time
extension CustomExtension{
    
    func getTodayString() -> String{
        
        let date = Date()
        let calender = Calendar.current
        let components = calender.dateComponents([.year,.month,.day,.hour,.minute,.second], from: date)
        let year = components.year
        let month = components.month
        let day = components.day
        let hour = components.hour
        let minute = components.minute
        let second = components.second
        
        
        let today_string = String(year!) + "-" + String(month!) + "-" + String(day!) + " " + String(hour!)  + ":" + String(minute!) + ":" +  String(second!)
        return today_string
    }
}
//MARK: - Current Time
extension CustomExtension{
    
    func getDateToFormateString(strDate:String) -> String{
        let seperateBy = "-"// set input date formet by seperater
        
        var arrDate = strDate.components(separatedBy: seperateBy)
        let strTame1: String = arrDate [0]
        let strTame2: String = arrDate [1]
        var strTame3: String = arrDate [2]
        var arrDate1 = strTame3.components(separatedBy: " ")
        strTame3 = arrDate1 [0]
        var newDate = ""
              if strTame2 == "01" || strTame2 == "1"{
            newDate = "Jan"
        }else if strTame2 == "02" || strTame2 == "2"{
            newDate = "Feb"
        }else if strTame2 == "03" || strTame2 == "3"{
            newDate = "Mar"
        }else if strTame2 == "04" || strTame2 == "4"{
            newDate = "Apr"
        }else if strTame2 == "05" || strTame2 == "5"{
            newDate = "May"
        }else if strTame2 == "06" || strTame2 == "6"{
            newDate = "Jun"
        }else if strTame2 == "07" || strTame2 == "7"{
            newDate = "Jul"
        }else if strTame2 == "08" || strTame2 == "8"{
            newDate = "Aug"
        }else if strTame2 == "09" || strTame2 == "9"{
            newDate = "Sep"
        }else if strTame2 == "10"{
            newDate = "Oct"
        }else if strTame2 == "11"{
            newDate = "Nov"
        }else if strTame2 == "12"{
            newDate = "Dec"
        }else{
            newDate = ""
        }
        newDate = strTame1 + "-" + newDate + "-" + strTame3
        return newDate
    }
    
    
    func getMonthName(strTame2:String) -> String{

        var newDate = ""
        if strTame2 == "01" || strTame2 == "1"{
            newDate = "Jan"
        }else if strTame2 == "02" || strTame2 == "2"{
            newDate = "Feb"
        }else if strTame2 == "03" || strTame2 == "3"{
            newDate = "Mar"
        }else if strTame2 == "04" || strTame2 == "4"{
            newDate = "Apr"
        }else if strTame2 == "05" || strTame2 == "5"{
            newDate = "May"
        }else if strTame2 == "06" || strTame2 == "6"{
            newDate = "Jun"
        }else if strTame2 == "07" || strTame2 == "7"{
            newDate = "Jul"
        }else if strTame2 == "08" || strTame2 == "8"{
            newDate = "Aug"
        }else if strTame2 == "09" || strTame2 == "9"{
            newDate = "Sep"
        }else if strTame2 == "10"{
            newDate = "Oct"
        }else if strTame2 == "11"{
            newDate = "Nov"
        }else if strTame2 == "12"{
            newDate = "Dec"
        }else{
            newDate = ""
        }
        return newDate
    }
}

//MARK: - Current Time
extension CustomExtension{

    func TimeFotmeteInAMPM(strTime:String) -> String{

        let seperateBy = ":"// set input date formet by seperater
        var newDate = ""
        var arrDate = strTime.components(separatedBy: seperateBy)
        let strTime1: String = arrDate [0]
        let strTime2: String = arrDate [1]

        var str1:Int = Int(strTime1)!
        if str1 > 12 {
           str1 = str1 - 12
            let new = String(str1)
            newDate = new+":"+strTime2
        }else{
            newDate = strTime
        }
        return newDate
    }
}
//MARK: - Current Time
extension CustomExtension{
    
    func DateMatches(strFirstDate:String, strSecondDate:String) -> String{
        var date = ""
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let firstDate = formatter.date(from: strFirstDate)
        let secondDate = formatter.date(from: strSecondDate)

        if firstDate?.compare(secondDate!) == .orderedSame {
            date = ""
        }
        else if firstDate?.compare(secondDate!) == .orderedAscending {
            date = "Small"

        }
        else if firstDate?.compare(secondDate!) == .orderedDescending {
            date = "Big"

        }
        
        return date
    }
}
// MARK: - chat room extension
extension CustomExtension{
 func ChatRoom(strId1:String, strId2:String) -> String{
    var charRoom = ""
    let id1 = Int(strId1) ?? 0
    let id2 = Int(strId2) ?? 0
    if id1 < id2 {
        charRoom = String(id1)+"_"+String(id2)
    }else {
        charRoom = String(id2)+"_"+String(id1)
    }
    return charRoom
    }
}

// MARK: - chat room extension
extension CustomExtension{
    func typeManage(strKey:String,dict:[String:Any]) -> String {
        var value = ""
        if let ids = dict[strKey] as? String{
            value = ids
        }else if let ids = dict[strKey] as? Int{
            value = String(ids)
        }
        return value
    }
}


extension UIView {
    func takeScreenshot() -> UIImage {
        // Begin context
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, UIScreen.main.scale)
        
        // Draw view in that context
        drawHierarchy(in: self.bounds, afterScreenUpdates: true)
        
        // And finally, get image
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
}
extension String
{
    func fromBase64() -> String? {
        guard let data = Data(base64Encoded: self) else {
            return nil
        }
        return String(data: data, encoding: .utf8)
    }
    func toBase64() -> String {
        return Data(self.utf8).base64EncodedString()
    }
    public func encode(_ s: String) -> String {
//         let data = s.data(using: .nonLossyASCII, allowLossyConversion: true)!
//         return String(data: data, encoding: .utf8)!
        return s
     }
     public func decode(_ s: String) -> String? {
//        let data = s.data(using: .utf8, allowLossyConversion: false)!
//        return String(data: data, encoding: .nonLossyASCII)
        return s
     }
    
    public func getAcronyms(separator: String = "") -> String
    {
        let acronyms = self.components(separatedBy: " ").map({ String($0.first ?? " ") }).joined(separator: separator);
        return acronyms;
    }
}


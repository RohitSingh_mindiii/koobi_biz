//
//  customColor.swift
//  MualabBusiness
//
//  Created by Mac on 25/12/2017 .
//  Copyright © 2017 Mindiii. All rights reserved.
//

import Foundation
import UIKit

//MARK: - UIColor
extension UIColor{
    enum theameColors {
         static let grayColor = UIColor.init(red: 130.0/255.0, green: 130.0/255.0, blue: 130.0/255.0, alpha: 1.0)
        
        static let pinkColor = UIColor.init(red: 248.0/255.0, green: 50.0/255.0, blue: 114.0/255.0, alpha: 1.0)
        
        static let darkColor = UIColor.init(red: 76.0/255.0, green: 53.0/255.0, blue: 73.0/255.0, alpha: 1.0)
        
        static let blueColor = UIColor.init(red: 63.0/255.0, green: 142.0/255.0, blue: 252.0/255.0, alpha: 1.0)
        
        static let backgroundColor = UIColor.init(red: 242.0/255.0, green: 242.0/255.0, blue: 242.0/255.0, alpha: 1.0)
        
        static let greenCalendar = UIColor.init(red: 0.0/255.0, green: 187.0/255.0, blue: 39.0/255.0, alpha: 1.0)
        
        static let skyBlueNewTheam = UIColor.init(red: 0.0/255.0, green: 211.0/255.0, blue: 201.0/255.0, alpha: 1.0)

    }
}

//MARK: - Userdeafults
extension UserDefaults{
    enum keys {
        static let baseURL = "baseURL"
        static let EditProfileAddress = "EditProfileAddress"
        static let business_id = "business_id"
        static let staff_id = "staff_id"
        static let userInfo = "userInfo"
        static let userData = "userData"
        static let businessInfoDic = "businessInfoDic"
        static let stripeCustomerId = "stripeCustomerId"
        static let userInfoDic = "userInfoDic"
        static let userId = "user_id"
        static let authToken = "auth_token"
        static let isLoggedIn = "isLoggedIn"
        static let service = "service"
        static let subCategories = "sub_categories"
        static let businessHour = "business_hour"
        static let businessHourCount = "business_hour_count"
        static let outCallData = "out_call_data"
        static let outCallPreparationTime = "out_call_prepration_time"
        static let inCallPreparationTime = "in_call_prepration_time"
        static let bio = "bio"
        static let bankStatus = "bank_status"
        static let isDocument = "is_document"
        static let paymentType = "payment_type"
        //deependra
        static let businessType = "business_type"
        static let mainServiceType = "main_service_type"
        static let bookingSetting = "booking_setting"
        static let navigationVC = "navigationVC"
        static let newAdminId = "newAdminId"
        static let payOption = "pay_option"
        static let notificationStatus = "notification_status"
        static let newAdminBookingType = "new_admin_booking_type"

        static let mainBusinessTypeArray = "mainBusinessTypeArray"
        static let mainCategoryTypeArray = "mainCategoryTypeArray"

        static let businessNameType = "business_name_type"
        static let lastBookingTime = "lastBookingTime"
        
        static let OTP = "OTP"
        static let  v = "__v"
        static let id = "_id"
        static let address = "address"
        static let address2 = "address2"
        static let appType = "appType"
 
        static let buildingNumber = "buildingNumber"
        static let businessName = "business_name";
        static let businesspostalCode = "businesspostalCode"
        static let certificateCount = "certificateCount"
        static let chatId = "chatId"
        static let city = "city"
        static let contactNo = "contact_no"
        static let country = "country"
        static let countryCode = "country_code"
        static let crd = "crd"
        static let deviceToken = "device_token"
        static let deviceType = "device_type"
        static let dob = "dob"
        static let businessEmail = "businessEmail"

        
        static let email = "email"
        static let firebaseToken = "firebase_token"
        static let firstName = "first_name"
        static let followersCount = "followersCount"
        static let followingCount = "followingCount"
        static let gender = "gender"
        static let inCallpreprationTime = "in_call_prepration_time"
        static let serviceType = "service_type"
        static let socialId = "social_id"
        static let socialType = "social_type"
        static let state = "state"
        static let status = "status"
        static let upd = "upd"
        static let userName = "user_name"
        static let userType = "user_type"
        static let serviceCount = "serviceCount"
        static let reviewCount = "reviewCount"
        static let ratingCount = "ratingCount"
        static let radius = "radius"
        static let profileImage = "profile_image"
        static let postCount = "postCount"
        static let password = "password"
        static let outCallpreprationTime = "out_call_prepration_time"
        static let otpVerified = "otpVerified"
        static let mailVerified = "mailVerified"
        static let longitude = "longitude"
        static let latitude = "latitude"
        static let lastName = "last_name"
 
        static let trackingLongitude = "tracking_longitude"
        static let trackingLatitude = "tracking_latitude"

        
        static let businessContactNo = "business_contact_no"
        static let incallTimeHours = "incallTimeHours"
        static let incallTimeMinute = "incallTimeMinute"
        static let outcallTimeHours = "outcallTimeHours"
        static let outcallTimeMinute = "outcallTimeMinute"
        
        
        static let myCurrentLat = "myCurrentLat"
        static let myCurrentLong = "myCurrentLong"
        static let myCurrentAdd = "myCurrentAdd"
        
        static let CustomerLat = "CustomerLat"
        static let CustomerLong = "CustomerLong"
        static let CustomerId = "CustomerId"
        static let CustomerBookingInfoId = "CustomerBookingInfoId"
        static let CustomerBookingId = "CustomerBookingId"
        static let ParamCustomerTracking = "ParamCustomerTracking"

        static let myId = "myId"
        static let myName = "myName"
        static let myImage = "myImage"
    }
    
    enum myDetail {
        static let OTP = "OTP"
        static let  v = "__v"
        static let id = "_id"
        static let Userid = "user_id"
        static let business_id = "business_id"
        static let staff_id = "staff_id"
        static let address = "address"
        static let address2 = "address2"
        static let appType = "app_type"
        static let authToken = "auth_token"
        static let bankStatus = "bank_status"
        static let bio = "bio"
        static let buildingNumber = "buildingNumber"
        static let businessName = "business_name";
        static let businessType = "business_type"
        static let businesspostalCode = "businesspostalCode"
        static let certificateCount = "certificateCount"
        static let chatId = "chatId"
        static let city = "city"
        static let contactNo = "contact_no"
        static let country = "country"
        static let countryCode = "country_code"
        static let crd = "crd"
        static let deviceToken = "device_token"
        static let deviceType = "device_type"
        static let dob = "dob"
        static let email = "email"
        static let firebaseToken = "firebase_token"
        static let firstName = "first_name"
        static let followersCount = "followersCount"
        static let followingCount = "followingCount"
        static let gender = "gender"
        static let inCallpreprationTime = "in_call_prepration_time"
        static let serviceType = "service_type"
        static let socialId = "social_id"
        static let socialType = "social_type"
        static let state = "state"
        static let status = "status"
        static let upd = "upd"
        static let userName = "user_name"
        static let userType = "user_type"
        static let serviceCount = "service_count"
        static let reviewCount = "review_count"
        static let ratingCount = "rating_count"
        static let radius = "radius"
        static let profileImage = "profile_image"
        static let postCount = "post_count"
        static let password = "password"
        static let outCallpreprationTime = "out_call_prepration_time"
        static let otpVerified = "otp_verified"
        static let mailVerified = "mail_verified"
        static let longitude = "longitude"
        static let latitude = "latitude"
        static let lastName = "last_name"
        static let isDocument = "is_document"
        
        static let businessContactNo = "business_contact_no"
        static let incallTimeHours = "incallTimeHours"
        static let incallTimeMinute = "incallTimeMinute"
        static let outcallTimeHours = "outcallTimeHours"
        static let outcallTimeMinute = "outcallTimeMinute"
        static let payOption = "pay_option"
        static let myCurrentLat = "myCurrentLat"
        static let myCurrentLong = "myCurrentLong"
        static let myCurrentAdd = "myCurrentAdd"
    }
}

//Notification names
struct notificationName {
    static let postFeedSuccess = "postFeedSuccess"
    static let postUploaded = "postUploaded"
    static let searchText = "searchText"
}
//MARK: - validation message
struct message {
    
    //static let noNetwork = "There's no network connection. Make sure the you're connected to a Wi-fi or mobile network and try again."
    static let noNetwork = "Your internet seems too slow to reach our server."
    static let accountActive = "Sorry,your account is inactive. Please Contact your Administrator."

    static let sessionExpire = "Your session has been expired. Please re-login to renew your session."
    static let invalidToken = "Invalid Auth Token"
    enum validation{
        static let genderSelection = "Please select gender."
        static let required = "Please enter required field."
        static let email = "Please enter a valid email address."
        static let firstName = "Please enter a valid first name."
        static let lastName = "Please enter a valid last name."

        static let invalidOTP = "Please enter valid OTP."
        static let businessNameLength = "Business name must be at least 4 characters long."
        static let dob = "Please select date of birth."

        static let userNameLength = "Username must be at least 4 characters long."
        static let userNameSpace = "Username cannot contains blank space."
        static let validPassword = "Use  8 or more characters including uppercase letters and numbers."
        static let passwordLength = "Passwords must be at least 8 characters long."
        static let incallMessage = "Please select incall buffer time."
        static let outcallMessage = "Please select outcall buffer time."
    }
    enum error {
        static let title = "Error"
        static let wrong = "Something went wrong at our end. Don't worry,it's not you - it's us. sorry about that."
    }
}


//MARK: - Alert Type
struct alertType {
    
    static let noNetwork = "noNetwork"
    static let error = "error"
    static let banner = "banner"
    static let bannerDark = "bannerDark"
    static let sessionExpire = "sessionExpire"
}

//MARK: - UIImage
extension UIImage{
    enum customImage {
//        static let error = UIImage(named:"worried-emoji")
//        static let noNetwork = UIImage(named:"wifi-2")
//        static let sessionExpire = UIImage(named:"sessionExpire")
//        session_expoird_ico
//        server_ico
//        server_eror_ico
//        account_inactive_ico
        
        static let error = UIImage(named:"server_eror_ico")
        static let noNetwork = UIImage(named:"server_ico")
        static let sessionExpire = UIImage(named:"session_expoird_ico")
        static let accountActive = UIImage(named:"account_inactive_ico")

        static let flashOn = UIImage(named:"flash_on")
        static let flashOff = UIImage(named:"flash_off")
        static let flashAuto = UIImage(named:"flash_automatic")
        static let camera = UIImage(named:"ico_camera")
        static let video = UIImage(named:"icoVideoRec")
        static let user = UIImage(named:"cellBackground")
    }
}

//MARK: - Keys
struct Keys {
    
    enum user {
        
        static let address = "address"
        static let type = "user_type"
        static let email = "email"
        static let countryCode = "country_code"
        static let contactNumber = "contact_number"
        static let firstName = "first_name"
        static let lastName = "last_name"
        static let businessName = "business_name"
        static let userName = "user_name"
        static let userId = "user_id"
        static let bankStatus = "bank_status"
        static let profileImage = "profile_image"
        static let bio = "bio"
        static let buildingNumber = "building_number"
        static let DOB = "dob"
        static let gender = "gender"
        
        enum addressComponent {
            static let fullAddress = "full_address"
            static let locality = "locality"
            static let address2 = "address2"
            static let city = "city"
            static let state = "state"
            static let postalCode = "businesspostalCode"
            static let country = "country"
            static let placeName = "place_name"
            static let latitude = "latitude"
            static let longitude = "longitude"
        }
    }
}

// MARK: - Call Options
struct callOptions {
    
    static let inCall = 1
    static let outCall = 2
    static let both = 3
}





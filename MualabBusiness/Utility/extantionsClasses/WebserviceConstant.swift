//
//  WebserviceConstant.swift
//  Mualab
//
//  Created by MINDIII on 10/25/17.
//  Copyright © 2017 MINDIII. All rights reserved.

import Foundation
import UIKit

let objWebserviceManager : WebServiceManager = WebServiceManager.sharedObject()
let k_success = "success"

//let stripeKey = "Bearer sk_test_0RIcHEnw2dna6r9qEk1yLWDU006s0c6hT4"  //// Dev
//let KUrlStripeId : String = "pk_test_2mCH7F6JmjI04AyuBpS29VpK00Srv2nBF7"  //// Dev

let stripeKey = "Bearer sk_live_yIqrIhWPMHynsvs4W23NJbaB00WgrhEygt"    //// Live
let KUrlStripeId : String = "pk_live_PfDNm4HgHKXuIK4yVCrr4ye500EJ4Cbm22" //// Live

class webUrl{
        
    static let BaseURL = "http://mindiii.com/mualab/service/"
    
    static let Login = "userLogin"
    static let ForgotPassword = "forgotPassword"
    static let Registration = "userRegistration"
    static let UserDataVerification = "userDataVerification"
    static let AddFeed =  "user/addFeeds"
    static let AddMyStory = "user/addMyStory"
    static let GetAllFeed = "user/getAllFeeds"
    static let GetUsers = "user/userList"
    static let LikeUnlikePost = "user/likes"
    static let GetLikeList = "user/likeList"
    static let GetCommnetList = "user/commentList"
    static let AddComment = "user/addComment"
    static let FollowUnfollowUser = "user/followUnfollow"
    static let LikeUnlikeComment = "user/commentLike"
    static let GetMyStories = "user/getMyStory"
    //http://mindiii.com/mualab/service/user/commentList
}

//MARK : - Webservices
struct WebURL {
    
   // static let BaseUrl = "https://koobi.co.uk/api/"  //>>Live
    //Rohit
    static let BaseUrl =  "http://192.168.1.215:3001/api/"  //>>Live
    static let termsURL = "https://koobi.co.uk/terms.pdf"
    static let privacyURL = "https://koobi.co.uk/privacy_policy.pdf"

//    static let BaseUrl = "https://dev.koobi.co.uk/api/"  //>>Dev
//    static let termsURL = "https://dev.koobi.co.uk/terms.pdf"
//    static let privacyURL = "https://dev.koobi.co.uk/privacy_policy.pdf"
   
    static let AboutUs = "http://koobi.co.uk/"
    static var Deeplinking = "http://koobi.co.uk/"
    
    static let registration = "auth/registration"
    static let login = "auth/user-login"
    static let phonVerification = "auth/send-auth-otp"
    static let verifyAuthOtp = "auth/verify-otp"
    static let checkUser = "auth/check-user"
    static let updateMyInfo = "business/update-user-info"
    static let allService = "allService"
    static let addBusinessHour = "business/add-business-hour"
    static let getBusinessSavedInfo = "business/get-business-profile?business_id="
    static let updateRange = "updateRange"
  //  static let addArtistService = "addArtistService"
    static let addArtistService = "service/add-artist-service"
    static let addService = "addService"
    static let getAllCertificate = "getAllCertificate"
    static let addArtistCertificate = "addArtistCertificate"
    static let deleteCertificate = "deleteCertificate"
   // static let addBankDetail = "addBankDetail"
    static let addBankDetail = "business/add-bank-detail"
    static let getBusinessProfile = "getBusinessProfile"
    static let updateRecord = "business/update-user-info"
    static let updateBusinessInfo = "business/update-business"
    static let skipPage = "business/skip-page"
    static let artistFreeSlotNew = "artistFreeSlotNew"
    static let artistBookingHistory = "artistBookingHistory"
    
    static let bookingAction = "bookingAction"
    static let bookingDetails = "bookingDetails"
    static let bookingupdate = "bookingupdate"
    
    
    static let allArtist = "allArtist"
    static let artistStaff = "artistStaff"
    static let getProfile = "getProfile"
    static let deleteService = "deleteService"
    static let editArtistCertificate = "editArtistCertificate"

    static let artistService = "artistService"
    static let staffInformation = "staffInformation"
    static let staffServices = "staffServices"

    static let addFeed = "addFeed"
    static let tagSearch = "tagSearch"

    // by deependra
    static let profileFeed = "profileFeed"
    static let getMyStoryUser = "getMyStoryUser"
    static let likeList = "likeList"
    static let followFollowing = "followFollowing"
    static let commentLike = "commentLike"
    static let commentList = "commentList"
    static let like = "like"
    static let addComment = "addComment"
    static let editComment = "editComment"
    static let followingList = "followingList"
    static let followerList = "followerList"

    static let addStaffService = "addStaffService"
    static let deleteStaffService = "deleteStaffService"
    static let addStaff = "addStaff"
    static let deleteStaff = "deleteStaff"
    static let changeStaff = "changeStaff"
    static let availableStaffList = "availableStaffList"
    static let companyInfo = "companyInfo"
    static let deleteCompany = "deleteCompany"

    static let exploreSearch = "exploreSearch"
    static let artistServices = "artistServices"
    

    static let profileUpdate = "profileUpdate"
    //Postcode API
    static let postcode = "https://api.postcodes.io/postcodes/"
    static let artistDetail = "artistDetail"
    

    //new api for business add catagory add on signup time
    static let myBusinessType = "business-type/my-business-type?business_id="
    //New API
    
  //  static let allBusinessTypeSearch = "allBusinessType"
    
    static let allBusinessTypeSearch = "business-type/get-business-type?business_type=&category=&business_id="
    static let allBusinessTypeSearchByID = "business-type/get-business-type?business_type=&category=&"
    static let searchAndAddCategory = "business-type/get-business-type?"
    static let allBusinessTypeSearchByType = "business-type/get-business-type?business_id=&category=&"
    
    static let myBusinessTypeShow = "business-type/get-my-business-type?business_type=&category=&business_id="
    
    static let mysubCategoryBusinessId = "mysubCategory"
    static let allSubcategorySearchBusinessId = "allSubcategory"
    
//    static let addSubCategory = "addSubCategory"
    static let addSubCategory = "category/add-category"
    static let addBusinessType = "business-type/add-business-type"
 
    static let deleteBussinessType = "business-type/delete-bussiness-type"
    //static let deleteCategory = "deleteCategory"
    static let deleteCategory = "category/delete-category"

    static let invitationUpdate = "invitationUpdate"

    static let allvoucherList = "allvoucherList"
    static let addVoucher = "addVoucher"
    static let deleteVoucher = "deleteVoucher"

    static let bankAccountDetail = "bankAccountDetail"
    static let confirmBooking = "confirmBooking"
    static let bookingDetail = "bookingDetail"
    
    static let userBookingHistory = "userBookingHistory"
    static let bookingReviewRating = "bookingReviewRating"
    static let getRatingReview = "getRatingReview"
    static let bookingCheck = "bookingCheck"
    static let getNotificationList = "getNotificationList"
    static let profileByUserName = "profileByUserName"
    static let checkTrackingBooking = "checkTrackingBooking"
    static let artistPayment = "artistPayment"
    static let nearByNotification = "nearByNotification"
    static let artistTimeSlot = "artistTimeSlot"
    static let customerList = "customerList"
    static let addCustomer = "addCustomer"
    static let applyVoucher = "applyVoucher"
    static let getBookedServices = "getBookedServices"
    static let deleteBookService = "deleteBookService"
    static let serviceStaff = "serviceStaff"
    static let bookArtist = "bookArtist"
    static let deleteUserBookService = "deleteUserBookService"
    static let artistTimeSlotNew = "artistTimeSlotNew"
    static let deleteClientBookService = "deleteClientBookService"
    static let bookingReport = "bookingReport"
    static let reportReason = "reportReason"
    static let addHoliday = "addHoliday"
    static let holidayList = "holidayList"
    static let bookingCount = "bookingCount"
    
    static let bookingAnalytics = "bookingAnalytics"
    static let addFavorite = "addFavorite"
    static let removeToFolder = "removeToFolder"
    static let paymentHistoryDelete = "paymentHistoryDelete"
    static let feedDetails = "feedDetails"
    static let userFeed = "userFeed"
    static let adminCommision = "adminCommision"
    static let serviceDetail = "serviceDetail"
    static let deleteFeed = "deleteFeed"
    static let deleteComment = "deleteComment"
    
    static let getFolderFeed = "getFolderFeed"
    static let getFolder = "getFolder"
    static let createFolder = "createFolder"
    static let deleteFolder = "deleteFolder"
    static let editFolder = "editFolder"
    static let saveToFolder = "saveToFolder"
    static let tagStaffServices = "tagStaffServices"
    static let bookingAdminCommsion = "bookingAdminCommsion"
    
    static let adminPayment = "adminPayment"
    static let adminReminder = "adminReminder"
    static let changepassword = "changepassword"
    static let walkingList = "walkingList"
    static let updateCustomer = "updateCustomer"
    
    static let notificationClear = "notificationClear"
 }





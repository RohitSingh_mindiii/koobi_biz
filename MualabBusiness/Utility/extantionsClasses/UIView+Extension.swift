//
//  UIView+Extension.swift
//  MualabCustomer
//
//  Created by Mac on 08/02/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import Foundation
import UIKit

extension UIView{
    
    //You can use UITableViewRowAction's backgroundColor to set custom image or view. The trick is using UIColor(patternImage:).
    
    func image() -> UIImage {
        UIGraphicsBeginImageContextWithOptions(bounds.size, isOpaque, 0)
        guard let context = UIGraphicsGetCurrentContext() else {
            return UIImage()
        }
        layer.render(in: context)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
}

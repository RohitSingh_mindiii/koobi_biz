//
//  LocationManager.swift
//  MualabCustomer
//
//  Created by Mac on 02/02/18.
//  Copyright © 2018 Mindiii. All rights reserved.
//

import UIKit
import CoreLocation

let objLocationManager = LocationManager.sharedObject()

class LocationManager: NSObject, CLLocationManagerDelegate{
    
    //Location
    var locationManager : CLLocationManager = CLLocationManager()
    var seenError : Bool = false
    var locationStatus : NSString = "Not Started"
    var strlatitude : String?
    var strlongitude : String?
    var strAddress : String = ""
    var strCurrentPlaceName: String = ""
    //Location
    var currentCLPlacemark : CLPlacemark?

    //MARK: - Shared object
    private static var sharedManager: LocationManager = {
        let manager = LocationManager()
        return manager
    }()
    
    // MARK: - Accessors
    class func sharedObject() -> LocationManager {
        return sharedManager
    }
}

//MARK: - Class functions
extension LocationManager {
    
    func getCurrentLocation(){
        
        //isAuthorizedtoGetUserLocation()
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.requestLocation();
        }
    }
    
    //if we have no permission to access user location, then ask user for permission.
    func isAuthorizedtoGetUserLocation() {
        
        if CLLocationManager.authorizationStatus() != .authorizedWhenInUse     {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    func getCurrentAdd(success:@escaping(CLPlacemark) ->Void, failure:@escaping (Error) ->Void) {
        
        let geoCoder = CLGeocoder()
        let latitude = Float(self.strlatitude ?? "0")
        let longitude = Float(self.strlongitude ?? "0")
        
        let location = CLLocation.init(latitude: CLLocationDegrees(String(format: "%.4f",latitude!)) ?? 0.0, longitude: CLLocationDegrees(String(format: "%.4f",longitude!)) ?? 0.0)
        geoCoder.reverseGeocodeLocation(location , completionHandler:
            {(placemarks, error) in
                
                if (error != nil){
                    failure(error!)
                }
                if let pm = placemarks{
                    if pm.count > 0 {
                        let pm1 = placemarks![0]
                        success(pm1)
                    }
                }
        })
    }
    
    func getAddressOfCurrentLocation(){
        
        objLocationManager.getCurrentAdd(success: { pm in
            
            var addressString : String = ""
            if pm.subLocality != nil {
                addressString = addressString + pm.subLocality! + ", "
            }
            if pm.thoroughfare != nil {
                addressString = addressString + pm.thoroughfare! + ", "
            }
            if pm.locality != nil {
                addressString = addressString + pm.locality! + ", "
            }
            if pm.country != nil {
                addressString = addressString + pm.country! + ", "
            }
            if pm.postalCode != nil {
                addressString = addressString + pm.postalCode! + " "
            }
            
            self.strAddress = addressString
        }) { error in
            
        }
    }
    
}

extension LocationManager {
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
    }
    
    //this method will be called each time when a user change his location access preference.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        if status == .authorizedWhenInUse {
        }
        
        var shouldIAllow = false
        
        switch status {
            
        case CLAuthorizationStatus.restricted:
            locationStatus = "Restricted Access to location"
        case CLAuthorizationStatus.denied:
            locationStatus = "User denied access to location"
        case CLAuthorizationStatus.notDetermined:
            locationStatus = "Status not determined"
        default:
            locationStatus = "Allowed to location Access"
            shouldIAllow = true
        }
        
        // NotificationCenter.defaultCenter().postNotificationName("LabelHasbeenUpdated", object: nil)
        
        if (shouldIAllow == true) {
            NSLog("Location to Allowed")
            // Start location services
            locationManager.startUpdatingLocation()
        } else {
            NSLog("Denied access: \(locationStatus)")
        }
    }
    
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if locations.first != nil {
            let locationArray = locations as NSArray
            if locationArray.count > 0{
                if let locationObj = locationArray.lastObject as? CLLocation{
                    let coord = locationObj.coordinate
                    strlatitude = coord.latitude.description
                    strlongitude = coord.longitude.description
                    self.getAddressOfCurrentLocation()
                    locationManager.stopUpdatingLocation()
                }
            }
            
        }
        
    }
    
}

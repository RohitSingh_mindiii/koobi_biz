//
//  AppShareData.swift
//  MualabBusiness
//
//  Created by Mac on 27/12/2017 .
//  Copyright © 2017 Mindiii. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import GooglePlaces
import INTULocationManager
import GoogleMaps
 import INTULocationManager
 import CoreLocation


let objAppShareData = AppShareData.sharedObject()

class AppShareData{
    
    
    var finalServiceTypeName = ""
    
    
//created by deependra
    let objModelFinalSubCategory = ModelFinelSubCategory()
    var param = ["serviceId":"",
                 "bookingType":"",
                 "title":"",
                 "description":"",
                 "incallPrice":"",
                 "outCallPrice":"",
                 "completionTime":""]
    
    
let objLoginVC = LoginVC()
let objModelHoldAddStaff = ModelHoldAddStaff()
let objUsersAddServiceVC = UsersAddServiceVC()
let objBookSerSelectionVC = BookingServiceSelectionVC()
//let objStaffListBookingVC = StaffListBookingVC()
let objBookingCalendarVC = BookingCalendarVC()
//let objAppoitmentBookingVC = AppoitmentBookingVC()
let objAddStaffDoneSubSubServices = AddStaffDoneSubSubServices()
var objModelServicesList = ModelServicesList(dict: [:])
let objAddStaffTimeSloat = AddStaffTimeSloat()
let objModelEditTimeSloat = ModelEditTimeSloat()
    var arrFeedsForArtistData = [feeds]()
    var isImageCrop:Bool = false
    var isComingFrom = ""
    var fromAddPost = false
    var isExploreGridToAllFeed:Bool = false
    var isFromNotification = false
    var forServiceTag = false
    var isFromDidFinish:Bool = false
    var notificationInfoDict : [String:Any]?
    var objModelBusinessSetup = ModelBusinessSetup()
    var notificationType = ""
    var arrTBottomSheetModal = [String]()
    var otpVerify = false
    var strExpSearchText : String = ""
    var onTracking = false
    var onTrackView = false
    var isFromChatToProfile = false
    var bookingInfiStatus = ""
    var arrAddedStaffServices = [ModelServicesList]()
    var arrAddedStaffServicesOriginal = [ModelServicesList]()
    var arrTagePeopleNameIds = [[String:Any]]()
    var arrTagePeopleNameData = [ExpSearchTag]()
    var fromLikeComment = false
    var objEditServiceVC = EditServiceVC()
    var workingHourse : Bool = false
var isOtherSelectedForProfile = false
    var strArtistID : String = ""
    var strCompanyID : String = ""
    var manageNavigation : Bool = false
    var editSelfServices = false
    var isBookingFromService = false
var btnAddHiddenONMyFolder = false
    var objAppdelegate:AppDelegate = AppDelegate()
    var strCurruntCountry:String!
    var strDeviceToken:String!
    var strFCMToken:String!
    var currentVCName:String!
    var isDeleteChat = false
    var isFileIsUploding = false
    var isCloseAddFeed = false
    var isSwitchToFeedTab = false
    var bookingType = "All Type"
    var fromDeleteButton = false
    var backView: UIView?
    var selectedTab = Int()
    var isSearchingUsingFilter = false
    var dictFilter : [String : Any] = [String : Any]()
     var arrSelectedService : [BookingServices] = [BookingServices]()
    var isFromEditBooking = false
    var isFromConfirmBooking = false
    var timer: Timer?
    var selectedOtherIdForProfile = ""
   // var objRefineData : RefineData = RefineData()
    var isFromPaymentScreen = false
    var isForBookingOutCallAddress : Bool = false
    var strBookingOutCallAddress : String = ""
    var latitudeBookingOutCallAddress : String = ""
    var longitudeBookingOutCallAddress : String = ""
    var strVoucherCode : String = ""
    var objServiceForEditBooking = SubSubService.init(dict: [:])
    var badgeContBookingNotification = 0
    var badgeContSocialNotification = 0
     var arrExpSearchServiceTag = [ExpSearchTag]()

var isFromPaymentDoneToAppointmentDetail = false
    //
 var arrSubSubService : [SubSubServices] = [SubSubServices]()
    
    ////
    var isCameraForFeed = false
    var imgForPostFeed:UIImage?
    var videoUrlForPostFeed:URL?
    var isVideoPostFeed:Bool = false
    ////
   
    var deviceToken : String = "abcd"
    //Rohit
    var firebaseToken : String = "idfjlksdjsdfds4f65d4f56d4sf64dsf4ds56f456ds4f6ds54f6sd54f"
    var isUpdateService : Bool = false
    var arrOfServices = [artistServices]()
 var selectedOtherTypeForProfile = ""
//created by deependra
    var fromStaffSelectCell = false
    var fromDetailPage = false
    var fromEditStaff = false
    var addStaffTimeSloat = ""
    var staffDeleted = false
    var editStaff = false

    var editStaffTimeSloat = false
    var imageShow = false
    var addTimeSloat = false
    //today pending
 
    var paramAddress = ["":""]
    var fromAddressVC = false
     var arrFeedsData = [feeds]()
    var onProfile = true
////
    
    //MARK: - Shared object
    private static var sharedManager: AppShareData = {
        let manager = AppShareData()
        return manager
    }()
    // MARK: - Accessors
    class func sharedObject() -> AppShareData {
        return sharedManager
    }
    
}
//MARK: - Class functions
extension AppShareData{
    
    //MARK:- Hide view with animation
    func setView(view: UIView, hidden: Bool) {
        UIView.transition(with: view, duration: 1.0, options: .transitionCrossDissolve, animations: {
            view.isHidden = hidden
        })
    }
    
    func validateUrl(_ candidate: String) -> Bool {
        //  Converted with Swiftify v1.0.6488 - https://objectivec2swift.com/
        if candidate.hasPrefix("http://") {
            return true
        }
        else if candidate.hasPrefix("https://") {
            return true
        }
        else if candidate.hasPrefix("www") {
            return true
        }
        else {
            return false
        }
    }
    
    func getAge(from date:String) -> String{
        var age = ""
        let dateFormator = DateFormatter()
        dateFormator.dateFormat = "yyyy-MM-dd"
        dateFormator.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale
        var firstDate = dateFormator.date(from: date)
        let calendar = NSCalendar.current
        // Replace the hour (time) of both dates with 00:00
        
        if firstDate == nil{
            dateFormator.dateFormat = "yyyy-MM-dd"
            let someDateTime = dateFormator.date(from: "2000-01-01")
            firstDate = someDateTime
        }
        
        let date1 = calendar.startOfDay(for: firstDate!)
        let date2 = calendar.startOfDay(for: Date())
        let components = calendar.dateComponents([.year], from: date1, to: date2)
        age = "\(components.year ?? 21)"
        return age
    }
    
    
    func clearNotificationData(){
        self.isFromNotification =  false
        self.notificationInfoDict = nil
        self.notificationType = ""
    }
    
    
    
    //MARK: - Shake Views
    func shakeTextField(_ txtField: UITextField) {
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.10
        animation.repeatCount = 2
        animation.autoreverses = true
        animation.fromValue = CGPoint(x:txtField.center.x - 7,y:txtField.center.y)
        animation.toValue = CGPoint(x:txtField.center.x + 7, y:txtField.center.y)
        txtField.layer.add(animation,forKey: "position")
    }
    
    func shakeViewField(_ view: UIView) {
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.10
        animation.repeatCount = 2
        animation.autoreverses = true
        animation.fromValue = CGPoint(x:view.center.x - 7,y:view.center.y)
        animation.toValue = CGPoint(x:view.center.x + 7, y:view.center.y)
        view.layer.add(animation,forKey: "position")
    }
    
    //MARK: - Date time format
    func timeFormat(forAPI date: Date) -> String {
        let formatter = DateFormatter()
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale
        formatter.dateFormat = "hh:mm a"
        let formattedTime: String = formatter.string(from: date)
        return formattedTime.uppercased()
    }
    
    // Set deafult Times from string
    func setDateFrom(_ time: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale
        dateFormatter.dateFormat = "hh:mm a"
        let date = dateFormatter.date(from: time)
        return date!
    }

    
    func setDateFromString(time: String) -> Date {
         let newDate = time
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
         dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let date = dateFormatter.date(from: newDate)
        return date!
    }
    func setDateFromString1(time: String) -> Date {
        let newDate = time
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy/MM/dd"
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let date = dateFormatter.date(from: newDate)
        return date!
    }
    func setDateFromString4(time: String) -> Date {
        let newDate = time
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let date = dateFormatter.date(from: newDate)
        return date!
    }
  
    func crtToDateString(crd: String) -> String {
        let newDate = crd
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let date = dateFormatter.date(from: newDate)
        let formattedTime: String = dateFormatter.string(from: date ?? Date())
        return formattedTime
    }
    
    func currentToRequiredDate(date: String,current:String,required:String) -> String {
        let newDate = date
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = current
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale
        let date = dateFormatter.date(from: newDate)
        dateFormatter.dateFormat = required

        let formattedTime: String = dateFormatter.string(from: date ?? Date())
        return formattedTime
    }
    
    
    
    
    func startTimerForHoldBookings(){
        let bookingHoldingTimeInMinute : Int = 5
        let calendar = Calendar.current
        let date = calendar.date(byAdding: .minute , value: bookingHoldingTimeInMinute, to: Date())
        UserDefaults.standard.set(date, forKey: UserDefaults.keys.lastBookingTime)
        UserDefaults.standard.synchronize()
        self.startTimer()
    }
    func startTimer() {
        if timer == nil {
            timer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(self.checkBookingHoldTime), userInfo: nil, repeats: true)
        }
    }
    
    
    @objc func checkBookingHoldTime(){
        
        if let lastBookingTime = UserDefaults.standard.object(forKey: UserDefaults.keys.lastBookingTime) as? Date{
            if Date() >= lastBookingTime   {
                //call API
                //self.callWebserviceFor_deleteAllbooking()
                
                // post a notification
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SessionExpiredNotification"), object: nil, userInfo: nil)
                // `default` is now a property, not a method call
                
            }else{
            }
        }else{
            self.stopTimerForHoldBookings()
        }
    }
    
    func dateFormat(forAPI date: Date) -> String {
        let dateFormator = DateFormatter()
        dateFormator.dateFormat = "dd-MM-yyyy"
        let formatedDate: String = dateFormator.string(from: date)
        return formatedDate
    }
    
    
    func dateFormatInDevideForm(forAPI date: Date) -> String {
        let dateFormator = DateFormatter()
        dateFormator.dateFormat = "dd/MM/yyyy"
        let formatedDate: String = dateFormator.string(from: date)
        return formatedDate
    }
    func dateFormatInFormYou(forAPI date: Date) -> String {
        let dateFormator = DateFormatter()
        dateFormator.dateFormat = "yyyy-MM-dd"
        let formatedDate: String = dateFormator.string(from: date)
        return formatedDate
    }
    
    func dateFormatToShow(forAPI date: Date) -> String {
        let dateFormator = DateFormatter()
        dateFormator.dateFormat = "dd-MM-yyyy"
        let formatedDate: String = dateFormator.string(from: date)
        return formatedDate
    }
    
    func dateFormatToShow1(forAPI date: Date) -> String {
        let dateFormator = DateFormatter()
        dateFormator.dateFormat = "yyyy-MM-dd"
        let formatedDate: String = dateFormator.string(from: date)
        return formatedDate
    }
    
    func dateFormatToShowDace(forAPI date: Date) -> String {
        let dateFormator = DateFormatter()
        dateFormator.dateFormat = "dd/MM/yyyy"
        let formatedDate: String = dateFormator.string(from: date)
        return formatedDate
    }
    
    func dateFormatAccourdingToMe(forAPI date: Date,formet:String) -> String {
        let dateFormator = DateFormatter()
        dateFormator.dateFormat = formet
        let formatedDate: String = dateFormator.string(from: date)
        return formatedDate
    }
    
    func dateFormat2(forAPI date: Date) -> String {
        let dateFormator = DateFormatter()
        dateFormator.dateFormat = "yyyy-MM-dd"
        let formatedDate: String = dateFormator.string(from: date)
        return formatedDate
    }
    
    //MARK: - Alerts
    func showAlert(withMessage message:String ,type : String, on Controller:UIViewController) {
        let sb = UIStoryboard(name:"Main",bundle:Bundle.main)
        let objVC = sb.instantiateViewController(withIdentifier:"alertVC") as! alertVC
        objVC.modalPresentationStyle = .overFullScreen
        objVC.alertMessage = message
        objVC.alertType = type
        Controller.present(objVC, animated: false, completion: nil)
    }
    
    func showDeafultAlert(withTitle title:String, message:String, on Controller:UIViewController){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let subView = alertController.view.subviews.first!
        let alertContentView = subView.subviews.first!
        alertContentView.backgroundColor = UIColor.gray
        alertContentView.layer.cornerRadius = 20
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(OKAction)
        Controller.present(alertController, animated: true, completion: nil)
    }
    
    //MARK: - Clear user data
    func clearUserData(){
        UserDefaults.standard.setValue(nil,forKey: UserDefaults.keys.userInfo)
        UserDefaults.standard.setValue(nil, forKey: UserDefaults.keys.service)
        UserDefaults.standard.set(nil, forKey: UserDefaults.keys.authToken)
        UserDefaults.standard.set(nil, forKey: UserDefaults.keys.businessHour)
        UserDefaults.standard.set(nil, forKey: UserDefaults.keys.outCallData)
        UserDefaults.standard.set(nil, forKey: UserDefaults.keys.bio)
        UserDefaults.standard.synchronize()
    }
    
    func stopTimerForHoldBookings(){
        UserDefaults.standard.set(nil, forKey: UserDefaults.keys.lastBookingTime)
        UserDefaults.standard.synchronize()
       self.stopTimer()
    }
    
    func stopTimer() {
        if timer != nil {
            timer?.invalidate()
            timer = nil
        }
    }
  
}

extension AppShareData{
    func callWebserviceFor_deleteAllbooking() {
        //var isServiceDeleted : Bool = false
        if !objServiceManager.isNetworkAvailable(){
            //objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        
        //objActivity.startActivityIndicator()
        var strUserId : String = ""
        if let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: UserDefaults.keys.userInfo){
            if let userId = dict["_id"] as? Int {
                strUserId = "\(userId)"
            }
        }
        
        let parameters : Dictionary = [
            "userId" : strUserId,
            ] as [String : Any]
        
        
        objWebserviceManager.requestPost(strURL: WebURL.addArtistCertificate, params: parameters  , success: { response in
            
            //objWebserviceManager.StopIndicator()
            let keyExists = response["responseCode"] != nil
            if  keyExists {
                // sessionExpireAlertVC(controller: self)
            }else{
                
                let strStatus =  response["status"] as? String ?? ""
                _ = response["message"] as? String ?? kErrorMessage
                if strStatus == k_success{
                    self.stopTimerForHoldBookings()
                    self.objAppdelegate.clearData()
                    self.objAppdelegate.gotoTabBar(withAnitmation: false)
                }else{
                    
                }
            }
        }){ error in
            
            if (error.localizedDescription.contains("The network connection was lost.")){
                self.callWebserviceFor_deleteAllbooking()
            }else{
            }
            //objWebserviceManager.StopIndicator()
            //objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }

    
    //MARK: - Image thumb with size
    func generateImage(fromURL URL: URL, withSize size:CGSize) -> UIImage {
        let asset = AVAsset.init(url: URL)
        let assetImageGenerator = AVAssetImageGenerator(asset: asset)
        
        assetImageGenerator.appliesPreferredTrackTransform=true;
        
        let maxSize = size
        assetImageGenerator.maximumSize = maxSize
        
        var time = asset.duration
        time.value = min(time.value, 2)
        
        do {
            
            let imageRef = try assetImageGenerator.copyCGImage(at: time, actualTime: nil)
            return UIImage(cgImage: imageRef)
            
        } catch {
            print(error)
        }
        return UIImage()
    }
}



extension AppShareData{
    func callWebserviceFor_deleteStaff() {
        if !objServiceManager.isNetworkAvailable(){
            //objAppShareData.showAlert(withMessage:"",type: alertType.noNetwork, on: self)
            return
        }
        
        objActivity.startActivityIndicator()
        let strBusinessId = objAppShareData.objModelHoldAddStaff.businessId
        let strStaffId = objAppShareData.objModelHoldAddStaff.staffId
        
        let parameters : Dictionary = [
            "businessId" : strBusinessId,"staffId" : strStaffId
            ] as [String : Any]
        
        objWebserviceManager.requestPost(strURL: WebURL.deleteStaff, params: parameters  , success: { response in
            
            objWebserviceManager.StopIndicator()
            let keyExists = response["responseCode"] != nil
            if  keyExists {
               // sessionExpireAlertVC(controller: self)
                
            }else{
                
                let strStatus =  response["status"] as? String ?? ""
                _ = response["message"] as? String ?? kErrorMessage
                // arvind , why used above line
                if strStatus == k_success{
                    objAppShareData.objModelEditTimeSloat.arrFinalTimes.removeAll()
                    objAppShareData.staffDeleted = true
                }else{
                    
                }
            }
        }){ error in
            
            if (error.localizedDescription.contains("The network connection was lost.")){
                self.callWebserviceFor_deleteStaff()
            }else{
                
            }
            //objWebserviceManager.StopIndicator()
            //objAppShareData.showAlert(withMessage: "", type: alertType.error, on: self)
        }
    }
}

//MARK: - add string method
extension AppShareData {
 func showCompletionTime( title:String) -> String {
        var newTime = ""
        let arrTime = title.split(separator: ":")
        
        if arrTime.count > 1{
            let str1 = arrTime[0]
            let str2 = arrTime[1]
            if str1 == "00"{
                newTime = str2+" min"
            }else{
                newTime = str1+" hr "+str2+" min"
            }
        }else{
            newTime = title
        }
        return newTime
    }
}

//MARK: - call webservice for save artist service from local data to server data
extension AppShareData{
    
    func callWebserviceForAnyAPIWithoutResponceCheck(param:[String:Any],api:String){
        if !objServiceManager.isNetworkAvailable(){
            return
        }
        objServiceManager.requestPost(strURL: api, params: param, success: { response in
             let  status = response["status"] as? String ?? ""
            if status == "success"{
            }else{
            }
        }) { error in
            objActivity.stopActivity()
         }
    }
    
    
    
    func getDayFromSelectedDate(strDate:String)-> String{
        var Day = "Day"
        if strDate == "6"{
            Day = "Sunday"//"Sun"
        }else if strDate == "0"{
            Day = "Monday"//Mon"
        }else if strDate == "1"{
            Day = "Tuesday"//Tue"
        }else if strDate == "2"{
            Day = "Wednesday"//"Wed"
        }else if strDate == "3"{
            Day = "Thursday"//"Thu"
        }else if strDate == "4"{
            Day = "Friday"//"Fri"
        }else if strDate == "5"{
            Day = "Saturday"//"Sat"
        }else{
            Day = "Day"
        }
        return Day
    }
    

        func matchesTwoDate(dateA:Date, type:String) -> Bool{
            let currentDate = Date()
            let CurrentTimeZone = NSTimeZone(abbreviation: "GMT")
            let SystemTimeZone = NSTimeZone.system as NSTimeZone
            let currentGMTOffset: Int? = CurrentTimeZone?.secondsFromGMT(for: currentDate)
            let SystemGMTOffset: Int = SystemTimeZone.secondsFromGMT(for: currentDate)
            let interval = TimeInterval((SystemGMTOffset - currentGMTOffset!))
            let TodayDate = Date(timeInterval: interval, since: currentDate)
            
            var dateB:Date = TodayDate
            //// Test
            let calendar = Calendar.current
            dateB = calendar.date(byAdding: .minute, value: 60, to: dateB)!
            ////
           
            var addIndex = false
            
            switch dateA.compare(dateB) {
                
            case .orderedAscending:
                addIndex = true
                print(type+"Date A is later than date B")
            case .orderedDescending:
                addIndex = false
                print(type+"Date A is earlier than date B")
            case .orderedSame:
                addIndex = true
                print(type+"The two dates are the same")
            }
            return addIndex
        }
        
        func getTimeFromTime(strDate:String)-> String{
            let formatter  = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd hh:mm a"
            formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale
            let todayDate = formatter.date(from: strDate)
            let formatedDate: String = formatter.string(from: todayDate!)
            return formatedDate
        }
    
    func getDateFromCRD(strDate:String)-> Date{
        let formatter  = DateFormatter()
       // "2019-04-25T05:17:30+00:00"
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale
        let todayDate = formatter.date(from: strDate)
      //  let formatedDate: String = formatter.string(from: todayDate ?? Date())
        return todayDate ?? Date()
    }
}


//MARK: - update location send near by notification and mesure distance
extension AppShareData{
    func UpdateLocationOnDatabase(){
        let isLoggedIn : Bool =  UserDefaults.standard.bool(forKey: UserDefaults.keys.isLoggedIn)
        let dict  =  UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] ?? ["":""]
        let isDocument  = dict[UserDefaults.keys.isDocument] as? String ?? ""
        if isLoggedIn {
            if isDocument == "3" {
                DispatchQueue.main.asyncAfter(deadline: .now() + 8) {
                    if objAppShareData.onTracking == true{
                        let myCurrentLatitude = String(objAppShareData.objModelBusinessSetup.trackingLatitude)
                        let myCurrentLongitude = String(objAppShareData.objModelBusinessSetup.trackingLongitude)
                       self.callWebserviceForUpdateLocation(strLat: myCurrentLatitude, strLong: myCurrentLongitude)
                    }
                }
            }
        }
    }
    
    func callWebserviceForUpdateLocation(strLat:String,strLong:String){
        let dict = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] ?? ["":""]
        var lat = strLat
        var long = strLong
        let MainLat = dict[UserDefaults.keys.latitude] as? String ?? ""
        let MainLong = dict[UserDefaults.keys.latitude] as? String ?? ""
        let trackLat = dict[UserDefaults.keys.trackingLatitude] as? String ?? ""
        let trackLong = dict[UserDefaults.keys.trackingLongitude] as? String ?? ""
        if lat == "" || long == ""{
            lat = trackLat
            long = trackLong
            if lat == "" || long == ""{
                lat = MainLat
                long = MainLong
            }
        }
        let address =  objAppShareData.objModelBusinessSetup.trackingAddress
        let dicParam = ["trackingLatitude":lat, "trackingLongitude":long,"trackingAddress":address]
       // objServiceManager.StartIndicator()
        objServiceManager.requestPost(strURL: WebURL.updateRecord, params: dicParam, success:{ response in
            objServiceManager.StopIndicator()
            if response["status"] as? String == "success"{
                 self.UpdateLocationOnDatabase()
                self.sendNotificationToNearestCustomer()
            }else{
                self.UpdateLocationOnDatabase()
            }
        }) { error in
             objServiceManager.StopIndicator()
             self.UpdateLocationOnDatabase()
        }
    }
    
    func callWebserviceForUpdateBusinessOutcallAddressp(dicParam:[String:Any]){
        objServiceManager.requestPost(strURL: WebURL.updateRecord, params: dicParam, success:{ response in  if response["status"] as? String == "success"{  }else{   }  }) { error in    }
    }
    
    func sendNotificationToNearestCustomer(){
        let dictCustomer = UserDefaults.standard.value(forKey: UserDefaults.keys.ParamCustomerTracking) as? [String:Any] ?? ["":""]
        let lat = dictCustomer[UserDefaults.keys.CustomerLat] as? String ?? "22.35"
        let long = dictCustomer[UserDefaults.keys.CustomerLong] as? String ?? "75.35"
        let id = dictCustomer[UserDefaults.keys.CustomerId] as? String ?? ""
        let serviceId = dictCustomer[UserDefaults.keys.CustomerBookingInfoId] as? String ?? ""
        let bookingId = dictCustomer[UserDefaults.keys.CustomerBookingId] as? String ?? ""

        let coordinate₀ = CLLocation(latitude: Double(lat)  ?? 0.0, longitude: Double(long) ?? 0.0)
        let coordinate₁ = CLLocation(latitude: Double(objAppShareData.objModelBusinessSetup.trackingLatitude) ?? 0.0, longitude:  Double(objAppShareData.objModelBusinessSetup.trackingLongitude) ?? 0.0)
        let distanceInMeters = coordinate₀.distance(from: coordinate₁)
        let dict  =  UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] ?? ["":""]
        let myId  = dict[UserDefaults.keys.id] as? String ?? ""
        let reachedStatus = objAppShareData.bookingInfiStatus
        if distanceInMeters <= 50{
            //if serviceId != "" && id != "" && lat != "22.35" && long !=  "75.35" && serviceId != "" && bookingId != "" && reachedStatus != "4" {
            if serviceId != "" && id != "" && serviceId != "" && bookingId != "" && reachedStatus != "4" {
                let param =  ["userId":id,
                    "id":serviceId,
                    "artistId":myId,
                    "bookingId":bookingId]
                 objServiceManager.requestPost(strURL: WebURL.nearByNotification, params: param, success:{ response in
                    objServiceManager.StopIndicator()
                    if response["status"] as? String == "success"{
                    }else{
                    }
                }) { error in
                    objServiceManager.StopIndicator()
                }
            }
        }
    }
    
    func callWebserviceForFindOnTheWayUser(){
        let artistId = UserDefaults.standard.string(forKey: UserDefaults.keys.newAdminId) ?? ""
        let dictCustomer = UserDefaults.standard.value(forKey: UserDefaults.keys.ParamCustomerTracking) as? [String:Any] ?? ["":""]
        let dictMyInfo = UserDefaults.standard.value(forKey: UserDefaults.keys.businessInfoDic) as? [String:Any] ?? ["":""]
        let myId =  dictMyInfo[UserDefaults.keys.id] as? String ?? ""
        let id = myId
 
        let param = ["artistId":id]
        objServiceManager.StopIndicator()
        objServiceManager.requestPost(strURL: WebURL.checkTrackingBooking, params: param, success:{ response in
            objServiceManager.StopIndicator()
            if response["status"] as? String == "success"{
                
                    print("response = ",response)
                    if let data = response["data"] as? [String:Any]{
                        let latitude = data["latitude"] as? String ?? ""
                        let longitude = data["longitude"] as? String ?? ""
                        let userId = data["userId"] as? Int ?? 0
                        let bookingId = data["_id"] as? Int ?? 0
                        var bookingInfoId = ""

                        if let bookingInfo = data["data"] as? [[String:Any]]{
                            if  bookingInfo.count > 0{
                                let obj = bookingInfo[0]
                                if let ids = obj["_id"] as? Int{
                                    bookingInfoId = String(ids)
                                }
                                
                                if let ids = obj["status"] as? Int{
                                    objAppShareData.bookingInfiStatus = String(ids)
                                }else if let ids = obj["status"] as? String{
                                    objAppShareData.bookingInfiStatus  = String(ids)
                                }
                                
                            }}
                        
                        let paramCustomer = ["CustomerLat":latitude,
                                             "CustomerLong":longitude,
                                             "CustomerId":String(userId),
                                             "CustomerBookingInfoId":bookingInfoId,
                                             "CustomerBookingId":String(bookingId)] as [String : Any]
                        UserDefaults.standard.set(paramCustomer, forKey: UserDefaults.keys.ParamCustomerTracking)
                    }
                if objAppShareData.onTracking == false{
                    objAppShareData.onTracking = true
                        self.UpdateLocationOnDatabase()
                }
            }else{
                objAppShareData.onTracking = false
            }
        }) { error in
            objAppShareData.onTracking = false
            objServiceManager.StopIndicator()
        }
    }
    
    
    
    
    func timeAgoSinceDateForNotificationList(date:NSDate, numericDates:Bool) -> String {
        let calendar = NSCalendar.current
        let now = NSDate()
        let earliest = now.earlierDate(date as Date)
        let latest = (earliest == now as Date) ? date : now
        
        let unitsSet : Set<Calendar.Component> = [.year,.month,.weekOfYear,.day, .hour, .minute, .second, .nanosecond]
        
        let components:NSDateComponents =  calendar.dateComponents(unitsSet, from: earliest, to: latest as Date) as NSDateComponents
        
        let yesterdayDate  = Calendar.current.date(byAdding: .day, value: -1, to: Date())
       // let oneDayOldDate  = Calendar.current.date(byAdding: .day, value: -0, to: date as Date)

       var dateOfOneDayOld = ""
        let yesterday = "\(String(describing: yesterdayDate))"
       let yesterDay = yesterday.components(separatedBy: " ")
        if yesterDay.count > 0{
            dateOfOneDayOld = String(yesterDay[0])
        }
        
        var returnDate = ""
        let newDate = "\(date)"
        let bbDate = newDate.components(separatedBy: " ")
        if bbDate.count > 0{
            returnDate = String(bbDate[0])
        }
        
        if (components.year >= 1){
            return "Before This Year"
        } else if (components.month >= 2) {
            return "This Year"
        } else if (components.month >= 1){
            return "This Year"
        } else if (components.weekOfYear >= 2) {
            return "This Month"
        } else if (components.weekOfYear >= 1){
            return "This Month"
        } else if (components.day >= 2) {
            return "This Week"
        } else if (components.day >= 1){
            if (components.day >= 1) && (components.day < 2){
                if returnDate == dateOfOneDayOld{
                    return "Yesterday"
                }else{
                    return "This Week"
                }
            }else{
                return "New"
            }
        } else{
            
            
            
            var c = ""
            let a = "\(Date())"
            let b = a.components(separatedBy: " ")
            if b.count > 0{
                c = String(b[0])
            }
            
            var d = ""
            let e = "\(date)"
            let f = e.components(separatedBy: " ")
            if f.count > 0{
                d = String(f[0])
            }
 
            if c == d{
                return "New"
            }else{
                return "Yesterday"
            }
            
        }
    }
}
extension UIViewController{
    
    func locationAuthorization() -> Bool{
        
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse{
            //self.openLocationAlert()
            return false
        }else if CLLocationManager.authorizationStatus() == .authorizedAlways{
            //self.dismissLocationAlert()
            return true
        }
        else if (CLLocationManager.authorizationStatus() == .denied) {
            //self.openLocationAlert()
            return false
        } else {
            //self.openLocationAlert()
            return false
        }
    }
}

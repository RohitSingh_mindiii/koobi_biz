//
//  ServicesModel.swift
//  MualabBusiness
//
//  Created by Mac on 23/01/2018 .
//  Copyright © 2018 Mindiii. All rights reserved.
//

import Foundation

class artistServices:NSObject {
    var _id : Int?
    var title : String?
    var subService = [SubService]()
    
    init?(id:Int,title:String,subService:[SubService]) {
        self._id = id
        self.title = title
        self.subService = subService
    }
}
class SubService:NSObject {
    var _id : Int?
    var serviceId : Int?
    var image : String?
    var title : String?
    var subCategories = [SubCategories]()
    
    init?(id:Int,title:String,serviceId:Int,image:String) {
        self._id = id
        self.title = title
        self.serviceId = serviceId
        self.image = image
    }
}

class SubCategories:NSObject {
    var _id : Int?
    var title : String?
    var serviceDescription : String?
    var incallPrice : Float?
    var oucallPrice : Float?
    var complitionTime: String?
    
    init?(title:String,desc:String,incallPrice:Float,oucallPrice:Float,complitionTime:String) {
        self.title = title
        self.serviceDescription = desc
        self.incallPrice = incallPrice
        self.oucallPrice = oucallPrice
        self.complitionTime = complitionTime
    }
}
